kubectl config set-cluster hrv-cluster01 --server="$STAGING_KUBE_URL" --insecure-skip-tls-verify=true
kubectl config set-credentials deploy --token="$STAGING_KUBE_TOKEN"
kubectl config set-context default --cluster=hrv-cluster01 --user=deploy
kubectl config use-context default
helm init --client-only
helm fetch \
  --repo http://35.185.184.111:8000/kube-template \
  --untar \
  --untardir ./chart \
    hrw-web
helm template --set CI_JOB_ID=$CI_JOB_ID --values ./build/values_staging.yml ./chart/hrw-web > kubernetes.yml
kubectl apply -f kubernetes.yml


helm fetch \
  --repo http://35.185.184.111:8000/kube-template \
  --untar \
  --untardir ./chart \
    hrw-esb
helm template --set CI_JOB_ID=$CI_JOB_ID --values ./build/values_staging_worker.yml ./chart/hrw-esb > kubernetes.yml
kubectl apply -f kubernetes.yml