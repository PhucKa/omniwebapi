﻿using BHN.Core;
using BHN.Entity;
using Haravan.Utils;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository.Infractstructure
{
    public interface IMongoBaseRepository<TEntity, TId> where TEntity : NoSqlEntityBase<TId>
    {
        Task<TEntity> AddAsync(TEntity entity);
        Task UpdateAsync(TEntity entity, params Expression<Func<TEntity, object>>[] properties);
        Task UpdateIgnoreAsync(TEntity entity, params Expression<Func<TEntity, object>>[] properties);
        Task DeleteAsync(TEntity entity);
        Task<List<TEntity>> AddManyAsync(List<TEntity> list);
        Task<TEntity> SetAsync(TEntity entity);
        Task<TEntity> GetByIdAsync(TId id);
        Task DeleteAsync(TId id);
        Task DeleteAsync(TId[] ids);
        Task<List<TEntity>> GetByIdAsync(TId[] ids);
    }
    public abstract class MongoBaseRepository<TEntity, TId> : IMongoBaseRepository<TEntity, TId> where TEntity : NoSqlEntityBase<TId>
    {
        private readonly IMongoDatabase _data;
        private readonly string entityName;
        private readonly string seqEntityName;

        public MongoBaseRepository(IMongoDatabase data)
        {
            _data = data;
            entityName = typeof(TEntity).Name;
            var nameAttribute = typeof(TEntity).GetTypeInfo().GetCustomAttribute<SeqEntityNameAttribute>(true);
            if (nameAttribute == null)
                seqEntityName = entityName;
            else
                seqEntityName = nameAttribute.Name;

            Collection = _data.GetCollection<TEntity>(entityName);
        }
        protected IMongoCollection<TEntity> Collection { get; private set; }

        protected BsonDocument GetFilterRaw<T>(FilterDefinition<T> filter)
        {
            if (filter == null) return null;

            return filter.Render(BsonSerializer.SerializerRegistry.GetSerializer<T>()
                    , BsonSerializer.SerializerRegistry);
        }

        protected BsonDocument GetUpdateRaw<T>(UpdateDefinition<T> update)
        {
            if (update == null) return null;

            return update.Render(BsonSerializer.SerializerRegistry.GetSerializer<T>()
                    , BsonSerializer.SerializerRegistry);
        }

        protected virtual Tuple<FilterDefinition<TEntity>, string, DateTime> GetUpdateFilterDefinition(TEntity entity)
        {
            var filter = Builders<TEntity>.Filter.Where(m => m._id.Equals(entity._id));

            var name = Helpers.GetIgnoreUpdateField<TEntity>();

            if (name != null)
            {
                var entityDate = Helpers.GetPropertyValue<DateTime, TEntity>(entity, name);

                if (entityDate != DateTime.MinValue)
                {
                    var date = Builders<TEntity>.Filter.Lte(name, entityDate);
                    var not_exist = Builders<TEntity>.Filter.Not(Builders<TEntity>.Filter.Exists(name));

                    var dateFilter = Builders<TEntity>.Filter.Or(date, not_exist);

                    var rs = Builders<TEntity>.Filter.And(filter, dateFilter);

                    return new Tuple<FilterDefinition<TEntity>, string, DateTime>(rs, name, entityDate);
                }

            }

            return new Tuple<FilterDefinition<TEntity>, string, DateTime>(filter, null, DateTime.MinValue);
        }

        public virtual async Task<TEntity> AddAsync(TEntity entity)
        {
            Helpers.ToAllUtcDate(entity);

            await Collection.InsertOneAsync(entity).ConfigureAwait(false);

            return entity;
        }

        public virtual async Task<List<TEntity>> AddManyAsync(List<TEntity> list)
        {
            if (list == null || list.Count == 0) return list;

            foreach (var obj in list)
            {
                Helpers.ToAllUtcDate(obj);
            }

            await Collection.InsertManyAsync(list).ConfigureAwait(false);

            return list;
        }

        public virtual async Task<TEntity> SetAsync(TEntity entity)
        {
            Helpers.ToAllUtcDate(entity);

            var filter = GetUpdateFilterDefinition(entity);

            await Collection.ReplaceOneAsync(filter.Item1, entity, new UpdateOptions() { IsUpsert = true }).ConfigureAwait(false);

            return entity;
        }

        public virtual async Task UpdateAsync(TEntity entity, params Expression<Func<TEntity, object>>[] properties)
        {
            Helpers.ToAllUtcDate(entity);

            var filter = GetUpdateFilterDefinition(entity);

            if (properties == null || !properties.Any())
            {

                await Collection.ReplaceOneAsync(filter.Item1, entity).ConfigureAwait(false);
            }
            else
            {
                UpdateDefinition<TEntity> upd = null;
                Type type = typeof(TEntity);
                foreach (var pro in properties)
                {
                    var propertyName = pro.GetPropertyName();
                    var propertyValue = type.GetProperty(propertyName).GetValue(entity);

                    upd = _invoke(propertyName, propertyValue, upd);
                }

                //if has attribute ignore update then force update this field
                if (filter.Item2 != null)
                {
                    var propertyName = filter.Item2;
                    var propertyValue = filter.Item3;
                    upd = _invoke(propertyName, propertyValue, upd);
                }

                await Collection.UpdateOneAsync(filter.Item1, upd).ConfigureAwait(false);
            }
        }

        public virtual async Task UpdateIgnoreAsync(TEntity entity, params Expression<Func<TEntity, object>>[] properties)
        {
            if (properties == null || !properties.Any()) throw new ArgumentNullException("properties");

            var ignoreNames = properties.Select(m => m.GetPropertyName()).ToList();

            Type type = typeof(TEntity);
            var update_properties = type.GetProperties().Where(m => !ignoreNames.Contains(m.Name));

            var forUpdate_properties = update_properties.Select(m => ExpressionHelper.GetExpression<TEntity, object>(m.Name)).ToArray();

            await UpdateAsync(entity, forUpdate_properties).ConfigureAwait(false);
        }

        private UpdateDefinition<TEntity> _invoke(string propertyName, object value, UpdateDefinition<TEntity> upd)
        {
            Type type = typeof(object);
            if (value != null)
            {
                type = value.GetType();
            }

            MethodInfo method = this.GetType().GetMethod("_set", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            MethodInfo generic = method.MakeGenericMethod(type);
            return (UpdateDefinition<TEntity>)generic.Invoke(this, new object[] { propertyName, value, upd });
        }

        protected UpdateDefinition<TEntity> _set<T>(string pName, T obj, UpdateDefinition<TEntity> upd)
        {
            if (upd == null)
            {
                var builder = Builders<TEntity>.Update;
                upd = builder.Set(pName, obj);
            }
            else
            {
                upd = upd.Set(pName, obj);
            }

            return upd;
        }

        public virtual async Task DeleteAsync(TEntity entity)
        {
            await Collection.DeleteOneAsync(m => m._id.Equals(entity._id)).ConfigureAwait(false);
        }

        public virtual async Task<List<TEntity>> GetByIdAsync(TId[] ids)
        {
            return await Collection.Find(m => ids.Contains(m._id)).ToListAsync().ConfigureAwait(false);
        }

        public virtual async Task<TEntity> GetByIdAsync(TId id)
        {
            return await Collection.Find(m => m._id.Equals(id)).SingleOrDefaultAsync().ConfigureAwait(false);
        }

        public virtual async Task DeleteAsync(TId id)
        {
            await Collection.DeleteOneAsync(m => m._id.Equals(id)).ConfigureAwait(false);
        }

        public virtual async Task DeleteAsync(TId[] ids)
        {
            if (ids == null || !ids.Any()) return;
            await Collection.DeleteManyAsync(m => ids.Contains(m._id)).ConfigureAwait(false);
        }
    }
}
