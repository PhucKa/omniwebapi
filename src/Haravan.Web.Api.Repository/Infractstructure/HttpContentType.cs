﻿namespace Haravan.Web.Api.Repository.Infractstructure
{
    public enum HttpContentType
    {
        Json = 1,
        Xml = 2
    }

    public class HttpMessageResponse
    {
        public int statusCode { get; set; }
        public string response { get; set; }
    }
}