﻿using Haravan.Libs.Abstractions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public static class ReportApiRepository
    {
        public static HttpClient HttpClient = new HttpClient();

        private const string HeaderStoreId = "X-ECOM-STORE";
        private const string HeaderUserId = "X-ECOM-USER";

        private const string HeaderUserName = "X-ECOM-USERNAME";
        private const string HeaderUserEmail = "X-ECOM-USEREMAIL";
        private const string HeaderUserAgentClient = "X-ECOM-USERAGENT-CLIENT";
        private const string HeaderUserAgentServer = "X-ECOM-USERAGENT-SERVER";
        private const string HeaderIfNoneMatch = "If-None-Match";
        private const string HeaderXCache = "X-Cache";
        private const string HeaderEtag = "ETag";

        public static async Task<T> SendRequestWebAsync<T>(IRequestContext context, string ReportApiUrl, string path, HttpMethod method, object data = null)
        {
            var content = await CallWebApi(context, ReportApiUrl, path, method, data);
            if (!string.IsNullOrWhiteSpace(content))
            {
                var st = JsonConvert.DeserializeObject<ResultData>(content);
                if (st.storeId != context.OrgId)
                {
                    return default(T);
                }
                return JsonConvert.DeserializeObject<T>(content);
            }
            else
                return default(T);
        }

        public class ResultData
        {
            public long storeId { get; set; }
        }

        private static async Task<string> CallWebApi(IRequestContext context, string ReportApiUrl, string path, HttpMethod method, object data = null)
        {
            var url = $"{ReportApiUrl}{path}";
            var requestMsg = new HttpRequestMessage(method, url);
            requestMsg.Headers.Add(HeaderStoreId, context.OrgId.ToString());
            requestMsg.Headers.Add(HeaderUserId, context.UserId.ToString());
            requestMsg.Headers.Add(HeaderUserName, WebUtility.UrlEncode(context.UserName));
            requestMsg.Headers.Add(HeaderUserEmail, WebUtility.UrlEncode(context.UserEmail));
            if (data != null)
            {
                if (data is string)
                {
                    requestMsg.Content = new StringContent((string)data, Encoding.UTF8, "application/json");
                }
                else if (data is List<Tuple<string, object>>)
                {
                    var formData = new MultipartFormDataContent();
                    foreach (var pair in data as List<Tuple<string, object>>)
                    {
                        if (pair.Item2 is byte[])
                        {
                            formData.Add(new ByteArrayContent(pair.Item2 as byte[]), pair.Item1, pair.Item1);
                        }
                        else
                        {
                            if (pair.Item2 != null)
                            {
                                formData.Add(new StringContent(pair.Item2.ToString()), pair.Item1);
                            }
                        }
                    }
                    requestMsg.Content = formData;
                }
                else
                {
                    requestMsg.Content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");
                }
            }

            using (var rq = await HttpClient.SendAsync(requestMsg).ConfigureAwait(false))
            {
                if (rq.IsSuccessStatusCode || rq.StatusCode == HttpStatusCode.NotModified)
                {
                    if (rq.StatusCode == HttpStatusCode.NotModified)
                    {
                        return null;
                    }

                    if (rq.Content != null)
                    {
                        return await rq.Content.ReadAsStringAsync();
                    }
                    else
                        return null;
                }
                else
                {
                    throw new Exception($"request to {url} error {rq.StatusCode}");
                }
            }
        }
    }
}