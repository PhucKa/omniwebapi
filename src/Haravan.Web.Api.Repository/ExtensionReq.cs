﻿using BHN.SharedObject.EBSMessage;
using Haravan.Caching;
using Haravan.Libs.Abstractions;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Configs;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class ExtensionPerRequest : IExtensionPerRequest
    {
        private long _storeid;
        private long _orgid;
        private long _userid;
        private string _username;
        private string _useremail;
        private readonly AppConfig config;
        private readonly ICache cache;

        private readonly IServiceProvider serviceProvider;

        public ExtensionPerRequest(
            IServiceProvider serviceProvider,
            IOptions<AppConfig> config, ICache cache)
        {
            this.serviceProvider = serviceProvider;
            this.config = config.Value;
            this.cache = cache;

            IRequestContext extReg = serviceProvider.GetService<IRequestContext>();
            IInternalApiContext intReq = serviceProvider.GetService<IInternalApiContext>();
            if (extReg != null && extReg.OrgId.HasValue && this._storeid <= 0)
            {
                this.StoreId = extReg.OrgId.Value;
                this.OrgId = extReg.OrgId.Value;
                this.UserId = extReg.UserId.Value;
                this.UserName = extReg.UserName;
                this.UserEmail = extReg.UserEmail;
                this._storeid = extReg.OrgId.Value;
            }
            else if (intReq != null && intReq.OrgId.HasValue && this._storeid <= 0)
            {
                this.StoreId = intReq.OrgId.Value;
                this.OrgId = intReq.OrgId.Value;
                this.UserId = intReq.UserId.Value;
                this.UserName = intReq.UserName;
                this.UserEmail = intReq.UserEmail;
                this._storeid = intReq.OrgId.Value;
            }
        }



        public long StoreId
        {
            get
            {
                return this._storeid;
            }
            set
            {
                this._storeid = value;
            }
        }

        public long UserId
        {
            get
            {
                return this._userid;
            }
            set
            {
                this._userid = value;
            }
        }

        public string UserName
        {
            get
            {
                return this._username;
            }
            set
            {
                this._username = value;
            }
        }


        public string UserEmail
        {
            get
            {
                return this._useremail;
            }
            set
            {
                this._useremail = value;
            }
        }

        public long OrgId
        {
            get
            {
                return this._orgid;
            }
            set
            {
                this._orgid = value;
            }
        }

        public async Task<StoreData> GetStoreData(CancellationToken cctoken = default(CancellationToken))
        {
            return await GetStoreData(_storeid, cctoken);
        }

        public async Task<StoreData> GetStoreData(long storeId, CancellationToken cctoken)
        {
            var key = $"{DefaultData.DefaultPrefixEcomCache}{CacheKeys.GetStoreSellerData(storeId)}";
            var cacheKey = CacheKey.Create(new DistributedCacheEntryOptions() { SlidingExpiration = new TimeSpan(config.SessionTimeout, 1, 0) }, key);
            try
            {
                return await cache.GetAsync<StoreData>(cacheKey, async (token) =>
                {
                    return await getStoreData(storeId, token);
                }, cctoken);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private async Task<StoreData> getStoreData(long storeId, CancellationToken cctoken = default(CancellationToken))
        {
            var rpStoreData = serviceProvider.GetService<IStoreDataUserRepository>();
            var data = new StoreData();
            data = await rpStoreData.GetStoreData();
            return data;
        }

        public async Task<string> GetStoreDomain(bool onlyDomain = false)
        {
            var rpStoreData = serviceProvider.GetService<IStoreDataUserRepository>();

            var data = new StoreData();
            data = await rpStoreData.GetStoreData();
            var result = data.SellerDomain;
            result = result.EndsWith("/") ? result : (result + "/");

            if (onlyDomain)
                result = result
                    .Replace("https://", "")
                    .Replace("http://", "")
                    .Replace("/admin/", "");

            return result;
        }


        public async Task<string> GetAdminLink(string subdomain)
        {
            var result = $"{subdomain}.{config.Domain}";
            result = result.EndsWith("/") ? result : (result + "/");
            result = result
                    .Replace("https://", "")
                    .Replace("http://", "")
                    .Replace("/admin/", "");

            return result;
        }


        public async Task<string> GetStoreSubDomain()
        {
            var rpStoreData = serviceProvider.GetService<IStoreDataUserRepository>();
            var data = new StoreData();
            data = await rpStoreData.GetStoreData();
            return data.SubDomain;
        }
        public async Task<string> GetBuyerDomain()
        {
            var rpStoreData = serviceProvider.GetService<IStoreDataUserRepository>();
            var data = new StoreData();
            data = await rpStoreData.GetStoreData();
            var result = data.BuyerDomain;
            return result.EndsWith("/") ? result : (result + "/");
        }
    }

    public interface IExtensionPerRequest
    {
        long StoreId { get; set; }
        long OrgId { get; set; }
        string UserName { get; set; }
        string UserEmail { get; }
        long UserId { get; set; }

        Task<StoreData> GetStoreData(CancellationToken cctoken = default(CancellationToken));

        Task<StoreData> GetStoreData(long storeId, CancellationToken cctoken);

        Task<string> GetStoreDomain(bool onlyDomain = false);

        Task<string> GetStoreSubDomain();
        Task<string> GetBuyerDomain();
        Task<string> GetAdminLink(string subdomain);
    }
}