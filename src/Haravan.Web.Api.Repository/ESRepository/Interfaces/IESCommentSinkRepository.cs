﻿using BHN.SharedObject.ESBuyerModel;
using Haravan.Web.Api.BusinessObjects.ESModels;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IESCommentSinkRepository : IESBuyerRepository<ESCommentModelv2>
    {
        Task DeleteByArticleIdAsync(long storeid, long articleid);
    }
}