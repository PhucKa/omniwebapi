﻿using BHN.SharedObject.ESBuyerModel;
using Haravan.Web.Api.BusinessObjects.ESModels;

namespace Haravan.Web.Api.Repository
{
    public interface IESArticleSinkRepository : IESBuyerRepository<ESArticleModelv2>
    {
    }
}