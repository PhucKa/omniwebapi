﻿using BHN.SharedObject.ESBuyerModel;

namespace Haravan.Web.Api.Repository
{
    public interface IESArticleRepository : IESBuyerRepository<ESArticleModel>
    {
    }
}