﻿using BHN.SharedObject.ESBuyerModel;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IESCommentRepository : IESBuyerRepository<ESCommentModel>
    {
        Task DeleteByArticleIdAsync(long storeid, long articleid);
    }
}