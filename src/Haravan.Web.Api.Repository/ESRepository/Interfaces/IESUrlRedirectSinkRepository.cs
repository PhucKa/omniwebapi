﻿using Haravan.Web.Api.BusinessObjects.ESModels;

namespace Haravan.Web.Api.Repository
{
    public interface IESUrlRedirectSinkRepository : IESBuyerRepository<ESUrlRedirectModelv2>
    {
    }
}