﻿using BHN.SharedObject.ESBuyerModel;
using Haravan.Web.Api.BusinessObjects.ESModels;

namespace Haravan.Web.Api.Repository
{
    public interface IESBlogSinkRepository : IESBuyerRepository<ESBlogModelv2>
    {
    }
}