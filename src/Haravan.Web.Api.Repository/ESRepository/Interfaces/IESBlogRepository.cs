﻿using BHN.SharedObject.ESBuyerModel;

namespace Haravan.Web.Api.Repository
{
    public interface IESBlogRepository : IESBuyerRepository<ESBlogModel>
    {
    }
}