﻿using Haravan.Web.Api.BusinessObjects.ESModels;

namespace Haravan.Web.Api.Repository
{
    public interface IESUrlRedirectRepository : IESBuyerRepository<ESUrlRedirectModel>
    {
    }
}