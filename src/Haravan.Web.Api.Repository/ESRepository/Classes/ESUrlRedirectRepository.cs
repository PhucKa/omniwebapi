﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.ESModels;
using Nest;

namespace Haravan.Web.Api.Repository
{
    public class ESUrlRedirectRepository : ESBuyerRepository<ESUrlRedirectModel>, IESUrlRedirectRepository
    {
        public ESUrlRedirectRepository(IElasticClient client)
            : base(client)
        {
            typeFriendlyName = "esurlredirectmodel";
        }

        protected override void Init_Expression_Allow_Fields(SearchExpressionAllowFields<ESUrlRedirectModel> fields)
        {
            fields
                .Fields(m => m.OldPath)
                .Fields(m => m.OldPath_ori)
                .Fields(m => m.RedirectTo_ori)
                .Fields(m => m.RedirectTo)
                .Fields(m => m.Id)
                .Fields(m => m.StoreId)
                ;
            base.Init_Expression_Allow_Fields(fields);
        }

        protected override IPromise<Fields> GetFreeTextFields(FieldsDescriptor<ESUrlRedirectModel> fields)
        {
            fields
                .Field(m => m.OldPath)
                .Field(m => m.OldPath_ori)
                .Field(m => m.RedirectTo_ori)
                .Field(m => m.RedirectTo)
                ;
            return base.GetFreeTextFields(fields);
        }
    }
}