﻿using BHN.Core.Repository;
using BHN.SharedObject.ESBuyerModel;
using Haravan.Web.Api.BusinessObjects.ESModels;
using Nest;

namespace Haravan.Web.Api.Repository
{
    public class ESArticleSinkRepository : ESBuyerRepository<ESArticleModelv2>, IESArticleSinkRepository
    {
        public ESArticleSinkRepository(IElasticClient client)
            : base(client)
        {
            typeFriendlyName = "article";
        }

        protected override IPromise<Fields> GetFreeTextFields(FieldsDescriptor<ESArticleModelv2> fields)
        {
            fields
                .Field(m => m.Title)
                .Field(m => m.Content)
                .Field(m => m.AuthorName)
                .Field(m => m.Tags)
                ;

            return base.GetFreeTextFields(fields);
        }

        protected override void Init_Expression_Allow_Fields(SearchExpressionAllowFields<ESArticleModelv2> fields)
        {
            fields
                .Fields("title", "title.sort", false)
                .Fields("body", m => m.Content)
                .Fields("author", m => m.AuthorName)
                .Fields("tag", m => m.Tags)
                .Fields("created_at", m => m.CreatedDate)
                .Fields("published_at", m => m.PublishedDate)
                .Fields(m => m.BlogId)
                .Fields("BlogTitle", "blogTitle.sort", false)
                .Fields(m => m.Id)
                .Fields(m => m.StoreId)
                .Fields("Tags", "tags.sort", false)
                .Fields(m => m.AuthorId)
                ;

            base.Init_Expression_Allow_Fields(fields);
        }
    }
}