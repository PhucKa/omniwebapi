﻿using BHN.Core.Repository;
using BHN.SharedObject.ESBuyerModel;
using Haravan.Web.Api.BusinessObjects.ESModels;
using Nest;
using System;
using System.Collections.Generic;
using System.Text;

namespace Haravan.Web.Api.Repository
{
    
    public class ESPageSinkRepository : ESBuyerRepository<ESPageModelv2>, IESPageSinkRepository
    {
        public ESPageSinkRepository(IElasticClient client)
            : base(client)
        {
            typeFriendlyName = "page";
        }
        protected override void Init_Expression_Allow_Fields(SearchExpressionAllowFields<ESPageModelv2> fields)
        {
            fields
               .Fields("title", "title.sort", false)
               .Fields("handle", m => m.UrlHandle)
               .Fields("body", m => m.Content)
               .Fields("author", "authorName.sort", false)
               .Fields(m => m.Id)
               .Fields(m => m.StoreId)
               ;
            base.Init_Expression_Allow_Fields(fields);
        }
        protected override IPromise<Fields> GetFreeTextFields(FieldsDescriptor<ESPageModelv2> fields)
        {
            fields
                .Field(m => m.Title)
                .Field(m => m.UrlHandle)
                .Field(m => m.Content)
                .Field(m => m.AuthorName)
                ;
            return base.GetFreeTextFields(fields);
        }


    }
}
