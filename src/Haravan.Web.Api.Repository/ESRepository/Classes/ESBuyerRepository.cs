﻿using BHN.Core;
using BHN.Core.Repository;
using BHN.Entity;
using Haravan.Utils;
using Nest;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IESBuyerRepository<T> : IESRepository<T> where T : ESEntity, new()
    {
        Task<ESSearchResult> SearchAsync(long storeId, FilterSearchModelBase andModel);
        Task<ESSearchResult> SearchAndBetweenAndModelWithOrModel(long storeId, FilterSearchModelBase andModel, FilterSearchModelBase orModel);
        Task<ESSearchResult> SearchOrBetweenFieldsInModel(long storeId, FilterSearchModelBase orModel);
        
        QueryContainer GetFreeTextQuery(string freetext);

        
        
        Task<ESSearchResult> CountAsync(long storeId, FilterSearchModelBase andModel);
    }
    public class ESBuyerRepository<T> : ESRepository<T>, IESBuyerRepository<T> where T : ESEntity, new()
    {
        public ESBuyerRepository(IElasticClient client)
            : base(client)
        {

        }
        private static readonly Regex regexSpecial = new Regex(@"[\=\|\[\]\\/\(\)\{\}`'~!@#$%^&*-.,_+<>""]", RegexOptions.Compiled);
        private static readonly string[] esspecialsymbols = new string[] { "+","-", "=", "&&" ,"||", ">" ,"<", "!","(", ")", "{",
            "}", "[", "]", "*" ,"?", ":","^","~"};
        private static readonly List<string> lstKeyword = new List<string>()
        {
            "lastmonth","lastweek","lastday","lastyear","in","after","before","today","<",">","<=",">=","<>",
            "missingfield","existfield","queryscript","beforeequalsnow","afterextract","beforeextract","last3month",
            "yesterday","!*","**","%*","*=","!%","*%","exists","!empty","missing","not in","=","inlast1day","inlast2day",
            "inlast3day","inlast4day","inlast6day","inlast6day","inlast7day","inlast15day","inlast30day"
        };
        private DateTime parseToDateTimeSymbol(string time)
        {
            switch (time.ToLower())
            {
                case "lastmonth":
                    return base.GetBeginUtcOfDay(DateTime.UtcNow.Date.AddDays(-30), this.TimeOffset);
                case "lastweek":
                    return base.GetBeginUtcOfDay(DateTime.UtcNow.Date.AddDays(-7), this.TimeOffset);
                case "lastday":
                    return base.GetBeginUtcOfDay(DateTime.UtcNow.Date.AddDays(-1), this.TimeOffset);

            }
            return base.GetBeginUtcOfDay(DateTime.UtcNow.Date, this.TimeOffset);
        }

        public QueryContainer GetFreeTextQuery(string freetext)
        {
            return GetFreeTextFieldsQuery(freetext);
        }

        protected override QueryContainer ParseFilterSearchField(FilterSearchField f)
        {
            QueryContainerDescriptor<T> filter = new QueryContainerDescriptor<T>();
            
           
            if (f.IsNummeric == true) return base.ParseFilterSearchField(f);
            if (string.IsNullOrWhiteSpace(f.OptionValue)) throw new InvalidOperationException();
            #region new operator
            if (f.OptionValue != null)
            {
                f.OptionValue = StringHelper.UnSignString(f.OptionValue).ToLower();
                switch (f.OptionValue.ToLower())
                {
                    case "missingfield":
                        try
                        {
                            var tmpFi = base.GetExpressionFilterField(f.FieldName);
                            if (tmpFi.IsNested)
                            {
                                return filter.Nested(m => m.Path(tmpFi.Path).Query(h => h.Missing(k => k.Field(tmpFi.Field))));
                            }
                            else
                            {
                                return filter.Missing(m => m.Field(base.GetExpressionFilterField(f.FieldName).Field));
                            }

                        }
                        catch
                        {
                            return filter.Missing(m => m.Field(base.GetFieldName(f.FieldName)));
                        }
                    case "existfield":
                        try
                        {
                            var tmpFi = base.GetExpressionFilterField(f.FieldName);
                            if (tmpFi.IsNested)
                            {
                                return filter.Nested(m => m.Path(tmpFi.Path).Query(h => h.Exists(k => k.Field(tmpFi.Field))));
                            }
                            else
                            {
                                return filter.Exists(m => m.Field(base.GetExpressionFilterField(f.FieldName).Field));
                            }

                        }
                        catch
                        {
                            return filter.Exists(m => m.Field(base.GetFieldName(f.FieldName)));
                        }
                    case "beforeequalsnow":
                        f.OptionValue = "Before";
                        f.OptionValueValue = DateTime.UtcNow.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'");
                        return base.ParseFilterSearchField(f);
                    case "afternow":
                        f.OptionValue = "After";
                        f.OptionValueValue = DateTime.UtcNow.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'");
                        return base.ParseFilterSearchField(f);
                    case "after":
                        DateTime dateAfter;
                        if (DateTime.TryParse(f.OptionValueValue, out dateAfter))
                        {
                            f.OptionValueValue = base.GetBeginUtcOfDay(dateAfter.AddDays(1), base.TimeOffset).ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'");
                            return base.ParseFilterSearchField(f);
                        }
                        else
                            throw new InvalidOperationException();
                    case "afterextract":
                        DateTime dateAfterEx = Convert.ToDateTime(f.OptionValueValue).AddHours(-base.TimeOffset);
                        return filter.DateRange(k => k.GreaterThanOrEquals(dateAfterEx).Field(base.GetFieldName(f.FieldName)));
                    case "before":
                        DateTime dateBefore;
                        if (DateTime.TryParse(f.OptionValueValue, out dateBefore))
                        {
                            f.OptionValueValue = base.GetBeginUtcOfDay(dateBefore, base.TimeOffset).ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'");
                            return base.ParseFilterSearchField(f);
                        }
                        else
                            throw new InvalidOperationException();
                    case "beforeextract":
                        var dateBeforeEx = Convert.ToDateTime(f.OptionValueValue).AddHours(-base.TimeOffset);
                        return filter.DateRange(k => k.LessThanOrEquals(dateBeforeEx).Field(base.GetFieldName(f.FieldName)));
                    case "yesterday":
                        f.OptionValue = "inlast1day";
                        return base.ParseFilterSearchField(f);
                    case "lastyear":
                        return filter.DateRange(k => k.GreaterThanOrEquals(base.GetBeginUtcOfDay(DateTime.UtcNow.Date.AddYears(-1), base.TimeOffset)).Field(f.FieldName));
                    case "queryscript":
                        return filter.Script(sc => sc.Inline(f.OptionValueValue).Lang("groovy"));
                    case "notpharse":
                        f.OptionValue = "**";
                        return filter.Bool(m => m.MustNot(base.ParseFilterSearchField(f)));
                    case "notequal":
                        f.OptionValue = "=";
                        return filter.Bool(m => m.MustNot(base.ParseFilterSearchField(f)));
                    case "contains":
                        filter.QueryString(m => m.Query(f.OptionValueValue).Analyzer("index_whitespace").DefaultField(base.GetFieldName(f.FieldName)));
                        return filter.Bool(m => m.MustNot(base.ParseFilterSearchField(f)));
                    default:
                        try
                        {
                            return base.ParseFilterSearchField(f);
                        }
                        catch
                        {
                            f.OptionValueValue = null;
                            return base.ParseFilterSearchField(f);
                        }

                }
            }
            #endregion
            return base.ParseFilterSearchField(f);
        }
        
        public async Task<ESSearchResult> SearchAndBetweenAndModelWithOrModel(long storeId, FilterSearchModelBase andModel, FilterSearchModelBase orModel)
        {
            var qStore = Query<T>
                .Term(m => m.Field(a => a.StoreId).Value(storeId));
            if (andModel != null && orModel != null)
            {
                var andquery = parseAndFieldsInModel(andModel);
                var qAndExp = parseSubFilter(andModel);
                var qOrExp = parseSubFilter(orModel);
                var orQuery = parseOrFieldsInModel(orModel);
                var qFreeText = GetFreeTextFieldsQuery(andModel.FreeText);
                var q = qStore && qFreeText && (andquery && qAndExp) && (orQuery || qOrExp);
                var sort = parseSort(andModel);
                return await base.SearchAsync(storeId, andModel.Page.pageSize, andModel.Page.currentPage, m => q, sort);
            }
            else if (andModel != null && orModel == null)
            {
                var andquery = parseAndFieldsInModel(andModel);
                var qAndExp = parseSubFilter(andModel);
                var qFreeText = GetFreeTextFieldsQuery(andModel.FreeText);
                var q = qStore && qFreeText && (andquery && qAndExp);
                var sort = parseSort(andModel);
                return await base.SearchAsync(storeId, andModel.Page.pageSize, andModel.Page.currentPage, m => q, sort);
            }
            else
            {
                var qOrExp = parseSubFilter(orModel);
                var orQuery = parseOrFieldsInModel(orModel);
                var qFreeText = GetFreeTextFieldsQuery(orModel.FreeText);
                var q = qStore && qFreeText && (orQuery || qOrExp);
                var sort = parseSort(orModel);
                return await base.SearchAsync(storeId, orModel.Page.pageSize, orModel.Page.currentPage, m => q, sort);
            }

        }
        public async Task<ESSearchResult> SearchOrBetweenFieldsInModel(long storeId, FilterSearchModelBase orModel)
        {
            var qStore = Query<T>
                .Term(m => m.Field(a => a.StoreId).Value(storeId));
            var orQuery = parseOrFieldsInModel(orModel);
            var qExp = parseSubFilter(orModel);
            var q = qStore && (orQuery || qExp);
            var sort = parseSort(orModel);
            return await base.SearchAsync(storeId, orModel.Page.pageSize, orModel.Page.currentPage, m => q, sort);
        }
        protected QueryContainer parseOrFieldsInModel(FilterSearchModelBase orModel)
        {
            if (orModel == null) return null; ;

            if (orModel.Fields == null || orModel.Fields.Count == 0) return null;
            var lstQuery = new List<QueryContainer>();
            orModel.Fields.ForEach(item =>
            {
                lstQuery.Add(ParseFilterSearchField(item));
            });
            QueryContainer orQuery = null;
            QueryContainerDescriptor<T> filter = new QueryContainerDescriptor<T>();
            orQuery = filter.Bool(m => m.Should(lstQuery.ToArray()));
            return orQuery;
        }
        protected QueryContainer parseAndFieldsInModel(FilterSearchModelBase andModel)
        {
            if (andModel == null) return null; ;
            QueryContainer andQuery = null;
            if (andModel.Fields == null || andModel.Fields.Count == 0) return null;
            var lstQuery = new List<QueryContainer>();
            andModel.Fields.ForEach(item =>
            {
                lstQuery.Add(ParseFilterSearchField(item));
            });
            QueryContainerDescriptor<T> filter = new QueryContainerDescriptor<T>();
            andQuery = filter.Bool(m => m.Must(lstQuery.ToArray()));
            return andQuery;
        }
        public override async Task<ESSearchResult> SearchAsync(long storeId, FilterSearchModelBase andModel)
        {
            if (!string.IsNullOrWhiteSpace(andModel.FreeText) || !string.IsNullOrWhiteSpace(andModel.FilterExpression))
            {
                andModel.FreeText = formatstring(andModel.FreeText);
                var andquery = parseAndFieldsInModel(andModel);
                var qFreeText = GetFreeTextFieldsQuery(andModel.FreeText);
                var qExp = base.parseSubFilter(andModel);
                var q = qFreeText && andquery && qExp;
                var sort = parseSort(andModel);
                var result = await base.SearchAsync(storeId, andModel.Page.pageSize, andModel.Page.currentPage, m => q, sort);
                return result;
            }

            var ret = await base.SearchAsync(storeId, andModel);
            return ret;
        }
        public async Task<ESSearchResult> CountAsync(long storeId, FilterSearchModelBase andModel)
        {
            andModel.SortExpression = "";
            andModel.SortFieldName = "";
            if (!string.IsNullOrWhiteSpace(andModel.FreeText) || !string.IsNullOrWhiteSpace(andModel.FilterExpression))
            {
                andModel.FreeText = formatstring(andModel.FreeText);
                var andquery = parseAndFieldsInModel(andModel);
                var qFreeText = GetFreeTextFieldsQuery(andModel.FreeText);
                var qExp = base.parseSubFilter(andModel);
                var q = qFreeText && andquery && qExp;

                var result = await base.SearchAsync(storeId, andModel.Page.pageSize, andModel.Page.currentPage, m => q, null);
                return result;
            }
            else
            {
                var andquery = parseAndFieldsInModel(andModel);
                var qExp = base.parseSubFilter(andModel);
                var q = andquery && qExp;
                var result = await base.SearchAsync(storeId, andModel.Page.pageSize, andModel.Page.currentPage, m => q, null);
                return result;
            }


        }
        protected string formatstring(string text)
        {
            if (string.IsNullOrWhiteSpace(text)) return null;
            var spcial = text.Split(' ');
            var newStr = "";
            foreach (var item in spcial)
            {
                var t = item.Trim();
                if (t.Length > 1)
                {

                    foreach (var ne in esspecialsymbols)
                    {
                        var fi = t.IndexOf(ne);
                        if (fi >= 0)
                        {
                            t = t.Replace(ne, "\\" + ne);
                        }
                    }
                    newStr = newStr + " " + t;
                }
            }
            newStr = newStr.Replace(@"\", "\\").Replace(@"""", "\\\"").Replace("/", "\\/").Trim();
            return newStr;
        }
        private Func<SortDescriptor<T>, IPromise<IList<ISort>>> parseSort(FilterSearchModelBase model)
        {
            Func<SortDescriptor<T>, IPromise<IList<ISort>>> sort = null;
            //default sort freetext empty
            if (string.IsNullOrEmpty(model.FreeText) || string.IsNullOrWhiteSpace(model.FreeText))
            {
                sort = m =>
                {
                    return base.ParseSortField(model);
                };
                return sort;
            }
            else
            {
                if (string.IsNullOrEmpty(model.SortFieldName)) model.SortFieldName = "Id";
                if (model.SortType == null || model.SortType == "desc")
                {
                    if (!string.IsNullOrEmpty(model.FreeText) && !string.IsNullOrWhiteSpace(model.FreeText))
                    {
                        sort = m => m
                                    .Field(b =>
                                    {
                                        try
                                        {
                                            var tt = b.Field(base.GetExpressionFilterField(model.SortFieldName).Field)
                                                .Descending();

                                            return tt;
                                        }
                                        catch
                                        {
                                            var tt = b.Field(base.GetFieldName(model.SortFieldName))
                                                .Descending();

                                            return tt;
                                        }
                                    });


                    }
                    else
                    {
                        sort = m => m
                                    .Field(b =>
                                    {
                                        try
                                        {
                                            var tt = b.Field(base.GetExpressionFilterField(model.SortFieldName).Field)
                                                .Descending();

                                            return tt;
                                        }
                                        catch
                                        {
                                            var tt = b.Field(base.GetFieldName(model.SortFieldName))
                                                .Descending();

                                            return tt;
                                        }
                                    });
                    }

                }
                else
                {
                    if (!string.IsNullOrEmpty(model.FreeText) && !string.IsNullOrWhiteSpace(model.FreeText))
                    {
                        sort = m => m
                                                            .Field(b =>
                                                            {
                                                                try
                                                                {
                                                                    var tt = b.Field(base.GetExpressionFilterField(model.SortFieldName).Field)
                                                                        .Ascending();

                                                                    return tt;
                                                                }
                                                                catch
                                                                {
                                                                    var tt = b.Field(base.GetFieldName(model.SortFieldName))
                                                                        .Ascending();

                                                                    return tt;
                                                                }

                                                            });

                    }
                    else
                    {
                        sort = m => m
                                                           .Field(b =>
                                                           {
                                                               try
                                                               {
                                                                   var tt = b.Field(base.GetExpressionFilterField(model.SortFieldName).Field)
                                                                       .Ascending();

                                                                   return tt;
                                                               }
                                                               catch
                                                               {
                                                                   var tt = b.Field(base.GetFieldName(model.SortFieldName))
                                                                       .Ascending();

                                                                   return tt;
                                                               }
                                                           });
                    }

                }

            }
            return sort;
        }
        

        
        
    }
}
