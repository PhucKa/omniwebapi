﻿using BHN.Core.Repository;
using BHN.SharedObject.ESBuyerModel;
using Nest;

namespace Haravan.Web.Api.Repository
{
    public class ESBlogRepository : ESBuyerRepository<ESBlogModel>, IESBlogRepository
    {
        public ESBlogRepository(IElasticClient client)
            : base(client)
        {
            typeFriendlyName = "blog";
        }

        protected override IPromise<Fields> GetFreeTextFields(FieldsDescriptor<ESBlogModel> fields)
        {
            fields
                .Field(m => m.Title)
                .Field(m => m.UrlHandle)
                .Field(m => m.Tags)
                ;

            return base.GetFreeTextFields(fields);
        }

        protected override void Init_Expression_Allow_Fields(SearchExpressionAllowFields<ESBlogModel> fields)
        {
            fields
                .Fields("title", "title.sort", false)
                .Fields("handle", m => m.UrlHandle)
                .Fields("created_at", m => m.CreatedDate)
                .Fields(m => m.Id)
                .Fields(m => m.StoreId)
                .Fields("Tags", "tags.sort", false)
                ;

            base.Init_Expression_Allow_Fields(fields);
        }
    }
}