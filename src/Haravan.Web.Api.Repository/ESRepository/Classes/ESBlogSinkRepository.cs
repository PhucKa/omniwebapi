﻿using BHN.Core.Repository;
using BHN.SharedObject.ESBuyerModel;
using Haravan.Web.Api.BusinessObjects.ESModels;
using Nest;

namespace Haravan.Web.Api.Repository
{
    public class ESBlogSinkRepository : ESBuyerRepository<ESBlogModelv2>, IESBlogSinkRepository
    {
        public ESBlogSinkRepository(IElasticClient client)
            : base(client)
        {
            typeFriendlyName = "blog";
        }

        protected override IPromise<Fields> GetFreeTextFields(FieldsDescriptor<ESBlogModelv2> fields)
        {
            fields
                .Field(m => m.Title)
                .Field(m => m.UrlHandle)
                .Field(m => m.Tags)
                ;

            return base.GetFreeTextFields(fields);
        }

        protected override void Init_Expression_Allow_Fields(SearchExpressionAllowFields<ESBlogModelv2> fields)
        {
            fields
                .Fields("title", "title.sort", false)
                .Fields("handle", m => m.UrlHandle)
                .Fields("created_at", m => m.CreatedDate)
                .Fields(m => m.Id)
                .Fields(m => m.StoreId)
                .Fields("Tags", "tags.sort", false)
                ;

            base.Init_Expression_Allow_Fields(fields);
        }
    }
}