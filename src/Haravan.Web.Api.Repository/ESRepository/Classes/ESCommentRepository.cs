﻿using BHN.Core.Repository;
using BHN.SharedObject.ESBuyerModel;

using Nest;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class ESCommentRepository : ESBuyerRepository<ESCommentModel>, IESCommentRepository
    {
        public ESCommentRepository(IElasticClient client)
            : base(client)
        {
            typeFriendlyName = "comment";
        }

        protected override IPromise<Fields> GetFreeTextFields(FieldsDescriptor<ESCommentModel> fields)
        {
            fields
                .Field(m => m.Comment)
                .Field(m => m.ArticleTitle)
                ;

            return base.GetFreeTextFields(fields);
        }

        protected override void Init_Expression_Allow_Fields(SearchExpressionAllowFields<ESCommentModel> fields)
        {
            fields
                .Fields("title", m => m.ArticleTitle)
                .Fields(m => m.Comment)
                .Fields(m => m.Id)
                .Fields(m => m.StoreId)
                .Fields(m => m.Status)
                .Fields(m => m.UpdatedDate);
            ;

            base.Init_Expression_Allow_Fields(fields);
        }
        public async Task DeleteByArticleIdAsync(long storeid, long articleid)
        {
            var rs = await base.Client.DeleteByQueryAsync<ESCommentModel>(this.index,
                           typeof(ESCommentModel).Name.ToLower(),
                           m => m.Query(h => h.Bool(
                               k => k.Must(f1 => f1.Term(f11 => f11.Field(n => n.StoreId).Value(storeid)),
                                           f2 => f2.Term(f22 => f22.Field(n => n.ArticleId).Value(articleid))
                                           ))));


        }
    }
}