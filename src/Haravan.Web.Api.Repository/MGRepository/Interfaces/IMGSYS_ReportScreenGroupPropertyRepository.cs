﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IMGSYS_ReportScreenGroupPropertyRepository : INoSqlRepository<MGSYS_ReportScreenGroupProperty>
    {
        Task<List<MGSYS_ReportScreenGroupProperty>> GetReportScreenProperty(long storeid, long screenid);
    }
}