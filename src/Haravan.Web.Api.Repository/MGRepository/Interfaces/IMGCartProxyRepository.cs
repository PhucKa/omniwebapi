using System.Threading.Tasks;
using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;

namespace Haravan.Web.Api.Repository
{
    public interface IMGCartProxyRepository : INoSqlRepository<MGCartProxyModel>
    {
        Task<MGCartProxyModel> GetByAppId(long storeId, long appId);
        Task<MGCartProxyModel> GetById(long storeId, long cartProxyId);
        Task<MGCartProxyModel> GetFirst(long storeId);
    }
}