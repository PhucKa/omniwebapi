﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IMGDomainPriceRepository : INoSqlRepository<MGDomainPrice>       
    {
        Task<MGDomainPrice> GetBy(string domainExtension);

        Task<List<MGDomainPrice>> GetAll();
    }
}