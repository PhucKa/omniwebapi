﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IMGSYS_ReportScreenRepository : INoSqlRepository<MGSYS_ReportScreen>
    {
        Task<List<MGSYS_ReportScreen>> GetReportScreenByStoreId(long storeid);

        Task<MGSYS_ReportScreen> GetReportScreenById(long storeid, long id);

        Task<MGSYS_ReportScreen> GetReportScreenById(long storeid, string nameReport);
    }
}