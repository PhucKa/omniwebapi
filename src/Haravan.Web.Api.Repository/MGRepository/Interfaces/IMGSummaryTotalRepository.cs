﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IMGSummaryTotalRepository : INoSqlRepository<MGSummaryTotalModel>
    {
        Task<List<string>> GetSummaryTotalNames(int Type, long StoreId);
        Task<List<MGSummaryTotalModel>> GetSummaryTotals(int Type, long StoreId);



    }
}
