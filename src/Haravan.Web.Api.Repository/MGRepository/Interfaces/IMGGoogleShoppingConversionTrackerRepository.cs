﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IMGGoogleShoppingConversionTrackerRepository : INoSqlRepository<MGGoogleShoppingConversionTrackerModel>
    {
        Task DeleteManyAsync(long storeId);
    }
}