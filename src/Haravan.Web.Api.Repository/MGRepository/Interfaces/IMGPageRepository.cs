﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IMGPageRepository : INoSqlRepository<MGPageModel>
    {
        Task<MGPageModel> GetById(long storeId, long id);

        Task<List<MGPageModel>> GetById(long storeId, List<long> ids);

        Task<List<MGPageModel>> GetById(long storeId, Dictionary<int, long> lids);

        Task DeleteByIdAsync(long storeId, long id);

        Task<List<long>> GetByPagingAsync(long storeId, int currentpage, int pagesize);

        Task<long> CountAsync(long storeId);

        Task<List<MGPageModel>> GetByStoreId(long storeId);

        Task<string> GetHandleByIdAsync(long storeId, long id);

        Task<MGPageModel> GetByHandleUrl(string handleUrl, long storeId);

        Task<List<MGPageModel>> GetByListUrlHandle(long storeId, List<string> handleUrl);

        Task<long> GetIdByUrlHandle(string handleUrl, long storeId);
    }
}