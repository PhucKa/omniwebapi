﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IMGGoogleSiteVerificationRepository : INoSqlRepository<MGGoogleSiteVerificationModel>
    {
        Task DeleteManyAsync(long storeId);
    }
}