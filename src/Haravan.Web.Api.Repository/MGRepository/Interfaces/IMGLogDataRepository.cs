﻿using Haravan.Web.Api.BusinessObjects.MongoModels;
using Haravan.Web.Api.Repository.Infractstructure;
using MongoDB.Bson;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IMGLogDataRepository : IMongoBaseRepository<MGLogData, ObjectId>
    {
        Task PostLog(MGLogData model);

        Task<MGLogData> GetById(long storeId, long refid, long doctypeid);

        Task DeleteByIdAsync(long storeId, long refid, long doctypeid);
    }
}