﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IMGBlogRepository : INoSqlRepository<MGBlogModel>
    {
        Task<MGBlogModel> GetById(long storeId, long id);

        Task<List<MGBlogModel>> GetById(long storeId, List<long> ids);

        Task<List<long>> GetByPagingAsync(long storeId, int currentpage, int pagesize);

        Task<long> CountAsync(long storeId);

        Task<List<MGBlogModel>> GetBlogsByStoreId(long storeId);

        Task DeleteByIdAsync(long storeId, long id);

        Task<string> GetHandleByIdAsync(long storeId, long id);

        Task<MGBlogModel> GetByHandleUrl(string handleUrl, long storeId);

        Task<List<MGBlogModel>> GetByListUrlHandle(long storeId, List<string> handleUrl);

        Task<long> GetByUrlHandle(string handleUrl, long storeId);

        Task<MGBlogModel> GetFirstByHandle(long storeId);
    }
}