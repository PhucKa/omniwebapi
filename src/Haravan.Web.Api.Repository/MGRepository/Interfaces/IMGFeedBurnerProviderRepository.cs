﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IMGFeedBurnerProviderRepository : INoSqlRepository<MGFeedBurnerProviderModel>
    {
        Task<List<MGFeedBurnerProviderModel>> GetAll();
    }
}