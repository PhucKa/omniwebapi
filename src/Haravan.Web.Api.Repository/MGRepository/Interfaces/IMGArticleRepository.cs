﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IMGArticleRepository : INoSqlRepository<MGArticleModel>
    {
        Task<long> CountByStoreAsync(long storeid);

        Task DeleteByIdAsync(long storeId, long id);

        Task<List<long>> GetByPagingAsync(long storeId, int currentpage, int pagesize);

        Task<MGArticleModel> GetById(long storeId, long id);

        Task<List<MGArticleModel>> GetArticlesByStoreId(long storeId);

        Task<List<MGArticleModel>> GetById(long storeId, Dictionary<int, long> articleListId);

        Task<List<long>> GetListByBlogId(long blogId, long storeId);

        Task<List<MGArticleModel>> GetByBlogIdAsync(long storeId, long blogId);

        Task<MGArticleModel> GetByHandle(long storeId, string handle);

        Task<MGArticleModel> GetFirstByHandle(long storeId);

        Task<MGArticleModel> GetBy(long storeId, long blogId, long articleId);

        Task<List<MGArticleModel>> GetBy(long storeId, List<long> articleIds);
    }
}