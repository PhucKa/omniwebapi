﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository.MGRepository
{
    public interface IMGRawLinkListFieldsRepository : INoSqlRepository<MGRawLinkListFields>
    {
        Task<MGRawLinkListFields> GetById(long storeId, long id);
        Task<List<MGRawLinkListFields>> GetByStoreId(long storeId);

        Task<List<MGRawLinkListFields>> GetListByLinkListId(long linkListId, long storeId);

        

        Task<List<MGRawLinkListFields>> GetByLinkListId(long linkListId, long storeId);

        Task<List<MGRawLinkListFields>> GetByRefId(long refId, int type, long storeId);

        Task<List<MGRawLinkListFields>> GetAllTreeDropDown_ByParentTreeHandle(string parentTreeHandle, long storeId);

        Task<MGRawLinkListFields> GetParentLink_ByDropdown(long dropdownId, string dropdownTreeHandle, long storeId);

        Task<List<MGRawLinkListFields>> GetBy(long refId, int fieldId, long linklistId, long storeId);

        Task<List<MGRawLinkListFields>> GetAllTreeByParentTreeHandle(string TreeHandle, long storeId);
    }
}