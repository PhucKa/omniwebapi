﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IMGCommentRepository : INoSqlRepository<MGCommentModel>
    {
        Task<long> CountAllCommentsByStoreId(long storeId);

        Task<List<long>> GetByPagingAsync(long storeId, int currentpage, int pagesize);

        
        Task<List<MGCommentModel>> GetByIdsAsync(long storeId, List<long> ids, bool isDeleted);

        Task<List<MGCommentModel>> GetBy(long[] ids, long storeId);

        Task<MGCommentModel> GetDetailById(long id, long storeId, bool isDeleted);

        Task<MGCommentModel> GetDetailById(long id, long storeId);

        Task<List<MGCommentModel>> GetByArticleId(long articleId, long storeId);

        Task DeleteByIdAsync(long storeId, long id);

        Task DeleteByIdArticleAsync(long storeId, long articleId);

        Task<List<MGCommentModel>> GetCommentsByArticleIds(List<long> articleIds, long storeId, int status = -1);
    }
}