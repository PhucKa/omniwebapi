﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository.MGRepository
{
    
    public interface IMGRawLinkListRepository : INoSqlRepository<MGRawLinkList>
    {
        Task<List<MGRawLinkList>> GetAllLinkList(long storeId);
        Task<List<MGRawLinkList>> GetAllLinkListByHandle(string handle, long storeId);
        Task<MGRawLinkList> GetByHandleUrl(string handleUrl, long storeId);
        Task<MGRawLinkList> GetLinkListByTreeHandle(string treeHandle, long storeId);
        Task<List<MGRawLinkList>> GetAlLinkListContainsTreeHandle(string oldParentTreeHandle, long storeId);
        Task<bool> CheckExistLinkListByHandle(string handle, long storeId);
        Task<List<MGRawLinkList>> GetByIds(List<long> ids, long storeId);
        Task<MGRawLinkList> GetById(long storeId, long id);
        Task<List<MGRawLinkList>> GetByStoreId(long storeId);
        Task DeleteByIdAsync(long storeId, long id);
    }
}
