﻿using BHN.Core.Repository;
using BHN.SharedObject.EBSMessage;
using Haravan.Web.Api.BusinessObjects.Models.Summary;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IMGSummaryRepository : INoSqlRepository<MGSummaryModel>
    {
        Task<long> ExistSummaryName(int Type, long RefId, long StoreId, string SummaryName);
        Task<List<string>> GetSummaryNames(int Type, long RefId, long StoreId);
        Task<List<MGSummaryModel>> GetSummaries(int Type, long RefId, long StoreId);
        
    }
}