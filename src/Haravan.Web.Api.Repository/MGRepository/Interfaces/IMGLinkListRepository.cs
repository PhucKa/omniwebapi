﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository.MGRepository
{
    public interface IMGLinkListRepository : INoSqlRepository<MGLinkListModel>
    {
        Task<MGLinkListModel> GetById(long storeId, long id);
        Task DeleteAsync(long storeId, long id);
    }
}