﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface ISysDomainRepository : INoSqlRepository<MGSysDomain>
    {
        Task<MGSysDomain> GetByDomainName(string domainName);
        Task RemoveByStoreId(long storeId);

        Task DeleteByIdAsync(long storeId, long id);

        Task<bool> CheckExistSubDomain(string subDomain);

        Task<bool> CheckExistDomain(string Domain);

        Task<bool> CheckExistDomain(string Domain, long storeId);

        Task<List<MGSysDomain>> GetListDomainByStore(long storeId);

        Task<MGSysDomain> GetStorePrimaryDomain(long storeId);

        Task<long?> GetStoreIdBySubDomain(string subDomain);

        Task<MGSysDomain> GetBySubDomain(string subDomain);

        Task<MGSysDomain> GetSubDomainByDomain(string domain);

        Task<List<MGSysDomain>> GetSubDomainByStoreId(List<long> listStoreId);

        Task<MGSysDomain> GetSubDomainByStoreId(long storeId);

        Task<MGSysDomain> GetByName(string domain);

        Task<MGSysDomain> GetById(long domainId);

        Task<List<MGSysDomain>> GetPrimaryDomainByStoreIds(List<long> storeIds);

        Task<MGSysDomain> GetByDomainId(long domainId, long storeId);

        Task<MGSysDomain> GetByDomainName(long storeId, string domainName);
    }
}