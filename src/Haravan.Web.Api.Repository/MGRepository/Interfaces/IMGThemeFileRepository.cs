﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IMGThemeFileRepository : INoSqlRepository<MGThemeFileModel>
    {
        Task<MGThemeFileModel> GetByFileNameAsync(long storeId, long themeId, string fileName);
    }
}
