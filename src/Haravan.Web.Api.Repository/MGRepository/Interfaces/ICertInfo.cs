﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface ICertInfo : INoSqlRepository<MGCertInfo>
    {
        Task<List<MGCertInfo>> GetByIds(List<long> certInfoIds);
    }
}