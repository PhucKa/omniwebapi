﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IMGAppProxyRepository : INoSqlRepository<MGAppProxy>
    {
        Task<MGAppProxy> GetByAppId(long storeId, long appId);
    }
}
