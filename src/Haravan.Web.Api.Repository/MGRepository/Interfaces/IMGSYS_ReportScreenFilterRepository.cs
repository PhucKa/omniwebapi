﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IMGSYS_ReportScreenFilterRepository : INoSqlRepository<MGSYS_ReportScreenFilter>
    {
        Task<List<MGSYS_ReportScreenFilter>> GetReportScreenFilter(long storeid, long reportid);
    }
}