﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IMGAppInstalledRepository : INoSqlRepository<MGAppInstalled>
    {
        Task RemoveByStoreId(long storeId);

        Task<MGAppInstalled> GetBy(string accessToken);

        Task<MGAppInstalled> GetBy(long appId, long storeId);

        Task<List<MGAppInstalled>> GetBy(long storeId);

        Task<List<long>> GetStoreIdBy(long appId);

        Task<List<MGAppInstalled>> GetBy(long storeId, bool isBasic);

        Task<List<MGAppInstalled>> GetBy(long storeId, bool isBasic, bool isFree);

        Task<MGAppInstalled> GetAppById(long Id);

        Task<List<MGAppInstalled>> GetBy(List<long> appIds);

        Task<MGAppInstalled> GetBy(long appId, long storeId, bool isBasic);

        Task<MGAppInstalled> GetBy(long appId, long storeId, bool isBasic, bool isFree);

        Task UpdateExpiredDateBy(long appId, DateTime expiredDate);

        Task<long[]> GetTop1000(long storeId, int startPosition);
    }
}