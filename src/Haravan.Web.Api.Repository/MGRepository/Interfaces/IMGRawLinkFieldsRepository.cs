﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository.MGRepository
{
    public interface IMGRawLinkFieldsRepository : INoSqlRepository<MGRawLinkFields>
    {
        Task<List<MGRawLinkFields>> GetLinkFields();
    }
}
