﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface ISysStoreRepository : INoSqlRepository<MGSysStore>
    {
        IList<MGSysStore> GetAll();

        MGSysStore GetByStoreId(long storeId, int statusId);

        Task<MGSysStore> GetByStoreId(long storeId);

        void RemoveByStoreId(long storeId);
    }
}