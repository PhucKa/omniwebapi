﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IMGSYS_ReportScreenFilterDataRepository : INoSqlRepository<MGSYS_ReportScreenFilterData>
    {
        Task<List<MGSYS_ReportScreenFilterData>> GetReportScreenFilterData(long storeid, long filterid);

        Task<List<MGSYS_ReportScreenFilterData>> GetReportScreenFilterLstData(long storeid, List<long?> lstfilterid);
    }
}