﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IMGSYS_ReportScreenMeasureRepository : INoSqlRepository<MGSYS_ReportScreenMeasure>
    {
        Task<List<MGSYS_ReportScreenMeasure>> GetReportScreenMeasure(long storeid, long screenid);
    }
}