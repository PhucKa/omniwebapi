﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IMGScriptTagsRepository : INoSqlRepository<MGScriptTagsModel>
    {
        Task<List<MGScriptTagsModel>> GetList(
                long storeId, long appId, int page, int limit, long since_id, DateTime? created_at_min,
                DateTime? created_at_max, DateTime? updated_at_min, DateTime? updated_at_max, string src
            );
        Task<long> Count(
                long storeId, long appId, long since_id, DateTime? created_at_min,
                DateTime? created_at_max, DateTime? updated_at_min, DateTime? updated_at_max, string src
            );
        Task<MGScriptTagsModel> GetDetail(long storeId, long appId, long id);
        Task<long> CountByStoreId(long storeId);
        Task<MGScriptTagsModel> GetBy(string src, string eventName, long storeId, long appId);
        Task<List<MGScriptTagsModel>> GetByAppId(long storeId, long appId);
    }
}
