﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IMGUrlRedirectModelRepository : INoSqlRepository<MGUrlRedirectModel>
    {
        Task<MGUrlRedirectModel> GetByOldPath(long storeId, string oldPath);

        Task<MGUrlRedirectModel> GetByRedirectTo(long storeId, string redirectTo);

        Task<MGUrlRedirectModel> GetByID(long storeId, long Id);

        Task<List<MGUrlRedirectModel>> GetListoldPath(long storeId);

        Task<List<MGUrlRedirectModel>> GetByIdsAsync(long storeId, List<long> ids);

        Task<List<MGUrlRedirectModel>> GetBy(string strPath, bool isCheckOldPath, long storeId);

        Task<List<long>> GetByPagingAsync(long storeId, int currentpage, int pagesize);

        Task DeleteById(long storeId, long Id);

        Task<long> CountAsync(long storeId);
    }
}