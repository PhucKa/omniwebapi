﻿using MongoDB.Driver;

namespace Haravan.Web.Api.Repository
{
    public interface IMongoMasterDb
    {
        IMongoDatabase Db { get; }
    }

    public class MongoMasterDb : IMongoMasterDb
    {
        public MongoMasterDb(IMongoDatabase db)
        {
            Db = db;
        }

        public IMongoDatabase Db { get; private set; }
    }
}
