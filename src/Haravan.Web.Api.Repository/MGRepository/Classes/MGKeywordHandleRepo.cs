﻿using BHN.Core.Repository;
using BHN.SharedObject.EBSMessage;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using MongoDB.Driver;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IMGKeywordHandleRepo : INoSqlRepository<MGKeywordHandle>
    {
        Task<string> ProcessingKeyHandle(long storeid, int typeid, string keywordold, string keywordnew);

        Task<string> ProcessingKeyHandleWithRefId(long storeid, int typeid, string keywordold, string keywordnew, long oldRefId, long newRefId);

        Task Remove(long storeid, int type);

        Task RemoveBySyStoreId(long storeid);
    }

    public class MGKeywordHandleRepo : NoSqlRepository<MGKeywordHandle>, IMGKeywordHandleRepo
    {
        public MGKeywordHandleRepo(IMongoSequenDb db, ISeqIdentity sq)
            : base(db.Db, sq)
        {
        }

        private Task<MGKeywordHandle> GetByKeyword(long storeid, int type, string keyword)
        {
            if (type == (int)EnumDocumentType.Domain)
            {
                var query = Builders<MGKeywordHandle>.Filter.And(
                    Builders<MGKeywordHandle>.Filter.Eq(m => m.keywordout, keyword),
                    Builders<MGKeywordHandle>.Filter.Eq(m => m.type, type)
                    );
                return Collection.Find(query).FirstOrDefaultAsync();
            }
            else
            {
                var query = Builders<MGKeywordHandle>.Filter.And(
                    Builders<MGKeywordHandle>.Filter.Eq(m => m.storeid, storeid),
                Builders<MGKeywordHandle>.Filter.Eq(m => m.keywordout, keyword),
                Builders<MGKeywordHandle>.Filter.Eq(m => m.type, type));
                return Collection.Find(query).FirstOrDefaultAsync();
            }
        }

        private Task<MGKeywordHandle> GetByKeywordWithRefId(long storeid, int type, string keyword, long refId)
        {
            var query = Builders<MGKeywordHandle>.Filter.And(
                Builders<MGKeywordHandle>.Filter.Eq(m => m.storeid, storeid),
            Builders<MGKeywordHandle>.Filter.Eq(m => m.keywordout, keyword),
            Builders<MGKeywordHandle>.Filter.Eq(m => m.refid, refId),
            Builders<MGKeywordHandle>.Filter.Eq(m => m.type, type));
            return Collection.Find(query).FirstOrDefaultAsync();
        }

        private Task DeleteByKeyword(long storeid, int type, string keyword)
        {
            var query = Builders<MGKeywordHandle>.Filter.And(
                Builders<MGKeywordHandle>.Filter.Eq(m => m.storeid, storeid),
                Builders<MGKeywordHandle>.Filter.Eq(m => m.keywordout, keyword),
                Builders<MGKeywordHandle>.Filter.Eq(m => m.type, type)
                );
            return Collection.DeleteManyAsync(query);
        }

        private Task DeleteByKeywordWithRefId(long storeid, int type, string keyword, long refId)
        {
            var query = Builders<MGKeywordHandle>.Filter.And(
                Builders<MGKeywordHandle>.Filter.Eq(m => m.storeid, storeid),
                Builders<MGKeywordHandle>.Filter.Eq(m => m.keywordout, keyword),
                Builders<MGKeywordHandle>.Filter.Eq(m => m.refid, refId),
                Builders<MGKeywordHandle>.Filter.Eq(m => m.type, type)
                );
            return Collection.DeleteManyAsync(query);
        }

        public Task Remove(long storeid, int type)
        {
            var query = Builders<MGKeywordHandle>.Filter.And(
                Builders<MGKeywordHandle>.Filter.Eq(m => m.storeid, storeid),
                Builders<MGKeywordHandle>.Filter.Eq(m => m.type, type)
                );
            return Collection.DeleteManyAsync(query);
        }

        public Task RemoveBySyStoreId(long storeid)
        {
            var query = Builders<MGKeywordHandle>.Filter.And(
                Builders<MGKeywordHandle>.Filter.Eq(m => m.storeid, storeid)
                );
            return Collection.DeleteManyAsync(query);
        }

        private async Task<long> GetAvailableIndexKeyword(long storeid, int type, string basickeyword)
        {
            var query = Builders<MGKeywordHandle>.Filter.And(
                Builders<MGKeywordHandle>.Filter.Eq(m => m.storeid, storeid),
            Builders<MGKeywordHandle>.Filter.Eq(m => m.basickeyword, basickeyword),
            Builders<MGKeywordHandle>.Filter.Eq(m => m.type, type));
            if (type == (int)EnumDocumentType.Domain)
            {
                query = Builders<MGKeywordHandle>.Filter.And(
                    Builders<MGKeywordHandle>.Filter.Eq(m => m.basickeyword, basickeyword),
                    Builders<MGKeywordHandle>.Filter.Eq(m => m.type, type));
            }
            var curObj = await Collection.Find(query).SortByDescending(m => m.index).FirstOrDefaultAsync();
            if (curObj == null || curObj.index == null)
            {
                return 1;
            }
            return (curObj.index.Value + 1);
        }

        private async Task<long> GetAvailableIndexKeywordWithRefId(long storeid, int type, string basickeyword, long refId)
        {
            var query = Builders<MGKeywordHandle>.Filter.And(
                Builders<MGKeywordHandle>.Filter.Eq(m => m.storeid, storeid),
            Builders<MGKeywordHandle>.Filter.Eq(m => m.basickeyword, basickeyword),
             Builders<MGKeywordHandle>.Filter.Eq(m => m.refid, refId),
            Builders<MGKeywordHandle>.Filter.Eq(m => m.type, type));
            var curObj = await Collection.Find(query).SortByDescending(m => m.index).FirstOrDefaultAsync();
            if (curObj == null || curObj.index == null)
            {
                return 1;
            }
            return (curObj.index.Value + 1);
        }

        public async Task<string> ProcessingKeyHandle(long storeid, int typeid, string keywordold, string keywordnew)
        {
            if ((string.IsNullOrEmpty(keywordnew) && string.IsNullOrEmpty(keywordold)) || (keywordold == keywordnew))
            {
                return keywordold;//khong lam gi ca
            }
            if (string.IsNullOrEmpty(keywordnew))
            {
                //delete keywordold
                await DeleteByKeyword(storeid, typeid, keywordold);
                return string.Empty;
            }
            if (keywordnew != keywordold)
            {
                await DeleteByKeyword(storeid, typeid, keywordold);
            }
            string basickeyword = string.Empty;
            long? indexnumber = null;
            // If value of inputHandle is "HANDLE-INDEX", where INDEX is number, then remove INDEX and get only HANDLE.
            if (keywordnew.Contains("-"))
            {
                var lastIndex = keywordnew.Substring(keywordnew.LastIndexOf('-') + 1);
                var num = keywordnew.Length - lastIndex.Length;
                long cur = 0;
                basickeyword = keywordnew.Substring(0, num > 0 ? num - 1 : num); // remove INDEX
                // Check if lastIndex is number. If not, reassign inputHandleUrl value to rawHandle
                if (long.TryParse(lastIndex, out cur) == false)
                {
                    basickeyword = keywordnew;
                    indexnumber = null;
                }
                else
                {
                    indexnumber = cur;
                }
            }
            else
            {
                basickeyword = keywordnew;
            }
            var existRecord = await GetByKeyword(storeid, typeid, keywordnew);
            if (existRecord == null)
            {
                //chua ton tai keyword nay, insert keyword
                existRecord = new MGKeywordHandle()
                {
                    index = indexnumber,
                    basickeyword = basickeyword,
                    keywordout = keywordnew,
                    storeid = storeid,
                    type = typeid
                };
            }
            else
            {
                //da ton tai keyword nay
                long availIndex = await GetAvailableIndexKeyword(storeid, typeid, basickeyword);
                keywordnew = basickeyword + "-" + availIndex.ToString();
                existRecord = new MGKeywordHandle()
                {
                    index = availIndex,
                    basickeyword = basickeyword,
                    keywordout = keywordnew,
                    storeid = storeid,
                    type = typeid
                };
            }

            await base.AddAsync(existRecord);

            return keywordnew;
        }

        public async Task<string> ProcessingKeyHandleWithRefId(long storeid, int typeid, string keywordold, string keywordnew, long oldRefId, long newRefid)
        {
            if ((string.IsNullOrEmpty(keywordnew) && string.IsNullOrEmpty(keywordold)) || ((keywordold == keywordnew) && (oldRefId == newRefid)))
            {
                return keywordold;//khong lam gi ca
            }
            if (string.IsNullOrEmpty(keywordnew))
            {
                //delete keywordold
                await DeleteByKeywordWithRefId(storeid, typeid, keywordold, oldRefId);
                return string.Empty;
            }
            if (keywordnew != keywordold || oldRefId != newRefid)
            {
                await DeleteByKeywordWithRefId(storeid, typeid, keywordold, oldRefId);
            }
            string basickeyword = string.Empty;
            long? indexnumber = null;
            // If value of inputHandle is "HANDLE-INDEX", where INDEX is number, then remove INDEX and get only HANDLE.
            if (keywordnew.Contains("-"))
            {
                var lastIndex = keywordnew.Substring(keywordnew.LastIndexOf('-') + 1);
                var num = keywordnew.Length - lastIndex.Length;
                long cur = 0;
                basickeyword = keywordnew.Substring(0, num > 0 ? num - 1 : num); // remove INDEX
                // Check if lastIndex is number. If not, reassign inputHandleUrl value to rawHandle
                if (long.TryParse(lastIndex, out cur) == false)
                {
                    basickeyword = keywordnew;
                    indexnumber = null;
                }
                else
                {
                    indexnumber = cur;
                }
            }
            else
            {
                basickeyword = keywordnew;
            }
            var existRecord = await GetByKeywordWithRefId(storeid, typeid, keywordnew, newRefid);
            if (existRecord == null)
            {
                //chua ton tai keyword nay, insert keyword
                existRecord = new MGKeywordHandle()
                {
                    index = indexnumber,
                    basickeyword = basickeyword,
                    keywordout = keywordnew,
                    storeid = storeid,
                    type = typeid,
                    refid = newRefid
                };
            }
            else
            {
                //da ton tai keyword nay
                long availIndex = await GetAvailableIndexKeywordWithRefId(storeid, typeid, basickeyword, newRefid);
                keywordnew = basickeyword + "-" + availIndex.ToString();
                existRecord = new MGKeywordHandle()
                {
                    index = availIndex,
                    basickeyword = basickeyword,
                    keywordout = keywordnew,
                    storeid = storeid,
                    type = typeid,
                    refid = newRefid
                };
            }
            await base.AddAsync(existRecord);
            return keywordnew;
        }
    }
}