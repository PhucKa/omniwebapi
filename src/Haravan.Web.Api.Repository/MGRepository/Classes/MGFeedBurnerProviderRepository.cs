﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class MGFeedBurnerProviderRepository : NoSqlRepository<MGFeedBurnerProviderModel>, IMGFeedBurnerProviderRepository
    {
        public MGFeedBurnerProviderRepository(IMongoDatabase db, ISeqIdentity seq)
           : base(db, seq)
        {
        }

        public Task<List<MGFeedBurnerProviderModel>> GetAll()
        {
            var query = Builders<MGFeedBurnerProviderModel>.Filter.And(
               Builders<MGFeedBurnerProviderModel>.Filter.Eq(m => m.status, 1)
               );
            return Collection.Find(query).ToListAsync();
        }
    }
}