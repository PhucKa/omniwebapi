﻿using Haravan.Web.Api.BusinessObjects.MongoModels;
using Haravan.Web.Api.Repository.Infractstructure;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class MGLogDataRepository : MongoBaseRepository<MGLogData, ObjectId>, IMGLogDataRepository
    {
        public MGLogDataRepository(IMongoDatabase db) : base(db)
        {
        }

        public async Task PostLog(MGLogData data)
        {
            await this.AddAsync(data);
        }

        public Task<MGLogData> GetById(long storeId, long refid, long doctypeid)
        {
            var query = Builders<MGLogData>.Filter.And(
                Builders<MGLogData>.Filter.Eq(m => m.storeid, storeId),
                Builders<MGLogData>.Filter.Eq(m => m.refid, refid),
                Builders<MGLogData>.Filter.Eq(m => m.doctypeid, doctypeid));
            return Collection.Find(query).FirstOrDefaultAsync();
        }

        public Task DeleteByIdAsync(long storeId, long refid, long doctypeid)
        {
            var query = Builders<MGLogData>.Filter.And(
                Builders<MGLogData>.Filter.Eq(m => m.storeid, storeId),
                Builders<MGLogData>.Filter.Eq(m => m.refid, refid),
                Builders<MGLogData>.Filter.Eq(m => m.doctypeid, doctypeid));

            return Collection.DeleteOneAsync(query);
        }
    }
}