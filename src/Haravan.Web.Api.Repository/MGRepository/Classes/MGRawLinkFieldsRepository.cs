﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository.MGRepository
{
    public class MGRawLinkFieldsRepository : NoSqlRepository<MGRawLinkFields>, IMGRawLinkFieldsRepository
    {
        public MGRawLinkFieldsRepository(IMongoDatabase db, ISeqIdentity seq)
            : base(db, seq)
        {
        }

        public async Task<List<MGRawLinkFields>> GetLinkFields()
        {
            return await Collection.Find(m => true).ToListAsync();
        }
    }
}