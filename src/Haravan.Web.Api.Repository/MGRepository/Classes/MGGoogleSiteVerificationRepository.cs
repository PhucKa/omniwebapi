﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using MongoDB.Driver;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class MGGoogleSiteVerificationRepository : NoSqlRepository<MGGoogleSiteVerificationModel>, IMGGoogleSiteVerificationRepository
    {
        public MGGoogleSiteVerificationRepository(IMongoDatabase db, ISeqIdentity seq)
          : base(db, seq)
        {
        }
        public async Task DeleteManyAsync(long storeId)
        {
            await Collection.DeleteManyAsync(m => m.storeid == storeId);
        }
    }
}