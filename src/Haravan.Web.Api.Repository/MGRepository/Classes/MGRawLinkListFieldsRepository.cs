﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository.MGRepository
{
    public class MGRawLinkListFieldsRepository : NoSqlRepository<MGRawLinkListFields>, IMGRawLinkListFieldsRepository
    {
        private readonly IServiceProvider serviceProvider;

        public MGRawLinkListFieldsRepository(IMongoDatabase db, ISeqIdentity seq, IServiceProvider serviceProvider)
            : base(db, seq)
        {
            this.serviceProvider = serviceProvider;
        }

        public async Task<MGRawLinkListFields> GetById(long storeId, long id)
        {
            return await Collection.Find(m => m._id == id && m.StoreId == storeId && m.IsDeleted == false).FirstOrDefaultAsync();
        }
        public async Task<List<MGRawLinkListFields>> GetByStoreId(long storeId)
        {
            return await Collection.Find(m => m.StoreId == storeId && m.IsDeleted == false).ToListAsync();
        }
        public async Task<List<MGRawLinkListFields>> GetListByLinkListId(long linkListId, long storeId)
        {
            return await Collection.Find(link => link.IsDeleted == false
                                                && link.LinkListId == linkListId
                                                && link.StoreId == storeId)
                                    .SortBy(link => link.LinkListFieldOrder)
                                    .ToListAsync();
        }

        public async Task<List<MGRawLinkListFields>> GetByLinkListId(long linkListId, long storeId)
        {
            return await Collection.Find(link => link.IsDeleted == false
                                                && link.LinkListId == linkListId
                                                && link.StoreId == storeId)
                                    .SortBy(link => link.LinkListFieldOrder)
                                    .ToListAsync();
        }

        public async Task<List<MGRawLinkListFields>> GetByRefId(long refId, int type, long storeId)
        {
            return await Collection.Find(p => p.IsDeleted == false && p.RefId == refId && p.LinkFieldsId == type && p.StoreId == storeId).ToListAsync();
        }

        public async Task<List<MGRawLinkListFields>> GetAllTreeDropDown_ByParentTreeHandle(string parentTreeHandle, long storeId)
        {
            return await Collection.Find(p => p.StoreId == storeId && p.IsDeleted == false
                                              && p.TreeHandle.Contains(parentTreeHandle))
                                .ToListAsync();
        }

        public async Task<MGRawLinkListFields> GetParentLink_ByDropdown(long dropdownId, string dropdownTreeHandle, long storeId)
        {
            return await Collection.Find(p => p.StoreId == storeId && p.IsDeleted == false
                                                       && p.LinkListId != dropdownId
                                                       && p.TreeHandle.Equals(dropdownTreeHandle)).FirstOrDefaultAsync();
        }

        public async Task<List<MGRawLinkListFields>> GetBy(long refId, int fieldId, long linklistId, long storeId)
        {
            return await Collection.Find(m => m.StoreId == storeId && m.LinkListId == linklistId && m.LinkFieldsId == fieldId && m.RefId == refId && m.IsDeleted == false).ToListAsync();
        }

        public async Task<List<MGRawLinkListFields>> GetAllTreeByParentTreeHandle(string TreeHandle, long storeId)
        {
            return await Collection.Find(p => p.StoreId == storeId && p.IsDeleted == false
                                              && p.TreeHandle.Contains(TreeHandle)).SortBy(m => m.LinkListFieldOrder)
                                .ToListAsync();
        }
    }
}