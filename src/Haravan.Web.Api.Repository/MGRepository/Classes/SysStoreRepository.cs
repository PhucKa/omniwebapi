﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class SysStoreRepository : NoSqlRepository<MGSysStore>, ISysStoreRepository
    {
        public SysStoreRepository(IMongoMasterDb db, ISeqIdentity seq)
            : base(db.Db, seq)
        {
        }

        public IList<MGSysStore> GetAll()
        {
            return Collection.Find(m => true).ToListAsync().Result;
        }

        public MGSysStore GetByStoreId(long storeId, int statusId)
        {
            var query = Builders<MGSysStore>.Filter.And(
            Builders<MGSysStore>.Filter.Eq(m => m._id, storeId),
            Builders<MGSysStore>.Filter.Eq(m => m.Status, statusId));
            return Collection.Find(query).FirstOrDefaultAsync().Result;
        }

        public async Task<MGSysStore> GetByStoreId(long storeId)
        {
            var query = Builders<MGSysStore>.Filter.Eq(m => m._id, storeId);
            return await Collection.Find(query).FirstOrDefaultAsync();
        }

        public void RemoveByStoreId(long storeId)
        {
            var query = Builders<MGSysStore>.Filter.Eq(m => m._id, storeId);
            Collection.DeleteManyAsync(query).Wait();
        }
    }
}