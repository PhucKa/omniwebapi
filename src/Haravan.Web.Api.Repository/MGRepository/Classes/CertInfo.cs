﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class CertInfo : NoSqlRepository<MGCertInfo>, ICertInfo
    {
        public CertInfo(IMongoMasterDb db, ISeqIdentity seq)
            : base(db.Db, seq)
        {
        }       

        public Task<List<MGCertInfo>> GetByIds(List<long> certInfoIds)
        {
            return Collection.Find(m => certInfoIds.Contains(m._id)).ToListAsync();
        }
    }
}