﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class MGScriptTagsRepository : NoSqlRepository<MGScriptTagsModel>, IMGScriptTagsRepository
    {
        public MGScriptTagsRepository(IMongoDatabase db, ISeqIdentity sq)
           : base(db, sq)
        { }


        public Task<List<MGScriptTagsModel>> GetByAppId(long storeId, long appId)
        {
            var query = Builders<MGScriptTagsModel>.Filter.And(
                Builders<MGScriptTagsModel>.Filter.Eq(m => m.storeid, storeId),
                Builders<MGScriptTagsModel>.Filter.Eq(m => m.appid, appId)
                );
            return Collection.Find(query).ToListAsync();
        }

        public Task<MGScriptTagsModel> GetBy(string src, string eventName, long storeId, long appId)
        {
            var query = Builders<MGScriptTagsModel>.Filter.And(
              Builders<MGScriptTagsModel>.Filter.Eq(m => m.storeid, storeId),
              Builders<MGScriptTagsModel>.Filter.Eq(m => m.appid, appId),
              Builders<MGScriptTagsModel>.Filter.Eq(m => m.src, src),
              Builders<MGScriptTagsModel>.Filter.Eq(m => m.evenname, eventName));
            return Collection.Find(query).FirstOrDefaultAsync();
        }

        public Task<long> CountByStoreId(long storeId)
        {
            var query = Builders<MGScriptTagsModel>.Filter.And(
             Builders<MGScriptTagsModel>.Filter.Eq(m => m.storeid, storeId));
            return Collection.CountDocumentsAsync(query);
        }

        public Task<MGScriptTagsModel> GetDetail(long storeId, long appId, long id)
        {
            var query = Builders<MGScriptTagsModel>.Filter.And(
              Builders<MGScriptTagsModel>.Filter.Eq(m => m.storeid, storeId),
              Builders<MGScriptTagsModel>.Filter.Eq(m => m.appid, appId),
              Builders<MGScriptTagsModel>.Filter.Eq(m => m._id, id));
            return Collection.Find(query).FirstOrDefaultAsync();
        }

        public Task<List<MGScriptTagsModel>> GetList(
                long storeId, long appId, int page, int limit, long since_id, DateTime? created_at_min,
                DateTime? created_at_max, DateTime? updated_at_min, DateTime? updated_at_max, string src
            )
        {
            var query = Builders<MGScriptTagsModel>.Filter.And(
              Builders<MGScriptTagsModel>.Filter.Eq(m => m.storeid, storeId),
              Builders<MGScriptTagsModel>.Filter.Eq(m => m.appid, appId));
            if (since_id > 0)
            {
                query = Builders<MGScriptTagsModel>.Filter.And(query,
                    Builders<MGScriptTagsModel>.Filter.Gte(m => m._id, since_id));
            }
            if (created_at_min != null)
            {
                query = Builders<MGScriptTagsModel>.Filter.And(query,
                    Builders<MGScriptTagsModel>.Filter.Gte(m => m.created_at, created_at_min));
            }
            if (created_at_max != null)
            {
                query = Builders<MGScriptTagsModel>.Filter.And(query,
                    Builders<MGScriptTagsModel>.Filter.Lte(m => m.created_at, created_at_max));
            }
            if (updated_at_min != null)
            {
                query = Builders<MGScriptTagsModel>.Filter.And(query,
                    Builders<MGScriptTagsModel>.Filter.Gte(m => m.updated_at, updated_at_min));
            }
            if (updated_at_max != null)
            {
                query = Builders<MGScriptTagsModel>.Filter.And(query,
                    Builders<MGScriptTagsModel>.Filter.Lte(m => m.updated_at, updated_at_max));
            }
            if (!string.IsNullOrWhiteSpace(src))
            {
                query = Builders<MGScriptTagsModel>.Filter.And(query,
                    Builders<MGScriptTagsModel>.Filter.Eq(m => m.src, src));
            }
            return Collection
                            .Find(query)
                            .SortByDescending(m => m._id)
                            .Skip((page - 1) * limit)
                            .Limit(limit)
                            .ToListAsync();
        }

        public Task<long> Count(
                long storeId, long appId, long since_id, DateTime? created_at_min,
                DateTime? created_at_max, DateTime? updated_at_min, DateTime? updated_at_max, string src
            )
        {
            var query = Builders<MGScriptTagsModel>.Filter.And(
              Builders<MGScriptTagsModel>.Filter.Eq(m => m.storeid, storeId),
              Builders<MGScriptTagsModel>.Filter.Eq(m => m.appid, appId));
            if (since_id > 0)
            {
                query = Builders<MGScriptTagsModel>.Filter.And(query,
                    Builders<MGScriptTagsModel>.Filter.Gte(m => m._id, since_id));
            }
            if (created_at_min != null)
            {
                query = Builders<MGScriptTagsModel>.Filter.And(query,
                    Builders<MGScriptTagsModel>.Filter.Gte(m => m.created_at, created_at_min));
            }
            if (created_at_max != null)
            {
                query = Builders<MGScriptTagsModel>.Filter.And(query,
                    Builders<MGScriptTagsModel>.Filter.Lte(m => m.created_at, created_at_max));
            }
            if (updated_at_min != null)
            {
                query = Builders<MGScriptTagsModel>.Filter.And(query,
                    Builders<MGScriptTagsModel>.Filter.Gte(m => m.updated_at, updated_at_min));
            }
            if (updated_at_max != null)
            {
                query = Builders<MGScriptTagsModel>.Filter.And(query,
                    Builders<MGScriptTagsModel>.Filter.Lte(m => m.updated_at, updated_at_max));
            }
            if (!string.IsNullOrWhiteSpace(src))
            {
                query = Builders<MGScriptTagsModel>.Filter.And(query,
                    Builders<MGScriptTagsModel>.Filter.Eq(m => m.src, src));
            }
            return Collection.CountDocumentsAsync(query);
        }
    }
}