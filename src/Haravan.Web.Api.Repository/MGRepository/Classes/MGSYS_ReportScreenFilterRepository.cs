﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class MGSYS_ReportScreenFilterRepository : NoSqlRepository<MGSYS_ReportScreenFilter>, IMGSYS_ReportScreenFilterRepository
    {
        public MGSYS_ReportScreenFilterRepository(IMongoSellerReportDb db, ISeqIdentity seq)
           : base(db.Db, seq)
        {
        }

        public async Task<List<MGSYS_ReportScreenFilter>> GetReportScreenFilter(long storeid, long screenid)
        {
            return await Collection.Find(d => (d.storeid == storeid || d.storeid == 0) && d.reportscreenid == screenid).ToListAsync();
        }
    }
}