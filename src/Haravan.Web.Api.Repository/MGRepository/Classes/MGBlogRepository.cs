﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class MGBlogRepository : NoSqlRepository<MGBlogModel>, IMGBlogRepository
    {
        public MGBlogRepository(IMongoDatabase db, ISeqIdentity seq)
           : base(db, seq)
        {
        }

        public Task DeleteByIdAsync(long storeId, long id)
        {
            var query = Builders<MGBlogModel>.Filter.And(
                Builders<MGBlogModel>.Filter.Eq(m => m.storeid, storeId),
                Builders<MGBlogModel>.Filter.Eq(m => m._id, id));
            var updateQuery = Builders<MGBlogModel>.Update
                                                   .Set(m => m.isdeleted, true);
            return Collection.UpdateOneAsync(query, updateQuery);
        }

        public Task<long> CountAsync(long storeId)
        {
            var query = Builders<MGBlogModel>.Filter.And(
                Builders<MGBlogModel>.Filter.Eq(m => m.storeid, storeId),
                Builders<MGBlogModel>.Filter.Ne(m => m.isdeleted, true)
                );
            return Collection.CountDocumentsAsync(query);
        }

        public Task<MGBlogModel> GetById(long storeId, long id)
        {
            return Collection.Find(m => m._id == id && m.storeid == storeId && !m.isdeleted).FirstOrDefaultAsync();
        }

        public Task<string> GetHandleByIdAsync(long storeId, long id)
        {
            return Collection.Find(m => m._id == id && m.storeid == storeId && !m.isdeleted).Project(m => m.handle).FirstOrDefaultAsync();
        }

        public async Task<List<MGBlogModel>> GetById(long storeId, List<long> ids)
        {
            if (ids == null || !ids.Any())
                return new List<MGBlogModel>();
            return await Collection.Find(m => ids.Contains(m._id) && m.storeid == storeId && !m.isdeleted).ToListAsync();
        }

        public Task<List<long>> GetByPagingAsync(long storeId, int currentpage, int pagesize)
        {
            return Collection.Find(m => m.storeid == storeId && !m.isdeleted)
                             .Project(m => m._id)
                             .SortByDescending(m => m._id)
                             .Skip((currentpage - 1) * pagesize)
                             .Limit(pagesize)
                             .ToListAsync();
        }

        public Task<List<MGBlogModel>> GetBlogsByStoreId(long storeId)
        {
            return Collection.Find(m => m.storeid == storeId && !m.isdeleted)
                             .ToListAsync();
        }

        public Task<MGBlogModel> GetByHandleUrl(string handleUrl, long storeId)
        {
            return Collection.Find(m => m.storeid == storeId && m.handle == handleUrl && !m.isdeleted)
                             .FirstOrDefaultAsync();
        }

        public Task<long> GetByUrlHandle(string handleUrl, long storeId)
        {
            return Collection.Find(m => m.storeid == storeId && m.handle == handleUrl && !m.isdeleted)
                             .Project(m => m._id)
                             .FirstOrDefaultAsync();
        }
        public Task<MGBlogModel> GetFirstByHandle(long storeId)
        {
            return Collection.Find(m => m.storeid == storeId && !m.isdeleted).SortByDescending(m => m.created_at).FirstOrDefaultAsync();
        }

        public async Task<List<MGBlogModel>> GetByListUrlHandle(long storeId, List<string> listUrlHandle)
        {
            if (listUrlHandle == null || !listUrlHandle.Any())
                return new List<MGBlogModel>();
            return await Collection.Find(m => m.storeid == storeId && listUrlHandle.Contains(m.handle) && !m.isdeleted).ToListAsync();
        }
    }
}