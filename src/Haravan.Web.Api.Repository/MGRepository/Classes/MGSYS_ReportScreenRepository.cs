﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class MGSYS_ReportScreenRepository : NoSqlRepository<MGSYS_ReportScreen>, IMGSYS_ReportScreenRepository
    {
        public MGSYS_ReportScreenRepository(IMongoSellerReportDb db, ISeqIdentity seq)
           : base(db.Db, seq)
        {
        }

        public async Task<List<MGSYS_ReportScreen>> GetReportScreenByStoreId(long storeid)
        {
            return await Collection.Find(d => d.storeid == storeid || d.storeid == 0
                                              && d.isaddnewreport == false)
                                  .ToListAsync();
        }

        public async Task<MGSYS_ReportScreen> GetReportScreenById(long storeid, long id)
        {
            return await Collection.Find(d => (d.storeid == storeid || d.storeid == 0) && d._id == id).FirstOrDefaultAsync();
        }

        public async Task<MGSYS_ReportScreen> GetReportScreenById(long storeid, string nameReport)
        {
            return await Collection.Find(d => (d.storeid == storeid || d.storeid == 0) && d.reportnamesystem.Equals(nameReport)).FirstOrDefaultAsync();
        }
    }
}