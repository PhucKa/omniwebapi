﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class SysDomainRepository : NoSqlRepository<MGSysDomain>, ISysDomainRepository
    {
        public SysDomainRepository(IMongoMasterDb db, ISeqIdentity seq)
            : base(db.Db, seq)
        {
        }

        public Task<List<MGSysDomain>> GetSubDomainByStoreId(List<long> listStoreId)
        {
            var query = Builders<MGSysDomain>.Filter.And(
                Builders<MGSysDomain>.Filter.In(m => m.StoreId, listStoreId),
                Builders<MGSysDomain>.Filter.Eq(m => m.IsSubDomain, true));
            return Collection.Find(query).ToListAsync();
        }

        public Task<MGSysDomain> GetSubDomainByStoreId(long storeId)
        {
            var query = Builders<MGSysDomain>.Filter.And(
                            Builders<MGSysDomain>.Filter.Eq(m => m.StoreId, storeId),
                            Builders<MGSysDomain>.Filter.Eq(m => m.IsSubDomain, true));
            return Collection.Find(query).FirstOrDefaultAsync();
        }

        public Task RemoveByStoreId(long storeId)
        {
            var query = Builders<MGSysDomain>.Filter.Eq(m => m.StoreId, storeId);
            return Collection.DeleteManyAsync(query);
        }

        public Task DeleteByIdAsync(long storeId, long id)
        {
            var query = Builders<MGSysDomain>.Filter.And(
                Builders<MGSysDomain>.Filter.Eq(m => m.StoreId, storeId),
                Builders<MGSysDomain>.Filter.Eq(m => m._id, id));

            return Collection.DeleteOneAsync(query);
        }

        public async Task<bool> CheckExistSubDomain(string subDomain)
        {
            var query = Builders<MGSysDomain>.Filter.And(
                      Builders<MGSysDomain>.Filter.Eq(m => m.Name, subDomain),
                      Builders<MGSysDomain>.Filter.Eq(m => m.IsSubDomain, true));
            return (await Collection.Find(query).FirstOrDefaultAsync()) != null;
        }


        public Task<MGSysDomain> GetByDomainName(string domainName)
        {
            return Collection.Find(m => m.Name == domainName).FirstOrDefaultAsync();
        }

        public async Task<bool> CheckExistDomain(string Domain)
        {
            var query = Builders<MGSysDomain>.Filter.Eq(a => a.Name, Domain);
            return (await Collection.Find(query).FirstOrDefaultAsync()) != null;
        }

        public async Task<bool> CheckExistDomain(string Domain, long storeId)
        {
            var query = Builders<MGSysDomain>.Filter.And(
                    Builders<MGSysDomain>.Filter.Eq(m => m.Name, Domain),
                    Builders<MGSysDomain>.Filter.Eq(m => m.StoreId, storeId));
            return (await Collection.Find(query).FirstOrDefaultAsync()) != null;
        }

        public Task<List<MGSysDomain>> GetListDomainByStore(long storeId)
        {
            var query = Builders<MGSysDomain>.Filter.Eq(m => m.StoreId, storeId);
            return Collection.Find(query).Limit(200).ToListAsync();
        }

        public Task<MGSysDomain> GetStorePrimaryDomain(long storeId)
        {
            var query = Builders<MGSysDomain>.Filter.And(
                Builders<MGSysDomain>.Filter.Eq(m => m.StoreId, storeId),
                Builders<MGSysDomain>.Filter.Eq(m => m.IsPrimary, true));
            return Collection.Find(query).FirstOrDefaultAsync();
        }

        public async Task<long?> GetStoreIdBySubDomain(string subDomain)
        {
            var query = Builders<MGSysDomain>.Filter.And(
                Builders<MGSysDomain>.Filter.Eq(m => m.Name, subDomain),
                Builders<MGSysDomain>.Filter.Eq(m => m.IsSubDomain, true));

            var ob = await Collection.Find(query).FirstOrDefaultAsync();

            return ob?.StoreId;
        }

        public Task<MGSysDomain> GetBySubDomain(string subDomain)
        {
            var query = Builders<MGSysDomain>.Filter.And(
                    Builders<MGSysDomain>.Filter.Eq(m => m.Name, subDomain),
                    Builders<MGSysDomain>.Filter.Eq(m => m.IsSubDomain, true));
            return Collection.Find(query).FirstOrDefaultAsync();
        }

        public async Task<MGSysDomain> GetSubDomainByDomain(string domain)
        {
            var query = Builders<MGSysDomain>.Filter.And(
                    Builders<MGSysDomain>.Filter.Eq(m => m.Name, domain),
                    Builders<MGSysDomain>.Filter.Eq(m => m.IsSubDomain, false));
            var dm = await Collection.Find(query).FirstOrDefaultAsync();

            if (dm == null)
                return null;

            var query2 = Builders<MGSysDomain>.Filter.And(
                    Builders<MGSysDomain>.Filter.Eq(m => m.StoreId, dm.StoreId),
                    Builders<MGSysDomain>.Filter.Eq(m => m.IsSubDomain, true));

            return await Collection.Find(query2).FirstOrDefaultAsync();
        }

        public Task<MGSysDomain> GetByName(string domain)
        {
            var query = Builders<MGSysDomain>.Filter.Eq(m => m.Name, domain);
            return Collection.Find(query).FirstOrDefaultAsync();
        }

        public Task<MGSysDomain> GetById(long domainId)
        {
            var query = Builders<MGSysDomain>.Filter.Eq(m => m._id, domainId);
            return Collection.Find(query).FirstOrDefaultAsync();
        }

        public Task<List<MGSysDomain>> GetPrimaryDomainByStoreIds(List<long> storeIds)
        {
            var query = Builders<MGSysDomain>.Filter.And(
                Builders<MGSysDomain>.Filter.Eq(p => p.IsPrimary, true),
                Builders<MGSysDomain>.Filter.In(p => p.StoreId, storeIds));
            return Collection.Find(query).ToListAsync();
        }

        public Task<MGSysDomain> GetByDomainId(long domainId, long storeId)
        {
            var query2 = Builders<MGSysDomain>.Filter.And(
                     Builders<MGSysDomain>.Filter.Eq(m => m.StoreId, storeId),
                     Builders<MGSysDomain>.Filter.Eq(m => m._id, domainId));
            return Collection.Find(query2).FirstOrDefaultAsync();
        }

        public Task<MGSysDomain> GetByDomainName(long storeId, string domainName)
        {
            var query = Builders<MGSysDomain>.Filter.And(
                     Builders<MGSysDomain>.Filter.Eq(m => m.StoreId, storeId),
                     Builders<MGSysDomain>.Filter.Eq(m => m.Name, domainName));
            return Collection.Find(query).FirstOrDefaultAsync();
        }
    }
}