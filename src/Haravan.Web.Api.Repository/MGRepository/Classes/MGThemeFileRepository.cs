﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using MongoDB.Driver;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class MGThemeFileRepository : NoSqlRepository<MGThemeFileModel>, IMGThemeFileRepository
    {
        public MGThemeFileRepository(IMongoDatabase db, ISeqIdentity seq)
           : base(db, seq)
        {
        }

        public Task<MGThemeFileModel> GetByFileNameAsync(long storeId, long themeId, string fileName)
        {
            var query = Builders<MGThemeFileModel>.Filter.And(
                Builders<MGThemeFileModel>.Filter.Eq(m => m.storeid, storeId),
                Builders<MGThemeFileModel>.Filter.Eq(m => m.themeid, themeId),
                Builders<MGThemeFileModel>.Filter.Eq(m => m.filename, fileName)
                );
            return Collection.Find(query).FirstOrDefaultAsync();
        }
    }
}