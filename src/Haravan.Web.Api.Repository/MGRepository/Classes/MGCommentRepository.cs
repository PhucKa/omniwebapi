﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class MGCommentRepository : NoSqlRepository<MGCommentModel>, IMGCommentRepository
    {
        public MGCommentRepository(IMongoDatabase db, ISeqIdentity seq)
            : base(db, seq)
        {
        }

        public Task<List<long>> GetByPagingAsync(long storeId, int currentpage, int pagesize)
        {
            return Collection.Find(m => m.storeid == storeId && !m.isdeleted)
                             .Project(m => m._id).SortByDescending(m => m._id)
                             .Skip((currentpage - 1) * pagesize)
                             .Limit(pagesize)
                             .ToListAsync();
        }

        public Task<long> CountAllCommentsByStoreId(long storeid)
        {
            var query = Builders<MGCommentModel>.Filter.And(
                Builders<MGCommentModel>.Filter.Eq(m => m.storeid, storeid),
                Builders<MGCommentModel>.Filter.Ne(m => m.isdeleted, true));

            return Collection.CountDocumentsAsync(query);
        }

        

        public async Task<List<MGCommentModel>> GetBy(long[] ids, long storeId)
        {
            if (ids == null || !ids.Any()) return new List<MGCommentModel>();
            return await Collection.Find(m => ids.Contains(m._id) && m.storeid == storeId && !m.isdeleted)
                                   .ToListAsync();
        }

        public Task<MGCommentModel> GetDetailById(long id, long storeId, bool isDeleted)
        {
            return Collection.Find(m => m._id == id && m.storeid == storeId).FirstOrDefaultAsync();
        }

        public async Task<List<MGCommentModel>> GetByIdsAsync(long storeId, List<long> ids, bool isDeleted)
        {
            if (ids == null || !ids.Any()) return new List<MGCommentModel>();
            return await Collection.Find(m => ids.Contains(m._id) && m.storeid == storeId && !m.isdeleted)
                                   .ToListAsync();
        }
        public Task<MGCommentModel> GetDetailById(long id, long storeId)
        {
            return Collection.Find(m => m._id == id && m.storeid == storeId && !m.isdeleted).FirstOrDefaultAsync();
        }

        public Task<List<MGCommentModel>> GetByArticleId(long articleId, long storeId)
        {
            return Collection.Find(m => m.articleid == articleId && m.storeid == storeId && !m.isdeleted).ToListAsync();
        }

        public Task DeleteByIdAsync(long storeId, long id)
        {
            var query = Builders<MGCommentModel>.Filter.And(
                Builders<MGCommentModel>.Filter.Eq(m => m.storeid, storeId),
                Builders<MGCommentModel>.Filter.Eq(m => m._id, id));
            var updateQuery = Builders<MGCommentModel>.Update
                                                      .Set(m => m.isdeleted, true);
            return Collection.UpdateOneAsync(query, updateQuery);
        }

        public Task DeleteByIdArticleAsync(long storeId, long articleId)
        {
            var query = Builders<MGCommentModel>.Filter.And(
                Builders<MGCommentModel>.Filter.Eq(m => m.storeid, storeId),
                Builders<MGCommentModel>.Filter.Eq(m => m.articleid, articleId));
            var updateQuery = Builders<MGCommentModel>.Update
                                                      .Set(m => m.isdeleted, true);
            return Collection.UpdateManyAsync(query, updateQuery);
        }

        public Task<List<MGCommentModel>> GetCommentsByArticleIds(List<long> articleIds, long storeId, int status = -1)
        {
            if (status == -1)
            {
                return Collection.Find(m => articleIds.Contains(m.articleid) && m.storeid == storeId && !m.isdeleted).ToListAsync();
            }
            else
            {
                return Collection.Find(m => articleIds.Contains(m.articleid) && m.storeid == storeId && m.status == status && !m.isdeleted)
                                 .ToListAsync();
            }
        }
    }
}