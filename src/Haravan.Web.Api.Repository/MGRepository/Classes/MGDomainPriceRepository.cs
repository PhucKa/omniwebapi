﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class MGDomainPriceRepository : NoSqlRepository<MGDomainPrice>, IMGDomainPriceRepository
    {
        public MGDomainPriceRepository(IMongoMasterDb db, ISeqIdentity seq)
            : base(db.Db, seq)
        {
        }

        public Task<MGDomainPrice> GetBy(string domainExtension)
        {
            var query = Builders<MGDomainPrice>.Filter.Eq(m => m.Extension, domainExtension);
            return Collection.Find(query).FirstOrDefaultAsync();
        }

        public Task<List<MGDomainPrice>> GetAll()
        {
            return Collection.Find(m => true).SortByDescending(m => m.OrderNumber).ToListAsync();
        }
    }
}