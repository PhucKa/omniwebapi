﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class MGSummaryTotalRepository : NoSqlRepository<MGSummaryTotalModel>, IMGSummaryTotalRepository
    {
        public MGSummaryTotalRepository(IMongoDatabase db, ISeqIdentity sq)
            : base(db, sq)
        { }

        // aVinh rebuild
        public Task<List<string>> GetSummaryTotalNames(int Type, long StoreId)
        {
            var query = Builders<MGSummaryTotalModel>.Filter.And(
                 Builders<MGSummaryTotalModel>.Filter.Eq(m => m.storeid, StoreId),
                 Builders<MGSummaryTotalModel>.Filter.Eq(m => m.type, (int)Type)
                );
            return Collection.Find(query).Project(t => t.name).ToListAsync();
        }

        // aVinh rebuild
        public Task<List<MGSummaryTotalModel>> GetSummaryTotals(int Type, long StoreId)
        {
            var query = Builders<MGSummaryTotalModel>.Filter.And(
                 Builders<MGSummaryTotalModel>.Filter.Eq(m => m.storeid, StoreId),
                 Builders<MGSummaryTotalModel>.Filter.Eq(m => m.type, (int)Type)
                );
            return Collection.Find(query).ToListAsync();
        }
    }
}