﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using MongoDB.Driver;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository.MGRepository
{
    public class MGLinkListRepository : NoSqlRepository<MGLinkListModel>, IMGLinkListRepository
    {
        public MGLinkListRepository(IMongoDatabase db)
            : base(db)
        {
        }

        public Task<MGLinkListModel> GetById(long storeId, long id)
        {
            var query = Builders<MGLinkListModel>.Filter.And(
                Builders<MGLinkListModel>.Filter.Eq(m => m.storeid, storeId),
                Builders<MGLinkListModel>.Filter.Eq(m => m._id, id));

            return Collection.Find(query).FirstOrDefaultAsync();
        }

        public Task DeleteAsync(long storeId, long id)
        {
            var query = Builders<MGLinkListModel>.Filter.And(
                Builders<MGLinkListModel>.Filter.Eq(m => m.storeid, storeId),
                Builders<MGLinkListModel>.Filter.Eq(m => m._id, id));

            return Collection.DeleteOneAsync(query);
        }
    }
}