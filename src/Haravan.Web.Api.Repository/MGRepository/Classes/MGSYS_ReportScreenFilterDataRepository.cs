﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class MGSYS_ReportScreenFilterDataRepository : NoSqlRepository<MGSYS_ReportScreenFilterData>, IMGSYS_ReportScreenFilterDataRepository
    {
        public MGSYS_ReportScreenFilterDataRepository(IMongoSellerReportDb db, ISeqIdentity seq)
           : base(db.Db, seq)
        {
        }

        public async Task<List<MGSYS_ReportScreenFilterData>> GetReportScreenFilterData(long storeid, long filterid)
        {
            var ret = Collection.Find(d => (d.storeid == storeid || d.storeid == 0) && d.filterid == filterid);
            if (ret != null)
            {
                return await ret.ToListAsync();
            }
            return null;
        }

        public Task<List<MGSYS_ReportScreenFilterData>> GetReportScreenFilterLstData(long storeid, List<long?> lstfilterid)
        {
            return Collection.Find(d => (d.storeid == storeid || d.storeid == 0) && lstfilterid.Contains(d.filterid))
                .ToListAsync();
        }
    }
}