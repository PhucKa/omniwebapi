﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository.MGRepository
{
    public class MGRawLinkListRepository : NoSqlRepository<MGRawLinkList>, IMGRawLinkListRepository
    {
        public MGRawLinkListRepository(IMongoDatabase db, ISeqIdentity seq)
            : base(db, seq)
        {
        }

        public async Task<List<MGRawLinkList>> GetAllLinkList(long storeId)
        {
            return await Collection.Find(linkList => linkList.IsDeleted == false && linkList.StoreId == storeId).ToListAsync();
        }

        public async Task<List<MGRawLinkList>> GetAllLinkListByHandle(string handle, long storeId)
        {
            return await Collection.Find(linkList => linkList.IsDeleted == false && linkList.StoreId == storeId && linkList.LinkListHandle.Contains(handle)).ToListAsync();
        }

        public async Task<MGRawLinkList> GetByHandleUrl(string handleUrl, long storeId)
        {
            return await Collection.Find(p => p.IsDeleted == false && p.StoreId == storeId && p.LinkListHandle == handleUrl).FirstOrDefaultAsync();
        }

        public async Task<MGRawLinkList> GetLinkListByTreeHandle(string treehandle, long storeId)
        {
            return await Collection.Find(p => p.StoreId == storeId && p.IsDeleted == false
                && p.TreeHandle.Equals(treehandle)).FirstOrDefaultAsync();
        }

        public async Task<List<MGRawLinkList>> GetAlLinkListContainsTreeHandle(string treehandle, long storeId)
        {
            return await Collection.Find(p => p.StoreId == storeId && p.IsDeleted == false
                                              && p.TreeHandle.Contains(treehandle))
                                .ToListAsync();
        }

        public async Task<bool> CheckExistLinkListByHandle(string handle, long storeId)
        {
            var ret = await Collection.Find(
                    p => p.IsDeleted == false && p.StoreId == storeId && p.LinkListHandle == handle).FirstOrDefaultAsync();
            if (ret != null) return true;
            return false;
        }

        public async Task<List<MGRawLinkList>> GetByIds(List<long> ids, long storeId)
        {
            var query = Builders<MGRawLinkList>.Filter.And(
                Builders<MGRawLinkList>.Filter.Eq(m => m.StoreId, storeId),
                Builders<MGRawLinkList>.Filter.Eq(m => m.IsDeleted, false),
                Builders<MGRawLinkList>.Filter.In(m => m._id, ids)
                );
            return await Collection.Find(query).ToListAsync();
        }

        public async Task<MGRawLinkList> GetById(long storeId, long id)
        {
            return await Collection.Find(p => p._id == id && p.StoreId == storeId).FirstOrDefaultAsync();
        }
        public async Task<List<MGRawLinkList>> GetByStoreId(long storeId)
        {
            return await Collection.Find(p => p.StoreId == storeId && !p.IsDeleted).ToListAsync();
        }

        public async Task DeleteByIdAsync(long storeId, long id)
        {
            await Collection.DeleteOneAsync(p => p._id == id && p.StoreId == storeId);
        }
    }
}