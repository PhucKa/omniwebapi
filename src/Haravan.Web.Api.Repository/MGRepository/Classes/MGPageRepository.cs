﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class MGPageRepository : NoSqlRepository<MGPageModel>, IMGPageRepository
    {
        public MGPageRepository(IMongoDatabase db, ISeqIdentity seq)
           : base(db, seq)
        {
        }

        public Task<MGPageModel> GetById(long storeId, long id)
        {
            return Collection.Find(m => m._id == id && m.storeid == storeId && !m.isdeleted).FirstOrDefaultAsync();
        }

        public Task<string> GetHandleByIdAsync(long storeId, long id)
        {
            return Collection.Find(m => m._id == id && m.storeid == storeId && !m.isdeleted).Project(m => m.handle).FirstOrDefaultAsync();
        }

        public Task<MGPageModel> GetByHandleUrl(string handleUrl, long storeId)
        {
            return Collection.Find(m => m.storeid == storeId && m.handle == handleUrl && !m.isdeleted).FirstOrDefaultAsync();
        }

        public Task<long> GetIdByUrlHandle(string handleUrl, long storeId)
        {
            return Collection.Find(m => m.storeid == storeId && m.handle == handleUrl && !m.isdeleted).Project(m => m._id).FirstOrDefaultAsync();
        }

        public async Task<List<MGPageModel>> GetByListUrlHandle(long storeId, List<string> listUrlHandle)
        {
            if (listUrlHandle == null || !listUrlHandle.Any()) return new List<MGPageModel>();
            return await Collection.Find(m => m.storeid == storeId && listUrlHandle.Contains(m.handle) && !m.isdeleted).ToListAsync();
        }

        public async Task<List<MGPageModel>> GetById(long storeId, List<long> ids)
        {
            if (ids == null || !ids.Any()) return new List<MGPageModel>();
            return await Collection.Find(m => ids.Contains(m._id) && m.storeid == storeId && !m.isdeleted).ToListAsync();
        }

        public async Task<List<MGPageModel>> GetById(long storeId, Dictionary<int, long> lids)
        {
            var ids = lids.Select(m => m.Value).ToList();
            if (ids == null || !ids.Any()) return new List<MGPageModel>();
            return await Collection.Find(m => ids.Contains(m._id) && m.storeid == storeId && !m.isdeleted).ToListAsync();
        }

        public Task DeleteByIdAsync(long storeId, long id)
        {
            var query = Builders<MGPageModel>.Filter.And(
                Builders<MGPageModel>.Filter.Eq(m => m.storeid, storeId),
                Builders<MGPageModel>.Filter.Eq(m => m._id, id));
            var updateQuery = Builders<MGPageModel>.Update
                                                   .Set(m => m.isdeleted, true);
            return Collection.UpdateOneAsync(query, updateQuery);
        }

        public Task<List<long>> GetByPagingAsync(long storeId, int currentpage, int pagesize)
        {
            return Collection.Find(m => m.storeid == storeId && !m.isdeleted)
                             .Project(m => m._id)
                             .SortByDescending(m => m._id)
                             .Skip((currentpage - 1) * pagesize)
                             .Limit(pagesize)
                             .ToListAsync();
        }

        public Task<long> CountAsync(long storeId)
        {
            var query = Builders<MGPageModel>.Filter.And(
                Builders<MGPageModel>.Filter.Eq(m => m.storeid, storeId),
                Builders<MGPageModel>.Filter.Ne(m => m.isdeleted, true));

            return Collection.CountDocumentsAsync(query);
        }

        public Task<List<MGPageModel>> GetByStoreId(long storeId)
        {
            return Collection.Find(m => m.storeid == storeId && !m.isdeleted).ToListAsync();
        }

        public Task<MGPageModel> GetByUrlHandle(long storeId, string urlHandle)
        {
            return Collection.Find(m => m.storeid == storeId && m.handle == urlHandle && !m.isdeleted).FirstOrDefaultAsync();
        }

        public async Task<List<MGPageModel>> GetListByListHandleUrl(List<string> listUrlHandle, long storeId)
        {
            if (listUrlHandle == null || !listUrlHandle.Any()) return new List<MGPageModel>();
            return await Collection.Find(m => m.storeid == storeId && listUrlHandle.Contains(m.handle) && !m.isdeleted).ToListAsync();
        }
    }
}