﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class MGAppInstalledRepository : NoSqlRepository<MGAppInstalled>, IMGAppInstalledRepository
    {
        public MGAppInstalledRepository(IMongoMasterDb db, ISeqIdentity seq)
            : base(db.Db, seq)
        {
        }

        public async Task<MGAppInstalled> GetBy(string accessToken)
        {
            var query = Builders<MGAppInstalled>.Filter.Eq(a => a.AccessToken, accessToken);
            return await Collection.Find(query).FirstOrDefaultAsync();
        }

        public async Task<MGAppInstalled> GetBy(long appId, long storeId)
        {
            var query = Builders<MGAppInstalled>.Filter.And(
                Builders<MGAppInstalled>.Filter.Eq(a => a.AppId, appId),
                Builders<MGAppInstalled>.Filter.Eq(a => a.StoreId, storeId));
            return await Collection.Find(query).FirstOrDefaultAsync();
        }

        public async Task<MGAppInstalled> GetAppById(long Id)
        {
            var query = Builders<MGAppInstalled>.Filter.Eq(m => m._id, Id);
            return await Collection.Find(query).FirstOrDefaultAsync();
        }

        public async Task<List<MGAppInstalled>> GetBy(List<long> appIds)
        {
            var query = Builders<MGAppInstalled>.Filter.In(m => m.AppId, appIds);
            return await Collection.Find(query).ToListAsync();
        }

        public async Task<List<MGAppInstalled>> GetBy(long storeId)
        {
            var query = Builders<MGAppInstalled>.Filter.Eq(m => m.StoreId, storeId);
            return await Collection.Find(query).ToListAsync();
        }

        public async Task<List<long>> GetStoreIdBy(long appId)
        {
            var query = Builders<MGAppInstalled>.Filter.Eq(a => a.AppId, appId);
            return await Collection.Find(query).Project(a => a.StoreId).ToListAsync();
        }

        public async Task<List<MGAppInstalled>> GetBy(long storeId, bool isBasic)
        {
            var query = Builders<MGAppInstalled>.Filter.And(
                        Builders<MGAppInstalled>.Filter.Eq(a => a.StoreId, storeId),
                        Builders<MGAppInstalled>.Filter.Eq(a => a.IsBasic, isBasic)
                );
            return await Collection.Find(query).ToListAsync();
        }

        public async Task RemoveByStoreId(long storeId)
        {
            var query = Builders<MGAppInstalled>.Filter.And(
                        Builders<MGAppInstalled>.Filter.Eq(a => a.StoreId, storeId)

                );
            await Collection.DeleteManyAsync(query);
        }

        public async Task<List<MGAppInstalled>> GetBy(long storeId, bool isBasic, bool isFree)
        {
            if (isFree)
            {
                var query = Builders<MGAppInstalled>.Filter.And(
                    Builders<MGAppInstalled>.Filter.Eq(a => a.StoreId, storeId),
                    Builders<MGAppInstalled>.Filter.Eq(a => a.IsBasic, isBasic),
                    Builders<MGAppInstalled>.Filter.Eq(a => a.AppExpiredDate, null)
                    );
                return await Collection.Find(query).ToListAsync();
            }
            else
            {
                var query = Builders<MGAppInstalled>.Filter.And(
                    Builders<MGAppInstalled>.Filter.Eq(a => a.StoreId, storeId),
                    Builders<MGAppInstalled>.Filter.Eq(a => a.IsBasic, isBasic),
                    Builders<MGAppInstalled>.Filter.Ne(a => a.AppExpiredDate, null)
                    );
                return await Collection.Find(query).ToListAsync();
            }
        }

        public async Task<MGAppInstalled> GetBy(long appId, long storeId, bool isBasic)
        {
            var query = Builders<MGAppInstalled>.Filter.And(
                Builders<MGAppInstalled>.Filter.Eq(a => a.AppId, appId),
                Builders<MGAppInstalled>.Filter.Eq(a => a.StoreId, storeId),
                Builders<MGAppInstalled>.Filter.Eq(a => a.IsBasic, isBasic));

            return await Collection.Find(query).FirstOrDefaultAsync();
        }

        public async Task<MGAppInstalled> GetBy(long appId, long storeId, bool isBasic, bool isFree)
        {
            if (isFree)
            {
                var query = Builders<MGAppInstalled>.Filter.And(
                    Builders<MGAppInstalled>.Filter.Eq(a => a.AppId, appId),
                    Builders<MGAppInstalled>.Filter.Eq(a => a.StoreId, storeId),
                    Builders<MGAppInstalled>.Filter.Eq(a => a.IsBasic, isBasic),
                    Builders<MGAppInstalled>.Filter.Eq(a => a.AppExpiredDate, null));
                return await Collection.Find(query).FirstOrDefaultAsync();
            }
            else
            {
                var query = Builders<MGAppInstalled>.Filter.And(
                    Builders<MGAppInstalled>.Filter.Eq(a => a.AppId, appId),
                    Builders<MGAppInstalled>.Filter.Eq(a => a.StoreId, storeId),
                    Builders<MGAppInstalled>.Filter.Eq(a => a.IsBasic, isBasic),
                    Builders<MGAppInstalled>.Filter.Ne(a => a.AppExpiredDate, null));
                return await Collection.Find(query).FirstOrDefaultAsync();
            }
        }

        public async Task UpdateExpiredDateBy(long appId, DateTime expiredDate)
        {
            var query = Builders<MGAppInstalled>.Filter.And(
                Builders<MGAppInstalled>.Filter.Eq(a => a.AppId, appId));

            var update = Builders<MGAppInstalled>.Update
                .Set(a => a.AppExpiredDate, expiredDate);
            await Collection.UpdateManyAsync(query, update);
        }

        public async Task<long[]> GetTop1000(long storeId, int startPosition)
        {
            return (await Collection.Find(m => true).SortByDescending(m => m._id)
                .Project(m => m._id).Skip(startPosition).Limit(1000).ToListAsync()).ToArray();
        }
    }
}