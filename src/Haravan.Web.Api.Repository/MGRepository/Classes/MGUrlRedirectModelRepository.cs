﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class MGUrlRedirectModelRepository : NoSqlRepository<MGUrlRedirectModel>, IMGUrlRedirectModelRepository
    {
        public MGUrlRedirectModelRepository(IMongoDatabase db, ISeqIdentity seq)
            : base(db, seq)
        {
        }

        public Task<List<MGUrlRedirectModel>> GetBy(string strPath, bool isCheckOldPath, long storeId)
        {
            if (isCheckOldPath)
            {
                return Collection.Find(n => n.storeid == storeId
                        && !n.isdeleted
                        && n.oldpath.Equals(strPath)).ToListAsync();
            }
            else
            {
                return Collection.Find(n => n.storeid == storeId
                       && !n.isdeleted
                       && n.redirectto.Equals(strPath)).ToListAsync();
            }
        }

        public Task<MGUrlRedirectModel> GetByOldPath(long storeId, string oldPath)
        {
            return Collection.Find(n => n.storeid == storeId
                         && !n.isdeleted
                         && n.oldpath.Equals(oldPath)).FirstOrDefaultAsync();
        }

        public Task<MGUrlRedirectModel> GetByRedirectTo(long storeId, string redirectTo)
        {
            return Collection.Find(n => n.storeid == storeId
                        && !n.isdeleted
                        && n.redirectto.Equals(redirectTo)).FirstOrDefaultAsync();
        }

        public Task<MGUrlRedirectModel> GetByID(long storeId, long Id)
        {
            return Collection.Find(m => m._id == Id && m.storeid == storeId && !m.isdeleted).FirstOrDefaultAsync();
        }

        public async Task<List<MGUrlRedirectModel>> GetByIdsAsync(long storeId, List<long> ids)
        {
            if (ids == null || !ids.Any()) return new List<MGUrlRedirectModel>();
            return await Collection.Find(m => ids.Contains(m._id) && m.storeid == storeId && !m.isdeleted).ToListAsync();
        }

        public Task<List<MGUrlRedirectModel>> GetListoldPath(long storeId)
        {
            return Collection.Find(m => m.storeid == storeId && !m.isdeleted).ToListAsync();
        }

        public Task<List<long>> GetByPagingAsync(long storeId, int currentpage, int pagesize)
        {
            return Collection.Find(m => m.storeid == storeId && !m.isdeleted)
                             .Project(m => m._id)
                             .SortByDescending(m => m._id)
                             .Skip((currentpage - 1) * pagesize)
                             .Limit(pagesize)
                             .ToListAsync();
        }

        public Task<long> CountAsync(long storeId)
        {
            var query = Builders<MGUrlRedirectModel>.Filter.And(
                Builders<MGUrlRedirectModel>.Filter.Eq(m => m.storeid, storeId),
                Builders<MGUrlRedirectModel>.Filter.Ne(m => m.isdeleted, true));

            return Collection.CountDocumentsAsync(query);
        }

        public Task DeleteById(long storeId, long Id)
        {
            var query = Builders<MGUrlRedirectModel>.Filter.And(
                Builders<MGUrlRedirectModel>.Filter.Eq(m => m.storeid, storeId),
                Builders<MGUrlRedirectModel>.Filter.Eq(m => m._id, Id));
            var updateQuery = Builders<MGUrlRedirectModel>.Update
                                                          .Set(m => m.isdeleted, true);
            return Collection.UpdateOneAsync(query, updateQuery);
        }
    }
}