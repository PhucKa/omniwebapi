﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class MGSummaryRepository : NoSqlRepository<MGSummaryModel>, IMGSummaryRepository
    {
        public MGSummaryRepository(IMongoDatabase db, ISeqIdentity sq)
            : base(db, sq)
        {
        }

        // aVinh rebuild
        public Task<List<string>> GetSummaryNames(int Type, long RefId, long StoreId)
        {
            var query = Builders<MGSummaryModel>.Filter.And(
                 Builders<MGSummaryModel>.Filter.Eq(m => m.storeid, StoreId),
                 Builders<MGSummaryModel>.Filter.Eq(m => m.refId, RefId),
                 Builders<MGSummaryModel>.Filter.Eq(m => m.type, (int)Type)
                );
            return Collection.Find(query).Project(t => t.name).ToListAsync();
        }

        // aVinh rebuild
        public Task<long> ExistSummaryName(int Type, long RefId, long StoreId, string SummaryName)
        {
            var query = Builders<MGSummaryModel>.Filter.And(
                 Builders<MGSummaryModel>.Filter.Eq(m => m.storeid, StoreId),
                 Builders<MGSummaryModel>.Filter.Eq(m => m.refId, RefId),
                 Builders<MGSummaryModel>.Filter.Eq(m => m.type, (int)Type),
                 Builders<MGSummaryModel>.Filter.Eq(m => m.name, SummaryName)
                );
            return Collection.Find(query).Project(t => t._id).FirstOrDefaultAsync();
        }

        // aVinh rebuild
        public Task<List<MGSummaryModel>> GetSummaries(int Type, long RefId, long StoreId)
        {
            var query = Builders<MGSummaryModel>.Filter.And(
                 Builders<MGSummaryModel>.Filter.Eq(m => m.storeid, StoreId),
                 Builders<MGSummaryModel>.Filter.Eq(m => m.refId, RefId),
                 Builders<MGSummaryModel>.Filter.Eq(m => m.type, (int)Type)
                );
            return Collection.Find(query).ToListAsync();
        }
    }
}