﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class MGArticleRepository : NoSqlRepository<MGArticleModel>, IMGArticleRepository
    {
        public MGArticleRepository(IMongoDatabase db, ISeqIdentity seq)
            : base(db, seq)
        {
        }

        public Task<List<MGArticleModel>> GetBy(long storeId, List<long> articleIds)
        {
            return Collection.Find(m => articleIds.Contains(m._id) && m.storeid == storeId && !m.isdeleted).ToListAsync();
        }

        public Task<MGArticleModel> GetBy(long storeId, long blogId, long articleId)
        {
            return Collection.Find(m => m._id == articleId && m.storeid == storeId && m.blogid == blogId && !m.isdeleted).FirstOrDefaultAsync();
        }

        public Task<List<long>> GetByPagingAsync(long storeId, int currentpage, int pagesize)
        {
            return Collection.Find(m => m.storeid == storeId && !m.isdeleted)
                             .Project(m => m._id)
                             .SortByDescending(m => m._id)
                             .Skip((currentpage - 1) * pagesize)
                             .Limit(pagesize)
                             .ToListAsync();
        }

        public Task<long> CountByStoreAsync(long storeid)
        {
            var query = Builders<MGArticleModel>.Filter.And(
                Builders<MGArticleModel>.Filter.Eq(m => m.storeid, storeid),
                Builders<MGArticleModel>.Filter.Ne(m => m.isdeleted, true));

            return Collection.CountDocumentsAsync(query);
        }

        public Task DeleteByIdAsync(long storeId, long id)
        {
            var query = Builders<MGArticleModel>.Filter.And(
                Builders<MGArticleModel>.Filter.Eq(m => m.storeid, storeId),
                Builders<MGArticleModel>.Filter.Eq(m => m._id, id));
            var updateQuery = Builders<MGArticleModel>.Update
                                                      .Set(m => m.isdeleted, true);
            return Collection.UpdateOneAsync(query, updateQuery);
        }

        public Task<MGArticleModel> GetByHandle(long storeId, string handle)
        {
            return Collection.Find(m => m.storeid == storeId && m.url_handle == handle && !m.isdeleted).FirstOrDefaultAsync();
        }

        public Task<MGArticleModel> GetById(long storeId, long id)
        {
            return Collection.Find(m => m._id == id && m.storeid == storeId && !m.isdeleted).FirstOrDefaultAsync();
        }
        public Task<List<MGArticleModel>> GetArticlesByStoreId(long storeId)
        {
            return Collection.Find(m => m.storeid == storeId &&  !m.isdeleted).ToListAsync();
        }

        public Task<List<MGArticleModel>> GetById(long storeId, Dictionary<int, long> lids)
        {
            var ids = lids.Select(m => m.Value).ToList();
            return Collection.Find(m => ids.Contains(m._id) && m.storeid == storeId && !m.isdeleted).ToListAsync();
        }

        public Task<List<long>> GetListByBlogId(long blogId, long storeId)
        {
            return Collection.Find(m => m.storeid == storeId && m.blogid == blogId && !m.isdeleted).Project(m => m._id).ToListAsync();
        }

        public Task<List<MGArticleModel>> GetByBlogIdAsync(long storeId, long blogId)
        {
            return Collection.Find(m => m.storeid == storeId && m.blogid == blogId && !m.isdeleted).ToListAsync();
        }
        public Task<MGArticleModel> GetFirstByHandle(long storeId)
        {
            return Collection.Find(m => m.storeid == storeId && !m.isdeleted).SortByDescending(m => m.created_at).FirstOrDefaultAsync();
        }

    }
}