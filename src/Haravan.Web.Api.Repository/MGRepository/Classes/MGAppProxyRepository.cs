﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using MongoDB.Driver;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class MGAppProxyRepository : NoSqlRepository<MGAppProxy>, IMGAppProxyRepository
    {
        public MGAppProxyRepository(IMongoDatabase db, ISeqIdentity seq)
         : base(db, seq)
        {
        }

        public Task<MGAppProxy> GetByAppId(long storeId, long appId)
        {
            var query = Builders<MGAppProxy>.Filter.And(
              Builders<MGAppProxy>.Filter.Eq(m => m.StoreId, storeId),
              Builders<MGAppProxy>.Filter.Eq(m => m.AppId, appId));
            return Collection.Find(query).FirstOrDefaultAsync();
        }
    }
}