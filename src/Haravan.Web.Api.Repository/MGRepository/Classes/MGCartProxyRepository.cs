using System.Threading.Tasks;
using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using MongoDB.Driver;

namespace Haravan.Web.Api.Repository
{
    public class MGCartProxyRepository : NoSqlRepository<MGCartProxyModel>, IMGCartProxyRepository
    {
        public MGCartProxyRepository(IMongoDatabase db, ISeqIdentity seq)
            : base(db, seq)
        {
        }

        public Task<MGCartProxyModel> GetByAppId(long storeId, long appId)
        {
            var query = Builders<MGCartProxyModel>.Filter.And(
              Builders<MGCartProxyModel>.Filter.Eq(m => m.storeid, storeId),
              Builders<MGCartProxyModel>.Filter.Eq(m => m.appid, appId));
            return Collection.Find(query).FirstOrDefaultAsync();
        }
        public Task<MGCartProxyModel> GetById(long storeId, long cartProxyId)
        {
            var query = Builders<MGCartProxyModel>.Filter.And(
              Builders<MGCartProxyModel>.Filter.Eq(m => m.storeid, storeId),
              Builders<MGCartProxyModel>.Filter.Eq(m => m._id, cartProxyId));
            return Collection.Find(query).FirstOrDefaultAsync();
        }
        public Task<MGCartProxyModel> GetFirst(long storeId)
        {
            var query = Builders<MGCartProxyModel>.Filter.And(
              Builders<MGCartProxyModel>.Filter.Eq(m => m.storeid, storeId));
            return Collection.Find(query).FirstOrDefaultAsync();
        }

    }
}