﻿using MongoDB.Driver;

namespace Haravan.Web.Api.Repository
{
    public interface IMongoSequenDb
    {
        IMongoDatabase Db { get; }
    }

    public class MongoSequenDb : IMongoSequenDb
    {
        public MongoSequenDb(IMongoDatabase db)
        {
            Db = db;
        }

        public IMongoDatabase Db { get; private set; }
    }
}
