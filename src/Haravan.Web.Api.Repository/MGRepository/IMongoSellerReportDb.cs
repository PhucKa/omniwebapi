﻿using MongoDB.Driver;

namespace Haravan.Web.Api.Repository
{
    public interface IMongoSellerReportDb
    {
        IMongoDatabase Db { get; }
    }

    public class MongoSellerReportDb : IMongoSellerReportDb
    {
        public MongoSellerReportDb(IMongoDatabase db)
        {
            Db = db;
        }

        public IMongoDatabase Db { get; private set; }
    }
}