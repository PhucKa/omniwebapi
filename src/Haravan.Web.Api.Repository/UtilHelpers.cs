﻿using Haravan.Web.Api.BusinessObjects.Configs;
using Haravan.Web.Api.Repository.Infractstructure;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Haravan.Web.Api.UtilHelpers
{
    public static class HelpersDO
    {
        public static HttpClient HttpClients = null;

        static HelpersDO()
        {
#if NETCORE
            var handler = new HttpClientHandler();
            handler.ServerCertificateCustomValidationCallback += delegate { return true; };
            HttpClients = new HttpClient(handler);
            HttpClients.Timeout = TimeSpan.FromMilliseconds(7000);
            handler.SslProtocols = System.Security.Authentication.SslProtocols.Tls12 | System.Security.Authentication.SslProtocols.Tls11 | System.Security.Authentication.SslProtocols.Tls;
#else
            System.Net.ServicePointManager
                                .ServerCertificateValidationCallback +=
                                (sender, cert, chain, sslPolicyErrors) => true;
            HttpClients = new HttpClient
            {
                Timeout = TimeSpan.FromMilliseconds(10000)
            };
#endif
        }
        public static string AddQuery(this string path, object obj)
        {
            if (path == null || obj == null)
                return path;
            var dicParams = obj.ToKeyPairs().ToDictionary(m => m.Key, m => m.Value?.ToString());
            if (dicParams != null && dicParams.Any())
            {
                var queryStr = string.Empty;
                foreach (var param in dicParams)
                {
                    var paramValue = param.Value != null ? WebUtility.UrlEncode(param.Value) : null;
                    queryStr += $"{param.Key}={paramValue}&";
                }
                if (!string.IsNullOrEmpty(queryStr))
                    path += $"?{queryStr.Remove(queryStr.Length - 1)}";
            }
            return path;
        }
        public static IEnumerable<KeyValuePair<string, object>> ToKeyPairs(this object obj)
        {
            if (obj == null)
                yield break;

            foreach (var property in obj.GetType().GetProperties())
            {
                var value = property.GetValue(obj);
                yield return new KeyValuePair<string, object>(property.Name, value);
            }
        }
        public static async Task<T> SendRequestAsync<T>(string url, HttpMethod method, object data = null, string bearer_token = null, string soap_action = null, HttpContentType content_type = HttpContentType.Json, bool isBadRequest = false)
        {
            var content = await SendRequestAsync(url, method, data, bearer_token, soap_action, HttpContentType.Json, isBadRequest).ConfigureAwait(false);

            if (content != null)
            {
                try
                {
                    return JsonConvert.DeserializeObject<T>(content);
                }
                catch (Exception ex)
                {
                    var strObject = JsonConvert.SerializeObject(data);
                    throw new Exception(ex.ToString() + " data:" + strObject);
                }
            }

            else
                return default(T);
        }
        public static async Task<string> SendRequestAsync(string url, HttpMethod method, object data = null, string bearer_token = null, string soap_action = null, HttpContentType content_type = HttpContentType.Json, bool isBadRequest = false)
        {
            using (var requestMessage = new HttpRequestMessage())
            {
                requestMessage.RequestUri = new Uri(url);
                requestMessage.Method = method;

                HttpContent content = null;

                if (data != null)
                    if (content_type == HttpContentType.Json)
                    {
                        if (data is string)
                            content = new StringContent((string)data, Encoding.UTF8, "application/json");
                        else
                        {
                            content = new StringContent(JsonConvert.SerializeObject(data),
                              Encoding.UTF8, "application/json");
                        }

                    }
                    else
                    {
                        requestMessage.Headers.Add("SOAPAction", soap_action);
                        content = new StringContent((string)data, Encoding.UTF8, "text/xml");
                    }

                if (data != null)
                    requestMessage.Content = content;

                if (!string.IsNullOrEmpty(bearer_token))
                    requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", bearer_token);

                try
                {

                
                using (var rq = await HttpClients.SendAsync(requestMessage).ConfigureAwait(false))
                {
                    if (rq.IsSuccessStatusCode)
                    {
                        if (rq.Content != null)
                        {
                            return await rq.Content.ReadAsStringAsync().ConfigureAwait(false);
                        }
                        else
                            return null;
                    }
                    else
                    {
                        if (isBadRequest)
                        {
                            if (rq.StatusCode == System.Net.HttpStatusCode.BadRequest)
                            {
                                if (rq.Content != null)
                                {
                                    return await rq.Content.ReadAsStringAsync().ConfigureAwait(false);
                                }
                                else
                                    return null;
                            }
                            else
                                throw new Exception($"request to {url} error {rq.StatusCode}");
                        }
                        else
                            throw new Exception($"request to {url} error {rq.StatusCode}");
                    }
                }
            }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}