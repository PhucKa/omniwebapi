﻿using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IFileRepository
    {
        Task<(List<FileModel> data, long totalRecord)> GetImageList(string query, int limit, int page, bool is_image);

        Task<ApiResponse<FileModel>> UploadStorageImage(string fileName, List<Tuple<string, object>> data);

        Task<ApiResponse<FileModel>> UploadStorageFile(string fileName, List<Tuple<string, object>> data);

        Task<ApiResponse<FileModel>> AddFile(FileModel file, long? RootFileId, bool isCommit = true, long? StoreId = null);

        Task<ApiResponse<List<FileModel>>> GetProductImagesListByProductId(long productId);

        Task<ApiResponse<Files>> GetByFile(long fileId);
    }
}