﻿using BHN.SharedObject.EBSMessage;
using Haravan.Web.Api.BusinessObjects.Models.Article;
using Haravan.Web.Api.BusinessObjects.Models.Tag;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface ITagRepository
    {
        Task<bool> AddTags(List<ArticleAddPublisRequest> model, List<string> tags);

        Task<bool> RemoveTags(List<ArticleAddPublisRequest> model, List<string> tags);

        Task<ArticleTags[]> GetListTag(long storeId, int[] Types, long[] RefIds, string SummaryName = null);

        Task<bool> SetSummaryNames(SummaryType Type, long RefId, List<string> SummaryNames);

        Task<bool> AddSummaryNamesForRefIds(SummaryType Type, long[] RefIds, List<string> SummaryNames);

        Task<bool> RemoveSummaryNamesForRefIds(SummaryType Type, long[] RefIds, List<string> SummaryNames);

        Task<List<string>> GetSummaryNames(SummaryType Type, long RefId);

        Task<List<string>> GetSummaryNamesByType(int type, long storeId, long refId, int limit);

        Task<List<string>> GetGroupArticleTags(long blogId);

        Task<List<string>> GetArticleTags(int type, int limit);

        Task<List<string>> GetSummaryNames(int Type, long RefId, long StoreId);
    }
}