﻿using Haravan.Web.Api.BusinessObjects.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface ILocationRepository
    {
        Task<List<LocationsModel>> GetAvailableLocationList();
    }
}