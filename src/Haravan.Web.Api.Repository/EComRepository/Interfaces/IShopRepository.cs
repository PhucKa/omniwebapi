﻿using BHN.SharedObject.APIDataModel;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Models;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IShopRepository
    {
        Task<ApiResponse<bool>> CreateShop(CreateShopModel model);

        Task<ApiResponse<bool>> ActiveWebChannel(string channel);

        Task<ApiResponse<ShopAPIModel>> GetShopInfo();
    }
}