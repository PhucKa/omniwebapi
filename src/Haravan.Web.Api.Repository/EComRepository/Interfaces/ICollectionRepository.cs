﻿using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.ESModels;
using Haravan.Web.Api.BusinessObjects.Models.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface ICollectionRepository
    {
        Task<OmniBuildHandleModel> GetCollectionSimpleById(long id);

        Task<long> GetIdByHandle(string handle);

        Task<ApiResponse<ThemeDropdownModel>> GetCollectionByHandle(string handle);

        Task<string> GetHandleByIdAsync(long id);

        Task<(List<CollectionListModelRequest> datas, long totalRecord)> GetCollectionList(FilterSearchModel model);
    }
}