﻿using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.ESModels;
using Haravan.Web.Api.BusinessObjects.Models.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IProductRepository
    {
        Task<OmniBuildHandleModel> GetProductSimpleById(long id);

        Task<string> GetHandleByIdAsync(long id);

        Task<(List<ProductSearchRowModel> datas, long totalRecord)> GetProductList(FilterSearchModel model);

        Task<ApiResponse<ThemeDropdownModel>> GetProductByHandle(string handle);
    }
}