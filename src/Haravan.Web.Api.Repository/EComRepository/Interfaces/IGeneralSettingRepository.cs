﻿using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Models;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IGeneralSettingRepository
    {
        Task<ApiResponse<OnlineStoreSetting>> GetGeneralSettingByStoreId();

        Task<ApiResponse<OnlineStoreSetting>> UpdateOnlineStoreSetting(OnlineStoreSetting settings);

        #region cấu hình tài khoản , Field

        Task<ApiResponse<bool>> Update(CheckoutSettingModel Model);

        Task<ApiResponse<bool>> UpdateCustomerAccount(CheckoutSettingModel Model);

        Task<ApiResponse<CheckoutSettingModel>> GetByStoreId();

        Task<ApiResponse<bool>> UpdateFieldSetting(CheckoutSettingModel Model);

        Task<ApiResponse<bool>> UpdateOrderAdditionalContent(CheckoutSettingModel Model);

        Task<ApiResponse<bool>> DisableMultipass(long id);

        Task<ApiResponse<bool>> EnableMultipass(long id, int checkoutAccountType);

        Task<ApiResponse<bool>> EnableAccountKit(long id, int checkoutAccountType, string appId, string appSecret, string secret, bool isAccountKitAllowCreateAccount, bool isVerifyPasswordAfterLogin);

        Task<ApiResponse<bool>> DisableAccountKit(long id);

        #endregion cấu hình tài khoản , Field
    }
}