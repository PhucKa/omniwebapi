﻿using BHN.SharedObject.APIDataModel;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IMetafieldRepository
    {
        Task<ApiResponse<MetafieldAPIModel>> GetDetailApi(long metafieldId, long? ownerId, string ownerResources);

        Task<ApiResponse<bool>> DeleteApi(long metafieldId, long? ownerId, string ownerResources);

        Task<ApiResponse<List<MetafieldAPIModel>>> Omni_GetMetafieldList(
            int limit, int page, DateTime? created_at_min, DateTime? created_at_max,
            DateTime? updated_at_min, DateTime? updated_at_max, string @namespace,
            string key, string value_type, long owner_id, string owner_resource, long since_id, string owner_resources
            );

        Task<ApiResponse<long>> Omni_CountMetafields(
            int limit, int page, DateTime? created_at_min, DateTime? created_at_max,
            DateTime? updated_at_min, DateTime? updated_at_max, string @namespace,
            string key, string value_type, long owner_id, string owner_resource, long since_id, string owner_resources
            );

        Task<ApiResponse<MetafieldAPIModel>> Omni_GetMetafieldDetail(long metafieldId, string ownerResource, long ownerId);

        Task<ApiResponse<bool>> Omni_DeleteMetafield(long metafieldId, string ownerResource, long ownerId);

        Task<ApiResponse<MetafieldAPIModel>> Omni_AddMetafield(OmniCreateMetafieldRequestModel model);

        Task<ApiResponse<MetafieldAPIModel>> Omni_UpdateMetafield(long metafieldId, string ownerResource, long ownerId, OmniUpdateMetafieldRequestModel model);

        Task<ApiResponse<MetafieldAPIModel>> Omni_UpdateMetafield(long metafieldId, OmniUpdateMetafieldRequestModel model);
    }
}