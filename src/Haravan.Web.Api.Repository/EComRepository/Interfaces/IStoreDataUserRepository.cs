﻿using Haravan.Web.Api.BusinessObjects;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IStoreDataUserRepository
    {
        Task<StoreData> GetStoreData();
    }
}