﻿using Haravan.Web.Api.BusinessObjects.Models.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IFilterRepository
    {
        Task<ViewFilterData> GetFilter(int viewId);

        Task<FilterTab> GetTabDetail(long tabId);

        Task<long> AddFilter(FilterTab tab);

        Task<long> UpdateFilter(FilterTab tab);

        Task<long> DeleteFilter(long tabId);

        Task<List<DropdownlistModel>> GetUsersSimpleList();

        Task<List<OmniSimpleModel>> GetSimpleUserList(long userId);
    }
}