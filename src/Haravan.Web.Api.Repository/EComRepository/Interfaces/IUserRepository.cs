﻿using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.BusinessObjects.Models.Common;
using Haravan.Web.Api.BusinessObjects.Models.user;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IUserRepository
    {
        Task<List<DropdownlistModel>> GetAuthorList();

        Task<OmniUserDetail> GetUserDetail(long user_id);

        Task<OmniUserSimple> GetOmniUserSimple(long userId);

        Task<WebUserDetailModel> GetByIds(long user_id);

        Task<UserUpdateModel> GetUserForDomain(long userId);

        Task<bool> RemoveSaleChannelAfterUninstallApp(string clientId);

        Task<bool> RemoveDataAfterUninstallApp(string clientId);
    }
}