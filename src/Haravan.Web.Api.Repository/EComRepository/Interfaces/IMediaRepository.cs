﻿using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Models;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IMediaRepository
    {
        Task<FileModel> UploadArticleImages(IFormFile file);

        Task<DataPaging<List<FileModel>>> GetFileList(string query, int page, int limit);

        Task<DataPaging<List<FileModel>>> GetProductFeaturedImages(string query, int page, int limit);

        Task<ApiResponse<bool>> DeleteFile(long fileId);

        Task<ApiResponse<bool>> DeleteAllSelectedFile(List<long> values);
    }
}