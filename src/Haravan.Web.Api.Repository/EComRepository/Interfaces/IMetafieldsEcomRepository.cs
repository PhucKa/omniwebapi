﻿using BHN.SharedObject.EBSMessage;
using System;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IMetafieldsEcomRepository
    {
        Task<bool> DeleteByOnwerIdResourceType(long ownerid, int typeresource);

        Task<Func<long>> AddMetafield(MetafieldResourceOwner resourceOwner, long resourceId, string nameSpace, string key, string value, MetafieldValueType valueType, string desc, bool isCommit);
    }
}