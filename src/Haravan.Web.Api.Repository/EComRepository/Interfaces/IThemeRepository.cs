﻿using BHN.SharedObject.APIDataModel;
using BHN.SharedObject.EBSMessage;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.ESModels;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.BusinessObjects.Models.Common;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

//using static BHN.SharedObject.EBSMessage.RestApi;

namespace Haravan.Web.Api.Repository
{
    public interface IThemeRepository
    {
        Task<ApiResponse<long>> AddTheme(Themes model);

        Task<ApiResponse<Themes>> GetById(long themeId, long storeId);

        Task<ApiResponse<Themes>> GetById(long themeId);

        Task<ApiResponse<ThemeFiles>> GetByIdThemeFile(long ThemeFileId);

        Task<List<string>> GetTemplateArticles();

        Task<ApiResponse<ThemeListModel>> GetThemeList();

        Task<List<string>> GetTemplateByName();

        Task<List<string>> GetPageTemplate();

        Task<int> CountTheme();

        Task<ApiResponse<ThemeModel>> ImportNewTheme(string themeName);

        Task<List<ThemeFileModel>> GetListThemeFileCanNotDelete(bool isGetPrefixType);

        Task<ApiResponse<ThemeModel>> UnPublishTheme(ThemeModel model);

        Task<ApiResponse<ThemeModel>> DuplicateTheme(long themeId);

        Task<ApiResponse<ThemeModel>> DuplicateThemeByLanguage(long themeId, string prefixText);

        Task<ApiResponse<bool>> DeleteTheme(long themeId);

        Task<ApiResponse<List<ThemeModel>>> PublishTheme(long Id);

        Task<ApiResponseBase> Export(long themeId);

        Task<ApiResponse<string>> GetSettingSchemaThemeFile(long themeId);

        Task<ApiResponse<ThemeFileModel>> EditSettingData(long themeId, StringModel model);

        Task<ApiResponse<string>> GetSettingDataFile(long themeId);

        Task<ApiResponse<string>> GetFrameToken(long themeId);

        Task<ApiResponse<ThemeModel>> GetThemeForEdit(long id);

        Task<ApiResponse<ThemeModel>> GetThemeFileForEdit(long themeid);

        Task<ApiResponse<ThemeFileModel>> LoadCurrentLocale(long id, string locale);

        Task<ApiResponse<ThemeModel>> GetThemeForLocale(long id);

        Task<ApiResponse<ThemeModel>> GetThemeForSetting(long id);

        Task<ApiResponse<List<FileModel>>> GetFileVersion(long Id, long RootFileId);

        Task<ApiResponseBase> DeleteThemeFile(ThemeFileModel themeFile);

        Task<ApiResponse<ThemeFileModel>> SaveThemeFile(ThemeFileModelRequest model);

        Task<ApiResponse<ThemeFileModel>> SaveLocaleThemeFile(ThemeFileModel themeFile, string newContent);

        Task<ApiResponseBase> RenameTheme(long Id, string Name);

        Task<ApiResponse<ThemeFileModel>> RenameThemeFile(RenameThemeFileRequest themeFile);

        Task<ApiResponse<ThemeFileModel>> AddNewThemeFile(ThemeFileModel oldThemeFile, ThemeFileModel themeFile);

        Task<ApiResponse<SimpleListModel>> GetListDefaultFont();

        Task<ApiResponse<ThemeFileModel>> EditSettingDataContent(long id, string content);

        Task<ApiResponse<ThemeModel>> CheckThemeIsImporting(long themeId);

        Task<ApiResponse<FreeThemesModel>> ThemeStoreFree(string name);

        Task<ApiResponse<string>> GetThemeFileContent(long themeId, string url);

        Task<ApiResponse<bool>> ValidateSameName(long themeId, string fileName);

        Task<ApiResponse<FileModel>> UploadThemeFile(
           long themeId, FileType fileType, string fileName, Stream stream, int? maxWidth = null, int? maxHeight = null);

        Task<ApiResponse<FileModel>> AddFile(FileModel file, long? RootFileId, bool isCommit = true, long? StoreId = null);

        Task<ApiResponse<ThemeModel>> GetThemefileByIdAsync(long themeId);

        Task<ApiResponse<string>> GetFileByUrlId(long fileId);

        Task<ApiResponse<List<FileModel>>> GetThemeFileUpload(long themeId, FileModel[] fileList);

        Task<ApiResponse<List<KeyValuePair<string, string>>>> ThemeSettingGetCollectionTitleByUrlHandle(List<string> listUrlHandle);

        Task<(List<SimpleListModel> datas, long totalRecord)> ThemeSettingGetListCollection(FilterSearchModel model);

        Task<ApiResponse<FileModel>> SettingThemeAssetUpload(FileModel assetFile);

        Task<ApiResponse<string>> InstallTheme(string name, long timestamp, string hash);

        #region OPEN_API

        Task<ApiResponse<List<AssetAPIModel>>> GetAssetList(long themeId);

        Task<ApiResponse<AssetAPIModel>> GetAssetDetail(long themeId, int fileType, string fileName);

        Task<ApiResponse<bool>> DeleteAssetApi(long themeId, int fileType, string fileName);

        Task<ApiResponse<AssetAPIModel>> CreateOrUpdateAsset(long themeId, Dictionary<string, object> dicInserted);

        Task<ApiResponse<List<ThemeAPIModel>>> GetListThemesApi();

        Task<ApiResponse<ThemeAPIModel>> GetThemeDetailApi(long themeId);

        Task<ApiResponse<bool>> DeleteThemeApi(long themeId);

        Task<ApiResponse<ThemeAPIModel>> UpdateThemeApi(long themeId, Dictionary<string, object> dicUpdated);

        Task<ApiResponse<ThemeAPIModel>> CreateThemeApi(ThemeAPIModel model);

        Task<ApiResponse<bool>> PublishThemeApi(long themeId, BusinessObjects.Enums.ThemeType? type);

        Task<ApiResponse<List<ThemeEditorDropdownModel>>> GetThemeEditorDropdown();

        #endregion OPEN_API

        Task<ApiResponse<string>> GetThemeZipUrlByThemeCode(string code);

        Task<ApiResponse<string>> GetExportThemeUrl(string key);
    }
}