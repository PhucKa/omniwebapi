﻿using BHN.SharedObject.APIDataModel;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class MetafieldRepository : IMetafieldRepository
    {
        private readonly IEcomApiRepository rpEcomApi;

        public MetafieldRepository(IEcomApiRepository rpEcomApi)
        {
            this.rpEcomApi = rpEcomApi;
        }

        public async Task<ApiResponse<MetafieldAPIModel>> Omni_AddMetafield(OmniCreateMetafieldRequestModel model)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<MetafieldAPIModel>>($"open_api/metafields", HttpMethod.Post, null, model);
            return result;
        }

        public async Task<ApiResponse<long>> Omni_CountMetafields(int limit, int page, DateTime? created_at_min, DateTime? created_at_max, DateTime? updated_at_min, DateTime? updated_at_max, string @namespace, string key, string value_type, long owner_id, string owner_resource, long since_id, string owner_resources)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<long>>($"open_api/metafields/count", HttpMethod.Get, new
            {
                limit,
                page,
                created_at_min,
                created_at_max,
                updated_at_min,
                updated_at_max,
                @namespace,
                key,
                value_type,
                owner_id,
                owner_resource,
                since_id,
                owner_resources
            });
            return result;
        }

        public async Task<ApiResponse<bool>> Omni_DeleteMetafield(long metafield_id, string owner_resource, long owner_resource_id)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<bool>>($"open_api/metafields/{metafield_id}/owner_resource/{owner_resource}/owner_resource_id/{owner_resource_id}", HttpMethod.Delete, new
            {
                metafield_id,
                owner_resource,
                owner_resource_id
            });
            return result;
        }

        public async Task<ApiResponse<MetafieldAPIModel>> Omni_GetMetafieldDetail(long metafield_id, string owner_resource, long owner_resource_id)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<MetafieldAPIModel>>($"open_api/metafields/{metafield_id}/owner_resource/{owner_resource}/owner_resource_id/{owner_resource_id}", HttpMethod.Get, new
            {
                metafield_id,
                owner_resource,
                owner_resource_id
            });
            return result;
        }

        public async Task<ApiResponse<List<MetafieldAPIModel>>> Omni_GetMetafieldList(int limit, int page, DateTime? created_at_min, DateTime? created_at_max, DateTime? updated_at_min, DateTime? updated_at_max, string @namespace, string key, string value_type, long owner_id, string owner_resource, long since_id, string owner_resources)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<List<MetafieldAPIModel>>>($"open_api/metafields", HttpMethod.Get, new
            {
                limit,
                page,
                created_at_min,
                created_at_max,
                updated_at_min,
                updated_at_max,
                @namespace,
                key,
                value_type,
                owner_id,
                owner_resource,
                since_id,
                owner_resources
            });
            return result;
        }

        public async Task<ApiResponse<MetafieldAPIModel>> Omni_UpdateMetafield(long metafield_id, string owner_resource, long owner_resource_id, OmniUpdateMetafieldRequestModel model)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<MetafieldAPIModel>>($"open_api/metafields/{metafield_id}/owner_resource/{owner_resource}/owner_resource_id/{owner_resource_id}", HttpMethod.Put, null, model);
            return result;
        }

        public async Task<ApiResponse<MetafieldAPIModel>> Omni_UpdateMetafield(long metafield_id, OmniUpdateMetafieldRequestModel model)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<MetafieldAPIModel>>($"open_api/metafields/{metafield_id}/common", HttpMethod.Put, null, model);
            return result;
        }

        public async Task<ApiResponse<MetafieldAPIModel>> GetDetailApi(long metafield_id, long? ownerId, string ownerResources)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<MetafieldAPIModel>>($"open_api/metafields/{metafield_id}/common", HttpMethod.Get);
            return result;
        }

        public async Task<ApiResponse<bool>> DeleteApi(long metafield_id, long? ownerId, string ownerResources)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<bool>>($"open_api/metafields/{metafield_id}/common", HttpMethod.Delete);
            return result;
        }
    }
}