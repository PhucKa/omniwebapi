﻿using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.ESModels;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.BusinessObjects.Models.Common;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class CollectionRepository : ICollectionRepository
    {
        private readonly IEcomApiRepository rpEcomApi;

        public CollectionRepository(IEcomApiRepository rpEcomApi)
        {
            this.rpEcomApi = rpEcomApi;
        }

        public async Task<ApiResponse<ThemeDropdownModel>> GetCollectionByHandle(string handle)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<ThemeDropdownModel>>(
                EcomApiPath.GetCollectionByHandle,
                HttpMethod.Post,
                null,
                new StringModel
                {
                    Value = handle
                });
            return result;
        }

        public async Task<OmniBuildHandleModel> GetCollectionSimpleById(long id)
        {
            var path = EcomApiPath.GetCollectionSimpleById;
            path = path.Replace("{id}", id + "");
            var result = await rpEcomApi.CallEcomApi<ApiResponse<OmniBuildHandleModel>>(path, HttpMethod.Get);
            if (result != null && result.Data != null)
            {
                return result.Data;
            }
            return null;
        }

        public async Task<(List<CollectionListModelRequest> datas, long totalRecord)> GetCollectionList(FilterSearchModel model)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<DataPaging<List<CollectionListModelRequest>>>>(EcomApiPath.GetCollectionList, HttpMethod.Post, null, model);
            if (result != null && result.Data != null)
            {
                return (result.Data.Data, result.Data.TotalRecord);
            }
            return (null, 0);
        }

        public async Task<string> GetHandleByIdAsync(long id)
        {
            var ret = await GetCollectionSimpleById(id);
            if (ret != null) return ret.handle;
            return null;
        }

        public async Task<long> GetIdByHandle(string handle)
        {
            var path = EcomApiPath.GetCollectionIdByHandle;
            path = path.Replace("{handle}", handle);
            var ret = await rpEcomApi.CallEcomApi<ApiResponse<long>>(path, HttpMethod.Get);
            if (ret.Data != 0)
            {
                return ret.Data;
            }
            return 0;
        }
    }
}