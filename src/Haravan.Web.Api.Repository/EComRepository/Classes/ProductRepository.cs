﻿using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.ESModels;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.BusinessObjects.Models.Common;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class ProductRepository : IProductRepository
    {
        private readonly IEcomApiRepository rpEcomApi;

        public ProductRepository(IEcomApiRepository rpEcomApi)
        {
            this.rpEcomApi = rpEcomApi;
        }

        public async Task<OmniBuildHandleModel> GetProductSimpleById(long id)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<OmniBuildHandleModel>>($"omni/products/simplehandle/{id}", HttpMethod.Get);
            if (result != null && result.Data != null) return result.Data;
            return null;
        }

        public async Task<string> GetHandleByIdAsync(long id)
        {
            var ret = await GetProductSimpleById(id);
            if (ret != null) return ret.handle;
            return null;
        }

        public async Task<ApiResponse<ThemeDropdownModel>> GetProductByHandle(string handle)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<ThemeDropdownModel>>($"omni/products/getproductbyhandle", HttpMethod.Post, null,
                new StringModel { Value = handle });

            return result;
        }

        public async Task<(List<ProductSearchRowModel> datas, long totalRecord)> GetProductList(FilterSearchModel model)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<DataPaging<List<ProductSearchRowModel>>>>($"products/list", HttpMethod.Post, null, model);
            if (result != null && result.Data.Data != null) return (result.Data.Data, result.Data.TotalRecord);
            return (null, 0);
        }
    }
}