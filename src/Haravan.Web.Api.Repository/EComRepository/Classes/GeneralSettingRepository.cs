﻿using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Models;
using System.Net.Http;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class GeneralSettingRepository : IGeneralSettingRepository
    {
        private readonly IEcomApiRepository rpEcomApi;

        public GeneralSettingRepository(IEcomApiRepository rpEcomApi)
        {
            this.rpEcomApi = rpEcomApi;
        }

        public async Task<ApiResponse<OnlineStoreSetting>> GetGeneralSettingByStoreId()
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<OnlineStoreSetting>>("stores/getonlinestoresetting", HttpMethod.Get);
            return result;
        }

        public async Task<ApiResponse<OnlineStoreSetting>> UpdateOnlineStoreSetting(OnlineStoreSetting settings)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<OnlineStoreSetting>>("stores/updateonlinestoresetting", HttpMethod.Post, null, settings);
            return result;
        }

        #region cấu hình tài khoản , Field

        public async Task<ApiResponse<bool>> Update(CheckoutSettingModel Model)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<bool>>("checkout_setting/updatesettingweb", HttpMethod.Post, null, Model);
            return result;
        }

        public async Task<ApiResponse<bool>> UpdateCustomerAccount(CheckoutSettingModel Model)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<bool>>("checkout_setting/update_checkoutsetting_customer_account", HttpMethod.Post, null, Model);
            return result;
        }

        public async Task<ApiResponse<CheckoutSettingModel>> GetByStoreId()
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<CheckoutSettingModel>>("checkout_setting/list", HttpMethod.Get);
            return result;
        }

        public async Task<ApiResponse<bool>> UpdateFieldSetting(CheckoutSettingModel Model)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<bool>>("checkout_setting/update_checkoutsetting_field_setting", HttpMethod.Post, null, Model);
            return result;
        }

        public async Task<ApiResponse<bool>> UpdateOrderAdditionalContent(CheckoutSettingModel Model)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<bool>>("checkout_setting/update_checkoutsetting_order_content", HttpMethod.Post, null, Model);
            return result;
        }

        public async Task<ApiResponse<bool>> DisableMultipass(long id)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<bool>>($"checkout_setting/disable_multipass/{id}", HttpMethod.Post, null, id);
            return result;
        }

        public async Task<ApiResponse<bool>> EnableMultipass(long id, int checkoutAccountType)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<bool>>("checkout_setting/enable_multipass", HttpMethod.Post, null, new
            {
                id = id,
                checkoutAccountType = checkoutAccountType
            });
            return result;
        }

        public async Task<ApiResponse<bool>> DisableAccountKit(long id)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<bool>>($"checkout_setting/disable_accountKit/{id}", HttpMethod.Post, null, id);
            return result;
        }

        public async Task<ApiResponse<bool>> EnableAccountKit(long id, int checkoutAccountType, string appId, string appSecret, string secret, bool isAccountKitAllowCreateAccount, bool isVerifyPasswordAfterLogin)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<bool>>("checkout_setting/enable_accountKit", HttpMethod.Post, null, new
            {
                id,
                checkoutAccountType,
                appId,
                appSecret,
                secret,
                isAccountKitAllowCreateAccount,
                isVerifyPasswordAfterLogin
            });
            return result;
        }

        #endregion cấu hình tài khoản , Field
    }
}