﻿using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Models;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class LocationRepository : ILocationRepository
    {
        private readonly IEcomApiRepository rpEcomApi;

        public LocationRepository(IEcomApiRepository rpEcomApi)
        {
            this.rpEcomApi = rpEcomApi;
        }

        public async Task<List<LocationsModel>> GetAvailableLocationList()
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<List<LocationsModel>>>("common/locations/available/list", HttpMethod.Get);
            return result.Data;
        }
    }
}