﻿using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class MediaRepository : IMediaRepository
    {
        private readonly IEcomApiRepository rpEcomApi;

        public MediaRepository(IEcomApiRepository rpEcomApi)
        {
            this.rpEcomApi = rpEcomApi;
        }

        public async Task<FileModel> UploadArticleImages(IFormFile file)
        {
            var data = new List<Tuple<string, object>>();
            data.Add(new Tuple<string, object>(file.FileName, await StreamToByArray(file.OpenReadStream())));
            var result = await rpEcomApi.CallEcomApi<ApiResponse<FileModel>>("omniwebapi/media/article/images", HttpMethod.Post, null, data);
            var listData = result.Data;
            return listData;
        }

        public async Task<DataPaging<List<FileModel>>> GetFileList(string query, int page, int limit)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<DataPaging<List<FileModel>>>>("omni/media/list", HttpMethod.Get, new
            {
                query,
                page,
                limit
            });
            return result.Data;
        }

        public async Task<DataPaging<List<FileModel>>> GetProductFeaturedImages(string query, int page, int limit)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<DataPaging<List<FileModel>>>>("omni/products/featureimages/list", HttpMethod.Get, new
            {
                query,
                page,
                limit
            });
            return result.Data;
        }

        public async Task<ApiResponse<bool>> DeleteFile(long fileId)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<bool>>($"omni/media/{fileId}", HttpMethod.Delete);
            return result;
        }

        public async Task<ApiResponse<bool>> DeleteAllSelectedFile(List<long> values)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<bool>>("omni/media/bulk/delete", HttpMethod.Put, null, new
            {
                values
            });
            return result;
        }

        private static async Task<byte[]> StreamToByArray(Stream input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                await input.CopyToAsync(ms);
                return ms.ToArray();
            }
        }
    }
}