﻿using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.BusinessObjects.Models.Common;
using Haravan.Web.Api.BusinessObjects.Models.user;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly IEcomApiRepository rpEcomApi;

        public UserRepository(IEcomApiRepository rpEcomApi)
        {
            this.rpEcomApi = rpEcomApi;
        }

        public async Task<bool> RemoveSaleChannelAfterUninstallApp(string clientId)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<bool>>($"omniwebapi/common/sale_channels/{clientId}", HttpMethod.Delete);
            return result.Data;
        }

        public async Task<bool> RemoveDataAfterUninstallApp(string clientId)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<bool>>($"omniwebapi/common/apps/remove/{clientId}", HttpMethod.Delete);
            return result.Data;
        }

        public async Task<List<DropdownlistModel>> GetAuthorList()
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<List<DropdownlistModel>>>("omniwebapi/common/users/active", HttpMethod.Get);
            return result.Data;
        }

        public async Task<OmniUserDetail> GetUserDetail(long user_id)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<OmniUserDetail>>($"omni/users/{user_id}", HttpMethod.Get);
            return result.Data;
        }

        public async Task<OmniUserSimple> GetOmniUserSimple(long userId)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<OmniUserSimple>>($"omni/users/simplev2/{userId}", HttpMethod.Get);
            return result.Data;
        }

        public async Task<WebUserDetailModel> GetByIds(long user_id)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<WebUserDetailModel>>($"omniwebapi/common/users/{user_id}", HttpMethod.Get);
            return result.Data;
        }

        public async Task<UserUpdateModel> GetUserForDomain(long userId)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<UserUpdateModel>>($"omniwebapi/common/userfordomain/{userId}", HttpMethod.Get);
            return result.Data;
        }
    }
}