﻿using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Models.Common;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

//using static BHN.SharedObject.EBSMessage.RestApi;

namespace Haravan.Web.Api.Repository
{
    public class FilterRepository : IFilterRepository
    {
        private readonly IEcomApiRepository rpEcomApi;

        public FilterRepository(IEcomApiRepository rpEcomApi)
        {
            this.rpEcomApi = rpEcomApi;
        }

        public async Task<ViewFilterData> GetFilter(int viewId)
        {
            var path = EcomApiPath.GetFilterByViewId;
            path = path.Replace("{viewId}", viewId + "");
            var result = await rpEcomApi.CallEcomApi<ApiResponse<ViewFilterData>>(path, HttpMethod.Get);
            var data = result.Data;
            return data;
        }

        public async Task<FilterTab> GetTabDetail(long tabId)
        {
            var path = EcomApiPath.GetFilterTabDetail;
            path = path.Replace("{tabId}", tabId + "");
            var result = await rpEcomApi.CallEcomApi<ApiResponse<FilterTab>>(path, HttpMethod.Get);
            var data = result.Data;
            return data;
        }

        public async Task<long> AddFilter(FilterTab tab)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<long>>(EcomApiPath.AddFilter, HttpMethod.Post, null, tab);
            var data = result.Data;
            return data;
        }

        public async Task<long> UpdateFilter(FilterTab tab)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<long>>(EcomApiPath.AddFilter, HttpMethod.Put, null, tab);
            var data = result.Data;
            return data;
        }

        public async Task<long> DeleteFilter(long tabId)
        {
            var path = EcomApiPath.DeleteFilter;
            path = path.Replace("{tabId}", tabId + "");
            var result = await rpEcomApi.CallEcomApi<ApiResponse<long>>(path, HttpMethod.Delete);
            var data = result.Data;
            return data;
        }

        public async Task<List<DropdownlistModel>> GetUsersSimpleList()
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<List<DropdownlistModel>>>(EcomApiPath.GetUserSimpleList, HttpMethod.Get);
            var data = result.Data;
            return data;
        }

        public async Task<List<OmniSimpleModel>> GetSimpleUserList(long userId)
        {
            var path = EcomApiPath.GetDetailSimpleUser;
            path = path.Replace("{userId}", userId + "");
            var result = await rpEcomApi.CallEcomApi<ApiResponse<List<OmniSimpleModel>>>(path, HttpMethod.Get);
            var data = result.Data;
            return data;
        }
    }
}