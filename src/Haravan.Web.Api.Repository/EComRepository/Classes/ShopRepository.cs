﻿using BHN.SharedObject.APIDataModel;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Models;
using System.Net.Http;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class ShopRepository : IShopRepository
    {
        private readonly IEcomApiRepository rpEcomApi;

        public ShopRepository(IEcomApiRepository rpEcomApi)
        {
            this.rpEcomApi = rpEcomApi;
        }

        public async Task<ApiResponse<long>> AddTheme(Themes model)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<long>>($"omniwebapi/theme", HttpMethod.Post, null, model);
            return result;
        }

        public async Task<ApiResponse<bool>> CreateShop(CreateShopModel model)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<bool>>($"stores", HttpMethod.Post, null, model);

            return result;
        }

        public async Task<ApiResponse<bool>> ActiveWebChannel(string channel)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<bool>>($"omni/salechannels/active?channel={channel}", HttpMethod.Post);
            return result;
        }

        public async Task<ApiResponse<ShopAPIModel>> GetShopInfo()
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<ShopAPIModel>>($"open_api/shop", HttpMethod.Get);
            return result;
        }
    }

    public class RequestModel
    {
        public string PartnerRef { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string ShopName { get; set; }
        public string ClientIp { get; set; }
    }
}