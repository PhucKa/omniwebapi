﻿using Haravan.Web.Api.BusinessObjects;
using System.Net.Http;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class StoreDataUserRepository : IStoreDataUserRepository
    {
        private readonly IEcomApiRepository rpEcomApi;

        public StoreDataUserRepository(IEcomApiRepository rpEcomApi)
        {
            this.rpEcomApi = rpEcomApi;
        }

        public async Task<StoreData> GetStoreData()
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<StoreData>>($"stores/cached", HttpMethod.Get);
            return result.Data;
        }
    }
}