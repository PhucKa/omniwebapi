﻿using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class FileRepository : IFileRepository
    {
        private readonly IEcomApiRepository rpEcomApi;

        public FileRepository(IEcomApiRepository rpEcomApi)
        {
            this.rpEcomApi = rpEcomApi;
        }

        public async Task<(List<FileModel> data, long totalRecord)> GetImageList(string query, int limit, int page, bool is_image)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<DataPaging<List<FileModel>>>>(EcomApiPath.GetImageList, HttpMethod.Get, new
            {
                query,
                limit,
                page,
                is_image
            });
            return (result.Data.Data, result.Data.TotalRecord);
        }

        public async Task<ApiResponse<FileModel>> UploadStorageImage(string fileName, List<Tuple<string, object>> data)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<FileModel>>(EcomApiPath.UploadStorageImage, HttpMethod.Post, null, data);
            return result;
        }

        public async Task<ApiResponse<FileModel>> UploadStorageFile(string fileName, List<Tuple<string, object>> data)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<FileModel>>(EcomApiPath.UploadStorageFile, HttpMethod.Post, null, data);
            return result;
        }

        public async Task<ApiResponse<FileModel>> AddFile(FileModel file, long? RootFileId, bool isCommit = true, long? StoreId = null)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<FileModel>>(EcomApiPath.UploadFile, HttpMethod.Post, null, new
            {
                file,
                RootFileId,
                isCommit,
                StoreId
            });
            if (result != null)
            {
                return result;
            }
            return result;
        }

        public async Task<ApiResponse<List<FileModel>>> GetProductImagesListByProductId(long productId)
        {
            var path = EcomApiPath.GetProductImagesByProductId;
            path = path.Replace("{productId}", productId + "");
            var result = await rpEcomApi.CallEcomApi<ApiResponse<List<FileModel>>>(path, HttpMethod.Get);
            return result;
        }

        public async Task<ApiResponse<Files>> GetByFile(long fileId)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<Files>>($"omniwebapi/theme/{fileId}/filebyid", HttpMethod.Get);
            return result;
        }
    }
}