﻿using BHN.SharedObject.APIDataModel;
using BHN.SharedObject.EBSMessage;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.ESModels;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.BusinessObjects.Models.Common;
using Haravan.Web.Api.UtilHelpers;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

//using static BHN.SharedObject.EBSMessage.RestApi;

namespace Haravan.Web.Api.Repository
{
    public class ThemeRepository : IThemeRepository
    {
        private readonly IEcomApiRepository rpEcomApi;

        public ThemeRepository(IEcomApiRepository rpEcomApi)
        {
            this.rpEcomApi = rpEcomApi;
        }

        public async Task<ApiResponse<ThemeModel>> DuplicateTheme(long themeId)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<ThemeModel>>($"omniwebapi/theme/duplicate/{themeId}", HttpMethod.Post);
            return result;
        }
        public async Task<ApiResponse<ThemeModel>> DuplicateThemeByLanguage(long themeId, string prefixText)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<ThemeModel>>($"omniwebapi/theme/duplicate_language/{themeId}", HttpMethod.Post, null, new
            {
                themeId,
                prefixText
            });
            return result;
        }

        public async Task<ApiResponse<bool>> DeleteTheme(long themeId)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<bool>>($"omniwebapi/theme/{themeId}", HttpMethod.Delete);

            return result;
        }

        public async Task<ApiResponse<Themes>> GetById(long themeId, long storeId)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<Themes>>($"omniwebapi/theme/{themeId}", HttpMethod.Get);

            return result;
        }

        public async Task<ApiResponse<Themes>> GetById(long themeId)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<Themes>>($"omniwebapi/theme/{themeId}/byid", HttpMethod.Get);

            return result;
        }

        public async Task<ApiResponse<ThemeFiles>> GetByIdThemeFile(long ThemeFileId)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<ThemeFiles>>($"omniwebapi/theme/{ThemeFileId}/themefilebyid", HttpMethod.Get);

            return result;
        }

        public async Task<ApiResponse<ThemeListModel>> GetThemeList()
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<ThemeListModel>>("omniwebapi/theme", HttpMethod.Get);

            return result;
        }

        public async Task<List<string>> GetTemplateArticles()
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<List<string>>>("omniwebapi/common/template/article", HttpMethod.Get);
            return result.Data;
        }

        public async Task<List<string>> GetTemplateByName()
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<List<string>>>("omniwebapi/common/template/blog", HttpMethod.Get);
            var data = result.Data;
            return data;
        }

        public async Task<List<string>> GetPageTemplate()
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<List<string>>>("omniwebapi/common/template/page", HttpMethod.Get);
            var data = result.Data;
            return data;
        }

        public async Task<int> CountTheme()
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<int>>($"omniwebapi/common/count", HttpMethod.Get);
            return result.Data;
        }

        public async Task<ApiResponse<ThemeModel>> ImportNewTheme(string themeName)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<ThemeModel>>($"omniwebapi/theme/importtheme", HttpMethod.Post, null, new StringModel { Value = themeName });

            return result;
        }

        public async Task<List<ThemeFileModel>> GetListThemeFileCanNotDelete(bool isGetPrefixType)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<List<ThemeFileModel>>>($"omniwebapi/theme/{isGetPrefixType}/themescannotdelete", HttpMethod.Get);

            return result.Data;
        }

        public async Task<ApiResponse<long>> AddTheme(Themes model)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<long>>($"omniwebapi/theme", HttpMethod.Post, null, model);

            return result;
        }

        public async Task<ApiResponse<ThemeModel>> UnPublishTheme(ThemeModel model)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<ThemeModel>>($"omniwebapi/theme/unpublish", HttpMethod.Put, null, model);

            return result;
        }

        public async Task<ApiResponse<List<ThemeModel>>> PublishTheme(long themeid)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<List<ThemeModel>>>($"omniwebapi/theme/{themeid}/publish", HttpMethod.Post, null, themeid);
            return result;
        }

        public async Task<ApiResponseBase> Export(long themeId)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponseBase>($"omniwebapi/theme/{themeId}/export", HttpMethod.Post, null, themeId);
            return result;
        }

        public async Task<ApiResponse<string>> GetSettingSchemaThemeFile(long themeId)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<string>>($"omniwebapi/theme/{themeId}/setting_schema", HttpMethod.Get);
            return result;
        }

        public async Task<ApiResponse<string>> GetSettingDataFile(long themeId)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<string>>($"omniwebapi/theme/{themeId}/setting_data", HttpMethod.Get);
            return result;
        }

        public async Task<ApiResponse<ThemeFileModel>> EditSettingData(long themeId, StringModel model)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<ThemeFileModel>>($"omniwebapi/theme/{themeId}/setting_data", HttpMethod.Put, null, model);
            return result;
        }

        public async Task<ApiResponse<string>> GetFrameToken(long themeId)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<string>>($"omniwebapi/theme/{themeId}/frame_token", HttpMethod.Get);
            return result;
        }

        public async Task<ApiResponse<ThemeModel>> GetThemeForEdit(long id)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<ThemeModel>>($"omniwebapi/theme/{id}/themeforedit", HttpMethod.Get);
            return result;
        }

        public async Task<ApiResponse<ThemeModel>> GetThemeFileForEdit(long themeid)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<ThemeModel>>($"omniwebapi/theme/{themeid}/themefiles", HttpMethod.Get);
            return result;
        }

        public async Task<ApiResponse<ThemeFileModel>> LoadCurrentLocale(long themeid, string locale)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<ThemeFileModel>>($"omniwebapi/theme/{themeid}/loadcurrentlocale", HttpMethod.Post, null, new
            { id = themeid, locale = locale });
            return result;
        }

        public async Task<ApiResponse<ThemeModel>> GetThemeForLocale(long id)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<ThemeModel>>($"omniwebapi/theme/{id}/getthemeforlocale", HttpMethod.Get);
            return result;
        }

        public async Task<ApiResponse<ThemeModel>> GetThemeForSetting(long id)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<ThemeModel>>($"omniwebapi/theme/{id}/getthemeforsetting", HttpMethod.Get);
            return result;
        }

        public async Task<ApiResponse<List<FileModel>>> GetFileVersion(long Id, long RootFileId)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<List<FileModel>>>($"omniwebapi/theme/getfileversion", HttpMethod.Post, null, new { Id, RootFileId });
            return result;
        }

        public async Task<ApiResponseBase> DeleteThemeFile(ThemeFileModel themeFile)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponseBase>($"omniwebapi/theme/deletethemeFile", HttpMethod.Post, null, themeFile);
            return result;
        }

        public async Task<ApiResponse<ThemeFileModel>> SaveThemeFile(ThemeFileModelRequest model)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<ThemeFileModel>>($"omniwebapi/theme/savethemefile", HttpMethod.Post, null, model);
            return result;
        }

        public async Task<ApiResponse<ThemeFileModel>> SaveLocaleThemeFile(ThemeFileModel themeFile, string newContent)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<ThemeFileModel>>($"omniwebapi/theme/savelocalethemefile", HttpMethod.Post, null, new { themeFile, newContent });
            return result;
        }

        public async Task<ApiResponseBase> RenameTheme(long Id, string Name)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponseBase>($"omniwebapi/theme/renametheme", HttpMethod.Post, null, new
            {
                Id,
                Name
            });
            return result;
        }

        public async Task<ApiResponse<ThemeFileModel>> RenameThemeFile(RenameThemeFileRequest themeFile)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<ThemeFileModel>>($"omniwebapi/theme/renamethemefile", HttpMethod.Post, null, themeFile);
            return result;
        }

        public async Task<ApiResponse<ThemeFileModel>> AddNewThemeFile(ThemeFileModel oldThemeFile, ThemeFileModel themeFile)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<ThemeFileModel>>($"omniwebapi/theme/addnewthemefile", HttpMethod.Post, null, new
            {
                oldThemeFile,
                themeFile
            });
            return result;
        }

        public async Task<ApiResponse<SimpleListModel>> GetListDefaultFont()
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<SimpleListModel>>($"omniwebapi/theme/getListdefaultfont", HttpMethod.Get);
            return result;
        }

        public async Task<ApiResponse<ThemeFileModel>> EditSettingDataContent(long id, string content)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<ThemeFileModel>>($"omniwebapi/theme/editsettingdatacontent", HttpMethod.Post, null, new
            {
                id,
                content
            });
            return result;
        }

        public async Task<ApiResponse<ThemeModel>> CheckThemeIsImporting(long themeId)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<ThemeModel>>($"omniwebapi/theme/{themeId}/checkthemeisimporting", HttpMethod.Get);
            return result;
        }

        public async Task<ApiResponse<FreeThemesModel>> ThemeStoreFree(string name)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<FreeThemesModel>>($"omniwebapi/theme/themestorefree", HttpMethod.Get, new { name });
            return result;
        }

        public async Task<ApiResponse<string>> GetThemeFileContent(long themeId, string url)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<string>>($"omniwebapi/theme/getthemefilecontent", HttpMethod.Post, new { themeId, url });
            return result;
        }

        public async Task<ApiResponse<string>> InstallTheme(string name, long timestamp, string hash)
        {
            return await rpEcomApi.CallEcomApi<ApiResponse<string>>($"omniwebapi/theme/install_from_theme_store", HttpMethod.Post, null, new
            {
                name,
                timestamp,
                hash
            });
        }

        public async Task<ApiResponse<bool>> ValidateSameName(long themeId, string fileName)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<bool>>($"omniwebapi/theme/validatesamename", HttpMethod.Post, null, new
            {
                themeId,
                fileName
            });
            if (result != null)
            {
                return result;
            }
            return result;
        }

        public async Task<ApiResponse<FileModel>> UploadThemeFile(long themeId, FileType fileType, string fileName, Stream stream, int? maxWidth = null, int? maxHeight = null)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<FileModel>>($"omniwebapi/theme/uploadthemefile", HttpMethod.Post, null, new
            {
                themeId,
                fileType,
                fileName,
                stream,
                maxWidth,
                maxHeight
            });
            if (result != null)
            {
                return result;
            }
            return result;
        }

        public async Task<ApiResponse<FileModel>> AddFile(FileModel file, long? RootFileId, bool isCommit = true, long? StoreId = null)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<FileModel>>($"omniwebapi/theme/file", HttpMethod.Post, null, new
            {
                file,
                RootFileId,
                isCommit,
                StoreId
            });
            if (result != null)
            {
                return result;
            }
            return result;
        }

        public async Task<ApiResponse<ThemeModel>> GetThemefileByIdAsync(long themeId)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<ThemeModel>>($"omniwebapi/theme/{themeId}/getthemefile", HttpMethod.Get);
            return result;
        }

        public async Task<ApiResponse<string>> GetFileByUrlId(long fileId)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<string>>($"omniwebapi/theme/{fileId}/fileurl", HttpMethod.Get);
            return result;
        }

        public async Task<ApiResponse<List<FileModel>>> GetThemeFileUpload(long themeId, FileModel[] fileList)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<List<FileModel>>>($"omniwebapi/theme/{themeId}/themefileupload", HttpMethod.Post, null, new
            {
                themeId,
                fileList
            });
            return result;
        }

        public async Task<ApiResponse<List<KeyValuePair<string, string>>>> ThemeSettingGetCollectionTitleByUrlHandle(List<string> listUrlHandle)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<List<KeyValuePair<string, string>>>>($"omniwebapi/theme/setting_collectiontitlebyurlhandle", HttpMethod.Post, null, new
            {
                listUrlHandle
            });
            return result;
        }

        public async Task<(List<SimpleListModel> datas, long totalRecord)> ThemeSettingGetListCollection(FilterSearchModel model)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<DataPaging<List<SimpleListModel>>>>($"omniwebapi/theme/getlistcollection", HttpMethod.Post, null, model);
            if (result != null && result.Data != null)
                return (result.Data.Data, result.Data.TotalRecord);
            return (null, 0);
        }

        public async Task<ApiResponse<FileModel>> SettingThemeAssetUpload(FileModel model)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<FileModel>>($"omniwebapi/theme/settings/upload", HttpMethod.Post, null, model);
            return result;
        }

        public async Task<ApiResponse<string>> GetThemeZipUrlByThemeCode(string code)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<string>>("omniwebapi/theme/zip_url", HttpMethod.Get, new
            {
                code
            });
            return result;
        }

        #region OPEN_API

        public async Task<ApiResponse<List<AssetAPIModel>>> GetAssetList(long themeId)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<List<AssetAPIModel>>>($"omniwebapi/theme/assetlist/{themeId}", HttpMethod.Get);
            return result;
        }

        public async Task<ApiResponse<AssetAPIModel>> GetAssetDetail(long themeId, int fileType, string fileName)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<AssetAPIModel>>($"omniwebapi/theme/assetdetail/{themeId}", HttpMethod.Get, new
            {
                themeId,
                fileType,
                fileName
            });
            return result;
        }

        public async Task<ApiResponse<bool>> DeleteAssetApi(long themeId, int fileType, string fileName)
        {
            var model = new ModelDeleteAssetApi
            {
                fileType = fileType,
                fileName = fileName
            };
            var result = await rpEcomApi.CallEcomApi<ApiResponse<bool>>($"omniwebapi/theme/asset/{themeId}", HttpMethod.Delete, null, model);
            return result;
        }

        public async Task<ApiResponse<AssetAPIModel>> CreateOrUpdateAsset(long themeId, Dictionary<string, object> dicInserted)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<AssetAPIModel>>($"omniwebapi/theme/createasset/{themeId}", HttpMethod.Post, null, new
            {
                themeId,
                dicInserted
            });
            return result;
        }

        public async Task<ApiResponse<List<ThemeAPIModel>>> GetListThemesApi()
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<List<ThemeAPIModel>>>($"omniwebapi/theme/themeapi", HttpMethod.Get);
            return result;
        }

        public async Task<ApiResponse<ThemeAPIModel>> GetThemeDetailApi(long themeId)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<ThemeAPIModel>>($"omniwebapi/theme/themeapidetail/{themeId}", HttpMethod.Get);
            return result;
        }

        public async Task<ApiResponse<bool>> DeleteThemeApi(long themeId)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<bool>>($"omniwebapi/theme/themeapi/{themeId}", HttpMethod.Delete, null, themeId);
            return result;
        }

        public async Task<ApiResponse<ThemeAPIModel>> UpdateThemeApi(long themeId, Dictionary<string, object> dicInserted)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<ThemeAPIModel>>($"omniwebapi/theme/updatethemeapi/{themeId}", HttpMethod.Post, null, new { themeId, dicInserted });

            return result;
        }

        public async Task<ApiResponse<ThemeAPIModel>> CreateThemeApi(ThemeAPIModel model)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<ThemeAPIModel>>($"omniwebapi/theme/createthemeapi", HttpMethod.Post, null, model);
            return result;
        }

        public async Task<ApiResponse<bool>> PublishThemeApi(long themeId, BusinessObjects.Enums.ThemeType? type)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<bool>>($"omniwebapi/theme/publishthemeapi/{themeId}", HttpMethod.Post, null, new { themeId, type });
            return result;
        }

        public async Task<ApiResponse<List<ThemeEditorDropdownModel>>> GetThemeEditorDropdown()
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<List<ThemeEditorDropdownModel>>>("omniwebapi/theme/theme_editor/dropdown", HttpMethod.Get);
            return result;
        }

        public async Task<ApiResponse<string>> GetExportThemeUrl(string key)
        {
            var link = "omniwebapi/theme/export_url";
            link = link.AddQuery(new
            {
                key
            });
            var result = await rpEcomApi.CallEcomApi<ApiResponse<string>>(link, HttpMethod.Get);
            return result;
        }

        #endregion OPEN_API
    }
}