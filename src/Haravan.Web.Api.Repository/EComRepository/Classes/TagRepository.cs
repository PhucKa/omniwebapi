﻿using BHN.SharedObject.EBSMessage;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Models.Article;
using Haravan.Web.Api.BusinessObjects.Models.Tag;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class TagRepository : ITagRepository
    {
        private readonly IEcomApiRepository rpEcomApi;

        public TagRepository(IEcomApiRepository rpEcomApi)
        {
            this.rpEcomApi = rpEcomApi;
        }

        public async Task<bool> AddTags(List<ArticleAddPublisRequest> model, List<string> tags)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<bool>>("omniwebapi/common/bulk/tags/set", HttpMethod.Post, null, new
            {
                model,
                Tags = tags
            });
            return result.Data;
        }

        public async Task<bool> RemoveTags(List<ArticleAddPublisRequest> model, List<string> tags)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<bool>>("omniwebapi/common/bulk/tags/remove", HttpMethod.Post, null, new
            {
                model,
                Tags = tags
            });
            return result.Data;
        }

        public async Task<ArticleTags[]> GetListTag(long storeId, int[] Types, long[] RefIds, string SummaryName = null)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<ArticleTags[]>>("omniwebapi/common/tags/getlist", HttpMethod.Post, null, new
            {
                storeId = storeId,
                Types = Types,
                RefIds = RefIds,
                SummaryName = SummaryName
            });
            return result.Data;
        }

        // aVinh rebuild
        public async Task<bool> SetSummaryNames(SummaryType Type, long RefId, List<string> SummaryNames)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<bool>>("omniwebapi/common/tags/setsumaryname", HttpMethod.Post, null, new
            {
                Type,
                RefId,
                SummaryNames
            });
            return result.Data;
        }

        // aVinh rebuild
        public async Task<bool> AddSummaryNamesForRefIds(SummaryType Type, long[] RefIds, List<string> SummaryNames)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<bool>>("omniwebapi/common/tags/setsumaryrefId", HttpMethod.Post, null, new
            {
                Type,
                RefIds,
                SummaryNames
            });
            return result.Data;
        }

        // aVinh rebuild
        public async Task<bool> RemoveSummaryNamesForRefIds(SummaryType Type, long[] RefIds, List<string> SummaryNames)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<bool>>("omniwebapi/common/tags/removesumaryrefId", HttpMethod.Post, null, new
            {
                Type,
                RefIds,
                SummaryNames
            });
            return result.Data;
        }

        // aVinh rebuild
        public async Task<List<string>> GetSummaryNamesByType(int type, long storeId, long refId, int limit)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<List<string>>>($"omniwebapi/common/tags/getsumarynamebytype", HttpMethod.Post, null, new
            {
                type,
                storeId,
                refId,
                limit
            });
            return result.Data;
        }

        // aVinh rebuild
        public async Task<List<string>> GetSummaryNames(int Type, long RefId, long StoreId)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<List<string>>>("omniwebapi/common/tags/getbyidsumaryname", HttpMethod.Post, null, new
            {
                Type,
                RefId,
                StoreId
            });
            return result.Data;
        }

        public async Task<List<string>> GetGroupArticleTags(long blogId)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<List<string>>>($"omniwebapi/common/{blogId}/grouptags", HttpMethod.Get);
            return result.Data;
        }

        public async Task<List<string>> GetArticleTags(int type, int limit)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<List<string>>>("omniwebapi/common/summaries", HttpMethod.Post, null, new
            {
                type,
                limit
            });
            return result.Data;
        }

        public async Task<List<string>> GetSummaryNames(SummaryType Type, long RefId)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<List<string>>>("omniwebapi/common/tags/getsumaryname", HttpMethod.Post, null, new
            {
                Type,
                RefId
            });
            return result.Data;
        }
    }
}