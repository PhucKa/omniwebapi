﻿using BHN.SharedObject.EBSMessage;
using Haravan.Web.Api.BusinessObjects;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class MetafieldsEcomRepository : IMetafieldsEcomRepository
    {
        private readonly IEcomApiRepository rpEcomApi;

        public MetafieldsEcomRepository(IEcomApiRepository rpEcomApi)
        {
            this.rpEcomApi = rpEcomApi;
        }

        public async Task<bool> DeleteByOnwerIdResourceType(long ownerid, int typeresource)
        {
            var result = await rpEcomApi.CallEcomApi<ApiResponse<bool>>($"metafields/deletebyowneridresource/{ownerid}/typeresource/{typeresource}", HttpMethod.Delete);
            return result.Data;
        }

        public async Task<Func<long>> AddMetafield(MetafieldResourceOwner resourceOwner, long resourceId, string nameSpace, string key, string value, MetafieldValueType valueType, string desc, bool isCommit)
        {
            var afterFunc = await rpEcomApi.CallEcomApi<ApiResponse<long>>("metafields/create", HttpMethod.Post, null, new
            {
                resourceOwner,
                resourceId,
                nameSpace,
                key,
                value,
                valueType,
                desc,
                isCommit
            });
            Func<long> result = () => afterFunc.Data;
            return result;
        }
    }
}