﻿using Haravan.Libs.Abstractions;
using Haravan.Web.Api.BusinessObjects;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class StoreContextMiddleware
    {
        private readonly RequestDelegate _next;

        public StoreContextMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(
            HttpContext httpContext,
            IRequestContext context,
            IInternalApiContext intContext
           )
        {
            #region request context

            intContext.Context = httpContext;

            #endregion request context

            await _next(httpContext);
        }
    }
}