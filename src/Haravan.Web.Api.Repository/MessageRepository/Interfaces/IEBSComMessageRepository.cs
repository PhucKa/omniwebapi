﻿using System;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IEBSComMessageRepository
    {
        Task UpdateStoreCache(long storeId);
        Task SendMsgAsync<T>(T data) where T : class;
        Task FireStoreDataChange(long storeid);
        Task FireStoreDataChangeAtTime(long storeid, DateTime date);
        Task FireStoreDataChangeLinkList(long storeid);
    }
}
