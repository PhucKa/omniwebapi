﻿using BHN.SharedObject.EBSMessage;
using MassTransit;
using System;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class EBSComMessageRepository : IEBSComMessageRepository
    {
        private readonly IBus _bus;

        public EBSComMessageRepository(IBus bus)
        {
            _bus = bus;

        }

        public async Task SendMsgAsync<T>(T data) where T : class
        {
            await _bus.Publish(FromSource.Seller(data));
        }

        public async Task UpdateStoreCache(long storeId)
        {
            var msg = new FireStoreDataChangedMsg()
            {
                Action = MessageAction.Update,
                StoreId = storeId
            };
            await _bus.Publish(FromSource.Seller(msg));
        }

        public async Task FireStoreDataChange(long storeid)
        {
            StoreDataChangedMsg msg = new StoreDataChangedMsg()
            {
                Action = MessageAction.Update,
                StoreId = storeid,
                PublishDate = DateTime.UtcNow.AddMinutes(-5)
            };
            await _bus.Publish(FromSource.Seller(msg));
        }
        public async Task FireStoreDataChangeLinkList(long storeid)
        {
            StoreDataChangedMsg msg = new StoreDataChangedMsg()
            {
                Action = MessageAction.Update,
                StoreId = storeid,
                PublishDate = DateTime.UtcNow.AddMinutes(-5),
                DocumentType = (int)EnumDocumentType.LinkList
            };
            await _bus.Publish(FromSource.Seller(msg));
        }

        public async Task FireStoreDataChangeAtTime(long storeid, DateTime date)
        {
            StoreDataChangedMsg msg = new StoreDataChangedMsg()
            {
                Action = MessageAction.Update,
                StoreId = storeid,
                PublishDate = date
            };
            await _bus.Publish(FromSource.Seller(msg));
        }
    }
}
