﻿using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IEtcdApiRepository
    {
        Task<bool> CallEtcdApi(string value, string path);
    }
}
