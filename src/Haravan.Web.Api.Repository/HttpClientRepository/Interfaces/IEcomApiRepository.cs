﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public interface IEcomApiRepository
    {
        Task<T> CallEcomApi<T>(string path, HttpMethod method, object param = null, object data = null);
    }
}
