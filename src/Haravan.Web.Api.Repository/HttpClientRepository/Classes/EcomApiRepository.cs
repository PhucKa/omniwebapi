﻿using Haravan.Web.Api.UtilHelpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class EcomApiRepository : IEcomApiRepository
    {
        private const string HeaderStoreId = "X-ECOM-STORE";
        private const string HeaderUserId = "X-ECOM-USER";
        private const string HeaderUserName = "X-ECOM-USERNAME";
        private const string HeaderUserEmail = "X-ECOM-USEREMAIL";
        private readonly HttpClient httpClient;
        private readonly IExtensionPerRequest ExtensionReq;

        public EcomApiRepository(
            HttpClient httpClient,
            IExtensionPerRequest ExtensionReq)
        {
            this.httpClient = httpClient;
            this.ExtensionReq = ExtensionReq;
        }

        public async Task<T> CallEcomApi<T>(string path, HttpMethod method, object param = null, object data = null)
        {
            var content = await CallApi(path, method, param, data);
            if (!string.IsNullOrWhiteSpace(content))
            {
                return JsonConvert.DeserializeObject<T>(content);
            }
            else
            {
                return default(T);
            }
        }

        private async Task<string> CallApi(string path, HttpMethod method, object param = null, object data = null)
        {
            if (param != null)
            {
                path = path.AddQuery(param);
            }
            var requestMsg = new HttpRequestMessage(method, path);
            addDataContextHeader(requestMsg);
            if (data != null)
            {
                if (data is string)
                {
                    requestMsg.Content = new StringContent((string)data, Encoding.UTF8, "application/json");
                }
                else if (data is List<Tuple<string, object>>)
                {
                    var formData = new MultipartFormDataContent();
                    foreach (var pair in data as List<Tuple<string, object>>)
                    {
                        if (pair.Item2 is byte[])
                        {
                            formData.Add(new ByteArrayContent(pair.Item2 as byte[]), pair.Item1, pair.Item1);
                        }
                        else
                        {
                            if (pair.Item2 != null)
                            {
                                formData.Add(new StringContent(pair.Item2.ToString()), pair.Item1);
                            }
                        }
                    }
                    requestMsg.Content = formData;
                }
                else
                {
                    requestMsg.Content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");
                }
            }

            using (var rq = await httpClient.SendAsync(requestMsg).ConfigureAwait(false))
            {
                if (rq.IsSuccessStatusCode || rq.StatusCode == HttpStatusCode.NotModified)
                {
                    if (rq.StatusCode == HttpStatusCode.NotModified)
                    {
                        return null;
                    }

                    if (rq.Content != null)
                    {
                        return await rq.Content.ReadAsStringAsync();
                    }
                    else
                        return null;
                }
                else
                {
                    throw new Exception($"request to {httpClient.BaseAddress + path} error {rq.StatusCode}");
                }
            }
        }

        private void addDataContextHeader(HttpRequestMessage rq)
        {
            rq.Headers.Add(HeaderStoreId, ExtensionReq.OrgId.ToString());
            rq.Headers.Add(HeaderUserId, ExtensionReq.UserId.ToString());
            rq.Headers.Add(HeaderUserEmail, WebUtility.UrlEncode(ExtensionReq.UserEmail));
            if (!string.IsNullOrEmpty(ExtensionReq.UserName))
            {
                rq.Headers.Add(HeaderUserName, WebUtility.UrlEncode(ExtensionReq.UserName));
            }
        }
    }
}
