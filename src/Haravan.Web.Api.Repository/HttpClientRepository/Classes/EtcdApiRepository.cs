﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Repository
{
    public class EtcdApiRepository : IEtcdApiRepository
    {
        private readonly HttpClient httpClient;
        public EtcdApiRepository(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public async Task<bool> CallEtcdApi(string value, string path)
        {
            var requestMsg = new HttpRequestMessage(HttpMethod.Put, path);
            var data = new Dictionary<string, string> { { "value", value } };
            requestMsg.Content = new FormUrlEncodedContent(data);
            try
            {
                using (var rq = await httpClient.SendAsync(requestMsg).ConfigureAwait(false))
                {
                    return rq.IsSuccessStatusCode ? true : false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
