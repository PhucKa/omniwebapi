namespace Haravan.Web.Api.Repository
{
    using Haravan.BlobService;
    using Microsoft.Extensions.Configuration;
    public class BlobServiceClientFactory : IBlobServiceClientFactory
    {
        public BlobServiceClientFactory(IConfiguration configuration)
        {
            var config = configuration.GetSection("BlobService");
            var defaultHost = config["Default:Domain"];
            var defaultToken = config["Default:Token"];
            var tmpHost = config["Temporary:Domain"];
            var tmpToken = config["Temporary:Token"];
            var fileHost = config["File:Domain"];
            var fileToken = config["File:Token"];
            var themeHost = config["Theme:Domain"];
            var themeToken = config["Theme:Token"];
            var hstaticHost = config["Hstatic:Domain"];
            var hstaticToken = config["Hstatic:Token"];
            tempClient = new MPBlobServiceClient(defaultHost, defaultToken, tmpHost, tmpToken);
            fileClient = new BlobServiceClient(fileHost, fileToken);
            themeClient = new BlobServiceClient(themeHost, themeToken);
            hstaticClient = new BlobServiceClient(hstaticHost, hstaticToken);
        }
        public IMPBlobServiceClient tempClient { get; }
        public IBlobServiceClient fileClient { get; }
        public IBlobServiceClient themeClient { get; }
        public IBlobServiceClient hstaticClient { get; }
    }
}