namespace Haravan.Web.Api.Repository
{
    using Haravan.BlobService;

    public interface IBlobServiceClientFactory
    {
        IMPBlobServiceClient tempClient { get; }
        IBlobServiceClient fileClient { get; }
        IBlobServiceClient themeClient { get; }
        IBlobServiceClient hstaticClient { get; }
    }
}
