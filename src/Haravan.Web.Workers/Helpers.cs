﻿
namespace Haravan.Web.Workers
{
    //public static class Helpers
    //{
    //    public static ICache Cache { get; set; }
    //    public static AppConfig AppConfig { get; set; }

    //    #region ECOM

    //    private static readonly HttpClient httpClient = new HttpClient(new HttpClientHandler() { UseCookies = false });

    //    public static async Task<string> CallEcomIntegrateApi(string path, HttpMethod method, long storeId, object data = null)
    //    {
    //        return await CallEcomApi(path, method, storeId, data);
    //    }

    //    public static async Task<T> CallEcomIntegrateApi<T>(string path, HttpMethod method, long storeId, object data = null)
    //    {
    //        return await CallEcomApi<T>(path, method, storeId, data);
    //    }

    //    private const string HeaderStoreId = "X-ECOM-STORE";

    //    private static void AddEcomDataContextHeader(HttpRequestMessage rq, long storeId)
    //    {
    //        rq.Headers.Add(HeaderStoreId, storeId.ToString());
    //    }

    //    private static async Task<T> CallEcomApi<T>(string path, HttpMethod method, long storeId, object data = null)
    //    {
    //        var url = $"{AppConfig.EcomApi}{path}";
    //        var requestMsg = new HttpRequestMessage(method, url);
    //        AddEcomDataContextHeader(requestMsg, storeId);

    //        HttpContent content = null;

    //        if (data != null)
    //            if (data is string)
    //                content = new StringContent((string)data, Encoding.UTF8, "application/json");
    //            else
    //                content = new StringContent(JsonConvert.SerializeObject(data),
    //                    Encoding.UTF8, "application/json");
    //        requestMsg.Content = content;

    //        using (var rq = await httpClient.SendAsync(requestMsg))
    //        {
    //            if (rq.IsSuccessStatusCode)
    //            {
    //                if (rq.Content != null)
    //                {
    //                    return JsonConvert.DeserializeObject<T>(await rq.Content.ReadAsStringAsync());
    //                }
    //                else
    //                    return default(T);
    //            }
    //            else
    //            {
    //                throw new Exception($"request to {url} error {rq.StatusCode}");
    //            }
    //        }
    //    }

    //    private static async Task<string> CallEcomApi(string path, HttpMethod method, long storeId, object data = null)
    //    {
    //        var url = $"{AppConfig.EcomApi}{path}";
    //        var requestMsg = new HttpRequestMessage(method, url);
    //        AddEcomDataContextHeader(requestMsg, storeId);

    //        HttpContent content = null;

    //        if (data != null)
    //            if (data is string)
    //                content = new StringContent((string)data, Encoding.UTF8, "application/json");
    //            else
    //                content = new StringContent(JsonConvert.SerializeObject(data),
    //                    Encoding.UTF8, "application/json");
    //        requestMsg.Content = content;

    //        using (var rq = await httpClient.SendAsync(requestMsg))
    //        {
    //            if (rq.IsSuccessStatusCode)
    //            {
    //                if (rq.Content != null)
    //                {
    //                    return await rq.Content.ReadAsStringAsync();
    //                }
    //                else
    //                    return null;
    //            }
    //            else
    //            {
    //                throw new Exception($"request to {url} error {rq.StatusCode}");
    //            }
    //        }
    //    }

    //    public static async Task RemoveKeyChannelConnect(long storeId, long channelId)
    //    {
    //        var key = $"{CacheKeys.GetStoreChannelConnect(storeId, channelId)}";
    //        await Cache.RemoveAsync(key);
    //    }

    //    #endregion
    //}
}
