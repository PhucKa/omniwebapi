﻿using System;
using System.Threading.Tasks;
using Haravan.Libs.Abstractions;
using Haravan.Web.Api.Business;
using Haravan.Web.Api.Repository;
using Haravan.WebHook.Objects.TransferMsg;
using MassTransit;
using Microsoft.Extensions.DependencyInjection;

namespace Haravan.Web.Workers.Handlers
{
    public class AppHandler : IConsumer<AppUninstallTransferMsg>
    {
        private readonly IServiceProvider serviceProvider;
        private readonly IScriptTagsBusiness bizScriptTag;
        private readonly IAppProxyBusiness bizAppProxy;
        private readonly ICartProxyBusiness bizCartProxy;
        private readonly IEcomBusiness bizEcom;
        public AppHandler(
            IServiceProvider serviceProvider,
            IScriptTagsBusiness bizScriptTag,
            ICartProxyBusiness bizCartProxy,
            IAppProxyBusiness bizAppProxy,
            IEcomBusiness bizEcom
            )
        {
            this.serviceProvider = serviceProvider;
            this.bizScriptTag = bizScriptTag;
            this.bizAppProxy = bizAppProxy;
            this.bizCartProxy = bizCartProxy;
            this.bizEcom = bizEcom;
        }

        public async Task Consume(ConsumeContext<AppUninstallTransferMsg> context)
        {
            if (context == null || context.Message == null)
            {
                return;
            }
            var msg = context.Message;
            var requestContext = (WorkerRequestContext)serviceProvider.GetService<IRequestContext>();
            requestContext.OrgId = msg.org_id;
            requestContext.UserId = msg.user_id;

            var extReq = serviceProvider.GetService<IExtensionPerRequest>();
            extReq.StoreId = msg.org_id;
            extReq.OrgId = msg.org_id;
            extReq.UserId = msg.user_id;

            await bizScriptTag.RemoveScriptTagsByAppId(msg.user_id);
            await bizAppProxy.DeleteFromWorker(msg.app_id);
            await bizCartProxy.DeleteFromWorker(msg.user_id);
            await bizEcom.RemoveDataAfterUninstallApp(msg.client_id);
        }
    }
}
