﻿using BHN.SharedObject.EBSMessage;
using Haravan.Kafka.Consumers;
using Haravan.Kafka.Handlers;
using Haravan.Libs.Abstractions;
using Haravan.Web.Api.BusinessObjects;
using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Haravan.Web.Api.Repository;
using Haravan.Web.Api.Business;

namespace Haravan.Web.Workers.Handlers
{
    [ConsumerMetadata(new string[] { KafkaTopics.InitDataNewShop }, ContinueAfterError: true)]
    public class InitDataNewShopConsumer :
       IHandler,
       IHandler<PrepareDataNewShopMsg>
    {
        private readonly IServiceProvider serviceProvider;
        private readonly IBlogBusiness bizBlog;
        private readonly IArticleBusiness bizArticle;
        private readonly IPageBusiness bizPage;
        private readonly INavigationBusiness bizNavigation;
        public InitDataNewShopConsumer(
            IServiceProvider serviceProvider,
            IBlogBusiness bizBlog,
            IArticleBusiness bizArticle,
            IPageBusiness bizPage,
            INavigationBusiness bizNavigation)
        {
            this.serviceProvider = serviceProvider;
            this.bizBlog = bizBlog;
            this.bizArticle = bizArticle;
            this.bizPage = bizPage;
            this.bizNavigation = bizNavigation;
        }

        public async Task Handle(HandleContext context, CancellationToken cancelToken)
        {
            await Task.CompletedTask;
        }

        public async Task Handle(HandleContext<PrepareDataNewShopMsg> context, CancellationToken cancelToken)
        {
            if (context == null || context.Message == null)
            {
                return;
            }

            var msg = context.Message;

            if (msg == null || msg.StoreId <= 0)
            {
                return;
            }

            var requestContext = (WorkerRequestContext)serviceProvider.GetService<IRequestContext>();
            requestContext.OrgId = msg.StoreId;
            requestContext.UserId = msg.UserId;
            requestContext.UserName = msg.UserName;

            var extReq = serviceProvider.GetService<IExtensionPerRequest>();
            extReq.StoreId = msg.StoreId;
            extReq.OrgId = msg.StoreId;
            extReq.UserId = msg.UserId;
            extReq.UserName = msg.UserName;
            await bizBlog.Init_NewStore();
            await bizArticle.Init_NewStore();
            await bizPage.Init_NewStore();
            await bizNavigation.Init_NewStore();

        }
    }
}
