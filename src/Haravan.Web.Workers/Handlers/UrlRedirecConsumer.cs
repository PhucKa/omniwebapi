﻿using BHN.SharedObject.EBSMessage;
using Haravan.Kafka.Consumers;
using Haravan.Kafka.Handlers;
using Haravan.Libs.Abstractions;
using Haravan.Web.Api.Business;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Enums;
using Haravan.Web.Api.Repository;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Haravan.Web.Workers.Handlers
{
    [ConsumerMetadata(new string[] { KafkaTopics.IntegrateUrlRedirect }, MaxErrorRetries: 1, CommitPeriod: 5, ContinueAfterError: true)]
    public class UrlRedirecConsumer :
      IHandler,
      IHandler<IntegrateUrlRedirecMsg>
    {
        private readonly IServiceProvider serviceProvider;
        private readonly INavigationBusiness bizNavigation;

        public UrlRedirecConsumer(
            IServiceProvider serviceProvider,
            INavigationBusiness bizNavigation
            )
        {
            this.serviceProvider = serviceProvider;
            this.bizNavigation = bizNavigation;
        }

        public async Task Handle(HandleContext context, CancellationToken cancelToken)
        {
            await Task.CompletedTask;
        }

        public async Task Handle(HandleContext<IntegrateUrlRedirecMsg> context, CancellationToken cancelToken)
        {
            if (context == null || context.Message == null)
            {
                return;
            }
            var msg = context.Message;

            if (msg == null || msg.StoreId <= 0)
            {
                return;
            }
            var requestContext = (WorkerRequestContext)serviceProvider.GetService<IRequestContext>();
            requestContext.OrgId = msg.StoreId;
            requestContext.UserId = msg.UserId;
            requestContext.UserName = msg.UserName;

            var extReq = serviceProvider.GetService<IExtensionPerRequest>();
            extReq.StoreId = msg.StoreId;
            extReq.OrgId = msg.StoreId;
            extReq.UserId = msg.UserId;
            extReq.UserName = msg.UserName;

            if (msg.type == (int)LinkFieldType.Product)
            {
                await bizNavigation.AddOrUpdateProductUrlRedirect(msg.OldPath, msg.NewPath);
            }
        }
    }
}