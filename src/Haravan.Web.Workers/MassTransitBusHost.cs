using Haravan.Caching;
using Haravan.ESB.Shared;
using Haravan.Libs.Abstractions;
using Haravan.Web.Api.Business.Extensions;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Configs;
using Haravan.Web.Api.Repository;
using Haravan.Web.Workers.Handlers;
using MassTransit;
using MassTransit.ExtensionsDependencyInjectionIntegration;
using MassTransit.RabbitMqTransport;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Nest;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Haravan.Web.Workers
{
    public class MassTransitBusHost : BusHost
    {
        public MassTransitBusHost(IConfiguration configuration, IHostingEnvironment env, Microsoft.Extensions.Hosting.IHost host)
            : base(configuration, env, host)
        {
        }

        public override void ConfigureBusConsumers(IServiceCollectionConfigurator services)
        {
            services.AddConsumer<AppHandler>();
        }

        public override void ConfigureBusHanler(string queueName, IServiceProvider provider, IRabbitMqReceiveEndpointConfigurator queue)
        {
            switch (queueName)
            {
                case "Bus_Web_Workers":
                    queue.Consumer<AppHandler>(provider);
                    break;
            }
        }

        public override void ConfigureServices(IServiceCollection services)
        {
            services.Configure<AppConfig>(Configuration);
            services.Configure<RedisConfig>(Configuration.GetSection("Redis"));
            services.Configure<ElasticConfig>(Configuration.GetSection("Elastic"));
            services.Configure<MongoConfig>(Configuration.GetSection("Mongo"));
            services.AddMongo(Configuration);
            services.AddScoped<IRequestContext, WorkerRequestContext>();
            services.AddScoped<IInternalApiContext, InternalApiContext>();
            services.AddScoped<IExtensionPerRequest, ExtensionPerRequest>();
            services.AddCoreWorkerService(Configuration);
            services.AddConsoleClient(Configuration);
            services.AddHealthChecks()
                    .AddKafkaHealthCheck();
            services.AddKafkaBuilder(Configuration)
                    .AddMultiJsonHandler<InitDataNewShopConsumer>(Configuration["Kafka:default-group"])
                    .JsonSerializeUseNewtonSoft();
            services.AddSingleton<IEBSComMessageRepository, EBSComMessageRepository>();
            services.AddSingleton<ICache>(u =>
            {
                var config = u.GetService<IOptions<RedisConfig>>().Value;

                var redis = config.SellerRedis.Substring(0, config.SellerRedis.Length - 2);
                redis += ",defaultDatabase=1";

                var opt = new RedisCacheOptions()
                {
                    Configuration = redis,
                    InstanceName = config.SellerInstant
                };

                return new HaravanCache(opt);
            });
            services.AddSingleton<IElasticClient>(u =>
            {
                var config = u.GetService<IOptions<ElasticConfig>>().Value;
                var settings = new ConnectionSettings(new Uri(config.ESSvr))
                    .DefaultIndex(config.ESIndex)
                    .ConnectionLimit(config.ESConnectionLimit)
                    .RequestTimeout(TimeSpan.FromSeconds(config.ESRequestTimeoutSeconds))
#if DEBUG
                                .PrettyJson()
                    .DisableDirectStreaming()
#endif
                                ;
                return new ElasticClient(settings);
            });
        }

        public override Task ServiceStart(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public override Task ServiceStop(CancellationToken token)
        {
            return Task.CompletedTask;
        }
    }
}