﻿using Haravan.ESB.Shared;
using Microsoft.Extensions.Hosting;

namespace Haravan.Web.Workers
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BusHostExtensions
                .StartUp<MassTransitBusHost>()
                .UseHaravanInsights()
                .Build()
                .Run()
                ;
        }
    }
}
