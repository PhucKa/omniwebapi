﻿using System.Runtime.Serialization;

namespace Haravan.WebHook.Objects.TransferMsg
{
    [DataContract]
    public class AppUninstallTransferMsg
    {
        [DataMember]
        public long org_id { get; set; }
        [DataMember]
        public long user_id { get; set; }
        [DataMember]
        public string client_id { get; set; }
        [DataMember]
        public string event_type { get; set; }
        [DataMember]
        public long app_id { get; set; }
        [DataMember]
        public bool is_salechannel { get; set; }
        [DataMember]
        public bool is_marketing { get; set; }
    }
}
