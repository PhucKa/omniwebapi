﻿using BHN.Core;
using BHN.SharedObject.APIDataModel;
using BHN.SharedObject.EBSMessage;
using BHN.SharedObject.EBSMessage.LogSeller;
using Haravan.Libs.Abstractions;
using Haravan.Web.Api.Business.Extensions;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.ESModels;
using Haravan.Web.Api.BusinessObjects.Mappers;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.BusinessObjects.Models.Article;
using Haravan.Web.Api.BusinessObjects.Models.Common;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using Haravan.Web.Api.Repository;
using Haravan.Web.Api.Utils.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Helpers = Haravan.Web.Api.Utils.Extensions.Helpers;

namespace Haravan.Web.Api.Business
{
    public partial class ArticleBusiness : IArticleBusiness
    {
        private readonly IRequestContext requestContext;
        private readonly IMGArticleRepository rpArticle;
        
        private readonly IMGBlogRepository rpBlog;
        private readonly IKeywordBusiness bizKeyword;
        private readonly IServiceProvider serviceProvider;
        private readonly IUserRepository rpUser;
        private readonly ITagRepository rpTag;
        private readonly IThemeRepository rpTheme;
        private readonly IFilterRepository rpFilter;
        private readonly IMediaRepository rpMedia;
        private readonly IEBSComMessageRepository rpCom;
        private readonly IMGLogDataRepository rpLogData;
        private readonly IMetafieldsEcomRepository rpMetafield;
        private readonly IMGCommentRepository rpComment;
        private readonly IESArticleRepository rpESArticle;
        private readonly IESCommentRepository rpESComment;
        private readonly IExtensionPerRequest exRequestContext;

        private readonly IESArticleSinkRepository rpESArticleSearch;
        private readonly IESCommentSinkRepository rpESCommentSearch;

        public ArticleBusiness(
                IRequestContext requestContext,
                IMGLogDataRepository rpLogData,
                IEBSComMessageRepository rpCom,
                IMGArticleRepository rpArticle,
                IESArticleRepository rpESArticle,
                IServiceProvider serviceProvider,
                IMGBlogRepository rpBlog,
                IUserRepository rpUser,
                ITagRepository rpTag,
                IThemeRepository rpTheme,
                IFilterRepository rpFilter,
                IMediaRepository rpMedia,
                IKeywordBusiness bizKeyword,
                IMetafieldsEcomRepository rpMetafield,
                IMGCommentRepository rpComment,
                IESCommentRepository rpESComment,
                IExtensionPerRequest exRequestContext,
                IESArticleSinkRepository rpESArticleSearch,
                IESCommentSinkRepository rpESCommentSearch
            )
        {
            this.rpLogData = rpLogData;
            this.requestContext = requestContext;
            this.rpArticle = rpArticle;
            this.rpESArticle = rpESArticle;
            this.rpBlog = rpBlog;
            this.rpTag = rpTag;
            this.rpUser = rpUser;
            this.rpTheme = rpTheme;
            this.rpMedia = rpMedia;
            this.rpFilter = rpFilter;
            this.bizKeyword = bizKeyword;
            this.serviceProvider = serviceProvider;
            this.rpCom = rpCom;
            this.rpMetafield = rpMetafield;
            this.rpComment = rpComment;
            this.rpESComment = rpESComment;
            this.exRequestContext = exRequestContext;
            this.rpESArticleSearch = rpESArticleSearch;
            this.rpESCommentSearch = rpESCommentSearch;
        }

        #region Internal

        public async Task<(List<ArticleListModelInternal> data, long totalrecord)> GetArticleList(FilterSearchModel filter)
        {
            var totalRecord = 0L;
            var data = new List<ArticleListModelInternal>();
            var isDefault = IsGetDataFromMongo(filter);
            var dicIds = new Dictionary<int, long>();
            var storeId = exRequestContext.StoreId;
            if (isDefault)
            {
                totalRecord = await rpArticle.CountByStoreAsync(storeId);
                var ids = await rpArticle.GetByPagingAsync(storeId, filter.Page.currentPage, filter.Page.pageSize);
                if (ids != null && ids.Any())
                {
                    for (int i = 0; i < ids.Count; i++)
                    {
                        dicIds.Add(i + 1, ids[i]);
                    }
                }
            }
            else
            {
                (ESSearchResult esResult, long total) = await SearchESArticle(filter);
                totalRecord = total;
                dicIds = esResult.Ids;
            }
            if (dicIds != null && dicIds.Any())
            {
                var result = await GetByDicIds(dicIds);
                data = result.MGArticleToArticleSearchList();
                var ids = dicIds.Select(m => m.Value);
                data.ForEach(r =>
                {
                    if (r.PublishedDate != null && r.PublishedDate.Value <= DateTime.UtcNow)
                        r.IsVisible = true;
                    else
                        r.IsVisible = false;
                });
                var lstSummaries = await rpTag.GetListTag(storeId, new int[] { (int)SummaryType.Article_Tag }.ToArray(), ids.ToArray());
                if (lstSummaries != null && lstSummaries.Any())
                {
                    data.ForEach(r =>
                    {
                        r.Tags = lstSummaries.Where(m => m.RefId == r.Id).Select(m => m.Name).ToList();
                    });
                }
            }
            return (data, totalRecord);
        }

        #endregion Internal

        #region public

        public async Task<DataPaging<List<FileModel>>> GetFileList(string query, int page, int limit)
        {
            return await rpMedia.GetFileList(query, page, limit);
        }

        public async Task<DataPaging<List<FileModel>>> GetProductFeaturedImages(string type, string query, int page, int limit)
        {
            if (string.IsNullOrEmpty(type))
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            switch (type.Trim())
            {
                case "feature":
                    return await rpMedia.GetProductFeaturedImages(query, page, limit);

                default:
                    requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                    return null;
            }
        }

        public async Task<FileModel> UploadArticleImages(IFormFile file)
        {
            FileModel rs = null;
            if (file == null)
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return rs;
            }
            else
            {
                return await rpMedia.UploadArticleImages(file);
            }
        }

        public async Task<bool> DeleteArticleByIdAsync(long articleId)
        {
            return await DeleteArticleAsync(new List<long>() { articleId });
        }

        public async Task<List<string>> GetArticleTags(int type, int limit)
        {
            var data = await rpTag.GetArticleTags(type, limit);
            return data;
        }

        public async Task<bool> AddTags(List<long> articlesIds, List<string> tags)
        {
            var _requestmodel = await CreaDataTag(articlesIds);
            var data = await rpTag.AddTags(_requestmodel, tags);

            var articlrAddTag = await RefineArticlesOfStore(articlesIds);
            if (data)
            {
                foreach (var article in articlrAddTag)
                {
                    article.updated_at = DateTime.UtcNow;
                    article.updated_user = exRequestContext.UserId;
                    var mgpage = await rpArticle.SetAsync(article);
                    await WriteLogDashboard((int)DashboardLogAction.Article_Update, mgpage.ArticleToModelDetail());
                    if (article.published_at != null && article.published_at > DateTime.UtcNow)
                        await rpCom.FireStoreDataChangeAtTime(exRequestContext.OrgId, article.published_at.Value);
                    else
                        await rpCom.FireStoreDataChange(exRequestContext.OrgId);
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<bool> RemoveTags(List<long> articlesIds, List<string> tags)
        {
            var _requestmodel = await CreaDataTag(articlesIds);
            var data = await rpTag.RemoveTags(_requestmodel, tags);

            var articlrRemoveTag = new List<MGArticleModel>();
            articlrRemoveTag = await RefineArticlesOfStore(articlesIds);
            if (data)
            {
                foreach (var article in articlrRemoveTag)
                {
                    article.updated_at = DateTime.UtcNow;
                    article.updated_user = exRequestContext.UserId;
                    var mgpage = await rpArticle.SetAsync(article);
                    await WriteLogDashboard((int)DashboardLogAction.Article_Update, article.ArticleToModelDetail());
                    if (article.published_at != null && article.published_at > DateTime.UtcNow)
                        await rpCom.FireStoreDataChangeAtTime(exRequestContext.OrgId, article.published_at.Value);
                    else
                        await rpCom.FireStoreDataChange(exRequestContext.OrgId);
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<List<DropdownlistModel>> GetAuthorList()
        {
            return await rpUser.GetAuthorList();
        }

        public async Task<List<DropdownlistModel>> GetDropdownlistTheme()
        {
            var list = await rpTheme.GetTemplateArticles();
            var themes = new List<DropdownlistModel>();
            if (list != null)
            {
                var it = list.Select((s, i) => new DropdownlistModel { Id = i, Name = s }).ToList();
                themes.AddRange(it);
            }
            return themes;
        }

        public async Task<bool> PublishArticles(List<long> articlesIds, bool isPublish)
        {
            if (articlesIds == null || !articlesIds.Any())
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return false;
            }
            var ArticlesToPublish = await RefineArticlesOfStore(articlesIds);

            foreach (var article in ArticlesToPublish)
            {
                article.published_at = isPublish ? DateTime.UtcNow : (DateTime?)null;
                article.updated_at = DateTime.UtcNow;
                article.updated_user = exRequestContext.UserId;
                var mgpage = await rpArticle.SetAsync(article);
                await WriteLogDashboard(
                    isPublish == true ? (int)DashboardLogAction.Article_Publish : (int)DashboardLogAction.Article_Hide,
                    mgpage.ArticleToModelDetail());

                if (article.published_at != null && article.published_at > DateTime.UtcNow)
                    await rpCom.FireStoreDataChangeAtTime(exRequestContext.OrgId, article.published_at.Value);
                else
                    await rpCom.FireStoreDataChange(exRequestContext.OrgId);
            }
            return true;
        }

        public async Task<ArticleDetailDropdownlistModel> GetDropdownlistDetail()
        {
            var authors = await rpUser.GetAuthorList();
            var resultModel = new ArticleDetailDropdownlistModel
            {
                AuthorList = authors,
                CurrentUser = exRequestContext.UserId,
                ThemeList = await rpTheme.GetTemplateArticles()
            };
            var blogList = await GetDropdownlistBlog();
            resultModel.BlogList = blogList;
            return resultModel;
        }

        public async Task<(List<ThemeDropdownModel> data, long totalrecord)> GetArticleDropDownListForTheme(
                                                string query,
                                                int page,
                                                int limit
                                                )
        {
            var filter = BuildArticleFilterSearch(false,
                false,
                query,
                null,
                null,
                page,
                limit,
                null,
                null,
                null,
                null,
                false
                );
            var results = await getMGArticleList(filter);
            var lstArticle = new List<ThemeDropdownModel>();
            if (results.articles != null && results.articles.Any())
            {
                lstArticle = results.articles.Select(p => new ThemeDropdownModel
                {
                    Id = p._id,
                    Name = p.title,
                    Handle = p.url_handle,
                    ImageUrl = p.image != null ? p.image.src : null
                }).ToList();
                return (lstArticle, results.totalRecord);
            }
            return (null, 0);
        }

        public async Task<(List<ArticleSearchRowModel> data, long totalrecord)> GetGlobalSearchListArticle(FilterSearchModel filter)
        {
            var totalRecord = 0L;
            var data = new List<ArticleSearchRowModel>();
            var isDefault = IsGetDataFromMongo(filter);
            var dicIds = new Dictionary<int, long>();
            if (isDefault)
            {
                totalRecord = await rpArticle.CountByStoreAsync(exRequestContext.StoreId);
                var ids = await rpArticle.GetByPagingAsync(exRequestContext.StoreId, filter.Page.currentPage, filter.Page.pageSize);
                if (ids != null && ids.Any())
                {
                    for (int i = 0; i < ids.Count; i++)
                    {
                        dicIds.Add(i + 1, ids[i]);
                    }
                }
            }
            else
            {
                if (filter.SortFieldName == null)
                    filter.SortFieldName = "UpdatedDate";
                if (filter.SortType == null)
                    filter.SortType = "desc";

                var rs = await rpESArticleSearch.SearchAsync(exRequestContext.StoreId, filter);
                totalRecord = rs.Total;
                dicIds = rs.Ids;
            }
            if (dicIds != null && dicIds.Any())
            {
                var result = await GetByDicIds(dicIds);
                data = result.ArticleToModelList();
                var ids = dicIds.Select(m => m.Value);
                var lstSummaries = await rpTag.GetListTag(exRequestContext.StoreId, new int[] { (int)SummaryType.Article_Tag }.ToArray(), ids.ToArray());
                if (lstSummaries != null && lstSummaries.Any())
                {
                    data.ForEach(r =>
                    {
                        r.Tags = lstSummaries.Where(m => m.RefId == r.Id).Select(m => m.Name).ToList();
                    });
                }
            }
            return (data, totalRecord);
        }

        public async Task<(List<ArticleSearchRowModel> data, long totalrecord)> GetArticleList(
                                                                            bool visible,
                                                                            bool hidden,
                                                                            string query,
                                                                            string order,
                                                                            string direction,
                                                                            int page,
                                                                            int limit,
                                                                            string blog_ids,
                                                                            string user_ids,
                                                                            string tag_contains,
                                                                            string tag_not_contains,
                                                                            bool tag_not_exist
                                                                      )
        {
            var filter = BuildArticleFilterSearch(
                                                    visible,
                                                    hidden,
                                                    query,
                                                    order,
                                                    direction,
                                                    page,
                                                    limit,
                                                    blog_ids,
                                                    user_ids,
                                                    tag_contains,
                                                    tag_not_contains,
                                                    tag_not_exist
                                                  );

            var totalRecord = 0L;
            var data = new List<ArticleSearchRowModel>();
            var isDefault = IsGetDataFromMongo(filter);
            var dicIds = new Dictionary<int, long>();
            if (isDefault)
            {
                totalRecord = await rpArticle.CountByStoreAsync(exRequestContext.StoreId);
                var ids = await rpArticle.GetByPagingAsync(exRequestContext.StoreId, filter.Page.currentPage, filter.Page.pageSize);
                if (ids != null && ids.Any())
                {
                    for (int i = 0; i < ids.Count; i++)
                    {
                        dicIds.Add(i + 1, ids[i]);
                    }
                }
            }
            else
            {
                if (filter.SortFieldName == null)
                    filter.SortFieldName = "UpdatedDate";
                if (filter.SortType == null)
                    filter.SortType = "desc";

                var rs = await rpESArticleSearch.SearchAsync(exRequestContext.StoreId, filter);
                totalRecord = rs.Total;
                dicIds = rs.Ids;
            }
            if (dicIds != null && dicIds.Any())
            {
                var result = await GetByDicIds(dicIds);
                data = result.ArticleToModelList();
                var ids = dicIds.Select(m => m.Value);
                var lstSummaries = await rpTag.GetListTag(exRequestContext.StoreId, new int[] { (int)SummaryType.Article_Tag }.ToArray(), ids.ToArray());
                if (lstSummaries != null && lstSummaries.Any())
                {
                    data.ForEach(r =>
                    {
                        r.Tags = lstSummaries.Where(m => m.RefId == r.Id).Select(m => m.Name).ToList();
                    });
                }
            }
            return (data, totalRecord);
        }

        public async Task UploadArticleImage(long articleId, string fileName, Stream fileStream)
        {
            var storeId = exRequestContext.StoreId;
            var objArticle = await rpArticle.GetById(storeId, articleId);
            if (objArticle == null)
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return;
            }
            var oldImageUrl = string.Empty;
            if (objArticle.image != null)
            {
                oldImageUrl = objArticle.image.src;
            }
            var bizFile = serviceProvider.GetService<IFileBusiness>();
            await UploadArticleImageFromStream(objArticle, fileStream, fileName, bizFile);
            await RemoveOldArticleImage(bizFile, oldImageUrl);
        }

        public async Task<ArticleDetailModel> AddNewAsync(ArticleDetailModel model)
        {
            if (model == null)
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            model.Tags = StringHelper.IsValidSummaryData(model.Tags);
            var validate = await ValidateModel(model, false);
            DashboardLogAction logAction = validate.Item2;
            var newArticle = validate.Item1;
            if (newArticle == null)
                return null;
            newArticle.storeid = exRequestContext.StoreId;
            newArticle.created_at = DateTime.UtcNow;
            newArticle.created_user = exRequestContext.UserId;
            newArticle.updated_at = DateTime.UtcNow;
            newArticle.updated_user = exRequestContext.UserId;
            newArticle.tags = string.IsNullOrEmpty(model.Tags) ? null : model.Tags.Split(',');

            var blog = await rpBlog.GetById(exRequestContext.StoreId, newArticle.blogid);
            if (string.IsNullOrWhiteSpace(newArticle.url_handle))
                newArticle.url_handle = await bizKeyword.ProcessingKeyHandleWithRefId(exRequestContext.StoreId, (int)EnumDocumentType.Article, null, newArticle.title.Trim(), -1, newArticle.blogid);
            else
                newArticle.url_handle = await bizKeyword.ProcessingKeyHandleWithRefId(exRequestContext.StoreId, (int)EnumDocumentType.Article, null, newArticle.url_handle.Trim(), -1, newArticle.blogid);

            if (newArticle.blogtitle == null && newArticle.blogid != 0)
            {
                newArticle.blogtitle = blog.title;
                newArticle.bloghandleurl = blog.handle;
                newArticle.blogcommentrule = blog.commentrule;
            }
            newArticle.url = blog.handle;
            newArticle.moderated = blog.commentrule != 3 ? true : false;
            if (newArticle.authorid != null)
            {
                if (newArticle.authorid.HasValue)
                {
                    var objUser = await rpUser.GetByIds(newArticle.authorid.Value);
                    if (objUser != null)
                    {
                        newArticle.author = StringHelper.JoinFullName(objUser.LastName, objUser.FirstName);
                        if (newArticle.user != null)
                        {
                            newArticle.user.bio = objUser.Bio != null ? objUser.Bio : string.Empty;
                            newArticle.user.email = objUser.Email;
                            newArticle.user.first_name = objUser.FirstName;
                            newArticle.user.last_name = objUser.LastName;
                            newArticle.user.homepage = objUser.Homepage;
                            newArticle.user.storeid = exRequestContext.StoreId;
                            newArticle.user.account_owner = newArticle.authorid.Value == objUser.User_Account_Owner;
                            newArticle.user._id = objUser.Id;
                        }
                        else
                        {
                            var mgUserModel = new MGUserModel()
                            {
                                bio = objUser.Bio != null ? objUser.Bio : string.Empty,
                                email = objUser.Email,
                                first_name = objUser.FirstName,
                                last_name = objUser.LastName,
                                homepage = objUser.Homepage,
                                storeid = exRequestContext.StoreId,
                                _id = objUser.Id
                            };
                            newArticle.user = mgUserModel;
                        }
                    }
                }
                else
                    newArticle.user.account_owner = false;
            }

            if (!string.IsNullOrEmpty(model.Tags))
                newArticle.tagsformated = Helpers.ParseTagsUrl(newArticle.tags.ToArray());

            var mgArticle = await rpArticle.SetAsync(newArticle);
            if (!StringHelper.CheckStringIsNullOrEmptyOrWhitespace(model.Tags))
            {
                var tags = model.Tags.Split(',');

                await rpTag.SetSummaryNames(SummaryType.Article_Tag, newArticle._id, tags.ToList());
                await rpTag.AddSummaryNamesForRefIds(SummaryType.Blog_ArticlesTag,
                    new[] { newArticle.blogid }, tags.ToList());
            }
            if (model.ImageId == null || model.ImageId <= 0)
            {
                if (!string.IsNullOrWhiteSpace(model.ImageUrl))
                {
                    await UploadArticleImageFromUrl(mgArticle, model.ImageUrl, model.ImageAttactmentFilename);
                }
                else if (!string.IsNullOrWhiteSpace(model.ImageAttactment) || string.IsNullOrWhiteSpace(model.ImageAttactmentFilename))
                {
                    await UploadArticleImageFromAttactment(mgArticle, model.ImageAttactment, model.ImageAttactmentFilename);
                }
            }

            var data = await GetDetail(mgArticle._id);
            await WriteLogDashboard((int)logAction, data);
            await rpCom.FireStoreDataChange(exRequestContext.OrgId);
            return data;
        }

        public async Task<ArticleDetailModel> UpdateAsync(ArticleDetailModel model)
        {            
            var validate = await ValidateModel(model, true);
            var article = validate.Item1;
            var logAction = validate.Item2;
            long oldBlogId = validate.Item3;
            if (article == null)
                return null;

            var existArticle = await rpArticle.GetById(exRequestContext.StoreId, model.Id);
            if (existArticle == null)
            {
                requestContext.AddError("errors.article.not_article_not_update", "Không tìm thấy bài viết. Hủy thao tác cập nhật");
                return null;
            }
            var blog = await rpBlog.GetById(exRequestContext.StoreId, article.blogid);
            if (article.blogtitle == null && article.blogid != 0)
                article.blogtitle = blog.title;

            article.bloghandleurl = blog.handle;
            article.blogcommentrule = blog.commentrule;
            article.moderated = blog.commentrule != 3 ? true : false;

            article.tags = string.IsNullOrEmpty(model.Tags) ? null : model.Tags.Split(',');
            article.url = blog.handle; // ngoai buyer

            if (!string.IsNullOrEmpty(model.Tags))
                article.tagsformated = Helpers.ParseTagsUrl(article.tags.ToArray());

            #region Save Tags

            List<string> inputTag = new List<string>();
            if (!string.IsNullOrEmpty(model.Tags))
            {
                var tempTags = model.Tags.Split(',');
                for (int i = 0; i < tempTags.Length; i++)
                {
                    if (!inputTag.Contains(tempTags[i].Trim()))
                        inputTag.Add(tempTags[i].Trim());
                }
            }

            var oldTags = await rpTag.GetSummaryNames(SummaryType.Article_Tag, model.Id);
            await rpTag.SetSummaryNames(SummaryType.Article_Tag, model.Id, inputTag.Count == 0 ? null : inputTag);
            List<string> removeTag = null, newTag = null;
            long blogIdToRemove = article.blogid;
            if (oldBlogId != article.blogid)
            {
                blogIdToRemove = oldBlogId;
                removeTag = inputTag;
                newTag = inputTag;
                await RemoveTagFromBlog(new List<long>() { blogIdToRemove }, oldTags);
                await rpTag.AddSummaryNamesForRefIds(SummaryType.Blog_ArticlesTag,
                    new[] { model.BlogId }, newTag);
            }
            else
            {
                if (inputTag != null)
                {
                    removeTag = oldTags.Where(a => !inputTag.Contains(a)).ToList();
                    newTag = inputTag.Where(a => !oldTags.Contains(a)).ToList();
                    await RemoveTagFromBlog(new List<long>() { model.BlogId }, removeTag);
                    await rpTag.AddSummaryNamesForRefIds(SummaryType.Blog_ArticlesTag,
                    new[] { model.BlogId }, newTag);
                }
            }
            // update tag cho blog

            #endregion Save Tags

            string oldUrlPath = existArticle.url + "/" + existArticle.url_handle;
            string newUrlPath = article.url + "/" + article.url_handle;
            if (model.IsChangeUrlRedirect || (oldUrlPath.Trim() != newUrlPath.Trim()))
            {                
                var _UrlRedirect = serviceProvider.GetRequiredService<INavigationBusiness>();
                await _UrlRedirect.AddOrUpdateArticleUrlRedirect(oldUrlPath, newUrlPath);
            }
            var data = await _priUpdateArticle(article);
            await WriteLogDashboard((int)logAction, data);

            if (article.published_at != null && article.published_at > DateTime.UtcNow)
                await rpCom.FireStoreDataChangeAtTime(exRequestContext.OrgId, article.published_at.Value);
            else
                await rpCom.FireStoreDataChange(exRequestContext.OrgId);
            return data;
        }

        public async Task<bool> DeleteArticleAsync(List<long> articleIds)
        {
            if (articleIds == null || !articleIds.Any())
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return false;
            }
            foreach (var id in articleIds)
            {
                await DeleteArticleById(id);
            }
            return true;
        }

        private async Task<bool> DeleteArticleById(long articleId)
        {
            var storeId = exRequestContext.StoreId;
            var article = await rpArticle.GetById(storeId, articleId);
            if (article == null)
                return true;

            var articleIds = new long[] { articleId };
            var lstSummaries = await rpTag.GetListTag(storeId, new int[] { (int)SummaryType.Article_Tag }.ToArray(), articleIds.ToArray());
            var blogId = article.blogid;
            await rpComment.DeleteByIdArticleAsync(storeId, articleId);
            await rpESComment.DeleteByArticleIdAsync(storeId, articleId);
            if (lstSummaries != null && lstSummaries.Any())
            {
                var tags = lstSummaries.Select(m => m.Name);
                if (tags != null && tags.Any())
                {
                    await rpTag.RemoveSummaryNamesForRefIds(SummaryType.Article_Tag, new long[] { articleId }, tags.ToList());
                    await RemoveTagFromBlog(new List<long>() { blogId }, tags.ToList());
                }
            }
            await bizKeyword.ProcessingKeyHandleWithRefId(storeId, (int)EnumDocumentType.Article, article.url_handle, null, article.blogid, article.blogid);
            await _priDelete(article);
            await rpMetafield.DeleteByOnwerIdResourceType(articleId, (int)MetafieldResourceOwner.Article);
            var deleteImageUrls = article.image?.src;
            if (!string.IsNullOrWhiteSpace(deleteImageUrls))
            {
                var bizFile = serviceProvider.GetService<IFileBusiness>();
                await bizFile.DeleteImages(new List<string>() { deleteImageUrls });
            }
            await WriteLogDashboard((int)DashboardLogAction.Article_Delete, article.ArticleToModelDetail());
            await rpCom.FireStoreDataChange(storeId);
            return true;
        }

        private ArticleDetailModel ToDetailModel(MGArticleModel objArticleModel, MGBlogModel objBlog, List<string> lstTags)
        {
            if (objArticleModel.authorid == null || objArticleModel.authorid <= 0)
            {
                if (objArticleModel.user != null)
                {
                    objArticleModel.authorid = objArticleModel.user._id;
                    objArticleModel.author = objArticleModel.user.name;
                }
            }
            if (string.IsNullOrWhiteSpace(objArticleModel.template_suffix))
                objArticleModel.template_suffix = TemplateName.Article;
            else
                objArticleModel.template_suffix = TemplateName.Article + "." + objArticleModel.template_suffix;
            var resultModel = objArticleModel.ArticleToModelDetail();
            resultModel.BlogTitle = objBlog.title;
            resultModel.BlogHandleUrl = objBlog.handle;
            resultModel.BlogCommentRule = objBlog.commentrule;
            if (string.IsNullOrEmpty(resultModel.OldUrlHandle))
            {
                resultModel.ViewUrl = string.Format("{0}-{1}",
                    resultModel.Id,
                    StringHelper.CreateFriendlyURL(resultModel.Title));
            }
            if (resultModel.PublishedDate != null && resultModel.PublishedDate.Value <= DateTime.UtcNow)
                resultModel.IsVisible = true;
            else
                resultModel.IsVisible = false;

            if (lstTags != null && lstTags.Any())
            {
                resultModel.Tags = string.Join(",", lstTags);
            }
            return resultModel;
        }

        public async Task<ArticleDetailModel> GetArticleDetail(long blogId, long articleId)
        {
            var storeId = exRequestContext.StoreId;
            var objArticle = await rpArticle.GetBy(storeId, blogId, articleId);
            if (objArticle == null)
            {
                requestContext.AddError("errors.article.not_exist", "Bài viết không tồn tại");
                return null;
            }
            var objBlog = await rpBlog.GetById(storeId, blogId);
            if (objBlog == null)
            {
                requestContext.AddError("errors.blog.not_exist", "Blog không tồn tại");
                return null;
            }

            var lstTags = await rpTag.GetSummaryNames((int)SummaryType.Article_Tag, articleId, storeId);
            return ToDetailModel(objArticle, objBlog, lstTags);
        }

        private async Task<List<ArticleDetailModel>> GetDetailBy(List<long> articleIds)
        {
            var storeId = exRequestContext.StoreId;
            var lstArticles = await rpArticle.GetBy(storeId, articleIds);
            if (lstArticles == null || !lstArticles.Any())
            {
                requestContext.AddError("errors.article.not_exist", "Bài viết không tồn tại");
                return null;
            }
            var lstBlogIds = lstArticles.Select(m => m.blogid).Distinct();
            var lstBlogs = await rpBlog.GetById(storeId, lstBlogIds.ToList());
            if (lstBlogs == null || !lstBlogs.Any())
            {
                requestContext.AddError("errors.blog.not_exist", "Blog không tồn tại");
                return null;
            }
            var lstTags = await rpTag.GetListTag(storeId, new List<int>() { (int)SummaryType.Article_Tag }.ToArray(), articleIds.ToArray());
            var result = new List<ArticleDetailModel>();
            foreach (var objArticle in lstArticles)
            {
                var objBlog = lstBlogs.FirstOrDefault(m => m._id == objArticle.blogid);
                if (objBlog == null)
                {
                    requestContext.AddError("errors.blog.not_exist", "Blog không tồn tại");
                    return null;
                }
                var tags = new List<string>();
                if (lstTags != null && lstTags.Any())
                {
                    tags = lstTags.Where(m => m.RefId == objArticle._id).Select(m => m.Name).ToList();
                }

                var objModel = ToDetailModel(objArticle, objBlog, tags);
                result.Add(objModel);
            }

            return result;
        }

        public async Task<ArticleDetailModel> GetDetail(long id)
        {
            var storeId = exRequestContext.StoreId;
            var objArticle = await rpArticle.GetById(storeId, id);
            if (objArticle == null)
            {
                requestContext.AddError("errors.article.not_exist", "Bài viết không tồn tại");
                return null;
            }
            var objBlog = await rpBlog.GetById(storeId, objArticle.blogid);
            if (objBlog == null)
            {
                requestContext.AddError("errors.blog.not_exist", "Blog không tồn tại");
                return null;
            }
            var articleTags = await rpTag.GetSummaryNames((int)SummaryType.Article_Tag, objArticle._id, storeId);

            return ToDetailModel(objArticle, objBlog, articleTags);
        }

        public async Task<ArticleDetailModel> GetArticleFirstByHandle()
        {
            var resultModel = new ArticleDetailModel();
            var article = await rpArticle.GetFirstByHandle(exRequestContext.StoreId);
            if (article == null)
            {
                requestContext.AddError("errors.article.not_exist", "Bài viết không tồn tại");
                return null;
            }
            resultModel = article.ArticleToModelDetail();
            return resultModel;
        }

        public async Task<List<DropdownlistModel>> GetDropdownlistBlog()
        {
            var _model = await rpBlog.GetBlogsByStoreId(exRequestContext.StoreId);
            _model = _model.OrderBy(m => m.updated_at).ToList();
            var resultModel = new DropdownlistModel();
            var blogList = _model.BlogDropdownlistToModelList();
            blogList.Add(new DropdownlistModel()
            {
                Id = -1,
                IsDisable = true,
                Name = "-----"
            });
            blogList.Add(new DropdownlistModel()
            {
                Id = 0,
                IsDisable = false,
                Name = "Tạo blog mới"
            });
            return blogList;
        }

        #endregion public

        #region private

        private async Task<ArticleDetailModel> ParseDictionaryArticleApi(Dictionary<string, object> dictionaryModel, long article_id = 0)
        {
            if (dictionaryModel == null || dictionaryModel.Count == 0)
            {
                requestContext.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return null;
            }

            ArticleDetailModel resultModel;

            if (article_id <= 0)
            {
                resultModel = new ArticleDetailModel()
                {
                    Id = article_id,
                    PublishedDate = DateTime.UtcNow,
                    TemplateName = TemplateName.Article
                };
            }
            else
            {
                resultModel = await GetDetail(article_id);
                if (requestContext.IsError || resultModel == null)
                {
                    return null;
                }
            }

            var propertyInfos = typeof(ArticleAPIModel).GetProperties();
            foreach (var propertyValuePair in dictionaryModel)
            {
                var colName = propertyInfos.Single(x => x.Name == propertyValuePair.Key);
                if (colName.Name.Equals("title"))
                {
                    if (article_id <= 0 && string.IsNullOrEmpty((string)propertyValuePair.Value))
                    {
                        requestContext.AddError("errors.article.title.is_empty", "Tiêu đề bài viết không được bỏ trống");
                        return null;
                    }
                    else
                    {
                        string strTitle = HtmlUtility.Instance.SanitizeHtml((string)propertyValuePair.Value, HtmlUtility.ValidHtmlTags.Article);
                        resultModel.Title = strTitle;
                    }
                }
                if (colName.Name.Equals("handle"))
                {
                    resultModel.UrlHandle = (string)propertyValuePair.Value;
                }
                if (colName.Name.Equals("author"))
                {
                    resultModel.AuthorName = (string)propertyValuePair.Value;
                }
                if (colName.Name.Equals("blog_id"))
                {
                    resultModel.BlogId = (long)propertyValuePair.Value;
                }
                if (colName.Name.Equals("body_html"))
                {
                    var strContent = propertyValuePair.Value?.ToString().Trim();
                    if (strContent == null || String.IsNullOrEmpty(strContent))
                        strContent = string.Empty;
                    var santilizeContent = HtmlUtility.Instance.SanitizeHtml(strContent, HtmlUtility.ValidHtmlTags.Article);
                    if (!string.IsNullOrWhiteSpace(santilizeContent))
                    {
                        if (santilizeContent.Trim().Length > DefaultData.DefaultMaxPageDescriptionLength)
                        {
                            requestContext.AddError("errors.article.content.too_long", "Nội dung quá dài");
                            return null;
                        }
                    }
                    resultModel.Content = santilizeContent;
                }
                if (colName.Name.Equals("template_suffix"))
                {
                    if (!string.IsNullOrEmpty((string)propertyValuePair.Value))
                    {
                        resultModel.TemplateName = (string)propertyValuePair.Value;
                    }
                    else
                    {
                        resultModel.TemplateName = string.Empty;
                    }
                }

                if (colName.Name.Equals("published"))
                {
                    var isPublish = bool.Parse(propertyValuePair.Value.ToString());
                    resultModel.PublishedDate = isPublish ? (DateTime?)DateTime.UtcNow : null;
                }
                if (colName.Name.Equals("image"))
                {
                    if (propertyValuePair.Value == null)
                    {
                        resultModel.ImageId = 0;
                        resultModel.ImageUrl = string.Empty;
                    }
                    else
                    {
                        var objImg = JsonConvert.DeserializeObject<ArticleImageAPIModel>(JsonConvert.SerializeObject(propertyValuePair.Value));
                        if (objImg != null)
                        {
                            var rsImg = await _checkArticleImageApi(objImg.src, objImg.attachment, objImg.filename, resultModel.ImageUrl, resultModel.ImageId);
                            if (rsImg != null)
                            {
                                resultModel.ImageId = rsImg.Item1;
                                resultModel.ImageUrl = rsImg.Item2;
                            }
                        }
                    }
                }
            }

            return resultModel;
        }

        private async Task RemoveOldArticleImage(IFileBusiness bizFile, string oldImageUrl)
        {
            if (!StringHelper.CheckStringIsNullOrEmptyOrWhitespace(oldImageUrl))
            {
                await bizFile.DeleteImages(new List<string>() { oldImageUrl });
            }
        }

        private async Task UploadArticleImageFromStream(MGArticleModel objArticle, Stream stream, string fileName, IFileBusiness bizFile)
        {
            var objFile = await bizFile.UploadArticleFeatureImage(objArticle.storeid, fileName, stream);
            if (objFile != null)
            {
                objArticle.image = new MGArticleImageModel()
                {
                    src = objFile.Url
                };
                objArticle.updated_at = DateTime.UtcNow;
                objArticle.updated_user = exRequestContext.UserId;
                await rpArticle.UpdateAsync(objArticle, m => m.image, m => m.updated_at, m => m.updated_user);
            }
        }

        private async Task UploadArticleImageFromUrl(MGArticleModel objArticle, string imageUrl, string fileName)
        {
            if (!StringHelper.CheckStringIsNullOrEmptyOrWhitespace(imageUrl))
            {
                var oldImageUrl = string.Empty;
                if (objArticle.image != null)
                {
                    oldImageUrl = objArticle.image.src;
                }

                var bizFile = serviceProvider.GetService<IFileBusiness>();
                var objFile = await bizFile.AddImageFromUrl(imageUrl, fileName, FileType.ArticleFeatureImage, objArticle.storeid);
                if (objFile != null)
                {
                    objArticle.image = new MGArticleImageModel()
                    {
                        src = objFile.Url
                    };
                    objArticle.updated_at = DateTime.UtcNow;
                    objArticle.updated_user = exRequestContext.UserId;
                    await rpArticle.UpdateAsync(objArticle, m => m.image, m => m.updated_at, m => m.updated_user);
                }
                await RemoveOldArticleImage(bizFile, oldImageUrl);
            }
        }

        private async Task UploadArticleImageFromAttactment(MGArticleModel objArticle, string attactment, string fileName)
        {
            if (!StringHelper.CheckStringIsNullOrEmptyOrWhitespace(attactment))
            {
                var oldImageUrl = string.Empty;
                if (objArticle.image != null)
                {
                    oldImageUrl = objArticle.image.src;
                }

                var bizFile = serviceProvider.GetService<IFileBusiness>();
                var objFile = await bizFile.AddImageFromBase64String(attactment, fileName, FileType.ArticleFeatureImage, objArticle.storeid);
                if (objFile != null)
                {
                    objArticle.image = new MGArticleImageModel()
                    {
                        src = objFile.Url
                    };
                    objArticle.updated_at = DateTime.UtcNow;
                    objArticle.updated_user = exRequestContext.UserId;
                    await rpArticle.UpdateAsync(objArticle, m => m.image, m => m.updated_at, m => m.updated_user);
                }
                await RemoveOldArticleImage(bizFile, oldImageUrl);
            }
        }

        private async Task<Tuple<long, string>> _checkArticleImageApi(string src, string attactment, string filename, string oldSrc, long? oldId)
        {
            if (string.IsNullOrWhiteSpace(src) && string.IsNullOrWhiteSpace(attactment) && string.IsNullOrWhiteSpace(filename))
                return new Tuple<long, string>(0, string.Empty);
            var bizFile = serviceProvider.GetService<IFileBusiness>();
            if (!string.IsNullOrWhiteSpace(src))
            {
                if (!string.IsNullOrWhiteSpace(oldSrc))
                {
                    if (src.Trim() == oldSrc)
                        return new Tuple<long, string>(oldId.Value, oldSrc);
                }
                var objFileModel = await bizFile.AddImageFromUrl(src, filename, FileType.ArticleFeatureImage, exRequestContext.StoreId);
                if (objFileModel != null) return new Tuple<long, string>(objFileModel.Size, objFileModel.Url);
            }
            else
            {
                if (string.IsNullOrWhiteSpace(attactment) || string.IsNullOrWhiteSpace(filename))
                    return new Tuple<long, string>(0, string.Empty);
                var objFileModel = await bizFile.AddImageFromBase64String(attactment, filename, FileType.ArticleFeatureImage, exRequestContext.StoreId);
                if (objFileModel != null)
                    return new Tuple<long, string>(objFileModel.Size, objFileModel.Url);
            }
            return new Tuple<long, string>(0, string.Empty);
        }

        private async Task _priDelete(MGArticleModel model)
        {
            await rpArticle.DeleteByIdAsync(exRequestContext.OrgId, model._id);
            await rpESArticle.RemoveAsync(exRequestContext.OrgId, model._id);
            var logmodel = new MGLogData()
            {
                created_date = DateTime.UtcNow,
                created_user = exRequestContext.UserId,
                data = JsonConvert.SerializeObject(model.ArticleToModelDetail()),
                refid = model._id,
                storeid = model.storeid,
                doctypeid = (int)EnumDocumentType.Article
            };

            await rpLogData.PostLog(logmodel);
        }

        private async Task<(List<MGArticleModel> articles, long totalRecord)> getMGArticleList(FilterSearchModel filter)
        {
            var totalRecord = 0L;
            var data = new List<MGArticleModel>();
            var isDefault = IsGetDataFromMongo(filter);
            var dicIds = new Dictionary<int, long>();
            if (isDefault)
            {
                totalRecord = await rpArticle.CountByStoreAsync(exRequestContext.StoreId);
                var ids = await rpArticle.GetByPagingAsync(exRequestContext.StoreId, filter.Page.currentPage, filter.Page.pageSize);
                if (ids != null && ids.Any())
                {
                    for (int i = 0; i < ids.Count; i++)
                    {
                        dicIds.Add(i + 1, ids[i]);
                    }
                }
            }
            else
            {
                if (filter.SortFieldName == null)
                {
                    filter.SortFieldName = "UpdatedDate";
                }
                if (filter.SortType == null)
                {
                    filter.SortType = "desc";
                }

                var rs = await rpESArticleSearch.SearchAsync(exRequestContext.StoreId, filter);
                totalRecord = rs.Total;
                dicIds = rs.Ids;
            }
            if (dicIds == null || !dicIds.Any())
                return (null, 0);
            var result = await GetByDicIds(dicIds);
            return (result, totalRecord);
        }

        private bool IsGetDataFromMongo(FilterSearchModel model)
        {
            return string.IsNullOrWhiteSpace(model.FreeText)
                   && (model.Fields == null || !model.Fields.Any())
                   && string.IsNullOrWhiteSpace(model.SortFieldName)
                   && string.IsNullOrWhiteSpace(model.SortType);
        }

        private async Task RemoveTagFromBlog(List<long> blogIds, List<string> tags)
        {
            if (blogIds == null || blogIds.Count == 0 || tags == null || tags.Count == 0)
                return;
            foreach (var blogId in blogIds)
            {
                var listArticleTags = await rpTag.GetGroupArticleTags(blogId);
                var removeTags = tags.Where(a => !listArticleTags.Contains(a)).ToList();
                await rpTag.RemoveSummaryNamesForRefIds(SummaryType.Blog_ArticlesTag, new[] { blogId }, removeTags);
            };
        }

        private async Task<List<MGArticleModel>> RefineArticlesOfStore(List<long> articleIds)
        {
            var articlesOfStore = new List<MGArticleModel>();
            if (articleIds == null || !articleIds.Any())
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var storeId = exRequestContext.StoreId;
            var articles = await rpArticle.GetBy(storeId, articleIds);
            if (articles == null || !articles.Any())
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            foreach (var article in articleIds)
            {
                var item = await rpArticle.GetById(storeId, article);
                var articleTags = await rpTag.GetSummaryNames((int)SummaryType.Article_Tag, article, exRequestContext.StoreId);
                item.tags = articleTags.ToArray();

                if (item == null || item.isdeleted == true)
                {
                    requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                }
                else
                {
                    articlesOfStore.Add(item);
                }
            }
            return articlesOfStore;
        }

        private async Task<ArticleDetailModel> _priUpdateArticle(MGArticleModel newArticle)
        {
            newArticle.storeid = exRequestContext.StoreId;
            newArticle.updated_at = DateTime.UtcNow;
            newArticle.updated_user = exRequestContext.UserId;
            var mgarticle = await rpArticle.SetAsync(newArticle);
            return mgarticle.ArticleToModelDetail();
        }

        private async Task<(MGArticleModel, DashboardLogAction, long, long?)> ValidateModel(ArticleDetailModel model, bool isUpdate, bool isKeepId = false)
        {
            DashboardLogAction logAction;
            long oldBlogId = 0L;
            long? oldImageId = null;
            logAction = isUpdate ? DashboardLogAction.Article_Update : DashboardLogAction.Article_Create;
            oldBlogId = model.BlogId;
            oldImageId = model.ImageId;
            var bizblog = serviceProvider.GetService<IBlogBusiness>();
            if (model == null)
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return (null, logAction, oldBlogId, oldImageId);
            }
            if (string.IsNullOrEmpty(model.NewBlog) && model.BlogId <= 0)
            {
                requestContext.AddError("errors.blog.title.is_empty", "Tiêu đề blog không được bỏ trống");
            }
            else if (model.BlogId <= 0)
            {
                var _blogmodel = await bizblog.AddNewAsync(new BlogDetailModel { Title = model.NewBlog });
                model.BlogId = _blogmodel.Id;
                model.BlogTitle = model.NewBlog;
                model.BlogHandleUrl = _blogmodel.UrlHandle;
            }
            if (model.Tags == null)
                model.Tags = string.Empty;

            if (!StringHelper.CheckStringIsNullOrEmptyOrWhitespace(model.Content))
            {
                model.Content = HtmlUtility.Instance.SanitizeHtml(model.Content, HtmlUtility.ValidHtmlTags.Article);
                if (model.Content.Trim().Length > DefaultData.DefaultMaxArticleDescriptionLength)
                {
                    requestContext.AddError("errors.article.content.too_long", "Nội dung quá dài");
                    return (null, logAction, oldBlogId, oldImageId);
                }
            }
            if (!StringHelper.CheckStringIsNullOrEmptyOrWhitespace(model.Excerpt))
            {
                model.Excerpt = HtmlUtility.Instance.SanitizeHtml(model.Excerpt, HtmlUtility.ValidHtmlTags.Article);
                if (model.Excerpt.Trim().Length > DefaultData.DefaultMaxArticleDescriptionLength)
                {
                    requestContext.AddError("errors.article.excerpt.too_long", "Trích dẫn quá dài");
                    return (null, logAction, oldBlogId, oldImageId);
                }
            }
            var strMetaDescription = model.MetaDescription.StripHtml();
            if (StringHelper.CheckStringIsNullOrEmptyOrWhitespace(strMetaDescription))
            {
                if (!StringHelper.CheckStringIsNullOrEmptyOrWhitespace(HtmlStringHelpers.StripHtml(model.Content)))
                {
                    strMetaDescription = HtmlStringHelpers.StripHtml(model.Content);
                    if (strMetaDescription.Length > DefaultData.DefaultMaxMetaDescription)
                        model.MetaDescription = strMetaDescription.Substring(0, DefaultData.DefaultMaxMetaDescription);
                    else
                        model.MetaDescription = strMetaDescription;
                }
                else
                    model.MetaDescription = strMetaDescription;
            }
            else
            {
                if (strMetaDescription.Length > DefaultData.DefaultMaxMetaDescription)
                    model.MetaDescription = strMetaDescription.Substring(0, DefaultData.DefaultMaxMetaDescription);
                else
                    model.MetaDescription = strMetaDescription;
            }
            if (string.IsNullOrEmpty(model.Title))
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return (null, logAction, oldBlogId, oldImageId);
            }
            model.Title = model.Title.Trim();
            if (model.Title.Length > 255 || model.Title.Length <= 0)
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return (null, logAction, oldBlogId, oldImageId);
            }            
            if (string.IsNullOrEmpty(model.PageTitle))
            {
                var title = model.Title.Trim();
                model.PageTitle = title.Length <= 70 ? title
                                    : title.Substring(0, 70);
            }
            else if (model.PageTitle.Length > 70)
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return (null, logAction, oldBlogId, oldImageId);
            }
            else
                model.PageTitle = model.PageTitle.Trim();

            if (!string.IsNullOrEmpty(model.AuthorName))
            {
                model.AuthorName = model.AuthorName.Trim();
                if (model.AuthorName.Length > 100)
                {
                    requestContext.AddError("errors.article.name_author.too_long", "Tên tác giả quá dài.");
                    return (null, logAction, oldBlogId, oldImageId);
                }
            }
            else
                model.AuthorName = exRequestContext.UserName;

            if (model.TemplateName == TemplateName.Article || string.IsNullOrWhiteSpace(model.TemplateName))
                model.TemplateName = string.Empty;
            else
                model.TemplateName = model.TemplateName.Replace(TemplateName.Article + ".", string.Empty);

            string oldUrlHandle = string.Empty;
            if (isUpdate)
            {
                if (model.Id <= 0)
                {
                    requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                    return (null, logAction, oldBlogId, oldImageId);
                }
                var article = await rpArticle.GetById(exRequestContext.StoreId, model.Id);
                if (article == null)
                {
                    requestContext.AddError("errors.article.not_article_not_update", "Không tìm thấy bài viết. Hủy thao tác cập nhật");
                    return (null, logAction, oldBlogId, oldImageId);
                }
                oldUrlHandle = article.url_handle;
                oldBlogId = article.blogid;
                if (StringHelper.CheckStringIsNullOrEmptyOrWhitespace(model.MetaDescription))
                    model.MetaDescription = article.meta_description;

                var isVisible = !model.PublishedDate.HasValue || model.PublishedDate.Value <= DateTime.UtcNow;
                var isPublishDateChange = (article.published_at.HasValue != model.PublishedDate.HasValue)
                                          || (model.PublishedDate.HasValue &&
                                                model.PublishedDate.Value != article.published_at.Value)
                                          ? true : false;
                model.CreatedDate = article.created_at;
                model.Created_user = article.created_user;                
                if (isPublishDateChange)
                {
                    switch (isVisible)
                    {
                        case true:
                            logAction = DashboardLogAction.Article_Publish;
                            break;

                        case false:
                            logAction = DashboardLogAction.Article_Hide;
                            break;
                    }
                }
            }
            else
            {
                if (isKeepId == false) model.Id = 0;
                if (model.IsSetPublishDate)
                {
                    if (model.PublishedDate == null)
                        model.PublishedDate = DateTime.UtcNow;
                }
            }

            if (model.PublishedDate != null && model.PublishedDate.Value.ToUniversalTime() <= DateTime.UtcNow)
                model.IsVisible = true;
            else
                model.IsVisible = false;

            var rs = model.ArticleToToModelMG();
            if (isUpdate)
            {
                if (string.IsNullOrWhiteSpace(rs.url_handle))
                {
                    rs.url_handle = await bizKeyword.ProcessingKeyHandleWithRefId(exRequestContext.StoreId, (int)EnumDocumentType.Article,
                                                                   oldUrlHandle, rs.title, oldBlogId, rs.blogid);
                }
                else
                {
                    rs.url_handle = await bizKeyword.ProcessingKeyHandleWithRefId(exRequestContext.StoreId, (int)EnumDocumentType.Article,
                                                             oldUrlHandle, rs.url_handle, oldBlogId, rs.blogid);
                }
            }
            return (rs, logAction, oldBlogId, oldImageId);
        }

        private FilterSearchModel BuildArticleFilterSearch(
                                                           bool visible,
                                                           bool hidden,
                                                           string query,
                                                           string order,
                                                           string direction,
                                                           int page,
                                                           int limit,
                                                           string blog_ids,
                                                           string user_ids,
                                                           string tag_contains,
                                                           string tag_not_contains,
                                                           bool tag_not_exist
                                                           )
        {
            var filter = new FilterSearchModel()
            {
                FreeText = query,
                Page = new FilterSearchPage()
                {
                    currentPage = page <= 0 ? 1 : page,
                    pageSize = limit <= 0 || limit > 50 ? 20 : limit
                },
                SortFieldName = PrepareSortFieldName(order),
                SortType = PrepareSortType(direction),
                Fields = new List<FilterSearchField>()
            };
            if (visible)
                filter = PreparePublishStatusFilter(filter, true);

            if (hidden)
                PreparePublishStatusFilter(filter, false);

            if (!StringHelper.CheckStringIsNullOrEmptyOrWhitespace(blog_ids))
                filter = PrepareBlogFilter(filter, blog_ids);

            if (!StringHelper.CheckStringIsNullOrEmptyOrWhitespace(user_ids))
                filter = PrepareUserFilter(filter, user_ids);

            if (!StringHelper.CheckStringIsNullOrEmptyOrWhitespace(tag_contains))
                filter = PrepareTagContainFilter(filter, tag_contains);

            if (!StringHelper.CheckStringIsNullOrEmptyOrWhitespace(tag_not_contains))
                filter = PrepareTagNotContainFilter(filter, tag_not_contains);

            if (tag_not_exist)
                filter = PrepareTagNotExistFilter(filter);

            return filter;
        }

        private string PrepareSortFieldName(string order)
        {
            if (!StringHelper.CheckStringIsNullOrEmptyOrWhitespace(order))
            {
                order = order.Trim();
                switch (order)
                {
                    case "title":
                        return "title";

                    case "blog_title":
                        return "blogtitle";

                    case "author":
                        return "authorname";

                    case "published_at":
                        return "publisheddate";

                    default:
                        return null;
                }
            }
            return null;
        }

        private string PrepareSortType(string direction)
        {
            if (!StringHelper.CheckStringIsNullOrEmptyOrWhitespace(direction))
            {
                direction = direction.Trim();
                switch (direction)
                {
                    case "asc":
                        return "asc";

                    case "desc":
                        return "desc";

                    default:
                        return null;
                }
            }
            return null;
        }

        private FilterSearchModel PreparePublishStatusFilter(FilterSearchModel model, bool visible)
        {
            switch (visible)
            {
                case true:
                    model.Fields.Add(new FilterSearchField()
                    {
                        FieldName = "PublishedDate",
                        HasOptions = true,
                        IsNummeric = false,
                        OptionValue = "BeforeEqualsNow"
                    });
                    break;

                case false:
                    model.Fields.Add(new FilterSearchField()
                    {
                        FieldName = "PublishedDate",
                        HasOptions = true,
                        IsNummeric = false,
                        OptionValue = "AfterNow"
                    });
                    break;

                default:
                    break;
            }
            return model;
        }

        private FilterSearchModel PrepareBlogFilter(FilterSearchModel model, string blog_ids)
        {
            var arrBlogIds = blog_ids.Split(',');
            foreach (var blogId in arrBlogIds)
            {
                if (!StringHelper.CheckStringIsNullOrEmptyOrWhitespace(blogId) && long.TryParse(blogId, out long id))
                {
                    model.Fields.Add(new FilterSearchField()
                    {
                        FieldName = "BlogId",
                        HasOptions = true,
                        OptionValue = id.ToString(),
                        OptionValueValue = id.ToString()
                    });
                }
            }

            return model;
        }

        private FilterSearchModel PrepareUserFilter(FilterSearchModel model, string user_ids)
        {
            var arrUserIds = user_ids.Split(',');
            foreach (var userId in arrUserIds)
            {
                if (!StringHelper.CheckStringIsNullOrEmptyOrWhitespace(userId) && long.TryParse(userId, out long id))
                {
                    model.Fields.Add(new FilterSearchField()
                    {
                        FieldName = "AuthorId",
                        HasOptions = true,
                        IsNummeric = false,
                        OptionValue = id.ToString()
                    });
                }
            }
            return model;
        }

        private FilterSearchModel PrepareTagContainFilter(FilterSearchModel model, string tag_contains)
        {
            var arrTags = tag_contains.Split(',');

            foreach (var tag in arrTags)
            {
                if (!StringHelper.CheckStringIsNullOrEmptyOrWhitespace(tag))
                {
                    model.Fields.Add(new FilterSearchField()
                    {
                        FieldName = "Tags",
                        HasOptions = true,
                        OptionValue = "IN",
                        OptionValueValue = tag
                    });
                }
            }

            return model;
        }

        private FilterSearchModel PrepareTagNotContainFilter(FilterSearchModel model, string tag_not_contains)
        {
            var arrTags = tag_not_contains.Split(',');
            foreach (var tag in arrTags)
            {
                if (!StringHelper.CheckStringIsNullOrEmptyOrWhitespace(tag))
                {
                    model.Fields.Add(new FilterSearchField()
                    {
                        FieldName = "Tags",
                        HasOptions = true,
                        OptionValue = "NOT IN",
                        OptionValueValue = tag
                    });
                }
            }
            return model;
        }

        private FilterSearchModel PrepareTagNotExistFilter(FilterSearchModel model)
        {
            model.Fields.Add(new FilterSearchField()
            {
                FieldName = "Tags",
                HasOptions = true,
                OptionValue = "MissingField"
            });
            return model;
        }

        private async Task<List<ArticleAddPublisRequest>> CreaDataTag(List<long> articlesIds)
        {
            var model = await RefineArticlesOfStore(articlesIds);
            List<ArticleAddPublisRequest> data = new List<ArticleAddPublisRequest>();
            if (model != null)
            {
                var it = model.Select((s, i) => new ArticleAddPublisRequest { articlesIds = s._id, blogId = s.blogid }).ToList();
                data.AddRange(it);
            }
            return data;
        }

        private async Task<List<MGArticleModel>> GetByDicIds(Dictionary<int, long> dicIds)
        {
            var lstIds = dicIds.Select(m => m.Value);
            var storeId = exRequestContext.StoreId;
            var lstArticles = await rpArticle.GetBy(storeId, lstIds.ToList());
            if (lstArticles != null && lstArticles.Any())
            {
                var lstBlogIds = lstArticles.Select(m => m.blogid).Distinct();
                var lstBlogs = await rpBlog.GetById(storeId, lstBlogIds.ToList());
                var result = from v in lstArticles
                             join k in dicIds on v._id equals k.Value
                             orderby k.Key
                             select v;
                foreach (var item in result)
                {
                    if (lstBlogs != null && lstBlogs.Any())
                    {
                        var objBlog = lstBlogs.FirstOrDefault(m => m._id == item.blogid);
                        if (objBlog != null)
                        {
                            item.blogtitle = objBlog.title;
                            item.bloghandleurl = objBlog.handle;
                            item.blogcommentrule = objBlog.commentrule;
                        }
                    }
                    if (item.authorid == null || item.authorid <= 0)
                    {
                        if (item.user != null)
                        {
                            item.authorid = item.user._id;
                            item.author = item.user.name;
                        }
                    }
                }
                return result.ToList();
            }
            return new List<MGArticleModel>();
        }

        private async Task<(ESSearchResult esResult, long totalRecord)> SearchESArticle(FilterSearchModel model)
        {
            if (model == null)
            {
                requestContext.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return (null, 0);
            }
            if (model.SortFieldName == null)
            {
                model.SortFieldName = "UpdatedDate";
            }
            if (model.SortType == null)
            {
                model.SortType = "desc";
            }
            var storeId = exRequestContext.StoreId;

            var rs = await rpESArticleSearch.SearchAsync(storeId, model);
            return (rs, rs.Total);
        }

        private async Task<List<string>> GetTagsByTypeAPI(FilterSearchModel model, int type, bool getPopular)
        {
            if (model == null && model.Fields == null && model.Page == null)
            {
                requestContext.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return null;
            }
            long refId = 0;
            foreach (var filterSearchField in model.Fields)
            {
                if (type == (int)SummaryType.Blog_ArticlesTag && filterSearchField.FieldName == "BlogId")
                    refId = long.Parse(filterSearchField.NummericValue);
                if (type == (int)SummaryType.Article_Tag && filterSearchField.FieldName == "ArticleId")
                    refId = long.Parse(filterSearchField.NummericValue);
            }
            var tags = (await rpTag.GetSummaryNamesByType(type
                                                            , exRequestContext.StoreId
                                                            , refId
                                                            , getPopular ? model.Page.pageSize : -1)).ToList();
            return tags;
        }

        private async Task WriteLogDashboard(int logAction, ArticleDetailModel page)
        {
            var model = new ArticleLogModel
            {
                Title = page.Title,
                LogDate = DateTime.UtcNow,
                LogUser = exRequestContext.UserName,
                LinkDetail = "article#/detail/" + page.Id,
                BlogId = page.BlogId
            };
            var logdata = JsonConvert.SerializeObject(model);
            var msg = new LogDataMsg
            {
                LogData = logdata,
                CreatedUser = exRequestContext.UserId,
                CreatedUserEmail = exRequestContext.UserEmail,
                ActionId = logAction,
                TypeId = (int)BHN.SharedObject.EBSMessage.LogType.Dashboard,
                RefId = page.Id,
                StoreId = exRequestContext.StoreId,
                CreatedDate = DateTime.UtcNow,
                Action = MessageAction.Insert,
                IsCommentLog = false,
                CreatedUserName = exRequestContext.UserName
            };

            await rpCom.SendMsgAsync(msg);
        }

        private FilterSearchModel GetArticleFilter(long articleId, long blogId, DateTime? created_at_max, DateTime? created_at_min, string fields, int limit, int page,
                                                 int popular, DateTime? published_at_max, DateTime? published_at_min, string published_status, long since_id, DateTime? updated_at_max, DateTime? updated_at_min)
        {
            var filter = new FilterSearchModel
            {
                Fields = new List<FilterSearchField>(),
                Page = new FilterSearchPage { pageSize = limit, currentPage = page },
                ViewId = BusinessObjects.Enums.SysView.Article
            };

            if (blogId > 0)
            {
                var field = new FilterSearchField { FieldName = "BlogId", NummericOperator = "=", IsNummeric = true, NummericValue = blogId.ToString() };
                filter.Fields.Add(field);
            }

            if (articleId > 0)
            {
                var field = new FilterSearchField { FieldName = "Id", NummericOperator = "=", IsNummeric = true, NummericValue = articleId.ToString() };
                filter.Fields.Add(field);
            }

            if (since_id > 0)
            {
                var field = new FilterSearchField { FieldName = "Id", NummericOperator = ">", IsNummeric = true, NummericValue = since_id.ToString() };
                filter.Fields.Add(field);
            }
            if (created_at_min != null && created_at_min != DateTime.MinValue)
            {
                var field = new FilterSearchField
                {
                    FieldName = "CreatedDate",
                    OptionValue = "AfterExtract",
                    OptionValueValue = created_at_min.Value.ToString("o")
                };
                filter.Fields.Add(field);
            }
            if (created_at_max != null && created_at_max != DateTime.MinValue)
            {
                var field = new FilterSearchField
                {
                    FieldName = "CreatedDate",
                    OptionValue = "BeforeExtract",
                    OptionValueValue = created_at_max.Value.ToString("o")
                };
                filter.Fields.Add(field);
            }
            if (updated_at_min != null && updated_at_min != DateTime.MinValue)
            {
                var field = new FilterSearchField
                {
                    FieldName = "UpdatedDate",
                    OptionValue = "AfterExtract",
                    OptionValueValue = updated_at_min.Value.ToString("o")
                };
                filter.Fields.Add(field);
            }
            if (updated_at_max != null && updated_at_max != DateTime.MinValue)
            {
                var field = new FilterSearchField
                {
                    FieldName = "UpdatedDate",
                    OptionValue = "BeforeExtract",
                    OptionValueValue = updated_at_max.Value.ToString("o")
                };
                filter.Fields.Add(field);
            }
            if (published_at_min != null && published_at_min != DateTime.MinValue)
            {
                var field = new FilterSearchField
                {
                    FieldName = "PublishDate",
                    OptionValue = "AfterExtract",
                    OptionValueValue = published_at_min.Value.ToString("o")
                };
                filter.Fields.Add(field);
            }
            if (published_at_max != null && published_at_max != DateTime.MinValue)
            {
                var field = new FilterSearchField
                {
                    FieldName = "PublishDate",
                    OptionValue = "BeforeExtract",
                    OptionValueValue = published_at_max.Value.ToString("o")
                };
                filter.Fields.Add(field);
            }
            if (!String.IsNullOrWhiteSpace(published_status))
            {
                var status = published_status;
                if (status.Equals("published"))
                {
                    var field = new FilterSearchField
                    {
                        FieldName = "PublishedDate",
                        OptionValue = "BeforeEqualsNow"
                    };
                    filter.Fields.Add(field);
                }
                else if (status.Equals("unpublished"))
                {
                    var field = new FilterSearchField
                    {
                        FieldName = "PublishedDate",
                        OptionValue = "AfterNow"
                    };
                    filter.Fields.Add(field);
                }
            }
            return filter;
        }

        #endregion private
    }
}