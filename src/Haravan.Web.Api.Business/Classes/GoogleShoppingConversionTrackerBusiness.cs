﻿using Haravan.Libs.Abstractions;
using Haravan.Web.Api.BusinessObjects.Mappers;
using Haravan.Web.Api.BusinessObjects.Models.Google;
using Haravan.Web.Api.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public class GoogleShoppingConversionTrackerBusiness : IGoogleShoppingConversionTrackerBusiness
    {
        private readonly IRequestContext requestContext;
        private readonly IMGGoogleShoppingConversionTrackerRepository rpMGGoogleShoppingConversionTracker;
        private readonly IExtensionPerRequest ExtensionReq;
        private readonly IEBSComMessageRepository rpCom;

        public GoogleShoppingConversionTrackerBusiness(
                IRequestContext requestContext,
                IMGGoogleShoppingConversionTrackerRepository rpMGGoogleShoppingConversionTracker,
                IExtensionPerRequest ExtensionReq,
                IEBSComMessageRepository rpCom
            )
        {
            this.ExtensionReq = ExtensionReq;
            this.requestContext = requestContext;
            this.rpMGGoogleShoppingConversionTracker = rpMGGoogleShoppingConversionTracker;
            this.rpCom = rpCom;
        }

        #region public

        public async Task AddApiAsync(List<GoogleShoppingConversionTrackerDetailApiModel> model)
        {
            var isInsert = ValidateGoogleShoppingConversionTracker(model);
            if (isInsert)
            {
                await rpMGGoogleShoppingConversionTracker.DeleteManyAsync(ExtensionReq.OrgId);
                foreach (var item in model)
                {
                    var MGmodeldetail = item.MGGoogleShoppingConversionTrackerToGoogleSiteVerification();
                    MGmodeldetail.storeid = ExtensionReq.StoreId;
                    MGmodeldetail.created_at = DateTime.UtcNow;
                    MGmodeldetail.updated_at = DateTime.UtcNow;
                    await rpMGGoogleShoppingConversionTracker.SetAsync(MGmodeldetail);
                }
                await rpCom.FireStoreDataChange(ExtensionReq.StoreId);
            }
        }

        public async Task DeleteManyAsync()
        {
            await rpMGGoogleShoppingConversionTracker.DeleteManyAsync(ExtensionReq.OrgId);
            await rpCom.FireStoreDataChange(ExtensionReq.StoreId);
        }

        #endregion public

        #region private

        private bool ValidateGoogleShoppingConversionTracker(List<GoogleShoppingConversionTrackerDetailApiModel> models)
        {
            if(models.Any(m => string.IsNullOrWhiteSpace(m.google_shopping_site_tag)))
            {
                requestContext.AddError("errors.google_shopping_site_tag.not_empty", "Google_shopping_site_tag không được bỏ trống");
                return false;
            }
            if (models.Any(m => string.IsNullOrWhiteSpace(m.google_event_snippet)))
            {
                requestContext.AddError("errors.google_event_snippet.not_empty", "Google_event_snippet không được bỏ trống");
                return false;
            }
            if (models.Any(m => string.IsNullOrWhiteSpace(m.adwords_id)))
            {
                requestContext.AddError("errors.adwords_id.not_empty", "Adwords Id không được bỏ trống");
                return false;
            }
            if (models.Any(m => m.conversion_type < 0))
            {
                requestContext.AddError("errors.conversion_type.not_valid", "Conversion_type không hợp lệ");
                return false;
            }
            return true;
        }

        #endregion private
    }
}