﻿using BHN.SharedObject.APIDataModel;
using Haravan.Libs.Abstractions;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.Repository;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public class MetafieldBusiness : IMetafieldBusiness
    {
        private readonly IRequestContext context;
        private readonly IMetafieldRepository rpMetafield;
        private readonly IExtensionPerRequest extensionReq;

        public MetafieldBusiness(IRequestContext context,
            IExtensionPerRequest ExtensionReq,
            IMetafieldRepository rpMetafield
            )
        {
            this.context = context;
            this.rpMetafield = rpMetafield;
            this.extensionReq = ExtensionReq;
        }

        public async Task<bool> DeleteApi(long metafieldId, long? ownerId, string ownerResources)
        {
            var objMetafiel = await rpMetafield.DeleteApi(metafieldId, ownerId, ownerResources);
            if (objMetafiel.HasError)
            {
                context.AddError(objMetafiel.ErrorCodes[0].ToString(),objMetafiel.Errors[0].ToString());
                return false;
            }
            return objMetafiel.Data;
        }

        public async Task<MetafieldAPIModel> GetDetailApi(long metafieldId, long? ownerId, string ownerResources)
        {
            var objMetafiel = await rpMetafield.GetDetailApi(metafieldId, ownerId, ownerResources);
            if (objMetafiel.HasError)
            {
                context.AddError(objMetafiel.ErrorCodes[0].ToString(), objMetafiel.Errors[0].ToString());
                return null;
            }
            return objMetafiel.Data;
        }

        public async Task<MetafieldAPIModel> Omni_AddMetafield(OmniCreateMetafieldRequestModel model)
        {
            var objMetafiel = await rpMetafield.Omni_AddMetafield(model);
            if (objMetafiel.HasError)
            {
                context.AddError(objMetafiel.ErrorCodes[0].ToString(), objMetafiel.Errors[0].ToString());
                return null;
            }
            return objMetafiel.Data;
        }

        public async Task<long> Omni_CountMetafields(int limit, int page, DateTime? created_at_min, DateTime? created_at_max, DateTime? updated_at_min, DateTime? updated_at_max, string @namespace, string key, string value_type, long owner_id, string owner_resource, long since_id, string owner_resources)
        {
            var objMetafiel = await rpMetafield.Omni_CountMetafields(limit,
                page,
                created_at_min,
                created_at_max,
                updated_at_min,
                updated_at_max,
                @namespace,
                key,
                value_type,
                owner_id,
                owner_resource,
                since_id,
                owner_resources);
            if (objMetafiel.HasError)
            {
                context.AddError(objMetafiel.ErrorCodes[0].ToString(), objMetafiel.Errors[0].ToString());
                return 0;
            }
            return objMetafiel.Data;
        }

        public async Task<bool> Omni_DeleteMetafield(long metafieldId, string ownerResource, long ownerId)
        {
            var objMetafiel = await rpMetafield.Omni_DeleteMetafield(metafieldId, ownerResource, ownerId);
            if (objMetafiel.HasError)
            {
                context.AddError(objMetafiel.ErrorCodes[0].ToString(), objMetafiel.Errors[0].ToString());
                return false;
            }
            return objMetafiel.Data;
        }

        public async Task<MetafieldAPIModel> Omni_GetMetafieldDetail(long metafieldId, string ownerResource, long ownerId)
        {
            var objMetafiel = await rpMetafield.Omni_GetMetafieldDetail(metafieldId, ownerResource, ownerId);
            if (objMetafiel.HasError)
            {
                context.AddError(objMetafiel.ErrorCodes[0].ToString(), objMetafiel.Errors[0].ToString());
                return null;
            }
            return objMetafiel.Data;
        }

        public async Task<List<MetafieldAPIModel>> Omni_GetMetafieldList(int limit, int page, DateTime? created_at_min, DateTime? created_at_max, DateTime? updated_at_min, DateTime? updated_at_max, string @namespace, string key, string value_type, long owner_id, string owner_resource, long since_id, string owner_resources)
        {
            var objMetafiel = await rpMetafield.Omni_GetMetafieldList(limit,
                page,
                created_at_min,
                created_at_max,
                updated_at_min,
                updated_at_max,
                @namespace,
                key,
                value_type,
                owner_id,
                owner_resource,
                since_id,
                owner_resources);
            if (objMetafiel.HasError)
            {
                context.AddError(objMetafiel.ErrorCodes[0].ToString(), objMetafiel.Errors[0].ToString());
                return null;
            }
            return objMetafiel.Data;
        }

        public async Task<MetafieldAPIModel> Omni_UpdateMetafield(long metafieldId, string ownerResource, long ownerId, OmniUpdateMetafieldRequestModel model)
        {
            var objMetafiel = await rpMetafield.Omni_UpdateMetafield(metafieldId, ownerResource, ownerId, model);
            if (objMetafiel.HasError)
            {
                context.AddError(objMetafiel.ErrorCodes[0].ToString(), objMetafiel.Errors[0].ToString());
                return null;
            }
            return objMetafiel.Data;
        }

        public async Task<MetafieldAPIModel> Omni_UpdateMetafield(long metafieldId, OmniUpdateMetafieldRequestModel model)
        {
            var objMetafiel = await rpMetafield.Omni_UpdateMetafield(metafieldId, model);
            if (objMetafiel.HasError)
            {
                context.AddError(objMetafiel.ErrorCodes[0].ToString(), objMetafiel.Errors[0].ToString());
                return null;
            }
            return objMetafiel.Data;
        }
    }
}