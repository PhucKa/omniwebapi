﻿using Haravan.Libs.Abstractions;
using Haravan.Web.Api.BusinessObjects.Models.Common;
using Haravan.Web.Api.Repository;
using System.Threading.Tasks;
using Haravan.Web.Api.Utils.Extensions;
namespace Haravan.Web.Api.Business
{
    public class FilterBusiness : IFilterBusiness
    {
        
        private readonly IRequestContext requestContext;
        private readonly IKeywordBusiness bizKeyword;
        private readonly IFilterRepository rpFilter;

        public FilterBusiness(IRequestContext requestContext,

            IKeywordBusiness bizKeyword,
            IFilterRepository rpFilter
        )
        {
            this.requestContext = requestContext;
            
            this.bizKeyword = bizKeyword;
            this.rpFilter = rpFilter;
        }

        public async Task<ViewFilterData> GetFilterAsync(int viewId)
        {
            return await rpFilter.GetFilter(viewId);
        }

        public async Task<FilterTab> GetTabDetail(long tabId)
        {
            return await rpFilter.GetTabDetail(tabId);
        }

        public async Task<long> AddFilter(FilterTab tab)
        {
            return await rpFilter.AddFilter(tab);
        }

        public async Task<long> UpdateFilter(FilterTab tab)
        {
            return await rpFilter.UpdateFilter(tab);
        }

        public async Task<long> DeleteFilter(long tabId)
        {
            return await rpFilter.DeleteFilter(tabId);
        }
    }
}