﻿using BHN.SharedObject.EBSMessage;
using BHN.SharedObject.EBSMessage.Enums;
using BHN.SharedObject.EBSMessage.LogSeller;
using DnsClient;
using Haravan.Acme;
using Haravan.Libs.Abstractions;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Configs;
using Haravan.Web.Api.BusinessObjects.Mappers;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.BusinessObjects.Models.user;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using Haravan.Web.Api.Repository;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public class DomainsBusiness : IDomainsBusiness
    {
        readonly long _storeId;
        private readonly IRequestContext context;
        private readonly ISysStoreRepository rpStore;
        private readonly ISysDomainRepository rpSysDomain;
        private readonly ICertInfo rpCertInfo;
        private readonly IMGDomainPriceRepository rpDomainPrice;
        private readonly IEBSComMessageRepository bus;
        private readonly IExtensionPerRequest ExtensionReq;
        private readonly IAcmeClient acmeClient;
        private readonly IMGAppInstalledRepository rpMGAppInstalled;
        private readonly IUserRepository rpUser;
        private readonly IKeywordBusiness bizKeyword;
        private readonly IEtcdApiRepository rpEtcdApi;
        private readonly AppConfig config;
        private readonly ILogger<DomainsBusiness> logger;

        public DomainsBusiness(
                 IRequestContext context,
                 ISysStoreRepository rpStore,
                 ICertInfo rpCertInfo,
                 IMGDomainPriceRepository rpDomainPrice,
                 IMGAppInstalledRepository rpMGAppInstalled,
                 ISysDomainRepository rpSysDomain,
                 IUserRepository rpUser,
                 IEBSComMessageRepository bus,
                 IAcmeClient acmeClient,
                 IKeywordBusiness bizKeyword,
                 IExtensionPerRequest ExtensionReq,
                 IEtcdApiRepository rpEtcdApi,
                 IOptions<AppConfig> config,
                 ILogger<DomainsBusiness> logger
            )
        {
            this.context = context;
            this.config = config.Value;
            this.ExtensionReq = ExtensionReq;
            _storeId = this.ExtensionReq.OrgId;
            this.rpStore = rpStore;
            this.rpCertInfo = rpCertInfo;
            this.rpDomainPrice = rpDomainPrice;
            this.rpMGAppInstalled = rpMGAppInstalled;
            this.rpSysDomain = rpSysDomain;
            this.bizKeyword = bizKeyword;
            this.rpUser = rpUser;
            this.bus = bus;
            this.acmeClient = acmeClient;
            this.rpEtcdApi = rpEtcdApi;
            this.logger = logger;
        }


        public async Task<bool> HasSpfRecord(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return true;
            var emailDomain = email.Split('@')[1].Trim().ToLower();
            var objDomain = await rpSysDomain.GetByDomainName(emailDomain);
            if (objDomain != null)
            {
                var client = new LookupClient();
                var response = client.Query(emailDomain, QueryType.TXT);
                if (
                        response == null
                        || response.Answers == null
                        || !response.Answers.Any()
                        || response.Answers.TxtRecords() == null
                        || !response.Answers.TxtRecords().Any()
                        || !response.Answers.TxtRecords().Any(a => a.Text.FirstOrDefault(b => b.StartsWith("v=spf1") && b.Contains("include:_spf.haravan.com")) != null)
                    )
                {
                    return false;
                }
            }
            return true;
        }

        #region public

        public async Task<bool> IsPointToHaravan(string domainName)
        {
            if (string.IsNullOrWhiteSpace(domainName))
            {
                return false;
            }
            return await acmeClient.VerifyHaravanIp(domainName);
        }
        public async Task<bool> CheckValidationDomain(string domains)
        {
            if (domains == null)
            {
                context.AddError("errors.domain.not_empty", "Tên miền không được trống");
                return false;
            }
            if (!domains.Contains(".")) return true; // subdomain
            var _valid = await acmeClient.VerifyHaravanIp(domains);

            if (!_valid)
            {
                context.AddError($"﻿errors.domain.not_connect__{domains}__", "Tên miền " + domains + " chưa được kết nối");
                return false;
            }
            return true;
        }
        public async Task<CheckValidDomainModel> CheckValidDomain(string hostname)
        {
            var model = new CheckValidDomainModel();

            if (hostname == "localhost" || hostname == "127.0.0.1")
                model.Status = 2;
            model.IsError = true;

            if (hostname == "localhost" || hostname == "127.0.0.1")
            {
                model.Status = 2;
                model.IsError = true;
                return model;
            }

            var isHaravanIp = await acmeClient.VerifyHaravanIp(hostname);
            if (isHaravanIp == false)
            {
                model.Status = 2;
                model.IsError = true;
                return model;
            }
            else
            {
                model.Status = 1;
                model.IsError = false;
                return model;
            }
        }
        public async Task<string> SubmitChangeSubDomain(string newSubDomain)
        {
            var objDomain = await ValidateChangeSubDomain(newSubDomain, true);

            if (objDomain == null)
                return null;

            if (objDomain.OldValue == null)
                objDomain.OldValue = new List<MGChangeSysDomain>();

            var oldSubDomain = objDomain.Name;
            objDomain.OldValue.Add(new MGChangeSysDomain()
            {
                Name = oldSubDomain,
                ChangedUser = ExtensionReq.UserId,
                ChangedNewDate = DateTime.UtcNow
            });

            objDomain.Name = newSubDomain;

            await rpSysDomain.UpdateAsync(objDomain, a => a.Name, a => a.OldValue);

            await bizKeyword.ProcessingKeyHandle(ExtensionReq.StoreId, (int)EnumDocumentType.Domain, null, newSubDomain);
            var uri = new UriBuilder(await ExtensionReq.GetStoreDomain());

            await WriteLogDashboard((int)DashboardLogAction.Setting_Domain_ChangeSubdomain, string.Format("từ {0} sang {1}", oldSubDomain + "." + uri.Host, newSubDomain + "." + uri.Host)
                , objDomain._id);

            await bus.FireStoreDataChange(ExtensionReq.OrgId);
            await bus.UpdateStoreCache(ExtensionReq.OrgId);

            var getStoreDomain = await ExtensionReq.GetStoreDomain();

            var returnUrl = getStoreDomain + "settings/domains";
            return returnUrl;
        }
        public async Task<bool> UnUseLetEnscript(long domainId)
        {
            var objDomain = await rpSysDomain.GetByDomainId(domainId, ExtensionReq.StoreId);

            if (objDomain == null)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return false;
            }
            var certInfoIds = new List<long>();
            if (!objDomain.IsHttps || objDomain.SslType != (int)SslTypeEnum.letsencrypt)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return false;
            }
            if (objDomain.CertInfoId != null && objDomain.CertInfoId.Value > 0) certInfoIds.Add(objDomain.CertInfoId.Value);
            objDomain.CertInfoId = null;
            objDomain.IsHttps = false;
            await rpSysDomain.UpdateAsync(objDomain, m => m.IsHttps, m => m.CertInfoId);
            if (objDomain.CertInfoId != null && objDomain.CertInfoId.Value > 0) certInfoIds.Add(objDomain.CertInfoId.Value);
            var objSubDomain = await rpSysDomain.GetByDomainName(ExtensionReq.StoreId, $"www.{objDomain.Name}");
            if (objSubDomain != null && objSubDomain.IsHttps && objSubDomain.SslType == (int)SslTypeEnum.letsencrypt)
            {
                if (objSubDomain.CertInfoId != null && objSubDomain.CertInfoId.Value > 0) certInfoIds.Add(objSubDomain.CertInfoId.Value);
                objSubDomain.IsHttps = false;
                objSubDomain.CertInfoId = null;
                await rpSysDomain.UpdateAsync(objSubDomain, m => m.IsHttps, m => m.CertInfoId);
            }
            if (certInfoIds != null && certInfoIds.Any())
            {
                var lstCertInfos = await rpCertInfo.GetByIds(certInfoIds);
                if (lstCertInfos != null && lstCertInfos.Any())
                {
                    foreach (var objCert in lstCertInfos)
                    {
                        await rpCertInfo.DeleteAsync(objCert);
                    }
                }
            }
            await bus.FireStoreDataChange(ExtensionReq.OrgId);
            await bus.UpdateStoreCache(ExtensionReq.OrgId);
            return true;
        }
        public async Task ChangeDomainIsHttps(SYSDomainModel domain)
        {
            if (domain == null
                || domain.Id <= 0
                || string.IsNullOrEmpty(domain.Name))
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return;
            }

            var objDomain = await rpSysDomain.GetById(domain.Id);

            if (objDomain == null
                || objDomain.StoreId != ExtensionReq.StoreId
                || objDomain.IsSubDomain)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return;
            }

            if (!domain.IsHttps == objDomain.IsHttps)
                return;

            objDomain.IsHttps = !objDomain.IsHttps;
            await rpSysDomain.UpdateAsync(objDomain, a => a.IsHttps);
            await bus.FireStoreDataChange(ExtensionReq.OrgId);
            await bus.UpdateStoreCache(ExtensionReq.OrgId);
        }
        public async Task<bool> UpdateHttps(long domainId)
        {
            var storeId = ExtensionReq.StoreId;
            var objDomain = await rpSysDomain.GetByDomainId(domainId, storeId);

            if (objDomain == null || objDomain.IsSubDomain)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return false;
            }
            var lstDomainNameAddSsl = new List<string>();
            if (!objDomain.IsHttps)
            {
                lstDomainNameAddSsl.Add(objDomain.Name);
            }

            var objSubDomain = await rpSysDomain.GetByDomainName(ExtensionReq.StoreId, $"www.{objDomain.Name}");
            if (objSubDomain != null && !objSubDomain.IsHttps && objSubDomain.CertInfoId == null)
                lstDomainNameAddSsl.Add(objSubDomain.Name);
            if (lstDomainNameAddSsl == null || !lstDomainNameAddSsl.Any())
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return false;
            }

            var lstDomainPointToHrv = await GetDomainPointToHrv(acmeClient, lstDomainNameAddSsl);
            if (lstDomainPointToHrv == null || !lstDomainPointToHrv.Any())
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return false;
            }

            var certInfo = await AddCertDomain(acmeClient, lstDomainPointToHrv, true);
            if (certInfo != null)
            {
                if (lstDomainPointToHrv.Contains(objDomain.Name))
                {
                    objDomain.SslType = (int)SslTypeEnum.letsencrypt;
                    objDomain.CertInfoId = certInfo._id;
                    objDomain.ExpireAt = certInfo.ExpireAt;
                }
                if (objSubDomain != null && lstDomainPointToHrv.Contains(objSubDomain.Name))
                {
                    objSubDomain.SslType = (int)SslTypeEnum.letsencrypt;
                    objSubDomain.CertInfoId = certInfo._id;
                    objSubDomain.ExpireAt = certInfo.ExpireAt;
                    objSubDomain.IsHttps = true;
                }
                objDomain.IsHttps = true;
                await rpSysDomain.UpdateAsync(objDomain, a => a.IsHttps
                                                    , a => a.SslType
                                                    , a => a.CertInfoId
                                                    , a => a.ExpireAt);
                if (objSubDomain != null)
                {
                    await rpSysDomain.UpdateAsync(objSubDomain, a => a.IsHttps
                                                       , a => a.SslType
                                                       , a => a.CertInfoId
                                                       , a => a.ExpireAt);
                }
                await bus.FireStoreDataChange(ExtensionReq.OrgId);
                await bus.UpdateStoreCache(ExtensionReq.OrgId);
            }
            return true;
        }
        public async Task<bool> UpdateIsHttpsAsync(long domainId, string name, bool isUseHttps, bool isThrowError = true)
        {
            if (domainId <= 0 || string.IsNullOrEmpty(name))
            {
                if (isThrowError)
                    context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return false;
            }

            var objDomain = await rpSysDomain.GetByDomainId(domainId, ExtensionReq.StoreId);

            if (objDomain == null || objDomain.IsSubDomain)
            {
                if (isThrowError)
                    context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");

                return false;
            }
            var lstDomainNameAddSsl = new List<string>();

            if (!objDomain.IsHttps) lstDomainNameAddSsl.Add(objDomain.Name);
            var objSubDomain = await rpSysDomain.GetByDomainName(ExtensionReq.StoreId, $"www.{objDomain.Name}");
            if (objSubDomain != null && !objSubDomain.IsHttps && objSubDomain.CertInfoId == null)
                lstDomainNameAddSsl.Add(objSubDomain.Name);

            if (lstDomainNameAddSsl == null || !lstDomainNameAddSsl.Any())
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return false;
            }

            var lstDomainPointToHrv = await GetDomainPointToHrv(acmeClient, lstDomainNameAddSsl);
            if (lstDomainPointToHrv == null || !lstDomainPointToHrv.Any())
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return false;
            }

            MGCertInfo certInfo = await AddCertDomain(acmeClient, lstDomainPointToHrv, isThrowError);
            if (certInfo != null)
            {
                if (lstDomainPointToHrv.Contains(objDomain.Name))
                {
                    objDomain.SslType = (int)SslTypeEnum.letsencrypt;
                    objDomain.CertInfoId = certInfo._id;
                    objDomain.ExpireAt = certInfo.ExpireAt;
                }
                if (objSubDomain != null && lstDomainPointToHrv.Contains(objSubDomain.Name))
                {
                    objSubDomain.SslType = (int)SslTypeEnum.letsencrypt;
                    objSubDomain.CertInfoId = certInfo._id;
                    objSubDomain.ExpireAt = certInfo.ExpireAt;
                    objSubDomain.IsHttps = true;
                }
                objDomain.IsHttps = isUseHttps;
                await rpSysDomain.UpdateAsync(objDomain, a => a.IsHttps
                                                    , a => a.SslType
                                                    , a => a.CertInfoId
                                                    , a => a.ExpireAt);
                if (objSubDomain != null)
                {
                    await rpSysDomain.UpdateAsync(objSubDomain, a => a.IsHttps
                                                       , a => a.SslType
                                                       , a => a.CertInfoId
                                                       , a => a.ExpireAt);
                }
                await bus.FireStoreDataChange(ExtensionReq.OrgId);
                await bus.UpdateStoreCache(ExtensionReq.OrgId);
            }
            return true;
        }
        public async Task<MGSysDomain> ValidateChangeSubDomain(string newSubDomain, bool isSubmit = false)
        {
            if (string.IsNullOrEmpty(newSubDomain))
            {
                context.AddError("errors.domain.name.not_empty", "Bạn chưa nhập tên miền");
                return null;
            }

            newSubDomain = newSubDomain.Trim().ToLower();

            if (newSubDomain.Length < 4)
            {
                context.AddError($"errors.domain.name.max_limit_reached__{4}__", "Độ dài tên miền phải lớn hơn 4 ký tự");
                return null;
            }

            var objExistDomain = await rpSysDomain.GetBySubDomain(newSubDomain);

            if (objExistDomain != null)
            {
                if (isSubmit)
                    context.AddError("errors.domain.this_time.exist", "Tại thời điểm này, tên miền của bạn đã được tạo bởi người dùng khác. Bạn hãy nhập lại tên miền mới.");
                else
                    context.AddError("errors.domain.exist", "Tên miền đã tồn tại, vui lòng nhập lại tên miền mới.");

                return null;
            }

            var _subdomain = await ExtensionReq.GetStoreSubDomain();
            var objDomain = await rpSysDomain.GetBySubDomain(_subdomain);

            if (objDomain == null)
            {
                context.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return null;
            }

            var _canchan_domain = await CanChangeDomain(objDomain);
            if (!_canchan_domain)
            {
                context.AddError("errors.domain.not_authorized", "Không được quyền thực hiện hành động này");
                return null;
            }
            return objDomain;
        }
        public async Task<UserUpdateModel> GetForUpdate(long userId)
        {
            return await rpUser.GetUserForDomain(userId);
        }
        public async Task<bool> CheckExistDomain(string domainName)
        {
            var _exist = await rpSysDomain.CheckExistDomain(domainName.Trim());
            if (_exist)
            {
                context.AddError("errors.domain.exist", "Tên miền đã tồn tại, vui lòng nhập lại tên miền mới.");
                return true;
            }
            return false;
        }
        private string validateDomain(string domainName)
        {
            var ipRegex = new Regex(@"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b");
            if (ipRegex.IsMatch(domainName))
            {
                context.AddError("errors.domain.not_valid", "Tên miền không hợp lệ");
                return null;
            }

            if (new Regex(@"\s").IsMatch(domainName))
            {
                context.AddError("errors.domain.cannot_contain_spaces", "Tên miền không được chứa khoảng trắng");
                return null;
            }

            if (!new Regex(@"\.").IsMatch(domainName))
            {
                context.AddError("errors.domain.not_valid", "Tên miền không hợp lệ");
                return null;
            }

            string[] blacklistDomain = config.DomainsNotAllowAdd.Split(',');

            for (int i = 0; i < blacklistDomain.Length; i++)
            {
                if (domainName.Contains(blacklistDomain[i]))
                {
                    context.AddError("errors.domain.not_valid", "Tên miền không hợp lệ");
                    return null;
                }
            }

            return domainName;
        }
        public async Task<string> SubmitNewDomains(SYSDomainModel model)
        {

            var domainName = model.Name;
            var isHttps = model.IsHttps;

            if (string.IsNullOrWhiteSpace(domainName))
            {
                context.AddError("errors.domain.not_empty", "Tên miền không được bỏ trống");
                return "";
            }

            domainName = domainName.Trim().ToLower();
            domainName = validateDomain(domainName);
            if (context.IsError)
            {
                return "";
            }
            var objExistDomain = await rpSysDomain.GetByDomainName(domainName);
            if (objExistDomain != null)
            {
                context.AddError("errors.domain.exist", "Tên miền đã tồn tại");
                var inputMD5 = config.HashSalt + ExtensionReq.StoreId.ToString() + domainName;
                var verifyDomainMD5 = BHN.Utils.HashUtils.MD5Hash(inputMD5);
                return domainName + "|" + verifyDomainMD5;
            }
            var isMasterDomain = CheckIsMasterDomain(model.Name);
            if (isMasterDomain == false && model.Name.StartsWith("www."))
            {
                return "";
            }

            var domain = model.MGSysToSYSDomain();
            domain.StoreId = ExtensionReq.StoreId;
            domain.IsSubDomain = false;
            domain.IsPrimary = false;
            await rpSysDomain.SetAsync(domain);
            await WriteLogDashboard((int)DashboardLogAction.Setting_Domain_Add, domain.Name, domain._id);

            var isSendStoreMsg = false;

            if (model.Name.StartsWith("www."))
            {
                var lstDomainNames = new List<string>() { model.Name };
                var lstPointToHRV = await GetDomainPointToHrv(acmeClient, lstDomainNames);
                if (lstPointToHRV != null && lstPointToHRV.Any())
                {
                    var masterDomain = model.Name.Replace("www.", "");
                    var objMasterDomain = await rpSysDomain.GetByDomainName(ExtensionReq.StoreId, masterDomain);
                    if (objMasterDomain != null)
                    {
                        lstDomainNames.Add(objMasterDomain.Name);
                        lstPointToHRV = await GetDomainPointToHrv(acmeClient, lstDomainNames);
                        if (lstPointToHRV != null && lstPointToHRV.Any())
                        {
                            var certInfo = await AddCertDomain(acmeClient, lstPointToHRV, false);
                            if (certInfo != null)
                            {
                                domain.SslType = (int)SslTypeEnum.letsencrypt;
                                domain.CertInfoId = certInfo._id;
                                domain.ExpireAt = certInfo.ExpireAt;
                                domain.IsHttps = true;
                                await rpSysDomain.UpdateAsync(domain, a => a.IsHttps
                                                                     , a => a.SslType
                                                                     , a => a.CertInfoId
                                                                     , a => a.ExpireAt
                                                             );
                                if (lstPointToHRV.Contains(objMasterDomain.Name))
                                {
                                    objMasterDomain.SslType = (int)SslTypeEnum.letsencrypt;
                                    objMasterDomain.CertInfoId = certInfo._id;
                                    objMasterDomain.ExpireAt = certInfo.ExpireAt;
                                    domain.IsHttps = true;
                                    await rpSysDomain.UpdateAsync(objMasterDomain, a => a.IsHttps
                                                                         , a => a.SslType
                                                                         , a => a.CertInfoId
                                                                         , a => a.ExpireAt);
                                }
                                await bus.FireStoreDataChange(ExtensionReq.OrgId);
                                await bus.UpdateStoreCache(ExtensionReq.OrgId);
                                isSendStoreMsg = true;
                            }
                        }
                    }
                }
            }
            else
            {
                var wwwDomainName = $"www.{domainName}";
                var lstDomainNames = new List<string>() { domainName };
                var objExistWWWDomain = await rpSysDomain.GetByDomainName(wwwDomainName);
                if (objExistWWWDomain == null || (objExistWWWDomain != null && objExistWWWDomain.StoreId == ExtensionReq.OrgId))
                {
                    lstDomainNames.Add(wwwDomainName);
                }
                var lstDomainsPointToHrv = await GetDomainPointToHrv(acmeClient, lstDomainNames);
                if (lstDomainsPointToHrv != null && lstDomainsPointToHrv.Any())
                {
                    var certInfo = await AddCertDomain(acmeClient, lstDomainsPointToHrv, false);
                    if (certInfo != null)
                    {
                        if (lstDomainsPointToHrv.Contains(domainName))
                        {
                            domain.SslType = (int)SslTypeEnum.letsencrypt;
                            domain.CertInfoId = certInfo._id;
                            domain.ExpireAt = certInfo.ExpireAt;
                            domain.IsHttps = true;
                            await rpSysDomain.UpdateAsync(domain, a => a.IsHttps
                                                                , a => a.SslType
                                                                , a => a.CertInfoId
                                                                , a => a.ExpireAt
                                                        );
                        }
                        if (lstDomainsPointToHrv.Contains(wwwDomainName))
                        {
                            if (objExistWWWDomain == null)
                            {
                                objExistWWWDomain = new MGSysDomain()
                                {
                                    CertInfoId = certInfo._id,
                                    SslType = (int)SslTypeEnum.letsencrypt,
                                    ExpireAt = certInfo.ExpireAt,
                                    IsHttps = true,
                                    Name = wwwDomainName,
                                    StoreId = ExtensionReq.OrgId
                                };
                                await rpSysDomain.AddAsync(objExistWWWDomain);
                            }
                            else
                            {
                                if (!objExistWWWDomain.IsHttps && objExistWWWDomain.StoreId == ExtensionReq.OrgId)
                                {
                                    objExistWWWDomain.SslType = (int)SslTypeEnum.letsencrypt;
                                    objExistWWWDomain.CertInfoId = certInfo._id;
                                    objExistWWWDomain.ExpireAt = certInfo.ExpireAt;
                                    objExistWWWDomain.IsHttps = true;
                                    await rpSysDomain.UpdateAsync(objExistWWWDomain, a => a.IsHttps
                                                                        , a => a.SslType
                                                                        , a => a.CertInfoId
                                                                        , a => a.ExpireAt
                                                                );
                                }
                            }
                        }
                        await bus.FireStoreDataChange(ExtensionReq.OrgId);
                        await bus.UpdateStoreCache(ExtensionReq.OrgId);
                        isSendStoreMsg = true;
                    }
                }
            }

            if (!isSendStoreMsg)
            {
                await bus.FireStoreDataChange(ExtensionReq.OrgId);
                await bus.UpdateStoreCache(ExtensionReq.OrgId);
            }
            return "";
        }
        public async Task<string> AddDomain(string domainName)
        {
            return await SubmitNewDomains(new SYSDomainModel() { Name = domainName });
        }
        public async Task<string> AddNewDomains(string domainName, bool isHttps)
        {
            if (string.IsNullOrWhiteSpace(domainName))
            {
                context.AddError("errors.domain.not_empty", "Tên miền không được trống");
                return "";
            }
            var objDomain = new SYSDomainModel()
            {
                Name = domainName,
                IsHttps = isHttps
            };
            var _model = await SubmitNewDomains(objDomain);
            return _model;
        }
        public async Task<DataDomainModel> GetDataDomain()
        {
            return new DataDomainModel()
            {
                IpAssigned = await _getIpAssignedByStore(_storeId),
                ListDomain = await _getListDomainByStore(_storeId)
            };
        }
        public async Task<List<SYSDomainModel>> ListDomainCanChangePrimary()
        {
            var lstDomains = await rpSysDomain.GetListDomainByStore(ExtensionReq.StoreId);
            if (lstDomains == null || !lstDomains.Any())
            {
                return new List<SYSDomainModel>();
            }
            return lstDomains.Where(m => !m.IsPrimary).ToList().ToModelSYSDomainList();
        }
        public async Task<List<DomainPriceModel>> GetListPriceDomains()
        {
            List<DomainPriceModel> models = new List<DomainPriceModel>();
            var result = await rpDomainPrice.GetAll();

            if (result == null
                || result.Count == 0)
            {
                return new List<DomainPriceModel>();
            }
            models = result.DomainPriceToMGDomainPrice();
            return models;
        }
        public async Task<bool> ChangToPrimaryDomain(long domainId, bool isRedirect)
        {
            if (domainId <= 0)
            {
                context.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return false;
            }

            var logOldDomainName = "";

            var lstDomain = await rpSysDomain.GetListDomainByStore(ExtensionReq.StoreId);
            if (lstDomain == null || !lstDomain.Any())
            {
                context.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return false;
            }
            var oldPrimaryDomain = lstDomain.FirstOrDefault(m => m.IsPrimary);
            var domain = lstDomain.Where(m => m._id == domainId).FirstOrDefault();
            if (domain == null || oldPrimaryDomain == null)
            {
                context.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return false;
            }
            if (!domain.IsSubDomain)
            {
                var isPointToHRV = await acmeClient.VerifyHaravanIp(domain.Name);
                if (!isPointToHRV)
                {
                    context.AddError("errors.domain.not_ip_haravan", "Tên miền chưa được trỏ về IP của Haravan");
                    return false;
                }
            }
            oldPrimaryDomain.IsPrimary = false;
            logOldDomainName = oldPrimaryDomain.Name;
            await rpSysDomain.UpdateAsync(oldPrimaryDomain, x => x.IsPrimary);
            domain.IsRedirect = false;
            domain.IsPrimary = true;
            await rpSysDomain.UpdateAsync(domain, x => x.IsPrimary, x => x.IsRedirect);
            foreach (var item in lstDomain.Where(m => m._id != domainId))
            {
                if (isRedirect)
                {
                    item.IsRedirect = true;
                    await rpSysDomain.UpdateAsync(item, x => x.IsRedirect);
                }
            }

            var logTitle = string.Format("từ {0} sang {1}", logOldDomainName, domain.Name);

            await WriteLogDashboard((int)DashboardLogAction.Setting_Domain_ChangePrimary, logTitle, domain._id);
            await bus.FireStoreDataChange(ExtensionReq.OrgId);
            await bus.UpdateStoreCache(ExtensionReq.OrgId);
            return true;
        }
        public async Task<bool> VerifyToReclaimDomain(string domainName)
        {
            if (string.IsNullOrEmpty(domainName))
            {
                context.AddError("errors.domain.parameters.not_valid", "Tham số không hợp lệ");
                return false;
            }

            domainName = domainName.Trim().ToLower();
            var verifyDomainMD5 = BHN.Utils.HashUtils.MD5Hash(config.HashSalt + ExtensionReq.StoreId.ToString() + domainName);
            var client = new LookupClient();
            var response = client.Query(domainName.Replace("www.", ""), QueryType.TXT);
            if (
                    response == null
                    || response.Answers == null
                    || !response.Answers.Any()
                    || response.Answers.TxtRecords() == null
                    || !response.Answers.TxtRecords().Any()
                    || !response.Answers.TxtRecords().Any(a => a.Text.FirstOrDefault(b => b == verifyDomainMD5) != null)
                )
            {
                context.AddError($"errors.domain.not_accuracy__{domainName}__", "Không xác thực được tên miền " + domainName);
                return false;
            }
            var listDomainAffected = new List<string>
            {
                domainName,
                domainName.StartsWith("www.") ? domainName.Replace("www.", "") : "www." + domainName
            };

            foreach (var domain in listDomainAffected)
            {
                var shop = await GetDomainByName(domain);
                if (shop != null && shop.StoreId != ExtensionReq.StoreId)
                {
                    var isDelete = await DeleteDomain(shop._id, true);

                    if (isDelete == false)
                        return false;

                    await SubmitNewDomains(new SYSDomainModel()
                    {
                        Name = domain
                    });
                }
            }
            return true;
        }
        public async Task<MGSysDomain> GetDomainByName(string name)
        {
            return await rpSysDomain.GetByName(name);
        }
        public async Task<bool> RemoveDomain(long domainId)
        {
            if (domainId <= 0)
            {
                context.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return false;
            }

            var storeId = ExtensionReq.StoreId;

            var objEx = await rpSysDomain.GetByDomainId(domainId, storeId);

            if (objEx == null)
            {
                context.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return false;
            }

            if (objEx.IsSubDomain)
            {
                context.AddError("errors.domain.sub.domain.not_delete", "Bạn không thể xóa sub domain");
                return false;
            }

            if (objEx.IsPrimary)
            {
                context.AddError("errors.domain.main.domain.not_delete", "Bạn không thể xóa domain chính");
                return false;
            }
            await rpSysDomain.DeleteAsync(objEx);
            await WriteLogDashboard((int)DashboardLogAction.Setting_Domain_Remove, objEx.Name, objEx._id);
            await bus.FireStoreDataChange(storeId);
            await bus.UpdateStoreCache(ExtensionReq.OrgId);
            return true;
        }
        public async Task<bool> DeleteDomain(long domainId, bool fromOtherStore = false)
        {
            if (domainId <= 0)
            {
                context.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return false;
            }

            var objEx = await rpSysDomain.GetById(domainId);

            if (objEx == null
                || (!fromOtherStore && objEx.StoreId != ExtensionReq.StoreId))
            {
                context.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return false;
            }

            if (objEx.IsSubDomain)
            {
                context.AddError("errors.domain.sub.domain.not_delete", "Bạn không thể xóa sub domain");
                return false;
            }

            if (objEx.IsPrimary)
            {
                if (!fromOtherStore)
                {
                    context.AddError("errors.domain.main.domain.not_delete", "Bạn không thể xóa domain chính");
                    return false;
                }
                else
                {
                    var objSubDomain = await rpSysDomain.GetSubDomainByStoreId(objEx.StoreId);

                    if (objSubDomain == null)
                    {
                        context.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                        return false;
                    }

                    objSubDomain.IsRedirect = false;
                    objSubDomain.IsPrimary = true;
                    await rpSysDomain.UpdateAsync(objSubDomain, a => a.IsRedirect, a => a.IsPrimary);
                }
            }

            await rpSysDomain.DeleteAsync(objEx);

            if (!fromOtherStore)
                await WriteLogDashboard((int)DashboardLogAction.Setting_Domain_Remove, objEx.Name, objEx._id);

            await bus.FireStoreDataChange(ExtensionReq.OrgId);
            await bus.UpdateStoreCache(ExtensionReq.OrgId);
            return true;
        }
        public async Task<string> GetIpAssignedByStore()
        {
            var store = await rpStore.GetByStoreId(_storeId);
            return store.IpAssigned == null ? config.DefaultIP : store.IpAssigned;
        }
        #endregion public

        #region private
        private async Task<string> _getIpAssignedByStore(long storeId)
        {
            var store = await rpStore.GetByStoreId(storeId);
            if (store != null)
            {
                return store.IpAssigned == null ? config.DefaultIP : store.IpAssigned;
            }
            return config.DefaultIP;
        }
        private async Task<List<SYSDomainModel>> _getListDomainByStore(long storeId)
        {
            var lstDomains = await rpSysDomain.GetListDomainByStore(storeId);

            if (lstDomains == null || !lstDomains.Any())
            {
                return new List<SYSDomainModel>();
            }

            var result = lstDomains.ToModelSYSDomainList();
            var lstCertInfoIds = lstDomains.Where(m => m.CertInfoId != null && m.CertInfoId.Value > 0).Select(m => m.CertInfoId.Value).ToList();
            var lstCertInfos = new List<MGCertInfo>();
            if (lstCertInfoIds != null)
                lstCertInfos = await rpCertInfo.GetByIds(lstCertInfoIds);

            foreach (var a in result)
            {
                if (a.CertInfoId != null && a.CertInfoId.Value > 0 && lstCertInfos != null && lstCertInfos.Any())
                {
                    var objCertInfo = lstCertInfos.FirstOrDefault(m => m._id == a.CertInfoId.Value);
                    if (objCertInfo != null)
                    {
                        a.TotalDays = (int)objCertInfo.ExpireAt.Subtract(DateTime.UtcNow).TotalDays;
                        a.LastRefreshAt = objCertInfo.LastRefreshAt;
                    }
                }
                a.CanChangeDomain = await CanChangeDomain(lstDomains.FirstOrDefault(b => b._id == a.Id));
                if (a.Name.StartsWith("www."))
                {
                    var parentName = a.Name.Replace("www.", "");
                    var objParent = lstDomains.FirstOrDefault(m => m.Name == parentName);
                    a.IsChild = objParent != null;
                    if (a.IsChild == true)
                        a.ParentHttps = objParent.IsHttps;
                }

                if (a.IsSubDomain)
                {
                    a.isPointed = true;
                    a.IsHttps = true;
                }
            }
            return result;
        }
        private async Task<bool> CanChangeDomain(MGSysDomain objDomain)
        {
            if (objDomain == null || !objDomain.IsSubDomain)
                return false;

            var listAppInstalled = await rpMGAppInstalled.GetBy(ExtensionReq.StoreId);

            if (listAppInstalled != null && listAppInstalled.Count > 0)
                return false;

            if (objDomain.OldValue == null)
                return true;

            if (objDomain.OldValue.Count < DefaultData.TimeAllowChangeSubDomain)
                return true;
            else
                return false;
        }
        private async Task<List<string>> GetDomainPointToHrv(IAcmeClient acmeClient, List<string> lstDomainNames)
        {
            var result = new List<string>();
            if (lstDomainNames == null || !lstDomainNames.Any())
                return null;
            foreach (var objDomainName in lstDomainNames)
            {
                try
                {
                    var isHaravanIp = await acmeClient.VerifyHaravanIp(objDomainName);
                    if (isHaravanIp) result.Add(objDomainName);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return result;
        }
        private async Task<MGCertInfo> AddCertDomain(IAcmeClient acmeClient, List<string> lstDomainNames, bool isThrowError)
        {
            MGCertInfo certInfo = null;
            try
            {
                if (lstDomainNames != null && lstDomainNames.Any())
                {
                    var cert = await acmeClient.GetNewCert(lstDomainNames.ToArray());
                    foreach (var objDomainName in lstDomainNames)
                    {
                        var certPath = $"{objDomainName}_c";
                        var privateKeyPath = $"{objDomainName}_k";

                        var rpCert = await rpEtcdApi.CallEtcdApi(cert.Cert, certPath);
                        if (rpCert == false)
                        {
                            if (isThrowError)
                            {
                                context.AddError("errors.domain.has_errors", "Đã có lỗi xảy ra.Vui lòng thử lại sau");
                            }
                            return null;
                        }
                        var rpPrivateKey = await rpEtcdApi.CallEtcdApi(cert.PrivateKey, privateKeyPath);
                        if (rpPrivateKey == false)
                        {
                            if (isThrowError)
                            {
                                context.AddError("errors.domain.has_errors", "Đã có lỗi xảy ra.Vui lòng thử lại sau");
                            }
                            return null;
                        }
                    }

                    certInfo = new MGCertInfo()
                    {
                        Cert = cert.Cert,
                        CertRequest = cert.CertRequest,
                        PrivateKey = cert.PrivateKey,
                        IssueAt = DateTime.UtcNow,
                        ShouldRefeshAt = DateTime.UtcNow.AddDays(config.DefaultSSLRefreshDay),
                        ExpireAt = DateTime.UtcNow.AddDays(config.DefaultSSLExpireDay),
                        CreatedAt = DateTime.UtcNow,
                        UpdatedAt = DateTime.UtcNow,
                        SslType = (int)SslTypeEnum.letsencrypt
                    };
                    await rpCertInfo.SetAsync(certInfo);
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex, ex.Message);
                if (isThrowError)
                {
                    context.AddError("errors.domain.has_errors", "Đã có lỗi xảy ra.Vui lòng thử lại sau");
                }
                return null;
            }

            return certInfo;
        }
        private string GetMasterDomain(string domainName)
        {
            foreach (var ex in DefaultData.DomainExtensions)
            {
                if (domainName.EndsWith(ex))
                    return $"{domainName.Substring(0, domainName.LastIndexOf(ex)).Split('.').LastOrDefault()}{ex}";
            }
            return domainName;
        }
        private bool CheckIsMasterDomain(string domainName)
        {
            foreach (var ex in DefaultData.DomainExtensions)
            {
                if (domainName.EndsWith(ex))
                    return domainName.Replace(ex, "").Replace("www.", "").Contains(".") ? false : true;
            }
            return true;
        }
        private async Task WriteLogDashboard(int logAction, string title, long domainId)
        {
            var model = new DomainLogModel
            {
                Title = title,
                LogDate = DateTime.UtcNow,
                LogUser = ExtensionReq.UserName,
            };
            var logdata = JsonConvert.SerializeObject(model);
            var msg = new LogDataMsg
            {
                LogData = logdata,
                CreatedUser = ExtensionReq.UserId,
                CreatedUserEmail = ExtensionReq.UserEmail,
                ActionId = logAction,
                TypeId = (int)BHN.SharedObject.EBSMessage.LogType.Dashboard,
                RefId = domainId,
                StoreId = ExtensionReq.StoreId,
                CreatedDate = DateTime.UtcNow,
                Action = MessageAction.Insert,
                IsCommentLog = false,
                CreatedUserName = ExtensionReq.UserName
            };
            await bus.SendMsgAsync(msg);
        }
        #endregion private
    }
}