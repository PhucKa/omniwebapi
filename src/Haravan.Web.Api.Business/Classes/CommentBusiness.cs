﻿using BHN.Core;
using BHN.SharedObject.APIDataModel;
using BHN.SharedObject.EBSMessage;
using Haravan.Libs.Abstractions;
using Haravan.Web.Api.BusinessObjects.Enums;
using Haravan.Web.Api.BusinessObjects.ESModels;
using Haravan.Web.Api.BusinessObjects.Mappers;
using Haravan.Web.Api.BusinessObjects.Models.Comment;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using Haravan.Web.Api.Repository;
using Haravan.Web.Api.Utils.Extensions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace Haravan.Web.Api.Business
{
    public class CommentBusiness : ICommentBusiness
    {
        private readonly IRequestContext requestContext;
        private readonly IMGCommentRepository rpComment;
        
        private readonly IMGBlogRepository rpBlog;
        private readonly IMGArticleRepository rpArticle;
        private readonly IMGLogDataRepository _mgLog;
        private readonly IEBSComMessageRepository _bus;
        private readonly IExtensionPerRequest ExtensionReq;

        private readonly IESCommentRepository rpESComment;
        private readonly IESCommentSinkRepository rpESCommentSearch;
        public CommentBusiness(
                    IRequestContext requestContext,
                    IMGCommentRepository rpComment,
                    IESCommentRepository rpESComment,
                    IMGBlogRepository rpBlog,
                    IMGArticleRepository rpArticle,
                    IMGLogDataRepository _mgLog,
                    IEBSComMessageRepository _bus,
                    IExtensionPerRequest ExtensionReq,
                    IESCommentSinkRepository rpESCommentSearch
            )
        {
            this.ExtensionReq = ExtensionReq;
            this.requestContext = requestContext;
            this.rpComment = rpComment;
            this.rpBlog = rpBlog;
            this.rpESComment = rpESComment;
            this.rpArticle = rpArticle;
            this._mgLog = _mgLog;
            this._bus = _bus;
            this.rpESCommentSearch = rpESCommentSearch;
        }

        #region Internal

        public async Task<(List<CommentListModel> data, long totalrecord)> GetCommentListAsync(FilterSearchModel filter)
        {
            var result = await getMgListComment(filter);
            var data = result.data.CommentToModelList();
            if (data != null)
            {
                foreach (var comment in data)
                {
                    if (!string.IsNullOrEmpty(comment.Content))
                        comment.Content = HttpUtility.HtmlDecode(comment.Content);

                    var _articleModel = await rpArticle.GetById(ExtensionReq.StoreId, comment.Articleid);
                    comment.StatusName = ((CommentStatus)comment.Status).GetDisplayName();
                    comment.BlogTitle = _articleModel != null ? _articleModel.blogtitle : string.Empty;
                    comment.BlogId = _articleModel != null ? _articleModel.blogid : 0;
                    comment.ArticleTitle = _articleModel != null ? _articleModel.title : string.Empty;
                };
            }
            return (data, result.totalrecord);
        }

        #endregion Internal

        #region Open API

        public async Task<(List<CommentAPIModel> comments, long totalRecord)> OpenApi_GetCommentList(int limit, int page, long since_id, DateTime? created_at_min, DateTime? created_at_max, DateTime? updated_at_min, DateTime? updated_at_max,
                                                DateTime? published_at_min, DateTime? published_at_max, string published_status, string fields, string status, long blog_Id, long article_Id)
        {
            var filter = GetCommentFilter(limit, page, since_id, created_at_min, created_at_max, updated_at_min, updated_at_max,
                                                 published_at_min, published_at_max, published_status, fields, status, blog_Id, article_Id);
            var (totalrecord, data) = await GetListCommentApiAsync(filter);
            if (requestContext.IsError)
            {
                return (null, 0);
            }
            return (data, totalrecord);
        }

        public async Task<long> OpenApi_CountComment(int limit, int page, long since_id, DateTime? created_at_min, DateTime? created_at_max, DateTime? updated_at_min, DateTime? updated_at_max,
                                                DateTime? published_at_min, DateTime? published_at_max, string published_status, string fields, string status, long blog_Id, long article_Id)
        {
            var filter = GetCommentFilter(limit, page, since_id, created_at_min, created_at_max, updated_at_min, updated_at_max,
                                                 published_at_min, published_at_max, published_status, fields, status, blog_Id, article_Id);
            var (totalrecord, data) = await GetListCommentApiAsync(filter);

            if (requestContext.IsError)
            {
                return (0);
            }
            return totalrecord;
        }

        public async Task<CommentAPIModel> OpenApi_GetCommentDetail(long commentId)
        {
            var result = await GetCommentDetailApiAsync(commentId);
            if (requestContext.IsError)
            {
                return null;
            }
            return result;
        }

        public async Task<CommentAPIModel> OpenApi_AddComment(Dictionary<string, object> dicInserted)
        {
            var commentModel = ParseDictionaryCommentApi(dicInserted);
            if (string.IsNullOrWhiteSpace(commentModel.CreatorName))
            {
                requestContext.AddError("errors.comment.user.is_empty", "Tên người bình luận không thể bỏ trống");
            }
            if (string.IsNullOrWhiteSpace(commentModel.Comment))
            {
                requestContext.AddError("errors.comment.content.is_empty", "Nội dung bình luận không thể bỏ trống");
            }
            if (string.IsNullOrWhiteSpace(commentModel.CreatorEmail) || !Regex.IsMatch(commentModel.CreatorEmail, RegexPattern.Email))
            {
                requestContext.AddError("errors.comment.email.not_valid", "Email người bình luận không hợp lệ");
            }
            if (commentModel.ArticleId <= 0)
            {
                requestContext.AddError("errors.comment.articleid.not_valid", "Không thể bỏ trống");
            }
            //var customerIp = HttpContext.Current.Request.UserHostAddress;
            //var userAgent = HttpContext.Current.Request.UserAgent;
            commentModel.BrowserIP = requestContext.ClientIp;
            commentModel.UserAgent = requestContext.ClientAgent;

            var objComment = await CreateCommentApiAsync(dicInserted, commentModel);
            if (requestContext.IsError)
            {
                return null;
            }
            return objComment;
        }

        public async Task<CommentAPIModel> OpenApi_UpdateComment(long commentId, Dictionary<string, object> dicUpdated)
        {
            var result = await UpdateCommentApiAsync(commentId, dicUpdated);
            if (requestContext.IsError)
            {
                return null;
            }
            return result;
        }

        public async Task<CommentAPIModel> OpenApi_UpdateCommentStatus(long commentId, int status)
        {
            var result = await UpdateCommentStatusApiAsync(commentId, status);
            if (requestContext.IsError)
            {
                return null;
            }
            return result;
        }

        public async Task<bool> OpenApi_UpdateCommentDeleted(long commentId, bool isDeleted)
        {
            var result = await UpdateCommentDeletedApiAsync(commentId, isDeleted);
            if (requestContext.IsError)
            {
                return false;
            }
            return true;
        }

        #endregion Open API

        #region Api

        public async Task<long> CountCommentsApiAsync(FilterSearchModel model)
        {
            if (model == null)
            {
                requestContext.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return 0;
            }
            else
            {
                if (model.Fields == null && model.SortFieldName == null && model.SortType == null)
                {
                    model.SortFieldName = "CreatedAt";
                    model.SortType = "desc";
                }
                else if (model.SortFieldName != null && model.SortType == null)
                {
                    model.SortType = "desc";
                }
                else if (model.SortFieldName == null && model.SortType != null)
                {
                    model.SortFieldName = "CreatedAt";
                }
                var rs = await rpESCommentSearch.SearchAsync(ExtensionReq.StoreId, model);
                return rs.Total;
            }
        }

        public async Task<(long, List<CommentAPIModel>)> GetListCommentApiAsync(FilterSearchModel model)
        {
            long totalRecord = 0;
            List<CommentAPIModel> result = new List<CommentAPIModel>();
            if (model == null)
            {
                requestContext.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                totalRecord = 0;
            }
            else
            {
                if (model.Fields == null && model.SortFieldName == null && model.SortType == null)
                {
                    model.SortFieldName = "CreatedAt";
                    model.SortType = "desc";
                }
                else if (model.SortFieldName != null && model.SortType == null)
                {
                    model.SortType = "desc";
                }
                else if (model.SortFieldName == null && model.SortType != null)
                {
                    model.SortFieldName = "CreatedAt";
                }
                var dicIds = new Dictionary<int, long>();
                var rs = await rpESCommentSearch.SearchAsync(ExtensionReq.StoreId, model);

                totalRecord = rs.Total;
                dicIds = rs.Ids;
                if (dicIds != null && dicIds.Any())
                {
                    var lst = await _priGetByIds(ExtensionReq.StoreId, dicIds);
                    var data = lst.CommentToModelList();
                    if (data != null)
                    {
                        foreach (var comment in data)
                        {
                            if (!string.IsNullOrEmpty(comment.Content))
                            {
                                comment.Content = HttpUtility.HtmlDecode(comment.Content);
                            }
                            var _articleModel = await rpArticle.GetById(ExtensionReq.StoreId, comment.Articleid);
                            comment.StatusName = ((CommentStatus)comment.Status).GetDisplayName();
                            comment.BlogTitle = _articleModel != null ? _articleModel.blogtitle : string.Empty;
                            comment.BlogId = _articleModel != null ? _articleModel.blogid : 0;
                            comment.ArticleTitle = _articleModel != null ? _articleModel.title : string.Empty;
                        };
                    }
                    result = data.CommentAPIToModelMGComment();
                }
            }
            return (totalRecord, result);
        }

        public async Task<CommentAPIModel> GetCommentDetailApiAsync(long commentId, bool isDeleted = false)
        {
            var data = new CommentAPIModel();
            if (commentId <= 0)
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var objDetail = await rpComment.GetDetailById(commentId, ExtensionReq.StoreId, isDeleted);
            if (objDetail == null)
            {
                requestContext.AddError("errors.comment.not_valid", "Bình luận không tồn tại");
                return null;
            }
            data = objDetail.CommentAPIToModelMGCommentDetail();

            if (!string.IsNullOrEmpty(data.body_html))
            {
                data.body_html = HttpUtility.HtmlDecode(data.body_html);
            }
            var _articleModel = await rpArticle.GetById(ExtensionReq.StoreId, data.article_id);
            data.blog_id = _articleModel != null ? _articleModel.blogid : 0;
            return data;
        }

        public async Task<CommentAPIModel> CreateCommentApiAsync(Dictionary<string, object> dicInserted, CommentDetailModel model)
        {
            if (dicInserted == null || dicInserted.Count == 0 || model == null)
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ!");
                return null;
            }
            if (String.IsNullOrWhiteSpace(model.CreatorName))
            {
                requestContext.AddError("errors.comment.user_name.is_empty", "Tên người tạo bình luận không được bỏ trống");
                return null;
            }
            if (String.IsNullOrWhiteSpace(model.Comment))
            {
                requestContext.AddError("errors.comment.content.not_empty", "Nội dung bình luận không được bỏ trống");
                return null;
            }
            if (String.IsNullOrWhiteSpace(model.CreatorEmail))
            {
                requestContext.AddError("errors.comment.email.not_empty", "Email người tạo không được bỏ trống");
                return null;
            }

            model.CreatorEmail = model.CreatorEmail.Trim().ToLower();
            var isValidEmail = model.CreatorEmail.IsValidEmail(255);
            if (!isValidEmail)
            {
                requestContext.AddError("errors.comment.email.author.not_valid", "Email người tạo không hợp lệ");
                return null;
            }

            if (model.ArticleId <= 0)
            {
                requestContext.AddError("errors.comment.article_code.not_valid", "Mã bài viết không hợp lệ");
                return null;
            }
            if (dicInserted.ContainsKey("blog_id") && model.BlogId <= 0)
            {
                requestContext.AddError("errors.comment.blog_code.not_valid", "Mã blog không hợp lệ");
                return null;
            }
            var objArticle = await rpArticle.GetById(ExtensionReq.StoreId, model.ArticleId);
            if (objArticle == null || objArticle.storeid != ExtensionReq.StoreId)
            {
                requestContext.AddError("errors.article.not_exist", "Bài viết không tồn tại");
                return null;
            }
            if (objArticle.blogid != model.BlogId && dicInserted.ContainsKey("blog_id"))
            {
                requestContext.AddError("errors.comment.blog_code.not_valid", "Mã blog không hợp lệ");
                return null;
            }

            MGCommentModel objComment = new MGCommentModel()
            {
                articleid = objArticle._id,
                browseriP = model.BrowserIP,
                user_agent = model.UserAgent,
                created_at = DateTime.UtcNow,
                storeid = ExtensionReq.StoreId,
                content = model.Comment,
                author = model.CreatorName,
                email = model.CreatorEmail.Trim().ToLower(),
                status = (int)CommentStatus.Unapprove,
                publich_at = DateTime.UtcNow,
                update_at = DateTime.UtcNow
            };
            var mgcomment = await rpComment.SetAsync(objComment);
            await _bus.FireStoreDataChange(ExtensionReq.OrgId);
            return await GetCommentDetailApiAsync(mgcomment._id);
        }

        public async Task<CommentAPIModel> UpdateCommentApiAsync(long commentId, Dictionary<string, object> dicUpdated)
        {
            if (commentId <= 0 || dicUpdated == null || dicUpdated.Count == 0)
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var objComment = await rpComment.GetDetailById(commentId, ExtensionReq.StoreId);
            if (objComment == null)
            {
                requestContext.AddError("errors.comment.not_valid", "Bình luận không tồn tại");
                return null;
            }
            if (dicUpdated.ContainsKey("body"))
            {
                if (string.IsNullOrWhiteSpace(dicUpdated["body"].ToString()))
                {
                    requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                    return null;
                }
                objComment.content = dicUpdated["body"].ToString();
            }
            if (dicUpdated.ContainsKey("body_html"))
            {
                if (string.IsNullOrWhiteSpace(dicUpdated["body_html"].ToString()))
                {
                    requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                    return null;
                }
                objComment.content = dicUpdated["body_html"].ToString();
            }
            if (objComment.articleid <= 0)
            {
                requestContext.AddError("errors.article.not_exist", "Bài viết không tồn tại");
            }
            var _articleModel = await rpArticle.GetById(ExtensionReq.StoreId, objComment.articleid);
            var mgcomment = await rpComment.SetAsync(objComment);
            await _bus.FireStoreDataChange(ExtensionReq.OrgId);
            return await GetCommentDetailApiAsync(mgcomment._id);
        }

        public async Task<CommentAPIModel> UpdateCommentStatusApiAsync(long commentId, int status)
        {
            if (commentId <= 0)
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var modelcomment = await rpComment.GetDetailById(commentId, ExtensionReq.StoreId);
            if (modelcomment == null)
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            if (modelcomment.articleid <= 0)
            {
                requestContext.AddError("errors.article.not_exist", "Bài viết không tồn tại");
            }
            var _articleModel = await rpArticle.GetById(ExtensionReq.StoreId, modelcomment.articleid);
            modelcomment.status = status;
            var mgcomment = await rpComment.SetAsync(modelcomment);
            await _bus.FireStoreDataChange(ExtensionReq.OrgId);
            return await GetCommentDetailApiAsync(mgcomment._id);
        }

        public async Task<bool> CommentDeletedApiAsync(long commentId)
        {
            if (commentId <= 0)
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return false;
            }
            var lstComments = await rpComment.GetDetailById(commentId, ExtensionReq.StoreId);
            await _priDelete(lstComments);
            await _bus.FireStoreDataChange(ExtensionReq.OrgId);
            return true;
        }

        public async Task<CommentAPIModel> UpdateCommentDeletedApiAsync(long commentId, bool isDeleted)
        {
            if (commentId <= 0)
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var modelComment = await rpComment.GetDetailById(commentId, ExtensionReq.StoreId);
            if (modelComment == null || modelComment.storeid != ExtensionReq.StoreId)
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            if (isDeleted)
                modelComment.isdeleted = true;
            else
            {
                modelComment.isdeleted = false;
                modelComment.status = (int)CommentStatus.Approved;
            }
            var _articleModel = await rpArticle.GetById(ExtensionReq.StoreId, modelComment.articleid);
            var mgcomment = await rpComment.SetAsync(modelComment);
            await _bus.FireStoreDataChange(ExtensionReq.StoreId);
            return await GetCommentDetailApiAsync(mgcomment._id, isDeleted);
        }

        #endregion Api

        #region public

        public async Task ApproveByChangeBlogCommentRuleAsync(long blogId)
        {
            var articlesByBlog = await rpArticle.GetListByBlogId(blogId, ExtensionReq.StoreId);
            var articleIds = articlesByBlog.ToList();
            var commentUnapprove = await rpComment.GetCommentsByArticleIds(articleIds, ExtensionReq.StoreId, (int)CommentStatus.Unapprove);
            foreach (var comment in commentUnapprove)
            {
                comment.status = (int)CommentStatus.Approved;
            }
        }

        public async Task<List<CommentListModel>> GetDetailAsync(long articleId)
        {
            var resultModel = new List<CommentListModel>();
            if (articleId <= 0)
            {
                requestContext.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return null;
            }
            var resul = await rpComment.GetByArticleId(articleId, ExtensionReq.StoreId);
            resultModel = resul.CommentToModelList().ToList();
            foreach (var comment in resultModel)
            {
                if (!string.IsNullOrEmpty(comment.Content))
                {
                    comment.Content = HttpUtility.HtmlDecode(comment.Content);
                }
                var _blogmodel = await rpBlog.GetById(ExtensionReq.StoreId, comment.BlogId);
                var _articleModel = await rpArticle.GetById(ExtensionReq.StoreId, comment.Articleid);
                comment.StatusName = ((CommentStatus)comment.Status).GetDisplayName();
                comment.BlogTitle = _blogmodel != null ? _blogmodel.title : string.Empty;
                comment.ArticleTitle = _articleModel != null ? _articleModel.title : string.Empty;
            };
            return resultModel;
        }

        public async Task<(List<CommentListModel> data, long totalrecord)> GetDetailAsync(long commentId, bool spam, bool noSpam, int page, int limit)
        {
            var searchModel = new FilterSearchModel();
            if (spam)
            {
                searchModel = new FilterSearchModel
                {
                    Fields = new List<FilterSearchField>
                {
                    new FilterSearchField()
                    {
                        FieldName = "ArticleId",
                        IsNummeric= true,
                        NummericOperator= "=",
                        NummericValue= commentId.ToString()
                    }
                },
                    Page = new FilterSearchPage
                    {
                        currentPage = page,
                        pageSize = limit
                    },
                };
            }
            if (noSpam)
            {
                searchModel = new FilterSearchModel
                {
                    Fields = new List<FilterSearchField>
                {
                    new FilterSearchField()
                    {
                        FieldName = "ArticleId",
                        IsNummeric= true,
                        NummericOperator= "=",
                        NummericValue= commentId.ToString()
                    },
                    new FilterSearchField()
                    {
                        FieldName = "Status",
                        IsNummeric= true,
                        NummericOperator= "<>",
                        NummericValue = "2"
                    }
                },
                    Page = new FilterSearchPage
                    {
                        currentPage = page,
                        pageSize = limit
                    },
                };
            }

            long totalRecord = 0;
            var data = new List<CommentListModel>();

            var rs = new ESSearchResult();
            rs = await rpESCommentSearch.SearchAsync(ExtensionReq.OrgId, searchModel);
            var result = await _priGetByIds(ExtensionReq.OrgId, rs.Ids);
            totalRecord = rs.Total;
            data = result.CommentToModelList().ToList();

            foreach (var comment in data)
            {
                if (!string.IsNullOrEmpty(comment.Content))
                    comment.Content = HttpUtility.HtmlDecode(comment.Content);

                var _articleModel = await rpArticle.GetById(ExtensionReq.StoreId, comment.Articleid);
                comment.StatusName = ((CommentStatus)comment.Status).GetDisplayName();
                comment.BlogTitle = _articleModel != null ? _articleModel.blogtitle : string.Empty;
                comment.BlogId = _articleModel != null ? _articleModel.blogid : 0;
                comment.ArticleTitle = _articleModel != null ? _articleModel.title : string.Empty;
            };
            return (data, totalRecord);
        }

        public async Task<long> GetNumberOfUnapproveAsync()
        {
            var searchModel = new FilterSearchModel
            {
                Fields = new List<FilterSearchField>
                {
                    new FilterSearchField()
                    {
                        FieldName = "Status",
                        HasOptions = true,
                        IsNummeric = false,
                        OptionValue = "1"
                    },
                    new FilterSearchField()
                    {
                        FieldName = "IsDeleted",
                        HasOptions = true,
                        OptionValue = "No"
                    }
                },
                Page = new FilterSearchPage
                {
                    currentPage = 1,
                    pageSize = 10
                },
                ViewId = SysView.Comment
            };
            var rs = await rpESCommentSearch.SearchAsync(ExtensionReq.StoreId, searchModel);

            return rs.Total;
        }

        public async Task<bool> DeleteCommentsAsync(long[] ids)
        {
            if (ids == null || !ids.Any())
            {
                requestContext.AddError("errors.information.no_valid", "Thông tin không hợp lệ.");
                return false;
            }
            foreach (var id in ids)
            {
                var lstComments = await rpComment.GetDetailById(id, ExtensionReq.StoreId);
                await _priDelete(lstComments);
            }
            await _bus.FireStoreDataChange(ExtensionReq.OrgId);
            return true;
        }

        private async Task _priDelete(MGCommentModel model)
        {
            await rpComment.DeleteByIdAsync(ExtensionReq.OrgId, model._id);
            await rpESComment.RemoveAsync(ExtensionReq.OrgId, model._id);
            var logmodel = new MGLogData()
            {
                created_date = DateTime.UtcNow,
                created_user = ExtensionReq.UserId,
                data = JsonConvert.SerializeObject(model.CommentAPIToModelMGCommentDetail()),
                refid = model._id,
                storeid = model.storeid,
                doctypeid = (int)EnumDocumentType.Comment
            };
            await _mgLog.PostLog(logmodel);
        }

        public async Task<bool> CommentRestoreApiAsync(long commentId)
        {
            if (commentId <= 0)
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return false;
            }
            var objComment = await _priRestore(commentId);
            var _articleModel = await rpArticle.GetById(ExtensionReq.StoreId, objComment.articleid);
            var mgcomment = await rpComment.SetAsync(objComment);
            await _mgLog.DeleteByIdAsync(ExtensionReq.StoreId, commentId, (int)EnumDocumentType.Comment);
            await _bus.FireStoreDataChange(ExtensionReq.OrgId);
            return true;
        }

        private async Task<MGCommentModel> _priRestore(long CommentId)
        {
            if (CommentId <= 0)
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var model = new MGCommentModel();
            var _logmodel = await _mgLog.GetById(ExtensionReq.StoreId, CommentId, (int)EnumDocumentType.Comment);
            if (_logmodel != null)
            {
                int _status = 1;
                var _datarestore = JsonConvert.DeserializeObject<CommentAPIModel>(_logmodel.data);

                if (_datarestore.status == "2")
                {
                    _status = (int)CommentStatus.Spam;
                }
                else if (_datarestore.status == "3")
                {
                    _status = (int)CommentStatus.Approved;
                }
                else
                {
                    _status = (int)CommentStatus.Unapprove;
                }
                MGCommentModel objComment = new MGCommentModel()
                {
                    _id = _logmodel.refid,
                    articleid = _datarestore.article_id,
                    browseriP = _datarestore.ip,
                    user_agent = _datarestore.user_agent,
                    created_at = _datarestore.created_at,
                    storeid = _logmodel.storeid,
                    content = _datarestore.body_html,
                    body = _datarestore.body_html,
                    author = _datarestore.author,
                    email = _datarestore.email,
                    status = _status,
                    publich_at = _datarestore.created_at,
                    update_at = _datarestore.created_at,
                };
                model = objComment;
            }

            return model;
        }

        public async Task<bool> UpdateStatusManyAsync(long[] ids, int status)
        {
            if (ids == null || !ids.Any())
            {
                requestContext.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return false;
            }
            var lstComments = await rpComment.GetBy(ids, ExtensionReq.StoreId);
            if (lstComments != null && lstComments.Any())
            {
                foreach (var comment in lstComments)
                {
                    comment.status = status;
                    comment.update_at = DateTime.Now;
                    var _articleModel = await rpArticle.GetById(ExtensionReq.StoreId, comment.articleid);
                    var mgcomment = await rpComment.SetAsync(comment);
                    await _bus.FireStoreDataChange(ExtensionReq.OrgId);
                }
            }
            return true;
        }

        public async Task<(List<CommentListModel> data, long totalrecord)> GetCommentListAsync(
                                                string query,
                                                string order,
                                                string direction,
                                                string status,
                                                int page,
                                                int limit)
        {
            var filter = BuildCommentListFilterSearch(
                                                        query,
                                                        order,
                                                        direction,
                                                        status,
                                                        page,
                                                        limit

                                                    );
            var result = await getMgListComment(filter);
            var data = result.data.CommentToModelList();
            if (data != null)
            {
                foreach (var comment in data)
                {
                    if (!string.IsNullOrEmpty(comment.Content))
                    {
                        comment.Content = HttpUtility.HtmlDecode(comment.Content);
                    }
                    var _articleModel = await rpArticle.GetById(ExtensionReq.StoreId, comment.Articleid);
                    comment.StatusName = ((CommentStatus)comment.Status).GetDisplayName();
                    comment.BlogTitle = _articleModel != null ? _articleModel.blogtitle : string.Empty;
                    comment.BlogId = _articleModel != null ? _articleModel.blogid : 0;
                    comment.ArticleTitle = _articleModel != null ? _articleModel.title : string.Empty;
                };
            }
            // BlogTitle, ArticleTitle ?
            return (data, result.totalrecord);
        }

        #endregion public

        #region private

        private async Task<(List<MGCommentModel> data, long totalrecord)> getMgListComment(FilterSearchModel filter)
        {
            long totalRecord = 0;
            var data = new List<MGCommentModel>();
            var isDefault = false;
            if (filter == null)
            {
                requestContext.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return (data, 0);
            }
            if (filter.Fields == null)
                filter.Fields = new List<FilterSearchField>();

            var fieldNames = filter.Fields.Select(p => p.FieldName).ToList();
            if (string.IsNullOrEmpty(filter.SortFieldName))
            {
                filter.SortFieldName = "Id";
            }
            if (string.IsNullOrEmpty(filter.SortType))
            {
                filter.SortType = "desc";
            }
            List<MGCommentModel> results = new List<MGCommentModel>();
            var dicIds = new Dictionary<int, long>();
            if (isDefault)
            {
                totalRecord = await rpComment.CountAllCommentsByStoreId(ExtensionReq.OrgId);
                var ids = await rpComment.GetByPagingAsync(ExtensionReq.OrgId, filter.Page.currentPage, filter.Page.pageSize);
                if (ids != null && ids.Any())
                {
                    for (int i = 0; i < ids.Count; i++)
                    {
                        dicIds.Add(i + 1, ids[i]);
                    }
                }
            }
            else
            {
                var rs = new ESSearchResult();
                rs = await rpESCommentSearch.SearchAsync(ExtensionReq.OrgId, filter);
                totalRecord = rs.Total;
                dicIds = rs.Ids;
            }
            if (dicIds != null && dicIds.Any())
            {
                data = await _priGetByIds(ExtensionReq.OrgId, dicIds);
            }
            return (data, totalRecord);
        }

        private async Task<List<MGCommentModel>> _priGetByIds(long orgid, Dictionary<int, long> dids)
        {
            var ids = dids.Select(m => m.Value).ToList();
            var result = await rpComment.GetByIdsAsync(ExtensionReq.OrgId, ids, false);
            var tmp = from v in result
                      join k in dids on v._id equals k.Value
                      orderby k.Key
                      select v;
            return tmp.ToList();
        }

        private FilterSearchModel BuildCommentListFilterSearch(
                                                            string query,
                                                            string order,
                                                            string direction,
                                                            string status,
                                                            int page,
                                                            int limit

                                                           )
        {
            var filter = new FilterSearchModel()
            {
                FreeText = query,
                Page = new FilterSearchPage()
                {
                    currentPage = page <= 0 ? 1 : page,
                    pageSize = limit <= 0 || limit > 50 ? 20 : limit
                },
                SortFieldName = "Id",
                SortType = PrepareSortType(direction),
                Fields = new List<FilterSearchField>()
            };
            if (!StringHelper.CheckStringIsNullOrEmptyOrWhitespace(status))
            {
                filter = PrepareCodStatusFilter(filter, status);
            }

            return filter;
        }

        private FilterSearchModel PrepareCodStatusFilter(FilterSearchModel model, string status)
        {
            var arrCodStatus = status.Split(',');
            foreach (var codStatus in arrCodStatus)
            {
                if (!StringHelper.CheckStringIsNullOrEmptyOrWhitespace(codStatus))
                {
                    var currentSource = codStatus.Trim();
                    switch (currentSource)
                    {
                        case "no":
                            model.Fields.Add(new FilterSearchField()
                            {
                                FieldName = "Status",
                                HasOptions = true,
                                OptionValue = "1"
                            });
                            break;

                        case "spam":
                            model.Fields.Add(new FilterSearchField()
                            {
                                FieldName = "Status",
                                HasOptions = true,
                                OptionValue = "2"
                            });
                            break;

                        case "approved":
                            model.Fields.Add(new FilterSearchField()
                            {
                                FieldName = "Status",
                                HasOptions = true,
                                OptionValue = "3"
                            });
                            break;

                        default:
                            break;
                    }
                }
            }
            return model;
        }

        

        private string PrepareSortType(string direction)
        {
            if (!StringHelper.CheckStringIsNullOrEmptyOrWhitespace(direction))
            {
                direction = direction.Trim();
                switch (direction)
                {
                    case "asc":
                        return "asc";

                    case "desc":
                        return "desc";

                    default:
                        return null;
                }
            }
            return null;
        }

        private FilterSearchModel GetCommentFilter(int limit, int page, long since_id, DateTime? created_at_min, DateTime? created_at_max, DateTime? updated_at_min, DateTime? updated_at_max,
                                                DateTime? published_at_min, DateTime? published_at_max, string published_status, string fields, string status, long blog_Id, long article_Id)
        {
            var filter = new FilterSearchModel
            {
                Fields = new List<FilterSearchField>(),
                Page = new FilterSearchPage { pageSize = limit, currentPage = page },
                ViewId = SysView.Comment
            };
            if (blog_Id > 0)
            {
                var field = new FilterSearchField { FieldName = "BlogId", NummericOperator = "=", IsNummeric = true, NummericValue = blog_Id.ToString() };
                filter.Fields.Add(field);
            }
            if (article_Id > 0)
            {
                var field = new FilterSearchField { FieldName = "ArticleId", NummericOperator = "=", IsNummeric = true, NummericValue = article_Id.ToString() };
                filter.Fields.Add(field);
            }
            if (since_id > 0)
            {
                var field = new FilterSearchField { FieldName = "Id", NummericOperator = ">", IsNummeric = true, NummericValue = since_id.ToString() };
                filter.Fields.Add(field);
            }
            if (created_at_min != null && created_at_min != DateTime.MinValue)
            {
                var field = new FilterSearchField
                {
                    FieldName = "CreatedDate",
                    OptionValue = "AfterExtract",
                    OptionValueValue = created_at_min.Value.ToString("o")
                };
                filter.Fields.Add(field);
            }
            if (created_at_max != null && created_at_max != DateTime.MinValue)
            {
                var field = new FilterSearchField
                {
                    FieldName = "CreatedDate",
                    OptionValue = "BeforeExtract",
                    OptionValueValue = created_at_max.Value.ToString("o")
                };
                filter.Fields.Add(field);
            }
            if (updated_at_min != null && updated_at_min != DateTime.MinValue)
            {
                var field = new FilterSearchField
                {
                    FieldName = "UpdatedDate",
                    OptionValue = "AfterExtract",
                    OptionValueValue = updated_at_min.Value.ToString("o")
                };
                filter.Fields.Add(field);
            }
            if (updated_at_max != null && updated_at_max != DateTime.MinValue)
            {
                var field = new FilterSearchField
                {
                    FieldName = "UpdatedDate",
                    OptionValue = "BeforeExtract",
                    OptionValueValue = updated_at_max.Value.ToString("o")
                };
                filter.Fields.Add(field);
            }
            if (published_at_min != null && published_at_min != DateTime.MinValue)
            {
                var field = new FilterSearchField
                {
                    FieldName = "UpdatedDate",
                    OptionValue = "AfterExtract",
                    OptionValueValue = published_at_min.Value.ToString("o")
                };
                filter.Fields.Add(field);
            }
            if (published_at_max != null && published_at_max != DateTime.MinValue)
            {
                var field = new FilterSearchField
                {
                    FieldName = "UpdatedDate",
                    OptionValue = "BeforeExtract",
                    OptionValueValue = published_at_max.Value.ToString()
                };
                filter.Fields.Add(field);
            }
            if (!String.IsNullOrWhiteSpace(published_status))
            {
                var _published_status = published_status;
                if (_published_status.Equals("published"))
                {
                    var field = new FilterSearchField
                    {
                        FieldName = "PublishedDate",
                        OptionValue = "BeforeEqualsNow"
                    };
                    filter.Fields.Add(field);
                }
                else if (_published_status.Equals("unpublished"))
                {
                    var field = new FilterSearchField
                    {
                        FieldName = "PublishedDate",
                        OptionValue = "AfterNow"
                    };
                    filter.Fields.Add(field);
                }
            }
            if (!string.IsNullOrEmpty(status))
            {
                switch (status)
                {
                    case "published":
                        var field = new FilterSearchField
                        {
                            FieldName = "Status",
                            OptionValue = ((int)CommentStatus.Approved).ToString()
                        };
                        filter.Fields.Add(field);
                        break;

                    case "unapproved":
                        var f = new FilterSearchField
                        {
                            FieldName = "Status",
                            OptionValue = ((int)CommentStatus.Unapprove).ToString()
                        };
                        filter.Fields.Add(f);
                        break;
                }
            }
            return filter;
        }

        private CommentDetailModel ParseDictionaryCommentApi(Dictionary<string, object> dictionaryModel)
        {
            if (dictionaryModel == null || dictionaryModel.Count == 0)
            {
                requestContext.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return null;
            }

            CommentDetailModel resultModel = new CommentDetailModel();
            var propertyInfos = typeof(CommentAPIModel).GetProperties();
            foreach (var propertyValuePair in dictionaryModel)
            {
                var colName = propertyInfos.Single(x => x.Name == propertyValuePair.Key);

                if (colName.Name.Equals("article_id"))
                {
                    resultModel.ArticleId = (long)propertyValuePair.Value;
                }
                if (colName.Name.Equals("author"))
                {
                    resultModel.CreatorName = (string)propertyValuePair.Value;
                }
                if (colName.Name.Equals("blog_id"))
                {
                    resultModel.BlogId = (long)propertyValuePair.Value;
                }
                if (colName.Name.Equals("body"))
                {
                    resultModel.Comment = (string)propertyValuePair.Value;
                }

                if (colName.Name.Equals("created_at"))
                {
                    resultModel.CreatedDate = (DateTime)propertyValuePair.Value;
                }
                if (colName.Name.Equals("email"))
                {
                    resultModel.CreatorEmail = (string)propertyValuePair.Value;
                }
                if (colName.Name.Equals("status"))
                {
                    resultModel.Status = (int)propertyValuePair.Value;
                }
            }

            return resultModel;
        }

        #endregion private
    }
}