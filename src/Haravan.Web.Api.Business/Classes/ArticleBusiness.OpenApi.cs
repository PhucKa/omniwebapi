﻿using BHN.Core;
using BHN.SharedObject.APIDataModel;
using Haravan.Web.Api.BusinessObjects.Mappers;
using Haravan.Web.Api.BusinessObjects.Models.Article;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public partial class ArticleBusiness : IArticleBusiness
    {
        #region Open_Api

        public async Task<(List<ArticleAPIModel> articles, long totalRecord)> OpenApi_GetArticlesList(long articleId, long blogId, DateTime? created_at_max, DateTime? created_at_min, string fields, int limit, int page,
                                                 int popular, DateTime? published_at_max, DateTime? published_at_min, string published_status, long since_id, DateTime? updated_at_max, DateTime? updated_at_min)
        {
            var filter = GetArticleFilter(articleId, blogId, created_at_max, created_at_min, fields, limit, page,
                                                  popular, published_at_max, published_at_min, published_status, since_id, updated_at_max, updated_at_min);
            (List<ArticleAPIModel> articles, long totalRecord) = await GetArticleAPIList(filter);
            if (requestContext.IsError) return (null, 0);
            return (articles, totalRecord);
        }

        public async Task<long> OpenApi_CountArticles(long articleId, long blogId, DateTime? created_at_max, DateTime? created_at_min, string fields, int limit, int page,
                                                 int popular, DateTime? published_at_max, DateTime? published_at_min, string published_status, long since_id, DateTime? updated_at_max, DateTime? updated_at_min)
        {
            var filter = GetArticleFilter(articleId, blogId, created_at_max, created_at_min, fields, limit, page,
                                                  popular, published_at_max, published_at_min, published_status, since_id, updated_at_max, updated_at_min);
            (ESSearchResult esResult, long totalRecord) = await SearchESArticle(filter);
            if (requestContext.IsError) return 0;
            return totalRecord;
        }

        public async Task<ArticleAPIModel> OpenApi_GetArticleDetail(long article_id)
        {
            var result = await GetDetail(article_id);
            return result.DetailToApi();
        }

        public async Task<ArticleAPIModel> OpenApi_AddArticle(Dictionary<string, object> dicInserted)
        {
            var articleModel = await ParseDictionaryArticleApi(dicInserted);
            if (requestContext.IsError)
                return null;
            var newarticle = await AddNewAsync(articleModel);
            return newarticle.DetailToApi();
        }

        public async Task<ArticleAPIModel> OpenApi_UpdateArticle(long article_id, Dictionary<string, object> model)
        {
            return await UpdateAPI(article_id, model);
        }

        public async Task<bool> OpenApi_DeleteArticle(long article_id)
        {
            return await DeleteArticleApi(article_id);
        }

        public async Task<List<string>> OpenApi_GetAuthorsList()
        {
            return await GetAuthorListAPI();
        }

        public async Task<List<string>> OpenApi_GetAllTags(long articleId, long blogId, DateTime? created_at_max, DateTime? created_at_min, string fields, int limit, int page,
                                                 int popular, DateTime? published_at_max, DateTime? published_at_min, string published_status, long since_id, DateTime? updated_at_max, DateTime? updated_at_min)
        {
            var filter = GetArticleFilter(articleId, blogId, created_at_max, created_at_min, fields, limit, page,
                                                   popular, published_at_max, published_at_min, published_status, since_id, updated_at_max, updated_at_min);
            var model = new ArticleTagsApiModel()
            {
                filter = filter,
                popular = true
            };
            return await GetAllTagsAPI(model.filter, model.popular);
        }

        public async Task<List<string>> OpenApi_GetTagsByBlogAPIAsync(long articleId, long blogId, DateTime? created_at_max, DateTime? created_at_min, string fields, int limit, int page,
                                                 int popular, DateTime? published_at_max, DateTime? published_at_min, string published_status, long since_id, DateTime? updated_at_max, DateTime? updated_at_min)
        {
            var filter = GetArticleFilter(articleId, blogId, created_at_max, created_at_min, fields, limit, page,
                                                    popular, published_at_max, published_at_min, published_status, since_id, updated_at_max, updated_at_min);
            var model = new ArticleTagsApiModel()
            {
                filter = filter,
                popular = true
            };
            return await GetTagsByBlogAPI(model.filter, model.popular);
        }

        #endregion Open_Api
    }
}