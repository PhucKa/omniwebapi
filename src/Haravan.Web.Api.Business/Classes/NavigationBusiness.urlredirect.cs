﻿using BHN.Core;
using Haravan.Web.Api.BusinessObjects.Enums;
using Haravan.Web.Api.BusinessObjects.ESModels;
using Haravan.Web.Api.BusinessObjects.Mappers;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public partial class NavigationBusiness : INavigationBusiness
    {
        #region Url Redirect

        public async Task<bool> DeleteUrlRedirectByIdAsync(long UrlRedirectId)
        {
            return await DeleteUrlRedirect(new List<long>() { UrlRedirectId });
        }

        public async Task<bool> DeleteUrlRedirect(List<long> UrlRedirectId)
        {
            if (UrlRedirectId == null || !UrlRedirectId.Any())
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return false;
            }
            foreach (var id in UrlRedirectId)
            {
                await DeleteByIdAsync(id);
            }
            return true;
        }

        public async Task<bool> DeleteByIdAsync(long UrlRedirectId)
        {
            if (UrlRedirectId < 0)
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return false;
            }
            await rpurlRedirect.DeleteById(ExtensionReq.StoreId, UrlRedirectId);
            await rpESUrlRedirect.RemoveAsync(ExtensionReq.StoreId, UrlRedirectId);
            await _bus.FireStoreDataChange(ExtensionReq.StoreId);
            return true;
        }

        public async Task<(List<UrlRedirectModel> data, long totalrecord)> GetRedirectListAsync(
                                                                            string query,
                                                                            int page,
                                                                            int limit
                                                                      )
        {
            var filter = buildFilterUrlRedirectSearchModel(
                                                    query,
                                                    page,
                                                    limit
                                                  );
            var lstdata = await GetRedirectList(filter);
            return (lstdata.Item1, lstdata.Item2);
        }

        public async Task<UrlRedirectModel> GetByIdUrlRedirect(long UrlRedirectId)
        {
            var resultModel = new UrlRedirectModel();
            var objUrl = await rpurlRedirect.GetByID(ExtensionReq.StoreId, UrlRedirectId);

            if (objUrl == null)
            {
                requestContext.AddError("errors.navigation.url_redirects.not_exist", "URL Redirects không tồn tại");
                return null;
            }
            resultModel = objUrl.ModelUrlToUrlMG();
            return resultModel;
        }

        public async Task<bool> DeleteUrlRedirect(UrlRedirectModel model)
        {
            var urlRedirect = await ValidateUrlRedirect(model, true);
            if (requestContext.IsError)
                return false;

            await rpurlRedirect.DeleteById(ExtensionReq.StoreId, urlRedirect._id);
            await rpESUrlRedirect.RemoveAsync(ExtensionReq.StoreId, urlRedirect._id);
            return true;
        }

        public async Task<(List<UrlRedirectModel>, long)> GetRedirectList(FilterSearchModel model)
        {
            var storeId = ExtensionReq.StoreId;
            long totalRecord = 0;
            var resultModel = new List<UrlRedirectModel>();
            var dicIds = new Dictionary<int, long>();
            if (model == null)
            {
                requestContext.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
            }
            else
            {
                var isDefault = IsGetDataFromMongo(model);
                if (isDefault)
                {
                    totalRecord = await rpurlRedirect.CountAsync(storeId);
                    var ids = await rpurlRedirect.GetByPagingAsync(storeId, model.Page.currentPage, model.Page.pageSize);
                    if (ids != null && ids.Any())
                    {
                        for (int i = 0; i < ids.Count; i++)
                        {
                            dicIds.Add(i + 1, ids[i]);
                        }
                    }
                }
                else
                {
                    if (model.Fields == null && model.SortFieldName == null && model.SortType == null)
                    {
                        model.SortFieldName = "CreatedDate";
                        model.SortType = "desc";
                    }
                    else if (model.SortFieldName != null && model.SortType == null)
                    {
                        model.SortType = "desc";
                    }
                    else if (model.SortFieldName == null && model.SortType != null)
                    {
                        model.SortFieldName = "CreatedDate";
                    }

                    var rs = await rpESUrlRedirectSearch.SearchAsync(ExtensionReq.StoreId, model);
                    totalRecord = rs.Total;
                    dicIds = rs.Ids;
                }

                if (dicIds != null && dicIds.Any())
                {
                    var result = await _priGetByIds(ExtensionReq.OrgId, dicIds);

                    resultModel = result.ModelToModelList();
                }
                if (resultModel != null)
                {
                    resultModel.ForEach(link =>
                    {
                        link.Url = link.RedirectTo;
                    });
                }
            }

            return (resultModel, totalRecord);
        }

        private bool IsGetDataFromMongo(FilterSearchModel model)
        {
            return string.IsNullOrWhiteSpace(model.FreeText)
                   && (model.Fields == null || !model.Fields.Any())
                   && string.IsNullOrWhiteSpace(model.SortFieldName)
                   && string.IsNullOrWhiteSpace(model.SortType);
        }
        public async Task<bool> UpdateUrlRedirect(UrlRedirectModel model)
        {
            var urlRedirect = await ValidateUrlRedirect(model, false, false, true);
            if (requestContext.IsError)
            {
                return false;
            }
            urlRedirect.storeid = ExtensionReq.StoreId;
            urlRedirect.updated_at = DateTime.UtcNow;
            urlRedirect.updated_user = ExtensionReq.UserId;
            await rpurlRedirect.UpdateAsync(urlRedirect, a => a.redirectto
                                                    , a => a.oldpath
                                                    , a => a.updated_user
                                                    , a => a.updated_at);
            var mgUrlRedirect = await rpurlRedirect.GetByID(ExtensionReq.StoreId, urlRedirect._id);
            var esUrlRedirect = mgUrlRedirect.MGToModelES();
            esUrlRedirect.RedirectTo = esUrlRedirect.RedirectTo.Trim();
            esUrlRedirect.OldPath = esUrlRedirect.OldPath.Trim();
            await rpESUrlRedirect.UpdateAsync(esUrlRedirect);
            await _bus.FireStoreDataChange(ExtensionReq.StoreId);
            return true;
        }

        public async Task<(bool, long)> AddUrlRedirect(UrlRedirectModel model)
        {
            var urlRedirect = await ValidateUrlRedirect(model);
            if (requestContext.IsError)
            {
                return (false, 0);
            }
            urlRedirect.storeid = ExtensionReq.StoreId;
            urlRedirect.created_at = DateTime.UtcNow;
            urlRedirect.created_user = ExtensionReq.UserId;
            urlRedirect.updated_at = DateTime.UtcNow;
            urlRedirect.updated_user = ExtensionReq.UserId;
            var mgUrlRedirect = await rpurlRedirect.SetAsync(urlRedirect);
            await _bus.FireStoreDataChange(ExtensionReq.StoreId);
            return (true, urlRedirect._id);
        }

        #endregion Url Redirect

        #region API

        public async Task<RedirectAPIModel> AddUrlRedirectApi(UrlRedirectModel model)
        {
            if (model == null || string.IsNullOrWhiteSpace(model.OldPath) || string.IsNullOrWhiteSpace(model.RedirectTo))
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ!");
                return null;
            }
            var result = await AddUrlRedirect(model);
            if (!result.Item1 || result.Item2 == 0) return null;
            else return await GetUrlRedirectDetailApi(result.Item2);
        }

        public async Task<RedirectAPIModel> GetUrlRedirectDetailApi(long id)
        {
            var model = new RedirectAPIModel();
            if (id <= 0)
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var objRedirect = await rpurlRedirect.GetByID(ExtensionReq.StoreId, id);

            if (objRedirect == null || objRedirect.storeid != ExtensionReq.StoreId)
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            model = objRedirect.RedirectAPIToMGUrlRedirectmodel();
            return model;
        }

        public async Task<List<RedirectAPIModel>> GetUrlRedirectListApi(FilterSearchModel model)
        {
            if (model == null)
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var result = await GetRedirectList(model);

            if (result.Item1 != null)
                return result.Item1.RedirectAPIToUrlRedirectModel();
            else return null;
        }

        public async Task<long> CountUrlRedirects(FilterSearchModel model)
        {
            if (model == null)
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return 0;
            }
            else
            {
                if (model.Fields == null && model.SortFieldName == null && model.SortType == null)
                {
                    model.SortFieldName = "CreatedDate";
                    model.SortType = "desc";
                }
                else if (model.SortFieldName != null && model.SortType == null)
                {
                    model.SortType = "desc";
                }
                else if (model.SortFieldName == null && model.SortType != null)
                {
                    model.SortFieldName = "CreatedDate";
                }
                var _storeId = ExtensionReq.OrgId;
                var rs = await rpESUrlRedirectSearch.SearchAsync(_storeId, model);

                return rs.Total;
            }
        }

        public async Task<bool> DeleteUrlRedirectApi(long id)
        {
            if (id <= 0)
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return false;
            }
            var objRedirect = await rpurlRedirect.GetByID(ExtensionReq.StoreId, id);
            if (objRedirect == null || objRedirect.storeid != ExtensionReq.StoreId)
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return false;
            }
            await rpurlRedirect.DeleteById(ExtensionReq.StoreId, objRedirect._id);
            await rpESUrlRedirect.RemoveAsync(ExtensionReq.StoreId, objRedirect._id);
            await _bus.FireStoreDataChange(ExtensionReq.StoreId);
            return true;
        }

        public async Task<RedirectAPIModel> UpdateUrlRedirectApi(long id, Dictionary<string, object> dicUpdated)
        {
            if (id <= 0 || dicUpdated == null || dicUpdated.Count == 0)
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var objRedirect = await rpurlRedirect.GetByID(ExtensionReq.StoreId, id);
            if (objRedirect == null)
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var propertyInfos = typeof(RedirectAPIModel).GetProperties();
            foreach (var propertyValuePair in dicUpdated)
            {
                var colName = propertyInfos.Single(x => x.Name == propertyValuePair.Key);
                if (colName.Name.Equals("path"))
                {
                    string strPath = (String)propertyValuePair.Value;
                    if (String.IsNullOrEmpty(strPath))
                    {
                        requestContext.AddError("errors.navigation.url_redirects_path.not_empty", "Path không được bỏ trống");
                        return null;
                    }
                    if (strPath.Trim().Length > 1041)
                    {
                        requestContext.AddError("errors.navigation.url_redirects_path_old.max_characters", "1|Đường dẫn cũ không nên nhập quá nhiều kí tự");
                        return null;
                    }
                    if (!IsUrlValid(strPath.Trim(), UriKind.Relative))
                    {
                        strPath = Uri.EscapeUriString(strPath.Trim());
                    }
                    else
                        objRedirect.oldpath = strPath.Trim();
                }
                if (colName.Name.Equals("target"))
                {
                    string strRedirect = (String)propertyValuePair.Value;
                    if (String.IsNullOrEmpty(strRedirect))
                    {
                        requestContext.AddError("errors.navigation.url_redirects_target.not_empty", "Target không được bỏ trống");
                        return null;
                    }
                    if (strRedirect.Trim().Length > 1041)
                    {
                        requestContext.AddError("errors.navigation.website_link.max_characters", "Website liên kết không nên nhập quá nhiều kí tự");
                        return null;
                    }
                    if (!IsUrlValid(strRedirect.Trim(), UriKind.Absolute) && !IsUrlValid(strRedirect.Trim(), UriKind.Relative))
                    {
                        if (strRedirect.Trim().Contains("http://") && strRedirect.Trim().IndexOf("http://") != 0)
                        {
                            requestContext.AddError("errors.navigation.website_link.no_valid", "Website liên kết đến không hợp lệ");
                            return null;
                        }
                        else if (strRedirect.Trim().Contains("https://") && strRedirect.Trim().IndexOf("https://") != 0)
                        {
                            requestContext.AddError("errors.navigation.website_link.no_valid", "Website liên kết đến không hợp lệ");
                            return null;
                        }
                        strRedirect = Uri.EscapeUriString(strRedirect.Trim());
                    }
                    else
                        objRedirect.redirectto = strRedirect.Trim();
                }
            }
            var _modelUrl = objRedirect.ModelUrlToUrlMG();
            var urlRedirect = await ValidateUrlRedirect(_modelUrl, false, false, true);
            if (requestContext.IsError)
            {
                return null;
            }
            urlRedirect.storeid = ExtensionReq.StoreId;
            urlRedirect.updated_at = DateTime.UtcNow;
            urlRedirect.updated_user = ExtensionReq.UserId;
            await rpurlRedirect.UpdateAsync(urlRedirect, a => a.redirectto
                                                    , a => a.oldpath
                                                    , a => a.updated_user
                                                    , a => a.updated_at);
            var mgUrlRedirect = await rpurlRedirect.GetByID(ExtensionReq.StoreId, objRedirect._id);
            var esUrlRedirect = mgUrlRedirect.MGToModelES();
            esUrlRedirect.RedirectTo_ori = esUrlRedirect.RedirectTo;
            esUrlRedirect.OldPath_ori = esUrlRedirect.OldPath;
            await rpESUrlRedirect.UpdateAsync(esUrlRedirect);
            await _bus.FireStoreDataChange(ExtensionReq.StoreId);
            return await GetUrlRedirectDetailApi(id);
        }

        #endregion API

        #region Open API

        public async Task<List<RedirectAPIModel>> OpenApi_GetRedirectList(int limit, int page, long since_id, string path, string target, string fields)
        {
            var filter = GetUrlRedirectFilter(limit, page, since_id, path, target, fields);
            var data = await GetUrlRedirectListApi(filter);
            if (requestContext.IsError)
            {
                return (null);
            }
            return data;
        }

        public async Task<long> OpenApi_CountRedirect(int limit, int page, long since_id, string path, string target, string fields)
        {
            var filter = GetUrlRedirectFilter(limit, page, since_id, path, target, fields); ;
            var totalrecord = await CountUrlRedirects(filter);
            if (requestContext.IsError)
            {
                return (0);
            }
            return totalrecord;
        }

        public async Task<RedirectAPIModel> OpenApi_GetRedirectDetail(long RedirectId)
        {
            var result = await GetUrlRedirectDetailApi(RedirectId);
            if (requestContext.IsError)
            {
                return null;
            }
            return result;
        }

        public async Task<RedirectAPIModel> OpenApi_AddRedirect(Dictionary<string, object> dicInserted)
        {
            var redirectModel = ParseDictionaryRedirectApi(dicInserted);
            if (redirectModel == null)
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
            }
            if (string.IsNullOrWhiteSpace(redirectModel.OldPath))
            {
                requestContext.AddError("errors.navigation.url_redirects_path.not_empty", "Path không thể bỏ trống");
            }
            if (string.IsNullOrWhiteSpace(redirectModel.RedirectTo))
            {
                requestContext.AddError("errors.navigation.url_redirects_target.not_empty", "Target không thể bỏ trống");
            }
            var objComment = await AddUrlRedirectApi(redirectModel);
            if (requestContext.IsError)
            {
                return null;
            }
            return objComment;
        }

        public async Task<RedirectAPIModel> OpenApi_UpdateRedirect(long RedirectId, Dictionary<string, object> dicUpdated)
        {
            var result = await UpdateUrlRedirectApi(RedirectId, dicUpdated);
            if (requestContext.IsError)
            {
                return null;
            }
            return result;
        }

        public async Task<bool> OpenApi_RedirectDeleted(long RedirectId)
        {
            var result = await DeleteUrlRedirectApi(RedirectId);
            if (requestContext.IsError)
            {
                return false;
            }
            return true;
        }

        #endregion Open API

        #region private

        private async Task<MGUrlRedirectModel> ValidateUrlRedirect(UrlRedirectModel model, bool isDelete = false, bool isApi = false, bool isUpdate = false)
        {
            if (model == null)
            {
                requestContext.AddError("errors.navigation.information_missing_or_invalid", "Thông tin còn thiếu hoặc không hợp lệ");
                return null;
            }

            var resultModel = model.MGUrlToUrlModel();

            if (isDelete)
            {
                if (resultModel._id <= 0)
                {
                    requestContext.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                    return null;
                }

                var urlRedirect = await rpurlRedirect.GetByID(ExtensionReq.StoreId, resultModel._id);

                if (urlRedirect == null)
                {
                    requestContext.AddError("errors.navigation.not_setup_not_delete", "Không tìm thấy thiết lập chuyển tiếp. Hủy thao tác xóa");
                    return null;
                }
            }
            else
            {
                if (string.IsNullOrEmpty(resultModel.oldpath))
                {
                    requestContext.AddError("errors.navigation.not_update_link_old", "Chưa nhập thông tin đường dẫn cũ");
                }
                if (string.IsNullOrEmpty(resultModel.redirectto))
                {
                    requestContext.AddError("errors.navigation.not_information_next", "Chưa nhập thông tin chuyển tiếp");
                }
                if (requestContext.IsError)
                    return null;

                if (resultModel.redirectto.Length > 1041)
                {
                    requestContext.AddError("errors.navigation.website_link.max_characters", "Website liên kết đến không nên nhập quá nhiều kí tự");
                }
                if (resultModel.oldpath.Length > 1041)
                {
                    requestContext.AddError("errors.navigation.url_redirects_path_old.max_characters", "Đường dẫn cũ không nên nhập quá nhiều kí tự");
                }

                if (!IsUrlValid(resultModel.oldpath, UriKind.Relative))
                {
                    if (resultModel.oldpath.Contains("http://") || resultModel.oldpath.Contains("https://"))
                    {
                        requestContext.AddError("errors.navigation.website_link_old.no_valid", "Đường dẫn cũ không hợp lệ");
                    }

                    resultModel.oldpath = Uri.EscapeUriString(resultModel.oldpath.Trim());
                }
                else
                {
                    resultModel.oldpath = resultModel.oldpath.Trim();
                }

                if (resultModel.oldpath.Contains("http:/"))
                {
                    resultModel.oldpath = resultModel.oldpath.Replace("http:/", "");
                }
                else if (resultModel.oldpath.Contains("https:/"))
                {
                    resultModel.oldpath = resultModel.oldpath.Replace("https:/", "");
                }

                if (!IsUrlValid(resultModel.redirectto, UriKind.Absolute) && !IsUrlValid(resultModel.redirectto, UriKind.Relative))
                {
                    if (resultModel.redirectto.Contains("http://") && resultModel.redirectto.IndexOf("http://") != 0)
                    {
                        requestContext.AddError("errors.navigation.website_link.no_valid", "Website liên kết đến không hợp lệ");
                    }
                    else if (resultModel.redirectto.Contains("https://") && resultModel.redirectto.IndexOf("https://") != 0)
                    {
                        requestContext.AddError("errors.navigation.website_link.no_valid", "Website liên kết đến không hợp lệ");
                    }
                    else
                        resultModel.redirectto = Uri.EscapeUriString(resultModel.redirectto.Trim());
                }
                else
                {
                    resultModel.redirectto = resultModel.redirectto.Trim();
                }

                if (resultModel.oldpath.Equals(resultModel.redirectto))
                {
                    requestContext.AddError("errors.navigation.website_link_not_duplicate_link_old", "Website liên kết đến không được trùng đường dẫn cũ");
                }

                // Check if input oldPath duplicate with RedirectTo in DB
                // this will cause loop Redirect
                var isLoopRedirect = await rpurlRedirect.GetByOldPath(ExtensionReq.StoreId, resultModel.oldpath);
                if (isLoopRedirect != null)
                {
                    requestContext.AddError("errors.navigation.link_old_not_duplicate_link_system", "Đường dẫn cũ không được cấu hình trùng với đường dẫn chuyển tiếp trong hệ thống");
                    return null;
                }

                // Make valid OldPath. Valid OldPath has correct format like "/oldpath"
                if (!resultModel.oldpath.Substring(0, 1).Equals("/"))
                    resultModel.oldpath = "/" + resultModel.oldpath;

                if (!resultModel.redirectto.Contains("http://")
                    && !resultModel.redirectto.Contains("https://")
                    && !resultModel.redirectto.Substring(0, 1).Equals("/"))
                {
                    resultModel.redirectto = "/" + resultModel.redirectto;
                }

                // Check if input OldPath existed
                var existedUrl = await rpurlRedirect.GetByOldPath(ExtensionReq.StoreId, resultModel.oldpath);
                if (existedUrl != null)
                {
                    var existoldpath = await CheckExisteroldpath(resultModel.oldpath, isUpdate);
                    if (existoldpath == false)
                    {
                        requestContext.AddError("errors.navigation.link_old.not_exist", "Đường dẫn cũ đã được cấu hình");
                        return null;
                    }
                }

                // Check if input newPath duplicate with oldPath in DB
                // this will cause loop Redirect
                var checkRedirectTo = resultModel.redirectto;
                if (!checkRedirectTo.Substring(0, 1).Equals("/"))
                    checkRedirectTo = "/" + checkRedirectTo;

                var isLoopOldUrl = await rpurlRedirect.GetByOldPath(ExtensionReq.StoreId, checkRedirectTo);
                if (isLoopOldUrl != null)
                {
                    var existRedirectTo = await CheckExisteRedirectTo(checkRedirectTo, isUpdate);
                    if (existRedirectTo == false)
                    {
                        requestContext.AddError("errors.navigation.link_not_duplicate_link_system", "Đường dẫn chuyển tiếp không được cấu hình trùng với đường dẫn cũ trong hệ thống");
                        return null;
                    }
                }
            }

            return resultModel;
        }

        private async Task<bool> CheckExisteRedirectTo(string RedirectTo, bool isUpdate)
        {
            var listexistedoldpath = await rpurlRedirect.GetListoldPath(ExtensionReq.StoreId);
            if (listexistedoldpath != null)
            {
                foreach (var itemoldpath in listexistedoldpath)
                {
                    if (isUpdate)
                    {
                        if (RedirectTo == itemoldpath.oldpath || RedirectTo != itemoldpath.oldpath)
                            return true;
                        else
                            return false;
                    }
                    else
                    {
                        if (RedirectTo == itemoldpath.oldpath)
                            return false;
                        else
                            return true;
                    }
                }
            }
            return true;
        }

        private async Task<bool> CheckExisteroldpath(string oldpath, bool isUpdate)
        {
            var listexistedoldpath = await rpurlRedirect.GetListoldPath(ExtensionReq.StoreId);
            if (listexistedoldpath != null)
            {
                foreach (var itemoldpath in listexistedoldpath)
                {
                    if (isUpdate)
                    {
                        if (oldpath == itemoldpath.oldpath || oldpath != itemoldpath.oldpath)
                            return true;
                        else
                            return false;
                    }
                    else
                    {
                        if (oldpath == itemoldpath.oldpath)
                            return false;
                        else
                            return true;
                    }
                }
            }
            return true;
        }

        private bool IsUrlValid(string url, UriKind _uriKind)
        {
            Uri uriResult;
            bool isCreatedUriSuccess = false, isWellFormedUri = false;

            isCreatedUriSuccess = Uri.TryCreate(url, _uriKind, out uriResult);

            if (_uriKind == UriKind.Absolute)
            {
                isCreatedUriSuccess = isCreatedUriSuccess && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
            }
            isWellFormedUri = isCreatedUriSuccess && (Uri.IsWellFormedUriString(uriResult.ToString(), _uriKind) || Uri.IsWellFormedUriString(url, _uriKind));
            return isWellFormedUri && isCreatedUriSuccess;
        }

        private FilterSearchModel buildFilterUrlRedirectSearchModel(
                                                        string query,
                                                        int page,
                                                        int limit
                                                           )
        {
            var filter = new FilterSearchModel()
            {
                FreeText = query,
                Page = new FilterSearchPage()
                {
                    currentPage = page <= 0 ? 1 : page,
                    pageSize = limit <= 0 || limit > 50 ? 50 : limit
                },
                Fields = new List<FilterSearchField>()
            };
            return filter;
        }

        private async Task<string> ProcessWebAdress(string textValue, bool isProcessForValidate = false)
        {
            if (string.IsNullOrEmpty(textValue))
                return "";

            if (!textValue.StartsWith("#"))
            {
                var compareString = new string[] { @"http://", @"https://", @"/" };
                if (!compareString.Any(textValue.StartsWith))
                    textValue = @"http://" + textValue.Trim();

                if (isProcessForValidate && textValue.StartsWith(compareString[2]))
                    textValue = (await ExtensionReq.GetBuyerDomain()) + textValue.Trim();
            }
            return textValue;
        }

        private async Task<List<MGUrlRedirectModel>> _priGetByIds(long orgid, Dictionary<int, long> dids)
        {
            var ids = dids.Select(m => m.Value).ToList();
            var result = await rpurlRedirect.GetByIdsAsync(orgid, ids);
            var tmp = from v in result
                      join k in dids on v._id equals k.Value
                      orderby k.Key
                      select v;
            tmp = tmp.OrderByDescending(a => a.updated_at);
            return tmp.ToList();
        }

        private async Task<bool> IsWebAddressValid(string url)
        {
            url = await ProcessWebAdress(url, true);
            var isCreatedUriSuccess = Uri.TryCreate(url, UriKind.Absolute, out Uri uriResult)
                                                && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
            var isWellFormedUri = isCreatedUriSuccess && Uri.IsWellFormedUriString(uriResult.ToString(), UriKind.Absolute);

            return isWellFormedUri && isCreatedUriSuccess;
        }

        private FilterSearchModel GetUrlRedirectFilter(int limit, int page, long since_id, string path, string target, string fields)
        {
            var filter = new FilterSearchModel
            {
                Fields = new List<FilterSearchField>(),
                Page = new FilterSearchPage { pageSize = limit, currentPage = page },
                ViewId = SysView.Navigation
            };
            if (since_id > 0)
            {
                var field = new FilterSearchField { FieldName = "Id", NummericOperator = ">", IsNummeric = true, NummericValue = since_id.ToString() };
                filter.Fields.Add(field);
            }
            if (!string.IsNullOrWhiteSpace(path))
            {
                var field = new FilterSearchField { FieldName = "OldPath_ori", HasOptions = true, IsNummeric = false, OptionValueValue = path, OptionValue = "=" };
                filter.Fields.Add(field);
            }
            if (!string.IsNullOrWhiteSpace(target))
            {
                var field = new FilterSearchField { FieldName = "RedirectTo_ori", HasOptions = true, IsNummeric = false, OptionValueValue = target, OptionValue = "=" };
                filter.Fields.Add(field);
            }
            return filter;
        }

        private UrlRedirectModel ParseDictionaryRedirectApi(Dictionary<string, object> dictionaryModel)
        {
            if (dictionaryModel == null || dictionaryModel.Count == 0)
            {
                requestContext.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return null;
            }
            UrlRedirectModel resultModel = new UrlRedirectModel();
            var propertyInfos = typeof(RedirectAPIModel).GetProperties();
            foreach (var propertyValuePair in dictionaryModel)
            {
                var colName = propertyInfos.Single(x => x.Name == propertyValuePair.Key);

                if (colName.Name.Equals("id"))
                {
                    resultModel.Id = (long)propertyValuePair.Value;
                }
                if (colName.Name.Equals("path"))
                {
                    resultModel.OldPath = (string)propertyValuePair.Value;
                }
                if (colName.Name.Equals("target"))
                {
                    resultModel.RedirectTo = (string)propertyValuePair.Value;
                }
            }

            return resultModel;
        }

        #endregion private
    }
}