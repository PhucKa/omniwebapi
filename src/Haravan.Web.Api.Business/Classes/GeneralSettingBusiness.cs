﻿using Haravan.Libs.Abstractions;
using Haravan.Web.Api.BusinessObjects.Mappers;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.Repository;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public class GeneralSettingBusiness : IGeneralSettingBusiness
    {
        private readonly IRequestContext _context;
        private readonly IGeneralSettingRepository _rpGeneral;

        public GeneralSettingBusiness(IRequestContext requestContext,
            IGeneralSettingRepository rpGeneral
            )
        {
            this._context = requestContext;
            this._rpGeneral = rpGeneral;
        }

        public async Task<OnlineStoreSetting> GetOnlineStoreSetting()
        {
            var result = await _rpGeneral.GetGeneralSettingByStoreId();
            if (result.HasError)
            {
                _context.AddError(result.ErrorCodes[0].ToString(),result.Errors[0].ToString());
                return null;
            }
            return result.Data;
        }

        public async Task<OnlineStoreSetting> UpdateOnlineStoreSetting(OnlineStoreSetting settings)
        {
            if (settings == null)
            {
                _context.AddError("errors.data.no_valid","Dữ liệu không hợp lệ");
                return null;
            }
            var result = await _rpGeneral.UpdateOnlineStoreSetting(settings);
            if (result.HasError)
            {
                _context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return null;
            }
            return result.Data;
        }

        #region cấu hình tài khoản , Field

        public async Task<bool> Update(CheckoutSettingModelWeb Model)
        {
            if (Model.Id <= 0)
            {
                _context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ!");
                return false;
            }
            var model = Model.ToModelCheckoutSettingModelWeb();
            var result = await _rpGeneral.Update(model);
            if (result.HasError)
            {
                _context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return false;
            }
            return result.Data;
        }

        public async Task<bool> UpdateCustomerAccount(CustomerAccountModel model)
        {
            if (model.Id <= 0)
            {
                _context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ!");
                return false;
            }
            var data = model.ToModelCustomerAccount();
            var result = await _rpGeneral.UpdateCustomerAccount(data);
            if (result.HasError)
            {
                _context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return false;
            }
            return result.Data;
        }

        public async Task<bool> UpdateFieldSetting(FieldSettingModel model)
        {
            if (model.Id <= 0)
            {
                _context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ!");
                return false;
            }
            var data = model.ToModelFieldSettingModel();
            var result = await _rpGeneral.UpdateFieldSetting(data);
            if (result.HasError)
            {
                _context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return false;
            }
            return result.Data;
        }

        public async Task<bool> UpdateOrderAdditionalContent(OrderAdditionalContentModel model)
        {
            if (model.Id <= 0)
            {
                _context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ!");
                return false;
            }
            var data = model.ToModelOrderAdditionalContentModel();
            var result = await _rpGeneral.UpdateOrderAdditionalContent(data);
            if (result.HasError)
            {
                _context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return false;
            }
            return result.Data;
        }

        public async Task<CheckoutSettingModel> GetByStoreId()
        {
            var result = await _rpGeneral.GetByStoreId();
            if (result.HasError)
            {
                _context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return null;
            }
            return result.Data;
        }

        public async Task<bool> DisableMultipass(long id)
        {
            if (id <= 0)
            {
                _context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ!");
                return false;
            }
            var result = await _rpGeneral.DisableMultipass(id);
            if (result.HasError)
            {
                _context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return false;
            }
            return result.Data;
        }

        public async Task<bool> EnableMultipass(long id, int checkoutAccountType)
        {
            if (id <= 0 || checkoutAccountType <= 0)
            {
                _context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ!");
                return false;
            }
            var result = await _rpGeneral.EnableMultipass(id, checkoutAccountType);
            if (result.HasError)
            {
                _context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return false;
            }
            return result.Data;
        }

        public async Task<bool> EnableAccountKit(long id, int checkoutAccountType, string appId, string appSecret, string secret, bool isAccountKitAllowCreateAccount, bool isVerifyPasswordAfterLogin)
        {
            if (id <= 0 || checkoutAccountType <= 0)
            {
                _context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ!");
                return false;
            }
            var result = await _rpGeneral.EnableAccountKit(id, checkoutAccountType, appId, appSecret, secret, isAccountKitAllowCreateAccount, isVerifyPasswordAfterLogin);
            if (result.HasError)
            {
                _context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return false;
            }
            return result.Data;
        }

        public async Task<bool> DisableAccountKit(long id)
        {
            if (id <= 0)
            {
                _context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ!");
                return false;
            }
            var result = await _rpGeneral.DisableAccountKit(id);
            if (result.HasError)
            {
                _context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return false;
            }
            return result.Data;
        }

        #endregion cấu hình tài khoản , Field
    }
}