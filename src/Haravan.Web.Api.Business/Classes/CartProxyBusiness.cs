using BHN.SharedObject.APIDataModel;
using Haravan.Libs.Abstractions;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Mappers;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using Haravan.Web.Api.Repository;
using System;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public class CartProxyBusiness : ICartProxyBusiness
    {
        private readonly IExtensionPerRequest extensionReq;
        private readonly IRequestContext context;
        private readonly IMGCartProxyRepository rpCartProxy;
        private readonly IEBSComMessageRepository bus;

        public CartProxyBusiness(
            IMGCartProxyRepository rpCartProxy,
            IRequestContext context,
            IEBSComMessageRepository bus,
            IExtensionPerRequest extensionReq
        )
        {
            this.rpCartProxy = rpCartProxy;
            this.context = context;
            this.extensionReq = extensionReq;
            this.bus = bus;
        }

        public async Task DeleteFromWorker(long app_id)
        {
            var _storeId = extensionReq.StoreId;
            var objCartProxy = await rpCartProxy.GetByAppId(_storeId, app_id);
            if (objCartProxy != null)
            {
                await rpCartProxy.DeleteAsync(objCartProxy);
                await bus.FireStoreDataChange(_storeId);
            }
        }

        public async Task Delete(long app_id)
        {
            var _storeId = extensionReq.StoreId;
            var objCartProxy = await rpCartProxy.GetByAppId(_storeId, app_id);
            if (objCartProxy == null)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return;
            }
            await rpCartProxy.DeleteAsync(objCartProxy);
            await bus.FireStoreDataChange(_storeId);
        }

        public async Task<CartProxyAPIModel> Add(CartProxyModel model)
        {
            if (
                 model == null || string.IsNullOrWhiteSpace(model.Url)
                 || string.IsNullOrWhiteSpace(model.AppApiKey) || string.IsNullOrWhiteSpace(model.AppSecretKey)
               )
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var _storeId = extensionReq.StoreId;
            var _userId = extensionReq.UserId;
            model.Url = model.Url.Trim();

            if (!model.Url.StartsWith("https://") && !model.Url.StartsWith("http://"))
                model.Url = "http://" + model.Url;

            var objCartProxy = await rpCartProxy.GetByAppId(_storeId, _userId);
            if (objCartProxy != null)
            {
                if (objCartProxy.url == model.Url)
                {
                    return objCartProxy.ToApiModel();
                }
                objCartProxy.url = model.Url;
                await rpCartProxy.UpdateAsync(objCartProxy, m => m.url);
                await bus.FireStoreDataChange(_storeId);
                return objCartProxy.ToApiModel();
            }
            else
            {
                objCartProxy = new MGCartProxyModel()
                {
                    storeid = _storeId,
                    url = model.Url,
                    appid = _userId,
                    appsecretkey = model.AppSecretKey,
                    appapikey = model.AppApiKey,
                    created_at = DateTime.UtcNow,
                    updated_at = DateTime.UtcNow
                };
                await rpCartProxy.AddAsync(objCartProxy);
                await bus.FireStoreDataChange(_storeId);
                return objCartProxy.ToApiModel();
            }
        }

        public async Task<long?> GetFirstCartProxyId()
        {
            var _storeId = extensionReq.StoreId;
            var objCartProxy = await rpCartProxy.GetFirst(_storeId);
            return objCartProxy != null ? (long?)objCartProxy._id : null;
        }

        public async Task<CartProxyModel> GetFirst()
        {
            var _storeId = extensionReq.StoreId;
            var objCartProxy = await rpCartProxy.GetFirst(_storeId);
            if (objCartProxy != null)
            {
                return objCartProxy.ToModel();
            }
            return null;
        }
    }
}