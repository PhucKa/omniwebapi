﻿using Haravan.Libs.Abstractions;
using Haravan.Web.Api.BusinessObjects.ESModels;
using Haravan.Web.Api.Repository;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public class GlobalSearchBusiness : IGlobalSearchBusiness
    {
        private readonly IRequestContext requestContext;
        private readonly IKeywordBusiness bizKeyword;
        private readonly IUserRepository rpUser;
        private readonly IServiceProvider serviceProvider;
        private readonly IExtensionPerRequest ExtensionReq;
        private int pagesize = 3;

        public GlobalSearchBusiness(
                IRequestContext requestContext,
                IKeywordBusiness bizKeyword,
                IUserRepository rpUser,
                IServiceProvider serviceProvider,
                IExtensionPerRequest ExtensionReq
            )
        {
            this.requestContext = requestContext;
            this.bizKeyword = bizKeyword;
            this.rpUser = rpUser;
            this.serviceProvider = serviceProvider;
            this.ExtensionReq = ExtensionReq;
        }

        public async Task<Tuple<List<ESGlobalSearchResult>, long>> GetGlobalSearchList(FilterSearchModel model)
        {
            long totalRecord = 0;
            ESGlobalSearchResult lstArticle = null;
            ESGlobalSearchResult lstPage = null;
            ESGlobalSearchResult lstBlog = null;

            lstArticle = await GetGlobalSearchListArticle(model);
            lstPage = await GetGlobalSearchListPage(model);
            lstBlog = await GetGlobalSearchListBlog(model);
            var result = new List<ESGlobalSearchResult>();

            if (lstArticle != null)
            {
                result.Add(lstArticle);
                totalRecord += lstArticle.totalrecord;
            }
            if (lstPage != null)
            {
                result.Add(lstPage);
                totalRecord += lstPage.totalrecord;
            }
            if (lstBlog != null)
            {
                result.Add(lstBlog);
                totalRecord += lstBlog.totalrecord;
            }

            return new Tuple<List<ESGlobalSearchResult>, long>(result, totalRecord);
        }

        private async Task<ESGlobalSearchResult> GetGlobalSearchListArticle(FilterSearchModel model)
        {
            long TotalRecord = 0;
            List<string> typeList = new List<string>();
            model.Page.pageSize = pagesize;
            var articleBusiness = serviceProvider.GetService<IArticleBusiness>();
            var data = await articleBusiness.GetGlobalSearchListArticle(model);
            var ret = new ESGlobalSearchResult();
            TotalRecord = data.Item2;
            ret.totalrecord = TotalRecord;
            if (data.data != null && data.data.Count > 0)
            {
                var listSub = new List<subarticlemodel>();
                foreach (var item in data.Item1)
                {
                    var sub = new subarticlemodel();
                    sub.Id = item.Id;
                    sub.ArticleName = item.Title;
                    sub.UrlImage = item.ImageUrl;
                    sub.ImageId = item.ImageId;
                    sub.BlogId = item.BlogId;
                    sub.BlogTitle = item.BlogTitle;
                    listSub.Add(sub);
                }
                return new ESGlobalSearchResult { title = "esarticlemodel", jsonArrayResult = JsonConvert.SerializeObject(listSub), totalrecord = TotalRecord };
            }
            return ret;
        }

        private async Task<ESGlobalSearchResult> GetGlobalSearchListPage(FilterSearchModel model)
        {
            long TotalRecord = 0;
            List<string> typeList = new List<string>();
            model.Page.pageSize = pagesize;
            var pageBusiness = serviceProvider.GetService<IPageBusiness>();
            var data = await pageBusiness.GetGlobalSearchListPage(model);
            var ret = new ESGlobalSearchResult();
            TotalRecord = data.Item2;
            ret.totalrecord = TotalRecord;
            if (data.data != null && data.data.Count > 0)
            {
                var listSub = new List<subpagemodel>();
                foreach (var item in data.Item1)
                {
                    var sub = new subpagemodel();
                    sub.Id = item.id;
                    sub.Title = item.title;
                    sub.Shortcontent = item.shortcontent;
                    sub.Content = item.content;
                    listSub.Add(sub);
                }
                return new ESGlobalSearchResult { title = "espagemodel", jsonArrayResult = JsonConvert.SerializeObject(listSub), totalrecord = TotalRecord };
            }
            return ret;
        }

        private async Task<ESGlobalSearchResult> GetGlobalSearchListBlog(FilterSearchModel model)
        {
            long TotalRecord = 0;
            List<string> typeList = new List<string>();
            model.Page.pageSize = pagesize;
            var blogBusiness = serviceProvider.GetService<IBlogBusiness>();
            var data = await blogBusiness.GetGlobalSearchListBlog(model);
            var ret = new ESGlobalSearchResult();
            TotalRecord = data.Item2;
            ret.totalrecord = TotalRecord;
            if (data.data != null && data.data.Count > 0)
            {
                var listSub = new List<subblogmodel>();
                foreach (var item in data.Item1)
                {
                    var sub = new subblogmodel();
                    sub.Id = item.Id;
                    sub.Title = item.Title;
                    sub.UrlHandle = item.UrlHandle;
                    listSub.Add(sub);
                }
                return new ESGlobalSearchResult { title = "esblogmodel", jsonArrayResult = JsonConvert.SerializeObject(listSub), totalrecord = TotalRecord };
            }
            return ret;
        }

        private class subarticlemodel
        {
            public long Id { get; set; }
            public string ArticleName { get; set; }
            public string UrlImage { get; set; }
            public long? ImageId { get; set; }
            public long BlogId { get; set; }
            public string BlogTitle { get; set; }
        }

        private class subpagemodel
        {
            public long Id { get; set; }
            public string Title { get; set; }
            public string Shortcontent { get; set; }
            public string Content { get; set; }
        }

        private class subblogmodel
        {
            public long Id { get; set; }
            public string Title { get; set; }
            public string UrlHandle { get; set; }
        }
    }
}