﻿using Haravan.Libs.Abstractions;
using Haravan.Web.Api.Repository;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public class EcomBusiness : IEcomBusiness
    {
        private readonly IRequestContext context;
        private readonly IUserRepository rpUser;

        public EcomBusiness(
            IRequestContext context,
            IUserRepository rpUser
            )
        {
            this.context = context;
            this.rpUser = rpUser;
        }

        public async Task RemoveSaleChannelAfterUninstallApp(string clientId)
        {
            if (string.IsNullOrWhiteSpace(clientId))
            {
                context.AddError("errors.data.no_valid","Dữ liệu không hợp lệ");
                return;
            }
            await rpUser.RemoveSaleChannelAfterUninstallApp(clientId);
        }

        public async Task RemoveDataAfterUninstallApp(string clientId)
        {
            if (string.IsNullOrWhiteSpace(clientId))
            {
                context.AddError("errors.data.no_valid","Dữ liệu không hợp lệ");
                return;
            }
            await rpUser.RemoveDataAfterUninstallApp(clientId);
        }
    }
}
