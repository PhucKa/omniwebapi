﻿using BHN.Core;
using BHN.SharedObject.EBSMessage;
using BHN.SharedObject.EBSMessage.LogSeller;
using Haravan.Libs.Abstractions;
using Haravan.Web.Api.BusinessObjects.Enums;
using Haravan.Web.Api.BusinessObjects.ESModels;
using Haravan.Web.Api.BusinessObjects.Mappers;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.BusinessObjects.Models.Common;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using Haravan.Web.Api.Repository;
using Haravan.Web.Api.Repository.MGRepository;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using StringHelper = Haravan.Web.Api.Utils.Extensions.StringHelper;

namespace Haravan.Web.Api.Business
{
    public partial class NavigationBusiness : INavigationBusiness
    {
        private readonly List<string> LinkPagePolicyHandle = new List<string>() { "chinh-sach-doi-tra", "chinh-sach-bao-mat", "dieu-khoan-dich-vu" };
        private readonly IRequestContext requestContext;
        private readonly IExtensionPerRequest ExtensionReq;
        private readonly IMGUrlRedirectModelRepository rpurlRedirect;

        private readonly IMGRawLinkListRepository rpRawLinkList;
        private readonly IMGLinkListRepository rpLinkList;
        private readonly IMGRawLinkListFieldsRepository rpRawLinkListField;
        private readonly IMGRawLinkFieldsRepository rpRawLinkFields;
        private readonly IMGBlogRepository rpMgBlog;
        private readonly IMGPageRepository rpMgPage;
        private readonly IProductRepository rpProduct;
        private readonly ICollectionRepository rpCollection;
        private readonly IServiceProvider serviceProvider;
        private readonly IEBSComMessageRepository _bus;
        private readonly IMGLogDataRepository _mgLog;

        private readonly IESUrlRedirectRepository rpESUrlRedirect;
        private readonly IESUrlRedirectSinkRepository rpESUrlRedirectSearch;

        public NavigationBusiness(IRequestContext requestContext,
            IMGUrlRedirectModelRepository rpurlRedirect,
            IESUrlRedirectRepository rpESUrlRedirect,
            IExtensionPerRequest ExtensionReq,
            IMGRawLinkListRepository rpRawLinkList,
            IMGRawLinkListFieldsRepository rpRawLinkListField,
            IMGBlogRepository rpMgBlog,
            IMGPageRepository rpMgPage,
            IProductRepository rpProduct,
            ICollectionRepository rpCollection,
            IMGRawLinkFieldsRepository rpRawLinkFields,
            IServiceProvider serviceProvider,
            IEBSComMessageRepository _bus,
            IMGLogDataRepository _mgLog,
            IMGLinkListRepository rpLinkList,
            IESUrlRedirectSinkRepository rpESUrlRedirectSearch)
        {
            this.requestContext = requestContext;
            this.ExtensionReq = ExtensionReq;
            this.rpurlRedirect = rpurlRedirect;
            this.rpESUrlRedirect = rpESUrlRedirect;
            this.rpRawLinkList = rpRawLinkList;
            this.rpRawLinkListField = rpRawLinkListField;
            this.rpMgBlog = rpMgBlog;
            this.rpMgPage = rpMgPage;
            this.rpProduct = rpProduct;
            this.rpCollection = rpCollection;
            this.rpRawLinkFields = rpRawLinkFields;
            this.serviceProvider = serviceProvider;
            this.rpLinkList = rpLinkList;
            this._bus = _bus;
            this._mgLog = _mgLog;
            this.rpESUrlRedirectSearch = rpESUrlRedirectSearch;
        }

        #region public

        private async Task<MGRawLinkList> Validate_NavigationDetailModel(NavigationDetailModel model, long storeId, bool isUpdate = false)
        {
            var resultModel = new MGRawLinkList();

            #region Validate LinkList

            if (model == null || string.IsNullOrEmpty(model.LinkListName))
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }

            if (isUpdate == false && model.Id > 0)
            {
                requestContext.AddError("errors.navigation.link.exist", "Danh sách link đã tồn tại.");
                return null;
            }

            if (model.LinkListName.Length > 255)
            {
                requestContext.AddError("errors.navigation.link.max_characters", "Tên danh sách liên kết không nên nhập quá nhiều kí tự.");
                return null;
            }

            model.LinkListName = model.LinkListName.Trim();

            if (!string.IsNullOrEmpty(model.LinkListHandle) && model.LinkListHandle.Length > 300)
            {
                requestContext.AddError("errors.navigation.handle.max_characters", "Handle không nên nhập quá nhiều kí tự.");
                return null;
            }
            if (!string.IsNullOrEmpty(model.LinkListHandle) && model.LinkListHandle.Contains("#"))
            {
                requestContext.AddError("errors.navigation.handle.not_valid_characters", "Handle chứa kí tự không hợp lệ.");
                return null;
            }
            model.LinkListHandle = model.LinkListHandle.Trim();

            if (isUpdate)
            {
                if (model.Id <= 0)
                {
                    requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                    return null;
                }

                var linkList = await rpRawLinkList.GetById(ExtensionReq.StoreId, model.Id);
                if (linkList == null || linkList.IsDeleted == true)
                {
                    requestContext.AddError("errors.navigation.not_list_link_not_update", "Không tìm thấy danh sách link. Hủy thao tác cập nhật");
                    return null;
                }
                if (linkList.StoreId != storeId)
                {
                    requestContext.AddError("errors.navigation.information_not.not_update", "Thông tin không hợp lệ. Hủy thao tác cập nhật");
                    return null;
                }

                resultModel = linkList;
            }

            #endregion Validate LinkList

            #region Validate Links

            if (model.LinkListFields != null)
            {
                foreach (var linkModel in model.LinkListFields)
                {
                    if (string.IsNullOrEmpty(linkModel.LinkListFieldName))
                    {
                        if (!linkModel.IsDeleted)
                            requestContext.AddError("errors.navigation.link.not_valid", "Liên kết không hợp lệ");
                    }
                    else if (linkModel.LinkListFieldName.Length > 255)
                    {
                        requestContext.AddError("errors.navigation.link_name.max_characters", "Tên liên kết không nên nhập quá nhiều kí tự.");
                    }
                    else
                    {
                        linkModel.LinkListFieldName = linkModel.LinkListFieldName.Trim();
                    }
                    if (!string.IsNullOrEmpty(linkModel.LinkListFieldHandle) && linkModel.LinkListFieldHandle.Length > 300)
                    {
                        requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                    }

                    if (linkModel.LinkFieldsId == (int)LinkFieldType.WebAddress && !linkModel.IsDeleted)
                    {
                        if (StringHelper.CheckStringIsNullOrEmptyOrWhitespace(linkModel.TextValue) || StringHelper.CheckStringIsNullOrEmptyOrWhitespace(linkModel.TextValue.Trim()))
                            requestContext.AddError("errors.navigation.address.not_empty", "Địa chỉ web không được trống");
                        else if (await IsWebAddressValid(linkModel.TextValue) == false && linkModel.TextValue.StartsWith("#") == false)
                        {
                            string textValue = string.IsNullOrEmpty(linkModel.TextValue) ? " " : linkModel.TextValue;
                            requestContext.AddError($"errors.navigation.link__{linkModel.LinkListFieldName}__ not_valid__{textValue}__",
                                string.Format(@"Liên kết '{0}' không hợp lệ. Chuỗi trong liên kết không phải là một URI hợp lệ: '{1}'", linkModel.LinkListFieldName, textValue));
                        }
                    }
                    // nhóm sản phẩm không được bỏ trống
                    if (linkModel.LinkFieldsId == (int)LinkFieldType.Collection && !linkModel.IsDeleted)
                    {
                        if (linkModel.RefId <= 0)
                            requestContext.AddError("errors.navigation.collections.not_empty", "Chưa chọn nhóm sản phẩm");
                    }
                    // nhóm sản phẩm không được bỏ trống
                    if (linkModel.LinkFieldsId == (int)LinkFieldType.Product && !linkModel.IsDeleted)
                    {
                        if (linkModel.RefId <= 0)
                            requestContext.AddError("errors.navigation.product.not_empty", "Chưa chọn sản phẩm");
                    }
                    // blog không được bỏ trống
                    if (linkModel.LinkFieldsId == (int)LinkFieldType.Blog && !linkModel.IsDeleted)
                    {
                        if (linkModel.RefId <= 0)
                            requestContext.AddError("errors.navigation.blog.not_empty", "Chưa chọn Blog");
                    }
                    if (linkModel.LinkFieldsId == (int)LinkFieldType.Page && !linkModel.IsDeleted)
                    {
                        if (linkModel.RefId <= 0)
                            requestContext.AddError("errors.navigation.page.not_empty", "Chưa chọn Page");
                    }
                    // linkModel.Id == 0: add new
                    // linkModel.Id > 0: update
                    if (linkModel.Id > 0) // validate if update
                    {
                        var linkListField = await rpRawLinkListField.GetById(ExtensionReq.StoreId, linkModel.Id);
                        if (linkListField == null || linkListField.IsDeleted == true)
                        {
                            requestContext.AddError("errors.navigation.not_link_cancel_update", "Không tìm thấy link. Hủy thao tác cập nhật");
                            return null;
                        }
                        if (linkListField.StoreId != storeId)
                        {
                            requestContext.AddError("errors.navigation.information_not.not_update", "Thông tin không hợp lệ. Hủy thao tác cập nhật");
                            return null;
                        }

                        if (string.IsNullOrEmpty(linkModel.LinkListFieldName) && linkModel.IsDeleted)
                        {
                            linkModel.LinkListFieldName = linkListField.LinkListFieldName;
                        }
                    }
                }
            }

            #endregion Validate Links

            return resultModel;
        }


        private string buildDisplayPath(NavigationDetailModelPath modelpath, LinkListFieldsDetailModel model)
        {
            var pathdisplay = "";
            if (modelpath.LinkListFields == null || !modelpath.LinkListFields.Any()) return pathdisplay;
            var obj = modelpath.LinkListFields.Find(m => m.Id == model.Id);
            foreach (var item in obj.Path)
            {
                var subObj = modelpath.LinkListFields.Find(m => m.Id == item);
                pathdisplay = pathdisplay + " >> " + subObj.LinkListFieldName;
            }
            pathdisplay = (pathdisplay + " >> " + model.LinkListFieldName).Trim();
            return pathdisplay;
        }


        private async Task<MGRawLinkList> Validate_NavigationDetailModelPath(NavigationDetailModelPath modelpath, long storeId, bool isUpdate = false)
        {
            var model = modelpath.ToNavigationDetailModel();
            var resultModel = new MGRawLinkList();

            #region Validate LinkList

            if (model == null || string.IsNullOrEmpty(model.LinkListName))
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }

            if (isUpdate == false && model.Id > 0)
            {
                requestContext.AddError("errors.navigation.link.exist", "Danh sách link đã tồn tại.");
                return null;
            }

            if (model.LinkListName.Length > 255)
            {
                requestContext.AddError("errors.navigation.link.max_characters", "Tên danh sách liên kết không nên nhập quá nhiều kí tự.");
                return null;
            }

            model.LinkListName = model.LinkListName.Trim();

            if (!string.IsNullOrEmpty(model.LinkListHandle) && model.LinkListHandle.Length > 300)
            {
                requestContext.AddError("errors.navigation.handle.max_characters", "Handle không nên nhập quá nhiều kí tự.");
                return null;
            }
            if (!string.IsNullOrEmpty(model.LinkListHandle) && model.LinkListHandle.Contains("#"))
            {
                requestContext.AddError("errors.navigation.handle.not_valid_characters", "Handle chứa kí tự không hợp lệ.");
                return null;
            }
            model.LinkListHandle = model.LinkListHandle.Trim();

            if (isUpdate)
            {
                if (model.Id <= 0)
                {
                    requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                    return null;
                }

                var linkList = await rpRawLinkList.GetById(ExtensionReq.StoreId, model.Id);
                if (linkList == null || linkList.IsDeleted == true)
                {
                    requestContext.AddError("errors.navigation.not_list_link_not_update", "Không tìm thấy danh sách link. Hủy thao tác cập nhật");
                    return null;
                }
                if (linkList.StoreId != storeId)
                {
                    requestContext.AddError("errors.navigation.information_not.not_update", "Thông tin không hợp lệ. Hủy thao tác cập nhật");
                    return null;
                }

                resultModel = linkList;
            }

            #endregion Validate LinkList

            #region Validate Links

            if (model.LinkListFields != null)
            {
                foreach (var linkModel in model.LinkListFields)
                {
                    if (string.IsNullOrEmpty(linkModel.LinkListFieldName))
                    {
                        if (!linkModel.IsDeleted)
                        {
                            requestContext.AddError("errors.navigation.link.not_valid", "Liên kết không hợp lệ");
                            return null;
                        }
                    }
                    else if (linkModel.LinkListFieldName.Length > 255)
                    {
                        requestContext.AddError("errors.navigation.link_name.max_characters", "Tên liên kết không nên nhập quá nhiều kí tự.");
                        return null;
                    }
                    else
                    {
                        linkModel.LinkListFieldName = linkModel.LinkListFieldName.Trim();
                    }
                    if (!string.IsNullOrEmpty(linkModel.LinkListFieldHandle) && linkModel.LinkListFieldHandle.Length > 300)
                    {
                        requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                        return null;
                    }

                    if (linkModel.LinkFieldsId == (int)LinkFieldType.WebAddress && !linkModel.IsDeleted)
                    {
                        if (StringHelper.CheckStringIsNullOrEmptyOrWhitespace(linkModel.TextValue) || StringHelper.CheckStringIsNullOrEmptyOrWhitespace(linkModel.TextValue.Trim()))
                        {
                            requestContext.AddError("errors.navigation.address.not_empty", "Địa chỉ web không được trống");
                            return null;
                        }

                        else if (await IsWebAddressValid(linkModel.TextValue) == false && linkModel.TextValue.StartsWith("#") == false)
                        {
                            string textValue = string.IsNullOrEmpty(linkModel.TextValue) ? " " : linkModel.TextValue;
                            requestContext.AddError($"errors.navigation.link__{linkModel.LinkListFieldName}__ not_valid__{textValue}__",
                                string.Format(@"Liên kết '{0}' không hợp lệ. Chuỗi trong liên kết không phải là một URI hợp lệ: '{1}'", linkModel.LinkListFieldName, textValue));
                            return null;
                        }
                    }
                    //// nhóm sản phẩm không được bỏ trống
                    //if (linkModel.LinkFieldsId == (int)LinkFieldType.Collection && !linkModel.IsDeleted)
                    //{
                    //    if (linkModel.RefId <= 0)
                    //    {
                    //        requestContext.AddError("errors.navigation.collections.not_empty", $"Chưa chọn nhóm sản phẩm {buildDisplayPath(modelpath, linkModel)}");
                    //        return null;
                    //    }

                    //}
                    //// nhóm sản phẩm không được bỏ trống
                    //if (linkModel.LinkFieldsId == (int)LinkFieldType.Product && !linkModel.IsDeleted)
                    //{
                    //    if (linkModel.RefId <= 0)
                    //    {
                    //        requestContext.AddError("errors.navigation.collections.not_empty", $"Chưa chọn nhóm sản phẩm {buildDisplayPath(modelpath,linkModel)}");
                    //        return null;
                    //    }
                    //}
                    //// blog không được bỏ trống
                    //if (linkModel.LinkFieldsId == (int)LinkFieldType.Blog && !linkModel.IsDeleted)
                    //{
                    //    if (linkModel.RefId <= 0)
                    //        requestContext.AddError("errors.navigation.blog.not_empty", $"Chưa chọn Blog {buildDisplayPath(modelpath, linkModel)}");
                    //}
                    //if (linkModel.LinkFieldsId == (int)LinkFieldType.Page && !linkModel.IsDeleted)
                    //{
                    //    if (linkModel.RefId <= 0)
                    //        requestContext.AddError("errors.navigation.page.not_empty", $"Chưa chọn Page {buildDisplayPath(modelpath, linkModel)}");
                    //}
                    // linkModel.Id == 0: add new
                    // linkModel.Id > 0: update
                    if (linkModel.Id > 0) // validate if update
                    {
                        var linkListField = await rpRawLinkListField.GetById(ExtensionReq.StoreId, linkModel.Id);
                        if (linkListField == null || linkListField.IsDeleted == true)
                        {
                            requestContext.AddError("errors.navigation.not_link_cancel_update", "Không tìm thấy link. Hủy thao tác cập nhật");
                            return null;
                        }
                        if (linkListField.StoreId != storeId)
                        {
                            requestContext.AddError("errors.navigation.information_not.not_update", "Thông tin không hợp lệ. Hủy thao tác cập nhật");
                            return null;
                        }

                        if (string.IsNullOrEmpty(linkModel.LinkListFieldName) && linkModel.IsDeleted)
                        {
                            linkModel.LinkListFieldName = linkListField.LinkListFieldName;
                        }
                    }
                }
            }

            #endregion Validate Links

            return resultModel;
        }

        public async Task<bool> UpdateLinkOrder(List<LinkListRowModel> listRow, long linkListId)
        {
            if (!listRow.Any())
                return false;

            foreach (var row in listRow)
            {
                var link = await rpRawLinkListField.GetById(ExtensionReq.StoreId, row.Id);

                // Not update if link null OR link not belongto linkList OR link order is not change
                if (link == null
                    || link.LinkListId != linkListId
                    || link.LinkListFieldOrder == row.LinkListFieldOrder)
                    continue;

                link.LinkListFieldOrder = row.LinkListFieldOrder;
                link.UpdatedDate = DateTime.UtcNow;
                link.UpdatedUser = ExtensionReq.UserId;
                await rpRawLinkListField.UpdateAsync(link, l => l.LinkListFieldOrder
                                                        , l => l.UpdatedDate
                                                        , l => l.UpdatedUser
                                                         );
            }

            await priRebuildLinkList(linkListId);
            return true;
        }

        private async Task DeleteLinkList(long linklistid)
        {
            if (linklistid <= 0)
            {
                requestContext.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return;
            }

            var linkList = await rpRawLinkList.GetById(ExtensionReq.StoreId, linklistid);

            if (linkList == null)
            {
                requestContext.AddError("errors.navigation.not_list_link_not_delete", "Không tìm thấy danh sách link. Hủy thao tác xóa");
                return;
            }
            var _storeId = ExtensionReq.OrgId;
            if (linkList.StoreId != _storeId)
            {
                requestContext.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return;
            }

            // Xóa tất cả link trong link list
            var result = await DeleteLinkListFields_ByLinkListId(linklistid);

            if (result != true)
                return;

            // Xóa link list
            linkList.IsDeleted = true;
            linkList.UpdatedDate = DateTime.UtcNow;
            linkList.UpdatedUser = ExtensionReq.UserId;
            await rpRawLinkList.UpdateAsync(linkList, p => p.IsDeleted
                                                    , p => p.UpdatedDate
                                                    , p => p.UpdatedUser
                                                    );

            var bizKeyword = serviceProvider.GetService<IKeywordBusiness>();

            await bizKeyword.ProcessingKeyHandle(_storeId, (int)EnumDocumentType.LinkList, linkList.LinkListHandle, null);

            await priDeleteLinkList(linklistid);
        }

        public async Task<string> IsLinkListHasParentLink(long linklistid)
        {
            if (linklistid <= 0)
            {
                requestContext.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return "0|";
            }
            var linkList = await rpRawLinkList.GetById(ExtensionReq.StoreId, linklistid);
            if (linkList == null || string.IsNullOrEmpty(linkList.TreeHandle))
                return "0|";
            var _storeId = ExtensionReq.OrgId;
            var parentLink = await rpRawLinkListField.GetParentLink_ByDropdown(linklistid, linkList.TreeHandle, _storeId);

            return parentLink != null ? "1|" + parentLink._id.ToString()
                                    : "0|";
        }

        public async Task<long> AddFromSeller(NavigationDetailModel model, bool isCommit = true, bool isInternal = false)
        {
            var _storeId = ExtensionReq.OrgId;
            await Validate_NavigationDetailModel(model, _storeId);
            if (requestContext.IsError)
                return -1;

            var attemptHandle = !string.IsNullOrEmpty(model.LinkListHandle) ? model.LinkListHandle
                                                                            : model.LinkListName;

            var bizKeyword = serviceProvider.GetService<IKeywordBusiness>();

            model.LinkListHandle = await bizKeyword.ProcessingKeyHandle(
                                                             _storeId,
                                                             (int)EnumDocumentType.LinkList
                                                            , null
                                                            , attemptHandle);
            var mapperModel = model.MGRawLinkListToNavigationDetailModel();

            mapperModel.StoreId = _storeId;

            if (model.ParentLinkId > 0)
            {
                // Kiểm tra xem parent link có tồn tại không
                var parentLink = await rpRawLinkListField.GetById(ExtensionReq.StoreId, model.ParentLinkId);
                if (parentLink == null || parentLink.IsDeleted)
                {
                    requestContext.AddError("errors.navigation.link.not_exist", "Link không tồn tại");
                    return -1;
                }
                if (parentLink.StoreId != _storeId)
                {
                    requestContext.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                    return -1;
                }

                var parentLinkList = await rpRawLinkList.GetById(ExtensionReq.StoreId, parentLink.LinkListId);
                if (parentLinkList == null)
                    return -1;

                if (!mapperModel.LinkListName.StartsWith(parentLinkList.LinkListName + " » "))
                    mapperModel.LinkListName = parentLinkList.LinkListName + " » " + mapperModel.LinkListName;

                //Update parentlink handle if not equal Dropdown handle
                if (!parentLink.LinkListFieldHandle.Equals(model.LinkListHandle))
                {
                    parentLink.TreeHandle = parentLinkList.TreeHandle + "#" + model.LinkListHandle;
                    parentLink.LinkListFieldHandle = model.LinkListHandle;
                    parentLink.UpdatedDate = DateTime.UtcNow;
                    parentLink.UpdatedUser = ExtensionReq.UserId;
                    await rpRawLinkListField.UpdateAsync(parentLink, p => p.LinkListFieldHandle
                                                                   , p => p.UpdatedDate
                                                                   , p => p.UpdatedUser
                                                                , p => p.TreeHandle);
                }

                mapperModel.TreeHandle = parentLink.TreeHandle;
            }
            else
            {
                mapperModel.TreeHandle = mapperModel.LinkListHandle;
            }

            if (!isInternal &&
                (mapperModel.LinkListHandle.StartsWith(LinkListHandle.Facebook)
                || mapperModel.LinkListHandle.StartsWith(LinkListHandle.FiveVN)))
            {
                //model.LinkListHandle = model.LinkListHandle.Replace("fb-", "");
                throw new Exception("Dữ liệu không hợp lệ");
            }
            mapperModel.CreatedDate = DateTime.UtcNow;
            mapperModel.CreatedUser = ExtensionReq.UserId;
            mapperModel.UpdatedDate = DateTime.UtcNow;
            mapperModel.UpdatedUser = ExtensionReq.UserId;
            var ret = await rpRawLinkList.AddAsync(mapperModel);

            // If there is no error then add LinkListField item
            await ProcessingLinkListField(_storeId, model.LinkListFields, ret._id, mapperModel.TreeHandle);

            await priRebuildLinkList(ret._id);

            await _bus.FireStoreDataChangeLinkList(ExtensionReq.OrgId);
            return ret._id;
        }

        public async Task UpdateFromSeller(NavigationDetailModel model, bool isCommit = true, bool isInternal = false)
        {
            var _storeId = ExtensionReq.OrgId;
            var oldLinkList = await Validate_NavigationDetailModel(model, _storeId, true);
            if (requestContext.IsError)
                return;

            var linkListTreeHandle = oldLinkList.TreeHandle;

            //Update Linklist;
            var hasParent = await IsLinkListHasParentLink(oldLinkList._id);
            if (oldLinkList.IsSystemLink)
            {
                if (!model.LinkListName.Equals(oldLinkList.LinkListName))
                    await UpdateDropDownName_ByParentLinkListName(model.Id, oldLinkList.LinkListName, model.LinkListName);

                var _modelLinkList = model.MGRawLinkListToNavigationDetailModel();
                _modelLinkList.UpdatedDate = DateTime.UtcNow;
                _modelLinkList.UpdatedUser = ExtensionReq.UserId;
                await rpRawLinkList.UpdateAsync(_modelLinkList, k => k.LinkListName
                                                                , k => k.UpdatedDate
                                                                , k => k.UpdatedUser
                                                              );
            }
            // only allow update Name and Handle if LinkList doesn't have parent.
            else if (hasParent.Split('|')[0] == "0")
            {
                if (!model.LinkListName.Equals(oldLinkList.LinkListName))
                    await UpdateDropDownName_ByParentLinkListName(model.Id, oldLinkList.LinkListName, model.LinkListName);

                var oldLinkHandle = oldLinkList.LinkListHandle;
                var attemptHandle = !string.IsNullOrEmpty(model.LinkListHandle)
                                            ? model.LinkListHandle
                                            : model.LinkListName;

                var bizKeyword = serviceProvider.GetService<IKeywordBusiness>();
                model.LinkListHandle = await bizKeyword.ProcessingKeyHandle(
                                                                _storeId,
                                                                (int)EnumDocumentType.LinkList
                                                                , oldLinkHandle
                                                                , attemptHandle);
                var mapperModel = model.MGRawLinkListToNavigationDetailModel();

                if (!isInternal && (mapperModel.LinkListHandle.StartsWith(LinkListHandle.Facebook)
                        || mapperModel.LinkListHandle.StartsWith(LinkListHandle.FiveVN)))
                {
                    //model.LinkListHandle = model.LinkListHandle.Replace("fb-", "");
                    throw new Exception("Dữ liệu không hợp lệ");
                }
                mapperModel.UpdatedDate = DateTime.UtcNow;
                mapperModel.UpdatedUser = ExtensionReq.UserId;
                await rpRawLinkList.UpdateAsync(mapperModel, k => k.LinkListName
                                                        , k => k.UpdatedDate
                                                        , k => k.UpdatedUser
                                                      , k => k.LinkListHandle);
            }

            // Update LinkListFields
            await ProcessingLinkListField(_storeId, model.LinkListFields, model.Id, linkListTreeHandle, oldLinkList.LinkListName);
            await priRebuildLinkList(model.Id);
        }

        public async Task Page_UpdateForLinkUrl(long pageId, bool isDeleteAction = false)
        {
            if (isDeleteAction)
            {
                await RefId_DeleteForLinkUrl(pageId, (int)LinkFieldType.Page);
                return;
            }
            await RefId_UpdateForLinkUrl(pageId, (int)LinkFieldType.Page);
        }

        public async Task Blog_UpdateForLinkUrl(long blogId, bool isDeleteAction = false)
        {
            if (isDeleteAction)
            {
                await RefId_DeleteForLinkUrl(blogId, (int)LinkFieldType.Blog);
                return;
            }
            await RefId_UpdateForLinkUrl(blogId, (int)LinkFieldType.Blog);
        }

        public async Task Collection_UpdateForLinkUrl(long collectionId, bool isDeleteAction = false)
        {
            if (isDeleteAction)
            {
                await RefId_DeleteForLinkUrl(collectionId, (int)LinkFieldType.Collection);
                return;
            }
            await RefId_UpdateForLinkUrl(collectionId, (int)LinkFieldType.Collection);
        }

        public async Task Product_UpdateForLinkUrl(long productId, bool isDeleteAction = false)
        {
            if (isDeleteAction)
            {
                await RefId_DeleteForLinkUrl(productId, (int)LinkFieldType.Product);
                return;
            }

            await RefId_UpdateForLinkUrl(productId, (int)LinkFieldType.Product);
        }

        public async Task AddOrUpdateProductUrlRedirect(string oldPath, string newPath)
        {
            if (string.IsNullOrWhiteSpace(oldPath) || string.IsNullOrWhiteSpace(newPath))
                return;

            oldPath = oldPath.Trim().Replace("products/", "").Replace("/products", "").Replace("/products/", "").Replace("/", "");
            newPath = newPath.Trim().Replace("products/", "").Replace("/products", "").Replace("/products/", "").Replace("/", "");

            if (string.IsNullOrEmpty(oldPath) || string.IsNullOrEmpty(newPath))
                return;

            if (oldPath == newPath)
                return;
            string strOldPath = "/products/" + oldPath;
            string strNewPath = "/products/" + newPath;

            Dictionary<long, MessageAction> dicDeleteds = new Dictionary<long, MessageAction>();
            var _storeId = ExtensionReq.OrgId;
            var lstOldPathByOldPath = await rpurlRedirect.GetBy(strOldPath, true, _storeId);
            var lstRedirectByOldPath = await rpurlRedirect.GetBy(strOldPath, false, _storeId);
            var lstOldPathByNewPath = await rpurlRedirect.GetBy(strNewPath, true, _storeId);
            var lstRedirectByNewPath = await rpurlRedirect.GetBy(strNewPath, false, _storeId);

            if (lstRedirectByOldPath != null && lstRedirectByOldPath != null)
            {
                foreach (var c in lstRedirectByOldPath)
                {
                    c.isdeleted = true;
                    await rpurlRedirect.UpdateAsync(c, m => m.isdeleted);
                    if (!dicDeleteds.ContainsKey(c._id))
                        dicDeleteds.Add(c._id, MessageAction.Delete);
                }
            }
            if (lstOldPathByNewPath != null && lstOldPathByNewPath != null)
            {
                foreach (var c in lstOldPathByNewPath)
                {
                    c.isdeleted = true;
                    await rpurlRedirect.UpdateAsync(c, m => m.isdeleted);
                    if (!dicDeleteds.ContainsKey(c._id))
                        dicDeleteds.Add(c._id, MessageAction.Delete);
                }
            }
            if (lstOldPathByOldPath != null && lstOldPathByOldPath != null)
            {
                foreach (var c in lstOldPathByOldPath)
                {
                    c.isdeleted = true;
                    await rpurlRedirect.UpdateAsync(c, m => m.isdeleted);
                    if (!dicDeleteds.ContainsKey(c._id))
                        dicDeleteds.Add(c._id, MessageAction.Delete);
                }
            }
            if (lstRedirectByNewPath != null && lstRedirectByNewPath != null)
            {
                foreach (var c in lstRedirectByNewPath)
                {
                    c.isdeleted = true;
                    await rpurlRedirect.UpdateAsync(c, m => m.isdeleted);
                    if (!dicDeleteds.ContainsKey(c._id))
                        dicDeleteds.Add(c._id, MessageAction.Delete);
                }
            }
            var obj = new UrlRedirectModel()
            {
                OldPath = strOldPath,
                RedirectTo = strNewPath
            };
            await AddUrlRedirect(obj);
        }
        public async Task AddOrUpdateArticleUrlRedirect(string oldPath, string newPath)
        {
            if (string.IsNullOrWhiteSpace(oldPath) || string.IsNullOrWhiteSpace(newPath))
                return;

            oldPath = oldPath.Trim();
            newPath = newPath.Trim();

            if (string.IsNullOrEmpty(oldPath) || string.IsNullOrEmpty(newPath))
                return;

            if (oldPath == newPath)
                return;
            string strOldPath = "/blogs/" + oldPath;
            string strNewPath = "/blogs/" + newPath;
            var _storeId = ExtensionReq.OrgId;
            Dictionary<long, MessageAction> dicDeleteds = new Dictionary<long, MessageAction>();

            var lstOldPathByOldPath = await rpurlRedirect.GetBy(strOldPath, true, _storeId);
            var lstRedirectByOldPath = await rpurlRedirect.GetBy(strOldPath, false, _storeId);
            var lstOldPathByNewPath = await rpurlRedirect.GetBy(strNewPath, true, _storeId);
            var lstRedirectByNewPath = await rpurlRedirect.GetBy(strNewPath, false, _storeId);

            if (lstRedirectByOldPath != null && lstRedirectByOldPath != null)
            {
                foreach (var c in lstRedirectByOldPath)
                {
                    c.isdeleted = true;
                    await rpurlRedirect.UpdateAsync(c, m => m.isdeleted);
                    if (!dicDeleteds.ContainsKey(c._id))
                        dicDeleteds.Add(c._id, MessageAction.Delete);
                }
            }
            if (lstOldPathByNewPath != null && lstOldPathByNewPath != null)
            {
                foreach (var c in lstOldPathByNewPath)
                {
                    c.isdeleted = true;
                    await rpurlRedirect.UpdateAsync(c, m => m.isdeleted);
                    if (!dicDeleteds.ContainsKey(c._id))
                        dicDeleteds.Add(c._id, MessageAction.Delete);
                }
            }
            if (lstOldPathByOldPath != null && lstOldPathByOldPath != null)
            {
                foreach (var c in lstOldPathByOldPath)
                {
                    c.isdeleted = true;
                    await rpurlRedirect.UpdateAsync(c, m => m.isdeleted);
                    if (!dicDeleteds.ContainsKey(c._id))
                        dicDeleteds.Add(c._id, MessageAction.Delete);
                }
            }
            if (lstRedirectByNewPath != null && lstRedirectByNewPath != null)
            {
                foreach (var c in lstRedirectByNewPath)
                {
                    c.isdeleted = true;
                    await rpurlRedirect.UpdateAsync(c, m => m.isdeleted);
                    if (!dicDeleteds.ContainsKey(c._id))
                        dicDeleteds.Add(c._id, MessageAction.Delete);
                }
            }
            var obj = new UrlRedirectModel()
            {
                OldPath = strOldPath,
                RedirectTo = strNewPath
            };
            await AddUrlRedirect(obj);
        }

        public async Task<bool> DeleteBulkLinkListAsync(List<long> linkListId)
        {
            if (linkListId == null || !linkListId.Any())
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return false;
            }
            foreach (var id in linkListId)
            {
                await DeleteLinkListById(id);
            }
            return true;
        }

        public async Task<bool> DeleteLinkListById(long linkListId)
        {
            if (linkListId <= 0)
            {
                requestContext.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return false;
            }

            var linkList = await rpRawLinkList.GetById(ExtensionReq.StoreId, linkListId);

            if (linkList == null)
            {
                requestContext.AddError("errors.navigation.not_list_link_not_delete", "Không tìm thấy danh sách link. Hủy thao tác xóa");
                return false;
            }
            var _storeId = ExtensionReq.OrgId;
            if (linkList.StoreId != _storeId)
            {
                requestContext.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return false;
            }

            // Xóa tất cả link trong link list
            var result = await DeleteLinkListFields_ByLinkListId(linkListId);

            if (result != true)
                return false;

            // Xóa link list
            linkList.IsDeleted = true;
            linkList.UpdatedDate = DateTime.UtcNow;
            linkList.UpdatedUser = ExtensionReq.UserId;
            await rpRawLinkList.UpdateAsync(linkList, p => p.IsDeleted
                                                    , p => p.UpdatedDate
                                                    , p => p.UpdatedUser
                                                    );

            var bizKeyword = serviceProvider.GetService<IKeywordBusiness>();

            await bizKeyword.ProcessingKeyHandle(_storeId, (int)EnumDocumentType.LinkList, linkList.LinkListHandle, null);

            await priDeleteLinkList(linkListId);
            // save log
            var nav = await rpRawLinkList.GetById(ExtensionReq.StoreId, linkListId);
            var logmodel = new MGLogData()
            {
                created_date = DateTime.UtcNow,
                created_user = ExtensionReq.UserId,
                data = JsonConvert.SerializeObject(nav.ToNavigationDetailModel()),
                refid = nav._id,
                storeid = nav.StoreId,
                doctypeid = (int)EnumDocumentType.LinkList
            };
            await _mgLog.PostLog(logmodel);

            return true;
        }

        public async Task<List<NavigationDetailModel>> GetLinkListByRefId_ExceptHandle(long refId, int type)
        {
            if (refId <= 0)
                return null;

            var resultModel = new List<NavigationDetailModel>();
            var exceptHandles = GetExceptHandle();
            var _storeId = ExtensionReq.OrgId;
            var getlinkListIds = await rpRawLinkListField.GetByRefId(refId, type, _storeId);

            var linkListIds = getlinkListIds.GroupBy(p => p.LinkListId)
                                                .Select(group => group.First())
                                                .Select(p => p.LinkListId)
                                                .ToList();

            var linkLists = await rpRawLinkList.GetByIds(linkListIds, _storeId);
            foreach (var exceptHandle in exceptHandles)
            {
                linkLists = linkLists.Where(p => !p.LinkListHandle.StartsWith(exceptHandle)).ToList();
            }
            foreach (var linkList in linkLists)
            {
                var linkDetailModel = linkList.ToNavigationDetailModel();
                linkDetailModel.LinkListFields = await GetListLinkFieldDetailOfLinkListId(linkList._id, _storeId);
                resultModel.Add(linkDetailModel);
            }
            return resultModel;
        }

        public async Task SetLinkByRefId(long refId, string refTitle, List<NavigationDetailModel> linkLists, LinkFieldType type)
        {
            if (refId <= 0
                || string.IsNullOrEmpty(refTitle)
                || linkLists == null)
                return;

            var linkListIds = linkLists.GroupBy(p => p.Id)
                                        .Select(group => group.First())
                                        .Select(p => p.Id).ToList();

            foreach (var id in linkListIds)
            {
                var linkListDetailModel = await GetDetailLinkListId(id);
                if (linkListDetailModel == null)
                    continue;

                var linkListsFilterById = linkLists.Where(p => p.Id == id).ToList();

                var numberOfDelete = linkListsFilterById.Count(p => p.IsDeleted);
                var numberOfInsert = linkListsFilterById.Count(p => p.IsDeleted == false);

                if (numberOfDelete > 0)
                {
                    // Delete
                    var step = numberOfDelete;
                    foreach (var link in linkListDetailModel.LinkListFields)
                    {
                        if (link.RefId == refId)
                        {
                            link.IsDeleted = true;
                            step--;
                        }

                        if (step <= 0)
                            break;
                    }
                }

                if (numberOfInsert > 0)
                {
                    var _storeId = ExtensionReq.OrgId;
                    for (int i = 0; i < numberOfInsert; i++)
                    {
                        var linkOrder = linkListDetailModel.LinkListFields == null
                                                                            ? 1
                                                                            : linkListDetailModel.LinkListFields
                                                                                                .Select(p => p.LinkListFieldOrder)
                                                                                                .LastOrDefault() + 1;
                        var exist = await rpRawLinkListField.GetBy(refId, (int)type, id, _storeId);
                        if (exist == null || exist.Count == 0)
                        {
                            linkListDetailModel.LinkListFields.Add(new LinkListFieldsDetailModel()
                            {
                                LinkListFieldName = refTitle,
                                LinkFieldsId = (int)type,
                                RefId = refId,
                                LinkListFieldOrder = linkOrder
                            });
                        }
                    }
                }

                await UpdateFromSeller(linkListDetailModel, true, false);
            }
        }

        public async Task<bool> Init_NewStore()
        {
            var storeId = ExtensionReq.OrgId;
            var navStoreby0 = await rpRawLinkList.GetAllLinkList(0);
            //long result = -1;
            foreach (var nav in navStoreby0)
            {
                var links = await rpRawLinkListField.GetByLinkListId(nav._id, 0);

                var navDetailModel = nav.ToNavigationDetailModel();
                navDetailModel.LinkListFields = new List<LinkListFieldsDetailModel>();

                foreach (var link in links)
                {
                    var linksDetailModel = link.ToLinkListFieldsDetailModel();
                    linksDetailModel.StoreId = storeId;
                    linksDetailModel.Id = 0;
                    linksDetailModel.LinkListId = 0;

                    // lấy refId theo store mới
                    switch (linksDetailModel.LinkFieldsId)
                    {
                        case (int)LinkFieldType.Blog:
                            var bizBlog = serviceProvider.GetService<IBlogBusiness>();
                            linksDetailModel.RefId = await bizBlog.GetIdByHandleUrl("news");
                            break;

                        case (int)LinkFieldType.Page:

                            var bizPage = serviceProvider.GetService<IPageBusiness>();
                            if (!LinkPagePolicyHandle.Contains(linksDetailModel.LinkListFieldHandle))
                            {
                                linksDetailModel.RefId = await bizPage.GetIdByHandleUrl("about-us");
                            }
                            else
                            {
                                if (linksDetailModel.LinkListFieldHandle == "chinh-sach-doi-tra")
                                {
                                    linksDetailModel.RefId = await bizPage.GetIdByHandleUrl("chinh-sach-doi-tra");
                                }
                                else if (linksDetailModel.LinkListFieldHandle == "chinh-sach-bao-mat")
                                {
                                    linksDetailModel.RefId = await bizPage.GetIdByHandleUrl("chinh-sach-bao-mat");
                                }
                                else if (linksDetailModel.LinkListFieldHandle == "dieu-khoan-dich-vu")
                                {
                                    linksDetailModel.RefId = await bizPage.GetIdByHandleUrl("dieu-khoan-dich-vu");
                                }
                            }
                            break;

                        case (int)LinkFieldType.Collection:
                            var collection = await rpCollection.GetIdByHandle(link.TextValue.Replace("/collections/", ""));
                            linksDetailModel.RefId = collection;
                            break;
                    }

                    navDetailModel.LinkListFields.Add(linksDetailModel);
                }
                navDetailModel.Id = 0;
                await AddFromSeller(navDetailModel);
            }
            return true;
        }

        public async Task<bool> CheckLinkFieldIdHasChildLinkList(long linkfieldid)
        {
            if (linkfieldid <= 0)
            {
                requestContext.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return false;
            }
            var _storeId = ExtensionReq.OrgId;
            var link = await rpRawLinkListField.GetById(_storeId, linkfieldid);
            if (link == null || string.IsNullOrEmpty(link.TreeHandle))
                return false;

            var linkListParent = await rpRawLinkList.GetById(_storeId, link.LinkListId);
            if (linkListParent == null)
                return false;

            var dropdown = await rpRawLinkList.GetLinkListByTreeHandle(link.TreeHandle, _storeId);

            return dropdown != null && dropdown._id != linkListParent._id;
        }

        public async Task<NavigationDetailModel> GetDetailLinkListId(long linkListId)
        {
            var resultModel = new NavigationDetailModel();

            if (linkListId <= 0)
                requestContext.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
            else
            {
                var _storeId = ExtensionReq.OrgId;
                var dataLinkList = await rpRawLinkList.GetById(ExtensionReq.StoreId, linkListId);
                resultModel = dataLinkList.ToNavigationDetailModel();
                if (resultModel == null || resultModel.IsDeleted)
                    requestContext.AddError("errors.navigation.list_link.not_exist", "Danh sách link không tồn tại");
                else
                    resultModel.LinkListFields = await GetListLinkFieldDetailOfLinkListId(resultModel.Id, _storeId);
            }
            return resultModel;
        }

        public async Task<NavigationDetailModel> GetLinkListFromLinkListFieldId(long linklistfieldid)
        {
            return await priBuildLinkListFromLinkListFieldId(linklistfieldid);
        }

        public async Task<string> AddToMenu(long linklistfieldid)
        {
            if (linklistfieldid <= 0)
            {
                requestContext.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return "0|";
            }
            var currentlinkfield = await rpRawLinkListField.GetById(ExtensionReq.StoreId, linklistfieldid);
            if (currentlinkfield == null)
                return "0|";

            var dropDownLinkListDetail = await GetLinkListFromLinkListFieldId(currentlinkfield._id);

            long dropDownLinkListId = dropDownLinkListDetail.Id;
            if (dropDownLinkListId <= 0)
            {
                dropDownLinkListId = await AddFromSeller(dropDownLinkListDetail);
            }

            return requestContext.IsError ? "0|"
                                : string.Format("1|{0}|{1}", dropDownLinkListId.ToString(), dropDownLinkListDetail.LinkListName);
        }

        public async Task<(List<DropdownlistModel>, long)> GetDropdownlistByLinkField(LinkFieldType linkFieldType, FilterSearchModel model)
        {
            var resultModel = new List<DropdownlistModel>();
            var totalRecord = 0L;

            if (linkFieldType <= 0 || model == null)
            {
                requestContext.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
            }
            else
            {
                if (model.Page == null)
                {
                    model.Page = new FilterSearchPage { currentPage = 1, pageSize = 10 };
                }
                else if (model.Page.currentPage <= 0)
                {
                    model.Page.currentPage = 1;
                }
                else if (model.Page.pageSize <= 0)
                {
                    model.Page.currentPage = 10;
                }

                if (linkFieldType == LinkFieldType.Blog)
                {
                    var bizBlog = serviceProvider.GetService<IBlogBusiness>();
                    var tplBlog = await bizBlog.GetBlogList(model);
                    if (tplBlog.data != null)
                    {
                        resultModel = tplBlog.data.DropdownToListBlog();
                        totalRecord = tplBlog.totalrecord;
                    }
                }
                else
                {
                    model.Fields = new List<FilterSearchField>();
                    var filterSearchField = new FilterSearchField()
                    {
                        HasOptions = true,
                        OptionValue = "BeforeEqualsNow",
                        FieldName = linkFieldType == LinkFieldType.Page ? "PublishedDate" : "PublishDate"
                    };
                    model.Fields.Add(filterSearchField);

                    switch (linkFieldType)
                    {
                        case LinkFieldType.Page:
                            var bizPage = serviceProvider.GetService<IPageBusiness>();
                            var tplPages = await bizPage.GetPageList(model);
                            if (tplPages.data != null)
                            {
                                resultModel = tplPages.data.DropdownToListPage();
                                totalRecord = tplPages.totalrecord;
                            }
                            break;

                        case LinkFieldType.Collection:
                            var tplCollections = await rpCollection.GetCollectionList(model);
                            if (tplCollections.Item1 != null)
                            {
                                resultModel = tplCollections.Item1.DropdownToListCollection();
                                totalRecord = tplCollections.Item2;
                            }
                            break;

                        case LinkFieldType.Product:
                            var tplProducts = await rpProduct.GetProductList(model);
                            if (tplProducts.Item1 != null)
                            {
                                resultModel = tplProducts.datas.DropdownToListProduct();
                                totalRecord = tplProducts.totalRecord;
                            }
                            break;
                    }
                }
            }

            return (resultModel, totalRecord);
        }

        public async Task<List<LinkFieldsListModel>> GetDropdownlist_ForLinkTo()
        {
            var lst = await rpRawLinkFields.GetLinkFields();
            return lst.ToListLinkFieldsListModel();
        }

        public async Task<(List<DropdownlistModel>, long)> GetDropdownListByLinkFieldNavigation(LinkFieldType linkFieldType, string query, int page, int limit
                                                                      )
        {
            var filter = buildFilterUrlRedirectSearchModel(
                                                    query,
                                                    page,
                                                    limit
                                                  );
            var lstdata = await GetDropdownlistByLinkField(linkFieldType, filter);
            return (lstdata.Item1, lstdata.Item2);
        }

        public async Task<NavigationListModel> GetNavigationList(bool is_drag)
        {
            var resultModel = new NavigationListModel();
            var _storeId = ExtensionReq.OrgId;

            var linkLists = await GetLinkList_ExceptHandle();

            var exp = new List<LinkList>();
            if (is_drag)
            {
                exp = linkLists.FindAll(m => !m.TreeHandle.Contains("#"));
            }
            else exp = linkLists;

            foreach (var linkList in exp)
            {
                var item = await rpRawLinkListField.GetListByLinkListId(linkList.Id, _storeId);
                linkList.LinkListFields = item.ToListLinkListFields();
            }
            resultModel.LinkLists = exp.ToListLinkListModel();
            return resultModel;
        }

        #endregion public

        #region private

        private async Task RefId_DeleteForLinkUrl(long refId, int type)
        {
            if (refId <= 0)
                return;
            var _storeId = ExtensionReq.OrgId;
            var links = await rpRawLinkListField.GetByRefId(refId, type, _storeId);
            var linkListIds = new List<long>();

            if (links.Any())
            {
                foreach (var link in links)
                {
                    await rpRawLinkListField.DeleteAsync(link);

                    if (!linkListIds.Contains(link.LinkListId))
                        linkListIds.Add(link.LinkListId);
                }
            }

            foreach (var linkListId in linkListIds)
            {
                await priRebuildLinkList(linkListId);
            }
        }

        private async Task RefId_UpdateForLinkUrl(long refId, int type)
        {
            if (refId <= 0)
                return;
            var _storeId = ExtensionReq.OrgId;
            var links = await rpRawLinkListField.GetByRefId(refId, type, _storeId);
            var linkListIds = new List<long>();

            if (links.Any())
            {
                foreach (var link in links)
                {
                    link.TextValue = await BuildUrlForLink(type, refId);
                    link.UpdatedDate = DateTime.UtcNow;
                    link.UpdatedUser = ExtensionReq.UserId;
                    await rpRawLinkListField.UpdateAsync(link, p => p.TextValue
                                                                , p => p.UpdatedDate
                                                                , p => p.UpdatedUser
                                                                );

                    if (!linkListIds.Contains(link.LinkListId))
                        linkListIds.Add(link.LinkListId);
                }
            }
            foreach (var linkListId in linkListIds)
            {
                await priRebuildLinkList(linkListId);
            }
        }

        private string parseLinkType(int fieldtype)
        {
            var _Linktype = string.Empty;
            switch ((LinkFieldType)fieldtype)
            {
                case LinkFieldType.StoreFrontpage:
                    _Linktype = LinkType.Relative;
                    break;

                case LinkFieldType.Collection:
                    _Linktype = LinkType.Collection;
                    break;

                case LinkFieldType.Product:
                    _Linktype = LinkType.Product;
                    break;

                case LinkFieldType.AllProducts:
                    _Linktype = LinkType.Relative;
                    break;

                case LinkFieldType.Page:
                    _Linktype = LinkType.Page;
                    break;

                case LinkFieldType.Blog:
                    _Linktype = LinkType.Blog;
                    break;

                case LinkFieldType.SearchPage:
                    _Linktype = LinkType.Relative;
                    break;

                case LinkFieldType.WebAddress:
                    _Linktype = LinkType.Http;
                    break;

                default:
                    break;
            }
            return _Linktype;
        }

        private async Task<MGLinkListModel> _priBuildCurrentLinkListFromLinkListId(long currentLinkListId)
        {
            var model = await rpRawLinkList.GetById(ExtensionReq.StoreId, currentLinkListId);
            if (model == null)
            {
                return null;
            }
            var linklistmodel = model.LinkListMsgToMGRawLinkList();

            var linkListFields = await rpRawLinkListField.GetByLinkListId(currentLinkListId, ExtensionReq.StoreId);
            var modellinks = new List<MGLinkModel>();

            var linkFields = await rpRawLinkFields.GetLinkFields();
            foreach (var link in linkListFields)
            {
                var fiFields = linkFields.Find(m => m._id == link.LinkFieldsId);
                var _Linktype = parseLinkType(link.LinkFieldsId);
                var mglinks = new MGLinkModel()
                {
                    id = link._id,
                    storeid = link.StoreId,
                    handle = link.LinkListFieldHandle,
                    title = link.LinkListFieldName,
                    type = _Linktype,
                    url = link.TextValue,
                    sort = link.LinkListFieldOrder,
                    refid = link.RefId
                };
                modellinks.Add(mglinks);
            }

            linklistmodel.links = modellinks;
            return linklistmodel;
        }

        private async Task<MGLinkListModel> _priBuildParentLinkListFromLinkListId(long currentLinkListId)
        {
            var model = await rpRawLinkList.GetById(ExtensionReq.StoreId, currentLinkListId);
            if (model == null) return null;
            var LinkListHandle = model.LinkListHandle;
            var TreeHandle = model.TreeHandle;
            if (string.IsNullOrWhiteSpace(TreeHandle)) return null;
            if (!TreeHandle.Contains("#")) return null;
            var tmp = TreeHandle.Split('#');
            var parentTreeHandle = "";
            for (int i = 0; i < tmp.Length - 1; i++)
            {
                parentTreeHandle = parentTreeHandle + tmp[i] + "#";
            }
            parentTreeHandle = parentTreeHandle.Remove(parentTreeHandle.Length - 1);
            var parentLinkModel = await rpRawLinkList.GetLinkListByTreeHandle(parentTreeHandle, ExtensionReq.StoreId);
            if (parentLinkModel == null) return null;
            var mgLinkModel = await rpLinkList.GetById(ExtensionReq.StoreId, parentLinkModel._id);
            var subLinks = new List<MGLinkModel>();
            var linkListFields = await rpRawLinkListField.GetByLinkListId(parentLinkModel._id, ExtensionReq.StoreId);
            var linkFields = await rpRawLinkFields.GetLinkFields();
            foreach (var link in linkListFields)
            {
                var fiFields = linkFields.Find(m => m._id == link.LinkFieldsId);
                var _Linktype = parseLinkType(link.LinkFieldsId);
                var mglinks = new MGLinkModel()
                {
                    id = link._id,
                    storeid = link.StoreId,
                    handle = link.LinkListFieldHandle,
                    title = link.LinkListFieldName,
                    type = _Linktype,
                    url = link.TextValue,
                    sort = link.LinkListFieldOrder,
                    refid = link.RefId
                };
                subLinks.Add(mglinks);
            }

            mgLinkModel.links = subLinks;
            return mgLinkModel;
        }

        private async Task WriteLogDashboard(int logAction, NavigationDetailModel nav)
        {
            var model = new NavigationLogModel
            {
                Title = nav.LinkListName,
                LogDate = DateTime.UtcNow,
                LogUser = ExtensionReq.UserName,
                LinkDetail = "navigation#/detail/" + nav.Id
            };
            var logdata = JsonConvert.SerializeObject(model);
            var msg = new LogDataMsg
            {
                LogData = logdata,
                CreatedUser = ExtensionReq.UserId,
                CreatedUserEmail = ExtensionReq.UserEmail,
                ActionId = logAction,
                TypeId = (int)BHN.SharedObject.EBSMessage.LogType.Dashboard,
                RefId = nav.Id,
                StoreId = ExtensionReq.StoreId,
                CreatedDate = DateTime.UtcNow,
                Action = MessageAction.Insert,
                IsCommentLog = false,
                CreatedUserName = ExtensionReq.UserName
            };

            await _bus.SendMsgAsync(msg);
        }

        // Hàm lấy thông tin tất cả link trong Link List
        private async Task<List<LinkListFieldsDetailModel>> GetListLinkFieldDetailOfLinkListId(long linkListId, long storeId)
        {
            if (linkListId <= 0)
                return new List<LinkListFieldsDetailModel>();
            var lst = await rpRawLinkListField.GetByLinkListId(linkListId, storeId);
            var lstlinksDetailModel = new List<LinkListFieldsDetailModel>();
            foreach (var link in lst)
            {
                var fields = await priGetLinkFieldDetailByFieldId(link._id, storeId);
                lstlinksDetailModel.Add(fields);
            }

            return lstlinksDetailModel;
        }

        private async Task<LinkListFieldsDetailModel> priGetLinkFieldDetailByFieldId(long linklistfieldid, long storeId)
        {
            if (linklistfieldid <= 0)
                return null;

            var link = await rpRawLinkListField.GetById(storeId, linklistfieldid);
            if (link == null) return null;
            var linksDetailModel = link.ToLinkListFieldsDetailModel();
            var lstLinkFields = await rpRawLinkFields.GetLinkFields();
            var fiFields = lstLinkFields.Find(m => m._id == link.LinkFieldsId);
            if (fiFields != null)
            {
                linksDetailModel.LinkFieldsName = fiFields.FieldName;
                linksDetailModel.LinkFieldsDisplay = fiFields.FieldDisplay;
                linksDetailModel.LinkFieldsHasText = fiFields.HasText == null ? false : fiFields.HasText.Value;
                linksDetailModel.LinkFieldsHasOptions = fiFields.HasOptions == null ? false : fiFields.HasOptions.Value;
                linksDetailModel.LinkFieldsHasTags = fiFields.HasTags == null ? false : fiFields.HasTags.Value;
                linksDetailModel.LinkFieldsOptionUrl = fiFields.OptionsUrl;
                linksDetailModel.LinkFieldsId = (int)fiFields._id;
            }

            if (linksDetailModel.LinkFieldsHasOptions)
            {
                if (link.RefId != null && link.RefId.HasValue && link.RefId.Value > 0)
                {
                    var refid = link.RefId.Value;
                    switch (linksDetailModel.LinkFieldsId)
                    {
                        case (int)LinkFieldType.Blog:
                            var blog = await rpMgBlog.GetById(ExtensionReq.StoreId, refid);
                            if (blog != null)
                                linksDetailModel.SelectedOption = blog.title;
                            break;

                        case (int)LinkFieldType.Page:
                            var page = await rpMgPage.GetById(ExtensionReq.StoreId, refid);
                            if (page != null)
                                linksDetailModel.SelectedOption = page.title;
                            break;

                        case (int)LinkFieldType.Collection:
                            var collection = await rpCollection.GetCollectionSimpleById(refid);
                            if (collection != null)
                                linksDetailModel.SelectedOption = collection.name;
                            break;

                        case (int)LinkFieldType.Product:
                            var product = await rpProduct.GetProductSimpleById(refid);
                            if (product != null)
                                linksDetailModel.SelectedOption = product.name;
                            break;
                    }
                }
            }

            return linksDetailModel;
        }

        private async Task<NavigationDetailModel> priBuildLinkListFromLinkListFieldId(long linklistfieldid)
        {
            var resultModel = new NavigationDetailModel();

            if (linklistfieldid <= 0)
            {
                requestContext.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
            }
            else
            {
                // Kiểm tra xem parent link có tồn tại không
                var currentchild = await rpRawLinkListField.GetById(ExtensionReq.StoreId, linklistfieldid);
                if (currentchild == null || currentchild.IsDeleted)
                {
                    requestContext.AddError("errors.navigation.link.not_exist", "Liên kết không tồn tại");
                    return null;
                }
                var _storeId = ExtensionReq.OrgId;
                if (currentchild.StoreId != _storeId)
                {
                    requestContext.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                    return null;
                }
                var parentLinkList = await rpRawLinkList.GetById(ExtensionReq.StoreId, currentchild.LinkListId);
                if (parentLinkList == null)
                    return null;

                var dropdownLinkList = await rpRawLinkList.GetLinkListByTreeHandle(currentchild.TreeHandle, _storeId);

                resultModel = dropdownLinkList.ToNavigationDetailModel();

                // Trường hợp chưa có dropdown => tạo mới
                if (resultModel == null || resultModel.IsDeleted || dropdownLinkList._id == parentLinkList._id)
                {
                    resultModel = new NavigationDetailModel
                    {
                        LinkListName = parentLinkList.LinkListName + " » " + currentchild.LinkListFieldName,
                        LinkListHandle = StringHelper.CreateFriendlyURL(currentchild.LinkListFieldName)
                    };
                }
                else
                {
                    //Nếu đã có dropdown => load thông tin lên
                    resultModel.LinkListFields = await GetListLinkFieldDetailOfLinkListId(resultModel.Id, _storeId);
                }
                resultModel.ParentLinkId = linklistfieldid;
            }

            return resultModel;
        }

        // Ví dụ:
        //  "Main menu >> level2 >> level2"                         đổi thành   "Main menu >> level2 >> changeLevel2"
        //  "Main menu >> level2 >> level2 >> level2 >> level2"     đổi thành   "Main menu >> level2 >> changeLevel2 >> level2 >> level2"
        //
        // parentLinkListName = "Main menu >> level2"
        // oldParentLinkName  = "level2"
        // newParentLinkName  = "changeLevel2"
        //
        // Đầu tiên bỏ chuỗi trong parentLinkListName, sau đó thay thế oldParentLinkName với newLinkName bằng hàm replace, điều kiện replace chuỗi đầu tiên
        // Cuối cùng nối chuỗi parentLinkListName và kết quả replace để được kết quả cuối.
        private async Task UpdateDropDown_ByParentLink(string oldParentLinkTreeHandle, string newParentLinkTreeHandle
            , string oldParentLinkName, string newParentLinkName, string parentLinkHandle, string parentLinkListName)
        {
            #region Update DropDown LinkList

            var storeId = ExtensionReq.OrgId;
            var dropDownLinkLists = await rpRawLinkList.GetAlLinkListContainsTreeHandle(oldParentLinkTreeHandle, storeId);

            if (dropDownLinkLists == null)
                return;

            var dropDownLinkList_ToUpdate = dropDownLinkLists.Where(s => s.TreeHandle.StartsWith(oldParentLinkTreeHandle));

            var regexParentLinkListName = new Regex(Regex.Escape(parentLinkListName));
            var regexParentLinkName = new Regex(Regex.Escape(oldParentLinkName));
            foreach (var dropDownLinkList in dropDownLinkList_ToUpdate)
            {
                //Update Name

                // 1. Bỏ chuỗi parentLinkListName
                var handle = regexParentLinkListName.Replace(dropDownLinkList.LinkListName, "", 1);

                // 2. Thay thế oldParentLinkName với newLinkName và nối lại chuỗi parentLinkListName
                dropDownLinkList.LinkListName = parentLinkListName + regexParentLinkName.Replace(handle, newParentLinkName, 1);
                dropDownLinkList.UpdatedDate = DateTime.UtcNow;
                dropDownLinkList.UpdatedUser = ExtensionReq.UserId;
                await rpRawLinkList.UpdateAsync(dropDownLinkList, p => p.LinkListName
                                                                , p => p.UpdatedDate
                                                                , p => p.UpdatedUser
                );
            }

            #endregion Update DropDown LinkList
        }

        private async Task<NavigationListModel> getListNavigation()
        {
            var resultModel = new NavigationListModel();
            var linkLists = await GetLinkList_ExceptHandle();
            var _storeId = ExtensionReq.OrgId;
            foreach (var linkList in linkLists)
            {
                var item = await rpRawLinkListField.GetListByLinkListId(linkList.Id, _storeId);
                linkList.LinkListFields = item.ToListLinkListFields();
            }
            resultModel.LinkLists = linkLists.ToListLinkListModel();
            return resultModel;
        }

        private async Task<List<LinkList>> GetLinkList_ExceptHandle()
        {
            // this method get all link list of store, except the ones' defined in constant LinkListHandle.cs
            var exceptHandles = GetExceptHandle();
            var _storeId = ExtensionReq.OrgId;
            var linkLists = await rpRawLinkList.GetAllLinkList(_storeId);
            foreach (var exceptHandle in exceptHandles)
            {
                linkLists = linkLists.Where(p => !p.LinkListHandle.Contains(exceptHandle)).ToList();
            }
            var ret = linkLists.ToListLinkList();
            return ret.OrderBy(p => p.TreeHandle).ThenByDescending(p => p.IsSystemLink).ToList();
        }

        private List<string> GetExceptHandle()
        {
            var exceptHandles = new List<string>();

            foreach (var field in typeof(LinkListHandle).GetFields())
            {
                var handle = StringHelper.CreateFriendlyURL(field.GetValue(null).ToString());
                exceptHandles.Add(handle);
            }

            return exceptHandles;
        }

        private async Task UpdateDropDownName_ByParentLinkListName(long parentLinkListId, string oldParentName
            , string newParentName)
        {
            var storeId = ExtensionReq.OrgId;
            var parentLinks = await rpRawLinkListField.GetByLinkListId(parentLinkListId, storeId);

            foreach (var link in parentLinks)
            {
                var dropDownLinkLists = await rpRawLinkList.GetAlLinkListContainsTreeHandle(link.TreeHandle, storeId);

                if (dropDownLinkLists == null)
                    return;

                var regex = new Regex(Regex.Escape(oldParentName));

                foreach (var dropDownLinkList in dropDownLinkLists)
                {
                    dropDownLinkList.LinkListName = regex.Replace(dropDownLinkList.LinkListName, newParentName, 1);
                    dropDownLinkList.UpdatedDate = DateTime.UtcNow;
                    dropDownLinkList.UpdatedUser = ExtensionReq.UserId;
                    await rpRawLinkList.UpdateAsync(dropDownLinkList, p => p.LinkListName
                                                                     , p => p.UpdatedDate
                                                                     , p => p.UpdatedUser
                                                                    );
                }
            }
            return;
        }

        #endregion private

        private async Task priDeleteLinkList(long linklistid)
        {
            var _storeId = ExtensionReq.OrgId;
            await rpLinkList.DeleteAsync(_storeId, linklistid);
            await _bus.FireStoreDataChangeLinkList(ExtensionReq.OrgId);
        }

        private async Task priRebuildLinkList(long linklistid)
        {
            var modellinkmenu = await _priBuildCurrentLinkListFromLinkListId(linklistid);
            if (modellinkmenu != null)
            {
                await rpLinkList.SetAsync(modellinkmenu);

                // update link _ParentLink
                var _ParentLink = await _priBuildParentLinkListFromLinkListId(linklistid);
                if (_ParentLink != null)
                {
                    await rpLinkList.SetAsync(_ParentLink);
                }
            }
            else
            {
                var _storeId = ExtensionReq.OrgId;
                await rpLinkList.DeleteAsync(_storeId, linklistid);
            }

            await _bus.FireStoreDataChangeLinkList(ExtensionReq.OrgId);
        }

        private async Task<int> GetLastLinkOrder(long linkListId)
        {
            if (linkListId <= 0)
            {
                requestContext.AddError("errors.domain.parameters.not_valid", "Tham số không hợp lệ");
                return -1;
            }

            int order = 1;
            var _storeId = ExtensionReq.OrgId;
            var links = await rpRawLinkListField.GetByLinkListId(linkListId, _storeId);
            if (links != null)
            {
                order = links.OrderBy(p => p.LinkListFieldOrder).Select(p => p.LinkListFieldOrder).LastOrDefault();
                if (order <= 0)
                    order = 1;
            }

            return order;
        }

        private async Task<string> BuildUrlForLink(int type, long refId, string tags = "")
        {
            //no need to check refId
            var url = "";

            var refHandle = "";
            switch (type)
            {
                case (int)LinkFieldType.StoreFrontpage:
                    return "/";

                case (int)LinkFieldType.SearchPage:
                    return "/search";

                case (int)LinkFieldType.AllProducts:
                    return "/collections/all";

                case (int)LinkFieldType.Collection:
                    url = "/collections/";
                    refHandle = await rpCollection.GetHandleByIdAsync(refId);
                    break;

                case (int)LinkFieldType.Product:
                    url = "/products/";
                    refHandle = await rpProduct.GetHandleByIdAsync(refId);
                    break;

                case (int)LinkFieldType.Blog:
                    url = "/blogs/";
                    refHandle = await rpMgBlog.GetHandleByIdAsync(ExtensionReq.StoreId, refId);
                    break;

                case (int)LinkFieldType.Page:
                    url = "/pages/";
                    refHandle = await rpMgPage.GetHandleByIdAsync(ExtensionReq.StoreId, refId);

                    break;

                default:
                    return url;
            }

            if (string.IsNullOrEmpty(refHandle)) refHandle = "";

            if (type == (int)LinkFieldType.Collection)
            {
                refHandle += string.IsNullOrEmpty(tags) ? "" : @"/" + tags.Trim().Replace(", ", "+").Replace(",", "+").Replace(" ", "-");
            }

            url += refHandle;

            return url;
        }

        #region theme

        public async Task<List<SimpleListModel>> GetDropdownlist_LinkList()
        {
            return await getDropdownlist_LinkList();
        }

        public async Task<List<ThemeDropdownModel>> GetDropdownlist_LinkList_ThemeSetting_V2()
        {
            return await getDropdownlist_LinkList_V2();
        }

        public async Task<List<SimpleListModel>> GetDropdownlist_LinkList_ThemeSetting()
        {
            return await getDropdownlist_LinkList();
        }

        private async Task<List<ThemeDropdownModel>> getDropdownlist_LinkList_V2()
        {
            var ret = await GetLinkList_ExceptHandle();
            if (ret != null && ret.Any())
            {
                var result = ret.Select(p => new ThemeDropdownModel
                {
                    Id = p.Id,
                    Name = p.LinkListName,
                    Handle = p.LinkListHandle,
                    ImageUrl = null
                }).ToList();
                return result;
            }
            return null;
        }

        private async Task<List<SimpleListModel>> getDropdownlist_LinkList()
        {
            var ret = await GetLinkList_ExceptHandle();
            var _model = ret.SimpleToLinkList();
            return _model;
        }

        private async Task ProcessingLinkListField(long storeId, List<LinkListFieldsDetailModel> linkListFieldsModel
                                                , long linkListId
                                                , string linkListTreeHandle
                                                , string oldLinkListName = "")
        {
            if (linkListFieldsModel != null && linkListFieldsModel.Any())
            {
                foreach (var linkdetail in linkListFieldsModel)
                {
                    await ProcessingLinkListFieldDetailSeller(storeId, linkdetail
                                                                   , linkListId
                                                                   , linkListTreeHandle
                                                                   , oldLinkListName = "");
                }
            }
        }

        private async Task<MGRawLinkListFields> ProcessingLinkListFieldDetailSeller(long storeId, LinkListFieldsDetailModel linkdetail
                                                , long linkListId
                                                , string linkListTreeHandle
                                                , string oldLinkListName = "")
        {
            var link = linkdetail.ToMGRawLinkListFields();
            link.StoreId = storeId;
            var linkOrder = await GetLastLinkOrder(linkListId);

            #region Xử lý chung cho thao tác INSERT - UPDATE

            if (link.IsDeleted == false)
            {
                if (link.LinkFieldsId == (int)LinkFieldType.WebAddress)
                    link.TextValue = await ProcessWebAdress(link.TextValue);
                else
                {
                    link.TextValue = await BuildUrlForLink(link.LinkFieldsId
                                                    , link.RefId ?? 0
                                                    , link.Tags);
                }
            }

            #endregion Xử lý chung cho thao tác INSERT - UPDATE

            // If INSERT
            if (link._id <= 0)
            {
                // Handle link cho phép trùng nên không cần dùng hàm Process.
                link.LinkListFieldHandle = Guid.NewGuid().ToString();

                if (link.LinkListId <= 0)
                    link.LinkListId = linkListId;

                if (link.LinkListFieldOrder <= 0)
                {
                    link.LinkListFieldOrder = ++linkOrder;
                }

                link.TreeHandle = linkListTreeHandle + "#" + link.LinkListFieldHandle;
                //var addLink = Mapper.Map<MGRawLinkListFields>(link);
                link.CreatedDate = DateTime.UtcNow;
                link.CreatedUser = ExtensionReq.UserId;
                link.UpdatedDate = DateTime.UtcNow;
                link.UpdatedUser = ExtensionReq.UserId;
                var ret = await rpRawLinkListField.AddAsync(link);
                return ret;
            }
            // ELSE If UPDATE
            else
            {
                var oldLink = await rpRawLinkListField.GetById(ExtensionReq.StoreId, link._id);
                // Check if:
                // 1. update and link Name change, I update Dropdown Name and Handle.
                // 2. or link is deleted, I also delete dropdown.

                if (oldLink == null)
                {
                    // không tìm thấy link, bỏ qua
                    return null;
                }

                var dropdownLinkList = await rpRawLinkList.GetLinkListByTreeHandle(oldLink.TreeHandle, storeId);
                if (dropdownLinkList != null && !dropdownLinkList.IsSystemLink)
                {
                    if (link.IsDeleted)
                    {
                        await DeleteLinkList(dropdownLinkList._id);
                    }
                    else
                    {
                        if (!oldLink.LinkListFieldName.Equals(link.LinkListFieldName))
                        //|| !oldLink.TreeHandle.Equals(link.TreeHandle))
                        {
                            await UpdateDropDown_ByParentLink(oldLink.TreeHandle
                                 , link.TreeHandle
                                 , oldLink.LinkListFieldName
                                 , link.LinkListFieldName
                                 , link.LinkListFieldHandle
                                 , oldLinkListName);
                        }
                    }
                }
                link.TreeHandle = linkListTreeHandle + "#" + link.LinkListFieldHandle;
                link.UpdatedDate = DateTime.UtcNow;
                link.UpdatedUser = ExtensionReq.UserId;
                link.LinkListId = linkListId;
                await rpRawLinkListField.UpdateAsync(link, k => k.LinkFieldsId
                    , k => k.IsDeleted
                    , k => k.LinkListFieldName
                    , k => k.LinkListFieldOrder
                    , k => k.RefId
                    , k => k.TextValue
                    , k => k.UpdatedDate
                    , k => k.UpdatedUser
                    , k => k.Tags
                    , k => k.LinkListId
                    , k => k.TreeHandle
                    );
                var ret = await rpRawLinkListField.GetById(storeId, link._id);
                return ret;
            }
        }

        #endregion theme
    }
}