﻿using BHN.Core;
using BHN.SharedObject.APIDataModel;
using BHN.SharedObject.EBSMessage;
using Haravan.Web.Api.Business.Extensions;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.ESModels;
using Haravan.Web.Api.BusinessObjects.Mappers;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.BusinessObjects.Models.Article;
using Haravan.Web.Api.BusinessObjects.Models.Common;
using Haravan.Web.Api.Utils.Extensions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public partial class ArticleBusiness : IArticleBusiness
    {
        #region Api

        public async Task<bool> Init_NewStore()
        {
            var articleStoreby0 = await rpArticle.GetArticlesByStoreId(0);
            if (articleStoreby0 != null && articleStoreby0.Any())
            {
                var article = articleStoreby0[0];
                article.published_at = DateTime.UtcNow.AddHours(-1);
                article.created_at = DateTime.UtcNow.AddHours(-1);
                article._id = 0;
                article.author = exRequestContext.UserName;
                var articleDetailModel = article.ArticleToModelDetail();
                var blog = await rpBlog.GetByHandleUrl("news", exRequestContext.OrgId);
                articleDetailModel.BlogId = blog != null ? blog._id : 0;
                articleDetailModel.AuthorId = exRequestContext.UserId;
                articleDetailModel.PublishedDate = DateTime.UtcNow;
                await AddNewAsync(articleDetailModel);
            }
            return true;
        }

        public async Task<List<DropdownlistModel>> GetUsersSimpleList()
        {
            return await rpFilter.GetUsersSimpleList();
        }

        public async Task<List<OmniSimpleModel>> GetSimpleUserList(long userId)
        {
            return await rpFilter.GetSimpleUserList(userId);
        }

        public async Task<bool> DeleteArticleApi(long articleId)
        {
            return await DeleteArticleById(articleId);
        }

        public async Task<List<string>> GetAllTagsAPI(FilterSearchModel model, bool getPopular)
        {
            var tags = await GetTagsByTypeAPI(model, (int)SummaryType.Article_Tag, getPopular);
            return tags;
        }

        public async Task<List<string>> GetTagsByBlogAPI(FilterSearchModel model, bool getPopular)
        {
            var tags = await GetTagsByTypeAPI(model, (int)SummaryType.Blog_ArticlesTag, getPopular);
            return tags;
        }

        public async Task<(List<ArticleAPIModel> articles, long totalRecord)> GetArticleAPIList(FilterSearchModel model)
        {
            (ESSearchResult esResult, long totalRecord) = await SearchESArticle(model);
            var result = new List<ArticleAPIModel>();
            if (!requestContext.IsError && esResult.Ids.Any())
            {
                var articleIds = esResult.Ids.Select(m => m.Value);
                var lstArticles = await GetDetailBy(articleIds.ToList());
                result = lstArticles.ToApiList();
            }
            return (result, totalRecord);
        }

        public async Task<ArticleAPIModel> GetDetailByIdAPI(FilterSearchModel filter)
        {
            var data = new ArticleAPIModel();
            if (filter == null)
            {
                return null;
            }
            var rs = await rpESArticleSearch.SearchAsync(exRequestContext.StoreId, filter);
            if (rs.Total == 0)
            {
                return null;
            }
            long articleId = rs.Ids.ToList().FirstOrDefault().Value;

            var result = await GetDetail(articleId);

            if (result == null)
            {
                requestContext.AddError("errors.article.not_exist", "Bài viết không tồn tại");
                return null;
            }
            else
            {
                data = result.DetailToApi();
            }
            return data;
        }

        public async Task<ArticleAPIModel> AddNewAPI(ArticleDetailModel model)
        {
            var data = await AddNewAsync(model);
            if (data != null)
                return data.DetailToApi();
            else
                return null;
        }

        public async Task<ArticleAPIModel> UpdateAPI(long articleId, Dictionary<string, object> dicUpdated)
        {
            if (dicUpdated == null || dicUpdated.Count == 0)
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var articleToUpdate = await GetDetail(articleId);
            if (requestContext.IsError || articleToUpdate == null)
            {
                return null;
            }
            var propertyInfos = typeof(ArticleAPIModel).GetProperties();
            foreach (var propertyValuePair in dicUpdated)
            {
                var colName = propertyInfos.Single(x => x.Name == propertyValuePair.Key);
                if (colName.Name.Equals("title"))
                {
                    if (propertyValuePair.Value == null || String.IsNullOrEmpty(propertyValuePair.Value.ToString()))
                    {
                        requestContext.AddError("errors.article.title.is_empty", "Tiêu đề bài viết không được trống");
                        return null;
                    }
                    else
                    {
                        articleToUpdate.Title = propertyValuePair.Value.ToString();
                    }
                }
                if (colName.Name.Equals("author"))
                {
                    if (propertyValuePair.Value == null || String.IsNullOrEmpty(propertyValuePair.Value.ToString()))
                    {
                        requestContext.AddError("errors.article.author.is_empty", "Tác giả bài viết không được trống");
                        return null;
                    }
                    if (propertyValuePair.Value.ToString().Length > 100)
                    {
                        requestContext.AddError("errors.article.author.too_long", "Tác giả bài viết quá dài");
                        return null;
                    }
                    articleToUpdate.AuthorName = propertyValuePair.Value.ToString();
                }
                if (colName.Name.Equals("body_html"))
                {
                    var strContent = propertyValuePair.Value?.ToString().Trim();
                    if (!StringHelper.CheckStringIsNullOrEmptyOrWhitespace(strContent))
                    {
                        strContent = HtmlUtility.Instance.SanitizeHtml(strContent, HtmlUtility.ValidHtmlTags.Article);

                        if (strContent.Trim().Length > DefaultData.DefaultMaxArticleDescriptionLength)
                        {
                            requestContext.AddError("errors.article.content.too_long", "Nội dung quá dài");
                            return null;
                        }
                    }
                    articleToUpdate.Content = strContent;
                }
                if (colName.Name.Equals("handle"))
                {
                    articleToUpdate.UrlHandle = propertyValuePair.Value?.ToString();
                }
                if (colName.Name.Equals("published"))
                {
                    bool isPublish = bool.Parse(propertyValuePair.Value.ToString());
                    if (isPublish)
                    {
                        articleToUpdate.PublishedDate = DateTime.UtcNow;
                    }
                    else
                    {
                        articleToUpdate.PublishedDate = null;
                    }
                }
                if (colName.Name.Equals("summary_html"))
                {
                    articleToUpdate.Excerpt = propertyValuePair.Value?.ToString();
                }
                if (colName.Name.Equals("page_title"))
                {
                    articleToUpdate.PageTitle = propertyValuePair.Value?.ToString();
                }
                if (colName.Name.Equals("meta_description"))
                {
                    articleToUpdate.MetaDescription = propertyValuePair.Value?.ToString();
                }
                if (colName.Name.Equals("template_suffix"))
                {
                    if (propertyValuePair.Value == null || String.IsNullOrEmpty(propertyValuePair.Value.ToString()))
                        articleToUpdate.TemplateName = TemplateName.Article;
                    else
                        articleToUpdate.TemplateName = propertyValuePair.Value.ToString();
                }
                if (colName.Name.Equals("tags"))
                {
                    if (propertyValuePair.Value == null || String.IsNullOrEmpty(propertyValuePair.Value.ToString()))
                        articleToUpdate.Tags = null;
                    else
                        articleToUpdate.Tags = propertyValuePair.Value.ToString();
                }
                if (colName.Name.Equals("image"))
                {
                    if (propertyValuePair.Value == null)
                    {
                        articleToUpdate.ImageId = 0;
                        articleToUpdate.ImageUrl = string.Empty;
                    }
                    else
                    {
                        var objImg = JsonConvert.DeserializeObject<ArticleImageAPIModel>(JsonConvert.SerializeObject(propertyValuePair.Value));
                        if (objImg != null)
                        {
                            var rsImg = await _checkArticleImageApi(objImg.src, objImg.attachment, objImg.filename, articleToUpdate.ImageUrl, articleToUpdate.ImageId);
                            if (rsImg != null)
                            {
                                articleToUpdate.ImageId = rsImg.Item1;
                                articleToUpdate.ImageUrl = rsImg.Item2;
                            }
                        }
                    }
                }
            }
            var data = await UpdateAsync(articleToUpdate);

            if (data == null)
                return null;

            return data.DetailToApi();
        }

        public async Task<List<string>> GetAuthorListAPI()
        {
            var articles = await rpArticle.GetArticlesByStoreId(exRequestContext.StoreId);
            if (articles == null)
                return null;
            var resultModel = articles.Where(p => !string.IsNullOrEmpty(p.author)).OrderBy(m => m.author)
                                      .Select(p => p.author).Distinct().ToList();
            var authorIds = articles.Where(p => string.IsNullOrEmpty(p.author)).OrderBy(m => m.author)
                                      .Select(p => p.authorid.Value).Distinct().ToList();
            if (authorIds != null || authorIds.Count > 0)
            {
                foreach (var user in authorIds)
                {
                    var userdetails = await rpUser.GetUserDetail(user);
                    if (userdetails != null)
                        resultModel.Add(userdetails.FullName);
                }
            }
            return resultModel;
        }

        #endregion Api
    }
}