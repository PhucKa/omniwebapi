﻿using BHN.Core;
using BHN.SharedObject.APIDataModel;
using Haravan.Libs.Abstractions;
using Haravan.Web.Api.BusinessObjects.Enums;
using Haravan.Web.Api.BusinessObjects.ESModels;
using Haravan.Web.Api.BusinessObjects.Models.Common;
using Haravan.Web.Api.Repository;
using Haravan.Web.Api.Repository.MGRepository;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public class CommonBusiness : ICommonBusiness
    {
        private readonly IRequestContext _context;
        private readonly IArticleBusiness _bizArticle;
        private readonly IBlogBusiness _bizBlog;
        private readonly INavigationBusiness _bizNavigation;
        private readonly IPageBusiness _bizPage;
        private readonly ICollectionRepository _rpCollection;
        private readonly IProductRepository _rpProduct;

        private readonly IMGRawLinkListRepository _rpRawLinkList;
        private readonly IMGArticleRepository _rpArticle;
        private readonly IMGPageRepository _rpPage;
        private readonly IShopRepository _rpShop;
        private readonly IExtensionPerRequest extensionReq;

        public CommonBusiness(IRequestContext requestContext,
            IArticleBusiness articleBusiness,
            IBlogBusiness blogBusiness,
            ICollectionRepository collectionRepository,
            INavigationBusiness navigationBusiness,
            IPageBusiness pageBusiness,
            IProductRepository rpProduct,
            IMGRawLinkListRepository rpMGRawLinkList,
            IMGArticleRepository rpArticle,
            IMGPageRepository rpPage,
            IExtensionPerRequest ExtensionReq,
            IShopRepository _rpShop
            )
        {
            this._context = requestContext;
            this._bizArticle = articleBusiness;
            this._bizBlog = blogBusiness;
            this._rpCollection = collectionRepository;
            this._bizNavigation = navigationBusiness;
            this._bizPage = pageBusiness;
            this._rpProduct = rpProduct;
            this._rpRawLinkList = rpMGRawLinkList;
            this.extensionReq = ExtensionReq;
            this._rpArticle = rpArticle;
            this._rpPage = rpPage;
            this._rpShop = _rpShop;
        }

        public async Task<ThemeDropdownModel> GetSimpleDetailByType(int type, string handle)
        {
            var result = null as ThemeDropdownModel;
            if (type <= 0)
            {
                _context.AddError("errors.data.no_valid","Dữ liệu không hợp lệ");
                return null;
            }
            switch (type)
            {
                case (int)ThemeDropdownListType.Blog:
                    result = await GetBlogByHandle(handle);
                    break;

                case (int)ThemeDropdownListType.Collection:
                    result = await GetCollectionByHandle(handle);
                    break;

                case (int)ThemeDropdownListType.Menu:
                    result = await GetLinkListByHandle(handle);
                    break;

                case (int)ThemeDropdownListType.Page:
                    result = await GetPageByHandle(handle);
                    break;

                case (int)ThemeDropdownListType.Article:
                    result = await GetArticleByHandle(handle);
                    break;

                case (int)ThemeDropdownListType.Product:
                    result = await GetProductByHandle(handle);
                    break;

                default:
                    break;
            }
            return result;
        }

        public async Task<(List<ThemeDropdownModel> datas, long totalRecord)> GetSimpleDropdownListByType(int type, string query,
                                                   int page,
                                                   int limit)
        {
            (List<ThemeDropdownModel>, long) datas = (null, 0);
            if (type <= 0)
            {
                _context.AddError("errors.data.no_valid","Dữ liệu không hợp lệ");
                return (null, 0);
            }
            switch (type)
            {
                case (int)ThemeDropdownListType.Blog:
                    datas = await _bizBlog.GetSimpleBlogsListForTheme(false, query, page, limit);
                    break;

                case (int)ThemeDropdownListType.Collection:
                    datas = await GetListCollectionSimple(query, page, limit);
                    break;

                case (int)ThemeDropdownListType.Menu:
                    var navigations = await _bizNavigation.GetDropdownlist_LinkList_ThemeSetting_V2();
                    datas = (navigations, navigations.Count());
                    break;

                case (int)ThemeDropdownListType.Page:
                    datas = await _bizPage.GetPageDropdownList(query, page, limit);
                    break;

                case (int)ThemeDropdownListType.Article:
                    datas = await _bizArticle.GetArticleDropDownListForTheme(query, page, limit);
                    break;

                case (int)ThemeDropdownListType.Product:
                    datas = await GetSimpleProductList(query, page, limit);
                    break;

                default:
                    break;
            }
            return datas;
        }

        public async Task<ShopAPIModel> GetShopInfo()
        {
            var objshop = await _rpShop.GetShopInfo();
            if (objshop.HasError)
            {
                _context.AddError(objshop.ErrorCodes[0].ToString(), objshop.Errors[0].ToString());
                return null;
            }
            return objshop.Data;
        }

        private async Task<ThemeDropdownModel> GetProductByHandle(string handle)
        {
            if (string.IsNullOrWhiteSpace(handle))
            {
                _context.AddError("errors.data.no_valid","Dữ liệu không hợp lệ");
                return null;
            }
            var productTask = await _rpProduct.GetProductByHandle(handle);
            if (productTask.HasError)
            {
                _context.AddError(productTask.ErrorCodes[0].ToString(), productTask.Errors[0].ToString());
                return null;
            }
            return productTask.Data;
        }

        private async Task<ThemeDropdownModel> GetArticleByHandle(string handle)
        {
            if (string.IsNullOrWhiteSpace(handle))
            {
                _context.AddError("errors.data.no_valid","Dữ liệu không hợp lệ");
                return null;
            }
            var article = await _rpArticle.GetByHandle(extensionReq.StoreId, handle);
            if (article == null)
                return null;
            return new ThemeDropdownModel
            {
                Id = article._id,
                Name = article.title,
                Handle = article.url_handle,
                ImageUrl = article.image != null ? article.image.src : null
            };
        }

        private async Task<ThemeDropdownModel> GetLinkListByHandle(string handle)
        {
            if (string.IsNullOrWhiteSpace(handle))
            {
                _context.AddError("errors.data.no_valid","Dữ liệu không hợp lệ");
                return null;
            }
            var linklist = await _rpRawLinkList.GetByHandleUrl(handle, extensionReq.StoreId);
            if (linklist == null)
                return null;
            return new ThemeDropdownModel
            {
                Id = linklist._id,
                Name = linklist.LinkListName,
                Handle = linklist.LinkListHandle,
                ImageUrl = null
            };
        }

        private async Task<ThemeDropdownModel> GetCollectionByHandle(string handle)
        {
            if (string.IsNullOrWhiteSpace(handle))
            {
                _context.AddError("errors.data.no_valid","Dữ liệu không hợp lệ");
                return null;
            }
            var colTask = await _rpCollection.GetCollectionByHandle(handle);
            if (colTask.HasError)
            {
                _context.AddError(colTask.ErrorCodes[0].ToString(),colTask.Errors[0].ToString());
                return null;
            }
            return colTask.Data;
        }

        private async Task<ThemeDropdownModel> GetBlogByHandle(string handle)
        {
            var blog = await _bizBlog.GetByUrlHandle(handle);
            if (blog == null)
                return null;
            return new ThemeDropdownModel
            {
                Id = blog.Id,
                Name = blog.Title,
                Handle = blog.UrlHandle,
                ImageUrl = null
            };
        }

        private async Task<ThemeDropdownModel> GetPageByHandle(string handle)
        {
            if (string.IsNullOrWhiteSpace(handle))
            {
                _context.AddError("errors.data.no_valid","Dữ liệu không hợp lệ");
                return null;
            }
            var page = await _rpPage.GetByHandleUrl(handle, extensionReq.StoreId);
            if (page == null)
                return null;
            return new ThemeDropdownModel
            {
                Id = page._id,
                Name = page.title,
                Handle = page.handle,
                ImageUrl = null
            };
        }

        private async Task<(List<ThemeDropdownModel> datas, long totalRecord)> GetSimpleProductList(string query,
                                                           int page,
                                                           int limit)
        {
            var filter = buildFilterSearchModel(query, page, limit);
            var products = await _rpProduct.GetProductList(filter);
            if (products.datas != null && products.datas.Any())
            {
                var results = products.datas.Select(p => new ThemeDropdownModel
                {
                    Id = p.Id,
                    Name = p.ProductName,
                    Handle = p.ProductURL,
                    ImageUrl = p.Url
                }).ToList();
                return (results, products.totalRecord);
            }
            return (null, 0);
        }

        private async Task<(List<ThemeDropdownModel> datas, long totalRecord)> GetListCollectionSimple(string query,
                                                           int page,
                                                           int limit)
        {
            var filter = buildFilterSearchModel(query, page, limit);
            var lstCollection = new List<ThemeDropdownModel>();
            var data = await _rpCollection.GetCollectionList(filter);
            if (data.datas != null && data.datas.Any())
            {
                lstCollection = data.datas.Select(p => new ThemeDropdownModel
                {
                    Id = p.Id,
                    Name = p.Title,
                    Handle = p.UrlHandle,
                    ImageUrl = p.ImageUrl
                }).ToList();
                return (lstCollection, data.totalRecord);
            }
            return (null, 0);
        }

        private FilterSearchModel buildFilterSearchModel(
                                                           string query,
                                                           int page,
                                                           int limit)
        {
            var filter = new FilterSearchModel()
            {
                FreeText = query,
                Page = new FilterSearchPage()
                {
                    currentPage = page <= 0 ? 1 : page,
                    pageSize = limit <= 0 || limit > 50 ? 20 : limit
                },
                Fields = new List<FilterSearchField>()
            };
            return filter;
        }
    }
}