﻿namespace Haravan.Web.Api.Business
{
    using BHN.SharedObject.EBSMessage;
    using Haravan.BlobService;
    using Haravan.Libs.Abstractions;
    using Haravan.Web.Api.Business.Extensions;
    using Haravan.Web.Api.BusinessObjects;
    using Haravan.Web.Api.BusinessObjects.Models;
    using Haravan.Web.Api.Repository;
    using Microsoft.AspNetCore.Http;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using StringHelper = Haravan.Web.Api.Utils.Extensions.StringHelper;

    public class FileBusiness : IFileBusiness
    {
        private static Regex _imgUrlRegex = new Regex(
            @"^(.*)(.jpg|.png|.bmp|.gif|.jpeg)$", RegexOptions.Compiled);

        private readonly long _fileSizeMax = 20971520; // 20 MB
        private readonly IBlobServiceClientFactory blob;
        private readonly IFileRepository _rpFile;
        private readonly IExtensionPerRequest _extensionReq;
        private readonly IRequestContext _context;
        private readonly IMediaRepository rpMedia;

        public FileBusiness(
            IBlobServiceClientFactory blob,
            IFileRepository rpFile,
            IRequestContext context,
            IMediaRepository rpMedia,
        IExtensionPerRequest ExtensionReq
            )
        {
            this.blob = blob;
            this._rpFile = rpFile;
            this._context = context;
            this.rpMedia = rpMedia;
            this._extensionReq = ExtensionReq;
        }

        public async Task<FileUploadedInfo> UploadStorageFile(IFormFile file)
        {
            if (file == null)
            {
                _context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var data = await file.UploadFileHelper();

            var resultTask = await _rpFile.UploadStorageImage(file.FileName, data);
            if (resultTask.HasError)
            {
                _context.AddError(resultTask.ErrorCodes[0].ToString(),resultTask.Errors[0].ToString());
                return null;
            }
            if (resultTask.Data == null)
                return null;
            var result = new FileUploadedInfo(
                name: resultTask.Data.FileName,
                ext: resultTask.Data.Extension,
                size: resultTask.Data.Size,
                url: resultTask.Data.Url
                );
            return result;
        }

        public async Task<FileModel> UploadStorageImages(IFormFile file)
        {
            if (file == null)
            {
                _context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var data = await file.UploadFileHelper();
            var resultTask = await _rpFile.UploadStorageImage(file.FileName, data);
            if (resultTask.HasError)
            {
                _context.AddError(resultTask.ErrorCodes[0].ToString(), resultTask.Errors[0].ToString());
                return null;
            }
            if (resultTask.Data == null)
                return null;
            return resultTask.Data;
        }

        public async Task<FileModel> UploadStorageAllFile(IFormFile file)
        {
            if (file == null)
            {
                _context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var data = await file.UploadFileHelper();
            var resultTask = await _rpFile.UploadStorageFile(file.FileName, data);
            if (resultTask.HasError)
            {
                _context.AddError(resultTask.ErrorCodes[0].ToString(), resultTask.Errors[0].ToString());
                return null;
            }
            if (resultTask.Data == null)
                return null;
            return resultTask.Data;
        }

        public async Task<(List<FileModel> data, long totalRecord)> GetImageList(string query, int limit, int page)
        {
            var result = await _rpFile.GetImageList(query, limit, page, true);
            return (result.data, result.totalRecord);
        }

        public async Task<List<FileModel>> GetProductImagesListByProductId(long productId)
        {
            if (productId <= 0)
            {
                _context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var resultTask = await _rpFile.GetProductImagesListByProductId(productId);
            if (resultTask.HasError)
            {
                _context.AddError(resultTask.ErrorCodes[0].ToString(), resultTask.Errors[0].ToString());
                return null;
            }
            return resultTask.Data;
        }

        public async Task<bool> DeleteFile(long fileId)
        {
            if (fileId <= 0)
            {
                _context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return false;
            }
            var result = await rpMedia.DeleteFile(fileId);
            if (result.HasError)
            {
                _context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return false;
            }
            return result.Data;
        }

        public async Task<bool> DeleteAllSelectedFile(List<long> fileId)
        {
            var result = await rpMedia.DeleteAllSelectedFile(fileId);
            if (result.HasError)
            {
                _context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return false;
            }
            return result.Data;
        }

        public async Task<string> AddTemporaryFile(string fileName, Stream stream)
        {
            var objFile = await _uploadTmpFile(stream, fileName);
            return objFile.Url;
        }

        public async Task<FileUploadedInfo> AddImageFromBase64String(string attactment, string filename, FileType fileType, long storeId)
        {
            var type = fileType == FileType.ArticleFeatureImage
                ? "article" : "file";

            int quantityImage = _getQuantityImage(fileType);
            var path = $"/{storeId}/{type}";

            using (var stream = new MemoryStream(Convert.FromBase64String(attactment)))
            {
                return await _uploadImage(stream, path, filename, 2048, 2048, quantityImage);
            }
        }

        public async Task<FileUploadedInfo> AddImageFromUrl(string url, string fileName, FileType fileType, long storeId)
        {
            if (!StringHelper.CheckStringIsNullOrEmptyOrWhitespace(fileName))
            {
                fileName = blob.fileClient.FormatPath(fileName);
                var match = _imgUrlRegex.Match(fileName);
                if (!match.Success)
                {
                    fileName = "upload.jpg";
                }
            }
            else
            {
                fileName = blob.fileClient.FormatPath(Path.GetFileName(url));
                if (StringHelper.CheckStringIsNullOrEmptyOrWhitespace(fileName))
                {
                    fileName = "upload.jpg";
                }
                else
                {
                    var match = _imgUrlRegex.Match(fileName);
                    if (!match.Success)
                    {
                        fileName = "upload.jpg";
                    }
                }
            }

            int quantityImage = _getQuantityImage(fileType);

            var path = fileType == FileType.ArticleFeatureImage
                ? $"/{storeId}/article" : $"/{storeId}/file";

            return await _uploadImageUrl(url, path, fileName, 2048, 2048, quantityImage, fileType);
        }

        public async Task<FileUploadedInfo> UploadArticleFeatureImage(long storeId, string fileName, Stream stream)
        {
            var path = $"/{storeId}/article";
            return await _uploadImage(stream, path, fileName, 2048, 2048, DefaultData.Default85QuantityImage);
        }

        public async Task DeleteImages(List<string> imageUrls)
        {
            try
            {
                foreach (var imageUrl in imageUrls)
                {
                    await blob.fileClient.DeleteAsync(imageUrl);
                }
            }
            catch
            {
            }
        }

        private async Task<FileUploadedInfo> _uploadImage(Stream fileStream, string path, string fileName, int width, int height, int quantity)
        {
            _validateFileSize(fileStream);
            fileStream.Position = 0;

            var blobClient = blob.fileClient;
            var name = Path.GetFileNameWithoutExtension(fileName);
            var ext = Path.GetExtension(fileName);
            name = $"{blobClient.FormatPath(name)}_{Guid.NewGuid().ToString("n")}{ext?.ToLower()}";
            return await blobClient.UploadImageAsync(fileStream, path, name, width, height, quantity, true);
        }

        private async Task<FileUploadedInfo> _uploadImageUrl(
            string imgUrl, string filePath, string fileName, int width, int height, int quantity, FileType fileType = FileType.StorageFile)
        {
            var blobClient = blob.fileClient;
            var name = Path.GetFileNameWithoutExtension(fileName);
            var ext = Path.GetExtension(fileName);
            name = $"{blobClient.FormatPath(name)}_{Guid.NewGuid().ToString("n")}{ext?.ToLower()}";
            return await blobClient.UploadImageUrlAsync(imgUrl, filePath, name, width, height, quantity, true);
        }

        private void _validateFileSize(Stream fileStream)
        {
            var size = fileStream.Length;

            if (size > _fileSizeMax)
                throw new Exception("Maximum request length exceeded.");
        }

        private int _getQuantityImage(FileType fileType)
        {
            var lst85 = new List<FileType>() { FileType.ProductImage, FileType.CollectionImage, FileType.ArticleFeatureImage };
            return lst85.Contains(fileType) ? DefaultData.Default85QuantityImage : DefaultData.DefaultQuantityImage;
        }

        private async Task<FileUploadedInfo> _uploadTmpFile(Stream fileStream, string fileName)
        {
            _validateFileSize(fileStream);
            fileStream.Position = 0;
            var name = $"{blob.tempClient.FormatPath(fileName)}";
            var obj = await blob.tempClient.UploadTemporaryAsync(fileStream, "/", name);
            return obj;
        }

        private async Task<FileServiceResponse> _uploadFile(
            Stream fileStream, string filePath, string fileName, bool isOldMedia = false)
        {
            var size = fileStream.Length;

            if (size > _fileSizeMax)
                throw new Exception("Maximum request length exceeded.");
            if (size == 0)
            {
                return null;
            }

            var name = Path.GetFileNameWithoutExtension(fileName);
            var ext = Path.GetExtension(fileName);

            var blobClient = blob.themeClient;
            if (isOldMedia)
            {
                blobClient = blob.hstaticClient;
            }
            name = blobClient.FormatPath(name);
            name = $"{name}_{Guid.NewGuid().ToString("n")}{ext?.ToLower()}";

            var obj = await blobClient.UploadAsync(fileStream, filePath, name, true);
            if (obj != null)
            {
                return new FileServiceResponse()
                {
                    Ext = obj.Ext,
                    FileName = obj.Name,
                    FileUrl = obj.Url,
                    Size = obj.Size
                };
            }
            return null;
        }

        private async Task<FileServiceResponse> _uploadTmpFile(
           Stream fileStream, string filePath, string fileName)
        {
            _validateFileSize(fileStream);
            fileStream.Position = 0;
            var obj = await blob.tempClient.UploadTemporaryAsync(fileStream, filePath, fileName);
            if (obj != null)
            {
                return new FileServiceResponse()
                {
                    Ext = obj.Ext,
                    FileName = obj.Name,
                    FileUrl = obj.Url,
                    Size = obj.Size
                };
            }
            return null;
        }

        private class ThemeFileUploadData
        {
            public ThemeFileUploadData(
                string path, string fileName, bool isOverwrite, bool isOptimizeImage, bool isScssFile)
            {
                FileName = fileName;
                Path = path;
                IsOverwrite = isOverwrite;
                IsOptimizeImage = isOptimizeImage;
                IsScssFile = isScssFile;
            }

            public string Path { get; set; }
            public string FileName { get; set; }
            public bool IsOverwrite { get; set; }
            public bool IsOptimizeImage { get; set; }
            public bool IsScssFile { get; set; }
        }

        private ThemeFileUploadData OldMedia_GetThemeFileUploadData(long themeId, FileType fileType, string fileName)
        {
            var uploadFileName = fileName;

            bool isOverwrite = false,
                 isOptimizeImage = false,
                 isScssFile = false;

            if (fileType == FileType.Theme_Assets)
            {
                isOverwrite = true;
                isOptimizeImage = true;

                var match = _imgUrlRegex.Match(uploadFileName);

                if (match.Success && match.Groups[2].Value == ".gif")
                    isOptimizeImage = false;

                if (uploadFileName.EndsWith(".scss.css"))
                    isScssFile = true;
                else if (uploadFileName.EndsWith(".scss.liquid"))
                {
                    isScssFile = true;
                }
                else if (uploadFileName.EndsWith(".scss"))
                {
                    uploadFileName += ".css";
                    isScssFile = true;
                }
            }

            var path = GeneratePathForOldMediaFile(themeId, (int)fileType);

            return new ThemeFileUploadData(path, uploadFileName, isOverwrite, isOptimizeImage, isScssFile);
        }

        private ThemeFileUploadData _getThemeFileUploadData(long themeId, FileType fileType, string fileName)
        {
            var storeId = _extensionReq.StoreId;
            var uploadFileName = fileName;

            bool isOverwrite = false,
                 isOptimizeImage = false,
                 isScssFile = false;

            if (fileType == FileType.Theme_Assets)
            {
                isOverwrite = true;
                isOptimizeImage = true;

                var match = _imgUrlRegex.Match(uploadFileName);

                if (match.Success && match.Groups[2].Value == ".gif")
                    isOptimizeImage = false;

                if (uploadFileName.EndsWith(".scss.css"))
                    isScssFile = true;
                else if (uploadFileName.EndsWith(".scss"))
                {
                    uploadFileName += ".css";
                    isScssFile = true;
                }
            }
            var path = $"/{storeId}/{themeId}/{(int)fileType}";

            return new ThemeFileUploadData(path, uploadFileName, isOverwrite, isOptimizeImage, isScssFile);
        }

        public async Task<FileServiceResponse> UploadSettingDataTmp(long themeId, FileType fileType, string fileName, Stream stream, bool isNewFileService)
        {
            ThemeFileUploadData uploadData = null;
            if (isNewFileService)
            {
                uploadData = _getThemeFileUploadData(themeId, fileType, fileName);
            }
            else
            {
                uploadData = OldMedia_GetThemeFileUploadData(themeId, fileType, fileName);
            }
            var path = uploadData.Path;
            var name = uploadData.FileName;
            var isOverwrite = uploadData.IsOverwrite;
            return await _uploadTmpFile(stream, path, name);
        }

        private string GeneratePathForOldMediaFile(long themeId, int type)
        {
            var storeId = _extensionReq.StoreId;
            var path = $"/{getStoreFolder(storeId.ToString())}/{themeId}";
            if (type == (int)ThemeFileType.Layout || type == (int)ThemeFileType.Template || type == (int)ThemeFileType.Snippet || type == (int)ThemeFileType.Config || type == (int)ThemeFileType.Locale)
            {
                var year = DateTime.UtcNow.Year;
                var month = DateTime.UtcNow.Month;
                var day = DateTime.UtcNow.Day;
                path += $"/{(int)type}/{year}/{month}-{day}";
            }
            return path;
        }

        private static string getStoreFolder(string storeId)
        {
            if (string.IsNullOrEmpty(storeId))
            {
                return storeId;
            }

            if (storeId.Length < 3)
                return storeId + "/" + storeId;
            else
            {
                return storeId.Substring(storeId.Length - 3, 3) + "/" + storeId;
            }
        }

        public async Task<FileServiceResponse> _uploadOverwriteFile(
        Stream fileStream, string filePath, string fileName, bool isOldMedia = false)
        {
            var size = fileStream.Length;

            if (size > _fileSizeMax)
                throw new Exception("Maximum request length exceeded.");
            if (size == 0)
            {
                return null;
            }

            var blobClient = blob.themeClient;
            if (isOldMedia)
            {
                blobClient = blob.hstaticClient;
            }

            var path = blobClient.FormatPath($"{filePath}/{fileName}");
            var obj = await blobClient.SetAsync(fileStream, path);
            if (obj > 0)
            {
                var fileUrl = $"{blobClient.Host}{path}";

                return new FileServiceResponse()
                {
                    Size = size,
                    FileUrl = fileUrl,
                    FileName = fileName,
                    Ext = Path.GetExtension(fileName)
                };
            }
            return null;
        }

        public async Task<string> GetContent(string url)
        {
            var content = string.Empty;

            using (var stream = await blob.themeClient.GetAsync(url))
            {
                using (var reader = new StreamReader(stream))
                {
                    content = await reader.ReadToEndAsync();
                }
            }
            return content;
        }

        public async Task<string> GetStaticContent(string url)
        {
            var content = string.Empty;
            var stream = await blob.hstaticClient.GetAsync(url);
            using (stream)
            {
                using (var reader = new StreamReader(stream))
                {
                    content = await reader.ReadToEndAsync();
                }
            }
            return content;
        }

        public async Task<FileUploadedInfo> UploadScriptTag(string fileContent, string fileName)
        {
            using (var fileStream = new MemoryStream(new UTF8Encoding(false).GetBytes(fileContent)))
            {
                _validateFileSize(fileStream);
                fileStream.Position = 0;
                var blobClient = blob.hstaticClient;
                var name = Path.GetFileNameWithoutExtension(fileName);
                var ext = Path.GetExtension(fileName);
                name = $"{blobClient.FormatPath(name)}_{Guid.NewGuid().ToString("n")}{ext?.ToLower()}";
                var filePath = GeneratePathForScripttag();
                return await blobClient.UploadAsync(fileStream, filePath, name, true);
            }
        }

        public async Task<FileModel> OldMedia_UploadThemeFile(long themeId, FileType fileType, string fileName, Stream fileStream, int? maxWidth = null, int? maxHeight = null)
        {
            var uploadData = OldMedia_GetThemeFileUploadData(themeId, fileType, fileName);
            var path = uploadData.Path;
            var name = uploadData.FileName;
            FileServiceResponse rp;
            if (uploadData.IsScssFile)
            {
                rp = await _uploadScss(fileStream, path, name, true);
            }
            else if (maxWidth.HasValue && maxHeight.HasValue)
            {
                rp = await _uploadOverwriteImage(fileStream, path, name, maxWidth.Value, maxHeight.Value, true);
            }
            else
            {
                if (uploadData.IsOverwrite)
                {
                    rp = await _uploadOverwriteFile(fileStream, path, name, true);
                }
                else
                {
                    rp = await _uploadFile(fileStream, path, name, true);
                }
            }

            return new FileModel()
            {
                ThemeId = themeId,
                FileName = fileName,
                Size = (int)rp.Size,
                Type = fileType,
                Url = rp.FileUrl
            };
        }

        public async Task<FileModel> UploadThemeFile(
            long themeId, FileType fileType, string fileName, Stream stream,
            long? rootFileId = null, bool isAddFileToDb = true, bool isCommit = true,
            int? maxWidth = null, int? maxHeight = null)
        {
            return await _uploadThemeFile(
                themeId, fileType, fileName, stream, rootFileId, isAddFileToDb, isCommit, maxWidth, maxHeight);
        }

        private async Task<FileModel> _uploadThemeFile(
            long themeId, FileType fileType, string fileName, Stream fileStream, long? rootFileId,
            bool isAddFileToDb, bool isCommit,
            int? maxWidth = null, int? maxHeight = null)
        {
            var uploadData = _getThemeFileUploadData(themeId, fileType, fileName);
            var path = uploadData.Path;
            var name = uploadData.FileName;
            FileServiceResponse rp = null;
            if (uploadData.IsScssFile)
            {
                rp = await _uploadScss(fileStream, path, name);
            }
            else if (maxWidth.HasValue && maxHeight.HasValue)
            {
                rp = await _uploadOverwriteImage(fileStream, path, name, maxWidth.Value, maxHeight.Value);
            }
            else
            {
                if (uploadData.IsOverwrite)
                    rp = await _uploadOverwriteFile(fileStream, path, name);
                else
                {
                    rp = await _uploadFile(fileStream, path, name);
                }
            }

            var newFile = new FileModel()
            {
                ThemeId = themeId,
                FileName = fileName,
                Size = (int)rp.Size,
                Type = fileType,
                Url = rp.FileUrl
            };

            if (isAddFileToDb)
            {
                var rs = rootFileId.HasValue
                    ? await _addFile(newFile, rootFileId.Value, isCommit)
                    : await _addFile(newFile, isCommit);

                return rs;
            }
            else
                return newFile;
        }

        private async Task<FileModel> _addFile(FileModel file, bool isCommit = true)
        {
            var modeladdFileRp = await _rpFile.AddFile(file, null, isCommit);
            if (modeladdFileRp.HasError)
            {
                _context.AddError(modeladdFileRp.ErrorCodes[0].ToString(), modeladdFileRp.Errors[0].ToString());
                return null;
            }
            return modeladdFileRp.Data;
        }

        private async Task<FileModel> _addFile(FileModel file, long rootFileId, bool isCommit = true)
        {
            var modeladdFileRp = await _rpFile.AddFile(file, rootFileId, isCommit);
            if (modeladdFileRp.HasError)
            {
                _context.AddError(modeladdFileRp.ErrorCodes[0].ToString(), modeladdFileRp.Errors[0].ToString());
                return null;
            }
            return modeladdFileRp.Data;
        }

        private string GeneratePathForScripttag()
        {
            var year = DateTime.UtcNow.Year;
            var month = DateTime.UtcNow.Month;
            var day = DateTime.UtcNow.Day;

            return $"/{getStoreFolder(_extensionReq.StoreId.ToString())}/{(int)FileType.ScriptTagFile}/{year}/{month}-{day}";
        }

        private async Task<FileServiceResponse> _uploadOverwriteImage(
            Stream fileStream, string filePath, string fileName, int width, int height, bool isOldMedia = false)
        {
            var size = fileStream.Length;

            if (size > _fileSizeMax)
                throw new Exception("Maximum request length exceeded.");
            if (size == 0)
            {
                return null;
            }
            fileStream.Position = 0;
            var blobClient = blob.themeClient;
            if (isOldMedia)
            {
                blobClient = blob.hstaticClient;
            }

            fileStream.Position = 0;

            var path = blobClient.FormatPath($"{filePath}/{fileName}");
            var obj = await blobClient.SetImageAsync(fileStream, path, width, height);
            if (obj > 0)
            {
                var fileUrl = $"{blobClient.Host}{path}";
                return new FileServiceResponse()
                {
                    Size = size,
                    FileUrl = fileUrl
                };
            }
            return null;
        }

        private async Task<FileServiceResponse> _uploadScss(
           Stream fileStream, string filePath, string fileName, bool isOldMedia = false)
        {
            var size = fileStream.Length;

            if (size > _fileSizeMax)
                throw new Exception("Maximum request length exceeded.");
            if (size == 0)
            {
                return null;
            }
            fileStream.Position = 0;
            var blobClient = blob.themeClient;
            if (isOldMedia)
            {
                blobClient = blob.hstaticClient;
            }

            fileStream.Position = 0;
            fileName = blobClient.FormatPath(fileName);
            var obj = await blobClient.UploadAsync(fileStream, filePath, fileName, true);
            if (obj != null)
            {
                return new FileServiceResponse()
                {
                    Ext = obj.Ext,
                    FileName = obj.Name,
                    FileUrl = obj.Url,
                    Size = obj.Size
                };
            }
            return null;
        }
    }
}