﻿using BHN.SharedObject.EBSMessage;
using Haravan.Libs.Abstractions;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using Haravan.Web.Api.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public class SummaryBusiness : ISummaryBusiness
    {
        private readonly IRequestContext context;
        private readonly IServiceProvider serviceProvider;
        private readonly IMGSummaryRepository rpSum;
        private readonly IMGSummaryTotalRepository rpSummaryTotal;
        private readonly IExtensionPerRequest ExtensionReq;
        private readonly long _storeId;

        private const int _limitSummary = 200;
        private const int _limitSummaryTotal = 500;

        public SummaryBusiness(
            IRequestContext context,
            IServiceProvider serviceProvider,
            IMGSummaryRepository rpSum,
            IMGSummaryTotalRepository rpSummaryTotal,
            IExtensionPerRequest ExtensionReq
            )
        {
            this.context = context;
            this.ExtensionReq = ExtensionReq;
            this.serviceProvider = serviceProvider;
            this.rpSum = rpSum;
            this.rpSummaryTotal = rpSummaryTotal;
            _storeId = this.ExtensionReq.OrgId;
        }

        public async Task<List<MGSummaryModel>> GetSummaries(SummaryType Type, long storeId, long RefId)
        {
            return await rpSum.GetSummaries((int)Type, _storeId, RefId);
        }

        public async Task<long> ExistSummaryName(SummaryType Type, long RefId, string name)
        {
            return await rpSum.ExistSummaryName((int)Type, RefId, _storeId, name);
        }

        public async Task<List<string>> GetSummaryNames(SummaryType Type, long RefId)
        {
            return await rpSum.GetSummaryNames((int)Type, RefId, _storeId);
        }

        public async Task<List<string>> GetSummaryTotalNames(SummaryType Type)
        {
            return await rpSummaryTotal.GetSummaryTotalNames((int)Type, _storeId);
        }

        public async Task AddSummaryNamesForRefIds(SummaryType Type, long[] RefIds, List<string> SummaryNames)
        {
            if (SummaryNames == null || SummaryNames.Count == 0)
            {
                return;
            }

            if (!RefIds.Any())
                return;

            if (RefIds.Where(id => id < 0).Any())
            {
                context.AddError("errors.code.no_valid", "Mã không hợp lệ.");
                return;
            }

            if (RefIds.Length > 250)
            {
                context.AddError("errors.data.too_big", "Dữ liệu quá lớn");
                return;
            }

            if (SummaryNames.Any())
            {
                for (var i = 0; i < SummaryNames.Count; i++)
                {
                    SummaryNames[i] = SummaryNames[i].Trim();
                }
            }
            foreach (var RefId in RefIds)
            {
                var currentNames = await GetSummaryNames(Type, RefId);

                if (currentNames == null)
                    currentNames = new List<string>();

                if (SummaryNames.Any(a => !currentNames.Contains(a)))
                {
                    currentNames = currentNames.Union(SummaryNames).ToList();

                    await SetSummaryNames(Type, RefId, currentNames);
                }
            }
        }

        public async Task SetSummaryNames(SummaryType Type, long RefId, List<string> SummaryNames)
        {
            if (RefId < 0)
            {
                context.AddError("errors.code.no_valid", "Mã không hợp lệ.");
                return;
            }

            if (SummaryNames == null)
                SummaryNames = new List<string>();

            SummaryNames = SummaryNames.Where(m => !string.IsNullOrWhiteSpace(m)).Take(_limitSummary).ToList();

            if (SummaryNames.Any())
            {
                for (var i = 0; i < SummaryNames.Count; i++)
                {
                    string strValue = SummaryNames[i].Trim();
                    if (strValue.Contains(","))
                    {
                        strValue = strValue.Replace(",", "^[]^");
                    }
                    SummaryNames[i] = strValue;
                }
            }

            int type = (int)Type;

            // update IMGSummaryRepository
            var listSummary = await GetSummaries(Type, _storeId, RefId);

            foreach (var summary in listSummary)
            {
                summary.name = "aa";
                // await rpArticle.SetAsync(article);
            }

            //MGSummaryTotalRepository

            //using (var scope = BusinessExtensions.GetDBTranScope(serviceProvider))
            //{
            //    var hasChanged = await rpSum.CalcSummaryNames(type, RefId, _storeId, SummaryNames);

            //    scope.Complete();
            //    if (hasChanged)
            //    {
            //        await TriggerSummaryChanged(type);

            //        var listSummary = await GetSummaries(Type, _storeId,RefId);
            //        var listSummaryTotal = await GetSummaryTotals(Type);

            //        var msgSetSummary = new SetSummaryMsg()
            //        {
            //            Action = MessageAction.Update,
            //            StoreId = _storeId,
            //            RefId = RefId,
            //            Type = type,
            //            IdNames = listSummary == null
            //                ? new List<SummarySimple>()
            //                : listSummary.Select(a => new SummarySimple() { Id = a._id, Name = a.name }).ToList()
            //        };
            //        var msgSetSummaryTotal = new SetSummaryTotalMsg()
            //        {
            //            Action = MessageAction.Update,
            //            StoreId = _storeId,
            //            Type = type,
            //            IdNameCounts = listSummaryTotal == null
            //                ? new List<SummaryTotalSimple>()
            //                : listSummaryTotal.Select(a => new SummaryTotalSimple() { Id = a._id, Name = a.name, Count = a.count }).ToList()
            //        };
            //        //await rpCom.SendMsgAsync(msgSetSummary);
            //        //await rpCom.SendMsgAsync(msgSetSummaryTotal);
            //    }
            //}
        }

        private async Task<List<MGSummaryTotalModel>> GetSummaryTotals(SummaryType Type)
        {
            return await rpSummaryTotal.GetSummaryTotals((int)Type, _storeId);
        }
    }
}