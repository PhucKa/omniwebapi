﻿using Haravan.Libs.Abstractions;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public class LocationBusiness : ILocationBusiness
    {
        private readonly IRequestContext _context;
        private readonly ILocationRepository _rpLocation;

        public LocationBusiness(IRequestContext requestContext,
            ILocationRepository rpLocation
            )
        {
            this._context = requestContext;
            this._rpLocation = rpLocation;
        }

        public async Task<List<LocationSimpleModel>> GetLocationSimpleList()
        {
            var locations = await _rpLocation.GetAvailableLocationList();
            if (locations != null && locations.Any())
            {
                var results = new List<LocationSimpleModel>();
                results = locations.Select(p => new LocationSimpleModel
                {
                    Id = p.Id,
                    LocationName = p.LocationName,
                    Address = p.Address,
                    IsPimaryLocation = p.IsPimaryLocation
                }).ToList();
                return results;
            }
            return null;
        }
    }
}