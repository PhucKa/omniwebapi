﻿using BHN.Core;
using BHN.SharedObject.APIDataModel;
using BHN.SharedObject.EBSMessage;
using BHN.SharedObject.EBSMessage.LogSeller;
using Haravan.Libs.Abstractions;
using Haravan.Web.Api.Business.Extensions;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Enums;
using Haravan.Web.Api.BusinessObjects.ESModels;
using Haravan.Web.Api.BusinessObjects.Mappers;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.BusinessObjects.Models.Common;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using Haravan.Web.Api.Repository;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HtmlStringHelpers = Haravan.Web.Api.Utils.Extensions.HtmlStringHelpers;
using StringHelper = Haravan.Web.Api.Utils.Extensions.StringHelper;

namespace Haravan.Web.Api.Business
{
    public class BlogBusiness : IBlogBusiness
    {
        private readonly long? _userId;
        private readonly IRequestContext requestContext;
        private readonly IMGBlogRepository rpBlog;
        private readonly IMGArticleRepository rpArticle;
        private readonly ICommentBusiness bizComment;
        private readonly IKeywordBusiness bizKeyword;
        private readonly IArticleBusiness bizArticle;
        private readonly INavigationBusiness bizNavigation;
        private readonly IMGFeedBurnerProviderRepository rpFeedBurner;
        private readonly ITagRepository rpTag;
        private readonly IThemeRepository rpTheme;
        private readonly IServiceProvider _serviceProvider;
        private readonly IEBSComMessageRepository _bus;
        private readonly IMGLogDataRepository _mgLog;
        private readonly IMetafieldsEcomRepository rpMetafield;
        private readonly IExtensionPerRequest ExtensionReq;
        
        private readonly IESBlogRepository rpESBlog;
        private readonly IESArticleRepository rpESArticle;
        private readonly IESBlogSinkRepository rpESBlogSearch;
        private readonly IESArticleSinkRepository rpESArticleSearch;

        public BlogBusiness(IRequestContext requestContext,
            IEBSComMessageRepository _bus,
            IMGLogDataRepository _mgLog,
            IMGBlogRepository rpBlog,
            IESBlogRepository rpESBlog,
            ICommentBusiness bizComment,
            IMGArticleRepository rpArticle,
            INavigationBusiness rpNavigation,
            IArticleBusiness bizArticle,
            IMGFeedBurnerProviderRepository rpFeedBurner,
            IThemeRepository rpTheme,
            ITagRepository rpTag,
            IServiceProvider serviceProvider,
            IKeywordBusiness bizKeyword,
            IESArticleRepository rpESArticle,
            IMetafieldsEcomRepository rpMetafield,
            IExtensionPerRequest ExtensionReq,
            IESBlogSinkRepository rpESBlogSearch,
            IESArticleSinkRepository rpESArticleSearch)
        {
            this.ExtensionReq = ExtensionReq;
            this.requestContext = requestContext;
            this.rpBlog = rpBlog;
            this.rpESBlog = rpESBlog;
            this.bizArticle = bizArticle;
            this.bizNavigation = rpNavigation;
            this.rpArticle = rpArticle;
            this.bizKeyword = bizKeyword;
            this.rpFeedBurner = rpFeedBurner;
            this.rpTheme = rpTheme;
            this.rpTag = rpTag;
            this._userId = requestContext.UserId;
            this._serviceProvider = serviceProvider;
            this._bus = _bus;
            this._mgLog = _mgLog;
            this.rpMetafield = rpMetafield;
            this.rpESArticle = rpESArticle;
            this.bizComment = bizComment;
            this.rpESBlogSearch = rpESBlogSearch;
            this.rpESArticleSearch= rpESArticleSearch;
        }

        #region public

        public async Task<BlogDetailModel> GetByUrlHandle(string urlHandle)
        {
            if (string.IsNullOrWhiteSpace(urlHandle))
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var blog = await rpBlog.GetByHandleUrl(urlHandle, ExtensionReq.OrgId);
            return blog.BlogToToModelDetail();
        }

        public async Task<BlogDetailModel> GetBlogFirstByHandle()
        {
            var resultModel = new BlogDetailModel();
            var blog = await rpBlog.GetFirstByHandle(ExtensionReq.StoreId);
            if (blog == null)
            {
                requestContext.AddError("errors.article.not_exist", "Bài viết không tồn tại");
                return null;
            }
            resultModel = blog.BlogToToModelDetail();
            return resultModel;
        }

        public async Task<List<BlogDetailModel>> GetListByListHandleUrl(List<string> urlHandles)
        {
            if (urlHandles == null || !urlHandles.Any())
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var blogs = await rpBlog.GetByListUrlHandle(ExtensionReq.OrgId, urlHandles);
            return blogs.MGBlogsToToDetails();
        }

        public async Task<long> GetIdByHandleUrl(string handleUrl)
        {
            return await rpBlog.GetByUrlHandle(handleUrl, ExtensionReq.StoreId);
        }

        public async Task<List<BlogDetailViewModelInternal>> GetByListUrlHandle(List<string> listUrlHandle)
        {
            var model = new List<BlogDetailViewModelInternal>();
            if (listUrlHandle == null)
            {
                requestContext.AddError("errors.blog.handle_url.not_valid", "Thông tin handleUrl không hợp lệ");
                return null;
            }
            var list = await rpBlog.GetByListUrlHandle(ExtensionReq.StoreId, listUrlHandle);
            if (list != null)
            {
                model = list.ListBlogDetailToListBlogModel();
            }
            return model;
        }

        public async Task<BlogDetailViewModelInternal> GetByUrlHandleV2(string handleUrl)
        {
            var model = new BlogDetailViewModelInternal();
            var data = await rpBlog.GetByHandleUrl(handleUrl, ExtensionReq.StoreId);
            model = data.BlogDetailToBlogModel();
            return model;
        }

        public async Task<bool> Init_NewStore()
        {
            var blogStore0 = await rpBlog.GetBlogsByStoreId(DefaultData.DefaultStoreId);
            foreach (var blog in blogStore0)
            {
                blog._id = 0;
                blog.created_at = DateTime.UtcNow.AddHours(-1);
                blog.published_at = DateTime.UtcNow.AddHours(-1);
                var blogDetailModel = blog.BlogToToModelDetail();
                await AddNewAsync(blogDetailModel);
            }
            return true;
        }

        public async Task<List<OmniSimpleModel>> GetSimpleBlogList(long blogId)
        {
            if (blogId <= 0)
            {
                requestContext.AddError("errors.blog.not_valid", "Thông tin Blog không hợp lệ");
                return null;
            }
            var list = await rpBlog.GetById(ExtensionReq.StoreId, blogId);
            List<OmniSimpleModel> BlogsList = new List<OmniSimpleModel>();
            if (list != null)
            {
                BlogsList.Add(new OmniSimpleModel() { id = list._id, name = list.title });
            }
            return BlogsList;
        }

        public async Task<bool> DeleteBlogByIdAsync(long blogId)
        {
            var blogIdToDelete = new List<long>();
            blogIdToDelete.Add(blogId);
            return await DeleteBlogId(blogIdToDelete);
        }

        public async Task<List<DropdownSimpleModel>> GetTemplateByName()
        {
            var list = await rpTheme.GetTemplateByName();
            List<DropdownSimpleModel> themes = new List<DropdownSimpleModel>();
            if (list != null)
            {
                var it = list.Select((s, i) => new DropdownSimpleModel { Id = i, Name = s }).ToList();
                themes.AddRange(it);
            }
            return themes;
        }

        public async Task<List<string>> GetTemplateName()
        {
            var list = await rpTheme.GetTemplateByName();
            return list;
        }

        public async Task<List<DropdownSimpleModel>> GetListFeedburnerSimpleAsync()
        {
            var data = new List<DropdownSimpleModel>();
            var result = await rpFeedBurner.GetAll();
            data = result.BurnerProviderList().OrderBy(a => a.Id).ToList();
            return data;
        }

        public async Task<(List<ThemeDropdownModel> data, long totalRecord)> GetSimpleBlogsListForTheme(bool isVisible,
                                                   string query,
                                                   int page,
                                                   int limit)
        {
            var data = new List<ThemeDropdownModel>();
            var blogs = await GetBlogList(isVisible, false, query, null, null, page, limit, null);
            if (blogs.data != null && blogs.data.Any())
            {
                data = blogs.data.Select(m => new ThemeDropdownModel()
                {
                    Id = m.Id,
                    Name = m.Title,
                    Handle = m.UrlHandle,
                    ImageUrl = null
                }).ToList();
            }
            var totalRecord = blogs.totalrecord;
            return (data, totalRecord);
        }

        public async Task<(List<DropdownSimpleModel> data, long totalRecord)> GetSimpleBlogsList(bool isVisible,
                                                   string query,
                                                   int page,
                                                   int limit)
        {
            var data = new List<DropdownSimpleModel>();
            var Model = await GetBlogList(isVisible, false, query, null, null, page, limit, null);
            if (Model.data != null && Model.data.Any())
            {
                data = Model.data.Select(m => new DropdownSimpleModel()
                {
                    Id = m.Id,
                    Name = m.Title
                }).ToList();
            }
            var totalRecord = Model.totalrecord;
            return (data, totalRecord);
        }

        public async Task<(List<BlogSearchRowModel> data, long totalrecord)> GetGlobalSearchListBlog(FilterSearchModel filter)

        {
            var data = new List<BlogSearchRowModel>();
            if (filter == null)
            {
                requestContext.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return (data, 0);
            }
            var result = await getMGBlogList(filter);
            return (result.data.BlogToModelList(), result.totalRecord);
        }

        public async Task<(List<BlogSearchRowModel> data, long totalrecord)> GetBlogList(
                                                                            bool visible, bool hidden, string query, string order, string direction, int page, int limit, string collection_type
                                                                      )

        {
            var filter = buildFilterSearchModel(
                                                        visible,
                                                        hidden,
                                                        query,
                                                        order,
                                                        direction,
                                                        page,
                                                        limit, collection_type
                                                    );

            var data = new List<BlogSearchRowModel>();
            if (filter == null)
            {
                requestContext.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return (data, 0);
            }
            var result = await getMGBlogList(filter);
            return (result.data.BlogToModelList(), result.totalRecord);
        }

        public async Task<(List<BlogSearchRowModel> data, long totalrecord)> GetBlogList(FilterSearchModel filter)
        {
            var data = new List<BlogSearchRowModel>();
            if (filter == null)
            {
                requestContext.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return (data, 0);
            }
            var result = await getMGBlogList(filter);
            return (result.data.BlogToModelList(), result.totalRecord);
        }

        public async Task<List<BlogAPIModel>> GetBlogListAPI(FilterSearchModel filter)
        {
            var data = new List<BlogAPIModel>();
            if (filter == null)
            {
                requestContext.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return data;
            }
            var result = await getMGBlogList(filter);
            if (result.data != null)
            {
                foreach (var item in result.data)
                {
                    var feedProvider = await rpFeedBurner.GetByIdAsync(item.feedburnerId);
                    if (feedProvider != null)
                    {
                        item.feed_burner_url = feedProvider.url;
                        item.feedburnerId = feedProvider._id;
                    }
                }
            }
            data = result.data.MGBlogListToApiBlogList();
            return data;
        }

        public async Task<long> CountBlogs(FilterSearchModel filter)
        {
            if (filter == null)
            {
                requestContext.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return 0;
            }
            var result = await getMGBlogList(filter);
            return result.totalRecord;
        }

        private async Task<(List<MGBlogModel> data, long totalRecord)> getMGBlogList(FilterSearchModel filter)
        {
            long totalRecord = 0;

            if (filter == null)
            {
                requestContext.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return (null, 0);
            }
            var _storeId = ExtensionReq.OrgId;
            var isDefault = IsGetDataFromMongo(filter);
            var result = new List<MGBlogModel>();
            var dicIds = new Dictionary<int, long>();
            if (isDefault)
            {
                totalRecord = await rpBlog.CountAsync(_storeId);
                var ids = await rpBlog.GetByPagingAsync(_storeId, filter.Page.currentPage, filter.Page.pageSize);
                if (ids != null && ids.Any())
                {
                    for (int i = 0; i < ids.Count; i++)
                    {
                        dicIds.Add(i + 1, ids[i]);
                    }
                }
            }
            else
            {
                if (filter.Fields == null && filter.SortFieldName == null && filter.SortType == null)
                {
                    filter.SortFieldName = "created_at";
                    filter.SortType = "desc";
                }
                else if (filter.SortFieldName != null && filter.SortType == null)
                {
                    filter.SortType = "desc";
                }
                else if (filter.SortFieldName == null && filter.SortType != null)
                {
                    filter.SortFieldName = "created_at";
                }
                else
                {
                    filter.SortFieldName = "created_at";
                    filter.SortType = "desc";
                }
                var rs = new ESSearchResult();
                rs = await rpESBlogSearch.SearchAsync(_storeId, filter);
                totalRecord = rs.Total;
                dicIds = rs.Ids;
            }
            if (dicIds != null && dicIds.Any())
            {
                result = await _priGetByIds(ExtensionReq.StoreId, dicIds);

                var ids = dicIds.Select(m => m.Value);
                var lstSummaries = await rpTag.GetListTag(ExtensionReq.StoreId, new int[] {
                        (int)BHN.SharedObject.EBSMessage.SummaryType.Blog_ArticlesTag }.ToArray(), ids.ToArray());
                if (lstSummaries != null && lstSummaries.Any())
                {
                    result.ForEach(r =>
                    {
                        r.tags = lstSummaries.Where(m => m.RefId == r._id).Select(m => m.Name).ToArray();
                    });
                }
            }
            return (result, totalRecord);
        }

        private async Task<List<MGBlogModel>> _priGetByIds(long orgid, Dictionary<int, long> dids)
        {
            var ids = dids.Select(m => m.Value).ToList();
            var _storeId = ExtensionReq.OrgId;
            var result = await rpBlog.GetById(_storeId, ids);
            var tmp = from v in result
                      join k in dids on v._id equals k.Value
                      orderby k.Key
                      select v;
            return tmp.ToList();
        }

        public async Task<BlogDetailModel> AddNewAsync(BlogDetailModel model, List<NavigationDetailModel> linkList = null)
        {
            var newblog = ValidateBlogInfo(model);
            if (newblog == null)
            {
                return null;
            }
            var _storeId = ExtensionReq.OrgId;
            newblog.handle = await bizKeyword.ProcessingKeyHandle(_storeId, (int)EnumDocumentType.Blog, null, model.UrlHandle);
            newblog.url = "/blogs/" + newblog.handle;
            newblog.moderated = model.CommentRule != 3 ? true : false;
            var id = await _priSetBlog(newblog);
            if (model.metafields != null && model.metafields.Count > 0)
            {
                foreach (var objMetafield in model.metafields)
                {
                    var objValueType = MetafieldValueType.String;
                    if ("integer" == objMetafield.value_type.Trim().ToLower())
                    {
                        objValueType = MetafieldValueType.Integer;
                    }
                    await rpMetafield.AddMetafield(
                        MetafieldResourceOwner.Blog,
                        newblog._id,
                        objMetafield.@namespace,
                        objMetafield.key,
                        objMetafield.value,
                        objValueType,
                        objMetafield.description,
                        true);
                }
            }

            if (linkList != null)
            {
                await bizNavigation.SetLinkByRefId(newblog._id, newblog.title, linkList, LinkFieldType.Blog);
            }
            var blog = await rpBlog.GetById(ExtensionReq.StoreId, id);
            await WriteLogDashboard((int)DashboardLogAction.Blog_Create, blog.BlogToToModelDetail());
            await _bus.FireStoreDataChange(ExtensionReq.OrgId);
            return blog.BlogToToModelDetail();
        }

        public async Task<BlogAPIModel> AddNewApiAsync(BlogDetailModel model)
        {
            var blog = await AddNewAsync(model);
            var _storeId = ExtensionReq.OrgId;
            var mgblog = await rpBlog.GetById(_storeId, blog.Id);
            return mgblog.MGBlogToApiBlog();
        }

        public async Task<BlogDetailModel> UpdateAsync(BlogDetailModel model, List<NavigationDetailModel> linkList = null, bool isAPICall = false)
        {
            var newblog = ValidateBlogInfo(model);
            if (newblog == null)
            {
                return null;
            }
            var blog = await rpBlog.GetById(ExtensionReq.StoreId, model.Id);
            if (blog == null)
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var isChangeCommentRule = false;
            if (blog.commentrule != newblog.commentrule)
            {
                isChangeCommentRule = true;
            }
            if (string.IsNullOrWhiteSpace(newblog.meta_description))
            {
                newblog.meta_description = blog.meta_description;
            }
            var oldUrlHandle = blog.handle;
            if (oldUrlHandle != newblog.handle)
            {
                var _storeId = ExtensionReq.OrgId;
                newblog.handle = await bizKeyword.ProcessingKeyHandle(_storeId, (int)EnumDocumentType.Blog, oldUrlHandle, newblog.handle);
                newblog.url = "/blogs/" + newblog.handle;
            }
            newblog.moderated = model.CommentRule != 3 ? true : false;
            var id = await _priUpdateBlog(newblog);

            if (oldUrlHandle != newblog.handle)
            {
                var lstArticles = await rpArticle.GetByBlogIdAsync(ExtensionReq.StoreId, blog._id);
                if (lstArticles != null && lstArticles.Any())
                {
                    foreach (var item in lstArticles)
                    {
                        item.bloghandleurl = model.UrlHandle;
                        item.url = model.UrlHandle;
                        var mgarticle = await rpArticle.SetAsync(item);
                    }
                }
                await bizNavigation.Blog_UpdateForLinkUrl(newblog._id);
            }

            if (isChangeCommentRule)
            {
                await bizComment.ApproveByChangeBlogCommentRuleAsync(newblog._id);
            }

            if (linkList != null)
            {
                await bizNavigation.SetLinkByRefId(newblog._id, newblog.title, linkList, LinkFieldType.Blog);
            }
            var objBlog = (await rpBlog.GetById(ExtensionReq.StoreId, id)).BlogToToModelDetail();
            await WriteLogDashboard((int)DashboardLogAction.Blog_Update, objBlog);
            await _bus.FireStoreDataChange(ExtensionReq.OrgId);
            return objBlog;
        }

        public async Task<bool> DeleteBlogId(List<long> blogIds)
        {
            if (blogIds == null || blogIds.Count <= 0)
            {
                requestContext.AddError("errors.information.no_valid", "Thông tin không hợp lệ.");
                return false;
            }
            foreach (var id in blogIds)
            {
                await DeleteBlogidAsync(id);
            }
            return true;
        }

        public async Task<bool> DeleteBlogidAsync(long blogId)
        {
            if (blogId <= 0)
            {
                requestContext.AddError("errors.information.no_valid", "Thông tin không hợp lệ.");
                return false;
            }
            var _storeId = ExtensionReq.OrgId;
            var existBlog = await rpBlog.GetById(_storeId, blogId);
            if (existBlog != null)
            {
                var articlesToDelete = await rpArticle.GetListByBlogId(existBlog._id, _storeId);
                if (articlesToDelete != null && articlesToDelete.Count > 0)
                    await bizArticle.DeleteArticleAsync(articlesToDelete);

                await _priDelete(existBlog);
                await bizNavigation.Blog_UpdateForLinkUrl(existBlog._id, true);
                await bizKeyword.ProcessingKeyHandle(_storeId, (int)BHN.SharedObject.EBSMessage.EnumDocumentType.Blog, existBlog.handle, null);

                await WriteLogDashboard((int)DashboardLogAction.Blog_Delete, existBlog.BlogToToModelDetail());
                await rpMetafield.DeleteByOnwerIdResourceType(existBlog._id, (int)MetafieldResourceOwner.Blog);
                await _bus.FireStoreDataChange(ExtensionReq.OrgId);
            }
            return true;
        }

        public async Task<BlogDetailModel> GetDetail(long id)
        {
            var resultModel = new BlogDetailModel();
            if (id <= 0)
            {
                requestContext.AddError("errors.blog.not_valid", "Thông tin Blog không hợp lệ");
                return null;
            }
            var blog = await rpBlog.GetById(ExtensionReq.StoreId, id);
            if (blog == null)
            {
                requestContext.AddError("errors.blog.not_exist", "Blog không tồn tại");
                return null;
            }
            if (string.IsNullOrWhiteSpace(blog.template_suffix))
                blog.template_suffix = TemplateName.Blog;
            else
                blog.template_suffix = TemplateName.Blog + "." + blog.template_suffix;

            resultModel = blog.BlogToToModelDetail();
            // get data for navigation
            var linkListDetailModel = await bizNavigation.GetLinkListByRefId_ExceptHandle(resultModel.Id, (int)LinkFieldType.Blog);
            if (linkListDetailModel != null)
            {
                resultModel.LinkList = new List<NavigationDetailModel>();
                resultModel.LinkList = linkListDetailModel;
            }
            return resultModel;
        }

        // ECOM Call Load menu
        public async Task<BlogDetailModel> GetDetailExcepNavigation(long id)
        {
            var resultModel = new BlogDetailModel();
            if (id <= 0)
            {
                requestContext.AddError("errors.blog.not_valid", "Thông tin Blog không hợp lệ");
                return null;
            }
            var blog = await rpBlog.GetById(ExtensionReq.StoreId, id);

            if (blog == null)
            {
                requestContext.AddError("errors.blog.not_exist", "Blog không tồn tại");
                return null;
            }

            resultModel = blog.BlogToToModelDetail();
            return resultModel;
        }

        public async Task<BlogAPIModel> GetDetailApi(long id)
        {
            if (id <= 0)
            {
                requestContext.AddError("errors.blog.not_valid", "Thông tin Blog không hợp lệ");
                return null;
            }
            var _storeId = ExtensionReq.OrgId;
            var blog = await rpBlog.GetById(_storeId, id);

            if (blog == null)
            {
                requestContext.AddError("errors.blog.not_exist", "Blog không tồn tại");
                return null;
            }
            if (blog == null)
                return null;
            var result = blog.MGBlogToApiBlog();
            var feedProvider = await rpFeedBurner.GetByIdAsync(blog.feedburnerId);
            if (feedProvider != null)
            {
                result.feedburner = feedProvider.url;
            }
            return result;
        }

        public async Task<BlogAPIModel> UpdateAPI(long blogId, Dictionary<string, object> dicUpdated)
        {
            if (blogId <= 0 || dicUpdated == null || dicUpdated.Count == 0)
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var _storeId = ExtensionReq.OrgId;
            var blogModel = await rpBlog.GetById(_storeId, blogId);
            if (requestContext.IsError || blogModel == null)
            {
                return null;
            }

            #region Parse Dictionary BlogAPI

            var propertyInfos = typeof(BlogAPIModel).GetProperties();
            foreach (var propertyValuePair in dicUpdated)
            {
                var colName = propertyInfos.Single(x => x.Name == propertyValuePair.Key);
                if (colName.Name.Equals("title"))
                {
                    if (String.IsNullOrEmpty((String)propertyValuePair.Value))
                    {
                        requestContext.AddError("errors.blog.title.is_empty", "Tiêu đề blog không được bỏ trống");
                        return null;
                    }
                    else
                        blogModel.title = (String)propertyValuePair.Value;
                }
                if (colName.Name.Equals("feedburner"))
                {
                    if (!String.IsNullOrEmpty((String)propertyValuePair.Value))
                        blogModel.feedburnervalue = StringHelper.CreateFriendlyURL(propertyValuePair.Value.ToString().Trim());
                }
                if (colName.Name.Equals("template_suffix"))
                {
                    if (!String.IsNullOrEmpty((String)propertyValuePair.Value))
                        blogModel.template_suffix = propertyValuePair.Value.ToString().Trim();
                }
                if (colName.Name.Equals("commentable"))
                {
                    if (!String.IsNullOrEmpty((String)propertyValuePair.Value))
                    {
                        if (propertyValuePair.Value.ToString().ToLower().Equals("moderate"))
                        {
                            blogModel.commentrule = (int)CommentRule.Allowed_Pending_Moderation;
                        }
                        else if (propertyValuePair.Value.ToString().ToLower().Equals("no"))
                        {
                            blogModel.commentrule = (int)CommentRule.Disabled;
                        }
                        else if (propertyValuePair.Value.ToString().ToLower().Equals("yes"))
                        {
                            blogModel.commentrule = (int)CommentRule.Allowed_Automatically_Published;
                        }
                        else
                        {
                            requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                            return null;
                        }
                    }
                    else
                    {
                        requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                        return null;
                    }
                }
                if (colName.Name.Equals("handle"))
                {
                    if (!String.IsNullOrEmpty((String)propertyValuePair.Value))
                    {
                        IKeywordBusiness bizKeyword = _serviceProvider.GetService<IKeywordBusiness>();
                        blogModel.handle = await bizKeyword.ProcessingKeyHandle(_storeId,
                            (int)EnumDocumentType.Blog,
                            blogModel.handle,
                            propertyValuePair.Value.ToString());
                    }
                    else
                    {
                        requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                        return null;
                    }
                }
            }

            #endregion Parse Dictionary BlogAPI

            // await _priUpdateBlog(blogModel);
            var model = blogModel.BlogDetailToModelMG();
            await UpdateAsync(model);

            return blogModel.MGBlogToApiBlog();
        }

        #endregion public

        #region OpenApi

        private FilterSearchModel PrepareBlogFilter(int page, int limit, long since_id, string handle)
        {
            var filter = new FilterSearchModel
            {
                Fields = new List<FilterSearchField>(),
                Page = new FilterSearchPage { pageSize = limit, currentPage = page },
                ViewId = SysView.Page
            };

            if (since_id > 0)
            {
                var field = new FilterSearchField { FieldName = "Id", NummericOperator = ">", IsNummeric = true, NummericValue = since_id.ToString() };
                filter.Fields.Add(field);
            }

            if (!string.IsNullOrWhiteSpace(handle))
            {
                var field = new FilterSearchField { FieldName = "UrlHandle", OptionValueValue = handle, HasOptions = true, OptionValue = "=" };
                filter.Fields.Add(field);
            }

            return filter;
        }

        public async Task<List<BlogAPIModel>> OpenApi_GetBlogList(int page, int limit, long since_id, string handle)
        {
            var filter = PrepareBlogFilter(page, limit, since_id, handle);

            var (data, totalrecord) = await getMGBlogList(filter);
            if (requestContext.IsError)
            {
                return null;
            }

            return data?.MGBlogListToApiBlogList();
        }

        public async Task<long> OpenApi_CountBlogs(int page, int limit, long since_id, string handle)
        {
            var filter = PrepareBlogFilter(page, limit, since_id, handle);
            var totalRecord = await CountBlogs(filter);
            if (requestContext.IsError)
            {
                return 0;
            }
            return totalRecord;
        }

        public async Task<BlogAPIModel> OpenApi_GetBlogDetail(long blog_id)
        {
            var resultModel = new BlogAPIModel();
            if (blog_id <= 0)
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }

            var blog = await rpBlog.GetById(ExtensionReq.StoreId, blog_id);
            if (blog == null)
            {
                requestContext.AddError("errors.blog.not_exist", "Blog không tồn tại");
                return null;
            }

            if (string.IsNullOrWhiteSpace(blog.template_suffix))
                blog.template_suffix = TemplateName.Blog;
            else
                blog.template_suffix = TemplateName.Blog + "." + blog.template_suffix;

            resultModel = blog.MGBlogToApiBlog();

            return resultModel;
        }

        private async Task<(MGBlogModel, List<MetafieldModel>)> ParseDictionarBlogApi(Dictionary<string, object> dictionaryModel, long blog_id = 0)
        {
            if (dictionaryModel == null || dictionaryModel.Count == 0)
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return (null, null);
            }

            MGBlogModel blogModel;
            List<MetafieldModel> metafields = new List<MetafieldModel>();

            if (blog_id == 0)
            {
                blogModel = new MGBlogModel();
            }
            else
            {
                var _storeId = ExtensionReq.OrgId;
                blogModel = await rpBlog.GetById(_storeId, blog_id);
                if (requestContext.IsError || blogModel == null)
                {
                    requestContext.AddError("errors.data.not_exist", "Dữ liệu không tồn tại");
                    return (null, null);
                }
            }

            var propertyInfos = typeof(BlogAPIModel).GetProperties();
            foreach (var propertyValuePair in dictionaryModel)
            {
                var colName = propertyInfos.Single(x => x.Name == propertyValuePair.Key);
                if (colName.Name.Equals("title"))
                {
                    if (String.IsNullOrEmpty((String)propertyValuePair.Value))
                    {
                        requestContext.AddError("errors.blog.title.is_empty", "Tiêu đề blog không được bỏ trống");
                        return (null, null);
                    }
                    else
                        blogModel.title = (String)propertyValuePair.Value;
                }
                if (colName.Name.Equals("feedburner"))
                {
                    if (!String.IsNullOrEmpty((String)propertyValuePair.Value))
                        blogModel.feedburnervalue = StringHelper.CreateFriendlyURL(propertyValuePair.Value.ToString().Trim());
                }
                if (colName.Name.Equals("template_suffix"))
                {
                    if (!String.IsNullOrEmpty((String)propertyValuePair.Value))
                        blogModel.template_suffix = propertyValuePair.Value.ToString().Trim();
                }
                if (colName.Name.Equals("commentable"))
                {
                    if (!String.IsNullOrEmpty((String)propertyValuePair.Value))
                    {
                        if (propertyValuePair.Value.ToString().ToLower().Equals("moderate"))
                        {
                            blogModel.commentrule = (int)CommentRule.Allowed_Pending_Moderation;
                        }
                        else if (propertyValuePair.Value.ToString().ToLower().Equals("no"))
                        {
                            blogModel.commentrule = (int)CommentRule.Disabled;
                        }
                        else if (propertyValuePair.Value.ToString().ToLower().Equals("yes"))
                        {
                            blogModel.commentrule = (int)CommentRule.Allowed_Automatically_Published;
                        }
                        else
                        {
                            requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                            return (null, null);
                        }
                    }
                    else
                    {
                        requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                        return (null, null);
                    }
                }
                if (colName.Name.Equals("handle"))
                {
                    if (!String.IsNullOrEmpty((String)propertyValuePair.Value))
                    {
                        var _storeId = ExtensionReq.OrgId;
                        IKeywordBusiness bizKeyword = _serviceProvider.GetService<IKeywordBusiness>();
                        blogModel.handle = await bizKeyword.ProcessingKeyHandle(_storeId,
                            (int)EnumDocumentType.Blog,
                            blogModel.handle,
                            propertyValuePair.Value.ToString());
                    }
                    else
                    {
                        requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                        return (null, null);
                    }
                }
                if (colName.Name.Equals("metafields"))
                {
                    var list = propertyValuePair.Value;
                    if (list != null)
                    {
                        var jsonObject = JsonConvert.SerializeObject(list);
                        metafields = JsonConvert.DeserializeObject<List<MetafieldModel>>(jsonObject);
                    }
                }
            }

            return (blogModel, metafields?.ToList());
        }

        public async Task<BlogAPIModel> OpenApi_UpdateBlog(long blog_id, Dictionary<string, object> dicUpdated)
        {
            if (blog_id <= 0)
            {
                requestContext.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return null;
            }
            var (blogModel, metafields) = await ParseDictionarBlogApi(dicUpdated, blog_id);

            if (requestContext.IsError)
                return null;

            var model = blogModel.BlogDetailToModelMG();
            model.metafields = metafields;
            var updpage = await UpdateAsync(model);

            return await OpenApi_GetBlogDetail(updpage.Id);
        }

        public async Task<BlogAPIModel> OpenApi_AddNewBlog(Dictionary<string, object> dictionary)
        {
            var (blogModel, metafields) = await ParseDictionarBlogApi(dictionary);

            if (requestContext.IsError)
                return null;

            var model = blogModel.BlogDetailToModelMG();
            model.metafields = metafields;
            var blog = await AddNewAsync(model);
            var _storeId = ExtensionReq.OrgId;
            var mgblog = await rpBlog.GetById(_storeId, blog.Id);
            return mgblog.MGBlogToApiBlog();
        }

        public async Task<bool> OpenApi_DeleteBlog(long blog_id)
        {
            return await DeleteBlogidAsync(blog_id);
        }

        #endregion OpenApi

        #region private

        private async Task<long> _priUpdateBlog(MGBlogModel newblog)
        {
            var _storeId = ExtensionReq.OrgId;
            newblog.storeid = _storeId;
            newblog.updated_at = DateTime.UtcNow;
            newblog.updated_user = _userId;
            newblog.published_at = DateTime.UtcNow;
            newblog.isdeleted = false;
            if (newblog.commentrule < 1)
            {
                newblog.commentrule = 1;
            }
            if (newblog.commentrule > 1)
            {
                newblog.comments_enabled = true;
            }
            else
            {
                newblog.comments_enabled = false;
            }
            var mgblog = await rpBlog.SetAsync(newblog);
            return mgblog._id;
        }

        private async Task<long> _priSetBlog(MGBlogModel newblog)
        {
            var _storeId = ExtensionReq.OrgId;
            newblog.storeid = _storeId;
            newblog.created_at = DateTime.UtcNow;
            newblog.published_at = DateTime.UtcNow;
            newblog.created_user = _userId;
            newblog.updated_at = DateTime.UtcNow;
            newblog.updated_user = _userId;
            newblog.isdeleted = false;
            if (newblog.commentrule < 1)
            {
                newblog.commentrule = 1;
            }
            if (newblog.commentrule > 1)
            {
                newblog.comments_enabled = true;
            }
            else
            {
                newblog.comments_enabled = false;
            }
            var mgblog = await rpBlog.SetAsync(newblog);
            return mgblog._id;
        }

        private MGBlogModel ValidateBlogInfo(BlogDetailModel model)
        {
            if (model == null)
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            if (string.IsNullOrWhiteSpace(model.Title))
            {
                requestContext.AddError("errors.blog.title.is_empty", "Tiêu đề blog không được bỏ trống");
                return null;
            }
            model.Title = model.Title.Trim();
            if (model.Title.Length > DefaultData.DefaultMaxBlogTitleLength)
            {
                requestContext.AddError($"errors.blog.title.max_limit_reached__{DefaultData.DefaultMaxBlogTitleLength}__ ",$"Tiêu đề blog không được vượt quá {DefaultData.DefaultMaxBlogTitleLength} ký tự");
                return null;
            }
            if (!StringHelper.CheckStringIsNullOrEmptyOrWhitespace(model.Descriptions))
            {
                model.Descriptions = HtmlUtility.Instance.SanitizeHtml(model.Descriptions, HtmlUtility.ValidHtmlTags.Article);
                if (model.Descriptions.Trim().Length > DefaultData.DefaultMaxBlogDescriptionLength)
                {
                    requestContext.AddError($"errors.article.content.max_limit_reached__{DefaultData.DefaultMaxBlogDescriptionLength}__",$"Nội dung không được vượt quá {DefaultData.DefaultMaxBlogDescriptionLength} ký tự");
                    return null;
                }
            }
            if (string.IsNullOrEmpty(model.PageTitle))
            {
                model.PageTitle = model.Title.Length > 70 ? model.Title.Substring(0, 70)
                                                    : model.Title;
            }
            var strMetaDescription = HtmlStringHelpers.StripHtml(model.MetaDescription);
            if (!string.IsNullOrEmpty(strMetaDescription))
            {
                if (strMetaDescription.Length > DefaultData.DefaultMaxMetaDescription)
                {
                    requestContext.AddError($"errors.blog.content.max_limit_reached__{DefaultData.DefaultMaxMetaDescription}__ ", $"Mô tả Blog không được quá {DefaultData.DefaultMaxMetaDescription} ký tự");
                    return null;
                }
            }
            else
            {
                if (!StringHelper.CheckStringIsNullOrEmptyOrWhitespace(HtmlStringHelpers.StripHtml(model.Descriptions)))
                {
                    strMetaDescription = HtmlStringHelpers.StripHtml(model.Descriptions);
                    if (strMetaDescription.Length > DefaultData.DefaultMaxMetaDescription)
                    {
                        model.MetaDescription = strMetaDescription.Substring(0, DefaultData.DefaultMaxMetaDescription);
                    }
                    else
                    {
                        model.MetaDescription = strMetaDescription;
                    }
                }
                else
                {
                    model.MetaDescription = strMetaDescription;
                }
            }
            if (string.IsNullOrEmpty(model.UrlHandle))
            {
                model.UrlHandle = model.Title;
            }
            if (model.TemplateName == TemplateName.Blog || string.IsNullOrWhiteSpace(model.TemplateName))
            {
                model.TemplateName = string.Empty;
            }
            else
            {
                model.TemplateName = model.TemplateName.Replace(TemplateName.Blog + ".", string.Empty);
            }
            if (!string.IsNullOrEmpty(model.FeedBurnerValue))
            {
                model.FeedBurnerValue = StringHelper.CreateFriendlyURL(model.FeedBurnerValue.Trim());
            }
            if (model.PageTitle.Length > 70 || model.UrlHandle.Length > 300)
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            if (model.metafields != null && model.metafields.Any())
            {
                foreach (var objMetafield in model.metafields)
                {
                    VerifyMetafield(objMetafield);
                }
            }

            if (requestContext.IsError)
            {
                return null;
            }
            return model.BlogToModelMG();
        }

        private FilterSearchModel buildFilterSearchModel(
                                                            bool visible,
                                                            bool hidden,
                                                            string query,
                                                            string order,
                                                            string direction,
                                                            int page,
                                                            int limit,
                                                            string collection_type)
        {
            var filter = new FilterSearchModel()
            {
                FreeText = query,
                Page = new FilterSearchPage()
                {
                    currentPage = page <= 0 ? 1 : page,
                    pageSize = limit <= 0 || limit > 50 ? 20 : limit
                },
                SortFieldName = PrepareSortFieldName(order),
                SortType = PrepareSortType(direction),
                Fields = new List<FilterSearchField>()
            };
            if (visible)
            {
                filter = PreparePublishStatusFilter(filter, true);
            }
            if (hidden)
            {
                PreparePublishStatusFilter(filter, false);
            }
            if (!StringHelper.CheckStringIsNullOrEmptyOrWhitespace(collection_type))
            {
                filter = PrepareArticleTypeFilter(filter, collection_type);
            }

            return filter;
        }

        private string PrepareSortFieldName(string order)
        {
            if (!StringHelper.CheckStringIsNullOrEmptyOrWhitespace(order))
            {
                order = order.Trim();
                switch (order)
                {
                    case "title":
                        return "title";

                    default:
                        return null;
                }
            }
            return null;
        }

        private string PrepareSortType(string direction)
        {
            if (!StringHelper.CheckStringIsNullOrEmptyOrWhitespace(direction))
            {
                direction = direction.Trim();
                switch (direction)
                {
                    case "asc":
                        return "asc";

                    case "desc":
                        return "desc";

                    default:
                        return null;
                }
            }
            return null;
        }

        private FilterSearchModel PreparePublishStatusFilter(FilterSearchModel model, bool visible)
        {
            switch (visible)
            {
                case true:
                    model.Fields.Add(new FilterSearchField()
                    {
                        FieldName = "PublishedDate",
                        HasOptions = true,
                        IsNummeric = false,
                        OptionValue = "AfterNow"
                    });
                    break;

                case false:
                    model.Fields.Add(new FilterSearchField()
                    {
                        FieldName = "PublishedDate",
                        HasOptions = true,
                        IsNummeric = false,
                        OptionValue = "BeforeEqualsNow"
                    });
                    break;

                default:
                    break;
            }
            return model;
        }

        private FilterSearchModel PrepareArticleTypeFilter(FilterSearchModel model, string collection_type)
        {
            var arrArticleTypes = collection_type.Split(',');

            foreach (var articleType in arrArticleTypes)
            {
                if (!StringHelper.CheckStringIsNullOrEmptyOrWhitespace(articleType))
                {
                    model.Fields.Add(new FilterSearchField()
                    {
                        FieldName = "BlogId",
                        HasOptions = true,
                        IsNummeric = false,
                        OptionValue = articleType.Trim()
                    });
                }
            }

            return model;
        }

        private bool IsGetDataFromMongo(FilterSearchModel model)
        {
            return string.IsNullOrWhiteSpace(model.FreeText)
                   && (model.Fields == null || !model.Fields.Any())
                   && string.IsNullOrWhiteSpace(model.SortFieldName)
                   && string.IsNullOrWhiteSpace(model.SortType);
        }

        private async Task WriteLogDashboard(int logAction, BlogDetailModel blogModel)
        {
            var model = new ArticleLogModel
            {
                Title = blogModel.Title,
                LogDate = DateTime.UtcNow,
                LogUser = ExtensionReq.UserName,
                LinkDetail = "blog#/detail/" + blogModel.Id
            };
            var logdata = JsonConvert.SerializeObject(model);
            var msg = new LogDataMsg
            {
                LogData = logdata,
                CreatedUser = ExtensionReq.UserId,
                CreatedUserEmail = ExtensionReq.UserEmail,
                ActionId = logAction,
                TypeId = (int)BHN.SharedObject.EBSMessage.LogType.Dashboard,
                RefId = blogModel.Id,
                StoreId = ExtensionReq.StoreId,
                CreatedDate = DateTime.UtcNow,
                Action = MessageAction.Insert,
                IsCommentLog = false,
                CreatedUserName = ExtensionReq.UserName
            };

            await _bus.SendMsgAsync(msg);
        }

        private async Task _priDelete(MGBlogModel blog)
        {
            await rpBlog.DeleteByIdAsync(ExtensionReq.OrgId, blog._id);
            await rpESBlog.RemoveAsync(ExtensionReq.OrgId, blog._id);
            var logmodel = new MGLogData()
            {
                created_date = DateTime.UtcNow,
                created_user = ExtensionReq.UserId,
                data = JsonConvert.SerializeObject(blog.BlogToToModelDetail()),
                refid = blog._id,
                storeid = blog.storeid,
                doctypeid = (int)EnumDocumentType.Blog
            };
            await _mgLog.PostLog(logmodel);
        }

        #endregion private

        #region Validate Metafield

        public void VerifyMetafield(MetafieldModel inputMetafield)
        {
            if (string.IsNullOrWhiteSpace(inputMetafield.key))
                requestContext.AddError("Key can't be blank");
            if (inputMetafield.key.Trim().Length < 3)
                requestContext.AddError("Key is too short (minimum is 3 characters)");
            if (inputMetafield.key.Trim().Length > 20)
                requestContext.AddError("Key is too long (maximum is 20 characters)");
            if (string.IsNullOrWhiteSpace(inputMetafield.@namespace))
                requestContext.AddError("Namespace can't be blank");
            if (inputMetafield.@namespace.Trim().Length < 3)
                requestContext.AddError("Namespace is too short (minimum is 3 characters)");
            if (inputMetafield.@namespace.Trim().Length > 20)
                requestContext.AddError("Namespace is too long (maximum is 20 characters)");
            if (string.IsNullOrWhiteSpace(inputMetafield.value))
                requestContext.AddError("Value can't be blank");
            if (inputMetafield.value.Trim().Length > 80000)
                requestContext.AddError("Value is too long (maximum is 80000 characters)");
            if (string.IsNullOrWhiteSpace(inputMetafield.value_type))
                requestContext.AddError("value_type can't be blank");
            if (!string.IsNullOrWhiteSpace(inputMetafield.description) && inputMetafield.description.Trim().Length > 100)
                requestContext.AddError("Description is too long (maximum is 100 characters)");
            string strType = inputMetafield.value_type.Trim().ToLower();
            if (strType.Trim().ToLower().Equals("integer"))
            {
                if (!double.TryParse(inputMetafield.value, out double d))
                {
                    requestContext.AddError("Invalid value number");
                }
            }
        }

        public async Task<string> GetHandleById(long id)
        {
            return await rpBlog.GetHandleByIdAsync(ExtensionReq.StoreId, id);
        }

        #endregion Validate Metafield
    }
}