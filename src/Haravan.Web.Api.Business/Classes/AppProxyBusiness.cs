﻿using BHN.SharedObject.APIDataModel;
using Haravan.Libs.Abstractions;
using Haravan.Web.Api.BusinessObjects.Mappers;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using Haravan.Web.Api.Repository;
using System;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public class AppProxyBusiness : IAppProxyBusiness
    {
        private readonly IExtensionPerRequest extensionReq;
        private readonly IRequestContext context;
        private readonly IMGAppProxyRepository rpAppProxy;
        private readonly IEBSComMessageRepository bus;

        public AppProxyBusiness(
            IMGAppProxyRepository rpAppProxy,
            IRequestContext context,
            IEBSComMessageRepository bus,
            IExtensionPerRequest extensionReq
            )
        {
            this.rpAppProxy = rpAppProxy;
            this.context = context;
            this.extensionReq = extensionReq;
            this.bus = bus;
        }

        public async Task<AppProxyAPIModel> GetByAppId(long appId)
        {
            var _storeId = extensionReq.StoreId;
            var objAppProxy = await rpAppProxy.GetByAppId(_storeId, appId);
            if (objAppProxy != null)
            {
                return objAppProxy.ToApiModel();
            }
            return null;
        }

        public async Task<AppProxyAPIModel> Update(long appId, AppProxyAPIModel model)
        {
            if (model == null)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }

            var _storeId = extensionReq.StoreId;

            var objAppProxy = await rpAppProxy.GetByAppId(_storeId, appId);

            if (objAppProxy == null)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }

            if (string.IsNullOrWhiteSpace(model.sub_path_prefix))
            {
                context.AddError("errors.subpathprefix.is_empty", "SubPathPrefix không được bỏ trống");
                return null;
            }

            objAppProxy.SubPathPrefixProxy = model.sub_path_prefix.Trim();
            if (string.IsNullOrWhiteSpace(model.sub_path))
            {
                context.AddError("errors.subpath.is_empty", "SubPath không được bỏ trống");
                return null;
            }

            objAppProxy.SubPathProxy = model.sub_path.Trim();
            if (string.IsNullOrWhiteSpace(model.proxy_url))
            {
                context.AddError("errors.proxyurl.is_empty", "ProxyUrl không được bỏ trống");
                return null;
            }

            model.proxy_url = model.proxy_url.Trim();
            var isValidProxyUrl = IsValidUrl(model.proxy_url);
            if (!isValidProxyUrl)
            {
                context.AddError("errors.proxyurl.not_valid", "ProxyUrl không hợp lệ");
                return null;
            }
            objAppProxy.ProxyURL = model.proxy_url;

            if (string.IsNullOrWhiteSpace(model.api_key))
            {
                context.AddError("errors.apikey.is_empty", "ApiKey không được bỏ trống");
                return null;
            }

            objAppProxy.APIKey = model.api_key.Trim();
            await rpAppProxy.UpdateAsync(objAppProxy);
            await bus.FireStoreDataChange(_storeId);
            return await GetByAppId(appId);
        }

        public async Task<bool> DeleteFromWorker(long appId)
        {
            var _storeId = extensionReq.StoreId;
            var objAppProxy = await rpAppProxy.GetByAppId(_storeId, appId);
            if (objAppProxy != null)
            {
                await rpAppProxy.DeleteAsync(objAppProxy);
                await bus.FireStoreDataChange(_storeId);
            }
            return true;
        }

        public async Task<bool> Delete(long appId)
        {
            var _storeId = extensionReq.StoreId;
            var objAppProxy = await rpAppProxy.GetByAppId(_storeId, appId);
            if (objAppProxy == null)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return false;
            }
            await rpAppProxy.DeleteAsync(objAppProxy);
            await bus.FireStoreDataChange(_storeId);
            return true;
        }

        public async Task<AppProxyAPIModel> Add(long appId, AppProxyAPIModel model)
        {
            if (model == null)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var _storeId = extensionReq.StoreId;
            var objAppProxy = await rpAppProxy.GetByAppId(_storeId, appId);

            if (objAppProxy != null)
            {
                context.AddError("errors.application.has_app_proxy", "Ứng dụng đã tạo App Proxy");
                return null;
            }
            if (string.IsNullOrWhiteSpace(model.sub_path_prefix))
            {
                context.AddError("errors.subpathprefix.is_empty", "SubPathPrefix không được bỏ trống");
                return null;
            }
            model.sub_path_prefix = model.sub_path_prefix.Trim();
            if (string.IsNullOrWhiteSpace(model.sub_path))
            {
                context.AddError("errors.subpath.is_empty", "SubPath không được bỏ trống");
                return null;
            }
            model.sub_path = model.sub_path.Trim();
            if (string.IsNullOrWhiteSpace(model.proxy_url))
            {
                context.AddError("errors.proxyurl.is_empty", "ProxyUrl không được bỏ trống");
                return null;
            }
            model.proxy_url = model.proxy_url.Trim();
            var isValidProxyUrl = IsValidUrl(model.proxy_url);
            if (!isValidProxyUrl)
            {
                context.AddError("errors.proxyurl.not_valid", "ProxyUrl không hợp lệ");
                return null;
            }
            if (string.IsNullOrWhiteSpace(model.api_key))
            {
                context.AddError("errors.apikey.is_empty", "ApiKey không được bỏ trống");
                return null;
            }
            model.api_key = model.api_key.Trim();
            objAppProxy = new MGAppProxy()
            {
                AppId = appId,
                ProxyURL = model.proxy_url,
                SubPathProxy = model.sub_path,
                SubPathPrefixProxy = model.sub_path_prefix,
                StoreId = _storeId,
                APIKey = model.api_key
            };

            await rpAppProxy.AddAsync(objAppProxy);
            await bus.FireStoreDataChange(_storeId);
            return await GetByAppId(appId);
        }

        private bool IsValidUrl(string url)
        {
            var isCreatedUriSuccess =
                Uri.TryCreate(url, UriKind.Absolute, out Uri uriResult)
                && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);

            var isWellFormedUri =
                isCreatedUriSuccess
                && Uri.IsWellFormedUriString(uriResult.ToString(), UriKind.Absolute);

            return isWellFormedUri && isCreatedUriSuccess;
        }
    }
}