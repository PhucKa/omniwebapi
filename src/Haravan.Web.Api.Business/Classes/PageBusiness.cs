﻿using BHN.Core;
using BHN.SharedObject.APIDataModel;
using BHN.SharedObject.EBSMessage;
using BHN.SharedObject.EBSMessage.LogSeller;
using Haravan.Libs.Abstractions;
using Haravan.Web.Api.Business.Extensions;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Enums;
using Haravan.Web.Api.BusinessObjects.ESModels;
using Haravan.Web.Api.BusinessObjects.Mappers;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.BusinessObjects.Models.Common;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using Haravan.Web.Api.Repository;
using Haravan.Web.Api.Utils.Extensions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public class PageBusiness : IPageBusiness
    {
        private readonly IRequestContext context;
        private readonly IMGPageRepository rpPage;
        
        private readonly IKeywordBusiness bizKeyword;
        private readonly IUserRepository rpUser;
        private readonly INavigationBusiness bizNavigation;
        private readonly IThemeRepository rpTheme;
        private readonly IEBSComMessageRepository _bus;
        private readonly IMGLogDataRepository _mgLog;
        private readonly IMetafieldsEcomRepository rpMetafield;
        private readonly IExtensionPerRequest ExtensionReq;

        private readonly IESPageRepository rpESPage;
        private readonly IESPageSinkRepository rpESPageSearch;

        public PageBusiness(
            IRequestContext requestContext,
            IEBSComMessageRepository _bus,
            IMGPageRepository rpPage,
            IESPageRepository rpESPage,
            IKeywordBusiness bizKeyword,
            INavigationBusiness bizNavigation,
            IThemeRepository rpTheme,
            IUserRepository rpUser,
            IMGLogDataRepository _mgLog,
            IMetafieldsEcomRepository rpMetafield,
            IExtensionPerRequest ExtensionReq,
            IESPageSinkRepository rpESPageSearch)
        {
            this.context = requestContext;
            this.ExtensionReq = ExtensionReq;
            this._bus = _bus;
            this.rpPage = rpPage;
            this.bizNavigation = bizNavigation;

            this.rpESPage = rpESPage;

            this.bizKeyword = bizKeyword;
            this.rpTheme = rpTheme;

            this.rpUser = rpUser;
            this._mgLog = _mgLog;
            this.rpMetafield = rpMetafield;
            this.rpESPageSearch = rpESPageSearch;
        }

        #region public

        public async Task<long> GetIdByHandleUrl(string handleUrl)
        {
            return await rpPage.GetIdByUrlHandle(handleUrl, ExtensionReq.StoreId);
        }

        public async Task<List<PageDetailModel>> GetByListUrlHandle(List<string> listUrlHandle)
        {
            var model = new List<PageDetailModel>();
            if (listUrlHandle == null)
            {
                context.AddError("errors.page.handle_url.not_valid", "Thông tin handleUrl không hợp lệ");
                return null;
            }
            var list = await rpPage.GetByListUrlHandle(ExtensionReq.StoreId, listUrlHandle);
            if (list != null)
            {
                model = list.ListPageDetailToListPageModel();
            }
            return model;
        }

        public async Task<bool> Init_NewStore()
        {
            var pagesStore0 = await rpPage.GetByStoreId(0);

            foreach (var page in pagesStore0)
            {
                page._id = 0;
                page.created_at = DateTime.UtcNow.AddHours(-1);
                page.published_at = DateTime.UtcNow.AddHours(-1);
                page.authorname = ExtensionReq.UserName;
                var pageDetailModel = page.MGPageToPageDetailModel();
                await AddNewAsync(pageDetailModel);
            }
            return true;
        }

        public async Task<List<DropdownSimpleModel>> GetPageTemplate()
        {
            var list = await rpTheme.GetPageTemplate();
            List<DropdownSimpleModel> themes = new List<DropdownSimpleModel>();
            if (list != null)
            {
                var it = list.Select((s, i) => new DropdownSimpleModel { Id = i, Name = s }).ToList();
                themes.AddRange(it);
            }
            return themes;
        }

        public async Task<List<string>> GetPageTemplates()
        {
            var list = await rpTheme.GetPageTemplate();
            return list;
        }

        public async Task<PageDetailModel> GetByUrlHandle(string urlHandle)
        {
            var modelData = new PageDetailModel();
            if (string.IsNullOrWhiteSpace(urlHandle))
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var pagemodel = await rpPage.GetByHandleUrl(urlHandle, ExtensionReq.StoreId);
            if (pagemodel != null)
                modelData = pagemodel.MGPageToPageDetailModel();

            return modelData;
        }

        public async Task<List<PageDetailModel>> GetListByListHandleUrl(List<string> urlHandles)
        {
            var modelData = new List<PageDetailModel>();
            if (urlHandles == null || !urlHandles.Any())
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var pagesmodel = await rpPage.GetByListUrlHandle(ExtensionReq.OrgId, urlHandles);
            if (pagesmodel != null)
                modelData = pagesmodel.MGPageListToPageDetailModelList();
            return modelData;
        }

        public async Task<bool> PublishPages(List<long> pagesIds, bool isPublish)
        {
            if (pagesIds == null || !pagesIds.Any())
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return false;
            }
            var pagesToPublish = await RefinePagesOfStore(pagesIds);

            foreach (var page in pagesToPublish)
            {
                page.published_at = isPublish ? DateTime.UtcNow : (DateTime?)null;
                page.updated_at = DateTime.UtcNow;
                page.updated_user = ExtensionReq.UserId;
                page.IsVisible = isPublish;
                var mgpage = await rpPage.SetAsync(page);
                if (page.published_at != null && page.published_at > DateTime.UtcNow)
                    await _bus.FireStoreDataChangeAtTime(ExtensionReq.OrgId, page.published_at.Value);
                else
                    await _bus.FireStoreDataChange(ExtensionReq.OrgId);
            }
            return true;
        }

        public async Task<(List<ThemeDropdownModel> data, long totalrecord)> GetPageDropdownList(
                                        string query,
                                        int page,
                                        int limit)
        {
            var filter = buildFilterSearchModel(
                                                    false,
                                                    false,
                                                    query,
                                                    null,
                                                    null,
                                                    page,
                                                    limit
                                                );
            var (data, totalrecord) = await GetMGPageList(filter);
            if (data != null && data.Any())
            {
                var result = data.ToDropdownList();
                return (result, totalrecord);
            }
            return (null, 0);
        }

        public async Task<(List<PageListModel> data, long totalrecord)> GetGlobalSearchListPage(FilterSearchModel filter)
        {
            var data = new List<PageListModel>();
            var isDefault = false;
            if (filter == null)
            {
                context.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return (data, 0);
            }
            if (
                   string.IsNullOrWhiteSpace(filter.FreeText)
                   && (filter.Fields == null || !filter.Fields.Any())
                   && string.IsNullOrWhiteSpace(filter.SortFieldName)
                   && string.IsNullOrWhiteSpace(filter.SortType)
              )
                isDefault = true;

            long totalRecord = 0;
            var ids = new List<long>();
            var dicIds = new Dictionary<int, long>();

            if (isDefault)
            {
                totalRecord = await rpPage.CountAsync(ExtensionReq.OrgId);
                ids = await rpPage.GetByPagingAsync(ExtensionReq.OrgId, filter.Page.currentPage, filter.Page.pageSize);
                if (ids != null && ids.Any())
                {
                    for (int i = 0; i < ids.Count; i++)
                    {
                        dicIds.Add(i + 1, ids[i]);
                    }
                }
            }
            else
            {
                if (filter.Fields == null && filter.SortFieldName == null && filter.SortType == null)
                {
                    filter.SortFieldName = "UpdatedDate";
                    filter.SortType = "desc";
                }
                else if (filter.SortFieldName != null && filter.SortType == null)
                {
                    filter.SortType = "desc";
                }
                else if (filter.SortFieldName == null && filter.SortType != null)
                {
                    filter.SortFieldName = "UpdatedDate";
                }
                var rs = new ESSearchResult();
                rs = await rpESPageSearch.SearchAsync(ExtensionReq.OrgId, filter);
                totalRecord = rs.Total;
                dicIds = rs.Ids;
            }
            if (dicIds != null && dicIds.Any())
            {
                var result = await _priGetByIds(ExtensionReq.OrgId, dicIds);
                data = result.ToModelList().ToList();
                data.ForEach(r =>
                {
                    if (r.published_at != null && r.published_at.Value <= DateTime.UtcNow)
                        r.IsVisible = true;
                    else
                        r.IsVisible = false;
                });
            }
            return (data, totalRecord);
        }

        public async Task<(List<PageListModel> data, long totalrecord)> GetPageList(
                                                                            bool visible,
                                                                            bool hidden,
                                                                            string query,
                                                                            string order,
                                                                            string direction,
                                                                            int page,
                                                                            int limit
                                                                      )

        {
            var filter = buildFilterSearchModel(
                                                    visible,
                                                    hidden,
                                                    query,
                                                    order,
                                                    direction,
                                                    page,
                                                    limit
                                                );
            var data = new List<PageListModel>();
            var (pageIds, totalRecord) = await GetPagingPageIds(filter);
            if (context.IsError)
            {
                return (null, 0);
            }
            if (pageIds != null && pageIds.Any())
            {
                var result = await _priGetByIds(ExtensionReq.OrgId, pageIds);
                data = result.ToModelList().ToList();
                data.ForEach(r =>
                {
                    if (r.published_at != null && r.published_at.Value <= DateTime.UtcNow)
                        r.IsVisible = true;
                    else
                        r.IsVisible = false;
                });
            }
            return (data, totalRecord);
        }

        public async Task<(List<PageDetailShortModel> data, long totalrecord)> GetPageList(FilterSearchModel filter)
        {
            var data = new List<PageDetailShortModel>();
            var result = await GetMGPageList(filter);
            data = result.data.MGPageToPageDetailShortList();
            return (data, result.totalrecord);
        }

        public async Task<PageDetailModel> GetDetail(long id)
        {
            var objPage = await rpPage.GetById(ExtensionReq.StoreId, id);
            if (objPage == null)
            {
                context.AddError("errors.page.not_exist", "Trang không tồn tại");
                return null;
            }
            if (string.IsNullOrWhiteSpace(objPage.template_suffix))
                objPage.template_suffix = TemplateName.Page;
            else
                objPage.template_suffix = TemplateName.Page + "." + objPage.template_suffix;
            var resultModel = objPage.MGPageToPageDetailModel();
            // get data for navigation not yet done
            var linkListDetailModel = await bizNavigation.GetLinkListByRefId_ExceptHandle(resultModel.Id, (int)LinkFieldType.Page);
            if (linkListDetailModel != null)
            {
                resultModel.LinkList = new List<NavigationDetailModel>();
                resultModel.LinkList = linkListDetailModel;
            }

            if (!StringHelper.CheckStringIsNullOrEmptyOrWhitespace(resultModel.Content))
            {
                var strContent = resultModel.Content.Trim();
                if (!strContent.StartsWith("<p>")) strContent = "<p>" + strContent + "</p>";
                resultModel.Content = strContent;
            }
            if (resultModel.PublishedDate != null && resultModel.PublishedDate.Value <= DateTime.UtcNow)
                resultModel.IsVisible = true;
            else
                resultModel.IsVisible = false;

            return resultModel;
        }

        public async Task<PageDetailModel> GetDetailExcepNavigation(long id)
        {
            var objPage = await rpPage.GetById(ExtensionReq.OrgId, id);

            if (objPage == null)
            {
                context.AddError("errors.page.not_exist", "Trang không tồn tại");
                return null;
            }

            var resultModel = objPage.MGPageToPageDetailModel();

            if (!StringHelper.CheckStringIsNullOrEmptyOrWhitespace(resultModel.Content))
            {
                var strContent = resultModel.Content.Trim();
                if (!strContent.StartsWith("<p>")) strContent = "<p>" + strContent + "</p>";
                resultModel.Content = strContent;
            }
            if (resultModel.PublishedDate != null && resultModel.PublishedDate.Value <= DateTime.UtcNow)
                resultModel.IsVisible = true;
            else
                resultModel.IsVisible = false;

            return resultModel;
        }

        public async Task<PageDetailModel> AddNewAsync(PageDetailModel model, List<NavigationDetailModel> linkList = null)
        {
            var validate = await ValidateModel(model, false);
            var newPage = validate.page;
            if (newPage == null)
                return null;
            newPage.handle = await bizKeyword.ProcessingKeyHandle(ExtensionReq.OrgId, (int)EnumDocumentType.Page, validate.Item1, model.UrlHandle);
            newPage.url = "/pages/" + newPage.handle;
            var data = await _priSetPage(newPage);
            if (linkList != null)
            {
                await bizNavigation.SetLinkByRefId(newPage._id, newPage.title, linkList, LinkFieldType.Page);
            }

            await WriteLogDashboard(validate.logaction, data);
            await _bus.FireStoreDataChange(ExtensionReq.StoreId);
            return data;
        }

        public async Task<string> GetHandleById(long id)
        {
            return await rpPage.GetHandleByIdAsync(ExtensionReq.StoreId, id);
        }

        public async Task<PageDetailModel> UpdateAsync(PageDetailModel model, List<NavigationDetailModel> linkList = null)
        {
            var validate = await ValidateModel(model, true);
            var newPage = validate.page;
            if (newPage == null)
                return null;
            newPage.handle = await bizKeyword.ProcessingKeyHandle(ExtensionReq.StoreId, (int)EnumDocumentType.Page, validate.Item1, model.UrlHandle);
            newPage.url = "/pages/" + newPage.handle;
            var data = await _priUpdatePage(newPage);
            if (string.IsNullOrWhiteSpace(data.TemplateName))
                data.TemplateName = TemplateName.Page;
            else
                data.TemplateName = TemplateName.Page + "." + data.TemplateName;
            if (!validate.Item1.Equals(newPage.handle))
                await bizNavigation.Page_UpdateForLinkUrl(newPage._id);

            if (linkList != null)
            {
                await bizNavigation.SetLinkByRefId(newPage._id, newPage.title, linkList, LinkFieldType.Page);
            }
            var esPage = await rpESPage.GetAsync(newPage.storeid, newPage._id);
            if (esPage != null)
            {
                esPage.PublishedDate = newPage.published_at;
                await rpESPage.UpdateAsync(esPage);
            }
            await WriteLogDashboard(validate.Item2, data);

            if (data.PublishedDate != null && data.PublishedDate > DateTime.UtcNow)
                await _bus.FireStoreDataChangeAtTime(ExtensionReq.StoreId, data.PublishedDate.Value);
            else
                await _bus.FireStoreDataChange(ExtensionReq.StoreId);
            return data;
        }

        public async Task<bool> DeletePagesAsync(List<long> pagesIds)
        {
            if (pagesIds == null || !pagesIds.Any())
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return false;
            }
            foreach (var id in pagesIds)
            {
                await DeletePageByIdAsync(id, false);
            }
            return true;
        }

        public async Task<bool> DeletePageByIdAsync(long pageId, bool isAddError = true)
        {
            var item = await rpPage.GetById(ExtensionReq.StoreId, pageId);
            if (item == null)
            {
                if (isAddError)
                    context.AddError("errors.page.not_exist", "Trang không tồn tại");
                return false;
            }
            await bizKeyword.ProcessingKeyHandle(ExtensionReq.StoreId, (int)EnumDocumentType.Page, item.handle, null);
            await bizNavigation.Page_UpdateForLinkUrl(item._id, true);
            await _priDelete(item);
            await WriteLogDashboard((int)DashboardLogAction.Page_Delete, item.MGPageToPageDetailModel());
            await rpMetafield.DeleteByOnwerIdResourceType(item._id, (int)MetafieldResourceOwner.Page);
            await _bus.FireStoreDataChange(ExtensionReq.StoreId);
            return true;
        }

        public async Task<(long, List<PageAPIModel>)> GetPageListApi(FilterSearchModel model)
        {
            var data = new List<PageAPIModel>();
            var result = await GetMGPageList(model);
            data = result.data.MGPageListToPageAPIList();
            data = data.OrderByDescending(m => m.updated_at).ToList();
            return (result.totalrecord, data);
        }

        public async Task<long> CountPages(FilterSearchModel model)
        {
            var (pageIds, totalRecord) = await GetPagingPageIds(model);
            if (context.IsError)
            {
                return 0;
            }
            return totalRecord;
        }

        public async Task<PageAPIModel> GetPageDetailApi(long pageId)
        {
            if (pageId <= 0)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var objPage = await rpPage.GetById(ExtensionReq.StoreId, pageId);
            if (objPage == null)
            {
                context.AddError("errors.page.not_exist", "Trang không tồn tại");
                return null;
            }
            return objPage.ToAPIModel();
        }

        public async Task<PageAPIModel> AddNewPageApi(Dictionary<string, object> dicInserted)
        {
            var pageModel = await ParseDictionaryPageApi(dicInserted);

            if (context.IsError)
                return null;

            var newpage = await AddNewAsync(pageModel);
            return await GetPageDetailApi(newpage.Id);
        }

        public async Task<PageAPIModel> UpdatePageApi(long pageId, Dictionary<string, object> dicUpdated)
        {
            if (pageId <= 0)
            {
                context.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return null;
            }
            var dicDashboard = new Dictionary<long, DashboardLogAction>();
            var pageModel = await ParseDictionaryPageApi(dicUpdated, pageId);
            if (context.IsError)
                return null;
            var updpage = await UpdateAsync(pageModel);
            return await GetPageDetailApi(pageModel.Id);
        }

        #endregion public

        #region private

        private async Task _priDelete(MGPageModel model)
        {
            await rpPage.DeleteByIdAsync(ExtensionReq.OrgId, model._id);
            await rpESPage.RemoveAsync(ExtensionReq.OrgId, model._id);
            var logmodel = new MGLogData()
            {
                created_date = DateTime.UtcNow,
                created_user = ExtensionReq.UserId,
                data = JsonConvert.SerializeObject(model.MGPageToPageDetailModel()),
                refid = model._id,
                storeid = model.storeid,
                doctypeid = (int)EnumDocumentType.Page
            };
            await _mgLog.PostLog(logmodel);
        }

        private async Task<(Dictionary<int, long> pageIds, long totalRecord)> GetPagingPageIds(FilterSearchModel filter)
        {
            var isDefault = false;
            if (filter == null)
            {
                context.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return (null, 0);
            }
            if (
                   string.IsNullOrWhiteSpace(filter.FreeText)
                   && (filter.Fields == null || !filter.Fields.Any())
                   && string.IsNullOrWhiteSpace(filter.SortFieldName)
                   && string.IsNullOrWhiteSpace(filter.SortType)
              )
                isDefault = true;

            long totalRecord = 0;
            var ids = new List<long>();
            var dicIds = new Dictionary<int, long>();
            if (isDefault)
            {
                totalRecord = await rpPage.CountAsync(ExtensionReq.OrgId);
                ids = await rpPage.GetByPagingAsync(ExtensionReq.OrgId, filter.Page.currentPage, filter.Page.pageSize);
                if (ids != null && ids.Any())
                {
                    for (int i = 0; i < ids.Count; i++)
                    {
                        dicIds.Add(i + 1, ids[i]);
                    }
                }
            }
            else
            {
                if (filter.Fields == null && filter.SortFieldName == null && filter.SortType == null)
                {
                    filter.SortFieldName = "UpdatedDate";
                    filter.SortType = "desc";
                }
                else if (filter.SortFieldName != null && filter.SortType == null)
                {
                    filter.SortType = "desc";
                }
                else if (filter.SortFieldName == null && filter.SortType != null)
                {
                    filter.SortFieldName = "UpdatedDate";
                }

                var rs = new ESSearchResult();
                rs = await rpESPageSearch.SearchAsync(ExtensionReq.OrgId, filter);
                totalRecord = rs.Total;
                dicIds = rs.Ids;
            }
            return (dicIds, totalRecord);
        }

        private async Task<(List<MGPageModel> data, long totalrecord)> GetMGPageList(FilterSearchModel filter)
        {
            var data = new List<MGPageModel>();
            var (pageIds, totalRecord) = await GetPagingPageIds(filter);
            if (context.IsError)
                return (null, 0);

            if (pageIds != null && pageIds.Any())
            {
                data = await _priGetByIds(ExtensionReq.StoreId, pageIds);
                data.ForEach(r =>
                {
                    if (r.published_at != null && r.published_at.Value <= DateTime.UtcNow)
                        r.IsVisible = true;
                    else
                        r.IsVisible = false;
                });
            }
            return (data, totalRecord);
        }

        private async Task<PageDetailModel> ParseDictionaryPageApi(Dictionary<string, object> dictionaryModel, long pageId = 0)
        {
            if (dictionaryModel == null || dictionaryModel.Count == 0)
            {
                context.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return null;
            }

            PageDetailModel resultModel;

            if (pageId <= 0)
            {
                resultModel = new PageDetailModel()
                {
                    Id = pageId,
                    PublishedDate = DateTime.UtcNow,
                    TemplateName = TemplateName.Page
                };
            }
            else
            {
                resultModel = await GetDetail(pageId);
                if (context.IsError || resultModel == null)
                {
                    return null;
                }
            }

            var propertyInfos = typeof(PageAPIModel).GetProperties();
            foreach (var propertyValuePair in dictionaryModel)
            {
                var colName = propertyInfos.Single(x => x.Name == propertyValuePair.Key);
                if (colName.Name.Equals("title"))
                {
                    if (pageId <= 0 && string.IsNullOrEmpty((string)propertyValuePair.Value))
                    {
                        context.AddError("errors.page.title.is_empty", "Tiêu đề trang không được bỏ trống");
                        return null;
                    }
                    else
                    {
                        resultModel.Title = (string)propertyValuePair.Value;
                    }
                }
                if (colName.Name.Equals("handle"))
                {
                    resultModel.UrlHandle = (string)propertyValuePair.Value;
                }
                if (colName.Name.Equals("author"))
                {
                    resultModel.AuthorName = (string)propertyValuePair.Value;
                }
                if (colName.Name.Equals("body_html"))
                {
                    var strContent = propertyValuePair.Value?.ToString().Trim();
                    var santilizeContent = HtmlUtility.Instance.SanitizeHtml(strContent, HtmlUtility.ValidHtmlTags.Page);
                    if (!string.IsNullOrWhiteSpace(santilizeContent))
                    {
                        if (santilizeContent.Trim().Length > DefaultData.DefaultMaxPageDescriptionLength)
                        {
                            context.AddError("errors.page.content.too_long", "Nội dung quá dài");
                            return null;
                        }
                    }
                    resultModel.Content = santilizeContent;
                }
                if (colName.Name.Equals("template_suffix"))
                {
                    if (!string.IsNullOrEmpty((string)propertyValuePair.Value))
                    {
                        resultModel.TemplateName = (string)propertyValuePair.Value;
                    }
                    else
                    {
                        resultModel.TemplateName = string.Empty;
                    }
                }

                if (colName.Name.Equals("published"))
                {
                    var isPublish = bool.Parse(propertyValuePair.Value.ToString());
                    resultModel.PublishedDate = isPublish ? (DateTime?)DateTime.UtcNow : null;
                }
            }

            return resultModel;
        }

        private async Task<(string handle, int logaction, MGPageModel page)> ValidateModel(PageDetailModel model, bool isUpdate)
        {
            var oldUrlHandle = "";
            var logAction = isUpdate ? (int)DashboardLogAction.Page_Update
                                 : (int)DashboardLogAction.Page_Create;

            if (model == null)
            {
                context.AddError("errors.information.no_valid", "Thông tin không hợp lệ.");
                return (null, logAction, null);
            }

            if (model.PublishedDate == DateTime.MinValue)
                model.PublishedDate = DateTime.UtcNow;

            if (!StringHelper.CheckStringIsNullOrEmptyOrWhitespace(model.Content))
            {
                model.Content = HtmlUtility.Instance.SanitizeHtml(model.Content, HtmlUtility.ValidHtmlTags.Page);
                if (model.Content.Trim().Length > DefaultData.DefaultMaxPageDescriptionLength)
                {
                    context.AddError("errors.page.content.too_long", "Nội dung quá dài");
                    return (null, logAction, null);
                }
            }
            var strMetaDescription = HtmlStringHelpers.StripHtml(model.MetaDescription);
            if (!string.IsNullOrEmpty(strMetaDescription))
            {
                if (strMetaDescription.Length > DefaultData.DefaultMaxMetaDescription)
                {
                    context.AddError($"errors.page.description.max_limit_reached__{DefaultData.DefaultMaxMetaDescription}__", string.Format("Mô tả trang không được quá {0} ký tự", DefaultData.DefaultMaxMetaDescription));
                    return (null, logAction, null);
                }
            }
            else
            {
                if (!StringHelper.CheckStringIsNullOrEmptyOrWhitespace(HtmlStringHelpers.StripHtml(model.Content)))
                {
                    strMetaDescription = HtmlStringHelpers.StripHtml(model.Content);
                    if (strMetaDescription.Length > DefaultData.DefaultMaxMetaDescription)
                        model.MetaDescription = strMetaDescription.Substring(0, DefaultData.DefaultMaxMetaDescription);
                    else
                        model.MetaDescription = strMetaDescription;
                }
                else model.MetaDescription = "";
            }
            if (string.IsNullOrEmpty(model.Title))
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return (null, logAction, null);
            }
            model.Title = model.Title.Trim();
            if (model.Title.Length > 255)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return (null, logAction, null);
            }

            if (string.IsNullOrEmpty(model.PageTitle))
            {
                var title = model.Title.Trim();
                model.PageTitle = title.Length <= 70 ? title
                                    : title.Substring(0, 70);
            }
            else if (model.PageTitle.Length > 70)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return (null, logAction, null);
            }
            else
            {
                model.PageTitle = model.PageTitle.Trim();
            }
            if (string.IsNullOrEmpty(model.UrlHandle))
            {
                model.UrlHandle = model.Title;
            }
            else if (model.UrlHandle.Length > 300)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return (null, logAction, null);
            }
            if (model.TemplateName == TemplateName.Page || string.IsNullOrWhiteSpace(model.TemplateName))
            {
                model.TemplateName = string.Empty;
            }
            else
                model.TemplateName = model.TemplateName.Replace(TemplateName.Page + ".", string.Empty);
            if (string.IsNullOrEmpty(model.AuthorName))
            {
                var userDetails = await rpUser.GetByIds(ExtensionReq.UserId);
                model.AuthorName = userDetails != null ? userDetails.LastName + " " + userDetails.FirstName : "";
            }
            else
            {
                model.AuthorName = model.AuthorName.Trim();
                if (model.AuthorName.Length > 100)
                {
                    context.AddError("errors.article.name_author.too_long", "Tên tác giả quá dài.");
                    return (null, logAction, null);
                }
            }
            if (isUpdate)
            {
                if (model.Id <= 0)
                {
                    context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                    return (null, logAction, null);
                }

                var page = await rpPage.GetById(ExtensionReq.OrgId, model.Id);

                if (page == null)
                {
                    context.AddError("errors.page.not_found.cancel_update", "Không tìm thấy trang. Hủy thao tác cập nhật");
                    return (null, logAction, null);
                }
                if (page.storeid != ExtensionReq.StoreId)
                {
                    context.AddError("errors.page.invalid_information.cancel_update", "Thông tin không hợp lệ. Hủy thao tác cập nhật");
                    return (null, logAction, null);
                }
                if (StringHelper.CheckStringIsNullOrEmptyOrWhitespace(model.MetaDescription))
                    model.MetaDescription = page.meta_description;
                oldUrlHandle = page.handle;
                model.CreatedDate = page.created_at;
                model.Created_user = page.created_user;
                var isVisible = !model.PublishedDate.HasValue || model.PublishedDate.Value <= DateTime.UtcNow;
                var isPublishDateChange = (page.published_at.HasValue != model.PublishedDate.HasValue)
                                          || (model.PublishedDate.HasValue &&
                                                model.PublishedDate.Value != page.published_at.Value)
                                          ? true : false;
                if (isPublishDateChange)
                {
                    switch (isVisible)
                    {
                        case true:
                            logAction = (int)DashboardLogAction.Page_Publish;
                            break;

                        case false:
                            logAction = (int)DashboardLogAction.Page_Hide;
                            break;
                    }
                }
            }
            else
            {
                if (model.IsSetPublishDate)
                {
                    if (model.PublishedDate == null)
                        model.PublishedDate = DateTime.UtcNow;
                }
            }
            if (model.PublishedDate != null && model.PublishedDate.Value.ToUniversalTime() <= DateTime.UtcNow)
                model.IsVisible = true;
            else
                model.IsVisible = false;
            return (oldUrlHandle, logAction, model.ToModelMG());
        }

        private FilterSearchModel buildFilterSearchModel(
                                                                           bool visible,
                                                                           bool hidden,
                                                                           string query,
                                                                           string order,
                                                                           string direction,
                                                                           int page,
                                                                           int limit

                                                           )
        {
            var filter = new FilterSearchModel()
            {
                FreeText = query,
                Page = new FilterSearchPage()
                {
                    currentPage = page <= 0 ? 1 : page,
                    pageSize = limit <= 0 || limit > 50 ? 50 : limit
                },
                SortFieldName = PrepareSortFieldName(order),
                SortType = PrepareSortType(direction),
                Fields = new List<FilterSearchField>()
            };
            if (visible)
            {
                filter = PreparePublishStatusFilter(filter, true);
            }
            if (hidden)
            {
                filter = PreparePublishStatusFilter(filter, false);
            }

            return filter;
        }

        private string PrepareSortFieldName(string order)
        {
            if (!StringHelper.CheckStringIsNullOrEmptyOrWhitespace(order))
            {
                order = order.Trim();
                switch (order)
                {
                    case "title":
                        return "Title";

                    case "updatedate":
                        return "UpdatedDate";

                    default:
                        return null;
                }
            }
            return null;
        }

        private string PrepareSortType(string direction)
        {
            if (!StringHelper.CheckStringIsNullOrEmptyOrWhitespace(direction))
            {
                direction = direction.Trim();
                switch (direction)
                {
                    case "asc":
                        return "asc";

                    case "desc":
                        return "desc";

                    default:
                        return null;
                }
            }
            return null;
        }

        private FilterSearchModel PreparePublishStatusFilter(FilterSearchModel model, bool visible)
        {
            switch (visible)
            {
                case true:
                    model.Fields.Add(new FilterSearchField()
                    {
                        FieldName = "PublishedDate",
                        HasOptions = true,
                        OptionValue = "BeforeEqualsNow"
                    });
                    break;

                case false:
                    model.Fields.Add(new FilterSearchField()
                    {
                        FieldName = "PublishedDate",
                        HasOptions = true,
                        OptionValue = "AfterNow"
                    });
                    break;

                default:
                    break;
            }
            return model;
        }

        private async Task<PageDetailModel> _priSetPage(MGPageModel newPage)
        {
            newPage.storeid = ExtensionReq.OrgId;
            newPage.created_at = DateTime.UtcNow;
            newPage.created_user = ExtensionReq.UserId;
            newPage.updated_at = DateTime.UtcNow;
            newPage.updated_user = ExtensionReq.UserId;
            newPage.isdeleted = false;
            var mgpage = await rpPage.SetAsync(newPage);
            return mgpage.MGPageToPageDetailModel();
        }

        private async Task<List<MGPageModel>> RefinePagesOfStore(List<long> pagesIds)
        {
            var pagesOfStore = new List<MGPageModel>();

            if (pagesIds == null) return pagesOfStore;

            foreach (var page in pagesIds)
            {
                var item = await rpPage.GetById(ExtensionReq.StoreId, page);

                if (item == null)
                    context.AddError("﻿errors.page.not_found", "Không tìm thấy page");
                else if (item.storeid != ExtensionReq.StoreId)
                    context.AddInfo(string.Format("Hủy thao tác với page {0}. Thông tin không hợp lệ", item.title));
                else
                    pagesOfStore.Add(item);
            }
            return pagesOfStore;
        }

        private async Task<PageDetailModel> _priUpdatePage(MGPageModel page)
        {
            page.storeid = ExtensionReq.OrgId;
            page.updated_at = DateTime.UtcNow;
            page.updated_user = ExtensionReq.UserId;
            page.isdeleted = false;
            var mgpage = await rpPage.SetAsync(page);
            return mgpage.MGPageToPageDetailModel();
        }

        private async Task<PageMsg> GetEBSPageMsg(long pageId)
        {
            MGPageModel entityModel = await rpPage.GetById(ExtensionReq.StoreId, pageId);
            if (entityModel == null)
                return null;
            var msg = entityModel.MGPageToPageMSG();
            msg.Action = msg.IsDeleted ? MessageAction.Delete : MessageAction.Insert;
            return msg;
        }

        private async Task<List<MGPageModel>> _priGetByIds(long orgid, Dictionary<int, long> dids)
        {
            var ids = dids.Select(m => m.Value).ToList();
            var result = await rpPage.GetById(orgid, ids);
            var tmp = from v in result
                      join k in dids on v._id equals k.Value
                      orderby k.Key
                      select v;
            return tmp.ToList();
        }

        private async Task WriteLogDashboard(int logAction, PageDetailModel page)
        {
            var model = new PageLogModel
            {
                Title = page.Title,
                LogDate = DateTime.UtcNow,
                LogUser = ExtensionReq.UserName,
                LinkDetail = "page#/detail/" + page.Id
            };
            var logdata = JsonConvert.SerializeObject(model);
            var msg = new LogDataMsg
            {
                LogData = logdata,
                CreatedUser = ExtensionReq.UserId,
                CreatedUserEmail = ExtensionReq.UserEmail,
                ActionId = logAction,
                TypeId = (int)BHN.SharedObject.EBSMessage.LogType.Dashboard,
                RefId = page.Id,
                StoreId = ExtensionReq.StoreId,
                CreatedDate = DateTime.UtcNow,
                Action = MessageAction.Insert,
                IsCommentLog = false,
                CreatedUserName = ExtensionReq.UserName
            };

            await _bus.SendMsgAsync(msg);
        }

        #endregion private

        private FilterSearchModel PreparePageFilter(
                int limit, int page, long since_id, string title, string handle, DateTime? created_at_min,
                DateTime? created_at_max, DateTime? updated_at_min, DateTime? updated_at_max,
                DateTime? published_at_min, DateTime? published_at_max, string published_status, long id)
        {
            var filter = new FilterSearchModel
            {
                Fields = new List<FilterSearchField>(),
                Page = new FilterSearchPage { pageSize = limit, currentPage = page },
                ViewId = SysView.Page
            };
            if (!String.IsNullOrWhiteSpace(title))
            {
                var field = new FilterSearchField { FieldName = "Title", OptionValueValue = title, HasOptions = true, OptionValue = "=" };
                filter.Fields.Add(field);
            }
            if (!String.IsNullOrWhiteSpace(handle))
            {
                var field = new FilterSearchField { FieldName = "UrlHandle", OptionValueValue = handle, HasOptions = true, OptionValue = "=" };
                filter.Fields.Add(field);
            }
            if (id > 0)
            {
                var field = new FilterSearchField { FieldName = "Id", NummericOperator = "=", IsNummeric = true, NummericValue = id.ToString() };
                filter.Fields.Add(field);
            }
            if (since_id > 0)
            {
                var field = new FilterSearchField { FieldName = "Id", NummericOperator = ">", IsNummeric = true, NummericValue = since_id.ToString() };
                filter.Fields.Add(field);
            }
            if (created_at_min != null && created_at_min != DateTime.MinValue)
            {
                var field = new FilterSearchField
                {
                    FieldName = "CreatedDate",
                    OptionValue = "AfterExtract",
                    OptionValueValue = created_at_min.Value.ToString("o")
                };
                filter.Fields.Add(field);
            }
            if (created_at_max != null && created_at_max != DateTime.MinValue)
            {
                var field = new FilterSearchField
                {
                    FieldName = "CreatedDate",
                    OptionValue = "BeforeExtract",
                    OptionValueValue = created_at_max.Value.ToString("o")
                };
                filter.Fields.Add(field);
            }
            if (updated_at_min != null && updated_at_min != DateTime.MinValue)
            {
                var field = new FilterSearchField
                {
                    FieldName = "UpdatedDate",
                    OptionValue = "AfterExtract",
                    OptionValueValue = updated_at_min.Value.ToString("o")
                };
                filter.Fields.Add(field);
            }
            if (updated_at_max != null && updated_at_max != DateTime.MinValue)
            {
                var field = new FilterSearchField
                {
                    FieldName = "UpdatedDate",
                    OptionValue = "BeforeExtract",
                    OptionValueValue = updated_at_max.Value.ToString("o")
                };
                filter.Fields.Add(field);
            }
            if (published_at_min != null && published_at_min != DateTime.MinValue)
            {
                var field = new FilterSearchField
                {
                    FieldName = "PublishDate",
                    OptionValue = "AfterExtract",
                    OptionValueValue = published_at_min.Value.ToString("o")
                };
                filter.Fields.Add(field);
            }
            if (published_at_max != null && published_at_max != DateTime.MinValue)
            {
                var field = new FilterSearchField
                {
                    FieldName = "PublishDate",
                    OptionValue = "BeforeExtract",
                    OptionValueValue = published_at_max.Value.ToString("o")
                };
                filter.Fields.Add(field);
            }
            if (!String.IsNullOrWhiteSpace(published_status))
            {
                var status = published_status;
                if (status.Equals("published"))
                {
                    var field = new FilterSearchField
                    {
                        FieldName = "PublishedDate",
                        OptionValue = "BeforeEqualsNow"
                    };
                    filter.Fields.Add(field);
                }
                else if (status.Equals("unpublished"))
                {
                    var field = new FilterSearchField
                    {
                        FieldName = "PublishedDate",
                        OptionValue = "AfterNow"
                    };
                    filter.Fields.Add(field);
                }
            }
            return filter;
        }

        #region OpenApi

        public async Task<(List<PageAPIModel> pages, long totalRecord)> OpenApi_GetPagesList(
                int limit, int page, long since_id, string title, string handle, DateTime? created_at_min,
                DateTime? created_at_max, DateTime? updated_at_min, DateTime? updated_at_max,
                DateTime? published_at_min, DateTime? published_at_max, string published_status, long id
            )
        {
            var filter = PreparePageFilter(
                limit, page, since_id, title, handle, created_at_min, created_at_max,
                updated_at_min, updated_at_max, published_at_min, published_at_max, published_status, id);
            var (data, totalrecord) = await GetMGPageList(filter);
            if (context.IsError)
            {
                return (null, 0);
            }
            return (data.MGPageListToPageAPIList(), totalrecord);
        }

        public async Task<long> OpenApi_CountPages(
                int limit, int page, long since_id, string title, string handle, DateTime? created_at_min,
                DateTime? created_at_max, DateTime? updated_at_min, DateTime? updated_at_max,
                DateTime? published_at_min, DateTime? published_at_max, string published_status, long id
            )
        {
            var filter = PreparePageFilter(
                 limit, page, since_id, title, handle, created_at_min, created_at_max,
                 updated_at_min, updated_at_max, published_at_min, published_at_max, published_status, id);
            var (pageIds, totalRecord) = await GetPagingPageIds(filter);
            if (context.IsError)
            {
                return 0;
            }
            return totalRecord;
        }

        public async Task<PageAPIModel> OpenApi_GetPageDetail(long page_id)
        {
            return await GetPageDetailApi(page_id);
        }

        public async Task<bool> OpenApi_DeletePage(long page_id)
        {
            return await DeletePageByIdAsync(page_id);
        }

        public async Task<PageAPIModel> OpenApi_UpdatePage(long page_id, Dictionary<string, object> model)
        {
            return await UpdatePageApi(page_id, model);
        }

        public async Task<PageAPIModel> OpenApi_AddPage(Dictionary<string, object> model)
        {
            return await AddNewPageApi(model);
        }

        #endregion OpenApi
    }
}