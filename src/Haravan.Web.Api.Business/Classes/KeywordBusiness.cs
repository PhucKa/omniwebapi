﻿using Haravan.Web.Api.Repository;
using Haravan.Web.Api.Utils.Extensions;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public class KeywordBusiness : IKeywordBusiness
    {
        private readonly IMGKeywordHandleRepo rpKeywordHandle;

        public KeywordBusiness(
          IMGKeywordHandleRepo rpKeywordHandle
          )
        {
            this.rpKeywordHandle = rpKeywordHandle;
        }

        public async Task<string> ProcessingKeyHandle(long storeId, int typeId, string keywordOld, string keywordNew)
        {
            var newKeyword = StringHelper.CreateFriendlyURL(keywordNew);
            return await rpKeywordHandle.ProcessingKeyHandle(storeId, typeId, keywordOld, newKeyword);
        }

        public async Task<string> ProcessingKeyHandleWithRefId(long storeId, int typeid, string keywordold, string keywordnew, long oldRefId, long newRefId)
        {
            var newKeyword = StringHelper.CreateFriendlyURL(keywordnew);
            return await rpKeywordHandle.ProcessingKeyHandleWithRefId(storeId, typeid, keywordold, newKeyword, oldRefId, newRefId);
        }
    }
}