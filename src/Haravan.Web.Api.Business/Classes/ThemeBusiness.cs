﻿using BHN.Core;
using BHN.SharedObject.APIDataModel;
using BHN.SharedObject.EBSMessage;
using BHN.SharedObject.EBSMessage.TaskProcessMsg;
using Haravan.Caching;
using Haravan.Libs.Abstractions;
using Haravan.Web.Api.Business.Extensions;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Configs;
using Haravan.Web.Api.BusinessObjects.ESModels;
using Haravan.Web.Api.BusinessObjects.Mappers;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.BusinessObjects.Models.Common;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using Haravan.Web.Api.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public class ThemeBusiness : IThemeBusiness
    {
        private readonly IRequestContext context;
        private readonly IThemeRepository rpTheme;
        private readonly IMGThemeFileRepository rpThemeFile;
        private readonly IExtensionPerRequest extensionReq;
        private readonly IServiceProvider serviceProvider;
        private readonly IEBSComMessageRepository bus;
        private readonly AppConfig config;
        private readonly IFileBusiness fileBiz;
        private readonly ICache cache;
        private readonly IMGBlogRepository rpBlog;
        private readonly IMGPageRepository rpPage;
        private readonly IFileRepository rpFile;

        public ThemeBusiness(IRequestContext context,
            IExtensionPerRequest ExtensionReq,
            IThemeRepository rpTheme,
            IMGThemeFileRepository rpThemeFile,
            IEBSComMessageRepository bus,
            IServiceProvider serviceProvider,
            IOptions<AppConfig> config,
            IFileBusiness fileBiz,
            ICache cache,
            IMGBlogRepository rpBlog,
            IMGPageRepository rpPage,
            IFileRepository rpFile
            )
        {
            this.config = config.Value;
            this.context = context;
            this.rpTheme = rpTheme;
            this.rpThemeFile = rpThemeFile;
            this.extensionReq = ExtensionReq;
            this.serviceProvider = serviceProvider;
            this.bus = bus;
            this.fileBiz = fileBiz;
            this.cache = cache;
            this.rpBlog = rpBlog;
            this.rpPage = rpPage;
            this.rpFile = rpFile;
        }

        #region OPEN API

        public async Task<List<AssetAPIModel>> GetAssetList(long themeId)
        {
            if (themeId <= 0)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var objAsset = await rpTheme.GetAssetList(themeId);
            if (objAsset.HasError)
            {
                context.AddError(objAsset.ErrorCodes[0].ToString(), objAsset.Errors[0].ToString());
                return null;
            }
            return objAsset.Data;
        }

        public async Task<AssetAPIModel> GetAssetDetail(long themeId, int fileType, string fileName)
        {
            if (themeId <= 0 || fileType <= 0 || String.IsNullOrEmpty(fileName))
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ!");
                return null;
            }
            var objAsset = await rpTheme.GetAssetDetail(themeId, fileType, fileName);
            if (objAsset.HasError)
            {
                context.AddError(objAsset.ErrorCodes[0].ToString(), objAsset.Errors[0].ToString());
                return null;
            }
            return objAsset.Data;
        }

        public async Task<bool> DeleteAssetApi(long themeId, int fileType, string fileName)
        {
            if (themeId <= 0)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ!");
                return false;
            }
            var objAsset = await rpTheme.DeleteAssetApi(themeId, fileType, fileName);
            if (objAsset.HasError)
            {
                context.AddError(objAsset.ErrorCodes[0].ToString(), objAsset.Errors[0].ToString());
                return false;
            }
            return objAsset.Data;
        }

        public async Task<AssetAPIModel> CreateOrUpdateAsset(long themeId, Dictionary<string, object> dicInserted)
        {
            if (themeId <= 0)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ!");
                return null;
            }
            var objAsset = await rpTheme.CreateOrUpdateAsset(themeId, dicInserted);
            if (objAsset.HasError)
            {
                context.AddError(objAsset.ErrorCodes[0].ToString(), objAsset.Errors[0].ToString());
                return null;
            }
            return objAsset.Data;
        }

        public async Task<List<ThemeAPIModel>> GetListThemesApi()
        {
            var objAsset = await rpTheme.GetListThemesApi();
            if (objAsset.HasError)
            {
                context.AddError(objAsset.ErrorCodes[0].ToString(), objAsset.Errors[0].ToString());
                return null;
            }
            return objAsset.Data;
        }

        public async Task<ThemeAPIModel> GetThemeDetailApi(long themeId)
        {
            if (themeId <= 0)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ!");
                return null;
            }
            var objAsset = await rpTheme.GetThemeDetailApi(themeId);
            if (objAsset.HasError)
            {
                context.AddError(objAsset.ErrorCodes[0].ToString(), objAsset.Errors[0].ToString());
                return null;
            }
            return objAsset.Data;
        }

        public async Task<bool> DeleteThemeApi(long themeId)
        {
            if (themeId <= 0)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ!");
                return false;
            }
            var objAsset = await rpTheme.DeleteThemeApi(themeId);
            if (objAsset.HasError)
            {
                context.AddError(objAsset.ErrorCodes[0].ToString(), objAsset.Errors[0].ToString());
                return false;
            }
            return objAsset.Data;
        }

        public async Task<ThemeAPIModel> UpdateThemeApi(long themeId, Dictionary<string, object> dicUpdated)
        {
            if (themeId <= 0)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ!");
                return null;
            }
            var objAsset = await rpTheme.UpdateThemeApi(themeId, dicUpdated);
            if (objAsset.HasError)
            {
                context.AddError(objAsset.ErrorCodes[0].ToString(), objAsset.Errors[0].ToString());
                return null;
            }
            if (dicUpdated.ContainsKey("role"))
            {
                {
                    var roleTheme = dicUpdated["role"].ToString().ToLower();
                    var publishRP = false;
                    switch (roleTheme)
                    {
                        case "main":
                            publishRP = await PublishThemeApi(themeId, BusinessObjects.Enums.ThemeType.Main);
                            break;

                        case "mobile":
                            publishRP = await PublishThemeApi(themeId, BusinessObjects.Enums.ThemeType.Mobile);
                            break;

                        default: break;
                    }

                    if (context.IsError)
                    {
                        context.AddError(objAsset.ErrorCodes[0].ToString(), objAsset.Errors[0].ToString());
                        return null;
                    }
                }
            }
            return objAsset.Data;
        }

        public async Task<ThemeAPIModel> CreateThemeApi(Dictionary<string, object> dicUpdated)
        {
            var model = ParseDictionaryThemesApi(dicUpdated);
            var objAsset = await rpTheme.CreateThemeApi(model);
            if (objAsset.HasError)
            {
                context.AddError(objAsset.ErrorCodes[0].ToString(), objAsset.Errors[0].ToString());
                return null;
            }

            return objAsset.Data;
        }

        public async Task<bool> PublishThemeApi(long themeId, BusinessObjects.Enums.ThemeType? type)
        {
            if (themeId <= 0)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ!");
                return false;
            }
            var objAsset = await rpTheme.PublishThemeApi(themeId, type);
            if (objAsset.HasError)
            {
                context.AddError(objAsset.ErrorCodes[0].ToString(), objAsset.Errors[0].ToString());
                return false;
            }
            return objAsset.Data;
        }

        #endregion OPEN API

        #region Theme

        public async Task<string> GetSettingDataFile(long themeId)
        {
            if (themeId <= 0)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var result = await rpTheme.GetSettingDataFile(themeId);
            if (result.HasError)
            {
                context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return null;
            }
            return result.Data;
        }

        public async Task<string> GetSettingSchemaThemeFile(long themeId)
        {
            if (themeId <= 0)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var result = await rpTheme.GetSettingSchemaThemeFile(themeId);
            if (result.HasError)
            {
                context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return null;
            }
            return result.Data;
        }

        public async Task<ThemeFileModel> EditSettingData(long themeId, StringModel model)
        {
            if (themeId <= 0 || model == null)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var resultTask = await rpTheme.EditSettingData(themeId, model);
            if (resultTask.HasError)
            {
                context.AddError(resultTask.ErrorCodes[0].ToString(), resultTask.Errors[0].ToString());
                return null;
            }
            await updateSettingDataTmpContent(themeId, model.Value);
            return resultTask.Data;
        }

        public async Task UpdateSettingDataTmpContent(long themeId, string content)
        {
            await updateSettingDataTmpContent(themeId, content);
        }

        public async Task ResetSettingDataTmpContent(long themeId)
        {
            var currentSettingDataTheme = await rpThemeFile.GetByFileNameAsync(extensionReq.StoreId, themeId, ThemeFile.SettingDataFileName);
            var content = currentSettingDataTheme.content;
            await updateSettingDataTmpContent(themeId, content);
        }

        private bool IsNewFileService(long themeId)
        {
            return themeId > config.FileServiceFromThemeId;
        }

        private async Task updateSettingDataTmpContent(long themeId, string content)
        {
            var storeId = extensionReq.StoreId;
            var fileName = ThemeFile.SettingDataTmpFileName;
            content = content.Replace("Shopify", "Haravan").Replace("shopify", "haravan");

            var objSettingDataTmpFile = await rpThemeFile.GetByFileNameAsync(storeId, themeId, fileName);
            var isInsert = objSettingDataTmpFile == null;

            if (isInsert)
            {
                objSettingDataTmpFile = new MGThemeFileModel()
                {
                    content = content,
                    created_at = DateTime.UtcNow,
                    filename = fileName,
                    storeid = storeId,
                    themeid = themeId,
                    updated_at = DateTime.UtcNow
                };
                await rpThemeFile.AddAsync(objSettingDataTmpFile);
            }
            else
            {
                var currentContent = objSettingDataTmpFile.content;
                if (currentContent != content)
                {
                    objSettingDataTmpFile.content = content;
                    objSettingDataTmpFile.updated_at = DateTime.UtcNow;
                    await rpThemeFile.UpdateAsync(objSettingDataTmpFile, m => m.content, m => m.updated_at, m => m.url);
                }
            }
        }

        //fix
        public async Task<bool> ExportTheme(long themeId)
        {
            if (themeId <= 0)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return false;
            }
            var resultTask = await rpTheme.Export(themeId);
            if (resultTask.HasError)
            {
                context.AddError(resultTask.ErrorCodes[0].ToString(), resultTask.Errors[0].ToString());
                return false;
            }
            return true;
        }

        public async Task<List<ThemeModel>> PublishTheme(long Id)
        {
            if (Id <= 0)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var resultTask = await rpTheme.PublishTheme(Id);
            if (resultTask.HasError)
            {
                context.AddError(resultTask.ErrorCodes[0].ToString(), resultTask.Errors[0].ToString());
                return null;
            }
            return resultTask.Data;
        }

        public async Task<ThemeModel> UnPublishTheme(ThemeModel model)
        {
            if (model == null || model.Id <= 0)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var resultTask = await rpTheme.UnPublishTheme(model);
            if (resultTask.HasError)
            {
                context.AddError(resultTask.ErrorCodes[0].ToString(), resultTask.Errors[0].ToString());
                return null;
            }
            return resultTask.Data;
        }

        public async Task<bool> DeleteTheme(long themeId)
        {
            if (themeId <= 0)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return false;
            }
            var result = await rpTheme.DeleteTheme(themeId);
            if (result.HasError)
            {
                context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return false;
            }
            return result.Data;
        }

        public async Task<ThemeListModel> GetThemeList()
        {
            var result = await rpTheme.GetThemeList();
            if (result.HasError)
            {
                context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return null;
            }
            return result.Data;
        }

        public async Task<ThemeModel> DuplicateTheme(long themeId)
        {
            if (themeId <= 0)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var result = await rpTheme.DuplicateTheme(themeId);
            if (result.HasError)
            {
                context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return null;
            }
            return result.Data;
        }
        public async Task<ThemeModel> DuplicateThemeByLanguage(long themeId, string prefixText)
        {
            if (themeId <= 0)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var result = await rpTheme.DuplicateThemeByLanguage(themeId, prefixText);
            if (result.HasError)
            {
                context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return null;
            }
            return result.Data;
        }

        public async Task<string> GetFrameToken(long themeId)
        {
            if (themeId <= 0)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var result = await rpTheme.GetFrameToken(themeId);
            if (result.HasError)
            {
                context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return null;
            }
            return result.Data;
        }

        public async Task<ThemeModel> ImportTheme(IFormFile file)
        {
            if (file == null)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var streamData = new MemoryStream();
            file.CopyTo(streamData);
            return await ImportTheme(file.FileName, file);
        }

        public async Task<ThemeModel> ImportTheme(string name, IFormFile file)
        {
            if (string.IsNullOrEmpty(name))
            {
                context.AddError("errors.name_file.is_empty", "Tên file không được để trống");
                return null;
            }
            Stream stream = new MemoryStream();
            file.CopyTo(stream);
            var themeName = name.Substring(0, name.IndexOf(".zip"))
               .Replace("Shopify", "Haravan")
               .Replace("shopify", "haravan")
               .ToLower();
            var lstThemeFileCanNotDelete = await rpTheme.GetListThemeFileCanNotDelete(true);
            if (lstThemeFileCanNotDelete != null && lstThemeFileCanNotDelete.Any())
            {
                stream.Seek(0, SeekOrigin.Begin);
                using (var zip = new ZipArchive(stream, ZipArchiveMode.Read))
                {
                    var zipEntries = zip.Entries.Where(p => !p.FullName.StartsWith("__MACOSX")).ToList();
                    foreach (var a in lstThemeFileCanNotDelete)
                    {
                        var isError = false;
                        if (!zipEntries.Any(b => b.FullName
                            .Replace("customers/account", "customers[account]")
                            .Replace("customers/activate_account", "customers[activate_account]")
                            .Replace("customers/addresses", "customers[addresses]")
                            .Replace("customers/login", "customers[login]")
                            .Replace("customers/order", "customers[order]")
                            .Replace("customers/register", "customers[register]")
                            .Replace("customers/reset_password", "customers[reset_password]")
                            .EndsWith(a.FileName)))
                        {
                            if (a.FileName != "config/settings.html"
                                || !zipEntries.Any(b => b.FullName.EndsWith("settings_schema.json")))
                                isError = true;
                        }
                        if (isError)
                        {
                            context.AddError($"errors.theme.not_file_display__{a.FileName}__", $"Tập tin tải lên không chứa giao diện hợp lệ: Thiếu tập tin {a.FileName}");
                            return null;
                        }
                        var listLayoutThemeFile = zipEntries.Where(p => p.FullName.Contains("/" + ThemeFolderName.Layout + "/") && !a.FileName.EndsWith("/")).ToList();
                        if (listLayoutThemeFile != null && listLayoutThemeFile.Count > 0)
                        {
                            Stream tempStream = null;
                            foreach (var layoutThemeFile in listLayoutThemeFile)
                            {
                                using (var ms = new MemoryStream())
                                {
                                    tempStream = layoutThemeFile.Open();
                                    tempStream.CopyTo(ms);
                                    ms.Position = 0;

                                    using (var reader = new StreamReader(ms, new UTF8Encoding(false)))
                                    {
                                        var tempFileNameArray = layoutThemeFile.FullName.Split('/');

                                        if (tempFileNameArray.Length > 0)
                                        {
                                            var newContent = reader.ReadToEnd();

                                            var tempFileName = tempFileNameArray[tempFileNameArray.Length - 1];

                                            if (tempFileNameArray.Length > 1)
                                            {
                                                tempFileName = tempFileNameArray[tempFileNameArray.Length - 2] + "/" + tempFileName;
                                            }

                                            if (!Regex.IsMatch(newContent, "{{\\s*content_for_header\\s*}}"))
                                            {
                                                context.AddError($"errors.theme.uploaded_file_does_not_contain_a_valid_theme_header__{tempFileName}__", $"Tập tin tải lên không chứa giao diện hợp lệ: Thiếu {{content_for_header}} trong phần header template trong tập tin  {tempFileName}");
                                                return null;
                                            }
                                            if (!Regex.IsMatch(newContent, "{{\\s*content_for_layout\\s*}}"))
                                            {
                                                context.AddError($"errors.theme.uploaded_file_does_not_contain_a_valid_theme_layout__{tempFileName}__", "Tập tin tải lên không chứa giao diện hợp lệ: Thiếu {{content_for_layout}} trong phần body template trong tập tin \"" + tempFileName + "\"");
                                                return null;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    zip.Dispose();
                }
                stream.Close();
                stream.Dispose();
            }
            stream = new MemoryStream();
            file.CopyTo(stream);
            var themeObj = await rpTheme.ImportNewTheme(themeName);
            if (themeObj.HasError)
            {
                context.AddError(themeObj.ErrorCodes[0].ToString(), themeObj.Errors[0].ToString());
                return null;
            }
            stream.Seek(0, SeekOrigin.Begin);
            var bizFile = serviceProvider.GetService<IFileBusiness>();
            var url = await bizFile.AddTemporaryFile(name, stream);

            await ImportTheme_SendMessageEBS(url, themeName, themeObj.Data);
            return themeObj.Data;
        }

        public async Task ImportTheme_SendMessageEBS(
            string url, string fileName, ThemeModel themeModel, bool isPublishAfterSuccess = false)
        {
            if (string.IsNullOrEmpty(url))
            {
                context.AddError("errors.domain.parameters.not_valid", "Tham số không hợp lệ");
                return;
            }

            await bus.SendMsgAsync(new TaskProcessBaseMsg<ImportThemeMsg>()
            {
                Msg = new ImportThemeMsg()
                {
                    DocumentType = (int)EnumDocumentType.Theme,
                    FileUrl = url,
                    StoreId = extensionReq.StoreId,
                    FileName = fileName,
                    ThemeModel = themeModel.ThemeModelToThemeExportMsg(),
                    IsPublishAfterSuccess = isPublishAfterSuccess
                }
            });
        }

        public async Task<ThemeModel> GetThemeForEdit(long id)
        {
            if (id <= 0)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var result = await rpTheme.GetThemeForEdit(id);
            if (result.HasError)
            {
                context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return null;
            }
            return result.Data;
        }

        public async Task<ThemeModel> GetThemeFileForEdit(long themeid)
        {
            if (themeid <= 0)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var result = await rpTheme.GetThemeFileForEdit(themeid);
            if (result.HasError)
            {
                context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return null;
            }
            return result.Data;
        }

        public async Task<ThemeFileModel> LoadCurrentLocale(long id, string locale)
        {
            if (id <= 0)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var result = await rpTheme.LoadCurrentLocale(id, locale);
            if (result.HasError)
            {
                context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return null;
            }
            var model = result.Data;
            return model;
        }

        public async Task<ThemeModel> GetThemeForLocale(long id)
        {
            if (id <= 0)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var result = await rpTheme.GetThemeForLocale(id);
            if (result.HasError)
            {
                context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return null;
            }
            return result.Data;
        }

        public async Task<string> LoadLocaleContent(long themeId, long themeFileId, long localeFileId)
        {
            var objTheme = await rpTheme.GetById(themeId);
            if (objTheme == null)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var objThemeFile = await rpTheme.GetByIdThemeFile(themeFileId);
            if (objThemeFile == null || objThemeFile.Data.ThemeId != themeId)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var objFile = await rpFile.GetByFile(localeFileId);
            if (objFile == null)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var result = await GetThemeFileContent(themeId, objFile.Data.Url, true);
            if (string.IsNullOrWhiteSpace(result))
                result = "{}";
            var _storeData = await extensionReq.GetStoreData();
            if (_storeData.IsUseNewCheckout)
                result = _mergeLocaleCheckout(objFile.Data.FileName, result);
            return result;
        }

        private string _mergeLocaleCheckout(string fileName, string fileContent)
        {
            if (string.IsNullOrWhiteSpace(fileContent))
                fileContent = "{}";

            var name = string.IsNullOrWhiteSpace(fileName)
                ? string.Empty
                : fileName.Replace(".default", "");

            var checkoutLocaleContent = ThemeLocaleDefault.vi;

            if (name.StartsWith(ThemeLocaleAllow.vi.ToString() + ".") && name.EndsWith(".json"))
                checkoutLocaleContent = ThemeLocaleDefault.vi;
            else if (name.StartsWith(ThemeLocaleAllow.en.ToString() + ".") && name.EndsWith(".json"))
                checkoutLocaleContent = ThemeLocaleDefault.en;

            var source = _deserialize(fileContent);
            var checkout = _deserialize(checkoutLocaleContent);
            var merged = _merge(source, checkout);

            return JsonConvert.SerializeObject(merged);
        }

        private IDictionary<string, object> _deserialize(string content)
        {
            var obj = JsonConvert.DeserializeObject<Dictionary<string, object>>(content);

            var rs = new Dictionary<string, object>();

            foreach (var pair in obj)
                if (pair.Value is JObject)
                    rs.Add(pair.Key, _deserialize(pair.Value.ToString()));
                else
                    rs.Add(pair.Key, pair.Value);

            return rs;
        }

        private IDictionary<string, object> _merge(
            IDictionary<string, object> source, IDictionary<string, object> checkout)
        {
            var rs = new Dictionary<string, object>();

            foreach (var obj in source)
                if (checkout.ContainsKey(obj.Key))
                {
                    if (obj.Value is Dictionary<string, object>)
                        rs.Add(obj.Key,
                            _merge(obj.Value as Dictionary<string, object>, checkout[obj.Key] as Dictionary<string, object>));
                    else if (obj.Value == null || string.IsNullOrWhiteSpace(obj.Value as string))
                        rs.Add(obj.Key, checkout[obj.Key]);
                    else
                        rs.Add(obj.Key, obj.Value);
                }
                else
                    rs.Add(obj.Key, obj.Value);

            foreach (var obj in checkout)
            {
                if (source.ContainsKey(obj.Key))
                    continue;

                rs.Add(obj.Key, obj.Value);
            }

            return rs;
        }

        public async Task<ThemeModel> GetThemeForSetting(long id)
        {
            if (id <= 0)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var result = await rpTheme.GetThemeForSetting(id);
            if (result.HasError)
            {
                context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return null;
            }
            return result.Data;
        }

        public async Task<List<FileModel>> GetFileVersion(long Id, long RootFileId)
        {
            if (Id <= 0)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var result = await rpTheme.GetFileVersion(Id, RootFileId);
            if (result.HasError)
            {
                context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return null;
            }
            return result.Data;
        }

        public async Task<bool> DeleteThemeFile(long themefileid)
        {
            if (themefileid <= 0)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return false;
            }
            var model = new ThemeFileModel()
            {
                Id = themefileid
            };
            var result = await rpTheme.DeleteThemeFile(model);
            if (result.HasError)
            {
                context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return false;
            }
            return true;
        }

        public async Task<ThemeFileModel> SaveThemeFile(ThemeFileModelRequest model)
        {
            if (model.Id <= 0 || model.ThemeId <= 0)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var result = await rpTheme.SaveThemeFile(model);
            if (result.HasError)
            {
                context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return null;
            }
            return result.Data;
        }

        public async Task<ThemeFileModel> SaveLocaleThemeFile(ThemeFileModel themeFile, string newContent)
        {
            if (themeFile == null || themeFile.Id <= 0 || themeFile.ThemeId <= 0)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var result = await rpTheme.SaveLocaleThemeFile(themeFile, newContent);
            if (result.HasError)
            {
                context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return null;
            }
            return result.Data;
        }

        public async Task<bool> RenameTheme(long Id, string Name)
        {
            if (Id <= 0)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return false;
            }
            var result = await rpTheme.RenameTheme(Id, Name);
            if (result.HasError)
            {
                context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return false;
            }
            return true;
        }

        public async Task<ThemeFileModel> RenameThemeFile(RenameThemeFileRequest model)
        {
            if (model.Id <= 0 || model.ThemeId <= 0)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var result = await rpTheme.RenameThemeFile(model);
            if (result.HasError)
            {
                context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return null;
            }
            return result.Data;
        }

        public async Task<ThemeFileModel> AddNewThemeFile(ThemeFileModelAddRequest model)
        {
            if (model.ThemeId <= 0)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var file = new FileModel();
            var themeFile = new ThemeFileModel()
            {
                Id = model.Id,
                ThemeId = model.ThemeId,
                FileName = model.FileName,
                Type = model.Type,
                File = file
            };
            var oldThemeFile = new ThemeFileModel();
            var result = await rpTheme.AddNewThemeFile(null, themeFile);
            if (result.HasError)
            {
                context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return null;
            }
            return result.Data;
        }

        public async Task<SimpleListModel> GetListDefaultFont()
        {
            var result = await rpTheme.GetListDefaultFont();
            if (result.HasError)
            {
                context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return null;
            }
            return result.Data;
        }

        public async Task<ThemeModel> CheckThemeIsImporting(long themeId)
        {
            if (themeId <= 0)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var result = await rpTheme.CheckThemeIsImporting(themeId);
            if (result.HasError)
            {
                context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return null;
            }
            return result.Data;
        }

        public async Task<string> GetThemeFileContent(long themeId, string url, bool? isSettingsHtml)
        {
            if (themeId <= 0)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var content = await GetFileContent(fileBiz, themeId, url, true, config);
            if (isSettingsHtml.HasValue && isSettingsHtml.Value && !string.IsNullOrWhiteSpace(content))
            {
                content = HtmlUtility.Instance.SanitizeHtml(content, HtmlUtility.ValidHtmlTags.ThemeSettings);
            }
            return content;
        }

        public async Task<string> GetThemeFileByFileId(long themeId, long fileId, bool? isSettingHtml)
        {
            var storeId = extensionReq.StoreId;
            var objTheme = await rpTheme.GetById(themeId, storeId);
            if (objTheme == null)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var objFile = await rpTheme.GetFileByUrlId(fileId);
            if (objFile == null)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var result = await GetFileContent(fileBiz, themeId, objFile.Data, true, config);
            if (isSettingHtml.HasValue && isSettingHtml.Value && !string.IsNullOrWhiteSpace(result))
            {
                result = HtmlUtility.Instance.SanitizeHtml(result, HtmlUtility.ValidHtmlTags.ThemeSettings);
            }
            return result;
        }

        public async Task<List<FileModel>> GetThemeFileUpload(long themeId, FileModel[] fileList)
        {
            if (themeId <= 0 && fileList == null)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var objTheme = await rpTheme.GetThemeFileUpload(themeId, fileList);
            if (objTheme.HasError)
            {
                context.AddError(objTheme.ErrorCodes[0].ToString(), objTheme.Errors[0].ToString());
                return null;
            }
            return objTheme.Data;
        }

        public async Task<List<KeyValuePair<string, string>>> ThemeSettingGetCollectionTitleByUrlHandle(List<string> listUrlHandle)
        {
            if (listUrlHandle == null)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var objTheme = await rpTheme.ThemeSettingGetCollectionTitleByUrlHandle(listUrlHandle);
            if (objTheme.HasError)
            {
                context.AddError(objTheme.ErrorCodes[0].ToString(), objTheme.Errors[0].ToString());
                return null;
            }
            return objTheme.Data;
        }

        public async Task<List<KeyValuePair<string, string>>> ThemeSettingGetBlogTitleByUrlHandle(List<string> listUrlHandle)
        {
            if (listUrlHandle == null)
            {
                context.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return null;
            }

            if (listUrlHandle.Count == 0)
            {
                return new List<KeyValuePair<string, string>>();
            }

            var listObjBlog = await rpBlog.GetByListUrlHandle(extensionReq.StoreId, listUrlHandle);

            if (listObjBlog == null || listObjBlog.Count == 0)
            {
                return new List<KeyValuePair<string, string>>();
            }

            var result = listObjBlog.ToDictionary(a => a.handle, a => a.title).ToList();

            return result;
        }

        public async Task<List<KeyValuePair<string, string>>> ThemeSettingGetPageTitleByUrlHandle(List<string> listUrlHandle)
        {
            if (listUrlHandle == null)
            {
                context.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return null;
            }

            if (listUrlHandle.Count == 0)
            {
                return new List<KeyValuePair<string, string>>();
            }

            var listObjPage = await rpPage.GetByListUrlHandle(extensionReq.StoreId, listUrlHandle);

            if (listObjPage == null || listObjPage.Count == 0)
            {
                return new List<KeyValuePair<string, string>>();
            }

            var result = listObjPage.ToDictionary(a => a.handle, a => a.title).ToList();

            return result;
        }

        public async Task<(List<SimpleListModel> data, long totalrecord)> ThemeSettingGetListBlog(FilterSearchModel model)
        {
            var data = new List<SimpleListModel>();
            if (model == null)
            {
                context.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return (data, 0);
            }

            if (model.Page == null)
            {
                model.Page = new FilterSearchPage { currentPage = 1, pageSize = 10 };
            }
            else if (model.Page.currentPage <= 0)
            {
                model.Page.currentPage = 1;
            }
            else if (model.Page.pageSize <= 0)
            {
                model.Page.currentPage = 10;
            }

            var bizBlog = serviceProvider.GetService<IBlogBusiness>();
            var resultList = await bizBlog.GetBlogList(model);
            if (resultList.data != null)
            {
                data = resultList.data.SimpleListModelToBlogSearchRowModel();
                return (data, resultList.totalrecord);
            }
            return (data, 0);
        }

        public async Task<(List<SimpleListModel> data, long totalrecord)> ThemeSettingGetListPage(FilterSearchModel model)
        {
            var data = new List<SimpleListModel>();
            if (model == null)
            {
                context.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return (data, 0);
            }

            if (model.Page == null)
            {
                model.Page = new FilterSearchPage { currentPage = 1, pageSize = 10 };
            }
            else if (model.Page.currentPage <= 0)
            {
                model.Page.currentPage = 1;
            }
            else if (model.Page.pageSize <= 0)
            {
                model.Page.currentPage = 10;
            }
            var bizPage = serviceProvider.GetService<IPageBusiness>();

            var resultList = await bizPage.GetPageList(model);

            if (resultList.data != null)
            {
                data = resultList.data.SimpleListModelToPageDetailShortModel();
                return (data, resultList.totalrecord);
            }
            return (data, 0);
        }

        public async Task<(List<SimpleListModel> data, long totalrecord)> ThemeSettingGetListCollection(FilterSearchModel model)
        {
            var data = new List<SimpleListModel>();
            long totalrecord = 0;
            if (model == null)
            {
                context.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return (data, totalrecord);
            }
            var objCollection = await rpTheme.ThemeSettingGetListCollection(model);
            if (objCollection.datas != null)
                return (objCollection.datas, objCollection.totalRecord);
            else
                return (data, totalrecord);
        }

        public async Task<ThemeFileModel> EditSettingDataContent(long id, string content)
        {
            if (id <= 0)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var objTheme = await rpTheme.EditSettingDataContent(id, content);
            if (objTheme.HasError)
            {
                context.AddError(objTheme.ErrorCodes[0].ToString(), objTheme.Errors[0].ToString());
                return null;
            }
            return objTheme.Data;
        }

        public async Task<ThemeModel> SelectThemeInstall(long id)
        {
            if (!(await BusinessExtensions.CanExportImport(extensionReq.StoreId, cache, config.ImportExportTime)))
            {
                context.AddError("errors.theme.wait_import", "Vui lòng đợi 10 phút để nhập file giao diện");
                return null;
            }

            if (id <= 0)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }

            var checklimittheme = await GetThemeList();
            if (checklimittheme.CanAddNew == false)
            {
                context.AddError("errors.theme.max_theme", "Bạn không thể có nhiều hơn 8 theme trong cửa hàng của bạn.");
                return null;
            }

            var modelresult = new ThemeModel();
            var _datagettheme = await GetByIdFreeThemes(id);
            var result = await rpTheme.ThemeStoreFree(_datagettheme.name);
            if (result.HasError)
            {
                context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return modelresult;
            }
            var _model = result.Data;

            var linkInstall = await GetThemeZipUrlByThemeCode(_datagettheme.code);
            if (string.IsNullOrEmpty(linkInstall))
            {
                return null;
            }

            await bus.SendMsgAsync(new TaskProcessBaseMsg<ImportThemeMsg>()
            {
                Msg = new ImportThemeMsg()
                {
                    DocumentType = (int)EnumDocumentType.Theme,
                    FileUrl = linkInstall,
                    StoreId = extensionReq.StoreId,
                    FileName = _datagettheme.name,
                    ThemeModel = _model != null ? _model.FreeThemesModelToThemeExportMsg() : null,
                    IsPublishAfterSuccess = false
                }
            });

            var themeObj = await rpTheme.GetThemefileByIdAsync(_model.Id);
            if (themeObj.HasError)
            {
                context.AddError(themeObj.ErrorCodes[0].ToString(), themeObj.Errors[0].ToString());
                return null;
            }
            modelresult = themeObj.Data;
            return modelresult;
        }

        public async Task<List<ThemesFree>> GetFreeThemes()
        {
            var model = new List<ThemesFree>();
            await Task.FromResult(0);
            model = config.ThemesFree;
            return model;
        }

        public async Task<ThemeFileModel> UploadThemeAsset(long themeId, string name, IFormFile file)
        {
            var modelResult = new ThemeFileModel();
            if (themeId <= 0 || string.IsNullOrEmpty(name) || file == null)
            {
                context.AddError("errors.information.no_valid", "Thông tin không hợp lệ.");
                return modelResult;
            }
            var url = string.Empty;

            Stream fileStream = new MemoryStream();
            var ms = new MemoryStream();
            ms.CopyTo(fileStream);
            if (IsImage(file) || IsFont(file))
            {
                file.CopyTo(fileStream);
            }
            else
            {
                using (var reader = new StreamReader(file.OpenReadStream(), new UTF8Encoding(false)))
                {
                    var value = reader.ReadToEnd();
                    fileStream = new MemoryStream(new UTF8Encoding(false).GetBytes(value));

                }
            }


            var _datafile = await file.UploadFileHelper();
            var modeluploadRp = await UploadThemeFile(themeId, FileType.Theme_Assets, name, fileStream);

            var uploadRp = modeluploadRp;

            var modeladdFileRp = await rpTheme.AddFile(uploadRp, null);
            if (modeladdFileRp.HasError)
            {
                context.AddError(modeladdFileRp.ErrorCodes[0].ToString(), modeladdFileRp.Errors[0].ToString());
                return modelResult;
            }
            var addFileRp = modeladdFileRp.Data;

            var rpThemeFile = await rpTheme.AddNewThemeFile(null, new ThemeFileModel()
            {
                File = addFileRp,
                Type = Enum.GetName(typeof(ThemeFileType), ThemeFileType.Asset),
                ThemeId = themeId,
                FileName = name
            });

            if (rpThemeFile.HasError)
                modeladdFileRp.Errors = rpThemeFile.Errors;
            else
            {
                modelResult = rpThemeFile.Data;
                modelResult.File = addFileRp;
            }
            return modelResult;
        }

        public async Task<bool> ValidateUploadThemeAsset(long themeId, string name)
        {
            if (themeId <= 0 || string.IsNullOrEmpty(name))
            {
                context.AddError("errors.information.no_valid", "Thông tin không hợp lệ.");
                return false;
            }

            var filevalide = await rpTheme.ValidateSameName(themeId, name);
            if (filevalide.Errors != null && filevalide.Errors.Count() > 0)
            {
                context.AddError(filevalide.ErrorCodes[0].ToString(), filevalide.Errors[0].ToString());
                return false;
            }
            return filevalide.Data;
        }

        public async Task<FileModel> SettingThemeAsset(long? themeId, string name, int? maxWidth, int? maxHeight, IFormFile file)
        {
            if (!themeId.HasValue
                 || string.IsNullOrEmpty(name)
                 || file == null)
            {
                context.AddError("errors.information.no_valid", "Thông tin không hợp lệ.");
                return null;
            }

            if (maxWidth.HasValue || maxHeight.HasValue)
            {
                maxWidth = maxWidth ?? 2048;
                maxHeight = maxHeight ?? 2048;
            }
            using (var stream = file.OpenReadStream())
            {
                var uploadRp = await UploadThemeFile(
                   themeId.Value, FileType.Theme_Assets, name, stream, maxWidth, maxHeight);
                var result = await SettingThemeAssetUpload(uploadRp);
                return result;
            }
        }

        public async Task<FileModel> UploadThemeFile(
            long themeId, FileType fileType, string fileName, Stream stream, int? maxWidth = null, int? maxHeight = null)
        {
            FileModel newFile;
            if (ThemeUtils.IsNewFileService(themeId, config))
            {
                newFile = await fileBiz.UploadThemeFile(themeId, fileType, fileName, stream, null, false, false, maxWidth, maxHeight);
            }
            else
            {
                var objFileModel = await fileBiz.OldMedia_UploadThemeFile(themeId, fileType, fileName, stream);

                newFile = new FileModel()
                {
                    FileName = objFileModel.FileName,
                    Size = (int)stream.Length,
                    Type = objFileModel.Type,
                    Url = objFileModel.Url
                };
            }
            return newFile;
        }

        #endregion Theme

        #region private

        private ThemeAPIModel ParseDictionaryThemesApi(Dictionary<string, object> dictionaryModel)
        {
            if (dictionaryModel == null || dictionaryModel.Count == 0)
            {
                context.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
                return null;
            }

            ThemeAPIModel resultModel = new ThemeAPIModel();
            var propertyInfos = typeof(ThemeAPIModel).GetProperties();
            foreach (var propertyValuePair in dictionaryModel)
            {
                var colName = propertyInfos.Single(x => x.Name == propertyValuePair.Key);

                if (colName.Name.Equals("name"))
                {
                    resultModel.name = (string)propertyValuePair.Value;
                }
                if (colName.Name.Equals("role"))
                {
                    resultModel.role = (string)propertyValuePair.Value;
                }
                if (colName.Name.Equals("previewable"))
                {
                    resultModel.previewable = (bool)propertyValuePair.Value;
                }
                if (colName.Name.Equals("processing"))
                {
                    resultModel.processing = (bool)propertyValuePair.Value;
                }

                if (colName.Name.Equals("src"))
                {
                    resultModel.src = (string)propertyValuePair.Value;
                }
                if (colName.Name.Equals("created_at"))
                {
                    resultModel.created_at = (DateTime)propertyValuePair.Value;
                }
                if (colName.Name.Equals("updated_at"))
                {
                    resultModel.updated_at = (DateTime)propertyValuePair.Value;
                }
            }

            return resultModel;
        }

        private bool IsImage(IFormFile file)
        {
            var streamData = new MemoryStream();
            file.CopyTo(streamData);

            if (file.ContentType.Contains("image"))
                return true;
            string[] formats = new string[] { ".jpg", ".png", ".gif", ".jpeg" };
            return formats.Any(item => file.FileName.EndsWith(item, StringComparison.OrdinalIgnoreCase));
        }

        private bool IsFont(IFormFile file)
        {
            var streamData = new MemoryStream();
            file.CopyTo(streamData);

            string[] formats = new string[] { ".eot", ".woff", ".woff2", ".ttf", ".svg" };
            return formats.Any(item => file.FileName.EndsWith(item, StringComparison.OrdinalIgnoreCase));
        }

        private async Task<ThemesFree> GetByIdFreeThemes(long id)
        {
            if (id <= 0)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var model = new ThemesFree();
            await Task.FromResult(0);
            var data = config.ThemesFree;
            model = data.Find(a => a.id == id);
            return model;
        }

        private static async Task<string> GetFileContent(IFileBusiness fileBiz, long themeId, string url, bool isCheckIp, AppConfig config)
        {
            if (isCheckIp)
                await _checkIp(url);
            var path = QueryHelpers.AddQueryString(url, "nocache", "true");
            if (_isNewFileService(themeId, config))
            {
                return await fileBiz.GetContent(path);
            }
            else
            {
                return await fileBiz.GetStaticContent(path);
            }
        }

        private static async Task _checkIp(string url)
        {
            var uri = new Uri(url);
            var ipAddress = await Dns.GetHostAddressesAsync(uri.Host);

            if (ipAddress.Any(host => host.ToString() == "localhost" || host.ToString() == "127.0.0.1"))
                throw new Exception(HttpStatusCode.NotFound.ToString());
        }

        private static bool _isNewFileService(long themeId, AppConfig config)
        {
            var result = themeId > config.FileServiceFromThemeId;
            return result;
        }

        private async Task<FileModel> SettingThemeAssetUpload(FileModel assetFile)
        {
            var obJthemes = await rpTheme.SettingThemeAssetUpload(assetFile);
            if (obJthemes.HasError)
            {
                context.AddError(obJthemes.ErrorCodes[0].ToString(), obJthemes.Errors[0].ToString());
                return null;
            }
            return obJthemes.Data;
        }

        #endregion private

        public async Task<string> InstallTheme(string name, long timestamp, string hash)
        {
            var result = await rpTheme.InstallTheme(name, timestamp, hash);
            if (result.HasError)
            {
                context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return null;
            }
            return result.Data;
        }

        public async Task<List<ThemeEditorDropdownModel>> GetThemeEditorDropdown()
        {
            var modelData = new List<ThemeEditorDropdownModel>();
            var result = await rpTheme.GetThemeEditorDropdown();
            if (result.HasError)
            {
                context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return null;
            }
            modelData = result.Data;
            var _bizBlog = serviceProvider.GetService<IBlogBusiness>();
            var _bizArticle = serviceProvider.GetService<IArticleBusiness>();

            var objBlog = await _bizBlog.GetBlogFirstByHandle();
            if (objBlog != null)
            {
                modelData.Add(new ThemeEditorDropdownModel()
                {
                    title = "Chi tiết blog",
                    code = "blog_detail",
                    url = $"/blogs/{objBlog.UrlHandle}"
                });
            }

            var objtArticle = await _bizArticle.GetArticleFirstByHandle();
            if (objtArticle != null)
            {
                modelData.Add(new ThemeEditorDropdownModel()
                {
                    title = "Chi tiết bài viết",
                    code = "articles_detail",
                    url = $"/articles/{objtArticle.UrlHandle}"
                });
            }
            return modelData;
        }

        public async Task<string> GetThemeZipUrlByThemeCode(string code)
        {
            if (string.IsNullOrWhiteSpace(code))
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var result = await rpTheme.GetThemeZipUrlByThemeCode(code);
            if (result.HasError)
            {
                context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return null;
            }
            return result.Data;
        }

        public async Task<string> GetExportThemeUrl(string key)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var result = await rpTheme.GetExportThemeUrl(key);
            if (result.HasError)
            {
                context.AddError(result.ErrorCodes[0].ToString(), result.Errors[0].ToString());
                return null;
            }
            return result.Data;
        }

        public async Task<List<string>> GetArticleTemplates()
        {
            return await rpTheme.GetTemplateArticles();
        }
    }
}