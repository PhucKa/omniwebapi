﻿using Haravan.Libs.Abstractions;
using Haravan.Web.Api.BusinessObjects.Mappers;
using Haravan.Web.Api.BusinessObjects.Models.Google;
using Haravan.Web.Api.Repository;
using System;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public class GoogleSiteVerificationBusiness : IGoogleSiteVerificationBusiness
    {
        private readonly IRequestContext requestContext;
        private readonly IMGGoogleSiteVerificationRepository rpMGGoogleSiteVerification;
        private readonly IExtensionPerRequest ExtensionReq;
        private readonly IEBSComMessageRepository rpCom;

        public GoogleSiteVerificationBusiness(
                IRequestContext requestContext,
                IMGGoogleSiteVerificationRepository rpMGGoogleSiteVerification,
                IExtensionPerRequest ExtensionReq,
                IEBSComMessageRepository rpCom
            )
        {
            this.ExtensionReq = ExtensionReq;
            this.requestContext = requestContext;
            this.rpMGGoogleSiteVerification = rpMGGoogleSiteVerification;
            this.rpCom = rpCom;
        }

        #region public

        public async Task AddApiAsync(GoogleSiteVerificationDetailApiModel model)
        {
            var newdata = ValidateGoogleSiteVerification(model);
            if (newdata == null) return;
            await rpMGGoogleSiteVerification.DeleteManyAsync(ExtensionReq.StoreId);

            var modeldetail = newdata.ToModel();
            modeldetail.storeid = ExtensionReq.StoreId;
            modeldetail.created_at = DateTime.UtcNow;
            modeldetail.updated_at = DateTime.UtcNow;
            await rpMGGoogleSiteVerification.SetAsync(modeldetail);           
            await rpCom.FireStoreDataChange(ExtensionReq.StoreId);
        }

        public async Task DeleteManyAsync()
        {
            await rpMGGoogleSiteVerification.DeleteManyAsync(ExtensionReq.OrgId);
            await rpCom.FireStoreDataChange(ExtensionReq.StoreId);
        }
        
        #endregion public

        #region private

        private GoogleSiteVerificationDetailApiModel ValidateGoogleSiteVerification(GoogleSiteVerificationDetailApiModel model)
        {
            if (model == null)
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ.");
                return null;
            }
            if (string.IsNullOrEmpty(model.merchant_id))
            {
                requestContext.AddError("errors.merchant_id.not_empty", "Merchant Id không được bỏ trống");
                return null;
            }
            if (string.IsNullOrEmpty(model.verification_code))
            {
                requestContext.AddError("errors.verification_code.not_empty", "Verification code không được bỏ trống");
                return null;
            }
            return model;
        }

        #endregion private
    }
}