﻿using BHN.SharedObject.EBSMessage;
using Haravan.Caching;
using Haravan.Libs.Abstractions;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Configs;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.Repository;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Options;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public class StoreBusiness : IStoreBusiness
    {
        private readonly long _storeId = 0;
        private readonly AppConfig config;
        private readonly ICache cache;
        private readonly ISysDomainRepository rpSysDomain;
        private readonly ISysStoreRepository rpStore;
        private readonly IServiceProvider serviceProvider;
        private readonly IStoreDataUserRepository rpStoreData;
        private readonly IShopRepository _rpShop;
        private readonly IRequestContext _context;
        public StoreBusiness(IServiceProvider serviceProvider,
            IOptions<AppConfig> config, ICache cache,
            ISysDomainRepository rpSysDomain,
            IStoreDataUserRepository rpStoreData,
            IShopRepository rpShop,
            IRequestContext context,
            ISysStoreRepository rpStore)
        {
            this.config = config.Value;
            this.cache = cache;
            this.rpStoreData = rpStoreData;
            this.rpSysDomain = rpSysDomain;
            this._rpShop = rpShop;
            this.rpStore = rpStore;
            this._context = context;
            this.serviceProvider = serviceProvider;
        }
        public async Task<bool> CreateShop(CreateShopModel model)
        {
            if (model == null) return false;
            var resultTask = await _rpShop.CreateShop(model);
            if(resultTask.HasError)
            {
                _context.AddError(resultTask.ErrorCodes[0].ToString(),resultTask.Errors[0].ToString());
                return false;
            }
            return resultTask.Data;
        }

        public async Task<bool> AddSaleChannel(string channel)
        {
            if (channel == null) return false;
            var resultTask = await _rpShop.ActiveWebChannel(channel);
            if (resultTask.HasError)
            {
                _context.AddError(resultTask.ErrorCodes[0].ToString(), resultTask.Errors[0].ToString());
                return false;
            }
            return resultTask.Data;
        }



        //public async Task<StoreData> GetStoreData(CancellationToken cctoken = default(CancellationToken))
        //{
        //    return await GetStoreData(_storeId, cctoken);
        //}

        //public async Task<StoreData> GetStoreData(long storeId, CancellationToken cctoken)
        //{
        //    var key = $"{ShopDefaultData.DefaultPrefixEcomCache}{CacheKeys.GetStoreSellerData(storeId)}";
        //    var cacheKey = CacheKey.Create(new DistributedCacheEntryOptions() { SlidingExpiration = new TimeSpan(config.SessionTimeout, 1, 0) }, key);

        //    return await cache.GetAsync<StoreData>(cacheKey, async (token) =>
        //    {
        //        return await getStoreData(storeId, token);
        //    }, cctoken);
        //}

        //private async Task<StoreData> getStoreData(long storeId, CancellationToken cctoken = default(CancellationToken))
        //{
        //    var data = new StoreData();
        //    data = await rpStoreData.GetStoreData();
        //    return data;
        //}

        //public async Task<string> GetStoreDomain( bool onlyDomain = false)
        //{
        //    var data = new StoreData();
        //    data = await rpStoreData.GetStoreData();
        //    var result = data.SellerDomain;
        //    result = result.EndsWith("/") ? result : (result + "/");

        //    if (onlyDomain)
        //        result = result
        //            .Replace("https://", "")
        //            .Replace("http://", "")
        //            .Replace("/admin/", "");

        //    return result;
        //}
        //public async Task<string> GetStoreSubDomain()
        //{
        //    var data = new StoreData();
        //    data = await rpStoreData.GetStoreData();
        //    return data.SubDomain;
        //}
    }
}