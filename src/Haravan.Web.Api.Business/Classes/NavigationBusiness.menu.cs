﻿using BHN.SharedObject.EBSMessage;
using Haravan.Web.Api.BusinessObjects.Enums;
using Haravan.Web.Api.BusinessObjects.Mappers;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public partial class NavigationBusiness : INavigationBusiness
    {
        public async Task<long> AddFromWebChannel(NavigationDetailModelPath modelpath)
        {
            var _storeId = ExtensionReq.OrgId;
            var model = modelpath.ToNavigationDetailModel();
            await Validate_NavigationDetailModel(model, _storeId);
            if (requestContext.IsError)
                return -1;

            var attemptHandle = !string.IsNullOrEmpty(model.LinkListHandle) ? model.LinkListHandle
                                                                            : model.LinkListName;

            var bizKeyword = serviceProvider.GetService<IKeywordBusiness>();

            model.LinkListHandle = await bizKeyword.ProcessingKeyHandle(
                                                             _storeId,
                                                             (int)EnumDocumentType.LinkList
                                                            , null
                                                            , attemptHandle);
            var currentModel = model.MGRawLinkListToNavigationDetailModel();
            currentModel.StoreId = _storeId;
            currentModel.TreeHandle = currentModel.LinkListHandle;
            currentModel.CreatedDate = DateTime.UtcNow;
            currentModel.CreatedUser = ExtensionReq.UserId;
            currentModel.UpdatedDate = DateTime.UtcNow;
            currentModel.UpdatedUser = ExtensionReq.UserId;

            var currentLinkList = await rpRawLinkList.AddAsync(currentModel);

            var lstDelete = modelpath.LinkListFields.FindAll(m => m.IsDeleted == true);
            var linklistpath = modelpath.LinkListFields.Except(lstDelete).ToList();
            foreach (var item in lstDelete)
            {
                await priDeleteLinkListFieldDetail(item.Id);
            }
            var lstrebuild = lstDelete.Select(m => m.LinkListId).Distinct().ToList();
            foreach (var m in lstrebuild)
            {
                await priRebuildLinkList(m);
            }

            var lstCheck = await priProcessBuildLinkListTree(linklistpath, currentLinkList);

            await priRebuildLinkList(currentLinkList._id);
            foreach (var item in lstCheck)
            {
                foreach (var sub in item.lstTrackProcessLinkList)
                {
                    await priRebuildLinkList(sub.idlinklist);
                    var haschild = await GetListLinkFieldDetailOfLinkListId(sub.idlinklist, _storeId);
                    if (haschild == null || !haschild.Any())
                    {
                        await rpRawLinkList.DeleteByIdAsync(_storeId, sub.idlinklist);
                    }
                }
            }
            return currentLinkList._id;
        }

        public async Task UpdateFromWebChannel(NavigationDetailModelPath modelpath)
        {
            var _storeId = ExtensionReq.OrgId;
            var model = modelpath.ToNavigationDetailModel();
            if (!string.IsNullOrWhiteSpace(model.LinkListHandle)) model.LinkListHandle = model.LinkListHandle.Trim();
            var currentLinkList = await Validate_NavigationDetailModelPath(modelpath, _storeId, true);
            if (requestContext.IsError)
                return;
            var linkListTreeHandle = currentLinkList.TreeHandle;
            if (currentLinkList.IsSystemLink)
            {
                var _modelLinkList = model.MGRawLinkListToNavigationDetailModel();
                _modelLinkList.UpdatedDate = DateTime.UtcNow;
                _modelLinkList.UpdatedUser = ExtensionReq.UserId;
                await rpRawLinkList.UpdateAsync(_modelLinkList, k => k.LinkListName
                                                                , k => k.UpdatedDate
                                                                , k => k.UpdatedUser
                                                              );
            }
            else
            {
                var _modelLinkList = model.MGRawLinkListToNavigationDetailModel();
                var oldLinkHandle = currentLinkList.LinkListHandle;
                var attemptHandle = !string.IsNullOrEmpty(_modelLinkList.LinkListHandle)
                                            ? _modelLinkList.LinkListHandle
                                            : _modelLinkList.LinkListName;

                var bizKeyword = serviceProvider.GetService<IKeywordBusiness>();
                _modelLinkList.LinkListHandle = await bizKeyword.ProcessingKeyHandle(
                                                                _storeId,
                                                                (int)EnumDocumentType.LinkList
                                                                , oldLinkHandle
                                                                , attemptHandle);

                _modelLinkList.UpdatedDate = DateTime.UtcNow;
                _modelLinkList.UpdatedUser = ExtensionReq.UserId;
                await rpRawLinkList.UpdateAsync(_modelLinkList, k => k.LinkListName
                                                                , k => k.UpdatedDate
                                                                , k => k.UpdatedUser
                                                                , k => k.LinkListHandle
                                                              );
            }

            var lstDelete = modelpath.LinkListFields.FindAll(m => m.IsDeleted == true);
            var linklistpath = modelpath.LinkListFields.Except(lstDelete).ToList();
            foreach (var item in lstDelete)
            {
                await priDeleteLinkListFieldDetail(item.Id);
            }
            var lstrebuild = lstDelete.Select(m => m.LinkListId).Distinct().ToList();
            foreach (var m in lstrebuild)
            {
                await priRebuildLinkList(m);
            }

            var lstCheck = await priProcessBuildLinkListTree(linklistpath, currentLinkList);

            await priRebuildLinkList(currentLinkList._id);
            var lstLinkList = await rpRawLinkList.GetAllLinkList(_storeId);

            foreach (var sub in lstLinkList)
            {
                var haschild = await GetListLinkFieldDetailOfLinkListId(sub._id, _storeId);
                if (haschild == null || !haschild.Any() || haschild.First().TreeHandle == sub.TreeHandle)
                {
                    if (!sub.IsSystemLink)
                    {
                        await rpRawLinkList.DeleteByIdAsync(_storeId, sub._id);
                        await rpLinkList.DeleteAsync(_storeId, sub._id);
                    }
                }
                else
                {
                    await priRebuildLinkList(sub._id);
                }
            }

            await _bus.FireStoreDataChangeLinkList(ExtensionReq.OrgId);
        }

        private async Task<List<ListLevelLinkListTrackProcess>> priProcessBuildLinkListTree(List<LinkListFieldsDetailModelPath> linklistpath, MGRawLinkList currentLinkList)
        {
            var lstCheck = new List<ListLevelLinkListTrackProcess>();
            var runs = linklistpath.Count;
            var i = 0;
            var _storeId = ExtensionReq.OrgId;
            while (i < runs)
            {
                var current = linklistpath[i];
                var pathlengh = current.Path.Count;
                var currentprocess = lstCheck.Find(m => m.level == pathlengh);
                if (currentprocess == null)
                {
                    currentprocess = new ListLevelLinkListTrackProcess();
                    currentprocess.level = pathlengh;
                    lstCheck.Add(currentprocess);
                }
                if (pathlengh == 0)
                {
                    var objCurrentLinkListField = current.ToLinkListFieldsDetailModel();
                    var change = await ProcessingLinkListFieldDetailTree(_storeId, objCurrentLinkListField, currentLinkList._id, currentLinkList.TreeHandle);
                    currentprocess.lstTrackProcessLinkListField.Add(change);
                }
                else if (pathlengh >= 1)
                {
                    var objCurrentLinkListField = current.ToLinkListFieldsDetailModel();
                    var previousprocess = lstCheck.Find(m => m.level == pathlengh - 1);
                    var latestL0 = previousprocess.lstTrackProcessLinkListField.LastOrDefault();
                    var checkL0 = previousprocess.lstTrackProcessLinkList.Find(m => m.idlinklistfield == latestL0._id);
                    if (checkL0 == null)
                    {
                        var retp0 = await priAddLinkListFromLinkListField(latestL0);
                        previousprocess.lstTrackProcessLinkList.Add(new LinkListTrackProcess()
                        {
                            idlinklist = retp0._id,
                            idlinklistfield = latestL0._id,
                            objlinklistfield = latestL0,
                            objlinklist = retp0
                        }
                            );
                        var l1 = await ProcessingLinkListFieldDetailTree(_storeId, objCurrentLinkListField, retp0._id, retp0.TreeHandle);
                        currentprocess.lstTrackProcessLinkListField.Add(l1);
                    }
                    else
                    {
                        var l1 = await ProcessingLinkListFieldDetailTree(_storeId, objCurrentLinkListField, checkL0.idlinklist, checkL0.objlinklistfield.TreeHandle);
                        currentprocess.lstTrackProcessLinkListField.Add(l1);
                    }
                }

                i = i + 1;
            }
            return lstCheck;
        }

        private async Task<MGRawLinkList> priAddLinkListFromLinkListField(MGRawLinkListFields field)
        {
            var parentLinkList = await rpRawLinkList.GetById(ExtensionReq.StoreId, field.LinkListId);
            var p0 = new MGRawLinkList();
            if (parentLinkList != null)
            {
                p0.LinkListName = parentLinkList.LinkListName + " » " + field.LinkListFieldName;
            }
            else
            {
                p0.LinkListName = field.LinkListFieldName;
            }
            var _storeId = ExtensionReq.OrgId;
            p0.StoreId = _storeId;
            p0.LinkListHandle = field.LinkListFieldHandle;
            p0.TreeHandle = field.TreeHandle;
            p0.IsSystemLink = false;
            p0.CreatedDate = DateTime.UtcNow;
            p0.CreatedUser = ExtensionReq.UserId;
            p0.UpdatedDate = DateTime.UtcNow;
            p0.UpdatedUser = ExtensionReq.UserId;
            var retp0 = await rpRawLinkList.AddAsync(p0);
            return retp0;
        }

        private async Task<MGRawLinkListFields> ProcessingLinkListFieldDetailTree(long storeId, LinkListFieldsDetailModel linkdetail
                                                , long linkListId
                                                , string linkListTreeHandle
                                                , string oldLinkListName = "")
        {
            var link = linkdetail.ToMGRawLinkListFields();
            link.StoreId = storeId;
            var linkOrder = await GetLastLinkOrder(linkListId);

            #region Xử lý chung cho thao tác INSERT - UPDATE

            if (link.IsDeleted == false)
            {
                if (link.LinkFieldsId == (int)LinkFieldType.WebAddress)
                    link.TextValue = await ProcessWebAdress(link.TextValue);
                else
                {
                    link.TextValue = await BuildUrlForLink(link.LinkFieldsId
                                                    , link.RefId ?? 0
                                                    , link.Tags);
                }
            }

            #endregion Xử lý chung cho thao tác INSERT - UPDATE

            // If INSERT
            if (link._id <= 0)
            {
                // Handle link cho phép trùng nên không cần dùng hàm Process.
                link.LinkListFieldHandle = Guid.NewGuid().ToString();

                if (link.LinkListId <= 0)
                    link.LinkListId = linkListId;

                if (link.LinkListFieldOrder <= 0)
                {
                    link.LinkListFieldOrder = ++linkOrder;
                }

                link.TreeHandle = linkListTreeHandle + "#" + link.LinkListFieldHandle;
                //var addLink = Mapper.Map<MGRawLinkListFields>(link);
                link.CreatedDate = DateTime.UtcNow;
                link.CreatedUser = ExtensionReq.UserId;
                link.UpdatedDate = DateTime.UtcNow;
                link.UpdatedUser = ExtensionReq.UserId;
                var ret = await rpRawLinkListField.AddAsync(link);
                return ret;
            }
            // ELSE If UPDATE
            else
            {
                var oldLink = await rpRawLinkListField.GetById(ExtensionReq.StoreId, link._id);
                // Check if:
                // 1. update and link Name change, I update Dropdown Name and Handle.
                // 2. or link is deleted, I also delete dropdown.

                if (oldLink == null)
                {
                    // không tìm thấy link, bỏ qua
                    return null;
                }

                var dropdownLinkList = await rpRawLinkList.GetLinkListByTreeHandle(oldLink.TreeHandle, storeId);
                if (dropdownLinkList != null && !dropdownLinkList.IsSystemLink)
                {
                    if (link.IsDeleted)
                    {
                        await DeleteLinkList(dropdownLinkList._id);
                    }
                    else
                    {
                        if (!oldLink.LinkListFieldName.Equals(link.LinkListFieldName))
                        //|| !oldLink.TreeHandle.Equals(link.TreeHandle))
                        {
                            await UpdateDropDown_ByParentLink(oldLink.TreeHandle
                                 , link.TreeHandle
                                 , oldLink.LinkListFieldName
                                 , link.LinkListFieldName
                                 , link.LinkListFieldHandle
                                 , oldLinkListName);
                        }
                    }
                }

                link.TreeHandle = linkListTreeHandle + "#" + oldLink.LinkListFieldHandle;
                link.UpdatedDate = DateTime.UtcNow;
                link.UpdatedUser = ExtensionReq.UserId;
                link.LinkListId = linkListId;
                await rpRawLinkListField.UpdateAsync(link, k => k.LinkFieldsId
                    , k => k.IsDeleted
                    , k => k.LinkListFieldName
                    , k => k.LinkListFieldOrder
                    , k => k.RefId
                    , k => k.TextValue
                    , k => k.UpdatedDate
                    , k => k.UpdatedUser
                    , k => k.Tags
                    , k => k.LinkListId
                    , k => k.TreeHandle
                    );
                var ret = await rpRawLinkListField.GetById(storeId, link._id);
                return ret;
            }
        }

        private async Task priDeleteLinkListFieldDetail(long linkfieldid)
        {
            var linkfield = await rpRawLinkListField.GetById(ExtensionReq.StoreId, linkfieldid);
            if (linkfield == null)
            {
                return;
            }
            var dropdownLinkList = await rpRawLinkList.GetLinkListByTreeHandle(linkfield.TreeHandle, ExtensionReq.StoreId);
            if (dropdownLinkList != null && !dropdownLinkList.IsSystemLink)
            {
                await DeleteLinkList(dropdownLinkList._id);
            }

            linkfield.UpdatedDate = DateTime.UtcNow;
            linkfield.UpdatedUser = ExtensionReq.UserId;
            linkfield.IsDeleted = true;
            await rpRawLinkListField.UpdateAsync(linkfield
                , k => k.IsDeleted
                , k => k.UpdatedDate
                , k => k.UpdatedUser

                );
        }

        private async Task<bool> DeleteLinkListFields_ByLinkListId(long linkListId)
        {
            if (linkListId <= 0)
            {
                return false;
            }
            var _storeId = ExtensionReq.OrgId;
            var linkListFieldsToDelete = await rpRawLinkListField.GetByLinkListId(linkListId, _storeId);
            if (linkListFieldsToDelete.Any())
            {
                foreach (var link in linkListFieldsToDelete)
                {
                    // delete dropdown of link
                    var dropdownLinkList = await rpRawLinkList.GetLinkListByTreeHandle(link.TreeHandle, _storeId);

                    if (dropdownLinkList != null && dropdownLinkList._id != linkListId)
                    {
                        await DeleteLinkList(dropdownLinkList._id);
                    }
                };

                //linkListFieldsToDelete.ForEach(async link =>
                foreach (var link in linkListFieldsToDelete)
                {
                    link.IsDeleted = true;
                    link.UpdatedDate = DateTime.UtcNow;
                    link.UpdatedUser = ExtensionReq.UserId;
                    await rpRawLinkListField.UpdateAsync(link, p => p.IsDeleted
                                                                 , p => p.UpdatedDate
                                                                 , p => p.UpdatedUser
                                                                 );
                };
            }
            return true;
        }

        public async Task<NavigationDetailModelPath> GetDetailLinkListWithPath(long linkListId)
        {
            var resultModel = new NavigationDetailModelPath();

            if (linkListId <= 0)
            {
                requestContext.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
            }
            else
            {
                var dataLinkList = await rpRawLinkList.GetById(ExtensionReq.StoreId, linkListId);
                resultModel = dataLinkList.DetailNewToNavigationDetailModel();
                if (resultModel == null || resultModel.IsDeleted)
                {
                    requestContext.AddError("errors.navigation.list_link.not_exist", "Danh sách link không tồn tại");
                }
                else
                {
                    var listbuil = await GetListLinkFieldDetailWithPath(linkListId);
                    resultModel.LinkListFields = priRefinePath(listbuil);
                }
            }
            return resultModel;
        }

        private List<LinkListFieldsDetailModelPath> priRefinePath(List<LinkListFieldsDetailModelPath> lst)
        {
            var ret = new List<LinkListFieldsDetailModelPath>();
            var parent = lst[0];
            var parentId = lst[0].Id;
            foreach (var item in lst)
            {
                if (item.Path != null && item.Path.Count >= 1)
                {
                    var subs = item.Path.ToList();
                    subs.RemoveAll(m => m == parentId);
                    item.Path = subs;
                    var check = ret.Find(m => m.Id == item.Id);
                    if (check == null)
                    {
                        ret.Add(item);
                    }
                }
            }

            return ret;
        }

        private async Task<List<LinkListFieldsDetailModelPath>> GetListLinkFieldDetailWithPath(long linkListId)
        {
            var resultModel = new NavigationDetailModel(); 
            var _storeId = ExtensionReq.OrgId;
            var resultModelDetail = new List<LinkListFieldsDetailModelPath>();

            if (linkListId <= 0)
            {
                requestContext.AddError("errors.information.no_valid", "Thông tin không hợp lệ");
            }
            else
            {
                var dataLinkList = await rpRawLinkList.GetById(ExtensionReq.StoreId, linkListId);
                resultModel = dataLinkList.ToNavigationDetailModel();
                if (resultModel == null || resultModel.IsDeleted)
                {
                    requestContext.AddError("errors.navigation.list_link.not_exist", "Danh sách link không tồn tại");
                }
                else
                {
                    resultModel.LinkListFields = await GetListLinkFieldDetailOfLinkListId(resultModel.Id, _storeId);
                }
            }

            // main menu
            var lstPath = new List<LinkListFieldsDetailModelPath>();
            var currentlinkfield = new LinkListFieldsDetailModelPath()
            {
                Id = resultModel.Id,
                LinkListFieldName = resultModel.LinkListName,
                LinkListFieldHandle = resultModel.LinkListHandle,
                Path = new List<long>()
            };
            lstPath.Add(currentlinkfield);

            if (resultModel.LinkListFields.Count() > 0)
            {
                List<MGRawLinkListFields> allField = await rpRawLinkListField.GetByStoreId(_storeId);
                List<MGRawLinkList> allLinkList = await rpRawLinkList.GetByStoreId(_storeId);
                foreach (var childlinkfield in resultModel.LinkListFields)
                {
                    lstPath = await priBuildListLinkFieldDetailWithPath(new List<long>() { resultModel.Id }, childlinkfield.Id, lstPath, allField, allLinkList);
                }
            }
            resultModelDetail = lstPath;
            return resultModelDetail;
        }

        private async Task<List<LinkListFieldsDetailModelPath>> priBuildListLinkFieldDetailWithPath(List<long> pathid, long rawlinklistfieldid, List<LinkListFieldsDetailModelPath> lst, List<MGRawLinkListFields> allField, List<MGRawLinkList> allLinkList)
        {
            if (lst == null) lst = new List<LinkListFieldsDetailModelPath>();
            var rawchildfield = allField.Find(m => m._id == rawlinklistfieldid && !m.IsDeleted);
            var rawlistnephew = new List<MGRawLinkListFields>();
            var objLinkList = allLinkList.Find(m => m.TreeHandle == rawchildfield.TreeHandle && !m.IsDeleted);
            if (objLinkList != null)
            {
                rawlistnephew = await rpRawLinkListField.GetByLinkListId(objLinkList._id, ExtensionReq.StoreId);
            }
            var _storeId = ExtensionReq.OrgId;
            var cur = await priGetLinkFieldDetailByFieldId(rawlinklistfieldid, _storeId);
            var curPath = cur.ToLinkListFieldsDetailModelPath();
            if (curPath.Path == null) curPath.Path = new List<long>();
            curPath.Path.AddRange(pathid);

            lst.Add(curPath);
            var lstpath = new List<long>(pathid);
            lstpath.Add(rawlinklistfieldid);
            foreach (var sub in rawlistnephew)
            {
                if (sub._id != rawlinklistfieldid)
                {
                    lst = await priBuildListLinkFieldDetailWithPath(lstpath, sub._id, lst, allField, allLinkList);
                }
            }
            return lst;
        }
    }
}