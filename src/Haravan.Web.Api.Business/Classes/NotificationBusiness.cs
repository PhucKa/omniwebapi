﻿using Haravan.Web.Api.BusinessObjects.Enums;
using Haravan.Web.Api.BusinessObjects.Models.Notification;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public class NotificationBusiness : INotificationBusiness
    {
        private readonly IServiceProvider serviceProvider;

        public NotificationBusiness(
                IServiceProvider serviceProvider
            )
        {
            this.serviceProvider = serviceProvider;
        }

        #region public

        public async Task<NotificationModel> GetListNotification(int View_type)
        {
            var model = new NotificationModel();
            switch (View_type)
            {
                case (int)NotificationType.Page:
                case (int)NotificationType.Blog:
                case (int)NotificationType.Comment:
                case (int)NotificationType.Article:
                case (int)NotificationType.Theme:
                    var resultmodel = await CheckPasswordProtected(View_type);
                    model = resultmodel != null ? resultmodel : null;
                    break;
            }
            return model;
        }

        #endregion public

        #region public

        public async Task<NotificationModel> CheckPasswordProtected(int View_type)
        {
            var model = new NotificationModel();
            var modelsting = serviceProvider.GetService<IGeneralSettingBusiness>();
            var result = await modelsting.GetOnlineStoreSetting();
            if (result.PasswordProtect == true)
            {
                model.View_type = View_type;
                model.Title = "Cửa hàng trực tuyến được bảo vệ bằng mật khẩu";
                model.Content = "Chỉ khách truy cập có mật khẩu mới có thể truy cập cửa hàng trực tuyến của bạn.";
                model.Button = "Vô hiệu hóa mật khẩu";
                model.Action = "/settings/online_store";
                model.Alert = "warning";
            }
            return model;
        }

        #endregion public
    }
}