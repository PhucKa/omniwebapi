﻿using BHN.SharedObject.APIDataModel;
using BHN.SharedObject.EBSMessage;
using Haravan.Libs.Abstractions;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Mappers;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using Haravan.Web.Api.Repository;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public class ScriptTagsBusiness : IScriptTagsBusiness
    {
        private const int DefaultTotalScriptTag = 200;
        private const string DefaultScriptName = "noname";
        private readonly IRequestContext context;
        private readonly IMGScriptTagsRepository rpScriptTag;
        private readonly IServiceProvider serviceProvider;
        private readonly IEBSComMessageRepository bus;
        private readonly IExtensionPerRequest extensionReq;

        public ScriptTagsBusiness(
            IMGScriptTagsRepository rpScriptTag,
            IRequestContext context,
            IExtensionPerRequest extensionReq,
            IServiceProvider serviceProvider,
            IEBSComMessageRepository bus
            )
        {
            this.rpScriptTag = rpScriptTag;
            this.context = context;
            this.serviceProvider = serviceProvider;
            this.bus = bus;
            this.extensionReq = extensionReq;
        }

        public async Task<ScriptTagAPIModel> GetDetail(long id)
        {
            var _storeId = extensionReq.StoreId;
            var _userId = extensionReq.UserId;
            var objScriptTag = await rpScriptTag.GetDetail(_storeId, _userId, id);
            if (objScriptTag == null)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            return objScriptTag.ToApiModel();
        }

        public async Task<bool> Delete(long id)
        {
            var _storeId = extensionReq.StoreId;
            var _userId = extensionReq.UserId;

            var objScriptTag = await rpScriptTag.GetDetail(_storeId, _userId, id);
            if (objScriptTag == null)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return false;
            }
            await rpScriptTag.DeleteAsync(objScriptTag);
            await bus.FireStoreDataChange(_storeId);
            return true;
        }

        public async Task<List<ScriptTagAPIModel>> GetList(
            int page, int limit, long since_id, DateTime? created_at_min,
            DateTime? created_at_max, DateTime? updated_at_min, DateTime? updated_at_max, string src
            )
        {
            var _storeId = extensionReq.StoreId;
            var _userId = extensionReq.UserId;
            var result = await rpScriptTag.GetList(
                    _storeId, _userId, page, limit, since_id, created_at_min,
                    created_at_max, updated_at_min, updated_at_max, src
                );
            return result.ToListApiModel();
        }

        public async Task<long> Count(
            long since_id, DateTime? created_at_min,
            DateTime? created_at_max, DateTime? updated_at_min, DateTime? updated_at_max, string src
            )
        {
            var _storeId = extensionReq.StoreId;
            var _userId = extensionReq.UserId;
            return await rpScriptTag.Count(
                    _storeId, _userId, since_id, created_at_min,
                    created_at_max, updated_at_min, updated_at_max, src
                );
        }

        public async Task<ScriptTagAPIModel> Add(ScriptTagAPIModel model)
        {
            if (model == null)
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }

            if (string.IsNullOrWhiteSpace(model.@event))
            {
                context.AddError("errors.script.event.is_empty", "Tên event không được bỏ trống");
                return null;
            }

            model.@event = model.@event.Trim().ToLower();

            if (!model.@event.Equals("onload"))
            {
                context.AddError("errors.script.event.incorrect", "Tên event không chính xác");
                return null;
            }

            var _storeId = extensionReq.StoreId;
            var _userId = extensionReq.UserId;

            var total = await rpScriptTag.CountByStoreId(_storeId);
            if (total >= DefaultTotalScriptTag)
            {
                context.AddError($"errors.script.event.max_limit_reached__{DefaultTotalScriptTag}__", $"Chỉ có tối đa {DefaultTotalScriptTag} scripttag cho 1 shop");
                return null;
            }

            int displayScope = 0;

            if (string.IsNullOrWhiteSpace(model.display_scope))
            {
                displayScope = (int)ScriptTagDisplayScope.all;
            }
            else
            {
                var tmpScope = model.display_scope.Trim().ToLower();
                switch (tmpScope)
                {
                    case "all":
                        displayScope = (int)ScriptTagDisplayScope.all;
                        break;

                    case "online_store":
                        displayScope = (int)ScriptTagDisplayScope.online_store;
                        break;

                    case "order_status":
                        displayScope = (int)ScriptTagDisplayScope.order_status;
                        break;

                    case "checkout":
                        displayScope = (int)ScriptTagDisplayScope.checkout;
                        break;

                    default:
                        displayScope = (int)ScriptTagDisplayScope.all;
                        break;
                }
            }

            var isSrc = false;

            if (!string.IsNullOrEmpty(model.src))
            {
                model.src = model.src.Trim();
                isSrc = true;
            }
            if (isSrc)
            {
                if (model.src.Length > DefaultData.DefaultMaxScripttagSrcLength)
                {
                    context.AddError($"errors.script.link.max_limit_reached__{DefaultData.DefaultMaxScripttagSrcLength}__", $"Đường dẫn không được vượt quá {DefaultData.DefaultMaxScripttagSrcLength} ký tự");
                    return null;
                }
                var objExist = await rpScriptTag.GetBy(model.src, model.@event, _storeId, _userId);
                if (objExist != null)
                {
                    context.AddError("errors.script.has_link_even", "Đã tồn tại scripttag với đường dẫn và event này.");
                    return null;
                }
                var objScriptTags = new MGScriptTagsModel()
                {
                    appid = _userId,
                    created_at = DateTime.UtcNow,
                    created_user = _userId,
                    display_scope = displayScope,
                    evenname = model.@event,
                    scriptname = DefaultScriptName,
                    src = model.src,
                    storeid = _storeId,
                    updated_at = DateTime.UtcNow,
                    updated_user = _userId
                };
                await rpScriptTag.AddAsync(objScriptTags);
                await bus.FireStoreDataChange(_storeId);
                return await GetDetail(objScriptTags._id);
            }
            else
            {
                if (string.IsNullOrWhiteSpace(model.body))
                {
                    context.AddError("errors.script.content.is_empty", "Nội dung không được bỏ trống");
                    return null;
                }
                model.body = model.body.Trim();

                if (model.body.Length > 2000)
                {
                    context.AddError($"errors.script.content.max_limit_reached__{2000}__", $"Nội dung không được vượt quá {2000} ký tự");
                    return null;
                }
                var newFileName = DefaultScriptName + ".js";
                var bizFile = serviceProvider.GetService<IFileBusiness>();
                var objFile = await bizFile.UploadScriptTag(model.body, newFileName);
                var objScript = new MGScriptTagsModel
                {
                    storeid = _storeId,
                    appid = _userId,
                    created_at = DateTime.UtcNow,
                    created_user = _userId,
                    updated_at = DateTime.UtcNow,
                    updated_user = _userId,
                    display_scope = displayScope,
                    evenname = model.@event,
                    scriptname = DefaultScriptName,
                    src = objFile.Url
                };

                await rpScriptTag.AddAsync(objScript);
                await bus.FireStoreDataChange(_storeId);
                return await GetDetail(objScript._id);
            }
        }

        public async Task<ScriptTagAPIModel> Update(long id, ScriptTagUpdateRequest model)
        {
            if (model == null || model.updated_model == null || model.updated_fields == null || !model.updated_fields.Any())
            {
                context.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return null;
            }
            var _storeId = extensionReq.StoreId;
            var _userId = extensionReq.UserId;
            var objScriptTag = await rpScriptTag.GetDetail(_storeId, _userId, id);
            if (objScriptTag == null)
            {
                context.AddError("errors.script.not_exist", "ScriptTag không tồn tại");
                return null;
            }

            var updatedModel = model.updated_model;
            var updatedFields = model.updated_fields;

            if (updatedFields.Contains("src"))
            {
                if (string.IsNullOrEmpty(updatedModel.src))
                {
                    context.AddError("errors.script.link.is_empty", "Đường dẫn không được bỏ trống");
                    return null;
                }
                updatedModel.src = updatedModel.src.Trim();

                if (updatedModel.src.Length > DefaultData.DefaultMaxScripttagSrcLength)
                {
                    context.AddError($"errors.script.link.max_limit_reached__{DefaultData.DefaultMaxScripttagSrcLength}__", $"Đường dẫn không được vượt quá {DefaultData.DefaultMaxScripttagSrcLength} ký tự");
                    return null;
                }

                if (!objScriptTag.src.Equals(updatedModel.src))
                {
                    if (string.IsNullOrWhiteSpace(updatedModel.@event))
                    {
                        updatedModel.@event = objScriptTag.evenname;
                    }
                    else
                    {
                        updatedModel.@event = updatedModel.@event.Trim().ToLower();
                        if (!updatedModel.@event.Equals("onload"))
                        {
                            context.AddError("errors.script.event.incorrect", "Tên event không chính xác");
                            return null;
                        }
                    }
                    var objExist = await rpScriptTag.GetBy(updatedModel.src, updatedModel.@event, _storeId, _userId);
                    if (objExist != null)
                    {
                        context.AddError("errors.script.has_link_even", "Đã tồn tại scripttag với đường dẫn và event này.");
                        return null;
                    }
                    objScriptTag.src = updatedModel.src;
                }
            }
            if (updatedFields.Contains("@event"))
            {
                if (string.IsNullOrWhiteSpace(updatedModel.@event))
                {
                    context.AddError("errors.script.event.is_empty", "Tên event không được bỏ trống");
                    return null;
                }
                updatedModel.@event = updatedModel.@event.Trim().ToLower();

                if (!updatedModel.@event.Equals("onload"))
                {
                    context.AddError("errors.script.event.incorrect", "Tên event không chính xác");
                    return null;
                }
                objScriptTag.evenname = updatedModel.@event;
            }
            if (updatedFields.Contains("display_scope"))
            {
                if (string.IsNullOrWhiteSpace(updatedModel.display_scope))
                {
                    context.AddError("errors.script.display_scope.is_empty", "display_scope không được bỏ trống");
                    return null;
                }
                else
                {
                    var tmpScope = updatedModel.display_scope.Trim().ToLower();
                    var error = string.Empty;
                    switch (tmpScope)
                    {
                        case "all":
                            objScriptTag.display_scope = (int)ScriptTagDisplayScope.all;
                            break;

                        case "online_store":
                            objScriptTag.display_scope = (int)ScriptTagDisplayScope.online_store;
                            break;

                        case "order_status":
                            objScriptTag.display_scope = (int)ScriptTagDisplayScope.order_status;
                            break;

                        case "checkout":
                            objScriptTag.display_scope = (int)ScriptTagDisplayScope.checkout;
                            break;

                        default:
                            error = "display_scope không hợp lệ";
                            break;
                    }
                    if (!string.IsNullOrWhiteSpace(error))
                    {
                        context.AddError("errors.script.display_scope.no_valid", error);
                        return null;
                    }
                }
            }

            objScriptTag.updated_at = DateTime.UtcNow;
            objScriptTag.updated_user = _userId;

            await rpScriptTag.UpdateAsync(objScriptTag, m => m.src, m => m.evenname, m => m.display_scope, m => m.updated_at, m => m.updated_user);
            await bus.FireStoreDataChange(_storeId);
            return await GetDetail(objScriptTag._id);
        }

        public async Task RemoveScriptTagsByAppId(long appId)
        {
            var _storeId = extensionReq.StoreId;
            var lstScriptTags = await rpScriptTag.GetByAppId(_storeId, appId);
            if (lstScriptTags != null && lstScriptTags.Any())
            {
                foreach (var objScriptTag in lstScriptTags)
                {
                    await rpScriptTag.DeleteAsync(objScriptTag);
                }
                await bus.FireStoreDataChange(_storeId);
            }
        }
    }
}