﻿using BHN.Core.Repository;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Configs;
using Haravan.Web.Api.Repository;
using Haravan.Web.Api.Repository.MGRepository;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Net.Http;

namespace Haravan.Web.Api.Business.Extensions
{
    public static class MongoExtensions
    {
        public static void AddMongo(this IServiceCollection services, IConfiguration Configuration)
        {
            services.AddSingleton<IMongoDatabase>(u =>
            {
                var config = u.GetService<IOptions<MongoConfig>>().Value;
                return config.Mongo.GetAndProfileMongoDb();
            });

            services.AddSingleton<IMongoMasterDb>(u =>
            {
                var config = u.GetService<IOptions<MongoConfig>>().Value;
                return new MongoMasterDb(config.MongoSellerMaster.GetAndProfileMongoDb());
            });

            services.AddSingleton<IMongoSellerReportDb>(u =>
            {
                var config = u.GetService<IOptions<MongoConfig>>().Value;
                return new MongoSellerReportDb(config.MongoSellerReport.GetAndProfileMongoDb());
            });
            services.AddSingleton<ISeqIdentity>(u =>
            {
                var config = u.GetService<IOptions<MongoConfig>>().Value;
                return new NoSqlSeqIdentity(config.MongoSeq.GetAndProfileMongoDb());
            });
            services.AddSingleton<IMongoSequenDb>(u =>
            {
                var config = u.GetService<IOptions<MongoConfig>>().Value;

                var db = config.MongoSeq.GetAndProfileMongoDb();

                return new MongoSequenDb(db);
            });
        }
    }

    public static class StartupExtensions
    {
        public static void AddCoreService(
         this IServiceCollection services, IConfiguration Configuration)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            #region Repository

            services.AddScoped<IInternalApiContext, InternalApiContext>();
            services.AddScoped<IExtensionPerRequest, ExtensionPerRequest>();
           
            services.AddScoped<IMGLinkListRepository, MGLinkListRepository>();
            services.AddScoped<IMGScriptTagsRepository, MGScriptTagsRepository>();
            services.AddScoped<IMGLogDataRepository, MGLogDataRepository>();
            services.AddScoped<IMGPageRepository, MGPageRepository>();
            services.AddScoped<IMGKeywordHandleRepo, MGKeywordHandleRepo>();
            services.AddScoped<IMediaRepository, MediaRepository>();
            services.AddScoped<ICertInfo, CertInfo>();
            services.AddScoped<ISysDomainRepository, SysDomainRepository>();
            services.AddScoped<ISysStoreRepository, SysStoreRepository>();
            
            services.AddScoped<IMGCommentRepository, MGCommentRepository>();
            services.AddScoped<IMetafieldsEcomRepository, MetafieldsEcomRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IFilterRepository, FilterRepository>();
            services.AddScoped<IThemeRepository, ThemeRepository>();
            services.AddScoped<ITagRepository, TagRepository>();
            services.AddScoped<IMGFeedBurnerProviderRepository, MGFeedBurnerProviderRepository>();
            
            services.AddScoped<IMGArticleRepository, MGArticleRepository>();
            
            services.AddScoped<IMGBlogRepository, MGBlogRepository>();
            services.AddScoped<IMGSummaryTotalRepository, MGSummaryTotalRepository>();
            services.AddScoped<IMGSummaryRepository, MGSummaryRepository>();
            services.AddScoped<IGeneralSettingRepository, GeneralSettingRepository>();
            services.AddScoped<ILocationRepository, LocationRepository>();
            services.AddScoped<IMGRawLinkListFieldsRepository, MGRawLinkListFieldsRepository>();
            services.AddScoped<IMGRawLinkFieldsRepository, MGRawLinkFieldsRepository>();
            services.AddScoped<IMGRawLinkListRepository, MGRawLinkListRepository>();
            services.AddScoped<IFileRepository, FileRepository>();
            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<ICollectionRepository, CollectionRepository>();
            services.AddScoped<IShopRepository, ShopRepository>();
            services.AddScoped<IMGThemeFileRepository, MGThemeFileRepository>();
            services.AddScoped<IMGAppInstalledRepository, MGAppInstalledRepository>();
            services.AddScoped<IMGFeedBurnerProviderRepository, MGFeedBurnerProviderRepository>();
            services.AddScoped<IMGDomainPriceRepository, MGDomainPriceRepository>();
            services.AddScoped<IStoreDataUserRepository, StoreDataUserRepository>();
            services.AddScoped<IMGUrlRedirectModelRepository, MGUrlRedirectModelRepository>();
            
            services.AddScoped<IMGAppProxyRepository, MGAppProxyRepository>();
         
            services.AddScoped<IMGSYS_ReportScreenRepository, MGSYS_ReportScreenRepository>();
            services.AddScoped<IMGSYS_ReportScreenMeasureRepository, MGSYS_ReportScreenMeasureRepository>();
            services.AddScoped<IMGSYS_ReportScreenGroupPropertyRepository, MGSYS_ReportScreenGroupPropertyRepository>();
            services.AddScoped<IMGSYS_ReportScreenFilterRepository, MGSYS_ReportScreenFilterRepository>();
            services.AddScoped<IMGSYS_ReportScreenFilterDataRepository, MGSYS_ReportScreenFilterDataRepository>();
            services.AddScoped<IMetafieldRepository, MetafieldRepository>();
            services.AddScoped<IMGCartProxyRepository, MGCartProxyRepository>();

            services.AddScoped<IESCommentRepository, ESCommentRepository>();
            services.AddScoped<IESPageRepository, ESPageRepository>();
            services.AddScoped<IESUrlRedirectRepository, ESUrlRedirectRepository>();
            services.AddScoped<IESArticleRepository, ESArticleRepository>();
            services.AddScoped<IESBlogRepository, ESBlogRepository>();
            services.AddScoped<IESUrlRedirectRepository, ESUrlRedirectRepository>();

            services.AddScoped<IESCommentSinkRepository, ESCommentSinkRepository>();
            services.AddScoped<IESPageSinkRepository, ESPageSinkRepository>();
            services.AddScoped<IESUrlRedirectSinkRepository, ESUrlRedirectSinkRepository>();
            services.AddScoped<IESArticleSinkRepository, ESArticleSinkRepository>();
            services.AddScoped<IESBlogSinkRepository, ESBlogSinkRepository>();
            services.AddScoped<IESUrlRedirectSinkRepository, ESUrlRedirectSinkRepository>();

            services.AddHttpClient<IEcomApiRepository, EcomApiRepository>()
                 .ConfigureHttpMessageHandlerBuilder
                 (
                     config => new HttpClientHandler()
                     {
                         UseProxy = false,
                         UseCookies = false
                     }
                 )
                 .ConfigureHttpClient
                 (
                   (serviceProvider, client) =>
                   {
                       var config = serviceProvider.GetService<IOptions<AppConfig>>().Value;
                       client.BaseAddress = new Uri(config.EcomApiUrl);
                   }
                 )
                 .SetHandlerLifetime(TimeSpan.FromMinutes(2));
            services.AddHttpClient<IEtcdApiRepository, EtcdApiRepository>()
                 .ConfigureHttpMessageHandlerBuilder
                 (
                     config => new HttpClientHandler()
                     {
                         UseProxy = false,
                         UseCookies = false
                     }
                 )
                 .ConfigureHttpClient
                 (
                   (serviceProvider, client) =>
                   {
                       var config = serviceProvider.GetService<IOptions<AppConfig>>().Value;
                       client.BaseAddress = new Uri(config.EtcdSsl);
                   }
                 )
                 .SetHandlerLifetime(TimeSpan.FromMinutes(2));
            services.AddScoped<IMGGoogleShoppingConversionTrackerRepository, MGGoogleShoppingConversionTrackerRepository>();
            services.AddScoped<IMGGoogleSiteVerificationRepository, MGGoogleSiteVerificationRepository>();

            #endregion Repository

            #region Business

            services.AddScoped<ICommonBusiness, CommonBusiness>();
            services.AddScoped<IBlogBusiness, BlogBusiness>();
            services.AddScoped<INavigationBusiness, NavigationBusiness>();
            services.AddScoped<ISummaryBusiness, SummaryBusiness>();
            services.AddScoped<IArticleBusiness, ArticleBusiness>();
            services.AddScoped<IFilterBusiness, FilterBusiness>();
            services.AddScoped<ICommentBusiness, CommentBusiness>();
            services.AddScoped<IPageBusiness, PageBusiness>();
            services.AddScoped<IKeywordBusiness, KeywordBusiness>();
            services.AddScoped<IDomainsBusiness, DomainsBusiness>();
            services.AddScoped<IFileBusiness, FileBusiness>();
            services.AddScoped<IGeneralSettingBusiness, GeneralSettingBusiness>();
            services.AddScoped<ILocationBusiness, LocationBusiness>();
            services.AddScoped<IThemeBusiness, ThemeBusiness>();
            services.AddScoped<IStoreBusiness, StoreBusiness>();
            services.AddScoped<IScriptTagsBusiness, ScriptTagsBusiness>();
            services.AddScoped<IGlobalSearchBusiness, GlobalSearchBusiness>();
            services.AddScoped<IAppProxyBusiness, AppProxyBusiness>();
        
            services.AddScoped<IMetafieldBusiness, MetafieldBusiness>();
            services.AddScoped<ICartProxyBusiness, CartProxyBusiness>();
            services.AddScoped<IGoogleSiteVerificationBusiness, GoogleSiteVerificationBusiness>();
            services.AddScoped<IGoogleShoppingConversionTrackerBusiness, GoogleShoppingConversionTrackerBusiness>();

            #endregion Business
        }

        public static void AddCoreWorkerService(
         this IServiceCollection services, IConfiguration Configuration)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton(u => new HttpClient(new HttpClientHandler { UseCookies = false, UseProxy = false }));

            #region Repository

            services.AddScoped<IInternalApiContext, InternalApiContext>();
            services.AddScoped<IExtensionPerRequest, ExtensionPerRequest>();
            services.AddScoped<IMGLinkListRepository, MGLinkListRepository>();
            services.AddScoped<IMGScriptTagsRepository, MGScriptTagsRepository>();
            services.AddScoped<IMGLogDataRepository, MGLogDataRepository>();
            services.AddScoped<IMGPageRepository, MGPageRepository>();
            services.AddScoped<IMGKeywordHandleRepo, MGKeywordHandleRepo>();
            services.AddScoped<IMediaRepository, MediaRepository>();
            services.AddScoped<ICertInfo, CertInfo>();
            services.AddScoped<ISysDomainRepository, SysDomainRepository>();
            services.AddScoped<ISysStoreRepository, SysStoreRepository>();
            
            services.AddScoped<IMGCommentRepository, MGCommentRepository>();
            services.AddScoped<IMetafieldsEcomRepository, MetafieldsEcomRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IFilterRepository, FilterRepository>();
            services.AddScoped<IThemeRepository, ThemeRepository>();
            services.AddScoped<ITagRepository, TagRepository>();
            services.AddScoped<IMGFeedBurnerProviderRepository, MGFeedBurnerProviderRepository>();
            
            services.AddScoped<IMGArticleRepository, MGArticleRepository>();
            
            services.AddScoped<IMGBlogRepository, MGBlogRepository>();
            services.AddScoped<IMGSummaryTotalRepository, MGSummaryTotalRepository>();
            services.AddScoped<IMGSummaryRepository, MGSummaryRepository>();
            services.AddScoped<IGeneralSettingRepository, GeneralSettingRepository>();
            services.AddScoped<ILocationRepository, LocationRepository>();
            services.AddScoped<IMGRawLinkListFieldsRepository, MGRawLinkListFieldsRepository>();
            services.AddScoped<IMGRawLinkFieldsRepository, MGRawLinkFieldsRepository>();
            services.AddScoped<IMGRawLinkListRepository, MGRawLinkListRepository>();
            services.AddScoped<IFileRepository, FileRepository>();
            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<ICollectionRepository, CollectionRepository>();
            services.AddScoped<IShopRepository, ShopRepository>();
            services.AddScoped<IMGThemeFileRepository, MGThemeFileRepository>();
            services.AddScoped<IMGAppInstalledRepository, MGAppInstalledRepository>();
            services.AddScoped<IMGFeedBurnerProviderRepository, MGFeedBurnerProviderRepository>();
            services.AddScoped<IMGDomainPriceRepository, MGDomainPriceRepository>();
            services.AddScoped<IStoreDataUserRepository, StoreDataUserRepository>();
            services.AddScoped<IMGUrlRedirectModelRepository, MGUrlRedirectModelRepository>();
            
            services.AddScoped<IMGAppProxyRepository, MGAppProxyRepository>();
    
            services.AddScoped<IMGSYS_ReportScreenRepository, MGSYS_ReportScreenRepository>();
            services.AddScoped<IMGSYS_ReportScreenMeasureRepository, MGSYS_ReportScreenMeasureRepository>();
            services.AddScoped<IMGSYS_ReportScreenGroupPropertyRepository, MGSYS_ReportScreenGroupPropertyRepository>();
            services.AddScoped<IMGSYS_ReportScreenFilterRepository, MGSYS_ReportScreenFilterRepository>();
            services.AddScoped<IMGSYS_ReportScreenFilterDataRepository, MGSYS_ReportScreenFilterDataRepository>();
            services.AddScoped<IMetafieldRepository, MetafieldRepository>();
            services.AddScoped<IMGCartProxyRepository, MGCartProxyRepository>();
            services.AddHttpClient<IEcomApiRepository, EcomApiRepository>()
                 .ConfigureHttpMessageHandlerBuilder
                 (
                     config => new HttpClientHandler()
                     {
                         UseProxy = false,
                         UseCookies = false
                     }
                 )
                 .ConfigureHttpClient
                 (
                   (serviceProvider, client) =>
                   {
                       var config = serviceProvider.GetService<IOptions<AppConfig>>().Value;
                       client.BaseAddress = new Uri(config.EcomApiUrl);
                   }
                 )
                 .SetHandlerLifetime(TimeSpan.FromMinutes(2));
            services.AddHttpClient<IEtcdApiRepository, EtcdApiRepository>()
                 .ConfigureHttpMessageHandlerBuilder
                 (
                     config => new HttpClientHandler()
                     {
                         UseProxy = false,
                         UseCookies = false
                     }
                 )
                 .ConfigureHttpClient
                 (
                   (serviceProvider, client) =>
                   {
                       var config = serviceProvider.GetService<IOptions<AppConfig>>().Value;
                       client.BaseAddress = new Uri(config.EtcdSsl);
                   }
                 )
                 .SetHandlerLifetime(TimeSpan.FromMinutes(2));

            services.AddScoped<IMGGoogleShoppingConversionTrackerRepository, MGGoogleShoppingConversionTrackerRepository>();
            services.AddScoped<IMGGoogleSiteVerificationRepository, MGGoogleSiteVerificationRepository>();

            services.AddScoped<IESCommentRepository, ESCommentRepository>();
            services.AddScoped<IESPageRepository, ESPageRepository>();
            services.AddScoped<IESUrlRedirectRepository, ESUrlRedirectRepository>();
            services.AddScoped<IESArticleRepository, ESArticleRepository>();
            services.AddScoped<IESBlogRepository, ESBlogRepository>();
            services.AddScoped<IESUrlRedirectRepository, ESUrlRedirectRepository>();

            services.AddScoped<IESCommentSinkRepository, ESCommentSinkRepository>();
            services.AddScoped<IESPageSinkRepository, ESPageSinkRepository>();
            services.AddScoped<IESUrlRedirectSinkRepository, ESUrlRedirectSinkRepository>();
            services.AddScoped<IESArticleSinkRepository, ESArticleSinkRepository>();
            services.AddScoped<IESBlogSinkRepository, ESBlogSinkRepository>();
            services.AddScoped<IESUrlRedirectSinkRepository, ESUrlRedirectSinkRepository>();

            #endregion Repository

            #region Business

            services.AddScoped<ICommonBusiness, CommonBusiness>();
            services.AddScoped<IBlogBusiness, BlogBusiness>();
            services.AddScoped<INavigationBusiness, NavigationBusiness>();
            services.AddScoped<ISummaryBusiness, SummaryBusiness>();
            services.AddScoped<IArticleBusiness, ArticleBusiness>();
            services.AddScoped<IFilterBusiness, FilterBusiness>();
            services.AddScoped<ICommentBusiness, CommentBusiness>();
            services.AddScoped<IPageBusiness, PageBusiness>();
            services.AddScoped<IKeywordBusiness, KeywordBusiness>();
            services.AddScoped<IDomainsBusiness, DomainsBusiness>();
            services.AddScoped<IFileBusiness, FileBusiness>();
            services.AddScoped<IGeneralSettingBusiness, GeneralSettingBusiness>();
            services.AddScoped<ILocationBusiness, LocationBusiness>();
            services.AddScoped<IThemeBusiness, ThemeBusiness>();
            services.AddScoped<IStoreBusiness, StoreBusiness>();
            services.AddScoped<IScriptTagsBusiness, ScriptTagsBusiness>();
            services.AddScoped<IGlobalSearchBusiness, GlobalSearchBusiness>();
            services.AddScoped<IAppProxyBusiness, AppProxyBusiness>();
  
            services.AddScoped<IMetafieldBusiness, MetafieldBusiness>();
            services.AddScoped<ICartProxyBusiness, CartProxyBusiness>();
            services.AddScoped<IEcomBusiness, EcomBusiness>();

            services.AddScoped<IGoogleSiteVerificationBusiness, GoogleSiteVerificationBusiness>();
            services.AddScoped<IGoogleShoppingConversionTrackerBusiness, GoogleShoppingConversionTrackerBusiness>();

            #endregion Business
        }
    }
}