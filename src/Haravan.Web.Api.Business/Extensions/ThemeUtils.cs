﻿using Haravan.Web.Api.BusinessObjects.Configs;

namespace Haravan.Web.Api.Business.Extensions
{
    public class ThemeUtils
    {
        public static bool IsNewFileService(long themeId, AppConfig config)
        {
            return _isNewFileService(themeId, config);
        }

        private static bool _isNewFileService(long themeId, AppConfig config)
        {
            var result = themeId > config.FileServiceFromThemeId;

            return result;
        }
    }
}