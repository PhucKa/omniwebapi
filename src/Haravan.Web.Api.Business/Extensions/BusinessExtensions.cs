﻿using BHN.SharedObject.EBSMessage;
using Haravan.Caching;
using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Collections.Generic;
using Haravan.Web.Api.BusinessObjects;
using System.Text;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business.Extensions
{
    public static class BusinessExtensions
    {
        public static async Task<bool> CanExportImport(long storeId, ICache cache, int timeExpire)
        {            
            var key = $"{DefaultData.DefaultPrefixEcomCache}{CacheKeys.GetStoreExportImport(storeId)}";
            var cacheKey = CacheKey.Create(new DistributedCacheEntryOptions() { SlidingExpiration = TimeSpan.FromSeconds(timeExpire) }, key);
            var existKey = await cache.GetAsync<string>(cacheKey) != null;
            if (existKey) return false;
            else
            {
                await cache.SetAsync(cacheKey, "1");
                return true;
            }
        }
    }
}
