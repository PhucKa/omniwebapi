﻿using Haravan.Web.Api.BusinessObjects.Models;
using System.Collections.Generic;

namespace Haravan.Web.Api.Business.Extensions
{
    public static class DataFreeTheme
    {
        public static List<ThemesFree> DefaultData = new List<ThemesFree>
        {
            new ThemesFree
                {
                    name= "Supply",
                    code= "Supply",
                    id= 1000211575,
                    image= "https://product.hstatic.net/1000012173/product/01_bf2251affeed464f85d37895b7d83402.jpg",
                    link_install= "https://static.hara.vn/inside/a044246d3ae944099f66ed3d8769d476_da688e5a-5d1d-43eb-beb8-7887f86629f3.zip",
                    link_demo="https://supply-theme.myharavan.com/"
                },
            new ThemesFree
                {
                    name= "Happy Babies",
                    code= "Happy Babies",
                    id= 1000211576,
                    image= "https://product.hstatic.net/1000012173/product/img01_0a18dd545ca741c48b0a8fb741b39373.jpg",
                    link_install= "https://static.hara.vn/inside/a0b0f2cbc38d4797828e6391ef7c656c_31d783aa-cfaa-440e-9bf6-b6df32df04f3.zip",
                    link_demo="https://happy-babies.myharavan.com/"
                },

            new ThemesFree
                {
                    name= "lama fashion",
                    code= "lama fashion",
                    id= 1000211577,
                    image= "https://product.hstatic.net/1000012173/product/01_bf2251affeed464f85d37895b7d83402.jpg",
                    link_install= " https://static.hara.vn/inside/d934629c7dea40d7b1086634cd69deef_94a686ca-b67b-4264-b220-c26b228b6fa1.zip",
                    link_demo="https://lama-fashion.myharavan.com/"
                },
            new ThemesFree
                {
                    name= "Mega Shoes",
                    code= "Mega Shoes",
                    id= 20187,
                    image= "https://product.hstatic.net/1000012173/product/01_f336bdcd825d4596b883c45cdad2192f.jpg",
                    link_install= "http://d-static.sku.vn/inside/4776d15e-6637-49ed-9e0e-ff0159b6ebae.zip",
                    link_demo="https://mega-shoes.myharavan.com/"
                },
            new ThemesFree
                {
                    name= "Grey Fashion",
                    code= "Grey Fashion",
                    id= 20178,
                    image= "https://product.hstatic.net/1000012173/product/01_01781be1f78240549ad7a392401bdcfd.jpg",
                    link_install= "http://d-static.sku.vn/inside/4776d15e-6637-49ed-9e0e-ff0159b6ebae.zip",
                    link_demo="https://grey-fashion.myharavan.com/"
                },
            new ThemesFree
                {
                    name= "decor",
                    code= "decor",
                    id= 83,
                    image= "https://product.hstatic.net/1000012173/product/01_738c7c758570406e9b516ff263bd9e0f.jpg",
                    link_install= "http://d-static.sku.vn/inside/4776d15e-6637-49ed-9e0e-ff0159b6ebae.zip",
                    link_demo="https://jwelry-store.myharavan.com/"
                },
            new ThemesFree
                {
                    name= "fashionplanet",
                    code= "fashionplanet",
                    id= 84,
                    image= "https://product.hstatic.net/1000012173/product/01_f336bdcd825d4596b883c45cdad2192f.jpg",
                    link_install= "http://d-static.sku.vn/inside/4776d15e-6637-49ed-9e0e-ff0159b6ebae.zip",
                    link_demo="https://mega-shoes.myharavan.com/"
                },
            new ThemesFree
                {
                    name= "Modern Furniture",
                    code= "Modern Furniture",
                    id= 85,
                    image= "https://product.hstatic.net/1000012173/product/01_0c3af95bb51c430586e80293834aafdd.jpg",
                    link_install= "http://d-static.sku.vn/inside/4776d15e-6637-49ed-9e0e-ff0159b6ebae.zip",
                    link_demo="https://modern-furniture.myharavan.com/"
                },
            new ThemesFree
                {
                    name= "Sunstyle Glasses",
                    code= "Sunstyle Glasses",
                    id= 86,
                    image= "https://product.hstatic.net/1000012173/product/01_1ebab0cd671f4cfe81054690e0963091.jpg",
                    link_install= "http://d-static.sku.vn/inside/4776d15e-6637-49ed-9e0e-ff0159b6ebae.zip",
                    link_demo="https://sunstyle-glasses.myharavan.com/"
                },
            new ThemesFree
                {
                    name= "Lux Residence",
                    code= "Lux Residence",
                    id= 87,
                    image= "https://hstatic.net/173/1000012173/1/2016/4-20/01_7b73dd6c-bf98-4dc3-454d-f4bcbe35e161.jpg",
                    link_install= "http://d-static.sku.vn/inside/4776d15e-6637-49ed-9e0e-ff0159b6ebae.zip",
                    link_demo="https://lux-residence.myharavan.com/"
                },
            new ThemesFree
                {
                    name= "Fresh Organic Food",
                    code= "Fresh Organic Food",
                    id= 88,
                    image= "https://product.hstatic.net/1000012173/product/01_02338cc21eae4d95ac6165bde51b2988.jpg",
                    link_install= "http://d-static.sku.vn/inside/4776d15e-6637-49ed-9e0e-ff0159b6ebae.zip",
                    link_demo="https://fresh-organic-food.myharavan.com/"
                },
            new ThemesFree
                {
                    name= "Happy Babies",
                    code= "Happy Babies",
                    id= 20178,
                    image= "https://product.hstatic.net/1000012173/product/img01_0a18dd545ca741c48b0a8fb741b39373.jpg",
                    link_install= "http://d-static.sku.vn/inside/4776d15e-6637-49ed-9e0e-ff0159b6ebae.zip",
                    link_demo="https://happy-babies.myharavan.com/"
                },
        };
    }
}