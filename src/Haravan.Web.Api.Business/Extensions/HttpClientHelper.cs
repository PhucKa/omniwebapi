﻿using Haravan.Web.Api.BusinessObjects.Configs;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business.Extensions
{
    public static class HttpClientHelper
    {
        private static HttpClient httpClient = new HttpClient();

        public static async Task<bool> PutSslCert(string domain, string cert)
        {
            var data = new Dictionary<string, string> { { "value", cert } };
            var encodedContent = new FormUrlEncodedContent(data);

            var apiUrl = $"{AppConfig.Current.EtcdSsl}{domain}_c";

            try
            {
                using (var rq = await httpClient.PutAsync(apiUrl, encodedContent).ConfigureAwait(false))
                {
                    return rq.IsSuccessStatusCode ? true : false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static async Task<bool> PutSslPrivateKey(string domain, string privateKey)
        {
            var data = new Dictionary<string, string> { { "value", privateKey } };
            var encodedContent = new FormUrlEncodedContent(data);

            var apiUrl = $"{AppConfig.Current.EtcdSsl}{domain}_k";

            try
            {
                using (var rq = await httpClient.PutAsync(apiUrl, encodedContent).ConfigureAwait(false))
                {
                    return rq.IsSuccessStatusCode ? true : false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
