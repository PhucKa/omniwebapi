﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business.Extensions
{
    public static class Helpers
    {
        public static string CreateFriendlyURL(string title)
        {
            if (string.IsNullOrEmpty(title))
                return string.Empty;
            string slug = title;
            if (slug.Length > 218)
                slug = slug.Substring(0, 218);

            Regex v_reg_regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string v_str_FormD = title.Normalize(NormalizationForm.FormD);
            slug = v_reg_regex.Replace(v_str_FormD, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');

            slug = Regex.Replace(slug, @"[^a-zA-Z0-9]+", "-");
            slug = Regex.Replace(slug, @"[^a-zA-Z0-9\-_/\\]+", "");
            slug = Regex.Replace(slug, "(-)+", "-").Trim('-');
            slug = slug.ToLower();

            if (string.IsNullOrEmpty(slug))
            {
                slug = "-";
            }
            else
            {
                if (slug[slug.Length - 1] == '-')
                    slug = slug.Remove(slug.Length - 1, 1);
            }

            return slug;
        }

        public static string[] ParseTagsUrl(string[] Input)
        {
            if (Input.Length == 0)
            {
                return Input;
            }

            var tags = new List<string>();

            foreach (var item in Input)
            {
                tags.Add(CreateFriendlyURL(item));
            }

            return tags.GroupBy(a => a).Select(a => a.Key).ToArray();
        }
        public static async Task<byte[]> StreamToByArray(Stream input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                await input.CopyToAsync(ms);
                return ms.ToArray();
            }
        }
        public static async Task<List<Tuple<string, object>>> UploadFileHelper(this IFormFile file)
        {
            var data = new List<Tuple<string, object>>();
            data.Add(new Tuple<string, object>(file.FileName, await StreamToByArray(file.OpenReadStream())));
            return data;
        }
    }
}
