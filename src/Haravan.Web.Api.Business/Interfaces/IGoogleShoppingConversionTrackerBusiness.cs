﻿using Haravan.Web.Api.BusinessObjects.Models.Google;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public interface IGoogleShoppingConversionTrackerBusiness
    {
        Task AddApiAsync(List<GoogleShoppingConversionTrackerDetailApiModel> model);
        Task DeleteManyAsync();
    }
}