namespace Haravan.Web.Api.Business
{
    using BHN.SharedObject.EBSMessage;
    using Haravan.BlobService;
    using Haravan.Web.Api.BusinessObjects;
    using Haravan.Web.Api.BusinessObjects.ESModels;
    using Haravan.Web.Api.BusinessObjects.Models;
    using Microsoft.AspNetCore.Http;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;

    public interface IFileBusiness
    {
        Task DeleteImages(List<string> imageUrls);

        Task<FileUploadedInfo> UploadArticleFeatureImage(long storeId, string fileName, Stream stream);

        Task<FileUploadedInfo> AddImageFromUrl(string url, string fileName, FileType fileType, long storeId);

        Task<FileUploadedInfo> AddImageFromBase64String(string attactment, string filename, FileType fileType, long storeId);

        Task<string> AddTemporaryFile(string fileName, Stream stream);

        Task<FileUploadedInfo> UploadStorageFile(IFormFile file);

        Task<FileModel> UploadStorageImages(IFormFile file);

        Task<FileModel> UploadStorageAllFile(IFormFile file);

        Task<List<FileModel>> GetProductImagesListByProductId(long productId);

        Task<(List<FileModel> data, long totalRecord)> GetImageList(string query, int limit, int page);

        Task<bool> DeleteFile(long fileId);

        Task<bool> DeleteAllSelectedFile(List<long> fileId);

        Task<FileServiceResponse> UploadSettingDataTmp(long themeId, FileType fileType, string fileName, Stream stream, bool isNewFileService);

        Task<string> GetContent(string url);

        Task<string> GetStaticContent(string url);

        Task<FileUploadedInfo> UploadScriptTag(string fileContent, string fileName);

        Task<FileModel> UploadThemeFile(
            long themeId, FileType fileType, string fileName, Stream stream,
            long? rootFileId = null, bool isAddFileToDb = true, bool isCommit = true,
            int? maxWidth = null, int? maxHeight = null);

        Task<FileModel> OldMedia_UploadThemeFile(long themeId, FileType fileType, string fileName, Stream fileStream, int? maxWidth = null, int? maxHeight = null);
    }
}