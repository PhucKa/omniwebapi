﻿using Haravan.Web.Api.BusinessObjects.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public interface IReportBusiness
    {
        Task<List<ReportScreenTypeModel>> GetListReportWeb();

        Task<ReportScreen> GetReportWebScreenById(string id);

        Task<List<ReportDataColumn>> GetReportSummaryData(string reportId, query query);

        Task<ReportDataViewWebModel> GetDataByQueryModelCountLy(string id, query query);
    }
}