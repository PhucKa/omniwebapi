﻿using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public interface IKeywordBusiness
    {
        Task<string> ProcessingKeyHandle(long storeId, int typeId, string keywordOld, string keywordNew);

        Task<string> ProcessingKeyHandleWithRefId(long storeId, int typeid, string keywordold, string keywordnew, long oldRefId, long newRefId);
    }
}