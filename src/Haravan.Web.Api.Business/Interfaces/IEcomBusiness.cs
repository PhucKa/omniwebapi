﻿using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public interface IEcomBusiness
    {
        Task RemoveSaleChannelAfterUninstallApp(string clientId);
        Task RemoveDataAfterUninstallApp(string clientId);
    }
}
