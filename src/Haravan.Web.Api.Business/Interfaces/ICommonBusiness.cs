﻿using BHN.SharedObject.APIDataModel;
using Haravan.Web.Api.BusinessObjects.Models.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public interface ICommonBusiness
    {
        Task<(List<ThemeDropdownModel> datas, long totalRecord)> GetSimpleDropdownListByType(int type,
                                                                                               string query,
                                                                                               int page,
                                                                                                int limit);

        Task<ThemeDropdownModel> GetSimpleDetailByType(int type, string handle);

        Task<ShopAPIModel> GetShopInfo();
    }
}