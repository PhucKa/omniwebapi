﻿using BHN.SharedObject.APIDataModel;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public interface IAppProxyBusiness
    {
        Task<bool> DeleteFromWorker(long appId);
        Task<bool> Delete(long appId);
        Task<AppProxyAPIModel> GetByAppId(long appId);
        Task<AppProxyAPIModel> Update(long appId, AppProxyAPIModel model);
        Task<AppProxyAPIModel> Add(long appId, AppProxyAPIModel model);
    }
}
