﻿using BHN.SharedObject.APIDataModel;
using Haravan.Web.Api.BusinessObjects.ESModels;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.BusinessObjects.Models.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public interface IBlogBusiness
    {
        Task<(List<BlogSearchRowModel> data, long totalrecord)> GetBlogList(bool visible,
                                                    bool hidden,
                                                    string query,
                                                    string order,
                                                    string direction,
                                                    int page,
                                                    int limit,
                                                    string collection_type);

        Task<(List<BlogSearchRowModel> data, long totalrecord)> GetGlobalSearchListBlog(FilterSearchModel model);

        Task<(List<BlogSearchRowModel> data, long totalrecord)> GetBlogList(FilterSearchModel filter);

        Task<List<BlogAPIModel>> GetBlogListAPI(FilterSearchModel filter);

        Task<long> CountBlogs(FilterSearchModel filter);

        Task<BlogDetailModel> AddNewAsync(BlogDetailModel model, List<NavigationDetailModel> linkList = null);

        Task<BlogAPIModel> AddNewApiAsync(BlogDetailModel model);

        Task<BlogDetailModel> UpdateAsync(BlogDetailModel model, List<NavigationDetailModel> linkList = null, bool isAPICall = false);

        Task<BlogDetailModel> GetDetail(long id);

        Task<BlogDetailModel> GetDetailExcepNavigation(long id);

        Task<string> GetHandleById(long id);

        Task<BlogDetailModel> GetByUrlHandle(string urlHandle);

        Task<List<BlogDetailModel>> GetListByListHandleUrl(List<string> urlHandles);

        Task<BlogAPIModel> GetDetailApi(long blogId);
        Task<BlogDetailModel> GetBlogFirstByHandle();

        Task<bool> DeleteBlogId(List<long> blogIds);

        Task<(List<DropdownSimpleModel> data, long totalRecord)> GetSimpleBlogsList(bool isVisible,
                                                   string query,
                                                   int page,
                                                   int limit);

        Task<(List<ThemeDropdownModel> data, long totalRecord)> GetSimpleBlogsListForTheme(bool isVisible,
                                                  string query,
                                                  int page,
                                                  int limit);

        Task<List<DropdownSimpleModel>> GetListFeedburnerSimpleAsync();

        Task<List<DropdownSimpleModel>> GetTemplateByName();

        Task<List<string>> GetTemplateName();

        Task<bool> DeleteBlogByIdAsync(long blogId);

        Task<BlogAPIModel> UpdateAPI(long blogId, Dictionary<string, object> dicUpdated);

        Task<List<OmniSimpleModel>> GetSimpleBlogList(long blogId);

        Task<bool> Init_NewStore();

        Task<BlogDetailViewModelInternal> GetByUrlHandleV2(string urlHandle);

        Task<List<BlogDetailViewModelInternal>> GetByListUrlHandle(List<string> listUrlHandle);

        Task<long> GetIdByHandleUrl(string handleUrl);

        #region OpenAPI
        Task<List<BlogAPIModel>> OpenApi_GetBlogList(int page, int limit, long since_id, string handle);
        Task<long> OpenApi_CountBlogs(int page, int limit, long since_id, string handle);
        Task<BlogAPIModel> OpenApi_GetBlogDetail(long blog_id);
        Task<BlogAPIModel> OpenApi_UpdateBlog(long blog_id, Dictionary<string, object> model);
        Task<BlogAPIModel> OpenApi_AddNewBlog(Dictionary<string, object> model);
        Task<bool> OpenApi_DeleteBlog(long blog_id);
        #endregion
    }
}