﻿using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public interface IStoreBusiness
    {
        Task<bool> CreateShop(CreateShopModel model);
        Task<bool> AddSaleChannel(string channel);
        //Task<StoreData> GetStoreData(long storeId, CancellationToken cctoken = default(CancellationToken));
        //Task<StoreData> GetStoreData(CancellationToken cctoken = default(CancellationToken));
    }
}
