﻿using BHN.SharedObject.APIDataModel;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.ESModels;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.BusinessObjects.Models.Article;
using Haravan.Web.Api.BusinessObjects.Models.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public interface IArticleBusiness
    {
        Task<ArticleDetailModel> GetArticleDetail(long blogId, long articleId);
        Task<(List<ArticleSearchRowModel> data, long totalrecord)> GetArticleList(
                                                bool visible,
                                                bool hidden,
                                                string query,
                                                string order,
                                                string direction,
                                                int page,
                                                int limit,
                                                string blog_ids,
                                                string user_ids,
                                                string tag_contains,
                                                string tag_not_contains,
                                                bool tag_not_exist
                                                );

        Task<(List<ArticleSearchRowModel> data, long totalrecord)> GetGlobalSearchListArticle(FilterSearchModel model);

        Task<(List<ThemeDropdownModel> data, long totalrecord)> GetArticleDropDownListForTheme(
                                                string query,
                                                int page,
                                                int limit
                                                );

        Task<ArticleDetailModel> AddNewAsync(ArticleDetailModel model);

        Task<ArticleDetailModel> UpdateAsync(ArticleDetailModel model);

        Task<bool> DeleteArticleAsync(List<long> articleIds);

        Task<ArticleDetailModel> GetDetail(long id);

        Task<List<DropdownlistModel>> GetDropdownlistBlog();

        Task<bool> PublishArticles(List<long> articlesIds, bool isPublish);

        Task<bool> AddTags(List<long> articlesIds, List<string> Tags);

        Task<bool> RemoveTags(List<long> articlesIds, List<string> Tags);

        Task<List<DropdownlistModel>> GetAuthorList();

        Task<List<DropdownlistModel>> GetUsersSimpleList();

        Task<List<OmniSimpleModel>> GetSimpleUserList(long userId);

        Task<List<string>> GetArticleTags(int type, int limit);

        Task<List<DropdownlistModel>> GetDropdownlistTheme();

        Task<bool> DeleteArticleByIdAsync(long blogId);

        Task UploadArticleImage(long articleId, string fileName, Stream fileStream);

        Task<DataPaging<List<FileModel>>> GetFileList(string query, int page, int limit);

        Task<DataPaging<List<FileModel>>> GetProductFeaturedImages(string type, string query, int page, int limit);

        Task<bool> Init_NewStore();

        Task<ArticleDetailModel> GetArticleFirstByHandle();

        #region Internal

        Task<ArticleDetailDropdownlistModel> GetDropdownlistDetail();

        Task<(List<ArticleListModelInternal> data, long totalrecord)> GetArticleList(FilterSearchModel filter);

        #endregion Internal

        #region API

        Task<(List<ArticleAPIModel> articles, long totalRecord)> GetArticleAPIList(FilterSearchModel model);

        Task<ArticleAPIModel> GetDetailByIdAPI(FilterSearchModel filter);

        Task<ArticleAPIModel> AddNewAPI(ArticleDetailModel model);

        Task<ArticleAPIModel> UpdateAPI(long articleId, Dictionary<string, object> dicUpdated);

        Task<List<string>> GetAuthorListAPI();

        Task<List<string>> GetAllTagsAPI(FilterSearchModel model, bool getPopular);

        Task<List<string>> GetTagsByBlogAPI(FilterSearchModel model, bool getPopular);

        Task<bool> DeleteArticleApi(long articleId);

        #endregion API
        #region OpenAPI
        Task<(List<ArticleAPIModel> articles, long totalRecord)> OpenApi_GetArticlesList(long articleId, long blogId, DateTime? created_at_max, DateTime? created_at_min, string fields, int limit, int page,
                                                 int popular, DateTime? published_at_max, DateTime? published_at_min, string published_status, long since_id, DateTime? updated_at_max, DateTime? updated_at_min);
        Task<long> OpenApi_CountArticles(long articleId, long blogId, DateTime? created_at_max, DateTime? created_at_min, string fields, int limit, int page,
                                                 int popular, DateTime? published_at_max, DateTime? published_at_min, string published_status, long since_id, DateTime? updated_at_max, DateTime? updated_at_min);

        Task<ArticleAPIModel> OpenApi_GetArticleDetail(long article_id);
        Task<ArticleAPIModel> OpenApi_AddArticle(Dictionary<string, object> model);
        Task<ArticleAPIModel> OpenApi_UpdateArticle(long article_id, Dictionary<string, object> model);
        Task<bool> OpenApi_DeleteArticle(long article_id);
        Task<List<string>> OpenApi_GetAuthorsList();
        Task<List<string>> OpenApi_GetAllTags(long articleId, long blogId, DateTime? created_at_max, DateTime? created_at_min, string fields, int limit, int page,
                                                 int popular, DateTime? published_at_max, DateTime? published_at_min, string published_status, long since_id, DateTime? updated_at_max, DateTime? updated_at_min);
        Task<List<string>> OpenApi_GetTagsByBlogAPIAsync(long articleId, long blogId, DateTime? created_at_max, DateTime? created_at_min, string fields, int limit, int page,
                                                 int popular, DateTime? published_at_max, DateTime? published_at_min, string published_status, long since_id, DateTime? updated_at_max, DateTime? updated_at_min);
        #endregion
    }
}