﻿using Haravan.Web.Api.BusinessObjects.Models.Common;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public interface IFilterBusiness
    {
        Task<ViewFilterData> GetFilterAsync(int viewId);

        Task<FilterTab> GetTabDetail(long tabId);

        Task<long> AddFilter(FilterTab tab);

        Task<long> UpdateFilter(FilterTab tab);

        Task<long> DeleteFilter(long tabId);
    }
}