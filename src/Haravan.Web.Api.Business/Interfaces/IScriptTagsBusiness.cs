﻿using BHN.SharedObject.APIDataModel;
using Haravan.Web.Api.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public interface IScriptTagsBusiness
    {
        Task<List<ScriptTagAPIModel>> GetList(
            int page, int limit, long since_id, DateTime? created_at_min,
            DateTime? created_at_max, DateTime? updated_at_min, DateTime? updated_at_max, string src
            );
        Task<long> Count(
            long since_id, DateTime? created_at_min,
            DateTime? created_at_max, DateTime? updated_at_min, DateTime? updated_at_max, string src
            );
        Task<ScriptTagAPIModel> GetDetail(long id);
        Task<ScriptTagAPIModel> Add(ScriptTagAPIModel model);
        Task<ScriptTagAPIModel> Update(long id, ScriptTagUpdateRequest model);
        Task<bool> Delete(long id);
        Task RemoveScriptTagsByAppId(long appId);
    }
}
