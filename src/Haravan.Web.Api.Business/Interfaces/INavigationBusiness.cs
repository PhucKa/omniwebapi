﻿using Haravan.Web.Api.BusinessObjects.Enums;
using Haravan.Web.Api.BusinessObjects.ESModels;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.BusinessObjects.Models.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public interface INavigationBusiness
    {
        Task<long> AddFromWebChannel(NavigationDetailModelPath model);

        Task UpdateFromWebChannel(NavigationDetailModelPath model);

        Task<string> AddToMenu(long linklistfieldid);

        Task<string> IsLinkListHasParentLink(long linklistid);

        Task<bool> CheckLinkFieldIdHasChildLinkList(long linkfieldid);

        Task<bool> UpdateLinkOrder(List<LinkListRowModel> listRow, long linkListId);

        Task<List<ThemeDropdownModel>> GetDropdownlist_LinkList_ThemeSetting_V2();

        Task<List<SimpleListModel>> GetDropdownlist_LinkList_ThemeSetting();

        Task<List<SimpleListModel>> GetDropdownlist_LinkList();

        Task<long> AddFromSeller(NavigationDetailModel model, bool isCommit = true, bool isInternal = false);

        Task UpdateFromSeller(NavigationDetailModel model, bool isCommit = true, bool isInternal = false);

        Task<bool> DeleteBulkLinkListAsync(List<long> linkListId);

        Task<bool> DeleteLinkListById(long linkListId);

        Task<List<LinkFieldsListModel>> GetDropdownlist_ForLinkTo();

        Task<NavigationDetailModel> GetDetailLinkListId(long linkListId);

        Task<NavigationDetailModelPath> GetDetailLinkListWithPath(long linkListId);

        Task<NavigationListModel> GetNavigationList(bool is_drag);

        Task<NavigationDetailModel> GetLinkListFromLinkListFieldId(long linklistfieldid);

        Task<(List<DropdownlistModel>, long)> GetDropdownlistByLinkField(LinkFieldType linkFieldType, FilterSearchModel model);

        Task<(List<UrlRedirectModel> data, long totalrecord)> GetRedirectListAsync(
                                                string query,
                                                int page,
                                                int limit
                                                );

        Task<(List<DropdownlistModel>, long)> GetDropdownListByLinkFieldNavigation(LinkFieldType linkFieldType, string query, int page, int limit);

        Task<bool> DeleteUrlRedirect(List<long> UrlRedirectId);

        Task<bool> DeleteUrlRedirectByIdAsync(long UrlRedirectId);

        Task Page_UpdateForLinkUrl(long pageId, bool isDeleteAction = false);

        Task Blog_UpdateForLinkUrl(long blogId, bool isDeleteAction = false);

        Task Collection_UpdateForLinkUrl(long collectionId, bool isDeleteAction = false);

        Task Product_UpdateForLinkUrl(long productId, bool isDeleteAction = false);

        Task<List<NavigationDetailModel>> GetLinkListByRefId_ExceptHandle(long refId, int type);

        Task SetLinkByRefId(long refId, string refTitle, List<NavigationDetailModel> linkLists, LinkFieldType type);

        Task AddOrUpdateProductUrlRedirect(string oldPath, string newPath);
        Task AddOrUpdateArticleUrlRedirect(string oldPath, string newPath);

        #region Url Redirect

        Task<bool> Init_NewStore();

        Task<(bool, long)> AddUrlRedirect(UrlRedirectModel model);

        Task<(List<UrlRedirectModel> data, long totalRecord)> GetRedirectList(FilterSearchModel model);

        Task<bool> DeleteUrlRedirect(UrlRedirectModel model);

        Task<UrlRedirectModel> GetByIdUrlRedirect(long UrlRedirectId);

        Task<bool> UpdateUrlRedirect(UrlRedirectModel model);

        #endregion Url Redirect

        #region API

        Task<RedirectAPIModel> AddUrlRedirectApi(UrlRedirectModel model);

        Task<RedirectAPIModel> GetUrlRedirectDetailApi(long id);

        Task<List<RedirectAPIModel>> GetUrlRedirectListApi(FilterSearchModel model);

        Task<long> CountUrlRedirects(FilterSearchModel model);

        Task<bool> DeleteUrlRedirectApi(long id);

        Task<RedirectAPIModel> UpdateUrlRedirectApi(long id, Dictionary<string, object> dicUpdated);

        #endregion API

        #region Open API

        Task<List<RedirectAPIModel>> OpenApi_GetRedirectList(int limit, int page, long since_id, string path, string target, string fields);

        Task<long> OpenApi_CountRedirect(int limit, int page, long since_id, string path, string target, string fields);

        Task<RedirectAPIModel> OpenApi_GetRedirectDetail(long RedirectId);

        Task<RedirectAPIModel> OpenApi_AddRedirect(Dictionary<string, object> model);

        Task<RedirectAPIModel> OpenApi_UpdateRedirect(long RedirectId, Dictionary<string, object> dicUpdated);

        Task<bool> OpenApi_RedirectDeleted(long RedirectId);

        #endregion Open API
    }
}