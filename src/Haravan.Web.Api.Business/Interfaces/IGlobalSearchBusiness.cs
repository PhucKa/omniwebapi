﻿using Haravan.Web.Api.BusinessObjects.ESModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public interface IGlobalSearchBusiness
    {
        Task<Tuple<List<ESGlobalSearchResult>, long>> GetGlobalSearchList(FilterSearchModel model);
    }
}