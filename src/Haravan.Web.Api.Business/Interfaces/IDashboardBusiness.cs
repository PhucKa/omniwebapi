﻿using Haravan.Web.Api.BusinessObjects.Models;
using System;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public interface IDashboardBusiness
    {
        #region Countly report dashboard

        Task<DashboardAnalyticsSession> ViewAnalyticsSession(DateTime? enddate, DateTime? startdate);

        Task<DashboardAnalyticsDurationsModel> ViewAnalyticsDurations(DateTime? enddate, DateTime? startdate);

        Task<DashboardAnalyticsFrequencyModel> ViewAnalyticsFrequency(DateTime? enddate, DateTime? startdate);

        Task<DashboardAnalyticsPlatformModel> ViewAnalyticsPlatform(DateTime? enddate, DateTime? startdate);

        Task<DashboardAnalyticsLocationModel> ViewAnalyticsCities(DateTime? enddate, DateTime? startdate);

        Task<DashboardAnalyticsViewVisitPage> ViewAnalyticsViewVisitPage(DateTime? enddate, DateTime? startdate);

        #endregion Countly report dashboard
    }
}