﻿using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.BusinessObjects.Models.user;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public interface IDomainsBusiness
    {
        Task<bool> UpdateHttps(long domainId);
        Task<string> AddDomain(string domainName);
        #region Checkout Domain

        Task<List<DomainPriceModel>> GetListPriceDomains();

        #endregion Checkout Domain

        #region SYS

        Task<string> SubmitNewDomains(SYSDomainModel model);

        Task<string> AddNewDomains(string domains, bool isHttps);

        Task<string> GetIpAssignedByStore();

        Task<DataDomainModel> GetDataDomain();

        Task<List<SYSDomainModel>> ListDomainCanChangePrimary();

        Task<bool> ChangToPrimaryDomain(long domainId, bool isRedirect);

        Task<bool> CheckExistDomain(string domainName);

        Task<bool> DeleteDomain(long domainId, bool fromOtherStore = false);

        Task<bool> RemoveDomain(long domainId);

        Task<bool> VerifyToReclaimDomain(string domainName);

        Task<UserUpdateModel> GetForUpdate(long userId);

        Task<string> SubmitChangeSubDomain(string newSubDomain);

        Task<MGSysDomain> ValidateChangeSubDomain(string newSubDomain, bool isSubmit = false);

        Task<bool> UpdateIsHttpsAsync(long domainId, string name, bool isUseHttps, bool isThrowError = true);

        Task ChangeDomainIsHttps(SYSDomainModel domain);

        Task<bool> UnUseLetEnscript(long domainId);

        Task<CheckValidDomainModel> CheckValidDomain(string hostname);

        Task<bool> CheckValidationDomain(string hostname);
        Task<bool> IsPointToHaravan(string domainName);
        #endregion SYS
        Task<bool> HasSpfRecord(string email);
    }
}