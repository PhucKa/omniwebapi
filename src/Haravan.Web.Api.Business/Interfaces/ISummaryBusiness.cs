﻿using BHN.SharedObject.EBSMessage;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public interface ISummaryBusiness
    {
        Task<long> ExistSummaryName(SummaryType Type, long RefId, string name);

        Task<List<string>> GetSummaryNames(SummaryType Type, long RefId);

        Task<List<MGSummaryModel>> GetSummaries(SummaryType Type, long storeId, long RefId);

        Task<List<string>> GetSummaryTotalNames(SummaryType Type);

        Task AddSummaryNamesForRefIds(SummaryType Type, long[] RefIds, List<string> SummaryNames);
    }
}