﻿using BHN.SharedObject.APIDataModel;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.ESModels;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.BusinessObjects.Models.Common;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public interface IThemeBusiness
    {
        #region Theme code

        Task<bool> ExportTheme(long themeId);

        Task<List<ThemeModel>> PublishTheme(long id);

        Task<ThemeModel> UnPublishTheme(ThemeModel model);

        Task<bool> DeleteTheme(long themeId);

        Task<ThemeModel> DuplicateTheme(long themeId);
        Task<ThemeModel> DuplicateThemeByLanguage(long themeId, string prefixText);

        Task<ThemeListModel> GetThemeList();

        Task<ThemeModel> ImportTheme(IFormFile file);

        Task<ThemeModel> ImportTheme(string name, IFormFile file);

        Task<string> GetSettingSchemaThemeFile(long themeId);

        Task<string> GetSettingDataFile(long themeId);

        Task<ThemeFileModel> EditSettingData(long themeId, StringModel model);

        Task<string> GetFrameToken(long themeId);

        Task UpdateSettingDataTmpContent(long themeId, string content);

        Task ResetSettingDataTmpContent(long themeId);

        Task<ThemeModel> GetThemeForEdit(long id);

        Task<ThemeModel> GetThemeFileForEdit(long themeid);

        Task<ThemeModel> GetThemeForLocale(long id);

        Task<ThemeFileModel> LoadCurrentLocale(long id, string locale);

        Task<string> LoadLocaleContent(long themeId, long themeFileId, long localeFileId);

        Task<ThemeModel> GetThemeForSetting(long id);

        Task<List<FileModel>> GetFileVersion(long Id, long RootFileId);

        Task<bool> DeleteThemeFile(long themefileid);

        Task<ThemeFileModel> SaveThemeFile(ThemeFileModelRequest model);

        Task<ThemeFileModel> SaveLocaleThemeFile(ThemeFileModel themeFile, string newContent);

        Task<bool> RenameTheme(long Id, string Name);

        Task<ThemeFileModel> RenameThemeFile(RenameThemeFileRequest themeFile);

        Task<ThemeFileModel> AddNewThemeFile(ThemeFileModelAddRequest themeFile);

        Task<SimpleListModel> GetListDefaultFont();

        Task<ThemeModel> CheckThemeIsImporting(long themeId);

        Task<string> GetThemeFileContent(long themeId, string url, bool? isSettingsHtml);

        Task<string> GetThemeFileByFileId(long themeId, long fileId, bool? isSettingHtml);

        Task<List<FileModel>> GetThemeFileUpload(long themeId, FileModel[] fileList);

        Task<List<KeyValuePair<string, string>>> ThemeSettingGetCollectionTitleByUrlHandle(List<string> listUrlHandle);

        Task<List<KeyValuePair<string, string>>> ThemeSettingGetBlogTitleByUrlHandle(List<string> listUrlHandle);

        Task<List<KeyValuePair<string, string>>> ThemeSettingGetPageTitleByUrlHandle(List<string> listUrlHandle);

        Task<(List<SimpleListModel> data, long totalrecord)> ThemeSettingGetListCollection(FilterSearchModel model);

        Task<(List<SimpleListModel> data, long totalrecord)> ThemeSettingGetListBlog(FilterSearchModel model);

        Task<(List<SimpleListModel> data, long totalrecord)> ThemeSettingGetListPage(FilterSearchModel model);

        Task<ThemeFileModel> EditSettingDataContent(long id, string content);

        Task<List<ThemesFree>> GetFreeThemes();

        Task<ThemeModel> SelectThemeInstall(long id);

        Task<ThemeFileModel> UploadThemeAsset(long themeId, string name, IFormFile file);

        Task<bool> ValidateUploadThemeAsset(long themeId, string name);

        Task<FileModel> SettingThemeAsset(long? themeId, string name, int? maxWidth, int? maxHeight, IFormFile file);

        Task<string> InstallTheme(string name, long timestamp, string hash);

        Task<List<ThemeEditorDropdownModel>> GetThemeEditorDropdown();

        #endregion Theme code

        #region OPEN_API

        Task<List<AssetAPIModel>> GetAssetList(long themeId);

        Task<AssetAPIModel> GetAssetDetail(long themeId, int fileType, string fileName);

        Task<bool> DeleteAssetApi(long themeId, int fileType, string fileName);

        Task<AssetAPIModel> CreateOrUpdateAsset(long themeId, Dictionary<string, object> dicInserted);

        Task<List<ThemeAPIModel>> GetListThemesApi();

        Task<ThemeAPIModel> GetThemeDetailApi(long themeId);

        Task<bool> DeleteThemeApi(long themeId);

        Task<ThemeAPIModel> UpdateThemeApi(long themeId, Dictionary<string, object> dicUpdated);

        Task<ThemeAPIModel> CreateThemeApi(Dictionary<string, object> dicUpdated);

        Task<bool> PublishThemeApi(long themeId, BusinessObjects.Enums.ThemeType? type);

        #endregion OPEN_API

        Task<string> GetThemeZipUrlByThemeCode(string code);

        Task<string> GetExportThemeUrl(string key);

        Task<List<string>> GetArticleTemplates();
    }
}