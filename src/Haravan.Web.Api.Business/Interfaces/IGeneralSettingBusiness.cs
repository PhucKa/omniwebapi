﻿using Haravan.Web.Api.BusinessObjects.Models;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public interface IGeneralSettingBusiness
    {
        Task<OnlineStoreSetting> GetOnlineStoreSetting();

        Task<OnlineStoreSetting> UpdateOnlineStoreSetting(OnlineStoreSetting settings);

        #region cấu hình tài khoản , Field

        Task<bool> Update(CheckoutSettingModelWeb model);

        Task<bool> UpdateCustomerAccount(CustomerAccountModel model);

        Task<CheckoutSettingModel> GetByStoreId();

        Task<bool> UpdateFieldSetting(FieldSettingModel model);

        Task<bool> UpdateOrderAdditionalContent(OrderAdditionalContentModel model);

        Task<bool> DisableMultipass(long id);

        Task<bool> EnableMultipass(long id, int checkoutAccountType);

        Task<bool> EnableAccountKit(long id, int checkoutAccountType, string appId, string appSecret, string secret, bool isAccountKitAllowCreateAccount, bool isVerifyPasswordAfterLogin);

        Task<bool> DisableAccountKit(long id);

        #endregion cấu hình tài khoản , Field
    }
}