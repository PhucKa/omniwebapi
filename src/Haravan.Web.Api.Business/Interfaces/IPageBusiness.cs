﻿using BHN.SharedObject.APIDataModel;
using Haravan.Web.Api.BusinessObjects.ESModels;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.BusinessObjects.Models.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public interface IPageBusiness
    {
        Task<PageDetailModel> GetDetail(long id);

        Task<PageDetailModel> GetDetailExcepNavigation(long id);

        Task<string> GetHandleById(long id);

        Task<(List<PageListModel> data, long totalrecord)> GetPageList(bool visible,
                                        bool hidden,
                                        string query,
                                        string order,
                                        string direction,
                                        int page,
                                        int limit);

        Task<(List<ThemeDropdownModel> data, long totalrecord)> GetPageDropdownList(
                                        string query,
                                        int page,
                                        int limit);

        Task<(List<PageListModel> data, long totalrecord)> GetGlobalSearchListPage(FilterSearchModel model);

        Task<(List<PageDetailShortModel> data, long totalrecord)> GetPageList(FilterSearchModel model);

        Task<PageDetailModel> AddNewAsync(PageDetailModel model, List<NavigationDetailModel> linkList);

        Task<PageDetailModel> UpdateAsync(PageDetailModel model, List<NavigationDetailModel> linkList);

        Task<bool> DeletePagesAsync(List<long> pagesIds);

        Task<bool> PublishPages(List<long> pagesIds, bool isPublish);

        Task<bool> DeletePageByIdAsync(long pageId, bool isAddError = true);

        Task<List<DropdownSimpleModel>> GetPageTemplate();

        Task<List<string>> GetPageTemplates();

        Task<(long, List<PageAPIModel>)> GetPageListApi(FilterSearchModel model);

        Task<long> CountPages(FilterSearchModel model);

        Task<PageAPIModel> GetPageDetailApi(long pageId);

        Task<PageAPIModel> AddNewPageApi(Dictionary<string, object> dicInserted);

        Task<PageAPIModel> UpdatePageApi(long pageId, Dictionary<string, object> dicUpdated);

        Task<bool> Init_NewStore();

        Task<PageDetailModel> GetByUrlHandle(string urlHandle);

        Task<List<PageDetailModel>> GetByListUrlHandle(List<string> listUrlHandle);

        Task<long> GetIdByHandleUrl(string handleUrl);

        #region OpenAPI
        Task<(List<PageAPIModel> pages, long totalRecord)> OpenApi_GetPagesList(
                 int limit, int page, long since_id, string title, string handle, DateTime? created_at_min,
                 DateTime? created_at_max, DateTime? updated_at_min, DateTime? updated_at_max,
                 DateTime? published_at_min, DateTime? published_at_max, string published_status, long id
             );
        Task<long> OpenApi_CountPages(
                int limit, int page, long since_id, string title, string handle, DateTime? created_at_min,
                DateTime? created_at_max, DateTime? updated_at_min, DateTime? updated_at_max,
                DateTime? published_at_min, DateTime? published_at_max, string published_status, long id
            );
        Task<PageAPIModel> OpenApi_GetPageDetail(long page_id);
        Task<bool> OpenApi_DeletePage(long page_id);
        Task<PageAPIModel> OpenApi_UpdatePage(long page_id, Dictionary<string, object> model);
        Task<PageAPIModel> OpenApi_AddPage(Dictionary<string, object> model);
        #endregion
    }
}