﻿using Haravan.Web.Api.BusinessObjects.Models.Google;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public interface IGoogleSiteVerificationBusiness
    {
        Task AddApiAsync(GoogleSiteVerificationDetailApiModel model);
        Task DeleteManyAsync();
    }
}