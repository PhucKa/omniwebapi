﻿using BHN.SharedObject.APIDataModel;
using Haravan.Web.Api.BusinessObjects.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public interface IMetafieldBusiness
    {
        Task<MetafieldAPIModel> GetDetailApi(long metafieldId, long? ownerId, string ownerResources);

        Task<bool> DeleteApi(long metafieldId, long? ownerId, string ownerResources);

        Task<List<MetafieldAPIModel>> Omni_GetMetafieldList(
            int limit, int page, DateTime? created_at_min, DateTime? created_at_max,
            DateTime? updated_at_min, DateTime? updated_at_max, string @namespace,
            string key, string value_type, long owner_id, string owner_resource, long since_id, string owner_resources
            );

        Task<long> Omni_CountMetafields(
            int limit, int page, DateTime? created_at_min, DateTime? created_at_max,
            DateTime? updated_at_min, DateTime? updated_at_max, string @namespace,
            string key, string value_type, long owner_id, string owner_resource, long since_id, string owner_resources
            );

        Task<MetafieldAPIModel> Omni_GetMetafieldDetail(long metafieldId, string ownerResource, long ownerId);

        Task<bool> Omni_DeleteMetafield(long metafieldId, string ownerResource, long ownerId);

        Task<MetafieldAPIModel> Omni_AddMetafield(OmniCreateMetafieldRequestModel model);

        Task<MetafieldAPIModel> Omni_UpdateMetafield(long metafieldId, string ownerResource, long ownerId, OmniUpdateMetafieldRequestModel model);

        Task<MetafieldAPIModel> Omni_UpdateMetafield(long metafieldId, OmniUpdateMetafieldRequestModel model);
    }
}