﻿using System.Threading.Tasks;
using BHN.SharedObject.APIDataModel;
using Haravan.Web.Api.BusinessObjects;

namespace Haravan.Web.Api.Business
{
    public interface ICartProxyBusiness
    {
        Task DeleteFromWorker(long app_id);
        Task Delete(long app_id);
        Task<CartProxyAPIModel> Add(CartProxyModel model);
        Task<long?> GetFirstCartProxyId();
        Task<CartProxyModel> GetFirst();
    }
}
