﻿using BHN.SharedObject.APIDataModel;
using Haravan.Web.Api.BusinessObjects.ESModels;
using Haravan.Web.Api.BusinessObjects.Models.Comment;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public interface ICommentBusiness
    {
        Task<(List<CommentListModel> data, long totalrecord)> GetCommentListAsync(
                                                string query,
                                                string order,
                                                string direction,
                                                string status,
                                                int page,
                                                int limit
                                               );

        Task<bool> UpdateStatusManyAsync(long[] ids, int status);

        Task<bool> DeleteCommentsAsync(long[] ids);

        Task<long> GetNumberOfUnapproveAsync();

        Task ApproveByChangeBlogCommentRuleAsync(long blogId);

        Task<(List<CommentListModel> data, long totalrecord)> GetDetailAsync(long commentId, bool spam, bool noSpam, int page, int limit);

        #region Internal

        Task<(List<CommentListModel> data, long totalrecord)> GetCommentListAsync(FilterSearchModel filter);

        Task<List<CommentListModel>> GetDetailAsync(long articleId);

        #endregion Internal

        #region Api

        Task<long> CountCommentsApiAsync(FilterSearchModel model);

        Task<(long, List<CommentAPIModel>)> GetListCommentApiAsync(FilterSearchModel model);

        Task<CommentAPIModel> GetCommentDetailApiAsync(long commentId, bool isDeleted = false);

        Task<CommentAPIModel> UpdateCommentApiAsync(long commentId, Dictionary<string, object> dicUpdated);

        Task<CommentAPIModel> UpdateCommentStatusApiAsync(long commentId, int status);

        Task<CommentAPIModel> UpdateCommentDeletedApiAsync(long commentId, bool isDeleted);

        Task<CommentAPIModel> CreateCommentApiAsync(Dictionary<string, object> dicInserted, CommentDetailModel model);

        #endregion Api

        #region Open API

        Task<(List<CommentAPIModel> comments, long totalRecord)> OpenApi_GetCommentList(int limit, int page, long since_id, DateTime? created_at_min, DateTime? created_at_max, DateTime? updated_at_min, DateTime? updated_at_max,
                                                DateTime? published_at_min, DateTime? published_at_max, string published_status, string fields, string status, long blog_Id, long article_Id);

        Task<long> OpenApi_CountComment(int limit, int page, long since_id, DateTime? created_at_min, DateTime? created_at_max, DateTime? updated_at_min, DateTime? updated_at_max,
                                                DateTime? published_at_min, DateTime? published_at_max, string published_status, string fields, string status, long blog_Id, long article_Id);

        Task<CommentAPIModel> OpenApi_GetCommentDetail(long commentId);

        Task<CommentAPIModel> OpenApi_AddComment(Dictionary<string, object> model);

        Task<CommentAPIModel> OpenApi_UpdateComment(long commentId, Dictionary<string, object> dicUpdated);

        Task<CommentAPIModel> OpenApi_UpdateCommentStatus(long commentId, int status);

        Task<bool> OpenApi_UpdateCommentDeleted(long commentId, bool isDeleted);

        Task<bool> CommentDeletedApiAsync(long commentId);

        Task<bool> CommentRestoreApiAsync(long commentId);

        #endregion Open API
    }
}