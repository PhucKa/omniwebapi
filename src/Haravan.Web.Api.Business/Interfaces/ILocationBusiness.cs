﻿using Haravan.Web.Api.BusinessObjects.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Business
{
    public interface ILocationBusiness
    {
        Task<List<LocationSimpleModel>> GetLocationSimpleList();
        //Task<OnlineStoreSetting> UpdateOnlineStoreSetting(OnlineStoreSetting settings);
    }
}
