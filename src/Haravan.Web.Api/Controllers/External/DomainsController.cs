﻿using Haravan.Libs.Abstractions;
using Haravan.Web.Api.Business;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.Infractstructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Controllers.External
{
    [Route("domains")]
    public class DomainsController : Controller
    {
        private readonly IDomainsBusiness bizDomain;
        private readonly IRequestContext requestContext;

        public DomainsController(
                    IDomainsBusiness bizDomain,
                    IRequestContext requestContext
            )
        {
            this.requestContext = requestContext;
            this.bizDomain = bizDomain;
        }

        [HttpGet]
        [Authorize(Roles = RoleConsts.CanReadDomain)]
        public async Task<IActionResult> GetList()
        {
            var data = await bizDomain.GetDataDomain();
            return Ok(requestContext.ToResponse(data));
        }
        [HttpGet("can_set_primary")]
        [Authorize(Roles = RoleConsts.CanReadDomain)]
        public async Task<IActionResult> GetListDomainCanChangePrimary()
        {
            var data = await bizDomain.ListDomainCanChangePrimary();
            return Ok(requestContext.ToResponse(data));
        }
        [HttpPut("{domain_id}/primary")]
        [Authorize(Roles = RoleConsts.CanWriteDomain)]
        public async Task<IActionResult> SetPrimaryDomain(long domain_id)
        {
            var data = await bizDomain.ChangToPrimaryDomain(domain_id, true);
            return Ok(requestContext.ToResponse(data));
        }
        [HttpDelete("{domainId}")]
        [Authorize(Roles = RoleConsts.CanWriteDomain)]
        public async Task<IActionResult> RemoveDomainById(long domainId)
        {
            var data = await bizDomain.RemoveDomain(domainId);
            return Ok(requestContext.ToResponse(data));
        }
        [HttpPost]
        [Authorize(Roles = RoleConsts.CanWriteDomain)]
        public async Task<IActionResult> AddDomainAsync([FromBody] AddDomainRequest data)
        {
            if (data == null)
            {
                requestContext.AddError("errors.data.no_valid", "Dữ liệu không hợp lệ");
                return BadRequest();
            }
            var result = await bizDomain.AddDomain(data.dmName);
            return Ok(requestContext.ToResponse(result));
        }
        [HttpPost]
        [Route("verify")]
        [Authorize(Roles = RoleConsts.CanWriteDomain)]
        public async Task<IActionResult> VerifyToReclaimDomain([FromBody] VerifyToReclaimDomainRequest model)
        {
            var result = await bizDomain.VerifyToReclaimDomain(model.domainName);
            return Ok(requestContext.ToResponse(result));
        }
        [HttpGet]
        [Route("{userid}/userfordomain")]
        [Authorize(Roles = RoleConsts.CanReadDomain)]
        public async Task<IActionResult> GetUserForDomain(long userId)
        {
            var result = await bizDomain.GetForUpdate(userId);
            return Ok(requestContext.ToResponse(result));
        }
        [HttpGet]
        [Route("ipassigned")]
        [Authorize(Roles = RoleConsts.CanReadDomain)]
        public async Task<IActionResult> GetIpAssignedByStore()
        {
            var result = await bizDomain.GetIpAssignedByStore();
            return Ok(requestContext.ToResponse(result));
        }
        [HttpPut("{domain_id}/https")]
        [Authorize(Roles = RoleConsts.CanWriteDomain)]
        public async Task<IActionResult> AddHttps(long domain_id)
        {
            var result = await bizDomain.UpdateHttps(domain_id);
            return Ok(requestContext.ToResponse(result));
        }
        [HttpGet("checkvalidationdomain")]
        [Authorize(Roles = RoleConsts.CanReadDomain)]
        public async Task<IActionResult> CheckValidationDomainAsync(string domainName)
        {
            var data = await bizDomain.CheckValidationDomain(domainName);
            return Ok(requestContext.ToResponse(data));
        }
        [HttpPut("verify_hrv_ip")]
        [Authorize(Roles = RoleConsts.CanReadDomain)]
        public async Task<IActionResult> VerifyHaravanIp(string domain_name)
        {
            var result = await bizDomain.IsPointToHaravan(domain_name);
            return Ok(requestContext.ToResponse(result));
        }
        [HttpPost("has_spf_record")]
        [Authorize]
        public async Task<IActionResult> HasSpfRecord(string email)
        {
            var result = await bizDomain.HasSpfRecord(email);
            return Ok(requestContext.ToResponse(result));
        }
    }
}