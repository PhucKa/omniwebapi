﻿using Haravan.Libs.Abstractions;
using Haravan.Web.Api.Business;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.ESModels;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.Infractstructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Controllers
{
    [Route("themes")]
    public class ThemeController : Controller
    {
        private readonly IThemeBusiness _bizTheme;
        private readonly IRequestContext _context;
        private readonly INavigationBusiness bizNav;

        public ThemeController(
                    IThemeBusiness bizTheme,
                    IRequestContext requestContext,
                    INavigationBusiness bizNav
            )
        {
            this._context = requestContext;
            this._bizTheme = bizTheme;
            this.bizNav = bizNav;
        }

        [HttpGet]
        [Authorize(Roles = RoleConsts.CanReadTheme)]
        public async Task<ApiResponse<ThemeListModel>> GetThemeList()
        {
            var result = await _bizTheme.GetThemeList();
            return _context.ToResponse(result);
        }

        [HttpPost]
        [Route("ImportTheme")]
        [Authorize(Roles = RoleConsts.CanWriteTheme)]
        public async Task<IActionResult> ImportTheme(IFormFile file)
        {
            var result = await _bizTheme.ImportTheme(file);
            return Ok(_context.ToResponse(result));
        }

        [HttpPost]
        [Route("duplicate/{themeId}")]
        [Authorize(Roles = RoleConsts.CanWriteTheme)]
        public async Task<IActionResult> DuplicateTheme(long themeId)
        {
            var result = await _bizTheme.DuplicateTheme(themeId);
            return Ok(_context.ToResponse(result));
        }
        [HttpPost]
        [Route("duplicate_language/{themeId}")]
        [Authorize(Roles = RoleConsts.CanWriteTheme)]
        public async Task<IActionResult> DuplicateThemeByLanguage([FromBody] DuplicateThemeModel model)
        {
            var result = await _bizTheme.DuplicateThemeByLanguage(model.themeId, model.prefixText);
            return Ok(_context.ToResponse(result));
        }

        [HttpPost]
        [Route("publish/{themeId}")]
        [Authorize(Roles = RoleConsts.CanWriteTheme)]
        public async Task<IActionResult> PublishTheme(long themeId)
        {
            var result = await _bizTheme.PublishTheme(themeId);
            return Ok(_context.ToResponse(result));
        }

        [HttpPost]
        [Route("export/{themeId}")]
        [Authorize(Roles = RoleConsts.CanReadTheme)]
        public async Task<IActionResult> ExportTheme(long themeId)
        {
            var result = await _bizTheme.ExportTheme(themeId);
            return Ok(_context.ToResponse(result));
        }

        [HttpDelete]
        [Route("{themeId}")]
        [Authorize(Roles = RoleConsts.CanWriteTheme)]
        public async Task<IActionResult> DeleteTheme(long themeId)
        {
            var result = await _bizTheme.DeleteTheme(themeId);
            return Ok(_context.ToResponse(result));
        }

        [HttpGet]
        [Route("{themeId}/setting_data")]
        [Authorize(Roles = RoleConsts.CanWriteTheme)]
        public async Task<IActionResult> GetSettingDataFile(long themeId)
        {
            var result = await _bizTheme.GetSettingDataFile(themeId);
            return Ok(_context.ToResponse(result));
        }

        [HttpGet]
        [Route("{themeId}/setting_schema")]
        [Authorize(Roles = RoleConsts.CanReadTheme)]
        public async Task<IActionResult> GetSettingShemaFile(long themeId)
        {
            var result = await _bizTheme.GetSettingSchemaThemeFile(themeId);
            return Ok(_context.ToResponse(result));
        }

        [HttpPut]
        [Route("{themeId}/setting_data")]
        [Authorize(Roles = RoleConsts.CanWriteTheme)]
        public async Task<IActionResult> EditSettingData(long themeId, [FromBody] StringModel model)
        {
            if (model == null)
            {
                _context.AddError("errors.data.no_valid","Dữ liệu không hợp lệ");
                return Ok(_context.ToResponse(new ThemeFileModel()));
            }
            var result = await _bizTheme.EditSettingData(themeId, model);
            return Ok(_context.ToResponse(result));
        }

        [HttpGet]
        [Route("{themeId}/frame_token")]
        [Authorize(Roles = RoleConsts.CanReadTheme)]
        public async Task<IActionResult> GetFrameToken(long themeId)
        {
            var result = await _bizTheme.GetFrameToken(themeId);
            return Ok(_context.ToResponse(result));
        }

        [HttpGet]
        [Route("{themeid}/themeforedit")]
        [Authorize(Roles = RoleConsts.CanReadTheme)]
        public async Task<IActionResult> GetThemeForEdit(long themeid)
        {
            var result = await _bizTheme.GetThemeForEdit(themeid);
            return Ok(_context.ToResponse(result));
        }

        [HttpGet]
        [Route("{themeid}/themefiles")]
        [Authorize(Roles = RoleConsts.CanReadTheme)]
        public async Task<IActionResult> GetThemeFileForEditAsync(long themeid)
        {
            var result = await _bizTheme.GetThemeFileForEdit(themeid);
            return Ok(_context.ToResponse(result));
        }

        [HttpGet]
        [Route("{id}/getthemeforlocale")]
        [Authorize(Roles = RoleConsts.CanReadTheme)]
        public async Task<IActionResult> GetThemeForLocale(long id)
        {
            var result = await _bizTheme.GetThemeForLocale(id);
            return Ok(_context.ToResponse(result));
        }

        [HttpPost]
        [Route("{theme_id}/data_from_locale")]
        [Authorize(Roles = RoleConsts.CanReadTheme)]
        public async Task<IActionResult> LoadLocaleContent(long theme_id, [FromBody] ThemeFileModel data)
        {
            if (data == null
                || data.Id <= 0
                || data.File == null
                || data.File.Id <= 0
                || string.IsNullOrEmpty(data.File.Url))
            {
                _context.AddError("errors.information.no_valid", "Thông tin không hợp lệ.");
                return Ok(_context.ToResponse());
            }

            var result = await _bizTheme.LoadLocaleContent(theme_id, data.Id, data.File.Id);
            return Ok(_context.ToResponse(result));
        }

        [HttpGet]
        [Route("{id}/getthemeforsetting")]
        [Authorize(Roles = RoleConsts.CanReadTheme)]
        public async Task<IActionResult> GetThemeForSetting(long id)
        {
            var result = await _bizTheme.GetThemeForSetting(id);
            return Ok(_context.ToResponse(result));
        }

        [HttpPost]
        [Route("getfileversion")]
        [Authorize(Roles = RoleConsts.CanReadTheme)]
        public async Task<IActionResult> GetFileVersionAsync([FromBody] FileVersionThemeFileModel model)
        {
            var result = await _bizTheme.GetFileVersion(model.Id, model.RootFileId);
            return Ok(_context.ToResponse(result));
        }

        [HttpDelete]
        [Route("{themefileid}/deletethemefile")]
        [Authorize(Roles = RoleConsts.CanWriteTheme)]
        public async Task<IActionResult> DeleteThemeFile(long themefileid)
        {
            var result = await _bizTheme.DeleteThemeFile(themefileid);
            return Ok(_context.ToResponse(result));
        }

        [HttpPost]
        [Route("savethemefile")]
        [Authorize(Roles = RoleConsts.CanWriteTheme)]
        public async Task<IActionResult> SaveThemeFileAsync([FromBody] ThemeFileModelRequest model)
        {
            var result = await _bizTheme.SaveThemeFile(model);
            return Ok(_context.ToResponse(result));
        }

        [HttpPost]
        [Route("savelocalethemefile")]
        [Authorize(Roles = RoleConsts.CanWriteTheme)]
        public async Task<IActionResult> SaveLocaleThemeFileAsync([FromBody] SaveLocaleThemeFileModelRequest model)
        {
            var result = await _bizTheme.SaveLocaleThemeFile(model.themeFile, model.newContent);
            return Ok(_context.ToResponse(result));
        }

        [HttpPost("loadcurrentlocale")]
        [Authorize(Roles = RoleConsts.CanWriteTheme)]
        public async Task<IActionResult> LoadCurrentLocale([FromBody] LoadCurrentLocaleModel model)
        {
            var result = await _bizTheme.LoadCurrentLocale(model.id, model.locale);
            return Ok(_context.ToResponse(result));
        }

        [HttpPost]
        [Route("renametheme")]
        [Authorize(Roles = RoleConsts.CanWriteTheme)]
        public async Task<IActionResult> RenameThemeAsync([FromBody] ThemeNameModel model)
        {
            var result = await _bizTheme.RenameTheme(model.Id, model.Name);
            return Ok(_context.ToResponse(result));
        }

        [HttpPost]
        [Route("renamethemefile")]
        [Authorize(Roles = RoleConsts.CanWriteTheme)]
        public async Task<IActionResult> RenameThemeFileAsync([FromBody] RenameThemeFileRequest model)
        {
            var result = await _bizTheme.RenameThemeFile(model);
            return Ok(_context.ToResponse(result));
        }

        [HttpPost]
        [Route("addnewthemefile")]
        [Authorize(Roles = RoleConsts.CanWriteTheme)]
        public async Task<IActionResult> AddNewThemeFileAsync([FromBody] ThemeFileModelAddRequest model)
        {
            var result = await _bizTheme.AddNewThemeFile(model);
            return Ok(_context.ToResponse(result));
        }

        [HttpGet]
        [Route("getListdefaultfont")]
        [Authorize(Roles = RoleConsts.CanReadTheme)]
        public async Task<IActionResult> GetListDefaultFontAsync()
        {
            var result = await _bizTheme.GetListDefaultFont();
            return Ok(_context.ToResponse(result));
        }

        [HttpGet]
        [Route("{themeId}/checkthemeisimporting")]
        [Authorize(Roles = RoleConsts.CanReadTheme)]
        public async Task<IActionResult> CheckThemeIsImporting(long themeId)
        {
            var result = await _bizTheme.CheckThemeIsImporting(themeId);
            return Ok(_context.ToResponse(result));
        }

        [HttpPost("{theme_id}/{file_id}/content")]
        [Authorize(Roles = RoleConsts.CanReadTheme)]
        public async Task<IActionResult> GetDataFromFileId(long theme_id, long file_id, [FromBody] GetDataFromUrlModel model)
        {
            var result = await _bizTheme.GetThemeFileByFileId(theme_id, file_id, model.is_setting_html);
            return Ok(_context.ToResponse(result));
        }

        [HttpGet]
        [Route("freethemes")]
        [Authorize(Roles = RoleConsts.CanReadTheme)]
        public async Task<IActionResult> GetFreeThemes()
        {
            var result = await _bizTheme.GetFreeThemes();
            return Ok(_context.ToResponse(result));
        }

        [HttpPost]
        [Route("{id}/themeinstall")]
        [Authorize(Roles = RoleConsts.CanWriteTheme)]
        public async Task<IActionResult> SelectThemeInstall(long id)
        {
            var result = await _bizTheme.SelectThemeInstall(id);
            return Ok(_context.ToResponse(result));
        }

        [HttpPost]
        [Route("{themeid}/uploadthemeasset")]
        [Authorize(Roles = RoleConsts.CanWriteTheme)]
        public async Task<IActionResult> UploadThemeAsset(IFormFile file, long themeid)
        {
            var result = await _bizTheme.UploadThemeAsset(themeid, file.FileName, file);
            return Ok(_context.ToResponse(result));
        }

        [HttpPost]
        [Route("validateuploadthemeasset")]
        [Authorize(Roles = RoleConsts.CanReadTheme)]
        public async Task<IActionResult> ValidateUploadThemeAsset([FromBody] ValidateUploadThemeAssetModel model)
        {
            var result = await _bizTheme.ValidateUploadThemeAsset(model.themeId, model.name);
            return Ok(_context.ToResponse(result));
        }

        [HttpPut]
        [Route("{themeId}/settings_data_tmp/reset")]
        [Authorize(Roles = RoleConsts.CanWriteTheme)]
        public async Task<IActionResult> ResetSettingDataTmpContent(long themeId)
        {
            await _bizTheme.ResetSettingDataTmpContent(themeId);
            return Ok(_context.ToResponse());
        }

        [HttpPut]
        [Route("{themeId}/settings_data_tmp")]
        [Authorize(Roles = RoleConsts.CanWriteTheme)]
        public async Task<IActionResult> UpdateSettingDataTmpContent(long themeId, [FromBody] StringModel model)
        {
            if (model == null)
            {
                _context.AddError("errors.data.no_valid","Dữ liệu không hợp lệ");
                return Ok(_context.ToResponse(new ThemeFileModel()));
            }
            await _bizTheme.UpdateSettingDataTmpContent(themeId, model.Value);
            return Ok(_context.ToResponse());
        }

        [HttpPost]
        [Route("{theme_id}/data_from_url")]
        [Authorize(Roles = RoleConsts.CanReadTheme)]
        public async Task<IActionResult> GetDataFromUrl(long theme_id, [FromBody] GetDataFromUrlModel model)
        {
            if (string.IsNullOrEmpty(model.url))
            {
                _context.AddError("errors.data.no_valid","Dữ liệu không hợp lệ");
                return Ok(_context.ToResponse());
            }
            var result = await _bizTheme.GetThemeFileContent(theme_id, model.url, model.is_setting_html);
            return Ok(_context.ToResponse(result));
        }

        [HttpPost]
        [Route("{themeId}/themefileupload")]
        [Authorize(Roles = RoleConsts.CanReadTheme)]
        public async Task<IActionResult> GetCollectionTitleByUrlHandleAsync([FromBody] GetThemeFileUploadModel model)
        {
            var result = await _bizTheme.GetThemeFileUpload(model.themeId, model.fileList);
            return Ok(_context.ToResponse(result));
        }

        [HttpPost]
        [Route("collectiontitlebyurlhandle")]
        [Authorize(Roles = RoleConsts.CanReadTheme)]
        public async Task<IActionResult> GetCollectionTitleByUrlHandleAsync([FromBody] ListUrlHandleModel model)
        {
            var result = await _bizTheme.ThemeSettingGetCollectionTitleByUrlHandle(model.listUrlHandle);
            return Ok(_context.ToResponse(result));
        }

        [HttpPost]
        [Route("blogtitlebyurlhandle")]
        [Authorize(Roles = RoleConsts.CanReadTheme)]
        public async Task<IActionResult> GetBlogTitleByUrlHandleAsync([FromBody] ListUrlHandleModel model)
        {
            var result = await _bizTheme.ThemeSettingGetBlogTitleByUrlHandle(model.listUrlHandle);
            return Ok(_context.ToResponse(result));
        }

        [HttpPost]
        [Route("pagetitlebyurlhandle")]
        [Authorize(Roles = RoleConsts.CanReadTheme)]
        public async Task<IActionResult> GetPageTitleByUrlHandleAsync([FromBody] ListUrlHandleModel model)
        {
            var result = await _bizTheme.ThemeSettingGetPageTitleByUrlHandle(model.listUrlHandle);
            return Ok(_context.ToResponse(result));
        }

        [HttpPost]
        [Route("dropdownlistcollection")]
        [Authorize(Roles = RoleConsts.CanReadTheme)]
        public async Task<IActionResult> LoadDropdownlistCollectionAsync([FromBody] FilterSearchModel model)
        {
            var (data, totalRecord) = await _bizTheme.ThemeSettingGetListCollection(model);
            return Ok(_context.ToResponse(DataPaging.Create(data, totalRecord)));
        }

        [HttpPost]
        [Route("dropdownlistblog")]
        [Authorize(Roles = RoleConsts.CanReadTheme)]
        public async Task<IActionResult> LoadDropdownlistBlogAsync([FromBody] FilterSearchModel model)
        {
            var (data, totalRecord) = await _bizTheme.ThemeSettingGetListBlog(model);
            return Ok(_context.ToResponse(DataPaging.Create(data, totalRecord)));
        }

        [HttpPost]
        [Route("dropdownlistpage")]
        [Authorize(Roles = RoleConsts.CanReadTheme)]
        public async Task<IActionResult> LoadDropdownlistPageAsync([FromBody] FilterSearchModel model)
        {
            var (data, totalRecord) = await _bizTheme.ThemeSettingGetListPage(model);
            return Ok(_context.ToResponse(DataPaging.Create(data, totalRecord)));
        }

        [HttpPost]
        [Route("{theme_id}/settings")]
        [Authorize(Roles = RoleConsts.CanWriteTheme)]
        public async Task<IActionResult> EditSetting(long theme_id, [FromBody] SettingDataContentModel model)
        {
            var result = await _bizTheme.EditSettingDataContent(theme_id, model.content);
            return Ok(_context.ToResponse(result));
        }

        [HttpGet("linklist_themesetting")]
        [Authorize(Roles = RoleConsts.CanReadTheme)]
        public async Task<IActionResult> GetDropdownlistThemeSetting()
        {
            var result = await bizNav.GetDropdownlist_LinkList_ThemeSetting();
            return Ok(_context.ToResponse(result));
        }

        [HttpPost("{theme_id}/settings/asset")]
        [Authorize(Roles = RoleConsts.CanWriteTheme)]
        public async Task<IActionResult> SettingThemeAsset(long theme_id, string name, IFormFile file)
        {
            var result = await _bizTheme.SettingThemeAsset(theme_id, name, null, null, file);
            return Ok(_context.ToResponse(result));
        }

        [HttpGet("install_theme_store")]
        [Authorize(Roles = RoleConsts.CanWriteTheme)]
        public async Task<IActionResult> InstallTheme(string name, long timestamp, string hash)
        {
            if (string.IsNullOrEmpty(name) || timestamp <= 0 || string.IsNullOrEmpty(hash))
            {
                _context.AddError("errors.data.no_valid","Dữ liệu không hợp lệ");
                return Ok(_context.ToResponse());
            }
            var result = await _bizTheme.InstallTheme(name, timestamp, hash);
            return Ok(_context.ToResponse(result));
        }

        [HttpGet("theme_editor/dropdown")]
        [Authorize(Roles = RoleConsts.CanReadTheme)]
        public async Task<IActionResult> GetThemeEditorDropdown()
        {
            var result = await _bizTheme.GetThemeEditorDropdown();
            return Ok(_context.ToResponse(result));
        }

        [HttpGet("export_url")]
        [Authorize(Roles = RoleConsts.CanWriteTheme)]
        public async Task<IActionResult> GetExportThemeUrl(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                _context.AddError("errors.data.no_valid","Dữ liệu không hợp lệ");
                return Ok(_context.ToResponse());
            }
            var result = await _bizTheme.GetExportThemeUrl(key);
            return Ok(_context.ToResponse(result));
        }
    }
}