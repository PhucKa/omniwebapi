﻿using BHN.Core;
using Haravan.Libs.Abstractions;
using Haravan.Web.Api.Business;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.ESModels;
using Haravan.Web.Api.Infractstructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using System.Web;

namespace Haravan.Web.Api.Controllers.External
{
    [Route("globalsearch")]
    public class GlobalSearchController : Controller
    {
        private readonly IGlobalSearchBusiness bizSearch;
        private readonly IRequestContext requestContext;

        public GlobalSearchController(
                    IGlobalSearchBusiness bizSearch,
                    IRequestContext requestContext
            )
        {
            this.requestContext = requestContext;
            this.bizSearch = bizSearch;
        }

        [HttpGet("search")]
        [Authorize(Roles = RoleConsts.CanReadContent)]
        public async Task<IActionResult> GetGlobalSearchList(string query)
        {
            var model = new FilterSearchModel
            {
                FreeText = query,
                Page = new FilterSearchPage()
                {
                    currentPage = 1,
                    pageSize = 20
                }
            };
            model.FreeText = HttpUtility.UrlDecode(model.FreeText);
            var (data, totalRecord) = await bizSearch.GetGlobalSearchList(model);
            return Ok(requestContext.ToResponse(DataPaging.Create(data, totalRecord)));
        }
    }
}