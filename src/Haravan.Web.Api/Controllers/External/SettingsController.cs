﻿using Haravan.Libs.Abstractions;
using Haravan.Web.Api.Business;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.Infractstructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Controllers
{
    [Route("settings")]
    public class SettingController : Controller
    {
        private readonly IGeneralSettingBusiness _bizGeneral;
        private readonly IRequestContext requestContext;
        private readonly ILocationBusiness _bizLocation;
        public SettingController(
                    IGeneralSettingBusiness bizGeneral,
                    IRequestContext requestContext,
                    ILocationBusiness bizLocation
            )
        {
            this.requestContext = requestContext;
            this._bizGeneral = bizGeneral;
            this._bizLocation = bizLocation;
        }

        [HttpGet("onlinestoresetting")]
        [Authorize(Roles = RoleConsts.CanReadSetting)]
        public async Task<ApiResponse<OnlineStoreSetting>> GetOnlineStoreSettingAsync()
        {
            var result = await _bizGeneral.GetOnlineStoreSetting();
            return requestContext.ToResponse(result);
        }

        [HttpPut("onlinestoresetting")]
        [Authorize(Roles = RoleConsts.CanWriteSetting)]
        public async Task<ApiResponse<OnlineStoreSetting>> UpdateOnlineStoreSettingAsync([FromBody] OnlineStoreSetting settings)
        {
            var detail = await _bizGeneral.UpdateOnlineStoreSetting(settings);
            return requestContext.ToResponse(detail);
        }

        [HttpGet("checkout_setting/list")]
        [Authorize(Roles = RoleConsts.CanReadSetting)]
        public async Task<IActionResult> GetCheckoutSettingByStoreId()
        {
            var result = await _bizGeneral.GetByStoreId();
            return Ok(requestContext.ToResponse(result));
        }
        [HttpPut("checkout_setting/update")]
        [Authorize(Roles = RoleConsts.CanWriteSetting)]
        public async Task<IActionResult> CheckoutSetting_Update([FromBody]CheckoutSettingModelWeb model)
        {
            var result = await _bizGeneral.Update(model);
            return Ok(requestContext.ToResponse(result));
        }
        [HttpPut("checkout_setting/customer_account")]
        [Authorize(Roles = RoleConsts.CanWriteSetting)]
        public async Task<IActionResult> CheckoutSetting_UpdateCustomerAccount([FromBody]CustomerAccountModel model)
        {
            var result = await _bizGeneral.UpdateCustomerAccount(model);
            return Ok(requestContext.ToResponse(result));
        }

        [HttpPut("checkout_setting/field_setting")]
        [Authorize(Roles = RoleConsts.CanWriteSetting)]
        public async Task<IActionResult> CheckoutSetting_UpdateFieldSetting([FromBody]FieldSettingModel model)
        {
            var result = await _bizGeneral.UpdateFieldSetting(model);
            return Ok(requestContext.ToResponse(result));
        }

        [HttpPut("checkout_setting/order_content")]
        [Authorize(Roles = RoleConsts.CanWriteSetting)]
        public async Task<IActionResult> CheckoutSetting_UpdateOrderAdditionalContent([FromBody]OrderAdditionalContentModel model)
        {
            var result = await _bizGeneral.UpdateOrderAdditionalContent(model);
            return Ok(requestContext.ToResponse(result));
        }

        [HttpPost("checkout_setting/enable_multipass")]
        [Authorize(Roles = RoleConsts.CanWriteSetting)]
        public async Task<IActionResult> EnableMultipass([FromBody]EnableMultipassRequest model)
        {
            var result =  await _bizGeneral.EnableMultipass(model.id, model.checkoutAccountType);
            return Ok(requestContext.ToResponse(result));
        }

        [HttpPost("checkout_setting/disable_multipass")]
        [Authorize(Roles = RoleConsts.CanWriteSetting)]
        public async Task<IActionResult> DisableMultipass([FromBody] DisableByIdRequest model)
        {
            var result =  await _bizGeneral.DisableMultipass(model.id);
            return Ok(requestContext.ToResponse(result));
        }

        [HttpPost("checkout_setting/enable_accountkit")]
        [Authorize(Roles = RoleConsts.CanWriteSetting)]
        public async Task<IActionResult> EnableAccountKit([FromBody]EnableAccountKitRequest model)
        {
            var result = await _bizGeneral.EnableAccountKit(model.id, model.checkoutAccountType, model.appId, model.appSecret, model.secret, model.isAccountKitAllowCreateAccount, model.isVerifyPasswordAfterLogin);          
            return Ok(requestContext.ToResponse(result));
        }

        [HttpPost("checkout_setting/disable_accountkit")]
        [Authorize(Roles = RoleConsts.CanWriteSetting)]
        public async Task<IActionResult> DisableAccountKit([FromBody] DisableByIdRequest model)
        {
            var result =  await _bizGeneral.DisableAccountKit(model.id);
            return Ok(requestContext.ToResponse(result));
        }
        [HttpGet("locations/available/list")]
        [Authorize(Roles = RoleConsts.CanReadSetting)]
        public async Task<ApiResponse<List<LocationSimpleModel>>> GetAvailableLocationsList()
        {
            var result = await _bizLocation.GetLocationSimpleList();
            return requestContext.ToResponse(result);
        }
    }
}