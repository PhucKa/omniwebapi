﻿using Haravan.Libs.Abstractions;
using Haravan.Web.Api.Business;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.Infractstructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Controllers.External
{
    [Route("announcement")]
    public class AnnouncementController : Controller
    {
        private readonly IGeneralSettingBusiness _bizSetting;
        private readonly IRequestContext requestContext;

        public AnnouncementController(IRequestContext requestContext,
            IGeneralSettingBusiness bizSetting)
        {
            this.requestContext = requestContext;
            _bizSetting = bizSetting;
        }

        [HttpGet("{view}")]
        [Authorize]
        public async Task<IActionResult> GetList(string view)
        {
            var lst = new List<AnnouncementModel>();
            if (string.IsNullOrWhiteSpace(view))
                return Ok(requestContext.ToResponse(lst));
            try
            {
                var objSetting = await _bizSetting.GetOnlineStoreSetting();
                if (objSetting != null)
                {
                    if (objSetting.PasswordProtect != null && objSetting.PasswordProtect == true)
                    {
                        var notipass = new AnnouncementModel()
                        {
                            announcement_type = "information",
                            title = "Website của bạn đang được bảo vệ bởi mật khẩu",
                            content = "Chỉ những khách hàng được cung cấp mật khẩu mới truy cập được Website của bạn.",
                            action = "button",
                            action_content = "Vô hiệu hóa mật khẩu",
                            action_link = "/settings/online_store#tutorial=unlock",
                            code = "password_protect"
                        };
                        lst.Add(notipass);
                    }
                }

                return Ok(requestContext.ToResponse(lst));
            }
            catch
            {
                return Ok(requestContext.ToResponse(lst));
            }
        }
    }
}