﻿using Haravan.Libs.Abstractions;
using Haravan.Web.Api.Business;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Enums;
using Haravan.Web.Api.BusinessObjects.Models.Comment;
using Haravan.Web.Api.BusinessObjects.Models.Common;
using Haravan.Web.Api.Infractstructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Controllers.External
{
    [Route("comments")]
    public class CommentController : Controller
    {
        private readonly ICommentBusiness bizComment;
        private readonly IFilterBusiness bizFilter;
        private readonly IRequestContext requestContext;

        public CommentController(
                    ICommentBusiness bizComment,
                    IRequestContext requestContext,
                    IFilterBusiness bizFilter
            )
        {
            this.requestContext = requestContext;
            this.bizComment = bizComment;
            this.bizFilter = bizFilter;
        }

        [HttpGet]
        [Authorize(Roles = RoleConsts.CanReadContent)]
        public async Task<IActionResult> GetList(
                                                    string query,
                                                    string order,
                                                    string direction,
                                                    string status,
                                                    int page,
                                                    int limit
                                               )
        {
            var (data, totalRecord) = await bizComment.GetCommentListAsync(query, order, direction, status, page, limit);
            return Ok(requestContext.ToResponse(DataPaging.Create(data, totalRecord)));
        }

        [HttpPut("updatestatus")]
        [Authorize(Roles = RoleConsts.CanWriteContent)]
        public async Task<IActionResult> UpdateStatusManyAsync([FromBody] CommentUpdateModel model)
        {
            var rs = await bizComment.UpdateStatusManyAsync(model.ids, model.status);
            return Ok(requestContext.ToResponse(rs));
        }

        [HttpPut("delete")]
        [Authorize(Roles = RoleConsts.CanWriteContent)]
        public async Task<ApiResponse<bool>> DeleteCommentsAsync([FromBody] CommentDeleteModel model)
        {
            var update = await bizComment.DeleteCommentsAsync(model.ids);
            return requestContext.ToResponse(update);
        }

        [HttpGet("filter")]
        [Authorize(Roles = RoleConsts.CanReadContent)]
        public async Task<ApiResponse<ViewFilterData>> GetFilter()
        {
            ViewFilterData data = null;
            data = await bizFilter.GetFilterAsync((int)SysView.Comment);
            return requestContext.ToResponse(data);
        }

        [HttpPost("filter")]
        [Authorize(Roles = RoleConsts.CanWriteContent)]
        public async Task<IActionResult> AddFilter([FromBody] FilterTab tab)
        {
            var rs = await bizFilter.AddFilter(tab);
            return Ok(requestContext.ToResponse(rs));
        }

        [HttpPut("filter")]
        [Authorize(Roles = RoleConsts.CanWriteContent)]
        public async Task<IActionResult> UpdateFilter([FromBody] FilterTab tab)
        {
            var rs = await bizFilter.UpdateFilter(tab);
            return Ok(requestContext.ToResponse(rs));
        }

        [HttpDelete("filter/{tabId}")]
        [Authorize(Roles = RoleConsts.CanWriteContent)]
        public async Task<IActionResult> DeleteFilter(long tabId)
        {
            var rs = await bizFilter.DeleteFilter(tabId);
            return Ok(requestContext.ToResponse(rs));
        }

        [HttpGet("{commentId}")]
        [Authorize(Roles = RoleConsts.CanReadContent)]
        public async Task<IActionResult> GetDetail(long commentId, bool spam, bool noSpam, int page, int limit)
        {
            var (data, totalRecord) = await bizComment.GetDetailAsync(commentId, spam, noSpam, page, limit);
            return Ok(requestContext.ToResponse(DataPaging.Create(data, totalRecord)));
        }
    }
}