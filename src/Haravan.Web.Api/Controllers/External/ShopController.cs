﻿using BHN.SharedObject.EBSMessage;
using Haravan.Libs.Abstractions;
using Haravan.Web.Api.Business;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.Infractstructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Controllers
{
    [Route("shop")]
    public class ShopController : Controller
    {
        private readonly IStoreBusiness _bizStore;
        private readonly IRequestContext _context;

        public ShopController(
                    IStoreBusiness bizStore,
                    IRequestContext requestContext
            )
        {
            this._context = requestContext;
            this._bizStore = bizStore;
        }

        [HttpPost]
        [Authorize(Roles = RoleConsts.CanOwner)]
        public async Task<IActionResult> CreateShop()
        {
            var result = await _bizStore.CreateShop(new CreateShopModel()
            {
                email = _context.UserEmail,
                shop_name = _context.OrgName,
                user_name = _context.UserEmail,
                salechannel = "haraweb"
            });
            return Ok(_context.ToResponse(result));
        }

        [HttpPost]
        [Route("activewebchannel")]
        [Authorize(Roles = RoleConsts.CanOwner)]
        public async Task<IActionResult> ActiveWebChannel()
        {
            var result = await _bizStore.AddSaleChannel(MultiChannelEnums.website.ToString());
            return Ok(_context.ToResponse(result));
        }
    }
}