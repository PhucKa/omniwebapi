﻿using BHN.SharedObject.EBSMessage;
using Haravan.Libs.Abstractions;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.Business;
using Haravan.Web.Api.BusinessObjects.Enums;
using Haravan.Web.Api.BusinessObjects.Mappers;
using Haravan.Web.Api.BusinessObjects.Models.Article;
using Haravan.Web.Api.BusinessObjects.Models.Common;
using Haravan.Web.Api.Infractstructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Controllers.External
{
    [Route("articles")]
    public class ArticleController : Controller
    {
        private readonly IBlogBusiness bizBlog;
        private readonly IArticleBusiness bizArticle;
        private readonly IRequestContext requestContext;
        private readonly IFilterBusiness bizFilter;
        private readonly ISummaryBusiness bizSummary;
        private readonly ICommentBusiness bizComment;

        public ArticleController(
                    IArticleBusiness bizArticle,
                    IBlogBusiness bizBlog,
                    IRequestContext requestContext,
                    ISummaryBusiness bizSummary,
                    IFilterBusiness bizFilter,
                    ICommentBusiness bizComment
            )
        {
            this.requestContext = requestContext;
            this.bizArticle = bizArticle;
            this.bizBlog = bizBlog;
            this.bizFilter = bizFilter;
            this.bizSummary = bizSummary;
            this.bizComment = bizComment;
        }

        [HttpGet]
        [Authorize(Roles = RoleConsts.CanReadContent)]
        public async Task<IActionResult> GetList(
                                                    bool visible,
                                                    bool hidden,
                                                    string query,
                                                    string order,
                                                    string direction,
                                                    int page,
                                                    int limit,
                                                    string blog_ids,
                                                    string user_ids,
                                                    string tag_contains,
                                                    string tag_not_contains,
                                                    bool tag_not_exist)
        {
            var (data, totalRecord) = await bizArticle.GetArticleList(visible, hidden, query, order, direction, page, limit, blog_ids, user_ids, tag_contains, tag_not_contains, tag_not_exist);
            return Ok(requestContext.ToResponse(DataPaging.Create(data, totalRecord)));
        }

        [HttpGet("simple")]
        [Authorize(Roles = RoleConsts.CanReadContent)]
        public async Task<IActionResult> GetSimpleBlogsList(
                                                    string query,
                                                    int page,
                                                    int limit
                                                   )
        {
            var (data, totalRecord) = await bizBlog.GetSimpleBlogsList(false, query, page, limit);
            return Ok(requestContext.ToResponse(DataPaging.Create(data, totalRecord)));
        }

        [HttpPost]
        [Authorize(Roles = RoleConsts.CanWriteContent)]
        public async Task<IActionResult> Add([FromBody] ArticleDetailModel model)
        {
            var data = await bizArticle.AddNewAsync(model);
            return Ok(requestContext.ToResponse(data));
        }

        [HttpPut("{articleId}")]
        [Authorize(Roles = RoleConsts.CanWriteContent)]
        public async Task<IActionResult> Update(long articleId, [FromBody] ArticleDetailModel model)
        {
            var data = await bizArticle.UpdateAsync(model);
            return Ok(requestContext.ToResponse(data));
        }

        [HttpPut("delete")]
        [Authorize(Roles = RoleConsts.CanWriteContent)]
        public async Task<IActionResult> DeleteArticle([FromBody] ListLongModel model)
        {
            var data = false;
            if (model == null || model.values == null || !model.values.Any())
            {
                requestContext.AddError("errors.data.no_valid","Dữ liệu không hợp lệ");
            }
            else
            {
                data = await bizArticle.DeleteArticleAsync(model.values);
            }
            return Ok(requestContext.ToResponse(data));
        }

        [HttpGet]
        [Route("{articleId}/blogs/{blogId}")]
        [Authorize(Roles = RoleConsts.CanReadContent)]
        public async Task<IActionResult> GetDetail(long blogId, long articleId)
        {
            var model = await bizArticle.GetArticleDetail(blogId, articleId);
            return Ok(requestContext.ToResponse(model.ArticleTToModel()));
        }

        [HttpGet("dropdownlistblog")]
        [Authorize(Roles = RoleConsts.CanReadContent)]
        public async Task<IActionResult> GetDropdownlistBlogAsync()
        {
            var data = await bizArticle.GetDropdownlistBlog();
            return Ok(requestContext.ToResponse(data));
        }

        [HttpGet("dropdownlistheme")]
        [Authorize(Roles = RoleConsts.CanReadContent)]
        public async Task<IActionResult> GetDropdownlistThemeAsync()
        {
            var data = await bizArticle.GetDropdownlistTheme();
            return Ok(requestContext.ToResponse(data));
        }

        [HttpGet("filter")]
        [Authorize(Roles = RoleConsts.CanReadContent)]
        public async Task<ApiResponse<ViewFilterData>> GetFilter()
        {
            ViewFilterData result = null;
            result = await bizFilter.GetFilterAsync((int)SysView.Article);
            return requestContext.ToResponse(result);
        }

        [HttpGet(("filter/{tabId}"))]
        [Authorize(Roles = RoleConsts.CanReadContent)]
        public async Task<ApiResponse<FilterTab>> GetTabDetail(long tabId)
        {
            var rs = await bizFilter.GetTabDetail(tabId);
            return requestContext.ToResponse(rs);
        }

        [HttpPost("filter")]
        [Authorize(Roles = RoleConsts.CanWriteContent)]
        public async Task<IActionResult> AddFilter([FromBody] FilterTab tab)
        {
            var rs = await bizFilter.AddFilter(tab);
            return Ok(requestContext.ToResponse(rs));
        }

        [HttpPut("filter")]
        [Authorize(Roles = RoleConsts.CanWriteContent)]
        public async Task<IActionResult> UpdateFilter([FromBody] FilterTab tab)
        {
            var rs = await bizFilter.UpdateFilter(tab);
            return Ok(requestContext.ToResponse(rs));
        }

        [HttpDelete("filter/{tabId}")]
        [Authorize(Roles = RoleConsts.CanWriteContent)]
        public async Task<IActionResult> DeleteFilter(long tabId)
        {
            var rs = await bizFilter.DeleteFilter(tabId);
            return Ok(requestContext.ToResponse(rs));
        }

        [HttpPost("publish")]
        [Authorize(Roles = RoleConsts.CanWriteContent)]
        public async Task<IActionResult> PublishArticles([FromBody] ArticlePublisRequest publish)
        {
            var data = await bizArticle.PublishArticles(publish.articlesIds, publish.isPublish);
            return Ok(requestContext.ToResponse(data));
        }

        [HttpPost("bulk/tags/set")]
        [Authorize(Roles = RoleConsts.CanWriteContent)]
        public async Task<IActionResult> AddTags([FromBody] BulkArticleTagRequest model)
        {
            var data = await bizArticle.AddTags(model.articlesIds, model.Tags);
            return Ok(requestContext.ToResponse(data));
        }

        [HttpPost("bulk/tags/remove")]
        [Authorize(Roles = RoleConsts.CanWriteContent)]
        public async Task<IActionResult> RemoveTags([FromBody] BulkArticleTagRequest model)
        {
            var data = await bizArticle.RemoveTags(model.articlesIds, model.Tags);
            return Ok(requestContext.ToResponse(data));
        }

        [HttpGet("tags")]
        [Authorize(Roles = RoleConsts.CanReadContent)]
        public async Task<IActionResult> GetArticleTags(int limit)
        {
            var data = await bizArticle.GetArticleTags((int)SummaryType.Article_Tag, limit);
            return Ok(requestContext.ToResponse(data));
        }

        [HttpGet("authors")]
        [Authorize(Roles = RoleConsts.CanReadContent)]
        public async Task<IActionResult> GetAuthors()
        {
            var data = await bizArticle.GetAuthorList();
            return Ok(requestContext.ToResponse(data));
        }

        [HttpGet("users/simple")]
        [Authorize(Roles = RoleConsts.CanReadContent)]
        public async Task<IActionResult> GetUsersSimpleList()
        {
            var data = await bizArticle.GetUsersSimpleList();
            return Ok(requestContext.ToResponse(data));
        }

        [HttpGet("users/simple/{userId}")]
        [Authorize(Roles = RoleConsts.CanReadContent)]
        public async Task<IActionResult> GetSimpleUserList(long userId)
        {
            var data = await bizArticle.GetSimpleUserList(userId);
            return Ok(requestContext.ToResponse(data));
        }

        [HttpGet("blog/simple/{blogId}")]
        [Authorize(Roles = RoleConsts.CanReadContent)]
        public async Task<IActionResult> GetSimpleblogList(long blogId)
        {
            var data = await bizBlog.GetSimpleBlogList(blogId);
            return Ok(requestContext.ToResponse(data));
        }

        [HttpDelete("{articleId}")]
        [Authorize(Roles = RoleConsts.CanWriteContent)]
        public async Task<IActionResult> DeleteArticleById(long articleId)
        {
            var rs = await bizArticle.DeleteArticleByIdAsync(articleId);
            return Ok(requestContext.ToResponse(rs));
        }

        [HttpGet("countcomment")]
        [Authorize(Roles = RoleConsts.CanReadContent)]
        public async Task<IActionResult> GetCountCommentAsync()
        {
            var data = await bizComment.GetNumberOfUnapproveAsync();
            return Ok(requestContext.ToResponse(data));
        }

        [HttpPost]
        [Route("{articleId}/image")]
        [Authorize(Roles = RoleConsts.CanWriteContent)]
        public async Task<ApiResponseBase> UploadArticleImage(long articleId)
        {
            var objForm = await Request.ReadFormAsync();
            if (objForm.Files != null && objForm.Files.Any())
            {
                var objFile = objForm.Files[0];
                using (var stream = new MemoryStream())
                {
                    await objFile.CopyToAsync(stream);
                    await bizArticle.UploadArticleImage(articleId, objFile.FileName, stream);
                }
            }
            return requestContext.ToResponse();
        }

        [HttpGet]
        [Route("file/list")]
        [Authorize(Roles = RoleConsts.CanReadContent)]
        public async Task<IActionResult> GetFileList(string query, int page, int limit)
        {
            var result = await bizArticle.GetFileList(query, page, limit);
            return Ok(requestContext.ToResponse(result));
        }

        [HttpGet("images")]
        [Authorize(Roles = RoleConsts.CanReadContent)]
        public async Task<IActionResult> GetProductImagesList(string type, string query, int page, int limit)
        {
            var result = await bizArticle.GetProductFeaturedImages(type, query, page, limit);
            return Ok(requestContext.ToResponse(result));
        }

    }
}