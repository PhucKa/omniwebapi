﻿using Haravan.Libs.Abstractions;
using Haravan.Web.Api.Business;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Configs;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.Infractstructure;
using Haravan.Web.Api.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using static BHN.SharedObject.EBSMessage.RestApi;

namespace Haravan.Web.Api.Controllers
{
    [Route("reports")]
    public class ReportController : Controller
    {
        private readonly IReportBusiness _bizReport;
        private readonly IRequestContext requestContext;
        private readonly AppConfig config;

        public ReportController(
                    IReportBusiness _bizReport,
                    IOptions<AppConfig> config,
                    IRequestContext requestContext
            )
        {
            this.requestContext = requestContext;
            this._bizReport = _bizReport;
            this.config = config.Value;
        }

        [HttpGet]
        [Authorize(Roles = RoleConsts.CanAdmin)]
        public async Task<IActionResult> GetReportList()
        {
            if (config.SwitchReportApi)
            {
                var result = await ReportApiRepository.SendRequestWebAsync<Response<List<ReportScreenTypeModel>>>(this.requestContext, config.ReportApiUrl, $"reports/web/getlist", HttpMethod.Get);
                return Ok(requestContext.ToResponse(result.Data));
            }
            else
            {
                var result = await _bizReport.GetListReportWeb();
                return Ok(requestContext.ToResponse(result));
            }
        }

        [HttpGet("info/{id}")]
        [Authorize(Roles = RoleConsts.CanAdmin)]
        public async Task<IActionResult> GetReportDetailModel(string id)
        {
            if (config.SwitchReportApi)
            {
                var result = await ReportApiRepository.SendRequestWebAsync<Response<ReportScreen>>(this.requestContext, config.ReportApiUrl, $"reports/web/info/{id}", HttpMethod.Get);
                return Ok(requestContext.ToResponse(result.Data));
            }
            else
            {
                var result = await _bizReport.GetReportWebScreenById(id);
                return Ok(requestContext.ToResponse(result));
            }
        }

        [HttpPost]
        [Route("{reportId}/summary")]
        [Authorize(Roles = RoleConsts.CanAdmin)]
        public async Task<IActionResult> GetReportOmniSummaryData(string reportId, [FromBody]query query)
        {
            if (config.SwitchReportApi)
            {
                var result = await ReportApiRepository.SendRequestWebAsync<Response<List<ReportDataColumn>>>(this.requestContext, config.ReportApiUrl, $"reports/web/{reportId}/summary", HttpMethod.Post, new { reportId = reportId, query = query });
                return Ok(requestContext.ToResponse(result.Data));
            }
            else
            {
                var ret = await _bizReport.GetReportSummaryData(reportId, query);
                return Ok(requestContext.ToResponse(ret));
            }
        }

        [HttpPost]
        [Route("{reportId}/query")]
        [Authorize(Roles = RoleConsts.CanAdmin)]
        public async Task<IActionResult> GetReportOmniDataFromModel(string reportId, [FromBody]query query)
        {
            if (config.SwitchReportApi)
            {
                var result = await ReportApiRepository.SendRequestWebAsync<Response<ReportDataViewWebModel>>(this.requestContext, config.ReportApiUrl, $"reports/web/{reportId}/query", HttpMethod.Post, new { reportId = reportId, query = query });
                return Ok(requestContext.ToResponse(result.Data));
            }
            else
            {
                var ret = await _bizReport.GetDataByQueryModelCountLy(reportId, query);
                return Ok(requestContext.ToResponse(ret));
            }
        }
    }
}