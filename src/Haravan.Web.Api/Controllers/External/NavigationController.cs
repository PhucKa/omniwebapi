﻿using Haravan.Libs.Abstractions;
using Haravan.Web.Api.Business;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Enums;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.BusinessObjects.Models.Common;
using Haravan.Web.Api.Infractstructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Controllers.External
{
    [Route("navigations")]
    public class NavigationController : Controller
    {
        private readonly INavigationBusiness bizNav;
        private readonly IFilterBusiness bizFilter;
        private readonly IRequestContext requestContext;

        public NavigationController(
                    INavigationBusiness bizNav,
                    IRequestContext requestContext,
                    IFilterBusiness bizFilter
            )
        {
            this.requestContext = requestContext;
            this.bizNav = bizNav;
            this.bizFilter = bizFilter;
        }

        [HttpGet]
        [Authorize(Roles = RoleConsts.CanReadNavigation)]
        public async Task<IActionResult> GetList(bool is_drag = true)
        {
            var data = await bizNav.GetNavigationList(is_drag);
            return Ok(requestContext.ToResponse(data));
        }


        #region Navigation

        [HttpDelete("{id}")]
        [Authorize(Roles = RoleConsts.CanWriteNavigation)]
        public async Task<IActionResult> DeleteLinkListById(long id)
        {
            var rs = await bizNav.DeleteLinkListById(id);
            return Ok(requestContext.ToResponse(rs));
        }


        [HttpGet]
        [Route("listforlinkto")]
        [Authorize(Roles = RoleConsts.CanReadNavigation)]
        public async Task<IActionResult> GetListForLinkTo()
        {
            var data = await bizNav.GetDropdownlist_ForLinkTo();
            return Ok(requestContext.ToResponse(data));
        }


        [HttpGet]
        [Route("{id}")]
        [Authorize(Roles = RoleConsts.CanReadNavigation)]
        public async Task<IActionResult> Detail(long id)
        {
            var data = await bizNav.GetDetailLinkListWithPath(id);
            return Ok(requestContext.ToResponse(data));
        }

        [HttpPost("dropdownlistbylinkfield")]
        [Authorize(Roles = RoleConsts.CanReadNavigation)]
        public async Task<IActionResult> LoadDropdownlistAsync([FromBody] LoadDropdownlistViewModelNavigation model)
        {
            var (data, totalRecord) = await bizNav.GetDropdownListByLinkFieldNavigation(model.linkFieldType, model.query, model.page, model.limit);
            return Ok(requestContext.ToResponse(DataPaging.Create(data, totalRecord)));
        }

        [HttpPut("{menuid}/addtomenu")]
        [Authorize(Roles = RoleConsts.CanReadNavigation)]
        public async Task<IActionResult> AddToMenu(long menuid)
        {
            var rs = await bizNav.AddToMenu(menuid);
            return Ok(requestContext.ToResponse(rs));
        }
        [HttpPost]
        [Authorize(Roles = RoleConsts.CanWriteNavigation)]
        public async Task<IActionResult> Add([FromBody] NavigationDetailModelPath model)
        {
            var data = await bizNav.AddFromWebChannel(model);
            return Ok(requestContext.ToResponse(data));
        }

        [HttpPut]
        [Authorize(Roles = RoleConsts.CanWriteNavigation)]
        public async Task<IActionResult> Update([FromBody] NavigationDetailModelPath model)
        {
            await bizNav.UpdateFromWebChannel(model);
            return Ok(requestContext.ToResponse());
        }

        #endregion Navigation

        #region Url Redirect

        [HttpGet("urlredirect/list")]
        [Authorize(Roles = RoleConsts.CanReadNavigation)]
        public async Task<IActionResult> GetRedirectListAsync(
                                                    string query,
                                                    int page,
                                                    int limit)
        {
            var (data, totalRecord) = await bizNav.GetRedirectListAsync(query, page, limit);
            return Ok(requestContext.ToResponse(DataPaging.Create(data, totalRecord)));
        }

        [HttpPost("urlredirect")]
        [Authorize(Roles = RoleConsts.CanWriteNavigation)]
        public async Task<IActionResult> AddUrlRedirectAsyncAsync([FromBody] UrlRedirectModel model)
        {
            var add = await bizNav.AddUrlRedirect(model);
            return Ok(requestContext.ToResponse(add));
        }

        [HttpPut]
        [Route("urlredirect/bulk/delete")]
        [Authorize(Roles = RoleConsts.CanWriteNavigation)]
        public async Task<IActionResult> DeleteMany([FromBody] ListLongModel model)
        {
            if (model == null)
            {
                return BadRequest(false);
            }
            var result = await bizNav.DeleteUrlRedirect(model.values);
            return Ok(requestContext.ToResponse(result));
        }

        [HttpDelete("{UrlRedirectId}/urlredirect")]
        [Authorize(Roles = RoleConsts.CanWriteNavigation)]
        public async Task<IActionResult> DeleteUrlRedirectById(long UrlRedirectId)
        {
            var rs = await bizNav.DeleteUrlRedirectByIdAsync(UrlRedirectId);
            return Ok(requestContext.ToResponse(rs));
        }

        [HttpGet("{UrlRedirectId}/urlredirectdetail")]
        [Authorize(Roles = RoleConsts.CanReadNavigation)]
        public async Task<IActionResult> DetailUrlRedirectAsyncAsync(long UrlRedirectId)
        {
            var add = await bizNav.GetByIdUrlRedirect(UrlRedirectId);
            return Ok(requestContext.ToResponse(add));
        }

        [HttpPut("{UrlRedirectId}/urlredirect")]
        [Authorize(Roles = RoleConsts.CanWriteNavigation)]
        public async Task<IActionResult> EditUrlRedirectAsyncAsync([FromBody] UrlRedirectModel model)
        {
            var add = await bizNav.UpdateUrlRedirect(model);
            return Ok(requestContext.ToResponse(add));
        }

        #endregion Url Redirect

        #region Omni
        [HttpPut("linklist")]
        [Authorize(Roles = RoleConsts.CanWriteNavigation)]
        public async Task<IActionResult> UpdateLinkListOrder([FromBody] LoadUpdateLinkOrder request)
        {
            var result = await bizNav.UpdateLinkOrder(request.listRow, request.linkListId);
            return Ok(requestContext.ToResponse(result));
        }
        [HttpGet("linklist/{collectionId}")]
        [Authorize(Roles = RoleConsts.CanReadNavigation)]
        public async Task<IActionResult> GetLinkListByRefIdAsync(long collectionId)
        {
            var result = await bizNav.GetLinkListByRefId_ExceptHandle(collectionId, (int)LinkFieldType.Collection);
            return Ok(requestContext.ToResponse(result));
        }
        #endregion Omni
    }
}