﻿using Haravan.Libs.Abstractions;
using Haravan.Web.Api.Business;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Enums;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.BusinessObjects.Models.Common;
using Haravan.Web.Api.Infractstructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Controllers
{
    [Route("pages")]
    public class PageController : Controller
    {
        private readonly IPageBusiness bizPage;
        private readonly IFilterBusiness bizFilter;
        private readonly IRequestContext requestContext;

        public PageController(
                    IPageBusiness bizPage,
                    IRequestContext requestContext,
                    IFilterBusiness bizFilter
            )
        {
            this.requestContext = requestContext;
            this.bizPage = bizPage;
            this.bizFilter = bizFilter;
        }

        [HttpGet("{pageId}")]
        [Authorize(Roles = RoleConsts.CanReadContent)]
        public async Task<IActionResult> GetDetail(long pageId)
        {
            var objPage = await bizPage.GetDetail(pageId);
            return Ok(requestContext.ToResponse(objPage));
        }

        [HttpGet]
        [Authorize(Roles = RoleConsts.CanReadContent)]
        public async Task<IActionResult> GetList(
                                                    bool visible,
                                                    bool hidden,
                                                    string query,
                                                    string order,
                                                    string direction,
                                                    int page,
                                                    int limit
                                                )
        {
            var (data, totalRecord) = await bizPage.GetPageList(visible, hidden, query, order, direction, page, limit);
            return Ok(requestContext.ToResponse(DataPaging.Create(data, totalRecord)));
        }

        [HttpPost]
        [Authorize(Roles = RoleConsts.CanWriteContent)]
        public async Task<IActionResult> Add([FromBody] PageDetailEcomViewModel model)
        {
            var rs = await bizPage.AddNewAsync(model.pages, model.linkList);
            return Ok(requestContext.ToResponse(rs));
        }

        [HttpPut("{pageId}")]
        [Authorize(Roles = RoleConsts.CanWriteContent)]
        public async Task<IActionResult> Update(long pageId, [FromBody] PageDetailEcomViewModel model)
        {
            var rs = await bizPage.UpdateAsync(model.pages, model.linkList);
            return Ok(requestContext.ToResponse(rs));
        }

        [HttpPut("delete")]
        [Authorize(Roles = RoleConsts.CanWriteContent)]
        public async Task<IActionResult> DeletePages([FromBody] ListLongModel model)
        {
            var rs = await bizPage.DeletePagesAsync(model.values);
            return Ok(requestContext.ToResponse(rs));
        }

        [HttpPost("publish")]
        [Authorize(Roles = RoleConsts.CanWriteContent)]
        public async Task<IActionResult> PublishPages([FromBody] PagePublishRequest publish)
        {
            var rs = await bizPage.PublishPages(publish.pagesIds, publish.isPublish);
            return Ok(requestContext.ToResponse(rs));
        }

        [HttpGet("filter")]
        [Authorize(Roles = RoleConsts.CanReadContent)]
        public async Task<ApiResponse<ViewFilterData>> GetFilter()
        {
            ViewFilterData rs = null;
            rs = await bizFilter.GetFilterAsync((int)SysView.Page);
            return requestContext.ToResponse(rs);
        }

        [HttpGet(("filter/{tabId}"))]
        [Authorize(Roles = RoleConsts.CanReadContent)]
        public async Task<ApiResponse<FilterTab>> GetTabDetail(long tabId)
        {
            var rs = await bizFilter.GetTabDetail(tabId);
            return requestContext.ToResponse(rs);
        }

        [HttpPost("filter")]
        [Authorize(Roles = RoleConsts.CanWriteContent)]
        public async Task<IActionResult> AddFilter([FromBody] FilterTab tab)
        {
            var rs = await bizFilter.AddFilter(tab);
            return Ok(requestContext.ToResponse(rs));
        }

        [HttpPut("filter")]
        [Authorize(Roles = RoleConsts.CanWriteContent)]
        public async Task<IActionResult> UpdateFilter([FromBody] FilterTab tab)
        {
            var rs = await bizFilter.UpdateFilter(tab);
            return Ok(requestContext.ToResponse(rs));
        }

        [HttpDelete("filter/{tabId}")]
        [Authorize(Roles = RoleConsts.CanWriteContent)]
        public async Task<IActionResult> DeleteFilter(long tabId)
        {
            var rs = await bizFilter.DeleteFilter(tabId);
            return Ok(requestContext.ToResponse(rs));
        }

        [HttpGet("theme")]
        [Authorize(Roles = RoleConsts.CanReadContent)]
        public async Task<IActionResult> GetListTheme()
        {
            var rs = await bizPage.GetPageTemplate();
            return Ok(requestContext.ToResponse(rs));
        }

        [HttpDelete("{pageId}")]
        [Authorize(Roles = RoleConsts.CanWriteContent)]
        public async Task<IActionResult> DeletePageById(long pageId)
        {
            var rs = await bizPage.DeletePageByIdAsync(pageId);
            return Ok(requestContext.ToResponse(rs));
        }
    }
}