﻿using Haravan.Web.Api.Repository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Controllers.External
{
    public class MonitorController : Controller
    {
        private readonly IMGBlogRepository rpBlog;
        public MonitorController(IMGBlogRepository rpBlog)
        {
            this.rpBlog = rpBlog;
        }

        [HttpGet]
        [Route("hrv-status")]
        public async Task<IActionResult> hrv_status()
        {
            await Task.CompletedTask;
            return Ok();
        }
        [HttpGet]
        [Route("hrv-warm-up")]
        public async Task<IActionResult> hrv_warm_up()
        {
            try
            {
                await rpBlog.GetById(1, 1);
            }
            catch
            {
            }
            return Ok();
        }
    }
}
