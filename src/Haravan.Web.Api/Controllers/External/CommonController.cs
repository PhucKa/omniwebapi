﻿using Haravan.Libs.Abstractions;
using Haravan.Web.Api.Business;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Enums;
using Haravan.Web.Api.BusinessObjects.Models.Common;
using Haravan.Web.Api.Infractstructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Controllers
{
    [Route("commons")]
    public class CommonController : Controller
    {
        private readonly ILocationBusiness _bizLocation;
        private readonly ICommonBusiness _bizCommon;
        private readonly IRequestContext requestContext;
        private readonly IFileBusiness _bizFile;
        private readonly IFilterBusiness bizFilter;

        public CommonController(
                    ILocationBusiness bizLocation,
                    ICommonBusiness commonBusiness,
                    IFileBusiness bizFile,
                    IRequestContext requestContext,
                    IFilterBusiness bizFilter
            )
        {
            this.requestContext = requestContext;
            this._bizLocation = bizLocation;
            this._bizCommon = commonBusiness;
            this._bizFile = bizFile;
            this.bizFilter = bizFilter;
        }

        [HttpGet("dropdownlistbytype")]
        [Authorize(Roles = RoleConsts.CanAdmin)]
        public async Task<IActionResult> GetDropDownlistByType(int type,
                                                                string query,
                                                                int page,
                                                                int limit)
        {
            var result = await _bizCommon.GetSimpleDropdownListByType(type, query, page, limit);
            return Ok(requestContext.ToResponse(DataPaging.Create(result.datas, result.totalRecord)));
        }

        [HttpGet("handletype")]
        [Authorize(Roles = RoleConsts.CanAdmin)]
        public async Task<IActionResult> GetDetailByHandle(int typeId, string handle)
        {
            var result = await _bizCommon.GetSimpleDetailByType(typeId, handle);
            return Ok(requestContext.ToResponse(result));
        }

        [HttpGet("file/imagelist")]
        [Authorize(Roles = RoleConsts.CanReadFile)]
        public async Task<IActionResult> GetImageList(string query,
                                                                int page,
                                                                int limit)
        {
            var result = await _bizFile.GetImageList(query, limit, page);
            return Ok(requestContext.ToResponse(DataPaging.Create(result.data, result.totalRecord)));
        }

        [HttpPost]
        [Route("upload/file")]
        [Authorize(Roles = RoleConsts.CanWriteFile)]
        public async Task<IActionResult> UploadStorageFile(IFormFile file)
        {
            var result = await _bizFile.UploadStorageFile(file);
            return Ok(requestContext.ToResponse(result));
        }

        [HttpDelete]
        [Route("file/{fileId}")]
        [Authorize(Roles = RoleConsts.CanWriteFile)]
        public async Task<IActionResult> DeleteFileById(long fileId)
        {
            var result = await _bizFile.DeleteFile(fileId);
            return Ok(requestContext.ToResponse(result));
        }

        [HttpPut]
        [Route("file/bulk/delete")]
        [Authorize(Roles = RoleConsts.CanWriteFile)]
        public async Task<IActionResult> BulkDeleteMediaFile([FromBody] ListLongModel model)
        {
            var result = await _bizFile.DeleteAllSelectedFile(model.values);
            return Ok(requestContext.ToResponse(result));
        }

        [HttpPost]
        [Route("storage/image")]
        [Authorize(Roles = RoleConsts.CanWriteFile)]
        public async Task<IActionResult> UploadStorageImage(IFormFile file)
        {
            var result = await _bizFile.UploadStorageImages(file);
            return Ok(requestContext.ToResponse(result));
        }

        [HttpPost]
        [Route("storage/allfile")]
        [Authorize(Roles = RoleConsts.CanWriteFile)]
        public async Task<IActionResult> UploadStorageAllFile(IFormFile file)
        {
            var result = await _bizFile.UploadStorageAllFile(file);
            return Ok(requestContext.ToResponse(result));
        }

        [HttpPost]
        [Route("{productId}/images")]
        [Authorize(Roles = RoleConsts.CanWriteFile)]
        public async Task<IActionResult> UploadStorageImage(long productId)
        {
            var result = await _bizFile.GetProductImagesListByProductId(productId);
            return Ok(requestContext.ToResponse(result));
        }

        [HttpGet("files/filter")]
        [Authorize(Roles = RoleConsts.CanReadContent)]
        public async Task<ApiResponse<ViewFilterData>> GetFilter()
        {
            ViewFilterData rs = null;
            rs = await bizFilter.GetFilterAsync((int)SysView.File);
            return requestContext.ToResponse(rs);
        }

        [HttpGet(("files/filter/{tabId}"))]
        [Authorize(Roles = RoleConsts.CanReadContent)]
        public async Task<ApiResponse<FilterTab>> GetTabDetail(long tabId)
        {
            var rs = await bizFilter.GetTabDetail(tabId);
            return requestContext.ToResponse(rs);
        }

        [HttpPost("files/filter")]
        [Authorize(Roles = RoleConsts.CanWriteContent)]
        public async Task<IActionResult> AddFilter([FromBody] FilterTab tab)
        {
            var rs = await bizFilter.AddFilter(tab);
            return Ok(requestContext.ToResponse(rs));
        }

        [HttpPut("files/filter")]
        [Authorize(Roles = RoleConsts.CanWriteContent)]
        public async Task<IActionResult> UpdateFilter([FromBody] FilterTab tab)
        {
            var rs = await bizFilter.UpdateFilter(tab);
            return Ok(requestContext.ToResponse(rs));
        }

        [HttpDelete("files/filter/{tabId}")]
        [Authorize(Roles = RoleConsts.CanWriteContent)]
        public async Task<IActionResult> DeleteFilter(long tabId)
        {
            var rs = await bizFilter.DeleteFilter(tabId);
            return Ok(requestContext.ToResponse(rs));
        }
    }
}