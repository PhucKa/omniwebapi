﻿using Haravan.Libs.Abstractions;
using Haravan.Web.Api.Business;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Configs;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.Infractstructure;
using Haravan.Web.Api.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using static BHN.SharedObject.EBSMessage.RestApi;

namespace Haravan.Web.Api.Controllers
{
    [Route("dashboard")]
    public class DashboardController : Controller
    {
        private readonly IDashboardBusiness _bizDashboard;
        private readonly IRequestContext requestContext;
        private readonly AppConfig config;

        public DashboardController(
                    IDashboardBusiness _bizDashboard,
                    IRequestContext requestContext,
                    IOptions<AppConfig> config
            )
        {
            this.requestContext = requestContext;
            this._bizDashboard = _bizDashboard;
            this.config = config.Value;
        }

        [HttpGet("sessions")]
        [Authorize(Roles = RoleConsts.CanReadSetting)]
        public async Task<IActionResult> RetrieveSessionReport(DateTime? startdate, DateTime? enddate)
        {
            if (config.SwitchReportApi)
            {
                var result = await ReportApiRepository.SendRequestWebAsync<Response<DashboardAnalyticsSession>>(this.requestContext, config.ReportApiUrl, $"reports/web/sessions", HttpMethod.Post, new
                {
                    startdate,
                    enddate
                });
                return Ok(requestContext.ToResponse(result.Data));
            }
            else
            {
                var result = await _bizDashboard.ViewAnalyticsSession(startdate, enddate);
                return Ok(requestContext.ToResponse(result));
            }
        }

        [HttpGet("durations")]
        [Authorize(Roles = RoleConsts.CanReadSetting)]
        public async Task<IActionResult> AnalyticsViewAnalyticsDurations(DateTime? startdate, DateTime? enddate)
        {
            if (config.SwitchReportApi)
            {
                var result = await ReportApiRepository.SendRequestWebAsync<Response<DashboardAnalyticsDurationsModel>>(this.requestContext, config.ReportApiUrl, $"reports/web/durations", HttpMethod.Post, new
                {
                    startdate,
                    enddate
                });
                return Ok(requestContext.ToResponse(result.Data));
            }
            else
            {
                var result = await _bizDashboard.ViewAnalyticsDurations(startdate, enddate);
                return Ok(requestContext.ToResponse(result));
            }
                
        }

        [HttpGet("frequency")]
        [Authorize(Roles = RoleConsts.CanReadSetting)]
        public async Task<IActionResult> AnalyticsViewAnalyticsFrequency(DateTime? startdate, DateTime? enddate)
        {
            if (config.SwitchReportApi)
            {
                var result = await ReportApiRepository.SendRequestWebAsync<Response<DashboardAnalyticsFrequencyModel>>(this.requestContext, config.ReportApiUrl, $"reports/web/frequency", HttpMethod.Post, new
                {
                    startdate,
                    enddate
                });
                return Ok(requestContext.ToResponse(result.Data));
            }
            else
            {
                var result = await _bizDashboard.ViewAnalyticsFrequency(startdate, enddate);
                return Ok(requestContext.ToResponse(result));
            }
        }

        [HttpGet("platforms")]
        [Authorize(Roles = RoleConsts.CanReadSetting)]
        public async Task<IActionResult> AnalyticsViewAnalyticsPlatforms(DateTime? startdate, DateTime? enddate)
        {
            if (config.SwitchReportApi)
            {
                var result = await ReportApiRepository.SendRequestWebAsync<Response<DashboardAnalyticsPlatformModel>>(this.requestContext, config.ReportApiUrl, $"reports/web/platforms", HttpMethod.Post, new
                {
                    startdate,
                    enddate
                });
                return Ok(requestContext.ToResponse(result.Data));
            }
            else
            {
                var result = await _bizDashboard.ViewAnalyticsPlatform(startdate, enddate);
                return Ok(requestContext.ToResponse(result));
            }
        }

        [HttpGet("location")]
        [Authorize(Roles = RoleConsts.CanReadSetting)]
        public async Task<IActionResult> AnalyticsViewAnalyticsCities(DateTime? startdate, DateTime? enddate)
        {
            if (config.SwitchReportApi)
            {
                var result = await ReportApiRepository.SendRequestWebAsync<Response<DashboardAnalyticsLocationModel>>(this.requestContext, config.ReportApiUrl, $"reports/web/location", HttpMethod.Post, new
                {
                    startdate,
                    enddate
                });
                return Ok(requestContext.ToResponse(result.Data));
            }
            else
            {
                var result = await _bizDashboard.ViewAnalyticsCities(startdate, enddate);
                return Ok(requestContext.ToResponse(result));
            }
        }

        [HttpGet("viewpage")]
        [Authorize(Roles = RoleConsts.CanReadSetting)]
        public async Task<IActionResult> AnalyticsViewAnalyticsViewVisitPage(DateTime? startdate, DateTime? enddate)
        {
            if (config.SwitchReportApi)
            {
                var result = await ReportApiRepository.SendRequestWebAsync<Response<DashboardAnalyticsViewVisitPage>>(this.requestContext, config.ReportApiUrl, $"reports/web/viewpage", HttpMethod.Post, new
                {
                    startdate,
                    enddate
                });
                return Ok(requestContext.ToResponse(result.Data));
            }
            else
            {
                var result = await _bizDashboard.ViewAnalyticsViewVisitPage(startdate, enddate);
                return Ok(requestContext.ToResponse(result));
            }
        }
    }
}