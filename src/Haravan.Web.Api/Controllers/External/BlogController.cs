﻿using Haravan.Libs.Abstractions;
using Haravan.Web.Api.Business;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Enums;
using Haravan.Web.Api.BusinessObjects.Mappers;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.BusinessObjects.Models.Common;
using Haravan.Web.Api.Infractstructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Controllers.External
{
    [Route("blogs")]
    public class BlogController : Controller
    {
        private readonly IBlogBusiness bizBlog;
        private readonly IRequestContext requestContext;
        private readonly IFilterBusiness bizFilter;
        private readonly ISummaryBusiness bizSummary;
        private readonly IArticleBusiness bizArticle;

        public BlogController(
                    IBlogBusiness bizBlog,
                    IFilterBusiness bizFilter,
                    ISummaryBusiness bizSummary,
                    IRequestContext requestContext,
                    IArticleBusiness bizArticle
            )
        {
            this.requestContext = requestContext;
            this.bizBlog = bizBlog;
            this.bizSummary = bizSummary;
            this.bizFilter = bizFilter;
            this.bizArticle = bizArticle;
        }

        [HttpGet]
        [Authorize(Roles = RoleConsts.CanReadContent)]
        public async Task<IActionResult> GetList(bool visible,
                                                bool hidden,
                                                string query,
                                                string order,
                                                string direction,
                                                int page,
                                                int limit,
                                                string collection_type)
        {
            var (data, totalRecord) = await bizBlog.GetBlogList(visible, hidden, query, order, direction, page, limit, collection_type);
            return Ok(requestContext.ToResponse(DataPaging.Create(data, totalRecord)));
        }
        [HttpGet]
        [Route("{blogId}/articles/{articleId}")]
        [Authorize(Roles = RoleConsts.CanReadContent)]
        public async Task<IActionResult> GetDetail(long blogId, long articleId)
        {
            var model = await bizArticle.GetArticleDetail(blogId, articleId);
            return Ok(requestContext.ToResponse(model.ArticleTToModel()));
        }

        [HttpPost]
        [Authorize(Roles = RoleConsts.CanWriteContent)]
        public async Task<IActionResult> Add([FromBody] BlogDetailEcomViewModel datas)
        {
            var data = await bizBlog.AddNewAsync(datas.model, datas.linkList);
            return Ok(requestContext.ToResponse(data));
        }

        [HttpPut("{blogId}")]
        [Authorize(Roles = RoleConsts.CanWriteContent)]
        public async Task<IActionResult> Update(long blogId, [FromBody] BlogDetailEcomViewModel datas)
        {
            var data = await bizBlog.UpdateAsync(datas.model, datas.linkList);
            return Ok(requestContext.ToResponse(data));
        }

        [HttpGet("{blogId}")]
        [Authorize(Roles = RoleConsts.CanReadContent)]
        public async Task<IActionResult> GetDetail(long blogId)
        {
            var model = await bizBlog.GetDetail(blogId);
            var data = model.BlogTToModel();
            return Ok(requestContext.ToResponse(data));
        }

        [HttpGet("filter")]
        [Authorize(Roles = RoleConsts.CanReadContent)]
        public async Task<ApiResponse<ViewFilterData>> GetFilter()
        {
            ViewFilterData data = null;
            data = await bizFilter.GetFilterAsync((int)SysView.Blog);
            return requestContext.ToResponse(data);
        }

        [HttpPost("filter")]
        [Authorize(Roles = RoleConsts.CanWriteContent)]
        public async Task<IActionResult> AddFilter([FromBody] FilterTab tab)
        {
            var rs = await bizFilter.AddFilter(tab);
            return Ok(requestContext.ToResponse(rs));
        }

        [HttpPut("filter")]
        [Authorize(Roles = RoleConsts.CanWriteContent)]
        public async Task<IActionResult> UpdateFilter([FromBody] FilterTab tab)
        {
            var rs = await bizFilter.UpdateFilter(tab);
            return Ok(requestContext.ToResponse(rs));
        }

        [HttpDelete("filter/{tabId}")]
        [Authorize(Roles = RoleConsts.CanWriteContent)]
        public async Task<IActionResult> DeleteFilter(long tabId)
        {
            var rs = await bizFilter.DeleteFilter(tabId);
            return Ok(requestContext.ToResponse(rs));
        }

        [HttpGet("tags")]
        [Authorize(Roles = RoleConsts.CanReadContent)]
        public async Task<IActionResult> GetTags()
        {
            var data = await bizSummary.GetSummaryTotalNames(BHN.SharedObject.EBSMessage.SummaryType.Blog_ArticlesTag);
            return Ok(requestContext.ToResponse(data));
        }

        [HttpPut("delete")]
        [Authorize(Roles = RoleConsts.CanWriteContent)]
        public async Task<IActionResult> DeleteBlogs([FromBody] ListLongModel model)
        {
            var data = false;
            if (model == null || model.values == null || !model.values.Any())
            {
                requestContext.AddError("errors.data.no_valid","Dữ liệu không hợp lệ");
            }
            else
            {
                data = await bizBlog.DeleteBlogId(model.values);
            }
            return Ok(requestContext.ToResponse(data));
        }

        [HttpGet("feedburnerprovider")]
        [Authorize(Roles = RoleConsts.CanReadContent)]
        public async Task<IActionResult> GetListFeedBurnerProvider()
        {
            var data = await bizBlog.GetListFeedburnerSimpleAsync();
            return Ok(requestContext.ToResponse(data));
        }

        [HttpGet("theme")]
        [Authorize(Roles = RoleConsts.CanReadContent)]
        public async Task<IActionResult> GetTemplate()
        {
            var data = await bizBlog.GetTemplateByName();
            return Ok(requestContext.ToResponse(data));
        }

        [HttpDelete("{blogId}")]
        [Authorize(Roles = RoleConsts.CanWriteContent)]
        public async Task<IActionResult> DeleteBlogById(long blogId)
        {
            var rs = await bizBlog.DeleteBlogByIdAsync(blogId);
            return Ok(requestContext.ToResponse(rs));
        }
    }
}