﻿using BHN.SharedObject.APIDataModel;
using Haravan.Libs.Abstractions;
using Haravan.Web.Api.Business;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.Infractstructure;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Controllers.OpenApi
{
    [InternalAccessFilter]
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("internal/open_api/articles")]
    public class ArticleController : Controller
    {
        private readonly IArticleBusiness bizArticle;
        private readonly IServiceProvider serviceProvider;
        private readonly IRequestContext requestContext;

        public ArticleController(
                    IArticleBusiness bizArticle,
                    IRequestContext requestContext,
                    IServiceProvider serviceProvider
            )
        {
            this.serviceProvider = serviceProvider;
            this.requestContext = requestContext;
            this.bizArticle = bizArticle;
        }

        #region Method Get

        [HttpGet]
        public async Task<IActionResult> GetList(long articleId, long blogId, DateTime? created_at_max, DateTime? created_at_min, string fields, int limit, int page,
                                                 int popular, DateTime? published_at_max, DateTime? published_at_min, string published_status, long since_id, DateTime? updated_at_max, DateTime? updated_at_min)
        {
            var (data, totalRecord) = await bizArticle.OpenApi_GetArticlesList(articleId, blogId, created_at_max, created_at_min, fields, limit, page,
                                                  popular, published_at_max, published_at_min, published_status, since_id, updated_at_max, updated_at_min);
            return Ok(serviceProvider.ToResponse(DataPaging.Create(data, totalRecord)));
        }

        [HttpGet]
        [Route("{article_id}")]
        public async Task<IActionResult> GetDetail(long article_id)
        {
            var data = await bizArticle.OpenApi_GetArticleDetail(article_id);
            return Ok(serviceProvider.ToResponse(data));
        }

        [HttpGet]
        [Route("count")]
        public async Task<IActionResult> CountArticles(long articleId, long blogId, DateTime? created_at_max, DateTime? created_at_min, string fields, int limit, int page,
                                                 int popular, DateTime? published_at_max, DateTime? published_at_min, string published_status, long since_id, DateTime? updated_at_max, DateTime? updated_at_min)
        {
            var totalRecord = await bizArticle.OpenApi_CountArticles(articleId, blogId, created_at_max, created_at_min, fields, limit, page,
                                                  popular, published_at_max, published_at_min, published_status, since_id, updated_at_max, updated_at_min);
            return Ok(serviceProvider.ToResponse(totalRecord));
        }

        [HttpGet]
        [Route("authors")]
        public async Task<IActionResult> GetAuthorsList()
        {
            var data = await bizArticle.OpenApi_GetAuthorsList();
            return Ok(serviceProvider.ToResponse(data));
        }

        [HttpGet]
        [Route("tags")]
        public async Task<IActionResult> GetAllTags(long articleId, long blogId, DateTime? created_at_max, DateTime? created_at_min, string fields, int limit, int page,
                                                 int popular, DateTime? published_at_max, DateTime? published_at_min, string published_status, long since_id, DateTime? updated_at_max, DateTime? updated_at_min)
        {
            var data = await bizArticle.OpenApi_GetAllTags(articleId, blogId, created_at_max, created_at_min, fields, limit, page,
                                                  popular, published_at_max, published_at_min, published_status, since_id, updated_at_max, updated_at_min);
            return Ok(serviceProvider.ToResponse(data));
        }

        [HttpGet]
        [Route("blogs/tags")]
        public async Task<IActionResult> GetTagsByBlogAPIAsync(long articleId, long blogId, DateTime? created_at_max, DateTime? created_at_min, string fields, int limit, int page,
                                                 int popular, DateTime? published_at_max, DateTime? published_at_min, string published_status, long since_id, DateTime? updated_at_max, DateTime? updated_at_min)
        {
            var data = await bizArticle.OpenApi_GetTagsByBlogAPIAsync(articleId, blogId, created_at_max, created_at_min, fields, limit, page,
                                                   popular, published_at_max, published_at_min, published_status, since_id, updated_at_max, updated_at_min);
            return Ok(serviceProvider.ToResponse(data));
        }

        #endregion Method Get

        #region Method Post

        [HttpPost]
        [Route("create")]
        public async Task<IActionResult> CreateArticle([FromBody]OpenApiArticleRequestModel model)
        {
            var result = null as ArticleAPIModel;
            if (model == null || model.dicData == null || !model.dicData.Any())
            {
                requestContext.AddError("errors.data.no_valid","Dữ liệu không hợp lệ");
            }
            else
            {
                result = await bizArticle.OpenApi_AddArticle(model.dicData);
            }

            return Ok(serviceProvider.ToResponse(result));
        }

        #endregion Method Post

        #region Method Put

        [HttpPut("{article_id}")]
        public async Task<IActionResult> Update(long article_id, [FromBody]OpenApiArticleRequestModel model)
        {
            var data = await bizArticle.UpdateAPI(article_id, model.dicData);
            return Ok(serviceProvider.ToResponse(data));
        }

        #endregion Method Put

        #region Method Delelte

        [HttpDelete("{article_id}")]
        public async Task<IActionResult> DeleteArticleApiAsync(long article_id)
        {
            var remove = await bizArticle.DeleteArticleApi(article_id);
            return Ok(serviceProvider.ToResponse(remove));
        }

        #endregion Method Delelte
    }
}