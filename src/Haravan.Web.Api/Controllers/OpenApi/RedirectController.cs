﻿using Haravan.Libs.Abstractions;
using Haravan.Web.Api.Business;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.Infractstructure;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Controllers.OpenApi
{
    [InternalAccessFilter]
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("internal/open_api/redirects")]
    public class RedirectController : Controller
    {
        private readonly INavigationBusiness bizRedirect;
        private readonly IServiceProvider serviceProvider;
        private readonly IRequestContext requestContext;

        public RedirectController(
                    INavigationBusiness bizRedirect,
                    IServiceProvider serviceProvider,
                    IRequestContext requestContext
            )
        {
            this.serviceProvider = serviceProvider;
            this.bizRedirect = bizRedirect;
            this.requestContext = requestContext;
        }

        #region Method Get

        [HttpGet]
        public async Task<IActionResult> GetRedirectList(int limit, int page, long since_id, string path, string target, string fields)
        {
            var result = await bizRedirect.OpenApi_GetRedirectList(limit, page, since_id, path, target, fields);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpGet("count")]
        public async Task<IActionResult> CountRedirect(int limit, int page, long since_id, string path, string target, string fields)
        {
            var result = await bizRedirect.OpenApi_CountRedirect(limit, page, since_id, path, target, fields);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpGet("{urlredirect_id}")]
        public async Task<IActionResult> GetRedirectDetail(long urlredirect_id)
        {
            var result = await bizRedirect.OpenApi_GetRedirectDetail(urlredirect_id);
            return Ok(serviceProvider.ToResponse(result));
        }

        #endregion Method Get

        #region Method HttpPut

        [HttpPut("{urlredirect_id}")]
        public async Task<IActionResult> UpdateRedirect(long urlredirect_id, [FromBody]OpenRedirectRequestModel model)
        {
            var result = await bizRedirect.OpenApi_UpdateRedirect(urlredirect_id, model.dicData);
            return Ok(serviceProvider.ToResponse(result));
        }

        #endregion Method HttpPut

        #region Method HttpPost

        [HttpPost]
        public async Task<IActionResult> AddNewRedirect([FromBody]OpenRedirectRequestModel model)
        {
            var result = await bizRedirect.OpenApi_AddRedirect(model.dicData);
            return Ok(serviceProvider.ToResponse(result));
        }

        #endregion Method HttpPost

        #region Method HttpDelete

        [HttpDelete("{urlredirect_id}")]
        public async Task<IActionResult> DeleteRedirect(long urlredirect_id)
        {
            var result = await bizRedirect.OpenApi_RedirectDeleted(urlredirect_id);
            return Ok(serviceProvider.ToResponse(result));
        }

        #endregion Method HttpDelete
    }
}