﻿using Haravan.Web.Api.Business;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.Infractstructure;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Controllers.OpenApi
{
    [InternalAccessFilter]
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("internal/open_api/shop")]
    public class ShopController : Controller
    {
        private readonly ICommonBusiness bizcommon;
        private readonly IServiceProvider serviceProvider;

        public ShopController(
                    ICommonBusiness bizcommon,
                    IServiceProvider serviceProvider
            )
        {
            this.serviceProvider = serviceProvider;
            this.bizcommon = bizcommon;
        }

        [HttpGet]
        public async Task<IActionResult> GetShopInfo()
        {
            var result = await bizcommon.GetShopInfo();
            return Ok(serviceProvider.ToResponse(result));
        }
    }
}