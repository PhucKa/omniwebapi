﻿using BHN.SharedObject.APIDataModel;
using BHN.SharedObject.EBSMessage;
using Haravan.Libs.Abstractions;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.Business;
using Haravan.Web.Api.BusinessObjects.Models.Comment;
using Haravan.Web.Api.Infractstructure;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Controllers.OpenApi
{
    [InternalAccessFilter]
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("internal/open_api/comments")]
    public class CommentController : Controller
    {
        private readonly ICommentBusiness bizComment;
        private readonly IServiceProvider serviceProvider;
        private readonly IRequestContext requestContext;

        public CommentController(
                    ICommentBusiness bizComment,
                    IServiceProvider serviceProvider,
                    IRequestContext requestContext
            )
        {
            this.serviceProvider = serviceProvider;
            this.bizComment = bizComment;
            this.requestContext = requestContext;
        }

        #region Method Get

        [HttpGet]
        public async Task<IActionResult> GetList(int limit, int page, long since_id, DateTime? created_at_min, DateTime? created_at_max, DateTime? updated_at_min, DateTime? updated_at_max,
                                                DateTime? published_at_min, DateTime? published_at_max, string published_status, string fields, string status, long blog_Id, long article_Id)
        {
            var (data, totalRecord) = await bizComment.OpenApi_GetCommentList(limit, page, since_id, created_at_min, created_at_max, updated_at_min, updated_at_max,
                                                 published_at_min, published_at_max, published_status, fields, status, blog_Id, article_Id);
            return Ok(serviceProvider.ToResponse(DataPaging.Create(data, totalRecord)));
        }

        [HttpGet]
        [Route("count")]
        public async Task<IActionResult> CountComment(int limit, int page, long since_id, DateTime? created_at_min, DateTime? created_at_max, DateTime? updated_at_min, DateTime? updated_at_max,
                                                DateTime? published_at_min, DateTime? published_at_max, string published_status, string fields, string status, long blog_Id, long article_Id)
        {
            var totalRecord = await bizComment.OpenApi_CountComment(limit, page, since_id, created_at_min, created_at_max, updated_at_min, updated_at_max,
                                                 published_at_min, published_at_max, published_status, fields, status, blog_Id, article_Id);
            return Ok(serviceProvider.ToResponse(totalRecord));
        }

        [HttpGet]
        [Route("{comment_id}")]
        public async Task<IActionResult> GetDetail(long comment_id)
        {
            var data = await bizComment.OpenApi_GetCommentDetail(comment_id);
            return Ok(serviceProvider.ToResponse(data));
        }

        #endregion Method Get

        #region Method Put

        [HttpPut("{comment_id}")]
        public async Task<IActionResult> Update(long comment_id, [FromBody]OpenApiCommentRequestModel model)
        {
            var data = await bizComment.OpenApi_UpdateComment(comment_id, model.dicData);
            return Ok(serviceProvider.ToResponse(data));
        }

        #endregion Method Put

        #region Method Post

        [HttpPost("{comment_id}/spam")]
        public async Task<IActionResult> UpdateCommentSpam(long comment_id)
        {
            var data = await bizComment.OpenApi_UpdateCommentStatus(comment_id, (int)CommentStatus.Spam);
            return Ok(serviceProvider.ToResponse(data));
        }

        [HttpPost("{comment_id}/not_spam")]
        public async Task<IActionResult> UpdateCommentNotSpam(long comment_id)
        {
            var data = await bizComment.OpenApi_UpdateCommentStatus(comment_id, (int)CommentStatus.Approved);
            return Ok(serviceProvider.ToResponse(data));
        }

        [HttpPost("{comment_id}/approve")]
        public async Task<IActionResult> UpdateCommentApprove(long comment_id)
        {
            var data = await bizComment.OpenApi_UpdateCommentStatus(comment_id, (int)CommentStatus.Approved);
            return Ok(serviceProvider.ToResponse(data));
        }

        [HttpPost("{comment_id}/restore")]
        public async Task<IActionResult> UpdateCommentRestore(long comment_id)
        {
            var data = await bizComment.CommentRestoreApiAsync(comment_id);
            return Ok(serviceProvider.ToResponse(data));
        }

        [HttpPost]
        [Route("create")]
        public async Task<IActionResult> CreateComment([FromBody]OpenApiCommentRequestModel model)
        {
            var result = null as CommentAPIModel;
            if (model == null || model.dicData == null || !model.dicData.Any())
            {
                requestContext.AddError("errors.data.no_valid","Dữ liệu không hợp lệ");
            }
            else
            {
                result = await bizComment.OpenApi_AddComment(model.dicData);
            }

            return Ok(serviceProvider.ToResponse(result));
        }

        #endregion Method Post

        #region Method Delelte

        [HttpDelete("{comment_id}")]
        public async Task<IActionResult> DeleteCommentApiAsync(long comment_id)
        {
            var remove = await bizComment.CommentDeletedApiAsync(comment_id);
            return Ok(serviceProvider.ToResponse(remove));
        }

        #endregion Method Delelte
    }
}