﻿using BHN.SharedObject.APIDataModel;
using Haravan.Libs.Abstractions;
using Haravan.Web.Api.Business;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.Infractstructure;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Controllers.OpenApi
{
    [InternalAccessFilter]
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("internal/open_api/pages")]
    public class PageController : Controller
    {
        private readonly IPageBusiness bizPage;
        private readonly IServiceProvider serviceProvider;
        private readonly IRequestContext requestContext;

        public PageController(
                    IPageBusiness bizPage,
                    IRequestContext requestContext,
                    IServiceProvider serviceProvider
            )
        {
            this.serviceProvider = serviceProvider;
            this.requestContext = requestContext;
            this.bizPage = bizPage;
        }

        [HttpGet]
        public async Task<IActionResult> GetList(
                                                    int limit, int page, long since_id, string title, string handle, DateTime? created_at_min,
                                                    DateTime? created_at_max, DateTime? updated_at_min, DateTime? updated_at_max,
                                                    DateTime? published_at_min, DateTime? published_at_max, string published_status, long id
                                                )
        {
            var (data, totalRecord) = await bizPage.OpenApi_GetPagesList(
                limit, page, since_id, title, handle, created_at_min, created_at_max,
                updated_at_min, updated_at_max, published_at_min, published_at_max, published_status, id);
            return Ok(serviceProvider.ToResponse(DataPaging.Create(data, totalRecord)));
        }

        [HttpGet("count")]
        public async Task<IActionResult> Count(
                                                   int limit, int page, long since_id, string title, string handle, DateTime? created_at_min,
                                                   DateTime? created_at_max, DateTime? updated_at_min, DateTime? updated_at_max,
                                                   DateTime? published_at_min, DateTime? published_at_max, string published_status, long id
                                               )
        {
            var totalRecord = await bizPage.OpenApi_CountPages(
                limit, page, since_id, title, handle, created_at_min, created_at_max,
                updated_at_min, updated_at_max, published_at_min, published_at_max, published_status, id);
            return Ok(serviceProvider.ToResponse(totalRecord));
        }

        [HttpGet("{page_id}")]
        public async Task<IActionResult> GetDetail(long page_id)
        {
            var result = await bizPage.OpenApi_GetPageDetail(page_id);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpPut("{page_id}")]
        public async Task<IActionResult> Update(long page_id, [FromBody]OpenApiPageRequestModel model)
        {
            var result = null as PageAPIModel;
            if (model == null || model.dicData == null || !model.dicData.Any())
            {
                requestContext.AddError("errors.data.no_valid","Dữ liệu không hợp lệ");
            }
            else
            {
                result = await bizPage.OpenApi_UpdatePage(page_id, model.dicData);
            }

            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpPost]
        public async Task<IActionResult> Add([FromBody]OpenApiPageRequestModel model)
        {
            var result = null as PageAPIModel;
            if (model == null || model.dicData == null || !model.dicData.Any())
            {
                requestContext.AddError("errors.data.no_valid","Dữ liệu không hợp lệ");
            }
            else
            {
                result = await bizPage.OpenApi_AddPage(model.dicData);
            }

            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpDelete("{page_id}")]
        public async Task<IActionResult> Delete(long page_id)
        {
            var result = await bizPage.OpenApi_DeletePage(page_id);
            return Ok(serviceProvider.ToResponse(result));
        }
    }
}