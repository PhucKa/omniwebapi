﻿using Haravan.Libs.Abstractions;
using Haravan.Web.Api.Business;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.Infractstructure;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Controllers.OpenApi
{
    [InternalAccessFilter]
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("internal/open_api/themes")]
    public class ThemeController : Controller
    {
        private readonly IThemeBusiness bizTheme;
        private readonly IServiceProvider serviceProvider;
        private readonly IRequestContext requestContext;

        public ThemeController(
                    IThemeBusiness bizTheme,
                    IServiceProvider serviceProvider,
                    IRequestContext requestContext
            )
        {
            this.serviceProvider = serviceProvider;
            this.bizTheme = bizTheme;
            this.requestContext = requestContext;
        }

        #region Asset

        [HttpGet("asset")]
        public async Task<IActionResult> GetAssetList(long theme_id)
        {
            var result = await bizTheme.GetAssetList(theme_id);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpGet("asset/{theme_id}")]
        public async Task<IActionResult> GetAssetDetail(long theme_id, int fileType, string fileName)
        {
            var result = await bizTheme.GetAssetDetail(theme_id, fileType, fileName);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpDelete("asset/{theme_id}")]
        public async Task<IActionResult> DeleteAssetApi(long theme_id, [FromBody]ModelDeleteAssetApi model)
        {
            var result = await bizTheme.DeleteAssetApi(theme_id, model.fileType, model.fileName);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpPut("asset/{theme_id}")]
        public async Task<IActionResult> UpdateAsset(long theme_id, [FromBody]OpenThemeRequestModel model)
        {
            var result = await bizTheme.CreateOrUpdateAsset(theme_id, model.dicData);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpPost("asset/{theme_id}")]
        public async Task<IActionResult> CreateAsset(long theme_id, [FromBody]OpenThemeRequestModel model)
        {
            var result = await bizTheme.CreateOrUpdateAsset(theme_id, model.dicData);
            return Ok(serviceProvider.ToResponse(result));
        }

        #endregion Asset

        #region Themes

        [HttpGet]
        public async Task<IActionResult> GetListThemesApi()
        {
            var result = await bizTheme.GetListThemesApi();
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpGet("{theme_id}")]
        public async Task<IActionResult> GetThemeDetailApi(long theme_id)
        {
            var result = await bizTheme.GetThemeDetailApi(theme_id);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpDelete("{theme_id}")]
        public async Task<IActionResult> DeleteThemeApi(long theme_id)
        {
            var result = await bizTheme.DeleteThemeApi(theme_id);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpPut("{theme_id}")]
        public async Task<IActionResult> UpdateThemeApi(long theme_id, [FromBody]OpenThemeRequestModel model)
        {
            var result = await bizTheme.UpdateThemeApi(theme_id, model.dicData);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpPost("create")]
        public async Task<IActionResult> CreateThemeApi([FromBody]OpenThemeRequestModel model)
        {
            var result = await bizTheme.CreateThemeApi(model.dicData);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpPost("publish/{theme_id}")]
        public async Task<IActionResult> PublishThemeApi(long theme_id, BusinessObjects.Enums.ThemeType? type)
        {
            var result = await bizTheme.PublishThemeApi(theme_id, type);
            return Ok(serviceProvider.ToResponse(result));
        }

        #endregion Themes
    }
}