﻿using Haravan.Web.Api.Business;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.Infractstructure;
using Haravan.Web.Api.BusinessObjects;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Controllers.OpenApi
{
    [Route("internal/open_api/metafields")]
    public class MetafieldController : Controller
    {
        private readonly IServiceProvider serviceProvider;
        private readonly IMetafieldBusiness bizMetafield;

        public MetafieldController(
                IServiceProvider serviceProvider,
                IMetafieldBusiness bizMetafield
            )
        {
            this.serviceProvider = serviceProvider;
            this.bizMetafield = bizMetafield;
        }

        [HttpPost]
        public async Task<IActionResult> AddMetafield([FromBody] OmniCreateMetafieldRequestModel model)
        {
            var result = await bizMetafield.Omni_AddMetafield(model);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpGet]
        public async Task<IActionResult> GetMetafieldList(
            int limit, int page, DateTime? created_at_min, DateTime? created_at_max,
            DateTime? updated_at_min, DateTime? updated_at_max, string @namespace,
            string key, string value_type, long owner_id, string owner_resource, long since_id, string owner_resources
            )
        {
            var result = await bizMetafield.Omni_GetMetafieldList(
                limit, page, created_at_min, created_at_max, updated_at_min,
                updated_at_max, @namespace, key, value_type, owner_id, owner_resource, since_id, owner_resources);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpGet("count")]
        public async Task<IActionResult> CountMetafields(
            int limit, int page, DateTime? created_at_min, DateTime? created_at_max,
            DateTime? updated_at_min, DateTime? updated_at_max, string @namespace,
            string key, string value_type, long owner_id, string owner_resource, long since_id, string owner_resources
            )
        {
            var result = await bizMetafield.Omni_CountMetafields(
                limit, page, created_at_min, created_at_max, updated_at_min,
                updated_at_max, @namespace, key, value_type, owner_id, owner_resource, since_id, owner_resources);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpGet("{metafield_id}/owner_resource/{owner_resource}/owner_resource_id/{owner_resource_id}")]
        public async Task<IActionResult> GetMetafiledDetail(long metafield_id, string owner_resource, long owner_resource_id)
        {
            var result = await bizMetafield.Omni_GetMetafieldDetail(metafield_id, owner_resource, owner_resource_id);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpDelete("{metafield_id}/owner_resource/{owner_resource}/owner_resource_id/{owner_resource_id}")]
        public async Task<IActionResult> DeleteMetafield(long metafield_id, string owner_resource, long owner_resource_id)
        {
            var result = await bizMetafield.Omni_DeleteMetafield(metafield_id, owner_resource, owner_resource_id);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpPut("{metafield_id}/owner_resource/{owner_resource}/owner_resource_id/{owner_resource_id}")]
        public async Task<IActionResult> UpdateMetafield(long metafield_id, string owner_resource, long owner_resource_id, [FromBody] OmniUpdateMetafieldRequestModel model)
        {
            var result = await bizMetafield.Omni_UpdateMetafield(metafield_id, owner_resource, owner_resource_id, model);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpGet("{metafield_id}/common")]
        public async Task<IActionResult> GetCommonMetafieldDetail(long metafield_id)
        {
            var result = await bizMetafield.GetDetailApi(metafield_id, null, null);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpDelete("{metafield_id}/common")]
        public async Task<IActionResult> DeleteCommonMetafield(long metafield_id)
        {
            var result = await bizMetafield.DeleteApi(metafield_id, null, null);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpPut("{metafield_id}/common")]
        public async Task<IActionResult> UpdateCommonMetafield(long metafield_id, [FromBody] OmniUpdateMetafieldRequestModel model)
        {
            var result = await bizMetafield.Omni_UpdateMetafield(metafield_id, model);
            return Ok(serviceProvider.ToResponse(result));
        }
    }
}