﻿using Haravan.Libs.Abstractions;
using Haravan.Web.Api.Business;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.Infractstructure;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Controllers.OpenApi
{
    [InternalAccessFilter]
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("internal/open_api/blogs")]
    public class BlogController : ControllerBase
    {
        private readonly IBlogBusiness bizBlog;
        private readonly IRequestContext requestContext;
        private readonly IServiceProvider serviceProvider;

        public BlogController(
                    IBlogBusiness bizBlog,
                    IRequestContext requestContext,
                    IServiceProvider serviceProvider
           )
        {
            this.requestContext = requestContext;
            this.bizBlog = bizBlog;
            this.serviceProvider = serviceProvider;
        }

        [HttpGet]
        public async Task<IActionResult> GetBlogsList(int page, int limit, long since_id, string handle)
        {
            var result = await bizBlog.OpenApi_GetBlogList(page, limit, since_id, handle);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpGet("count")]
        public async Task<IActionResult> Count(int page, int limit, long since_id, string handle)
        {
            var result = await bizBlog.OpenApi_CountBlogs(page, limit, since_id, handle);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpGet("{blog_id}")]
        public async Task<IActionResult> GetBlogDetail(long blog_id)
        {
            var result = await bizBlog.OpenApi_GetBlogDetail(blog_id);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpPut("{blog_id}")]
        public async Task<IActionResult> UpdateBlog(long blog_id, [FromBody]OpenApiBlogRequestModel model)
        {
            var result = await bizBlog.OpenApi_UpdateBlog(blog_id, model.dicData);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpPost]
        public async Task<IActionResult> AddNewBlog([FromBody]OpenApiBlogRequestModel model)
        {
            var result = await bizBlog.OpenApi_AddNewBlog(model.dicData);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpDelete("{blog_id}")]
        public async Task<IActionResult> DeleteBlog(long blog_id)
        {
            var result = await bizBlog.OpenApi_DeleteBlog(blog_id);
            return Ok(serviceProvider.ToResponse(result));
        }
    }
}