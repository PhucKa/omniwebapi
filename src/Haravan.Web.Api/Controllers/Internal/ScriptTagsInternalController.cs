﻿using BHN.SharedObject.APIDataModel;
using Haravan.Web.Api.Business;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.Infractstructure;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Controllers.Internal
{
    [InternalAccessFilter]
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("internal/script_tags")]
    public class ScriptTagsInternalController : Controller
    {
        private readonly IScriptTagsBusiness bizScriptTag;
        private readonly IServiceProvider serviceProvider;

        public ScriptTagsInternalController(
                    IScriptTagsBusiness bizScriptTag,
                    IServiceProvider serviceProvider
            )
        {
            this.bizScriptTag = bizScriptTag;
            this.serviceProvider = serviceProvider;
        }
        [HttpGet]
        public async Task<IActionResult> GetList(
            int page, int limit, long since_id, DateTime? created_at_min,
            DateTime? created_at_max, DateTime? updated_at_min, DateTime? updated_at_max, string src
            )
        {
            var result = await bizScriptTag.GetList(page, limit, since_id, created_at_min, created_at_max, updated_at_min, updated_at_max, src);
            return Ok(serviceProvider.ToResponse(result));
        }
        [HttpGet("count")]
        public async Task<IActionResult> Count(
            long since_id, DateTime? created_at_min,
            DateTime? created_at_max, DateTime? updated_at_min, DateTime? updated_at_max, string src
            )
        {
            var result = await bizScriptTag.Count(since_id, created_at_min, created_at_max, updated_at_min, updated_at_max, src);
            return Ok(serviceProvider.ToResponse(result));
        }
        [HttpPost]
        public async Task<IActionResult> Add([FromBody] ScriptTagAPIModel model)
        {
            var result = await bizScriptTag.Add(model);
            return Ok(serviceProvider.ToResponse(result));
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> Update(long id, [FromBody] ScriptTagUpdateRequest model)
        {
            var result = await bizScriptTag.Update(id, model);
            return Ok(serviceProvider.ToResponse(result));
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            var result = await bizScriptTag.Delete(id);
            return Ok(serviceProvider.ToResponse(result));
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetDetail(long id)
        {
            var result = await bizScriptTag.GetDetail(id);
            return Ok(serviceProvider.ToResponse(result));
        }
        [HttpDelete("{appId}/uninstall")]
        public async Task<IActionResult> RemoveScriptTagAfterUninstallApp(long appId)
        {
            await bizScriptTag.RemoveScriptTagsByAppId(appId);
            return Ok(serviceProvider.ToResponse());
        }
    }
}