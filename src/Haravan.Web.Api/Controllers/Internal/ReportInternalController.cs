﻿using BHN.SharedObject.APIDataModel;
using Haravan.Web.Api.Business;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Configs;
using Haravan.Web.Api.BusinessObjects.ESModels;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.Infractstructure;
using Haravan.Web.Api.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using static BHN.SharedObject.EBSMessage.RestApi;

namespace Haravan.Web.Api.Controllers.Internal
{
    
    [InternalAccessFilter]
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("internal/reports")]
    public class ReportInternalController : Controller
    {
        private readonly IReportBusiness _bizReport;
        private readonly IServiceProvider serviceProvider;
        private readonly AppConfig config;

        public ReportInternalController(
                    IReportBusiness _bizReport,
                    IOptions<AppConfig> config,
                    IServiceProvider serviceProvider
            )
        {
            this.serviceProvider = serviceProvider;
            this._bizReport = _bizReport;
            this.config = config.Value;
        }

        [HttpGet]        
        public async Task<IActionResult> GetReportList()
        {
            if (config.SwitchReportApi)
            {
                var result = await ReportApiRepository.SendRequestWebAsync<Response<List<ReportScreenTypeModel>>>(null, config.ReportApiUrl, $"reports/web/getlist", HttpMethod.Get);                
                return Ok(serviceProvider.ToResponse(result.Data));
            }
            else
            {
                var result = await _bizReport.GetListReportWeb();
                return Ok(serviceProvider.ToResponse(result));
            }
        }

        [HttpGet("info/{id}")]        
        public async Task<IActionResult> GetReportDetailModel(string id)
        {
            if (config.SwitchReportApi)
            {
                var result = await ReportApiRepository.SendRequestWebAsync<Response<ReportScreen>>(null, config.ReportApiUrl, $"reports/web/info/{id}", HttpMethod.Get);
                return Ok(serviceProvider.ToResponse(result.Data));
            }
            else
            {
                var result = await _bizReport.GetReportWebScreenById(id);
                return Ok(serviceProvider.ToResponse(result));
            }
        }

        [HttpPost]
        [Route("{reportId}/summary")]       
        public async Task<IActionResult> GetReportOmniSummaryData([FromBody] WebReportDataModellRequest model)
        {
            if (config.SwitchReportApi)
            {
                var result = await ReportApiRepository.SendRequestWebAsync<Response<List<ReportDataColumn>>>(null, config.ReportApiUrl, $"reports/web/{model.reportId}/summary", HttpMethod.Post, new { reportId = model.reportId, query = model.query });
                return Ok(serviceProvider.ToResponse(result.Data));
            }
            else
            {
                var result = await _bizReport.GetReportSummaryData(model.reportId, model.query);
                return Ok(serviceProvider.ToResponse(result));
            }
        }

        [HttpPost]
        [Route("{reportId}/query")]        
        public async Task<IActionResult> GetReportOmniDataFromModel([FromBody] WebReportDataModellRequest model)
        {
            if (config.SwitchReportApi)
            {
                var result = await ReportApiRepository.SendRequestWebAsync<Response<ReportDataViewWebModel>>(null, config.ReportApiUrl, $"reports/web/{model.reportId}/query", HttpMethod.Post, new { reportId = model.reportId, query = model.query });
                return Ok(serviceProvider.ToResponse(result.Data));
            }
            else
            {
                var result = await _bizReport.GetDataByQueryModelCountLy(model.reportId, model.query);
                return Ok(serviceProvider.ToResponse(result));
            }
        }
    }
}
