﻿using BHN.SharedObject.APIDataModel;
using Haravan.Web.Api.Business;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.ESModels;
using Haravan.Web.Api.BusinessObjects.Mappers;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.BusinessObjects.Models.Article;
using Haravan.Web.Api.Infractstructure;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Controllers.Internal
{
    [InternalAccessFilter]
    [ApiExplorerSettings(IgnoreApi = false)]
    [Route("internal/articles")]
    public class ArticleInternalController : Controller
    {
        private readonly IArticleBusiness bizArticle;
        private readonly IFilterBusiness bizFilter;
        private readonly ICommentBusiness bizComment;
        private readonly IServiceProvider serviceProvider;
        private readonly IThemeBusiness bizTheme;

        public ArticleInternalController(
                    IArticleBusiness bizArticle,
                    IFilterBusiness bizFilter,
                    ICommentBusiness bizComment,
                    IThemeBusiness bizTheme,
                    IServiceProvider serviceProvider
            )
        {
            this.serviceProvider = serviceProvider;
            this.bizArticle = bizArticle;
            this.bizFilter = bizFilter;
            this.bizComment = bizComment;
            this.bizTheme = bizTheme;
        }

        [HttpPost]
        [Route("list")]
        public async Task<IActionResult> GetList([FromBody]FilterSearchModel model)
        {
            var (data, totalRecord) = await bizArticle.GetArticleList(model);
            return Ok(serviceProvider.ToResponse(DataPaging.Create(data, totalRecord)));
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetDetail(long id)
        {
            var model = await bizArticle.GetDetail(id);
            var data = model.ArticleDetaiToModelInternal();
            return Ok(serviceProvider.ToResponse(data));
        }

        [HttpGet]
        [Route("countcomment")]
        public async Task<IActionResult> GetCountCommentAsync()
        {
            var data = await bizComment.GetNumberOfUnapproveAsync();
            return Ok(serviceProvider.ToResponse(data));
        }
        [HttpGet]
        [Route("firstbyhandle")]
        public async Task<IActionResult> GetArticleFirstByHandle()
        {
            var data = await bizArticle.GetArticleFirstByHandle();
            return Ok(serviceProvider.ToResponse(data));
        }

        [HttpGet]
        [Route("blogs")]
        public async Task<IActionResult> GetDropdownlistDetailAsync()
        {
            var result = await bizArticle.GetDropdownlistDetail();
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpPost]
        public async Task<IActionResult> Add([FromBody] ArticleDetailModel _model)
        {
            var data = await bizArticle.AddNewAsync(_model);
            return Ok(serviceProvider.ToResponse(data.Id));
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromBody] ArticleDetailModel _model)
        {
            var data = await bizArticle.UpdateAsync(_model);
            return Ok(serviceProvider.ToResponse(data.Id));
        }

        [HttpPut]
        [Route("bulk/delete")]
        public async Task<IActionResult> DeleteMany([FromBody] ListLong lstPageIds)
        {
            if (lstPageIds == null)
            {
                return BadRequest(false);
            }
            var result = await bizArticle.DeleteArticleAsync(lstPageIds.lstlong);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpPut("bulk/publish/{isPublish}")]
        public async Task<IActionResult> PublishMany(bool isPublish, [FromBody] ListLong lstArticleIds)
        {
            if (lstArticleIds == null)
            {
                return BadRequest(false);
            }
            var result = await bizArticle.PublishArticles(lstArticleIds.lstlong, isPublish);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpPost("bulk/tags/set")]
        public async Task<IActionResult> AddTags([FromBody] BulkArticleTagRequest model)
        {
            var data = await bizArticle.AddTags(model.articlesIds, model.Tags);
            return Ok(serviceProvider.ToResponse(data));
        }

        [HttpPost("bulk/tags/remove")]
        public async Task<IActionResult> RemoveTags([FromBody] BulkArticleTagRequest model)
        {
            var data = await bizArticle.RemoveTags(model.articlesIds, model.Tags);
            return Ok(serviceProvider.ToResponse(data));
        }

        [HttpGet("dropdownlistblog")]
        public async Task<IActionResult> GetDropdownlistBlogAsync()
        {
            var data = await bizArticle.GetDropdownlistBlog();
            return Ok(serviceProvider.ToResponse(data));
        }

        [HttpGet("dropdownlistheme")]
        public async Task<IActionResult> GetDropdownlistThemeAsync()
        {
            var data = await bizTheme.GetArticleTemplates();
            return Ok(serviceProvider.ToResponse(data));
        }

        [HttpGet("comments")]
        public async Task<IActionResult> Getcomments(long articleId)
        {
            var data = await bizComment.GetDetailAsync(articleId);
            return Ok(serviceProvider.ToResponse(data));
        }

        #region API

        [HttpPost("external/list")]
        public async Task<IActionResult> GetApiList([FromBody]FilterSearchModel model)
        {
            (List<ArticleAPIModel> articles, long totalRecord) = await bizArticle.GetArticleAPIList(model);
            return Ok(serviceProvider.ToResponse(DataPaging.Create(articles, totalRecord)));
        }

        [HttpPost("external/detail")]
        public async Task<IntApiResponse<ArticleAPIModel>> GetDetailByIdAPIAsync([FromBody]FilterSearchModel model)
        {
            var result = await bizArticle.GetDetailByIdAPI(model);
            return serviceProvider.ToResponse(result);
        }

        [HttpPost("external/articles")]
        public async Task<IntApiResponse<ArticleAPIModel>> AddNewAPIAsync([FromBody]ArticleDetailModel model)
        {
            var result = await bizArticle.AddNewAPI(model);
            return serviceProvider.ToResponse(result);
        }

        [HttpPut("external/articles")]
        public async Task<IntApiResponse<ArticleAPIModel>> UpdateAPIAsync([FromBody] ArticleDetailApiModel model)
        {
            var update = await bizArticle.UpdateAPI(model.articleId, model.updatedData);
            return serviceProvider.ToResponse(update);
        }

        [HttpGet("external/articles/authors")]
        public async Task<IntApiResponse<List<string>>> GetAuthorListAPIAsync()
        {
            var authors = await bizArticle.GetAuthorListAPI();
            return serviceProvider.ToResponse(authors);
        }

        [HttpPost("external/articles/tags")]
        public async Task<IntApiResponse<List<string>>> GetAllTagsAPIAsync([FromBody] ArticleTagsApiModel model)
        {
            var tags = await bizArticle.GetAllTagsAPI(model.filter, model.popular);
            return serviceProvider.ToResponse(tags);
        }

        [HttpPost("external/articles/blogs/tags")]
        public async Task<IntApiResponse<List<string>>> GetTagsByBlogAPIAsync([FromBody] ArticleTagsApiModel model)
        {
            var tags = await bizArticle.GetTagsByBlogAPI(model.filter, model.popular);
            return serviceProvider.ToResponse(tags);
        }

        [HttpDelete("external/articles/{articleId}")]
        public async Task<IntApiResponse<bool>> DeleteArticleApiAsync(long articleId)
        {
            var remove = await bizArticle.DeleteArticleApi(articleId);
            return serviceProvider.ToResponse(remove);
        }

        #endregion API

        [HttpGet]
        [Route("init")]
        public async Task<IntApiResponse<bool>> InitArticle()
        {
            var result = await bizArticle.Init_NewStore();
            return serviceProvider.ToResponse(true);
        }
    }
}