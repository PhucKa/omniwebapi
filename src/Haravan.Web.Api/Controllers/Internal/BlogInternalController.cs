﻿using BHN.SharedObject.APIDataModel;
using Haravan.Web.Api.Business;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.ESModels;
using Haravan.Web.Api.BusinessObjects.Mappers;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.BusinessObjects.Models.Common;
using Haravan.Web.Api.Infractstructure;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Controllers.Internal
{
    [InternalAccessFilter]
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("internal/blogs")]
    public class BlogInternalController : Controller
    {
        private readonly IBlogBusiness bizBlog;
        private readonly IFilterBusiness bizFilter;
        private readonly IServiceProvider serviceProvider;

        public BlogInternalController(
                    IBlogBusiness bizBlog,
                    IFilterBusiness bizFilter,
                    IServiceProvider serviceProvider
            )
        {
            this.serviceProvider = serviceProvider;
            this.bizBlog = bizBlog;
            this.bizFilter = bizFilter;
        }

        [HttpGet("{blogId}")]
        public async Task<IActionResult> GetDetail(long blogId)
        {
            var objPage = await bizBlog.GetDetail(blogId);
            var data = objPage.BlogDetaiToModelInternal();
            return Ok(serviceProvider.ToResponse(data));
        }

        [HttpGet("handle/{blogid}")]
        public async Task<IActionResult> GetHandle(long blogid)
        {
            var handle = await bizBlog.GetHandleById(blogid);

            return Ok(serviceProvider.ToResponse(handle));
        }

        [HttpGet("urlhandle/{urlhandle}")]
        public async Task<IActionResult> GetBlogByHandle(string urlhandle)
        {
            var blog = await bizBlog.GetByUrlHandle(urlhandle);

            return Ok(serviceProvider.ToResponse(blog));
        }

        [HttpPost("urlhandle")]
        public async Task<IActionResult> GetBlogsByHandles([FromBody]List<string> urlhandles)
        {
            var blog = await bizBlog.GetListByListHandleUrl(urlhandles);
            return Ok(serviceProvider.ToResponse(blog));
        }
        [HttpGet]
        [Route("firstbyhandle")]
        public async Task<IActionResult> GetArticleFirstByHandle()
        {
            var data = await bizBlog.GetBlogFirstByHandle();
            return Ok(serviceProvider.ToResponse(data));
        }

        [HttpPost]
        [Route("list")]
        public async Task<IActionResult> GetList([FromBody]FilterSearchModel model)
        {
            var (data, totalRecord) = await bizBlog.GetBlogList(model);
            return Ok(serviceProvider.ToResponse(DataPaging.Create(data, totalRecord)));
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody]BlogDetailInternalViewModel model)
        {
            var result = await bizBlog.AddNewAsync(model.model, model.linkList);
            return Ok(serviceProvider.ToResponse(result.Id));
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromBody]BlogDetailInternalViewModel model)
        {
            var result = await bizBlog.UpdateAsync(model.model, model.linkList);
            if (result != null && result.Id > 0)
                return Ok(serviceProvider.ToResponse(true));
            else
                return Ok(serviceProvider.ToResponse(false));
        }

        [HttpPut("bulk/delete")]
        public async Task<IActionResult> DeleteMany([FromBody] ListLong lstPageIds)
        {
            if (lstPageIds == null)
            {
                return BadRequest(false);
            }
            var result = await bizBlog.DeleteBlogId(lstPageIds.lstlong);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpGet("dropdownlist")]
        public async Task<IntApiResponse<List<DropdownSimpleModel>>> GetDropdownlistAsync()
        {
            var list = await bizBlog.GetListFeedburnerSimpleAsync();
            return serviceProvider.ToResponse(list);
        }

        [HttpGet("template")]
        public async Task<IActionResult> GetPageTemplate()
        {
            var result = await bizBlog.GetTemplateName();
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpPost]
        [Route("json/list")]
        public async Task<IActionResult> GetApiList([FromBody]FilterSearchModel model)
        {
            var result = await bizBlog.GetBlogListAPI(model);
            return Ok(serviceProvider.ToResponse(DataPaging.Create(result, 0)));
        }

        [HttpPost("json/count")]
        public async Task<IntApiResponse<long>> CountBlogApi([FromBody] FilterSearchModel filter)
        {
            var result = await bizBlog.CountBlogs(filter);
            return serviceProvider.ToResponse(result);
        }

        [HttpGet("json/{blogId}")]
        public async Task<IntApiResponse<BlogAPIModel>> GetPageAPIDetail(long blogId)
        {
            var result = await bizBlog.GetDetailApi(blogId);
            return serviceProvider.ToResponse(result);
        }

        [HttpDelete("json/{blogId}")]
        public async Task<IntApiResponse<bool>> DeleteBlogAPIAsync(long blogId)
        {
            var result = await bizBlog.DeleteBlogByIdAsync(blogId);
            return serviceProvider.ToResponse(result);
        }

        [HttpPost("json")]
        public async Task<IntApiResponse<BlogAPIModel>> AddNewAPIAsync([FromBody] BlogDetailModel model)
        {
            var result = await bizBlog.AddNewApiAsync(model);
            return serviceProvider.ToResponse(result);
        }

        [HttpPut("json")]
        public async Task<IntApiResponse<BlogAPIModel>> UpdateAPIAsync([FromBody] APIRequestUpdateModel model)
        {
            var result = await bizBlog.UpdateAPI(model.Id, model.Data);
            return serviceProvider.ToResponse(result);
        }

        [HttpGet]
        [Route("init")]
        public async Task<IntApiResponse<bool>> InitBlog()
        {
            var result = await bizBlog.Init_NewStore();
            return serviceProvider.ToResponse(true);
        }

        [HttpGet]
        [Route("{urlHandle}")]
        public async Task<IActionResult> GetByUrlHandle(string urlHandle)
        {
            var data = await bizBlog.GetByUrlHandle(urlHandle);
            return Ok(serviceProvider.ToResponse(data));
        }

        [HttpPost]
        [Route("urlHandle")]
        public async Task<IActionResult> GetByListUrlHandle([FromBody] BlogDetailModelToEcom model)
        {
            var data = await bizBlog.GetByListUrlHandle(model.listUrlHandle);
            return Ok(serviceProvider.ToResponse(data));
        }

        [HttpPost]
        [Route("list/urlhandle")]
        public async Task<IActionResult> GetListUrlHandle([FromBody] List<string> listUrlHandle)
        {
            var data = await bizBlog.GetByListUrlHandle(listUrlHandle);
            return Ok(serviceProvider.ToResponse(data));
        }

        [HttpGet]
        [Route("urlHandle/{handleUrl}")]
        public async Task<IActionResult> GetIdByHandleUrl(string handleUrl)
        {
            var data = await bizBlog.GetIdByHandleUrl(handleUrl);
            return Ok(serviceProvider.ToResponse(data));
        }

        [HttpGet("detail/{blogId}")]
        public async Task<IActionResult> GetDetailId(long blogId)
        {
            var objPage = await bizBlog.GetDetailExcepNavigation(blogId);
            var data = objPage.BlogDetaiToModelInternal();
            return Ok(serviceProvider.ToResponse(data));
        }
    }
}