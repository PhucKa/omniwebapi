﻿using BHN.SharedObject.APIDataModel;
using Haravan.Web.Api.Business;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.ESModels;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.Infractstructure;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Controllers.Internal
{
    [InternalAccessFilter]
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("internal/pages")]
    public class PageInternalController : Controller
    {
        private readonly IPageBusiness bizPage;
        private readonly IFilterBusiness bizFilter;

        private readonly IServiceProvider serviceProvider;

        public PageInternalController(
                    IPageBusiness bizPage,
                     IServiceProvider serviceProvider,
                    IFilterBusiness bizFilter
            )
        {
            this.serviceProvider = serviceProvider;
            this.bizPage = bizPage;
            this.bizFilter = bizFilter;
        }

        [HttpGet("{pageId}")]
        public async Task<IActionResult> GetDetail(long pageId)
        {
            var objPage = await bizPage.GetDetail(pageId);
            return Ok(serviceProvider.ToResponse(objPage));
        }

        [HttpPost]
        [Route("list")]
        public async Task<IActionResult> GetList([FromBody]FilterSearchModel model)
        {
            var (data, totalRecord) = await bizPage.GetPageList(model);
            return Ok(serviceProvider.ToResponse(DataPaging.Create(data, totalRecord)));
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody]PageRequestModel model)
        {
            var result = await bizPage.AddNewAsync(model.pages, model.linkList);
            return Ok(serviceProvider.ToResponse(result.Id));
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromBody]PageRequestModel model)
        {
            var result = await bizPage.UpdateAsync(model.pages, model.linkList);
            return Ok(serviceProvider.ToResponse(true));
        }

        [HttpPut("bulk/delete")]
        public async Task<IActionResult> DeleteMany([FromBody] ListLong lstPageIds)
        {
            if (lstPageIds == null)
            {
                return BadRequest(false);
            }
            var result = await bizPage.DeletePagesAsync(lstPageIds.lstlong);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpPut("bulk/publish/{isPublish}")]
        public async Task<IActionResult> PublishMany(bool isPublish, [FromBody] ListLong lstPageIds)
        {
            if (lstPageIds == null)
            {
                return BadRequest(false);
            }
            var result = await bizPage.PublishPages(lstPageIds.lstlong, isPublish);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpGet("template")]
        public async Task<IActionResult> GetPageTemplate()
        {
            var result = await bizPage.GetPageTemplates();
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpPost]
        [Route("external/list")]
        public async Task<IActionResult> GetApiList([FromBody]FilterSearchModel model)
        {
            var (totalRecord, data) = await bizPage.GetPageListApi(model);
            return Ok(serviceProvider.ToResponse(DataPaging.Create(data, totalRecord)));
        }

        [HttpPost("external/count")]
        public async Task<IntApiResponse<long>> CountPagesApi([FromBody] FilterSearchModel filter)
        {
            var result = await bizPage.CountPages(filter);
            return serviceProvider.ToResponse(result);
        }

        [HttpGet("external/page/{pageId}")]
        public async Task<IntApiResponse<PageAPIModel>> GetPageAPIDetail(long pageId)
        {
            var result = await bizPage.GetPageDetailApi(pageId);
            return serviceProvider.ToResponse(result);
        }

        [HttpDelete("external/page/{pageId}")]
        public async Task<IntApiResponse<bool>> DeletePageAPIAsync(long pageId)
        {
            var remove = await bizPage.DeletePageByIdAsync(pageId);
            return serviceProvider.ToResponse(remove);
        }

        [HttpPost("external/page")]
        public async Task<IntApiResponse<PageAPIModel>> AddNewAPIAsync([FromBody] APIRequestModel model)
        {
            var add = await bizPage.AddNewPageApi(model.updatedData);
            return serviceProvider.ToResponse(add);
        }

        [HttpPut("external/page")]
        public async Task<IntApiResponse<PageAPIModel>> UpdateAPIAsync([FromBody] APIRequestUpdateModel model)
        {
            var add = await bizPage.UpdatePageApi(model.Id, model.Data);
            return serviceProvider.ToResponse(add);
        }

        [HttpGet]
        [Route("init")]
        public async Task<IntApiResponse<bool>> InitPage()
        {
            var result = await bizPage.Init_NewStore();
            return serviceProvider.ToResponse(true);
        }

        [HttpGet("handle/{pageid}")]
        public async Task<IActionResult> GetHandle(long pageid)
        {
            var handle = await bizPage.GetHandleById(pageid);
            return Ok(serviceProvider.ToResponse(handle));
        }

        [HttpGet]
        [Route("{urlHandle}")]
        public async Task<IActionResult> GetByUrlHandle(string urlHandle)
        {
            var data = await bizPage.GetByUrlHandle(urlHandle);
            return Ok(serviceProvider.ToResponse(data));
        }

        [HttpPost]
        [Route("urlHandle")]
        public async Task<IActionResult> GetByListUrlHandle([FromBody] BlogDetailModelToEcom model)
        {
            var data = await bizPage.GetByListUrlHandle(model.listUrlHandle);
            return Ok(serviceProvider.ToResponse(data));
        }

        [HttpPost]
        [Route("list/urlhandle")]
        public async Task<IActionResult> GetListUrlHandle([FromBody] List<string> listUrlHandle)
        {
            var data = await bizPage.GetByListUrlHandle(listUrlHandle);
            return Ok(serviceProvider.ToResponse(data));
        }

        [HttpGet]
        [Route("urlHandle/{handleUrl}")]
        public async Task<IActionResult> GetIdByHandleUrl(string handleUrl)
        {
            var data = await bizPage.GetIdByHandleUrl(handleUrl);
            return Ok(serviceProvider.ToResponse(data));
        }

        [HttpGet("detail/{pageId}")]
        public async Task<IActionResult> GetDetailId(long pageId)
        {
            var objPage = await bizPage.GetDetailExcepNavigation(pageId);
            return Ok(serviceProvider.ToResponse(objPage));
        }
    }
}