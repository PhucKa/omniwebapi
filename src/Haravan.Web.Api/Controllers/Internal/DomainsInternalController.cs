﻿using Haravan.Libs.Abstractions;
using Haravan.Web.Api.Business;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.Infractstructure;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Controllers.Internal
{
    [InternalAccessFilter]
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("internal/domains")]
    public class DomainsInternalController : Controller
    {
        private readonly IDomainsBusiness bizDomain;
        private readonly IServiceProvider serviceProvider;
        private readonly IRequestContext requestContext;

        public DomainsInternalController(
                   IDomainsBusiness bizDomain,
                   IServiceProvider serviceProvider,
                   IRequestContext requestContext

           )
        {
            this.bizDomain = bizDomain;
            this.serviceProvider = serviceProvider;
            this.requestContext = requestContext;
        }

        [HttpGet]
        public async Task<IActionResult> GetDataDomainAsync()
        {
            var result = await bizDomain.GetDataDomain();
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpGet]
        [Route("pricedomains")]
        public async Task<IActionResult> GetListPriceDomainsAsync()
        {
            var result = await bizDomain.GetListPriceDomains();
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpPost]
        [Route("{domainId}/pricedomains")]
        public async Task<IActionResult> ChangToPrimaryDomain([FromBody] PrimaryDomainRequest model)
        {
            var result = await bizDomain.ChangToPrimaryDomain(model.domainId, model.isRedirect);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpDelete("{domainId}")]
        public async Task<IActionResult> DeleteDomainAsync(long domainId)
        {
            var result = await bizDomain.DeleteDomain(domainId);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpPost]
        public async Task<IActionResult> AddDomain([FromBody] SYSDomainModel model)
        {
            var result = await bizDomain.AddNewDomains(model.Name, model.IsHttps);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpPost("checkvalidomain")]
        public async Task<IActionResult> CheckValidDomainAsync([FromBody] CheckValidDomainRequest model)
        {
            var data = await bizDomain.CheckValidDomain(model.hostname);
            return Ok(serviceProvider.ToResponse(data));
        }

        [HttpPost]
        [Route("verifydomain")]
        public async Task<IActionResult> VerifyToReclaimDomain([FromBody] VerifyToReclaimDomainRequest model)
        {
            var result = await bizDomain.VerifyToReclaimDomain(model.domainName);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpGet]
        [Route("{userid}/userfordomain")]
        public async Task<IActionResult> GetUserForDomain(long userId)
        {
            var result = await bizDomain.GetForUpdate(userId);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpPost]
        [Route("changedomain")]
        public async Task<ApiResponseBase> ChangeDomainIsHttps([FromBody] SYSDomainModel model)
        {
            await bizDomain.ChangeDomainIsHttps(model);
            return requestContext.ToResponse();
        }

        [HttpPost]
        [Route("updateishttp")]
        public async Task<IActionResult> UpdateIsHttpsAsync([FromBody] UpdateIsHttpsRequest model)
        {
            var result = await bizDomain.UpdateIsHttpsAsync(model.domainId, model.name, model.isUseHttps);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpPost]
        [Route("validatechange")]
        public async Task<IActionResult> ValidateChangeSubDomain(string newSubDomain)
        {
            var result = await bizDomain.ValidateChangeSubDomain(newSubDomain);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpPost]
        [Route("submithangesubdomain")]
        public async Task<IActionResult> SubmitChangeSubDomain(string newSubDomain)
        {
            var result = await bizDomain.SubmitChangeSubDomain(newSubDomain);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpGet]
        [Route("{domainId}/useletensript")]
        public async Task<IActionResult> UnUseLetEnscript(long domainId)
        {
            var result = await bizDomain.UnUseLetEnscript(domainId);
            return Ok(requestContext.ToResponse(result));
        }
    }
}