﻿using Haravan.Libs.Abstractions;
using Haravan.Web.Api.Business;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Models.Google;
using Haravan.Web.Api.Infractstructure;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Controllers.Internal
{
    [InternalAccessFilter]
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("internal/google_verify")]
    public class GoogleSiteVerificationController : Controller
    {
        private readonly IGoogleSiteVerificationBusiness bizGoogleSiteVerification;
        private readonly IRequestContext requestContext;

        public GoogleSiteVerificationController(
                    IGoogleSiteVerificationBusiness bizGoogleSiteVerification,
                    IRequestContext requestContext
            )
        {
            this.requestContext = requestContext;
            this.bizGoogleSiteVerification = bizGoogleSiteVerification;
        }

        [HttpPost]
        public async Task<IActionResult> Add([FromBody] GoogleSiteVerificationDetailApiModel model)
        {
            await bizGoogleSiteVerification.AddApiAsync(model);
            return Ok(requestContext.ToResponse());
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteByOrgId()
        {
            await bizGoogleSiteVerification.DeleteManyAsync();
            return Ok(requestContext.ToResponse());
        }
    }
}