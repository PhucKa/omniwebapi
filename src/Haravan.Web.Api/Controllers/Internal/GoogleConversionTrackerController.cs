﻿using Haravan.Libs.Abstractions;
using Haravan.Web.Api.Business;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Models.Google;
using Haravan.Web.Api.Infractstructure;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Controllers.Internal
{
    [InternalAccessFilter]
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("internal/google_conversion_tracker")]
    public class GoogleConversionTrackerController : Controller
    {
        private readonly IGoogleShoppingConversionTrackerBusiness bizGoogleShoppingConversionTracker;
        private readonly IRequestContext requestContext;

        public GoogleConversionTrackerController(
                    IGoogleShoppingConversionTrackerBusiness bizGoogleShoppingConversionTracker,
                    IRequestContext requestContext
            )
        {
            this.requestContext = requestContext;
            this.bizGoogleShoppingConversionTracker = bizGoogleShoppingConversionTracker;
        }

        [HttpPost]
        public async Task<IActionResult> Add([FromBody] List<GoogleShoppingConversionTrackerDetailApiModel> model)
        {
            if(model == null)
            {
                requestContext.AddError("Dữ liệu không hợp lệ");
                return Ok(requestContext.ToResponse());
            }
            await bizGoogleShoppingConversionTracker.AddApiAsync(model);
            return Ok(requestContext.ToResponse());
        }
        
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteManyAsync()
        {
            await bizGoogleShoppingConversionTracker.DeleteManyAsync();
            return Ok(requestContext.ToResponse());
        }
    }
}