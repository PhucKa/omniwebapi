﻿using Haravan.Web.Api.Business;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.ESModels;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.BusinessObjects.Models.Common;
using Haravan.Web.Api.Infractstructure;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Controllers.Internal
{
    [InternalAccessFilter]
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("internal/navigations")]
    public class NavigationController : Controller
    {
        private readonly INavigationBusiness bizNav;
        private readonly IFilterBusiness bizFilter;

        private readonly IServiceProvider serviceProvider;

        public NavigationController(
                    INavigationBusiness bizNav,
                     IServiceProvider serviceProvider,
                    IFilterBusiness bizFilter
            )
        {
            this.serviceProvider = serviceProvider;
            this.bizNav = bizNav;
            this.bizFilter = bizFilter;
        }

        [HttpGet]
        [Route("init")]
        public async Task<IntApiResponse<bool>> InitNavigation()
        {
            var result = await bizNav.Init_NewStore();
            return serviceProvider.ToResponse(true);
        }

        [HttpPost]
        public async Task<IActionResult> Add([FromBody] NavigationDetailModel model)
        {
            var data = await bizNav.AddFromSeller(model);
            return Ok(serviceProvider.ToResponse(data));
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromBody] NavigationDetailModel model)
        {
            await bizNav.UpdateFromSeller(model);
            return Ok(serviceProvider.ToResponse());
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> Detail(long id)
        {
            var data = await bizNav.GetDetailLinkListId(id);
            return Ok(serviceProvider.ToResponse(data));
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteLinkList([FromBody]LinkListModel model)
        {
            await bizNav.DeleteLinkListById(model.Id);
            return Ok(serviceProvider.ToResponse());
        }

        [HttpGet]
        public async Task<IActionResult> GetList()
        {
            var data = await bizNav.GetNavigationList(false);
            return Ok(serviceProvider.ToResponse(data));
        }

        [HttpPut("updateLinkorder")]
        public async Task<IActionResult> UpdateLinkOrder([FromBody] LoadUpdateLinkOrder model)
        {
            var detail = await bizNav.UpdateLinkOrder(model.listRow, model.linkListId);
            return Ok(serviceProvider.ToResponse(detail));
        }

        [HttpGet]
        [Route("listforlinkto")]
        public async Task<IActionResult> GetListForLinkTo()
        {
            var data = await bizNav.GetDropdownlist_ForLinkTo();
            return Ok(serviceProvider.ToResponse(data));
        }

        [HttpGet]
        [Route("dropdownlistlinklist")]
        public async Task<IActionResult> GetDropdownlist_LinkList()
        {
            var data = await bizNav.GetDropdownlist_LinkList();
            return Ok(serviceProvider.ToResponse(data));
        }

        [HttpGet]
        [Route("dropdown/{parentLinkId}")]
        public async Task<IActionResult> GetDropDownLinkListDetail(long parentLinkId)
        {
            var result = await bizNav.GetLinkListFromLinkListFieldId(parentLinkId);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpGet]
        [Route("checklinkhasdropdown/{id}")]
        public async Task<IActionResult> CheckLinkHasDropdown(long id)
        {
            var result = await bizNav.CheckLinkFieldIdHasChildLinkList(id);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpGet]
        [Route("checklinkhasparent/{id}")]
        public async Task<IActionResult> CheckLinkHasParent(long id)
        {
            var result = await bizNav.IsLinkListHasParentLink(id);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpPost("dropdownlistbylinkfield")]
        public async Task<IActionResult> LoadDropdownlistAsync([FromBody] LoadDropdownlistViewModel model)
        {
            var (data, totalRecord) = await bizNav.GetDropdownlistByLinkField(model.linkFieldType, model.FilterSearchModel);
            return Ok(serviceProvider.ToResponse(DataPaging.Create(data, totalRecord)));
        }

        #region Url Redirect

        [HttpPost("urlredirect/list")]
        public async Task<IActionResult> GetRedirectListAsync([FromBody] FilterSearchModel model)
        {
            var (data, totalRecord) = await bizNav.GetRedirectList(model);
            return Ok(serviceProvider.ToResponse(DataPaging.Create(data, totalRecord)));
        }

        [HttpPost("urlredirect")]
        public async Task<IActionResult> AddUrlRedirectAsyncAsync([FromBody] UrlRedirectModel model)
        {
            var add = await bizNav.AddUrlRedirect(model);
            return Ok(serviceProvider.ToResponse(add.Item1));
        }

        [HttpDelete("urlredirect")]
        public async Task<IActionResult> DeleteUrlRedirect([FromBody] UrlRedirectModel model)
        {
            var delete = await bizNav.DeleteUrlRedirect(model);
            return Ok(serviceProvider.ToResponse(delete));
        }

        [HttpGet("adddropdownlinklist/{parentLinkId}")]
        public async Task<IActionResult> AddDropdownLinkList(long parentLinkId)
        {
            var add = await bizNav.AddToMenu(parentLinkId);
            return Ok(serviceProvider.ToResponse(add));
        }

        [HttpGet("getdropdownlist/themesetting")]
        public async Task<IActionResult> GetDropdownlistThemeSetting()
        {
            var add = await bizNav.GetDropdownlist_LinkList_ThemeSetting();
            return Ok(serviceProvider.ToResponse(add));
        }

        #endregion Url Redirect

        #region API

        [HttpPost("external/urlredirect")]
        public async Task<IActionResult> AddUrlRedirectApiAsync([FromBody] UrlRedirectModel model)
        {
            var add = await bizNav.AddUrlRedirectApi(model);
            return Ok(serviceProvider.ToResponse(add));
        }

        [HttpGet("external/urlredirect/{id}")]
        public async Task<IActionResult> GetUrlRedirectDetailApiAsync(long id)
        {
            var add = await bizNav.GetUrlRedirectDetailApi(id);
            return Ok(serviceProvider.ToResponse(add));
        }

        [HttpPost("external/urlredirect/list")]
        public async Task<IActionResult> GetUrlRedirectListApi([FromBody] FilterSearchModel model)
        {
            var data = await bizNav.GetUrlRedirectListApi(model);
            return Ok(serviceProvider.ToResponse(data));
        }

        [HttpPost("external/counturlredirect")]
        public async Task<IActionResult> CountUrlRedirectsAsync([FromBody] FilterSearchModel model)
        {
            var data = await bizNav.CountUrlRedirects(model);
            return Ok(serviceProvider.ToResponse(data));
        }

        [HttpDelete("external/urlredirect/{id}")]
        public async Task<IActionResult> DeleteUrlRedirectApiAsync(long id)
        {
            var data = await bizNav.DeleteUrlRedirectApi(id);
            return Ok(serviceProvider.ToResponse(data));
        }

        [HttpPut("external/urlredirect/{id}")]
        public async Task<IActionResult> UpdateUrlRedirectApiAsync([FromBody] RedirectAPIModelRequest model)
        {
            var data = await bizNav.UpdateUrlRedirectApi(model.id, model.dicUpdated);
            return Ok(serviceProvider.ToResponse(data));
        }

        #endregion API

        #region Ecom call Collection

        [HttpPost("GetLinkListByRefId/{refId}")]
        public async Task<IActionResult> GetLinkListByRefId_ExceptHandleApiAsync([FromBody] GetLinkListByRefId_ExceptHandle model)
        {
            var models = await bizNav.GetLinkListByRefId_ExceptHandle(model.refId, model.type);
            return Ok(serviceProvider.ToResponse(models));
        }

        [HttpPost("Collection_UpdateForLink/{collectionId}")]
        public async Task<IActionResult> Collection_UpdateForLinkUrlApiAsync([FromBody] Collection_UpdateForLinkUrlApiAsync model)
        {
            await bizNav.Collection_UpdateForLinkUrl(model.collectionId, model.isDeleteAction);
            return Ok(serviceProvider.ToResponse());
        }
        [HttpPost("Product_UpdateForLink/{productId}")]
        public async Task<IActionResult> Product_UpdateForLinkUrlApiAsync([FromBody] Prouct_UpdateForLinkUrlApiAsync model)
        {
            await bizNav.Product_UpdateForLinkUrl(model.productId, model.isDeleteAction);
            return Ok(serviceProvider.ToResponse());
        }
        [HttpPost("UpdateProductUrlRedirect")]
        public async Task<IActionResult> Product_UpdateForLinkUrlApiAsync([FromBody] UpdateProductUrlRedirect model)
        {
            await bizNav.AddOrUpdateProductUrlRedirect(model.oldPath, model.newPath);
            return Ok(serviceProvider.ToResponse());
        }

        [HttpPost("SetLinkByRefId/{refId}")]
        public async Task<IActionResult> SetLinkByRefIdApiAsync([FromBody] SetLinkByRefId model)
        {
            await bizNav.SetLinkByRefId(model.refId, model.refTitle, model.linkLists, model.type);
            return Ok(serviceProvider.ToResponse());
        }


        #endregion  Ecom call Collection

    }
}