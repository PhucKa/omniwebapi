﻿using BHN.SharedObject.APIDataModel;
using Haravan.Web.Api.Business;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.Infractstructure;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Controllers.Internal
{
    [InternalAccessFilter]
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("internal/apps")]
    public class AppProxyInternalController : Controller
    {
        private readonly IAppProxyBusiness bizAppProxy;
        private readonly ICartProxyBusiness bizCartProxy;
        private readonly IServiceProvider serviceProvider;

        public AppProxyInternalController(
                   IAppProxyBusiness bizAppProxy,
                   ICartProxyBusiness bizCartProxy,
                   IServiceProvider serviceProvider
            )
        {
            this.bizAppProxy = bizAppProxy;
            this.bizCartProxy = bizCartProxy;
            this.serviceProvider = serviceProvider;
        }

        [HttpGet("{appId}/app_proxy")]
        public async Task<IActionResult> GetAppProxy(long appId)
        {
            var result = await bizAppProxy.GetByAppId(appId);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpPost("{appId}/app_proxy")]
        public async Task<IActionResult> Add(long appId, [FromBody] AppProxyAPIModel model)
        {
            var result = await bizAppProxy.Add(appId, model);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpPut("{appId}/app_proxy")]
        public async Task<IActionResult> Update(long appId, [FromBody] AppProxyAPIModel model)
        {
            var result = await bizAppProxy.Update(appId, model);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpDelete("{appId}/app_proxy")]
        public async Task<IActionResult> Delete(long appId)
        {
            var result = await bizAppProxy.Delete(appId);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpPost("cart_proxy")]
        public async Task<IActionResult> AddCartProxy([FromBody]CartProxyModel model)
        {
            var result = await bizCartProxy.Add(model);
            return Ok(serviceProvider.ToResponse(result));
        }

        [HttpDelete("{app_id}/cart_proxy")]
        public async Task<IActionResult> DeleteCartProxy(long app_id)
        {
            await bizCartProxy.Delete(app_id);
            return Ok(serviceProvider.ToResponse());
        }

        [HttpGet("cart_proxy/first")]
        public async Task<IActionResult> GetFirstCartProxyId()
        {
            var result = await bizCartProxy.GetFirst();
            return Ok(serviceProvider.ToResponse(result));
        }
    }
}