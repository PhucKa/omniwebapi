﻿using Haravan.Web.Api.Business;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.Mappers;
using Haravan.Web.Api.BusinessObjects.ESModels;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.BusinessObjects.Models.Common;
using Haravan.Web.Api.Infractstructure;
using Haravan.Web.Api.Repository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Controllers.Internal
{
    [InternalAccessFilter]
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("internal/settings")]
    public class SettingsInternal : Controller
    {
        private readonly IGeneralSettingBusiness _bizGeneral;
        private readonly IServiceProvider serviceProvider;

        public SettingsInternal(
                     IGeneralSettingBusiness bizGeneral,
                    IServiceProvider serviceProvider
            )
        {
            this.serviceProvider = serviceProvider;
            this._bizGeneral = bizGeneral;
           
        }
        [HttpGet("onlinestoresetting")]
        public async Task<IActionResult> GetOnlineStoreSettingAsync()
        {
            var result = await _bizGeneral.GetOnlineStoreSetting();
            return Ok(serviceProvider.ToResponse(result));
        }
        [HttpPut("onlinestoresetting")]
        public async Task<IActionResult> UpdateOnlineStoreSettingAsync([FromBody] OnlineStoreSetting settings)
        {
            var detail = await _bizGeneral.UpdateOnlineStoreSetting(settings);
            return Ok(serviceProvider.ToResponse(detail));
        }
        
    }
}