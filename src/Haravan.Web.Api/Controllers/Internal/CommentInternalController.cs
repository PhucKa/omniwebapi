﻿using BHN.SharedObject.APIDataModel;
using Haravan.Web.Api.Business;
using Haravan.Web.Api.BusinessObjects;
using Haravan.Web.Api.BusinessObjects.ESModels;
using Haravan.Web.Api.BusinessObjects.Mappers;
using Haravan.Web.Api.BusinessObjects.Models.Comment;
using Haravan.Web.Api.Infractstructure;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Controllers.Internal
{
    [InternalAccessFilter]
    [ApiExplorerSettings(IgnoreApi = false)]
    [Route("internal/comments")]
    public class CommentInternalController : Controller
    {
        private readonly ICommentBusiness bizComment;
        private readonly IFilterBusiness bizFilter;

        private readonly IServiceProvider serviceProvider;

        public CommentInternalController(
                    ICommentBusiness bizComment,
                    IServiceProvider serviceProvider,
                    IFilterBusiness bizFilter
            )
        {
            this.serviceProvider = serviceProvider;
            this.bizComment = bizComment;
            this.bizFilter = bizFilter;
        }

        #region internal

        [HttpPost]
        [Route("list")]
        public async Task<IActionResult> GetCommentListAsync([FromBody]FilterSearchModel model)
        {
            var (data, totalRecord) = await bizComment.GetCommentListAsync(model);
            return Ok(serviceProvider.ToResponse(DataPaging.Create(data.SellerToListModel(), totalRecord)));
        }

        [HttpPut]
        public async Task<IntApiResponse<bool>> UpdateStatusManyAsync([FromBody] CommentUpdateModel model)
        {
            var update = await bizComment.UpdateStatusManyAsync(model.ids, model.status);
            return serviceProvider.ToResponse(update);
        }

        [HttpDelete]
        public async Task<IntApiResponse<bool>> DeleteCommentsAsync([FromBody] CommentDeleteModel model)
        {
            var update = await bizComment.DeleteCommentsAsync(model.ids);
            return serviceProvider.ToResponse(update);
        }

        #endregion internal

        #region API

        [HttpPost("external/list")]
        public async Task<IActionResult> GetApiList([FromBody]FilterSearchModel model)
        {
            var (totalRecord, data) = await bizComment.GetListCommentApiAsync(model);
            return Ok(serviceProvider.ToResponse(DataPaging.Create(data, totalRecord)));
        }

        [HttpPost("external/count")]
        public async Task<IntApiResponse<long>> CountCommentsApi([FromBody] FilterSearchModel filter)
        {
            var count = await bizComment.CountCommentsApiAsync(filter);
            return serviceProvider.ToResponse(count);
        }

        [HttpGet("external/{commentId}")]
        public async Task<IntApiResponse<CommentAPIModel>> GetCommentDetailApiAsync([FromRoute] long commentId)
        {
            var detail = await bizComment.GetCommentDetailApiAsync(commentId);
            return serviceProvider.ToResponse(detail);
        }

        [HttpPost("external/comment")]
        public async Task<IntApiResponse<CommentAPIModel>> CreateCommentApiAsync([FromBody] CreateCommentModel model)
        {
            var comment = await bizComment.CreateCommentApiAsync(model.dicInserted, model.model);
            return serviceProvider.ToResponse(comment);
        }

        [HttpPut("external/{commentId}")]
        public async Task<IntApiResponse<CommentAPIModel>> UpdateCommentApiAsync([FromBody] UpdateCommentModel model)
        {
            var comment = await bizComment.UpdateCommentApiAsync(model.commentId, model.dicUpdated);
            return serviceProvider.ToResponse(comment);
        }

        [HttpPost("external/commentStatus")]
        public async Task<IntApiResponse<CommentAPIModel>> UpdateCommentStatusAsync([FromBody] UpdateCommentStatus model)
        {
            var comment = await bizComment.UpdateCommentStatusApiAsync(model.commentId, model.status);
            return serviceProvider.ToResponse(comment);
        }

        [HttpPost("external/updateCommentDelete")]
        public async Task<IntApiResponse<CommentAPIModel>> UpdateCommentDeletedApiAsyncAsync([FromBody] UpdateCommentDeleted model)
        {
            var comment = await bizComment.UpdateCommentDeletedApiAsync(model.commentId, model.isDeleted);
            return serviceProvider.ToResponse(comment);
        }

        #endregion API
    }
}