﻿using Haravan.Web.Api.Business.Extensions;
using Haravan.Web.Api.BusinessObjects.Configs;
using Haravan.Web.Api.Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Converters;

namespace Haravan.Web.Api
{
    public partial class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            Env = env;
        }

        public IConfiguration Configuration { get; }
        public IHostingEnvironment Env { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<AppConfig>(Configuration);
            services.Configure<RedisConfig>(Configuration.GetSection("Redis"));
            services.Configure<MongoConfig>(Configuration.GetSection("Mongo"));
            services.Configure<ElasticConfig>(Configuration.GetSection("Elastic"));
            services.Configure<BusConfig>(Configuration.GetSection("Bus"));
            services.AddApiCore(Configuration, Env);
            services.AddSingleton<IBlobServiceClientFactory, BlobServiceClientFactory>();
            ConfigureAppServices(services);
            services.AddCoreService(Configuration);
            services.AddMvc().AddJsonOptions(opt =>
            {
                opt.SerializerSettings.Converters.Add(new StringEnumConverter(true));
            });
            
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApplicationLifetime lifetime)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            var pathBase = "/api";

            app.UsePathBase(pathBase);

            app.UseCors("default");

            app.UseAuthentication();
            app.UseApiCore(Env, pathBase);
            app.UseMiddleware<StoreContextMiddleware>();
            app.UseMvc();
        }
    }
}
