﻿namespace Haravan.Web.Api.Models
{
    public class SettingThemeAssetModel
    {
        public string name { get; set; }
        public int? max_width { get; set; }
        public int? max_height { get; set; }
    }
}