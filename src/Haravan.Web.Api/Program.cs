﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace Haravan.Web.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostingContext, configBuilder) =>
                {
                     configBuilder.AddJsonFile("secrets/appsettings.secrets.json", optional: true);
                })
                .UseHaravanInsights()
                .UseUrls("http://+:5022", "http://+:2011")
                .UseStartup<Startup>()
                .Build();
    }
}
