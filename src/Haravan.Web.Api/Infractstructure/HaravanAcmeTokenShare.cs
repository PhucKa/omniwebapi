﻿using Haravan.Acme;
using StackExchange.Redis;
using System.Threading;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Infractstructure
{
    public class HaravanAcmeTokenShare : ITokenShare
    {
        private volatile ConnectionMultiplexer _connection;
        private readonly SemaphoreSlim _connectionLock = new SemaphoreSlim(initialCount: 1, maxCount: 1);
        protected IDatabase _cache;
        private readonly string redisConfig;

        public HaravanAcmeTokenShare(string redisConfig)
        {
            this.redisConfig = redisConfig;
        }

        protected async Task ConnectAsync()
        {
            if (_connection != null)
            {
                return;
            }

            await _connectionLock.WaitAsync().ConfigureAwait(false);
            try
            {
                if (_connection == null)
                {
                    _connection = await ConnectionMultiplexer.ConnectAsync(redisConfig).ConfigureAwait(false);
                    _connection.PreserveAsyncOrder = false;
                    _cache = _connection.GetDatabase();
                }
            }
            finally
            {
                _connectionLock.Release();
            }
        }

        public async Task<string> GetAsync(string path)
        {
            await ConnectAsync();

            return await _cache.StringGetAsync(path);
        }

        public async Task SetAsync(string path, string content)
        {
            await ConnectAsync();
            await _cache.StringSetAsync(path, content);
        }
    }
}