﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Threading.Tasks;

namespace Haravan.Web.Api.Infractstructure
{
    public class InternalAccessFilter : Attribute, IAsyncAuthorizationFilter
    {
        public Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            var port = context.HttpContext.Connection.LocalPort;

            if (port != 2011)
            {
                context.Result = new ContentResult()
                {
                    Content = "not allowed",
                    ContentType = "text/plain",
                    StatusCode = 401
                };
            }

            return Task.CompletedTask;
        }
    }
}