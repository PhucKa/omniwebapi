﻿using Haravan.Libs.Abstractions.Roles;

namespace Haravan.Web.Api.Infractstructure
{
    public class RoleConsts
    {
        public const string CanOwner = "admin";
        public const string CanOwnerOmni = "com_api.admin";
        public const string CanAdmin = COM_RoleConsts.CanAdmin;

        public const string CanWriteContent = COM_RoleConsts.CanAdmin + "," + COM_RoleConsts.Com_HaraWeb_Content_Write;
        public const string CanReadContent = CanWriteContent + "," + COM_RoleConsts.Com_HaraWeb_Content_Read;
        public const string CanWriteDomain = COM_RoleConsts.CanAdmin + "," + COM_RoleConsts.Com_HaraWeb_Domain_Write;
        public const string CanReadDomain = CanWriteDomain + "," + COM_RoleConsts.Com_HaraWeb_Domain_Read;
        public const string CanWriteNavigation = COM_RoleConsts.CanAdmin + "," + COM_RoleConsts.Com_HaraWeb_Navigation_Write + "," + COM_RoleConsts.Com_Collection_Write;
        public const string CanReadNavigation = CanWriteNavigation + "," + COM_RoleConsts.Com_HaraWeb_Navigation_Read + "," + COM_RoleConsts.Com_Collection_Read;
        public const string CanWriteTheme = COM_RoleConsts.CanAdmin + "," + COM_RoleConsts.Com_HaraWeb_Theme_Write;
        public const string CanReadTheme = CanWriteTheme + "," + COM_RoleConsts.Com_HaraWeb_Theme_Read;
        public const string CanWriteSetting = COM_RoleConsts.CanAdmin + "," + COM_RoleConsts.Com_HaraWeb_Setting_Write;
        public const string CanReadSetting = CanWriteSetting + "," + COM_RoleConsts.Com_HaraWeb_Setting_Read;

        public const string CanWriteFile = CanWriteContent + "," + CanWriteTheme;
        public const string CanReadFile = CanReadContent + "," + CanReadTheme;

       
    }
}