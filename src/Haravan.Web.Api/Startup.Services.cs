﻿using Haravan.Acme;
using Haravan.Caching;
using Haravan.Web.Api.Business.Extensions;
using Haravan.Web.Api.BusinessObjects.Configs;
using Haravan.Web.Api.Infractstructure;
using Haravan.Web.Api.Repository;
using Haravan.Web.Api.Repository.Infractstructure;
using MassTransit;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Nest;
using System;
using System.Net.Http;

namespace Haravan.Web.Api
{
    public partial class Startup
    {
        public void ConfigureAppServices(IServiceCollection services)
        {
            services.AddSingleton<IBus>(u =>
            {
                var busConfig = u.GetRequiredService<IOptions<BusConfig>>().Value;
                return Bus.Factory.CreateUsingRabbitMq(cfg =>
                {
                    cfg.Host(new Uri(busConfig.Host), host =>
                    {
                        host.Username(busConfig.User);
                        host.Password(busConfig.Pass);
                    });
                });
            });

            services.AddSingleton<IEBSComMessageRepository, EBSComMessageRepository>();
            services.AddSingleton(u => new HttpClient(new HttpClientHandler { UseCookies = false, UseProxy = false }));
            services.AddMongo(Configuration);
            services.AddSingleton<IElasticClient>(u =>
            {
                var config = u.GetService<IOptions<ElasticConfig>>().Value;
                var settings = new ConnectionSettings(new Uri(config.ESSvr))
                    .DefaultIndex(config.ESIndex)
                    .ConnectionLimit(config.ESConnectionLimit)
                    .RequestTimeout(TimeSpan.FromSeconds(config.ESRequestTimeoutSeconds))
#if DEBUG
                                .PrettyJson()
                    .DisableDirectStreaming()
#endif
                                ;
                return new ElasticClient(settings);
            });

            services.AddSingleton<ICache>(u =>
            {
                var config = u.GetService<IOptions<RedisConfig>>().Value;

                var redis = config.SellerRedis.Substring(0, config.SellerRedis.Length - 2);
                redis = redis + ",defaultDatabase=1";

                var opt = new RedisCacheOptions()
                {
                    Configuration = redis,
                    InstanceName = config.SellerInstant
                };

                return new HaravanCache(opt);
            });

            services.AddTransient<ITokenShare, HaravanAcmeTokenShare>(u =>
            {
                var config = u.GetService<IOptions<RedisConfig>>().Value;
                return new HaravanAcmeTokenShare(config.BuyerRedis);
            });

            services.AddTransient<IAcmeClient>(u =>
            {
                var tokenShare = u.GetService<ITokenShare>();
                var acmeClient = new AcmeClient(tokenShare, "/srv/acme");
                return acmeClient;
            });
        }
    }
}
