﻿using System;
using System.Runtime.Serialization;

namespace Haravan.Web.Api.BusinessObjects
{
    [DataContract]
    public class StoreData
    {
        [DataMember(Order = 1)]
        public long StoreId { get; set; }

        [DataMember(Order = 2)]
        public string StoreName { get; set; }

        [DataMember(Order = 3)]
        public string StoreTitle { get; set; }

        [DataMember(Order = 4)]
        public string BuyerDomain { get; set; }

        [DataMember(Order = 5)]
        public string SellerDomain { get; set; }

        [DataMember(Order = 6)]
        public string SubDomain { get; set; }

        [DataMember(Order = 7)]
        public int TimeZone { get; set; }

        [DataMember(Order = 8)]
        public string Culture { get; set; }

        [DataMember(Order = 9)]
        public string ContactEmail { get; set; }

        [DataMember(Order = 10)]
        public string CustomerServiceEmail { get; set; }

        [DataMember(Order = 11)]
        public int UnitSystem { get; set; }

        [DataMember(Order = 12)]
        public int CurrencyId { get; set; }

        [DataMember(Order = 13)]
        public long OwnerUserId { get; set; }

        [DataMember(Order = 14)]
        public string OwnerUserEmail { get; set; }

        [DataMember(Order = 15)]
        public string Currency { get; set; }

        [DataMember(Order = 16)]
        public string CurrencySymbol { get; set; }

        [DataMember(Order = 17)]
        public string CurrencyPattern { get; set; }

        [DataMember(Order = 18)]
        public string HomePageMetaDesc { get; set; }

        [DataMember(Order = 19)]
        public string OrderNumberFormat { get; set; }

        [DataMember(Order = 20)]
        public string HtmlWithoutCurrency { get; set; }

        [DataMember(Order = 21)]
        public string HtmlCurrency { get; set; }

        [DataMember(Order = 22)]
        public int Status { get; set; }

        [DataMember(Order = 23)]
        public string GA { get; set; }

        [DataMember(Order = 24)]
        public string GAAddCode { get; set; }

        [DataMember(Order = 25)]
        public bool IsForNewbie { get; set; }

        #region CNN

        [DataMember(Order = 26)]
        public string SqlCnn { get; set; }

        #endregion CNN

        [DataMember(Order = 27)]
        public int ShopPlanId { get; set; }

        [DataMember(Order = 28)]
        public int FacebookPlanId { get; set; }

        [DataMember(Order = 29)]
        public bool IsAllowLoginFromFacebook { get; set; }

        [DataMember(Order = 30)]
        public int InventoryMethod { get; set; }

        [DataMember(Order = 31)]
        public bool IsConnectedUser5giay { get; set; }

        [DataMember(Order = 32)]
        public bool IsConnectedUserWtt { get; set; }

        [DataMember(Order = 33)]
        public bool IsConnectFacebookChat { get; set; }

        [DataMember(Order = 34)]
        public bool IsUseNewCheckout { get; set; }

        [DataMember(Order = 35)]
        public long? AppIdCartProxy { get; set; }

        [DataMember(Order = 36)]
        public bool IsConnectLazadaStore { get; set; }

        [DataMember(Order = 37)]
        public string FacebookPlanPageNum { get; set; }

        [DataMember(Order = 38)]
        public bool IsConnectFacebookStore { get; set; }

        [DataMember(Order = 39)]
        public DateTime? HaraTrackingExpiredDate { get; set; }

        [DataMember(Order = 40)]
        public int? AdminUserNum { get; set; }

        [DataMember(Order = 41)]
        public DateTime? AdminUserNumExpiredDate { get; set; }

        [DataMember(Order = 42)]
        public bool IsConnectTikiStore { get; set; }

        [DataMember(Order = 43)]
        public bool IsUseEnhancedEcommerce { get; set; }

        [DataMember(Order = 44)]
        public bool IsUseHaravanTracking { get; set; }

        [DataMember(Order = 45)]
        public int? TotalCurrentStore { get; set; }

        [DataMember(Order = 46)]
        public bool IsConnectAdayroiStore { get; set; }

        [DataMember(Order = 47)]
        public bool IsConnectShopeeStore { get; set; }

        [DataMember(Order = 48)]
        public DateTime ActualExpiryDate { get; set; }

        [DataMember(Order = 49)]
        public DateTime FacebookPlanExpiryDate { get; set; }

        [DataMember(Order = 50)]
        public DateTime CreatedDate { get; set; }

        [DataMember(Order = 51)]
        public bool IsAdvancePassword { get; set; }

        [DataMember(Order = 52)]
        public int OPCMode { get; set; }

        [DataMember(Order = 53)]
        public int RegisterType { get; set; }

        [DataMember(Order = 54)]
        public bool IsForOmniPowerNewbie { get; set; }

        [DataMember(Order = 55)]
        public bool IsForOmniPowerOnboarding { get; set; }
    }

    [DataContract]
    public class PaymentMethodModel
    {
        [DataMember(Order = 1)]
        public long CheckoutSettingId { get; set; }

        [DataMember(Order = 2)]
        public int PaymentInstructionTypeId { get; set; }

        [DataMember(Order = 3)]
        public string Name { get; set; }

        [DataMember(Order = 4)]
        public string Instruction { get; set; }
    }
}