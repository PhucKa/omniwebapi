﻿using Haravan.Web.Api.BusinessObjects.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Haravan.Web.Api.BusinessObjects
{
    public class CollectionDetailModel
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public long? ImageId { get; set; }
        public string ImageUrl { get; set; }
        public bool IsManualSelect { get; set; }
        public bool IsAllConditions { get; set; }
        public bool Visibility { get; set; }
        public bool VisibilityPOS { get; set; }
        public string PageTitle { get; set; }
        public string MetaDescription { get; set; }
        public string UrlHandle { get; set; }
        public DateTime? PublishDate { get; set; }
        public int SortTypeId { get; set; }
        public string TemplateName { get; set; }
        public bool IsDeleted { get; set; }
        public string VersionNo { get; set; }
        public List<NavigationDetailModel> LinkList { get; set; }
        public List<ConditionModel> Condition { get; set; }
        public List<long> ProductIds { get; set; }
        public bool IsFinishInsertProduct { get; set; }
        public bool IsSetPublishDate { get; set; }
        public bool VisibilityFacebook { get; set; }
        public bool IsShowVisibilityFacebook { get; set; }
        public string LinkToViewCollection { get; set; }
        public bool VisibilityZalo { get; set; }
        public bool IsShowVisibilityZalo { get; set; }
    }
    public class ConditionModel
    {
        public long Id { get; set; }
        public int TypeId { get; set; }
        public string TypeName { get; set; }
        public int RuleId { get; set; }
        public string RuleName { get; set; }
        public string Value { get; set; }
        public string ControlType { get; set; }
        public long CollectionId { get; set; }
        public string VersionNo { get; set; }
        public bool IsDeleted { get; set; }
    }
}
