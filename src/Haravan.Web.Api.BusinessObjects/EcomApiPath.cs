﻿namespace Haravan.Web.Api.BusinessObjects
{
    public static class EcomApiPath
    {
        public const string GetCollectionByHandle = "omniwebapi/common/collection/getcollectionbyhandle";
        public const string GetCollectionSimpleById = "omni/collections/simplehandle/{id}";
        public const string GetCollectionList = "collection/GetList";
        public const string GetCollectionIdByHandle = "omniwebapi/common/collection/getbyhandle/{handle}";
        public const string GetImageList = "omniwebapi/media/files";
        public const string UploadStorageImage = "media/storage/image";
        public const string UploadStorageFile = "media/storage/file";
        public const string UploadFile = "omniwebapi/theme/file";
        public const string GetProductImagesByProductId = "omni/products/{productId}/images";
        public const string GetFilterByViewId = "omni/filters/{viewId}/list";
        public const string GetFilterTabDetail = "omni/filters/{tabId}";
        public const string AddFilter = "omni/filters";
        public const string DeleteFilter = "omni/filters/{tabId}";
        public const string GetUserSimpleList = "omniwebapi/common/users/simple";
        public const string GetDetailSimpleUser = "omniwebapi/common/users/simple/{userId}";
    }
}
