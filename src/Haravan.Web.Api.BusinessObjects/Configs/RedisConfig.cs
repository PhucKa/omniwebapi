﻿using System.Collections.Generic;

namespace Haravan.Web.Api.BusinessObjects.Configs
{
    public class RedisConfig
    {
        public string BuyerRedis { get; set; }
        public string SellerRedis { get; set; }
        public string SellerInstant { get; set; }
    }
}
