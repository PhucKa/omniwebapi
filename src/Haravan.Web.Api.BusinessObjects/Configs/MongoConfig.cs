﻿namespace Haravan.Web.Api.BusinessObjects.Configs
{
    public class MongoConfig
    {
        public string Mongo { get; set; }
        public string MongoSellerMaster { get; set; }
        public string MongoSeq { get; set; }
        public string MongoSellerReport { get; set; }
    }
}
