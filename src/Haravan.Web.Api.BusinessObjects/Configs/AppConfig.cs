﻿using Haravan.Web.Api.BusinessObjects.Models;
using System.Collections.Generic;

namespace Haravan.Web.Api.BusinessObjects.Configs
{
    public class AppConfig
    {
        private static AppConfig _current = null;

        public int DefaultSSLRefreshDay { get; set; }
        public int DefaultSSLExpireDay { get; set; }

        public string DomainsNotAllowAdd { get; set; }
        public string Domain { get; set; }
        public string EcomApiUrl { get; set; }
        public string ReportApiUrl { get; set; }
        public string EtcdSsl { get; set; }
        public int SessionTimeout { get; set; }
        public string BuyerDomainDefault { get; set; }
        public string SellerDomain { get; set; }
        public long FileServiceFromThemeId { get; set; }
        public List<ThemesFree> ThemesFree { get; set; }
        public int ImportExportTime { get; set; }
        public string DefaultIP { get; set; }
        public string Stats_Api_Key { get; set; }
        public string Stats_App_Id { get; set; }
        public string Link_ReportData { get; set; }
        public bool SwitchReportApi { get; set; }
        public string HashSalt { get; set; }

        public static AppConfig Current
        {
            get
            {
                if (_current == null) _current = new AppConfig();
                return _current;
            }
        }
    }
}