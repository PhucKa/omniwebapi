﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Haravan.Web.Api.BusinessObjects.Configs
{
    public class BusConfig
    {
        public string Host { get; set; }
        public string User { get; set; }
        public string Pass { get; set; }
    }
}
