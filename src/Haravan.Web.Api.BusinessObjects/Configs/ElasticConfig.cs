﻿namespace Haravan.Web.Api.BusinessObjects.Configs
{
    public class ElasticConfig
    {
        public string ESIndex { get; set; }
        public string ESSvr { get; set; }
        public int ESRequestTimeoutSeconds { get; set; }
        public int ESConnectionLimit { get; set; } = 80;
    }
}
