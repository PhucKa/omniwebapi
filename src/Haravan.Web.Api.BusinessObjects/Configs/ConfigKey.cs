﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Haravan.Web.Api.BusinessObjects.Configs
{
    public static class ConfigKey
    {
        #region ReadOnly

        public const string QUEUE = "Queue";
        public const string QUEUEUSER = "QueueUser";
        public const string QUEUEPASS = "QueuePass";
        public const string SERVICEBUSCONCURENCY = "ServiceBusConcurency";
        public const string HASHSALT = "HashSalt";
        public const string SELLERDATACACHE = "SellerDataCache";
        public const string WCFSESSION = "WcfSession";
        public const string REDIS = "Redis";
        public const string BUYERREDIS = "BuyerRedis";
        public const string MEDIADOMAIN = "MediaDomain";
        public const string SESSIONTIMEOUT = "SessionTimeout";
        public const string ESSVR = "ESSvr";
        public const string ESSVRSOCIAL = "ESSvrSocial";
        public const string ESINDEX = "ESIndex";
        public const string ESINDEXSOCIAL = "ESIndexSocial";
        public const string MEDIASVR = "MediaSvr";
        public const string MONGO = "Mongo";
        public const string MONGOSEQ = "MongoSeq";
        public const string MONGOSELLERREPORT = "MongoSellerReport";
        public const string MONGOSOCIAL = "MongoSocial";
        public const string MONGOAPPDB = "MongoAppDb";
        public const string SELLERDOMAIN = "SellerDomain";
        public const string BUYERDOMAINDEFAULT = "BuyerDomainDefault";
        public const string MONGOSALECHANNEL = "MongoSaleChannel";

        public const string FACEBOOKAPPID = "FacebookAppId";
        public const string FACEBOOKAPPSECRET = "FacebookAppSecret";
        public const string FACEBOOKAPPID_1 = "FacebookAppId_1";
        public const string FACEBOOKAPPSECRET_1 = "FacebookAppSecret_1";
        public const string FACEBOOKAPPID_2 = "FacebookAppId_2";
        public const string FACEBOOKAPPSECRET_2 = "FacebookAppSecret_2";
        public const string FACEBOOKAPPID_3 = "FacebookAppId_3";
        public const string FACEBOOKAPPSECRET_3 = "FacebookAppSecret_3";
        public const string FACEBOOKSUBSCRIPTIONVERIFYTOKEN = "FacebookSubscriptionVerifyToken";

        public const string ZALOAPPID = "ZaloAppId";
        public const string ZALOAPPSECRETKEY = "ZaloAppSecretKey";
        public const string ZALOOPENAPIURL = "ZaloOpenApiUrl";
        public const string ZALOTEMPLATEID = "ZaloTemplateId";

        public const string FIVEVNSECRET = "5giaySecret";
        public const string FIVEVNCHECKSUM = "5giayCheckSum";
        public const string WTTSECRET = "wttSecret";

        public const string Clothing = "Clothing";
        public const string MomBaby = "MomBaby";
        public const string Technology = "Technology";
        public const string Furniture = "Furniture";
        public const string Food = "Food";
        public const string Others = "Others";
        public const string NewClothing = "NewClothing";
        public const string NewMomBaby = "NewMomBaby";
        public const string NewTechnology = "NewTechnology";
        public const string NewFurniture = "NewFurniture";
        public const string NewFood = "NewFood";
        public const string NewOthers = "NewOthers";
        public const string CanUsePromotionGroupCustomerShopIds = "CanUsePromotionGroupCustomerShopIds";
        public const string EtcdDomainService = "EtcdDomainService";
        public const string UseEtcdDomainService = "UseEtcdDomainService";
        public const string DefaultIP = "DefaultIP";
        public const string HaravanIps = "HaravanIps";

        public const string MaxNumFacebookConversation = "MaxNumFacebookConversation";
        public const string MaxNumFacebookConversationPerReceiveFromOther = "MaxNumFacebookConversationPerReceiveFromOther";
        public const string MaxNumFacebookConversationRegisterFromRef = "MaxNumFacebookConversationRegisterFromRef";

        public const string CUSTOMERCALLINGURL = "CustomerCallingUrl";
        public const string CUSTOMERCALLINGSIGNKEY = "CustomerCallingSignKey";

        public const string INSIDEAPI = "InsideApi";

        public const string FILESERVICE = "FileService";
        public const string FILESERVICESUBTMP = "FileServiceSubTmp";
        public const string FILESERVICESUBFILE = "FileServiceSubFile";
        public const string FILESERVICESUBPRODUCT = "FileServiceSubProduct";
        public const string FILESERVICESUBTHEME = "FileServiceSubTheme";

        public const string IMPORTEXPORTTIME = "ImportExportTime";

        #region Carrier Service

        public const string CarrierServiceViettelPostUsername = "CarrierServiceViettelPostUsername";
        public const string CarrierServiceViettelPostPassword = "CarrierServiceViettelPostPassword";
        public const string CarrierServiceViettelPostAppKey = "CarrierServiceViettelPostAppKey";
        public const string CarrierServiceViettelPostToken = "CarrierServiceViettelPostToken";
        public const string CarrierServiceViettelPostApiOrderUrl = "CarrierServiceViettelPostApiOrderUrl";

        public const string CarrierServiceViettelPostV2ApiUrl = "CarrierServiceViettelPostV2ApiUrl";

        public const string CarrierServiceVNPTApiSI = "CarrierServiceVNPTApiSI";
        public const string CarrierServiceVNPTApiUrl = "CarrierServiceVNPTApiUrl";
        public const string CarrierServiceVNPTApiSK = "CarrierServiceVNPTApiSK";

        public const string CarrierServiceGHN2018ApiUrl = "CarrierServiceGHN2018ApiUrl";
        public const string CarrierServiceGHN2018AppKey = "CarrierServiceGHN2018AppKey";

        public const string CarrierServiceGHNWebUrl = "CarrierServiceGHNWebUrl";
        public const string CarrierServiceGHNWebUrlLogin = "CarrierServiceGHNWebUrlLogin";
        public const string CarrierServiceGHNWebUrlGetToken = "CarrierServiceGHNWebUrlGetToken";
        public const string CarrierServiceGHNApiUrl = "CarrierServiceGHNApiUrl";
        public const string CarrierServiceGHNAppKey = "CarrierServiceGHNAppKey";
        public const string CarrierServiceGHNSecretKey = "CarrierServiceGHNSecretKey";

        public const string CarrierServiceShipChungApiUrl = "CarrierServiceShipChungApiUrl";
        public const string CarrierServiceProShipApiUrl = "CarrierServiceProShipApiUrl";
        public const string CarrierServiceFlexShipApiUrl = "CarrierServiceFlexShipApiUrl";
        public const string CarrierServiceGHTKApiUrl = "CarrierServiceGHTKApiUrl";
        public const string CarrierServiceNinjavanApiUrl = "CarrierServiceNinjavanApiUrl";
        public const string CarrierServiceVNPostApiUrl = "CarrierServiceVNPostApiUrl";
        public const string CarrierServiceVNPostReceiveOrderApiUrl = "CarrierServiceVNPostReceiveOrderApiUrl";
        public const string CarrierServiceVNPostToken = "CarrierServiceVNPostToken";
        public const string CarrierServiceVNPostReceiveOrderToken = "CarrierServiceVNPostReceiveOrderToken";
        public const string GHNClientId = "GHNClientId";
        public const string CarrierServiceDHLApiUrl = "CarrierServiceDHLApiUrl";
        public const string CarrierServiceDHLClientID = "CarrierServiceDHLClientID";
        public const string CarrierServiceDHLPassword = "CarrierServiceDHLPassword";
        public const string CarrierServiceDHLGetAccessToken = "CarrierServiceDHLGetAccessToken";
        #endregion

        #region Multi Channel

        public const string ADRDefaultPublicKey = "ADRDefaultPublicKey";
        public const string ADRApiUrl = "ADRApiUrl";
        public const string ADRActionGetFeedList = "ADRActionGetFeedList";
        public const string ADRApiVersion = "ADRApiVersion";

        public const string SendoApiUrl = "SendoApiUrl";
        public const string SendoApiPassword = "SendoApiPassword";

        public const string LazadaApiUrl = "LazadaApiUrl";
        public const string LazadaApiVersion = "LazadaApiVersion";
        public const string LazadaApiUser = "LazadaApiUser";
        public const string LazadaApiPassword = "LazadaApiPassword";

        #endregion

        public const string HSTATICAPI = "HStaticApi";

        public const string HARAPAGECOMMENTPLUGINAPI = "HarapageCommentPluginApi";

        public const string FILESERVICEFROMTHEMEID = "FileServiceFromThemeId";

        public const string FILETHEMEASSETPATH = "FileThemeAssetPath";

        #endregion

        public const string EtcdSsl = "EtcdSsl";
    }
}
