﻿using AngleSharp.Parser.Html;
using Haravan.Utils;
namespace Haravan.Web.Api.Utils.Extensions
{
    public static class HtmlStringHelpers
    {
        public static string StripHtml(this string obj)
        {
            if (obj == null) return null;
            if (obj.Length > 800000) return "[...]";

            var parser = new HtmlParser();
            var doc = parser.Parse(obj);

            var text = doc.Body.TextContent;

            if (text != null)
            {
                if (!CrossSiteScriptingValidation.IsDangerousString(text, out var n))
                {
                    return text.Trim();
                }
                else
                    return "";
            }

            else
                return text;
        }

    }
}
