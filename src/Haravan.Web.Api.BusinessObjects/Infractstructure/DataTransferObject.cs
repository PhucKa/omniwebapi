﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Haravan.Web.Api.BusinessObjects
{
    [DataContract(IsReference = true)]
    public class ApiResponseBase
    {
        public ApiResponseBase()
        {
            Errors = new List<object>();
            ErrorCodes = new List<object>();
        }

        [DataMember]
        public bool HasError
        {
            get
            {
                if (Errors != null && Errors.Any())
                {
                    return true;
                }
                else
                    return false;
            }
        }
        [DataMember]
        public List<object> Errors { get; set; }
        [DataMember]
        public List<object> ErrorCodes { get; set; }

        public string ToErrorMsg()
        {
            if (HasError)
            {
                return Errors.Aggregate((a, b) =>
                {
                    return (a ?? "").ToString() + "\n" + (b ?? "").ToString();
                }).ToString();
            }
            return null;
        }


        public static ApiResponse<T> FromData<T>(T data)
        {
            ApiResponse<T> rp = new ApiResponse<T>(data);
            return rp;
        }

    }
    [DataContract]
    public class ApiResponse<T> : ApiResponseBase
    {
        public ApiResponse()
        {

        }

        public ApiResponse(T data)
        {
            Data = data;
        }
        [DataMember]
        public T Data { get; set; }

    }

}
