﻿using Haravan.Libs.Abstractions;
using System.Linq;

namespace Haravan.Web.Api.BusinessObjects
{
    public static class ResponseHelper
    {
        public static ApiResponseBase ToResponse(this IRequestContext context)
        {
            ApiResponseBase rp = new ApiResponseBase
            {
                Errors = null,
                ErrorCodes = null
            };
            if (context.IsError)
            {
                rp.ErrorCodes = context.Errors.Select(m => m.Key).ToList<object>();
                rp.Errors = context.Errors.Select(m => m.Value).ToList<object>();
            }
            return rp;
        }

        public static ApiResponse<T> ToResponse<T>(this IRequestContext context, T data)
        {
            ApiResponse<T> rp = new ApiResponse<T>(data)
            {
                Errors = null,
                ErrorCodes = null
            };
            if (context.IsError)
            {
                rp.ErrorCodes = context.Errors.Select(m => m.Key).ToList<object>();
                rp.Errors = context.Errors.Select(m => m.Value).ToList<object>();
            }
            return rp;
        }
    }
}