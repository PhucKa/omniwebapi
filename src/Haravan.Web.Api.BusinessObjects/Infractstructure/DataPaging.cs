﻿using System.Runtime.Serialization;

namespace Haravan.Web.Api.BusinessObjects
{
    [DataContract(IsReference = true)]
    public class DataPaging
    {
        public static DataPaging<T> Create<T>(T data, long totalRecords) where T : class
        {
            DataPaging<T> d = new DataPaging<T>
            {
                Data = data,
                TotalRecord = totalRecords
            };
            return d;
        }

        [DataMember]
        public long TotalRecord { get; set; }
    }
    [DataContract]
    public class DataPaging<T> : DataPaging where T : class
    {
        [DataMember]
        public T Data { get; set; }
    }
}
