﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Haravan.Web.Api.BusinessObjects
{
    [DataContract(IsReference = true)]
    public class IntApiResponseBase
    {
        public IntApiResponseBase()
        {
            Errors = new List<object>();
            ErrorCodes = new List<object>();
        }

        [DataMember]
        public bool HasError
        {
            get
            {
                if (Errors != null && Errors.Any())
                {
                    return true;
                }
                else
                    return false;
            }
        }
        [DataMember]
        public List<object> Errors { get; set; }
        [DataMember]
        public List<object> ErrorCodes { get; set; }

        public string ToErrorMsg()
        {
            if (HasError)
            {
                return Errors.Aggregate((a, b) =>
                {
                    return (a ?? "").ToString() + "\n" + (b ?? "").ToString();
                }).ToString();
            }
            return null;
        }


        public static IntApiResponse<T> FromData<T>(T data)
        {
            IntApiResponse<T> rp = new IntApiResponse<T>(data);
            return rp;
        }
        [DataMember]
        public long StoreId { get; set; }
    }
    [DataContract]
    public class IntApiResponse<T> : ApiResponseBase
    {
        public IntApiResponse()
        {

        }

        public IntApiResponse(T data)
        {
            Data = data;
        }
        [DataMember]
        public T Data { get; set; }
        [DataMember]
        public long StoreId { get; set; }
    }
}
