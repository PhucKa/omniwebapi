﻿using Haravan.Libs.Abstractions;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace Haravan.Web.Api.BusinessObjects
{
    public static class IntResponseHelper
    {
        public static IntApiResponseBase ToResponse(this IInternalApiContext context)
        {
            IntApiResponseBase rp = new IntApiResponseBase
            {
                Errors = context.IsError ? context.Errors.Select(m => m.Value).ToList<object>() : null,
                ErrorCodes = context.IsError ? context.Errors.Select(m => m.Key).ToList<object>() : null,
                StoreId = context.OrgId.Value
            };
            return rp;
        }

        public static IntApiResponse<T> ToResponse<T>(this IInternalApiContext context, IRequestContext req, T data)
        {
            var tmp = req;
            IntApiResponse<T> rp = new IntApiResponse<T>(data)
            {
                Errors = context.IsError ? context.Errors.Select(m => m.Value).ToList<object>() : null,
                ErrorCodes = context.IsError ? context.Errors.Select(m => m.Key).ToList<object>() : null,
                StoreId = context.OrgId.Value
            };

            return rp;
        }

        public static IntApiResponse<T> ToResponse<T>(this IInternalApiContext context, T data)
        {
            //var tmp = req;
            IntApiResponse<T> rp = new IntApiResponse<T>(data)
            {
                Errors = context.IsError ? context.Errors.Select(m => m.Value).ToList<object>() : null,
                ErrorCodes = context.IsError ? context.Errors.Select(m => m.Key).ToList<object>() : null,
                StoreId = context.OrgId.Value
            };

            return rp;
        }

        public static IntApiResponse<T> ToResponse<T>(this IServiceProvider serviceProvider, T data)
        {
            var intReq = serviceProvider.GetRequiredService<IInternalApiContext>();
            var context = serviceProvider.GetRequiredService<IRequestContext>();
            IntApiResponse<T> rp = new IntApiResponse<T>(data)
            {
                Errors = context.IsError ? context.Errors.Select(m => m.Value).ToList<object>() : null,
                ErrorCodes = context.IsError ? context.Errors.Select(m => m.Key).ToList<object>() : null,
                StoreId = intReq.OrgId.Value
            };

            return rp;
        }

        public static IntApiResponseBase ToResponse(this IServiceProvider serviceProvider)
        {
            var intReq = serviceProvider.GetRequiredService<IInternalApiContext>();
            var context = serviceProvider.GetRequiredService<IRequestContext>();
            IntApiResponseBase rp = new IntApiResponseBase
            {
                Errors = context.IsError ? context.Errors.Select(m => m.Value).ToList<object>() : null,
                ErrorCodes = context.IsError ? context.Errors.Select(m => m.Key).ToList<object>() : null,
                StoreId = intReq.OrgId.Value
            };
            return rp;
        }
    }
}