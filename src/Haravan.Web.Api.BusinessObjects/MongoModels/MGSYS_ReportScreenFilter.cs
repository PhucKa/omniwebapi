﻿using BHN.Entity;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Runtime.Serialization;

namespace Haravan.Web.Api.BusinessObjects.MongoModels
{
    [DataContract]
    [SeqEntityName("MGSYS_ReportScreenFilter")]
    [BsonIgnoreExtraElements]
    public class MGSYS_ReportScreenFilter : NoSqlEntityBase
    {
        [DataMember]
        public long? reportscreenid { get; set; }

        [DataMember]
        public long? storeid { get; set; }

        [DataMember]
        public string filterarea { get; set; }

        [DataMember]
        public bool isdisplay { get; set; }

        [DataMember]
        public string filtername { get; set; }

        [DataMember]
        public string fieldname { get; set; }

        [DataMember]
        public string filterdatatype { get; set; }

        [DataMember]
        public string operatorsymbol { get; set; }

        [DataMember]
        public long? createduser { get; set; }

        [DataMember]
        public DateTime? createddate { get; set; }

        [DataMember]
        public long? updateduser { get; set; }

        [DataMember]
        public DateTime? updateddate { get; set; }

        [DataMember]
        public string acceptsymbols { get; set; }

        [DataMember]
        public string conjunction { get; set; }
    }
}