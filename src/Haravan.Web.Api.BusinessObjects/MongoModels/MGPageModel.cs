﻿using BHN.Entity;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Haravan.Web.Api.BusinessObjects.MongoModels
{
    [SeqEntityName("Pages")]
    [BsonIgnoreExtraElements]
    public class MGPageModel : NoSqlEntityBase
    {
        public long storeid { get; set; }
        public string authorname { get; set; }
        public string content { get; set; }
        public DateTime? created_at { get; set; }
        public long? created_user { get; set; }
        public string handle { get; set; }
        public object metafield { get; set; }
        public DateTime? published_at { get; set; }
        public long shop_id { get; set; }
        public string template_suffix { get; set; }
        public string title { get; set; }
        public string shortcontent { get; set; }
        public DateTime updated_at { get; set; }
        public long? updated_user { get; set; }
        public string url { get; set; }
        public string meta_description { get; set; }
        public string meta_title { get; set; }       
        public bool IsVisible { get; set; }
        public bool isdeleted { get; set; }
    }
}
