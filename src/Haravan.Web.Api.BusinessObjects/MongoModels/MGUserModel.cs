﻿using BHN.Entity;
using MongoDB.Bson.Serialization.Attributes;

namespace Haravan.Web.Api.BusinessObjects.MongoModels
{
    [BsonIgnoreExtraElements]
    public class MGUserModel : NoSqlEntityBase
    {
        public long storeid { get; set; }
        public bool account_owner { get; set; }
        public string bio { get; set; }
        public string email { get; set; }
        public string first_name { get; set; }
        public string homepage { get; set; }
        public string name { get; set; }
        public string last_name { get; set; }
        public bool isdeleted { get; set; }
    }
}