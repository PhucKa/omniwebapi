﻿using BHN.Entity;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Haravan.Web.Api.BusinessObjects.MongoModels
{
    [DataContract]
    [SeqEntityName("MGAppInstalled")]
    [BsonIgnoreExtraElements]
    public class MGAppInstalled : NoSqlEntityBase
    {
        [DataMember]
        public long StoreId { get; set; }

        [DataMember]
        public long AppId { get; set; }

        [DataMember]
        public long AppTokenId { get; set; }

        [DataMember]
        public string AccessToken { get; set; }

        [DataMember]
        public string Roles { get; set; }

        [DataMember]
        public bool IsBasic { get; set; }

        [DataMember]
        public DateTime AppInstalledDate { get; set; }

        [DataMember]
        public DateTime? AppExpiredDate { get; set; }

        [DataMember]
        public List<MGAppInstalledChangeExpiredDate> OldExpiredDateValue { get; set; }
    }

    [DataContract]
    [SeqEntityName("MGAppInstalledChangeHistory")]
    [BsonIgnoreExtraElements]
    public class MGAppInstalledChangeHistory : NoSqlEntityBase
    {
        [DataMember]
        public long StoreId { get; set; }

        [DataMember]
        public long AppId { get; set; }

        [DataMember]
        public List<MGAppInstalledChangeExpiredDate> OldExpiredDateValue { get; set; }
    }

    [DataContract]
    public class MGAppInstalledChangeExpiredDate
    {
        [DataMember]
        public DateTime? AppExpiredDate { get; set; }

        [DataMember]
        public DateTime ChangedDate { get; set; }

        [DataMember]
        public bool IsTrial { get; set; }

        [DataMember]
        public bool IsCancel { get; set; }

        [DataMember]
        public bool HasBeenCanceled { get; set; }

        [DataMember]
        public bool IsUninstalledApp { get; set; }
    }
}