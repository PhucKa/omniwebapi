﻿using BHN.Entity;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Haravan.Web.Api.BusinessObjects.MongoModels
{
    [SeqEntityName("MGDomainPrice")]
    [BsonIgnoreExtraElements]
    public class MGDomainPrice : NoSqlEntityBase
    {
        public string Extension { get; set; }
        public decimal Price { get; set; }
        public List<MGDomainPriceDiscountModel> Discount { get; set; }
        public int OrderNumber { get; set; }
    }
    [BsonIgnoreExtraElements]
    public class MGDomainPriceDiscountModel
    {
        public int Year { get; set; }
        public int DiscountPercent { get; set; }
    }
}
