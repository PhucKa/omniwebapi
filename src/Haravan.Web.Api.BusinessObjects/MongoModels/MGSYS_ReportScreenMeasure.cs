﻿using BHN.Entity;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Runtime.Serialization;

namespace Haravan.Web.Api.BusinessObjects.MongoModels
{
    [DataContract]
    [SeqEntityName("MGSYS_ReportScreenMeasure")]
    [BsonIgnoreExtraElements]
    public class MGSYS_ReportScreenMeasure : NoSqlEntityBase
    {
        [DataMember]
        public long storeid { get; set; }

        [DataMember]
        public long reportscreenid { get; set; }

        [DataMember]
        public string measurename { get; set; }

        [DataMember]
        public string measurefield { get; set; }

        [DataMember]
        public bool? isdefault { get; set; }

        [DataMember]
        public bool? ismoney { get; set; }

        [DataMember]
        public bool? isselected { get; set; }

        [DataMember]
        public int? selectordernumber { get; set; }

        [DataMember]
        public long? createduser { get; set; }

        [DataMember]
        public DateTime? createddate { get; set; }

        [DataMember]
        public long? updateduser { get; set; }

        [DataMember]
        public DateTime? updateddate { get; set; }

        [DataMember]
        public int? dataformat { get; set; }

        [DataMember]
        public string charttype { get; set; }

        [DataMember]
        public bool? isdraw { get; set; }

        [DataMember]
        public bool? isfix { get; set; }
    }
}