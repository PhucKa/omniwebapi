﻿using BHN.Entity;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Haravan.Web.Api.BusinessObjects.MongoModels
{
    [SeqEntityName("MGUrlRedirectModel")]
    [BsonIgnoreExtraElements]
    public class MGUrlRedirectModel : NoSqlEntityBase
    {
        public long storeid { get; set; }
        public string oldpath { get; set; }
        public string redirectto { get; set; }
        public bool isdeleted { get; set; }
        public long? created_user { get; set; }
        public DateTime created_at { get; set; }
        public long? updated_user { get; set; }
        public DateTime? updated_at { get; set; }
    }
}