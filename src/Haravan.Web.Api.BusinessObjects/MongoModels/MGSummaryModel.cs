﻿using BHN.Entity;
using MongoDB.Bson.Serialization.Attributes;

namespace Haravan.Web.Api.BusinessObjects.MongoModels
{
    [SeqEntityName("Summary")]
    [BsonIgnoreExtraElements]
    public class MGSummaryModel : NoSqlEntityBase
    {
        public long storeid { get; set; }
        public string name { get; set; }
        public long refId { get; set; }
        public int type { get; set; }
    }
}