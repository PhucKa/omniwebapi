﻿using BHN.Entity;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Haravan.Web.Api.BusinessObjects.MongoModels
{
    [SeqEntityName("GoogleSiteVerification")]
    [BsonIgnoreExtraElements]
    public class MGGoogleSiteVerificationModel : NoSqlEntityBase
    {
        public long storeid { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public string verification_code { get; set; }
        public string merchant_id { get; set; }
        public bool is_verify { get; set; }
    }
}