﻿using BHN.Entity;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Haravan.Web.Api.BusinessObjects.MongoModels
{
    [SeqEntityName("Files")]
    [IgnoreUpdate("updated_at")]
    [BsonIgnoreExtraElements]
    public class MGThemeFileModel : NoSqlEntityBase
    {
        public long storeid { get; set; }
        public long themeid { get; set; }
        public string filename { get; set; }
        public string content { get; set; }
        public string url { get; set; }
        public string extension { get; set; }
        public bool isdeleted { get; set; }
        public DateTime updated_at { get; set; }
        public DateTime created_at { get; set; }
    }
}
