﻿using BHN.Entity;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Haravan.Web.Api.BusinessObjects.MongoModels
{
    [SeqEntityName("Articles")]
    [BsonIgnoreExtraElements]
    public class MGArticleModel : NoSqlEntityBase
    {
        public long storeid { get; set; }
        public long blogid { get; set; }       
        public string blogtitle { get; set; }       
        public string bloghandleurl { get; set; }        
        public int blogcommentrule { get; set; }
        public long? authorid { get; set; }
        public string author { get; set; }
        public bool comments_enabled { get; set; }
        public string comment_post_url { get; set; }
        public string content { get; set; }
        public DateTime created_at { get; set; }
        public long? created_user { get; set; }
        public string excerpt { get; set; }
        public string excerpt_or_content { get; set; }
        public bool moderated { get; set; }
        public DateTime? published_at { get; set; }
        public string[] tags { get; set; }
        public string[] tagsformated { get; set; }
        public string title { get; set; }
        public string url { get; set; }
        public string meta_description { get; set; }
        public bool isdeleted { get; set; }
        public string template_suffix { get; set; }
        public MGUserModel user { get; set; }
        public string page_title { get; set; }
        public MGArticleImageModel image { get; set; }       
        public string url_handle { get; set; }
        public DateTime? updated_at { get; set; }
        public long? updated_user { get; set; }       
    }

    [BsonIgnoreExtraElements]
    public class MGArticleImageModel : NoSqlEntityBase
    {
        public string src { get; set; }
        public string alt { get; set; }
    }
}