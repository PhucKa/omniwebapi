﻿using BHN.Entity;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Haravan.Web.Api.BusinessObjects.MongoModels
{
    [SeqEntityName("Blogs")]
    [BsonIgnoreExtraElements]
    public class MGBlogModel : NoSqlEntityBase
    {
        public long storeid { get; set; }
        public string meta_description { get; set; }
        public string meta_title { get; set; }
        public string[] all_tags { get; set; }
        public bool comments_enabled { get; set; }
        public string handle { get; set; }
        public bool moderated { get; set; }        
        public long feedburnerId { get; set; }      
        public string feedburnervalue { get; set; }        
        public int commentrule { get; set; }
        public DateTime published_at { get; set; }
        public string[] tags { get; set; }
        public string title { get; set; }
        public string url { get; set; }
        public string description { get; set; }
        public string feed_burner_url { get; set; }
        public string template_suffix { get; set; }
        public long? updated_user { get; set; }
        public DateTime? updated_at { get; set; }
        public long? created_user { get; set; }
        public DateTime? created_at { get; set; }
        public long totalarticlecount { get; set; }
        public long totalarticlevisiblecount { get; set; }
        public bool isdeleted { get; set; }
    }
}