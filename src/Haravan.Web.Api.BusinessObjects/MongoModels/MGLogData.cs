﻿using BHN.Entity;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Haravan.Web.Api.BusinessObjects.MongoModels
{
    
    [BsonIgnoreExtraElements]
    public class MGLogData:  NoSqlEntityBase<ObjectId>
    {
        public long storeid { get; set; }
        public long refid { get; set; }
        public long doctypeid { get; set; }
        public long created_user { get; set; }
        public DateTime created_date { get; set; }
        public string data { get; set; }
    }
}
