﻿using BHN.Entity;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Haravan.Web.Api.BusinessObjects.MongoModels
{
    [DataContract]
    [SeqEntityName("MGSysDomain")]
    [BsonIgnoreExtraElements]
    public class MGSysDomain : NoSqlEntityBase
    {
        [DataMember]
        public long StoreId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public bool IsSubDomain { get; set; }

        [DataMember]
        public bool IsPrimary { get; set; }

        [DataMember]
        public bool IsRedirect { get; set; }

        [DataMember]
        public bool IsHttps { get; set; }

        [DataMember]
        public int? PriceId { get; set; }

        [DataMember]
        public List<MGChangeSysDomain> OldValue { get; set; }

        [DataMember]
        public int SslType { get; set; }

        [DataMember]
        public long? CertInfoId { get; set; }

        [DataMember]
        public DateTime? ExpireAt { get; set; }
    }

    [DataContract]
    public class MGChangeSysDomain
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public long ChangedUser { get; set; }

        [DataMember]
        public DateTime ChangedNewDate { get; set; }
    }
}