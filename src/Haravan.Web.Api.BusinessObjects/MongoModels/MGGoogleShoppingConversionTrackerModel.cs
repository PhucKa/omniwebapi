﻿using BHN.Entity;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Haravan.Web.Api.BusinessObjects.MongoModels
{
    
    [SeqEntityName("GoogleShoppingConversionTracker")]
    [BsonIgnoreExtraElements]
    public class MGGoogleShoppingConversionTrackerModel : NoSqlEntityBase
    {
        public long storeid { get; set; }
        public string google_shopping_site_tag { get; set; }
        public string google_event_snippet { get; set; }
        public string merchant_id { get; set; }
        public string adwords_id { get; set; }
        public int conversion_type { get; set; } = 1;
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
    }
}