﻿using BHN.Entity;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Haravan.Web.Api.BusinessObjects.MongoModels
{
    [SeqEntityName("Comments")]
    [BsonIgnoreExtraElements]
    public class MGCommentModel : NoSqlEntityBase
    {
        public long storeid { get; set; }
        public long articleid { get; set; }
        public string author { get; set; }
        public string email { get; set; }
        public string body { get; set; }
        public string url { get; set; }
        public int status { get; set; }
        public string content { get; set; }
        public DateTime created_at { get; set; }
        public DateTime update_at { get; set; }
        public DateTime publich_at { get; set; }
        public bool isdeleted { get; set; }
        public string user_agent { get; set; }
        public bool from_api { get; set; }
        public string browseriP { get; set; }
    }
}