﻿using BHN.Entity;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Runtime.Serialization;

namespace Haravan.Web.Api.BusinessObjects.MongoModels
{
    [DataContract]
    [SeqEntityName("MGSYS_ReportScreenFilterData")]
    [BsonIgnoreExtraElements]
    public class MGSYS_ReportScreenFilterData : NoSqlEntityBase
    {
        [DataMember]
        public long? storeid { get; set; }

        [DataMember]
        public long? filterid { get; set; }

        [DataMember]
        public string filterdata { get; set; }

        [DataMember]
        public DateTime? filterdatadatetime { get; set; }

        [DataMember]
        public long? createduser { get; set; }

        [DataMember]
        public DateTime? createddate { get; set; }

        [DataMember]
        public long? updateduser { get; set; }

        [DataMember]
        public DateTime? updateddate { get; set; }

        [DataMember]
        public string operatorsymbol { get; set; }
    }
}