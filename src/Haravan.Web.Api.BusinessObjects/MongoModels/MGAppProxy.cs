﻿using BHN.Entity;
using MongoDB.Bson.Serialization.Attributes;

namespace Haravan.Web.Api.BusinessObjects.MongoModels
{
    [SeqEntityName("MGAppProxy")]
    [BsonIgnoreExtraElements]
    public class MGAppProxy : NoSqlEntityBase
    {
        public long AppId { get; set; }
        public long StoreId { get; set; }
        public string SubPathPrefixProxy { get; set; }
        public string SubPathProxy { get; set; }
        public string ProxyURL { get; set; }
        public string APIKey { get; set; }
    }
}
