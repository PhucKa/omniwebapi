﻿using BHN.Entity;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Runtime.Serialization;

namespace Haravan.Web.Api.BusinessObjects.MongoModels
{
    [DataContract]
    [SeqEntityName("MGAcmeInfo")]
    [BsonIgnoreExtraElements]
    public class MGCertInfo : NoSqlEntityBase
    {
        [DataMember]
        public string Cert { get; set; }

        [DataMember]
        public string CertRequest { get; set; }

        [DataMember]
        public string PrivateKey { get; set; }

        [DataMember]
        public DateTime IssueAt { get; set; }

        [DataMember]
        public DateTime ExpireAt { get; set; }

        [DataMember]
        public DateTime ShouldRefeshAt { get; set; }

        [DataMember]
        public DateTime? LastRefreshAt { get; set; }

        [DataMember]
        public DateTime CreatedAt { get; set; }

        [DataMember]
        public DateTime UpdatedAt { get; set; }

        [DataMember]
        public int SslType { get; set; }
    }
}