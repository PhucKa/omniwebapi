using BHN.Entity;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Haravan.Web.Api.BusinessObjects.MongoModels
{
    [SeqEntityName("CartProxy")]
    [BsonIgnoreExtraElements]
    public class MGCartProxyModel : NoSqlEntityBase
    {
        public long storeid { get; set; }
        public string url { get; set; }
        public long? appid { get; set; }
        public string appsecretkey { get; set; }
        public string appapikey { get; set; }
        public DateTime created_at { get; set; }
        public DateTime? updated_at { get; set; }
    }
}