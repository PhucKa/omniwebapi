﻿using BHN.Entity;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Haravan.Web.Api.BusinessObjects.MongoModels
{
    [SeqEntityName("ScriptTags")]
    [BsonIgnoreExtraElements]
    public class MGScriptTagsModel : NoSqlEntityBase
    {
        public long storeid { get; set; }
        public DateTime created_at { get; set; }
        public long created_user { get; set; }
        public DateTime updated_at { get; set; }
        public long updated_user { get; set; }
        public string evenname { get; set; }
        public string src { get; set; }
        public string scriptname { get; set; }
        public long? appid { get; set; }
        public int display_scope { get; set; } = 1;
    }
}
