﻿using BHN.Entity;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Haravan.Web.Api.BusinessObjects.MongoModels
{
    [DataContract]
    [SeqEntityName("Store")]
    [BsonIgnoreExtraElements]
    public class MGSysStore : NoSqlEntityBase
    {
        [DataMember]
        public int ShopPlanId { get; set; }

        [DataMember]
        public DateTime DisplayExpiryDate { get; set; }

        [DataMember]
        public DateTime ActualExpiryDate { get; set; }

        [DataMember]
        public int Type { get; set; }

        [DataMember]
        public string Ref { get; set; }

        [DataMember]
        public string HChan { get; set; }

        [DataMember]
        public string Referrer { get; set; }

        [DataMember]
        public string ReferringSite { get; set; }

        [DataMember]
        public string LandingSite { get; set; }

        [DataMember]
        public string LandingSiteRef { get; set; }

        [DataMember]
        public string UtmSource { get; set; }

        [DataMember]
        public string UtmMedium { get; set; }

        [DataMember]
        public string UtmCampaign { get; set; }

        [DataMember]
        public string UtmTerm { get; set; }

        [DataMember]
        public string UtmContent { get; set; }

        [DataMember]
        public List<MGSysStoreChangeShopPlan> OldShopPlanValue { get; set; }

        [DataMember]
        public int FacebookPlanId { get; set; }

        [DataMember]
        public DateTime FacebookPlanExpiryDate { get; set; }

        [DataMember]
        public int? FacebookPlanPageNum { get; set; }

        [DataMember]
        public List<MGSysStoreChangeFacebookPlan> OldFacebookPlanValue { get; set; }

        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public string Seller_SqlCnn { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }

        [DataMember]
        public bool IsFinishSetupAccount { get; set; }

        [DataMember]
        public int RegisterType { get; set; }

        [DataMember]
        public int FacebookResetTrialTimes { get; set; }

        [DataMember]
        public string IpAssigned { get; set; }

        [DataMember]
        public DateTime? HaraTrackingExpiredDate { get; set; }

        [DataMember]
        public List<MGSysStoreChangeHaraTracking> OldStoreChangeHaraTracking { get; set; }

        [DataMember]
        public int? AdminUserNum { get; set; }

        [DataMember]
        public DateTime? AdminUserNumExpiredDate { get; set; }

        [DataMember]
        public List<MGSysStoreChangeAdminUserNum> OldStoreChangeAdminUserNum { get; set; }

        [DataMember]
        public int? TotalCurrentStore { get; set; }
    }

    [DataContract]
    public class MGSysStoreChangeShopPlan
    {
        [DataMember]
        public int ShopPlanId { get; set; }

        [DataMember]
        public DateTime DisplayExpiryDate { get; set; }

        [DataMember]
        public DateTime ActualExpiryDate { get; set; }

        [DataMember]
        public int Type { get; set; }

        [DataMember]
        public string Ref { get; set; }

        [DataMember]
        public DateTime ChangedDate { get; set; }

        [DataMember]
        public bool IsCancel { get; set; }

        [DataMember]
        public bool HasBeenCanceled { get; set; }
    }

    [DataContract]
    public class MGSysStoreChangeFacebookPlan
    {
        [DataMember]
        public int FacebookPlanId { get; set; }

        [DataMember]
        public DateTime FacebookPlanExpiryDate { get; set; }

        [DataMember]
        public int? FacebookPlanPageNum { get; set; }

        [DataMember]
        public DateTime ChangedDate { get; set; }

        [DataMember]
        public bool IsCancel { get; set; }

        [DataMember]
        public bool HasBeenCanceled { get; set; }
    }

    [DataContract]
    public class MGSysStoreChangeHaraTracking
    {
        [DataMember]
        public DateTime? HaraTrackingExpiredDate { get; set; }

        [DataMember]
        public DateTime ChangedDate { get; set; }

        [DataMember]
        public bool IsCancel { get; set; }

        [DataMember]
        public bool HasBeenCanceled { get; set; }
    }

    [DataContract]
    public class MGSysStoreChangeAdminUserNum
    {
        [DataMember]
        public int? AdminUserNum { get; set; }

        [DataMember]
        public DateTime? AdminUserNumExpiredDate { get; set; }

        [DataMember]
        public DateTime ChangedDate { get; set; }

        [DataMember]
        public bool IsCancel { get; set; }

        [DataMember]
        public bool HasBeenCanceled { get; set; }
    }
}