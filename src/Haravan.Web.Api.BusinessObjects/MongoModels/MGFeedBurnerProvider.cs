﻿using BHN.Entity;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Haravan.Web.Api.BusinessObjects.MongoModels
{
    [SeqEntityName("FeedBurnerProvider")]
    [BsonIgnoreExtraElements]
    public class MGFeedBurnerProviderModel : NoSqlEntityBase
    {
        public string url { get; set; }
        public int status { get; set; } = 1;
        public long created_user { get; set; }
        public long? updated_user { get; set; }
        public DateTime created_date { get; set; }
        public DateTime updated_date { get; set; }
    }
}