﻿using BHN.Entity;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Runtime.Serialization;

namespace Haravan.Web.Api.BusinessObjects.MongoModels
{
    [DataContract]
    [SeqEntityName("MGSYS_ReportScreenGroupProperty")]
    [BsonIgnoreExtraElements]
    public class MGSYS_ReportScreenGroupProperty : NoSqlEntityBase
    {
        [DataMember]
        public long? storeid { get; set; }

        [DataMember]
        public long reportscreenid { get; set; }

        [DataMember]
        public string groupareaname { get; set; }

        [DataMember]
        public string grouppropertyfield { get; set; }

        [DataMember]
        public string grouppropertyname { get; set; }

        [DataMember]
        public bool? isselected { get; set; }

        [DataMember]
        public long? createduser { get; set; }

        [DataMember]
        public DateTime? createddate { get; set; }

        [DataMember]
        public long? updateduser { get; set; }

        [DataMember]
        public DateTime? updateddate { get; set; }

        [DataMember]
        public int? selectordernumber { get; set; }

        [DataMember]
        public int? ordernumber { get; set; }

        [DataMember]
        public int? ordernumbergroup { get; set; }

        [DataMember]
        public string dataformat { get; set; }

        [DataMember]
        public bool? isdisplay { get; set; }

        [DataMember]
        public bool? isfix { get; set; }
    }
}