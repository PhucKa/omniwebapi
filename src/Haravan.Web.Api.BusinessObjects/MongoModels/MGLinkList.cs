﻿using BHN.Entity;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Haravan.Web.Api.BusinessObjects.MongoModels
{
    [BsonIgnoreExtraElements]
    public class MGLinkListModel : NoSqlEntityBase
    {
        public long storeid { get; set; }
        public string handle { get; set; }
        public List<MGLinkModel> links { get; set; }
        public string title { get; set; }
    }
    [BsonIgnoreExtraElements]
    public class MGLinkModel
    {
        public long id { get; set; }
        public long? refid { get; set; }
        public long storeid { get; set; }
        public string handle { get; set; }
        public string title { get; set; }
        public string type { get; set; }
        public string url { get; set; }
        public string tags { get; set; }
        public int sort { get; set; }
    }
    [SeqEntityName("MGRawLinkFields")]
    [BsonIgnoreExtraElements]
    public class MGRawLinkFields : NoSqlEntityBase
    {
        public long StoreId { get; set; }
        public string FieldName { get; set; }
        public string FieldDisplay { get; set; }
        public bool? HasText { get; set; }
        public bool? HasTags { get; set; }
        public bool? HasOptions { get; set; }
        public string OptionsUrl { get; set; }
        public string LinkValue { get; set; }
        public bool IsDeleted { get; set; } = false;
        public long? CreatedUser { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? UpdatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
    [SeqEntityName("MGRawLinkListFields")]
    [BsonIgnoreExtraElements]
    public class MGRawLinkListFields : NoSqlEntityBase
    {
        public long LinkListId { get; set; }
        public int LinkFieldsId { get; set; }
        public long StoreId { get; set; }
        public string LinkListFieldName { get; set; }
        public string LinkListFieldHandle { get; set; }
        public int LinkListFieldOrder { get; set; }
        public string TreeHandle { get; set; }
        public long? RefId { get; set; }
        public string TextValue { get; set; }
        public string Tags { get; set; }
        public bool IsDeleted { get; set; } = false;
        public long? CreatedUser { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? UpdatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
    [SeqEntityName("MGRawLinkList")]
    [BsonIgnoreExtraElements]
    public class MGRawLinkList : NoSqlEntityBase
    {
        public long StoreId { get; set; }
        public string LinkListName { get; set; }
        public string LinkListHandle { get; set; }
        public string TreeHandle { get; set; }
        public bool IsSystemLink { get; set; } = false;
        public bool IsDeleted { get; set; } = false;
        public long? CreatedUser { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? UpdatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }

    }
}
