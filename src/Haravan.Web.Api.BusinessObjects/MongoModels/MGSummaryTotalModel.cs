﻿using BHN.Entity;
using MongoDB.Bson.Serialization.Attributes;

namespace Haravan.Web.Api.BusinessObjects.MongoModels
{
    [SeqEntityName("SummaryTotal")]
    [BsonIgnoreExtraElements]
    public class MGSummaryTotalModel : NoSqlEntityBase
    {
        public long storeid { get; set; }
        public int type { get; set; }
        public string name { get; set; }
        public int count { get; set; }
    }
}