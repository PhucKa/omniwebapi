﻿using BHN.Entity;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Haravan.Web.Api.BusinessObjects.MongoModels
{
    [SeqEntityName("MGKeywordHandle")]
    [BsonIgnoreExtraElements]
    public class MGKeywordHandle : NoSqlEntityBase
    {
        public long storeid { get; set; }
        public int type { get; set; }
        public string basickeyword { get; set; }
        public string keywordout { get; set; }
        public long? index { get; set; }
        public long refid { get; set; }
    }
}
