﻿using BHN.Entity;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Runtime.Serialization;

namespace Haravan.Web.Api.BusinessObjects.MongoModels
{
    [DataContract]
    [SeqEntityName("MGSYS_ReportScreen")]
    [BsonIgnoreExtraElements]
    public class MGSYS_ReportScreen : NoSqlEntityBase
    {
        [DataMember]
        public long storeid { get; set; }

        [DataMember]
        public int viewid { get; set; }

        [DataMember]
        public string typename { get; set; }

        [DataMember]
        public string reportname { get; set; }

        [DataMember]
        public string reportnamesystem { get; set; }

        [DataMember]
        public string description { get; set; }

        [DataMember]
        public bool issystemreport { get; set; }

        [DataMember]
        public bool? isaddnewreport { get; set; }

        [DataMember]
        public long? createduser { get; set; }

        [DataMember]
        public DateTime? createddate { get; set; }

        [DataMember]
        public long? updateduser { get; set; }

        [DataMember]
        public DateTime? updateddate { get; set; }

        [DataMember]
        public string fieldsort { get; set; }

        [DataMember]
        public string sortype { get; set; }

        [DataMember]
        public int? ordernumber { get; set; }

        [DataMember]
        public string displaychartmode { get; set; }

        [DataMember]
        public bool? isdisplay { get; set; }
    }
}