﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Haravan.Web.Api.BusinessObjects
{
    public class KafkaTopics
    {
        public const string InitDataNewShop = "omni_init_data_new_shop";
    }
    public enum UserAccountType
    {
        [Display(Name = "")]
        Default = 1, // web
        [Display(Name = "Facebook")]
        FromFacebook = 2,
        [Display(Name = "Haravan Account")]
        FromHaravanAccount = 4
    }
    public class DefaultData
    {
        public const int DefaultMaxArticleTitleLength = 255;
        public const int DefaultMaxBlogTitleLength = 255;
        public const int DefaultMaxArticlePageTitleLength = 70;
        public const int DefaultMaxArticleAuthorNameLength = 100;
        public const int DefaultMaxArticleDescriptionLength = 2000000;
        public const int DefaultMaxBlogDescriptionLength = 2000000;
        public const int DefaultMaxPageDescriptionLength = 2000000;
        public const int DefaultMaxMetaDescription = 320;
        public const int DefaultQuantityImage = 100;
        public const int Default85QuantityImage = 85;
        public const string DefaultPrefixEcomCache = "ecom_";

        public const long DefaultStoreId = 0L;
        public const int TimeAllowChangeSubDomain = 1;

        public static readonly List<string> DomainExtensions = new List<string>() {
            ".net.vn",".com.vn",".biz.vn",".info.vn",
            ".gov.vn",".org.vn",".edu.vn",".com",".org",".vn",".biz",".net",".info",
            ".us",".tv",".tel",".asia",".me",".cc",".ws",".co"
        };

        public const int DefaultMaxScripttagSrcLength = 500;
    }
}