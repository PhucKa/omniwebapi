﻿using System.ComponentModel.DataAnnotations;

namespace Haravan.Web.Api.BusinessObjects.Enums
{
    public enum ThemeType : int
    {
        [Display(Name = "Không sử dụng")]
        None = 0,

        [Display(Name = "Theme chính")]
        Main = 1,

        [Display(Name = "Theme Mobile")]
        Mobile = 2
    }
}