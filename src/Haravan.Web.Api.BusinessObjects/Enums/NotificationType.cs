﻿using System.ComponentModel.DataAnnotations;

namespace Haravan.Web.Api.BusinessObjects.Enums
{
    public enum NotificationType
    {
        [Display(Name = "Page")]
        Page = 1,

        [Display(Name = "Blog")]
        Blog = 2,

        [Display(Name = "Article")]
        Article = 3,

        [Display(Name = "Comment")]
        Comment = 4,

        [Display(Name = "Theme")]
        Theme = 5,

        [Display(Name = "Menu")]
        Menu = 6,

        [Display(Name = "Setting")]
        Setting = 7,

        [Display(Name = "Domain")]
        Domain = 8,

        [Display(Name = "Checkout")]
        Checkout = 9,

        [Display(Name = "File")]
        File = 10
    }
}