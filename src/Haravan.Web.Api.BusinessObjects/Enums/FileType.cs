﻿namespace Haravan.Web.Api.BusinessObjects.Enums
{
    public enum FileType
    {
        None = 0,
        ProductImage = 1,
        CollectionImage = 2,
        ArticleFeatureImage = 3,
        ImportFile = 8,
        ExportFile = 9,
        StorageFile = 10,
        Theme_Layout = 11,
        Theme_Template = 12,
        Theme_Snippets = 13,
        Theme_Assets = 14,
        Theme_Config = 15,
        Theme_Image = 16,
        ExportTheme = 17,
        FacebookStore_BannerImage = 18,
        ExportImportTempFile = 19,
        ScriptTagFile = 20,
        FiveVNStore_BannerImage = 21,
        Inside_Announcement_Icon = 22,
        App_Banner = 23,
        App_LargeIcon = 24,
        App_SmallIcon = 25,
        OrderProduct_Properties = 26,
        Inside_ThemeStore_ZipUrl = 27,
        Inside_ThemeStore_DefaultImage = 28,
        Inside_Case_AttachedFile = 29,
        Theme_Locales = 30,
        EmailLogoImage = 31,
        Inside_UserDetail_Image = 32
    }
}