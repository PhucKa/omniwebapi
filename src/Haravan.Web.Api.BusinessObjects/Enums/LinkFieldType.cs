﻿using System.ComponentModel.DataAnnotations;

namespace Haravan.Web.Api.BusinessObjects.Enums
{
    public enum LinkFieldType
    {
        [Display(Name = "Store Frontpage")]
        StoreFrontpage = 1,

        [Display(Name = "Collection")]
        Collection = 2,

        [Display(Name = "Product")]
        Product = 3,

        [Display(Name = "All Products")]
        AllProducts = 4,

        [Display(Name = "Page")]
        Page = 5,

        [Display(Name = "Blog")]
        Blog = 6,

        [Display(Name = "Search Page")]
        SearchPage = 7,

        [Display(Name = "Web Address")]
        WebAddress = 8
    }
}