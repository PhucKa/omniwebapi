﻿namespace Haravan.Web.Api.BusinessObjects.Enums
{
    public enum ThemeDropdownListType : int
    {
        Blog = 1,
        Collection = 2,
        Page = 3,
        Article = 4,
        Menu = 5,
        Product = 6
    }
}