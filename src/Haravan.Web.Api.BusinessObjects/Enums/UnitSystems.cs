﻿using System.ComponentModel.DataAnnotations;

namespace Haravan.Web.Api.BusinessObjects.Enums
{
    public enum UnitSystems : int
    {
        [Display(Name = "Imperial system (Pound, inch)")]
        ImperialSys = 1,

        [Display(Name = "Metric system (Kilogram, centimeter)")]
        MetricSys = 2,
    }
}