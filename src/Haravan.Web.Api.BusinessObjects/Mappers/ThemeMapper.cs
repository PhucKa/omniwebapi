﻿using AutoMapper;
using BHN.SharedObject.EBSMessage.TaskProcessMsg;
using Haravan.Web.Api.BusinessObjects.Enums;
using Haravan.Web.Api.BusinessObjects.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Haravan.Web.Api.BusinessObjects.Mappers
{
    public class ThemeMapperProfile : Profile
    {
        public ThemeMapperProfile()
        {
            ///mapper
            CreateMap<Themes, ThemeModel>()
                   .ForMember(m => m.Type, r => r.ResolveUsing(t => { return ((ThemeType)t.Type).ToString(); }))
                   .ForMember(m => m.DefaultCategories, r => r.ResolveUsing(t =>
                   {
                       var result = new List<string>();
                       if (!string.IsNullOrEmpty(t.DefaultCategories))
                           result = t.DefaultCategories.ToLower().Split(',').ToList();
                       return result;
                   }))
                   .ForMember(m => m.DefaultImageUrls, r => r.ResolveUsing(t =>
                   {
                       var result = new List<string>();
                       if (!string.IsNullOrEmpty(t.DefaultImageUrls))
                           result = t.DefaultImageUrls.Split(',').ToList();
                       return result;
                   }));
            CreateMap<ThemeModel, ThemeExportMsg>();
            CreateMap<ThemePublishModel, ThemeModel>();
            CreateMap<FreeThemesModel, ThemeExportMsg>();
            CreateMap<ThemeModel, Themes>()
                .ForMember(m => m.Type, r => r.ResolveUsing(t => { return (int)((ThemeType)Enum.Parse(typeof(ThemeType), t.Type)); }));
        }
    }

    public static class ThemeMapper
    {
        static ThemeMapper()
        {
            Mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ThemeMapperProfile>();
            }).CreateMapper();
        }

        internal static IMapper Mapper { get; }

        public static ThemeModel ThemeToThemeModel(this Themes entity)
        {
            return Mapper.Map<ThemeModel>(entity);
        }

        public static ThemeExportMsg ThemeModelToThemeExportMsg(this ThemeModel entity)
        {
            return Mapper.Map<ThemeExportMsg>(entity);
        }

        public static ThemeModel ThemePublishModelToThemeModel(this ThemePublishModel entity)
        {
            return Mapper.Map<ThemeModel>(entity);
        }

        public static ThemeExportMsg FreeThemesModelToThemeExportMsg(this FreeThemesModel entity)
        {
            return Mapper.Map<ThemeExportMsg>(entity);
        }
    }
}