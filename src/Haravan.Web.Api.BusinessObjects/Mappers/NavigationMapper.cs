﻿using AutoMapper;
using Haravan.Web.Api.BusinessObjects.ESModels;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.BusinessObjects.Models.Common;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Haravan.Web.Api.BusinessObjects.Mappers
{
    public class NavigationMapperProfile : Profile
    {
        Regex regexAcceptCharacter = new Regex(@"[^a-zA-Z0-9@.#-&()|+]", RegexOptions.Compiled);
        public NavigationMapperProfile()
        {
            #region Url Redirect

            CreateMap<MGUrlRedirectModel, UrlRedirectModel>()
               .ForMember(m => m.Id, k => k.MapFrom(a => a._id));
            CreateMap<UrlRedirectModel, MGUrlRedirectModel>()
              .ForMember(m => m._id, k => k.MapFrom(a => a.Id));

            CreateMap<ESUrlRedirectModel, MGUrlRedirectModel>()
              .ForMember(m => m._id, k => k.MapFrom(a => a.Id))
              .ForMember(m => m.created_at, k => k.MapFrom(a => a.CreatedDate))
                  .ForMember(m => m.oldpath, k => k.ResolveUsing(c =>
                  {
                      if (!string.IsNullOrEmpty(c.OldPath))
                      {
                          return regexAcceptCharacter.Replace(System.Web.HttpUtility.UrlDecode(c.OldPath), " ");
                      }
                      else return null;
                  }))   
                  .ForMember(m => m.redirectto, m => m.ResolveUsing(c =>
                  {
                      if (!string.IsNullOrEmpty(c.RedirectTo))
                      {
                          return regexAcceptCharacter.Replace(System.Web.HttpUtility.UrlDecode(c.RedirectTo), " ");
                      }
                      else return null;
                  }))
                  .ForMember(m => m.oldpath , k => k.ResolveUsing(c => c.OldPath_ori))
                  .ForMember(m => m.redirectto, k => k.ResolveUsing(c => c.RedirectTo_ori))
              ;

            CreateMap<MGUrlRedirectModel, ESUrlRedirectModel>()
              .ForMember(m => m.Id, k => k.MapFrom(a => a._id))
              .ForMember(m => m.CreatedDate, k => k.MapFrom(a => a.created_at))
              .ForMember(m => m.OldPath, k => k.ResolveUsing(c =>
              {
                  if (!string.IsNullOrEmpty(c.oldpath))
                  {
                      return regexAcceptCharacter.Replace(System.Web.HttpUtility.UrlDecode(c.oldpath), " ");
                  }
                  else return null;
              }))
                  .ForMember(m => m.RedirectTo, m => m.ResolveUsing(c =>
                  {
                      if (!string.IsNullOrEmpty(c.redirectto))
                      {
                          return regexAcceptCharacter.Replace(System.Web.HttpUtility.UrlDecode(c.redirectto), " ");
                      }
                      else return null;
                  }))
                  .ForMember(m => m.OldPath_ori, k => k.ResolveUsing(c => c.oldpath))
                  .ForMember(m => m.RedirectTo_ori, k => k.ResolveUsing(c => c.redirectto))
              ;

            #endregion Url Redirect

            #region Collection Product

            CreateMap<CollectionListModelRequest, DropdownlistModel>()
                   .ForMember(dest => dest.Id, src => src.MapFrom(k => k.Id))
                   .ForMember(dest => dest.Name, src => src.MapFrom(k => k.Title));


            CreateMap<ProductSearchRowModel, DropdownlistModel>()
                   .ForMember(dest => dest.Id, src => src.MapFrom(k => k.Id))
                   .ForMember(dest => dest.Name, src => src.MapFrom(k => k.ProductName));

            #endregion Collection Product
        }
    }

    public static class NavigationMapper
    {
        static NavigationMapper()
        {
            Mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<CommonMapperProfile>();
                cfg.AddProfile<NavigationMapperProfile>();
            }).CreateMapper();
        }


        internal static IMapper Mapper { get; }

        #region Url Redirect

        public static MGUrlRedirectModel MGUrlToUrlModel(this UrlRedirectModel entity)
        {
            var model = Mapper.Map<MGUrlRedirectModel>(entity);
            return model;
        }
        public static UrlRedirectModel ModelUrlToUrlMG(this MGUrlRedirectModel entity)
        {
            var model = Mapper.Map<UrlRedirectModel>(entity);
            return model;
        }

        public static List<UrlRedirectModel> ModelToModelList(this List<MGUrlRedirectModel> entity)
        {
            return Mapper.Map<List<UrlRedirectModel>>(entity);
        }

        public static ESUrlRedirectModel MGToModelES(this MGUrlRedirectModel entity)
        {
            var model = Mapper.Map<ESUrlRedirectModel>(entity);

            return model;
        }

        #endregion Url Redirect

        #region Collection Product
        public static List<DropdownlistModel> DropdownToListCollection(this List<CollectionListModelRequest> entity)
        {
            return Mapper.Map<List<DropdownlistModel>>(entity);
        }
        public static List<DropdownlistModel> DropdownToListProduct(this List<ProductSearchRowModel> entity)
        {
            return Mapper.Map<List<DropdownlistModel>>(entity);
        }
        #endregion Collection Product
    }
}