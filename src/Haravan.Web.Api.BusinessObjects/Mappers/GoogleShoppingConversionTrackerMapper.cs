﻿using AutoMapper;
using Haravan.Web.Api.BusinessObjects.Models.Google;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System.Collections.Generic;

namespace Haravan.Web.Api.BusinessObjects.Mappers
{
    public class GoogleShoppingConversionTrackerProfile : Profile
    {
        public GoogleShoppingConversionTrackerProfile()
        {
            CreateMap<MGGoogleShoppingConversionTrackerModel, GoogleShoppingConversionTrackerDetailApiModel>()
                .ForMember(m => m.id, k => k.MapFrom(a => a._id)).ReverseMap();
        }
    }

    public static class GoogleShoppingConversionTrackerMapper
    {
        static GoogleShoppingConversionTrackerMapper()
        {
            Mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<CommonMapperProfile>();
                cfg.AddProfile<GoogleShoppingConversionTrackerProfile>();
            }).CreateMapper();
        }

        internal static IMapper Mapper { get; }

        public static MGGoogleShoppingConversionTrackerModel MGGoogleShoppingConversionTrackerToGoogleSiteVerification(this GoogleShoppingConversionTrackerDetailApiModel entity)
        {
            return Mapper.Map<MGGoogleShoppingConversionTrackerModel>(entity);
        }

        public static GoogleShoppingConversionTrackerDetailApiModel GoogleShoppingConversionTrackerToMGGoogleSiteVerification(this MGGoogleShoppingConversionTrackerModel entity)
        {
            return Mapper.Map<GoogleShoppingConversionTrackerDetailApiModel>(entity);
        }

        public static List<GoogleShoppingConversionTrackerDetailApiModel> ListGoogleShoppingConversionTrackerToMGGoogleSiteVerification(this List<MGGoogleShoppingConversionTrackerModel> entity)
        {
            return Mapper.Map<List<GoogleShoppingConversionTrackerDetailApiModel>>(entity);
        }
    }
}