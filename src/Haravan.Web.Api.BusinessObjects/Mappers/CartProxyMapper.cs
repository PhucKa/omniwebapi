﻿using AutoMapper;
using BHN.SharedObject.APIDataModel;
using Haravan.Web.Api.BusinessObjects.MongoModels;

namespace Haravan.Web.Api.BusinessObjects.Mappers
{
    public class CartProxyProfile : Profile
    {
        public CartProxyProfile()
        {
            CreateMap<MGCartProxyModel, CartProxyAPIModel>()
                .ForMember(m => m.id, k => k.MapFrom(a => a._id))
                .ForMember(m => m.address, k => k.MapFrom(a => a.url))
                .ForMember(m => m.created_at, k => k.MapFrom(a => a.created_at))
                .ForMember(m => m.updated_at, k => k.MapFrom(a => a.updated_at));
            CreateMap<MGCartProxyModel, CartProxyModel>()
               .ForMember(m => m.AppApiKey, k => k.MapFrom(a => a.appapikey))
               .ForMember(m => m.AppSecretKey, k => k.MapFrom(a => a.appsecretkey))
               .ForMember(m => m.Url, k => k.MapFrom(a => a.url))
               .ForMember(m => m.AppId, k => k.MapFrom(a => a.appid));
        }
    }

    public static class CartProxyMapper
    {
        static CartProxyMapper()
        {
            Mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<CommonMapperProfile>();
                cfg.AddProfile<CartProxyProfile>();
            }).CreateMapper();
        }

        internal static IMapper Mapper { get; }

        public static CartProxyAPIModel ToApiModel(this MGCartProxyModel entity)
        {
            return Mapper.Map<CartProxyAPIModel>(entity);
        }
        public static CartProxyModel ToModel(this MGCartProxyModel entity)
        {
            return Mapper.Map<CartProxyModel>(entity);
        }
    }
}
