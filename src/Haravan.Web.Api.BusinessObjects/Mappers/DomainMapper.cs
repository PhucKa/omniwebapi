﻿using AutoMapper;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System.Collections.Generic;

namespace Haravan.Web.Api.BusinessObjects.Mappers
{
    public class DomainProfile : Profile
    {
        public DomainProfile()
        {
            CreateMap<MGSysDomain, SYSDomainModel>()
                .ForMember(m => m.Id, k => k.MapFrom(a => a._id));
            CreateMap<SYSDomainModel, MGSysDomain>()
                .ForMember(m => m._id, k => k.MapFrom(a => a.Id));

            CreateMap<MGDomainPrice, DomainPriceModel>()
                    .ForMember(a => a.Id, b => b.MapFrom(c => c._id));

            CreateMap<DomainPriceModel, MGDomainPrice>()
                .ForMember(a => a._id, b => b.MapFrom(c => c.Id));

            CreateMap<DomainPriceDiscountModel, MGDomainPriceDiscountModel>();
            CreateMap<MGDomainPriceDiscountModel, DomainPriceDiscountModel>();
        }
    }

    public static class DomainMapper
    {
        static DomainMapper()
        {
            Mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<CommonMapperProfile>();
                cfg.AddProfile<DomainProfile>();
            }).CreateMapper();
        }

        internal static IMapper Mapper { get; }

        public static SYSDomainModel ToModelSYSDomain(this MGSysDomain entity)
        {
            return Mapper.Map<SYSDomainModel>(entity);
        }

        public static MGSysDomain MGSysToSYSDomain(this SYSDomainModel entity)
        {
            return Mapper.Map<MGSysDomain>(entity);
        }

        public static List<SYSDomainModel> ToModelSYSDomainList(this List<MGSysDomain> entity)
        {
            return Mapper.Map<List<SYSDomainModel>>(entity);
        }

        public static List<DomainPriceModel> DomainPriceToMGDomainPrice(this List<MGDomainPrice> entity)
        {
            var model = Mapper.Map<List<DomainPriceModel>>(entity);
            return model;
        }
    }
}