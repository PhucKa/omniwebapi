﻿using AutoMapper;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System.Collections.Generic;
using System.Linq;

namespace Haravan.Web.Api.BusinessObjects.Mappers
{
    public class ReportMapperProfile : Profile
    {
        public ReportMapperProfile()
        {
            CreateMap<MGSYS_ReportScreen, ReportScreenRowModel>()
                 .ForMember(m => m.Id, k => k.MapFrom(a => a._id))
                 .ReverseMap();
            CreateMap<ReportScreenRowModel, MGSYS_ReportScreen>()
                .ForMember(m => m._id, k => k.MapFrom(a => a.Id))
                .ReverseMap();

            CreateMap<MGSYS_ReportScreen, ReportScreen>()
                .ForMember(m => m.Id, k => k.MapFrom(a => a._id));
            CreateMap<ReportScreen, MGSYS_ReportScreen>()
                .ForMember(m => m._id, k => k.MapFrom(a => a.Id));

            CreateMap<MGSYS_ReportScreenFilter, ReportScreenFilter>()
                .ForMember(m => m.Id, k => k.MapFrom(a => a._id));
            CreateMap<ReportScreenFilter, MGSYS_ReportScreenFilter>()
                .ForMember(m => m._id, k => k.MapFrom(a => a.Id));

            CreateMap<MGSYS_ReportScreenFilterData, ReportScreenFilterData>()
                .ForMember(m => m.Id, k => k.MapFrom(a => a._id));
            CreateMap<ReportScreenFilterData, MGSYS_ReportScreenFilterData>()
                .ForMember(m => m._id, k => k.MapFrom(a => a.Id));

            CreateMap<MGSYS_ReportScreenGroupProperty, ReportScreenGroupProperty>()
                .ForMember(m => m.Id, k => k.MapFrom(a => a._id));
            CreateMap<ReportScreenGroupProperty, MGSYS_ReportScreenGroupProperty>()
                .ForMember(m => m._id, k => k.MapFrom(a => a.Id));

            CreateMap<MGSYS_ReportScreenMeasure, ReportScreenMeasure>()
                .ForMember(m => m.Id, k => k.MapFrom(a => a._id));
            CreateMap<ReportScreenMeasure, MGSYS_ReportScreenMeasure>()
                .ForMember(m => m._id, k => k.MapFrom(a => a.Id));

            CreateMap<ReportScreen, ReportScreen>()
               .ForMember(dest => dest.LstReportScreenFilter, src => src.ResolveUsing(p => p.LstReportScreenFilter))
               .ForMember(dest => dest.LstReportScreenGroupProperty, src => src.ResolveUsing(p => p.LstReportScreenGroupProperty))
               .ForMember(dest => dest.LstReportScreenMeasure, src => src.ResolveUsing(p => p.LstReportScreenMeasure));

            CreateMap<DataTempmodel, Rootobject>()                    
                    .ForMember(dest => dest.view, src => src.ResolveUsing(p => p.View))
                    .ForMember(dest => dest.u, src => src.MapFrom(p => p.TotalVisitors))
                    .ForMember(dest => dest.n, src => src.MapFrom(p => p.NewVisitors))
                    .ForMember(dest => dest.t, src => src.MapFrom(p => p.TotalVisits))
                    .ForMember(dest => dest.dcalc, src => src.MapFrom(p => p.Avgtime))
                    .ForMember(dest => dest.s, src => src.ResolveUsing(p => p.Landings))
                    .ForMember(dest => dest.e, src => src.MapFrom(p => p.Exits))
                    .ForMember(dest => dest.b, src => src.MapFrom(p => p.Bounces))
                    .ForMember(dest => dest.scrcalc, src => src.MapFrom(p => p.Avgscroll))
                    .ForMember(dest => dest._id, src => src.MapFrom(p => p._id));

            CreateMap<Rootobject, DataTempmodel>()                    
                    .ForMember(dest => dest.View, src => src.ResolveUsing(p => p.view))
                    .ForMember(dest => dest.TotalVisitors, src => src.MapFrom(p => p.u))
                    .ForMember(dest => dest.NewVisitors, src => src.MapFrom(p => p.n))
                    .ForMember(dest => dest.TotalVisits, src => src.MapFrom(p => p.t))
                    .ForMember(dest => dest.Avgtime, src => src.MapFrom(p => p.dcalc))
                    .ForMember(dest => dest.Landings, src => src.ResolveUsing(p => p.s))
                    .ForMember(dest => dest.Exits, src => src.MapFrom(p => p.e))
                    .ForMember(dest => dest.Bounces, src => src.MapFrom(p => p.b))
                    .ForMember(dest => dest.Avgscroll, src => src.MapFrom(p => p.scrcalc))
                    .ForMember(dest => dest._id, src => src.MapFrom(p => p._id));

            CreateMap<RetrieveSessionCountlyAPIModel, DataTempmodel>()
                    .ForMember(dest => dest.Day, src => src.MapFrom(p => p._id))
                    .ForMember(dest => dest.TotalSessions, src => src.ResolveUsing(p => p.t))
                    .ForMember(dest => dest.NewSessions, src => src.MapFrom(p => p.n))
                    .ForMember(dest => dest.UniqueSessions, src => src.MapFrom(p => p.u));
            CreateMap<DataTempmodel, RetrieveSessionCountlyAPIModel>()
                    .ForMember(dest => dest._id, src => src.MapFrom(p => p.Day))
                    .ForMember(dest => dest.t, src => src.ResolveUsing(p => p.TotalSessions))
                    .ForMember(dest => dest.n, src => src.MapFrom(p => p.NewSessions))
                    .ForMember(dest => dest.u, src => src.MapFrom(p => p.UniqueSessions));

            CreateMap<AnalyticsDurationsCountlyAPIModel, DataTempmodel>()
                   .ForMember(dest => dest.TimeDurations, src => src.MapFrom(p => p.ds))
                   .ForMember(dest => dest.UsersDurations, src => src.ResolveUsing(p => p.t))
                   .ForMember(dest => dest.PercentageDurations, src => src.MapFrom(p => p.percent));
            CreateMap<DataTempmodel, AnalyticsDurationsCountlyAPIModel>()
                    .ForMember(dest => dest.t, src => src.MapFrom(p => p.UsersDurations))
                    .ForMember(dest => dest.ds, src => src.ResolveUsing(p => p.TimeDurations))
                    .ForMember(dest => dest.percent, src => src.MapFrom(p => p.PercentageDurations));

            CreateMap<AnalyticsFrequencyCountlyAPIModel, DataTempmodel>()
                   .ForMember(dest => dest.TimeFrequency, src => src.MapFrom(p => p.f))
                   .ForMember(dest => dest.UsersFrequency, src => src.ResolveUsing(p => p.t))
                   .ForMember(dest => dest.PercentageFrequency, src => src.MapFrom(p => p.percent));
            CreateMap<DataTempmodel, AnalyticsFrequencyCountlyAPIModel>()
                    .ForMember(dest => dest.t, src => src.MapFrom(p => p.UsersFrequency))
                    .ForMember(dest => dest.f, src => src.ResolveUsing(p => p.TimeFrequency))
                    .ForMember(dest => dest.percent, src => src.MapFrom(p => p.PercentageFrequency));

            CreateMap<AnalyticsPlatformCountlyAPIModel, DataTempmodel>()
               .ForMember(dest => dest.NamePlatforms, src => src.MapFrom(p => p.name))
               .ForMember(dest => dest.ValuePlatforms, src => src.ResolveUsing(p => p.value))
               .ForMember(dest => dest.PercentPlatforms, src => src.MapFrom(p => p.percent));
            CreateMap<DataTempmodel, AnalyticsPlatformCountlyAPIModel>()
                .ForMember(dest => dest.name, src => src.MapFrom(p => p.NamePlatforms))
                .ForMember(dest => dest.value, src => src.ResolveUsing(p => p.ValuePlatforms))
                .ForMember(dest => dest.percent, src => src.MapFrom(p => p.PercentPlatforms));

            CreateMap<ReportScreenMeasure, ReportDataColumn>()
             .ForMember(dest => dest.Name, src => src.ResolveUsing(p => p.MeasureName));
            CreateMap<ReportDataColumn, ReportScreenMeasure>()
             .ForMember(dest => dest.MeasureName, src => src.ResolveUsing(p => p.Name));

            CreateMap<ReportDataColumn, ReportScreenGroupProperty>()
            .ForMember(dest => dest.GroupPropertyName, src => src.ResolveUsing(p => p.Name));
            CreateMap<ReportScreenGroupProperty, ReportDataColumn>()
           .ForMember(dest => dest.Name, src => src.ResolveUsing(p => p.GroupPropertyName));

            CreateMap<Provincial, DataTempmodel>()
                   .ForMember(dest => dest.ProvincialCities, src => src.MapFrom(p => p.Name))
                   .ForMember(dest => dest.TotalCities, src => src.ResolveUsing(p => p.t))
                   .ForMember(dest => dest.NewCities, src => src.MapFrom(p => p.n))
                   .ForMember(dest => dest.UniqueCities, src => src.MapFrom(p => p.u));
            CreateMap<DataTempmodel, Provincial>()
                .ForMember(dest => dest.Name, src => src.MapFrom(p => p.ProvincialCities))
                .ForMember(dest => dest.n, src => src.ResolveUsing(p => p.TotalCities))
                .ForMember(dest => dest.t, src => src.MapFrom(p => p.NewCities))
                .ForMember(dest => dest.u, src => src.MapFrom(p => p.UniqueCities));
        }
    }

    public static class ReportMapper
    {
        static ReportMapper()
        {
            Mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<CommonMapperProfile>();
                cfg.AddProfile<ReportMapperProfile>();
            }).CreateMapper();
        }

        internal static IMapper Mapper { get; }

        public static List<ReportScreenRowModel> ListReportScreen(this List<MGSYS_ReportScreen> entities)
        {
            return Mapper.Map<List<ReportScreenRowModel>>(entities);
        }

        public static ReportScreen ReportScreenId(this MGSYS_ReportScreen entities)
        {
            return Mapper.Map<ReportScreen>(entities);
        }

        public static List<ReportScreenFilter> ListReportScreenFilter(this List<MGSYS_ReportScreenFilter> entities)
        {
            return Mapper.Map<List<ReportScreenFilter>>(entities);
        }

        public static List<ReportScreenFilterData> ListSYS_ReportScreenFilterData(this MGSYS_ReportScreenFilterData entities)
        {
            return Mapper.Map<List<ReportScreenFilterData>>(entities);
        }

        public static List<ReportScreenGroupProperty> ListReportScreenGroupProperty(this List<MGSYS_ReportScreenGroupProperty> entities)
        {
            return Mapper.Map<List<ReportScreenGroupProperty>>(entities);
        }

        public static List<ReportScreenMeasure> ListReportScreenMeasure(this List<MGSYS_ReportScreenMeasure> entities)
        {
            return Mapper.Map<List<ReportScreenMeasure>>(entities);
        }

        public static ReportScreen ReportScreenMap(this ReportScreen entities)
        {
            return Mapper.Map<ReportScreen>(entities);
        }

        public static List<DataTempmodel> DataTempmodelMap(this List<Rootobject> entities)
        {
            return Mapper.Map<List<DataTempmodel>>(entities);
        }

        public static List<ReportDataColumn> ReportDataColumnReportScreenMeasure(this List<ReportScreenMeasure> entities)
        {
            return Mapper.Map<List<ReportDataColumn>>(entities);
        }

        public static List<ReportDataColumn> ReportDataColumnReportScreenGroupProperty(this IOrderedEnumerable<ReportScreenGroupProperty> entities)
        {
            return Mapper.Map<List<ReportDataColumn>>(entities);
        }

        public static List<ReportDataColumn> ReportDataColumnReportScreenMeasureBy(this IOrderedEnumerable<ReportScreenMeasure> entities)
        {
            return Mapper.Map<List<ReportDataColumn>>(entities);
        }

        public static List<DataTempmodel> DataAnalyticsSession(this List<RetrieveSessionCountlyAPIModel> entities)
        {
            return Mapper.Map<List<DataTempmodel>>(entities);
        }

        public static List<DataTempmodel> DataAnalyticsDurations(this List<AnalyticsDurationsCountlyAPIModel> entities)
        {
            return Mapper.Map<List<DataTempmodel>>(entities);
        }

        public static List<DataTempmodel> DataAnalyticsFrequency(this List<AnalyticsFrequencyCountlyAPIModel> entities)
        {
            return Mapper.Map<List<DataTempmodel>>(entities);
        }

        public static List<DataTempmodel> DataAnalyticsPlatforms(this List<AnalyticsPlatformCountlyAPIModel> entities)
        {
            return Mapper.Map<List<DataTempmodel>>(entities);
        }

        public static List<DataTempmodel> DataAnalyticsProvincial(this List<Provincial> entities)
        {
            return Mapper.Map<List<DataTempmodel>>(entities);
        }
    }
}