﻿using AutoMapper;
using Haravan.Web.Api.BusinessObjects.Models;
using System.Collections.Generic;

namespace Haravan.Web.Api.BusinessObjects.Mappers
{
    public class DashboardProfile : Profile
    {
        public DashboardProfile()
        {
            CreateMap<RetrieveSessionCountlyModel, RetrieveSessionCountlyAPIModel>()
              .ForMember(dest => dest._id, src => src.MapFrom(p => p.Id))
              .ForMember(dest => dest.e, src => src.MapFrom(p => p.RequestsToSever))
              .ForMember(dest => dest.n, src => src.MapFrom(p => p.NewSessions))
              .ForMember(dest => dest.t, src => src.MapFrom(p => p.TotalSessions))
              .ForMember(dest => dest.u, src => src.MapFrom(p => p.UniqueLocations))
              .ForMember(dest => dest.d, src => src.MapFrom(p => p.TotalSessionDuration))
              .ForMember(dest => dest.ds, src => src.MapFrom(p => p.Durations))
              .ForMember(dest => dest.f, src => src.MapFrom(p => p.Frequency))
              .ForMember(dest => dest.l, src => src.MapFrom(p => p.Loyalty));

            CreateMap<RetrieveSessionCountlyAPIModel, RetrieveSessionCountlyModel>()
            .ForMember(dest => dest.Id, src => src.MapFrom(p => p._id))
            .ForMember(dest => dest.RequestsToSever, src => src.MapFrom(p => p.e))
            .ForMember(dest => dest.NewSessions, src => src.MapFrom(p => p.n))
            .ForMember(dest => dest.TotalSessions, src => src.MapFrom(p => p.t))
            .ForMember(dest => dest.UniqueLocations, src => src.MapFrom(p => p.u))
            .ForMember(dest => dest.TotalSessionDuration, src => src.MapFrom(p => p.d))
            .ForMember(dest => dest.Durations, src => src.MapFrom(p => p.ds))
            .ForMember(dest => dest.Frequency, src => src.MapFrom(p => p.f))
            .ForMember(dest => dest.Loyalty, src => src.MapFrom(p => p.l));

            CreateMap<AnalyticsSessionCountlyModel, AnalyticsSessionCountlyAPIModel>()
             .ForMember(dest => dest._id, src => src.MapFrom(p => p.Id))
             .ForMember(dest => dest.u, src => src.MapFrom(p => p.TotalUser))
             .ForMember(dest => dest.t, src => src.MapFrom(p => p.TotalSessions))
             .ForMember(dest => dest.n, src => src.MapFrom(p => p.NewUsers))
             .ForMember(dest => dest.d, src => src.MapFrom(p => p.TotalSessionDuration))
             .ForMember(dest => dest.e, src => src.MapFrom(p => p.TotalAPIRequests));

            CreateMap<AnalyticsSessionCountlyAPIModel, AnalyticsSessionCountlyModel>()
            .ForMember(dest => dest.Id, src => src.MapFrom(p => p._id))
            .ForMember(dest => dest.TotalUser, src => src.MapFrom(p => p.u))
            .ForMember(dest => dest.TotalSessions, src => src.MapFrom(p => p.t))
            .ForMember(dest => dest.NewUsers, src => src.MapFrom(p => p.n))
            .ForMember(dest => dest.TotalSessionDuration, src => src.MapFrom(p => p.d))
            .ForMember(dest => dest.TotalAPIRequests, src => src.MapFrom(p => p.e));

            CreateMap<AnalyticsDurationsCountlyAPIModel, AnalyticsDurationsModel>()
            .ForMember(dest => dest.TimeDurations, src => src.MapFrom(p => p.ds))
            .ForMember(dest => dest.UsersDurations, src => src.MapFrom(p => p.t))
            .ForMember(dest => dest.PercentageDurations, src => src.MapFrom(p => p.percent));

            CreateMap<AnalyticsDurationsModel, AnalyticsDurationsCountlyAPIModel>()
            .ForMember(dest => dest.ds, src => src.MapFrom(p => p.TimeDurations))
            .ForMember(dest => dest.t, src => src.MapFrom(p => p.UsersDurations))
            .ForMember(dest => dest.percent, src => src.MapFrom(p => p.PercentageDurations));

            CreateMap<AnalyticsDurationsModel, AnalyticsDurationsCountlyAPIModel>()
            .ForMember(dest => dest.ds, src => src.MapFrom(p => p.TimeDurations))
            .ForMember(dest => dest.t, src => src.MapFrom(p => p.UsersDurations))
            .ForMember(dest => dest.percent, src => src.MapFrom(p => p.PercentageDurations));

            CreateMap<AnalyticsDurationsModel, CharDataModel>()
            .ForMember(dest => dest.Id, src => src.MapFrom(p => p.TimeDurations))
            .ForMember(dest => dest.Data, src => src.MapFrom(p => p.UsersDurations));
            CreateMap<CharDataModel, AnalyticsDurationsModel>()
           .ForMember(dest => dest.TimeDurations, src => src.MapFrom(p => p.Id))
           .ForMember(dest => dest.UsersDurations, src => src.MapFrom(p => p.Data));

            CreateMap<AnalyticsFrequencyCountlyAPIModel, AnalyticsFrequencyModel>()
           .ForMember(dest => dest.TimeFrequency, src => src.MapFrom(p => p.f))
           .ForMember(dest => dest.UsersFrequency, src => src.MapFrom(p => p.t))
           .ForMember(dest => dest.PercentageFrequency, src => src.MapFrom(p => p.percent));
            CreateMap<AnalyticsFrequencyModel, AnalyticsFrequencyCountlyAPIModel>()
            .ForMember(dest => dest.f, src => src.MapFrom(p => p.TimeFrequency))
            .ForMember(dest => dest.t, src => src.MapFrom(p => p.UsersFrequency))
            .ForMember(dest => dest.percent, src => src.MapFrom(p => p.PercentageFrequency));

            CreateMap<AnalyticsFrequencyModel, CharDataModel>()
            .ForMember(dest => dest.Id, src => src.MapFrom(p => p.TimeFrequency))
            .ForMember(dest => dest.Data, src => src.MapFrom(p => p.UsersFrequency));
            CreateMap<CharDataModel, AnalyticsFrequencyModel>()
           .ForMember(dest => dest.TimeFrequency, src => src.MapFrom(p => p.Id))
           .ForMember(dest => dest.UsersFrequency, src => src.MapFrom(p => p.Data));

            CreateMap<RetrieveSessionCountlyModel, CharDataModel>()
            .ForMember(dest => dest.Id, src => src.MapFrom(p => p.Id))
            .ForMember(dest => dest.Data, src => src.MapFrom(p => p.TotalSessions));
            CreateMap<CharDataModel, RetrieveSessionCountlyModel>()
           .ForMember(dest => dest.TotalSessions, src => src.MapFrom(p => p.Id))
           .ForMember(dest => dest.Id, src => src.MapFrom(p => p.Data));

            CreateMap<ViewPageTableTotalVisitors, Rootobject>()
           .ForMember(dest => dest._id, src => src.MapFrom(p => p._id));
            CreateMap<Rootobject, ViewPageTableTotalVisitors>()
           .ForMember(dest => dest._id, src => src.MapFrom(p => p._id));

            CreateMap<ViewPageTableNewVisitors, Rootobject>()
          .ForMember(dest => dest._id, src => src.MapFrom(p => p._id));
            CreateMap<Rootobject, ViewPageTableNewVisitors>()
           .ForMember(dest => dest._id, src => src.MapFrom(p => p._id));

            CreateMap<ViewPageTableTotalVisits, Rootobject>()
          .ForMember(dest => dest._id, src => src.MapFrom(p => p._id));
            CreateMap<Rootobject, ViewPageTableTotalVisits>()
           .ForMember(dest => dest._id, src => src.MapFrom(p => p._id));

            CreateMap<ViewPageTableNewVisitors, CharDataModel>()
           .ForMember(dest => dest.Id, src => src.MapFrom(p => p.view))
           .ForMember(dest => dest.Data, src => src.MapFrom(p => p.n));
            CreateMap<CharDataModel, ViewPageTableNewVisitors>()
           .ForMember(dest => dest.view, src => src.MapFrom(p => p.Id))
           .ForMember(dest => dest.n, src => src.MapFrom(p => p.Data));

            CreateMap<ViewPageTableTotalVisitors, CharDataModel>()
           .ForMember(dest => dest.Id, src => src.MapFrom(p => p.view))
           .ForMember(dest => dest.Data, src => src.MapFrom(p => p.u));
            CreateMap<CharDataModel, ViewPageTableTotalVisitors>()
           .ForMember(dest => dest.view, src => src.MapFrom(p => p.Id))
           .ForMember(dest => dest.u, src => src.MapFrom(p => p.Data));

            CreateMap<ViewPageTableTotalVisits, CharDataModel>()
          .ForMember(dest => dest.Id, src => src.MapFrom(p => p.view))
          .ForMember(dest => dest.Data, src => src.MapFrom(p => p.t));
            CreateMap<CharDataModel, ViewPageTableTotalVisits>()
           .ForMember(dest => dest.view, src => src.MapFrom(p => p.Id))
           .ForMember(dest => dest.t, src => src.MapFrom(p => p.Data));

            CreateMap<AnalyticsSessionNewUsers, RetrieveSessionCountlyAPIModel>()
           .ForMember(dest => dest._id, src => src.MapFrom(p => p._id));
            CreateMap<RetrieveSessionCountlyAPIModel, AnalyticsSessionNewUsers>()
           .ForMember(dest => dest._id, src => src.MapFrom(p => p._id));
            CreateMap<AnalyticsSessionNewUsers, CharDataModel>()
            .ForMember(dest => dest.Id, src => src.MapFrom(p => p._id))
            .ForMember(dest => dest.Data, src => src.MapFrom(p => p.n));
            CreateMap<CharDataModel, AnalyticsSessionNewUsers>()
           .ForMember(dest => dest._id, src => src.MapFrom(p => p.Id))
           .ForMember(dest => dest.n, src => src.MapFrom(p => p.Data));

            CreateMap<AnalyticsSessionTotalSessions, RetrieveSessionCountlyAPIModel>()
           .ForMember(dest => dest._id, src => src.MapFrom(p => p._id));
            CreateMap<RetrieveSessionCountlyAPIModel, AnalyticsSessionTotalSessions>()
           .ForMember(dest => dest._id, src => src.MapFrom(p => p._id));
            CreateMap<AnalyticsSessionTotalSessions, CharDataModel>()
            .ForMember(dest => dest.Id, src => src.MapFrom(p => p._id))
            .ForMember(dest => dest.Data, src => src.MapFrom(p => p.t));
            CreateMap<CharDataModel, AnalyticsSessionTotalSessions>()
           .ForMember(dest => dest._id, src => src.MapFrom(p => p.Id))
           .ForMember(dest => dest.t, src => src.MapFrom(p => p.Data));

            CreateMap<AnalyticsSessionTotalUsers, RetrieveSessionCountlyAPIModel>()
          .ForMember(dest => dest._id, src => src.MapFrom(p => p._id));
            CreateMap<RetrieveSessionCountlyAPIModel, AnalyticsSessionTotalUsers>()
           .ForMember(dest => dest._id, src => src.MapFrom(p => p._id));
            CreateMap<AnalyticsSessionTotalUsers, CharDataModel>()
            .ForMember(dest => dest.Id, src => src.MapFrom(p => p._id))
            .ForMember(dest => dest.Data, src => src.MapFrom(p => p.u));
            CreateMap<CharDataModel, AnalyticsSessionTotalUsers>()
           .ForMember(dest => dest._id, src => src.MapFrom(p => p.Id))
           .ForMember(dest => dest.u, src => src.MapFrom(p => p.Data));

            CreateMap<AnalyticsPlatformCountlyAPIModel, CharDataModel>()
           .ForMember(dest => dest.Id, src => src.MapFrom(p => p.name))
           .ForMember(dest => dest.Data, src => src.MapFrom(p => p.value));
            CreateMap<CharDataModel, AnalyticsPlatformCountlyAPIModel>()
           .ForMember(dest => dest.name, src => src.MapFrom(p => p.Id))
           .ForMember(dest => dest.value, src => src.MapFrom(p => p.Data));

            CreateMap<Provincial, CharDataModel>()
            .ForMember(dest => dest.Id, src => src.MapFrom(p => p.Name))
            .ForMember(dest => dest.Data, src => src.MapFrom(p => p.t));
            CreateMap<CharDataModel, Provincial>()
           .ForMember(dest => dest.Name, src => src.MapFrom(p => p.Id))
           .ForMember(dest => dest.t, src => src.MapFrom(p => p.Data));
        }
    }

    public static class DashboardMapper
    {
        static DashboardMapper()
        {
            Mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<CommonMapperProfile>();
                cfg.AddProfile<DashboardProfile>();
            }).CreateMapper();
        }

        internal static IMapper Mapper { get; }

        public static List<AnalyticsSessionCountlyModel> ToListAnalyticsSession(this List<AnalyticsSessionCountlyAPIModel> entity)
        {
            return Mapper.Map<List<AnalyticsSessionCountlyModel>>(entity);
        }

        public static List<RetrieveSessionCountlyModel> TolistRetrieveSession(this List<RetrieveSessionCountlyAPIModel> entity)
        {
            return Mapper.Map<List<RetrieveSessionCountlyModel>>(entity);
        }

        public static List<AnalyticsDurationsModel> TolistAnalyticsDurationsModel(this List<AnalyticsDurationsCountlyAPIModel> entity)
        {
            return Mapper.Map<List<AnalyticsDurationsModel>>(entity);
        }

        public static List<CharDataModel> TolistAnalyticsDurationsCharModel(this List<AnalyticsDurationsModel> entity)
        {
            return Mapper.Map<List<CharDataModel>>(entity);
        }

        public static List<AnalyticsFrequencyModel> TolistAnalyticsFrequencyModel(this List<AnalyticsFrequencyCountlyAPIModel> entity)
        {
            return Mapper.Map<List<AnalyticsFrequencyModel>>(entity);
        }

        public static List<CharDataModel> TolistAnalyticsFrequencyCharModel(this List<AnalyticsFrequencyModel> entity)
        {
            return Mapper.Map<List<CharDataModel>>(entity);
        }

        public static List<CharDataModel> TolistAnalyticsSessionCharModel(this List<RetrieveSessionCountlyModel> entity)
        {
            return Mapper.Map<List<CharDataModel>>(entity);
        }

        public static List<CharDataModel> AnalyticsViewVisitPageCharNewVisitors(this List<ViewPageTableNewVisitors> entity)
        {
            return Mapper.Map<List<CharDataModel>>(entity);
        }

        public static List<CharDataModel> AnalyticsViewVisitPageCharTotalVisitors(this List<ViewPageTableTotalVisitors> entity)
        {
            return Mapper.Map<List<CharDataModel>>(entity);
        }

        public static List<CharDataModel> AnalyticsViewVisitPageCharTotalVisits(this List<ViewPageTableTotalVisits> entity)
        {
            return Mapper.Map<List<CharDataModel>>(entity);
        }

        public static List<ViewPageTableNewVisitors> NewVisitorsToRootobject(this List<Rootobject> entity)
        {
            return Mapper.Map<List<ViewPageTableNewVisitors>>(entity);
        }

        public static List<ViewPageTableTotalVisitors> TotalVisitorsToRootobject(this List<Rootobject> entity)
        {
            return Mapper.Map<List<ViewPageTableTotalVisitors>>(entity);
        }

        public static List<ViewPageTableTotalVisits> TotalVisitsToRootobject(this List<Rootobject> entity)
        {
            return Mapper.Map<List<ViewPageTableTotalVisits>>(entity);
        }

        public static List<AnalyticsSessionNewUsers> SessionNewUsersToSessionCountly(this List<RetrieveSessionCountlyAPIModel> entity)
        {
            return Mapper.Map<List<AnalyticsSessionNewUsers>>(entity);
        }

        public static List<CharDataModel> SessionNewUsersToCharData(this List<AnalyticsSessionNewUsers> entity)
        {
            return Mapper.Map<List<CharDataModel>>(entity);
        }

        public static List<AnalyticsSessionTotalSessions> SessionTotalSessionsToSessionCountly(this List<RetrieveSessionCountlyAPIModel> entity)
        {
            return Mapper.Map<List<AnalyticsSessionTotalSessions>>(entity);
        }

        public static List<CharDataModel> SessionTotalSessionsToCharData(this List<AnalyticsSessionTotalSessions> entity)
        {
            return Mapper.Map<List<CharDataModel>>(entity);
        }

        public static List<AnalyticsSessionTotalUsers> SessionTotalUsersToSessionCountly(this List<RetrieveSessionCountlyAPIModel> entity)
        {
            return Mapper.Map<List<AnalyticsSessionTotalUsers>>(entity);
        }

        public static List<CharDataModel> SessionTotalUsersToCharData(this List<AnalyticsSessionTotalUsers> entity)
        {
            return Mapper.Map<List<CharDataModel>>(entity);
        }

        public static List<CharDataModel> TolistAnalyticsPlatformCharModel(this List<AnalyticsPlatformCountlyAPIModel> entity)
        {
            return Mapper.Map<List<CharDataModel>>(entity);
        }

        public static List<CharDataModel> TolistAnalyticsCitiesTotalSessionsCharModel(this List<Provincial> entity)
        {
            return Mapper.Map<List<CharDataModel>>(entity);
        }
    }
}