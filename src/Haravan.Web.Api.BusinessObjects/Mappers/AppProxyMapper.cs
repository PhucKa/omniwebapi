﻿using AutoMapper;
using BHN.SharedObject.APIDataModel;
using Haravan.Web.Api.BusinessObjects.MongoModels;

namespace Haravan.Web.Api.BusinessObjects.Mappers
{
    public class AppProxyProfile : Profile
    {
        public AppProxyProfile()
        {
            CreateMap<MGAppProxy, AppProxyAPIModel>()
                .ForMember(m => m.id, k => k.MapFrom(a => a._id))
                .ForMember(m => m.sub_path_prefix, k => k.MapFrom(a => a.SubPathPrefixProxy))
                .ForMember(m => m.sub_path, k => k.MapFrom(a => a.SubPathProxy))
                .ForMember(m => m.proxy_url, k => k.MapFrom(a => a.ProxyURL));
        }
    }

    public static class AppProxyMapper
    {
        static AppProxyMapper()
        {
            Mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<CommonMapperProfile>();
                cfg.AddProfile<AppProxyProfile>();
            }).CreateMapper();
        }

        internal static IMapper Mapper { get; }

        public static AppProxyAPIModel ToApiModel(this MGAppProxy entity)
        {
            return Mapper.Map<AppProxyAPIModel>(entity);
        }
    }
}
