﻿using AutoMapper;
using Haravan.Web.Api.BusinessObjects.Models.Google;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System.Collections.Generic;

namespace Haravan.Web.Api.BusinessObjects.Mappers
{
    public class GoogleSiteVerificationProfile : Profile
    {
        public GoogleSiteVerificationProfile()
        {
            CreateMap<MGGoogleSiteVerificationModel, GoogleSiteVerificationDetailApiModel>()
                .ForMember(m => m.id, k => k.MapFrom(a => a._id)).ReverseMap();
        }
    }

    public static class GoogleSiteVerificationMapper
    {
        static GoogleSiteVerificationMapper()
        {
            Mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<CommonMapperProfile>();
                cfg.AddProfile<GoogleSiteVerificationProfile>();
            }).CreateMapper();
        }

        internal static IMapper Mapper { get; }

        public static MGGoogleSiteVerificationModel ToModel(this GoogleSiteVerificationDetailApiModel entity)
        {
            return Mapper.Map<MGGoogleSiteVerificationModel>(entity);
        }

        public static GoogleSiteVerificationDetailApiModel ToModel(this MGGoogleSiteVerificationModel entity)
        {
            return Mapper.Map<GoogleSiteVerificationDetailApiModel>(entity);
        }
        public static List<GoogleSiteVerificationDetailApiModel> ToListModel(this List<MGGoogleSiteVerificationModel> entity)
        {
            return Mapper.Map<List<GoogleSiteVerificationDetailApiModel>>(entity);
        }
    }
}