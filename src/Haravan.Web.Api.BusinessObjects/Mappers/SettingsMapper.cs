﻿using AutoMapper;
using Haravan.Web.Api.BusinessObjects.Models;

namespace Haravan.Web.Api.BusinessObjects.Mappers
{
    public class SettingsMapperProfile : Profile
    {
        public SettingsMapperProfile()
        {
            CreateMap<CheckoutSettingModel, CheckoutSettingModelWeb>()
                .ForMember(m => m.Id, k => k.MapFrom(a => a.Id));
            CreateMap<CheckoutSettingModelWeb, CheckoutSettingModel>()
                .ForMember(m => m.Id, k => k.MapFrom(a => a.Id));

            CreateMap<CheckoutSettingModel, CustomerAccountModel>()
                .ForMember(m => m.Id, k => k.MapFrom(a => a.Id))
                .ForMember(m => m.CheckoutAccountType, k => k.MapFrom(a => a.CheckoutAccountType));
            CreateMap<CustomerAccountModel, CheckoutSettingModel>()
                .ForMember(m => m.Id, k => k.MapFrom(a => a.Id))
                .ForMember(m => m.CheckoutAccountType, k => k.MapFrom(a => a.CheckoutAccountType));

            CreateMap<FieldSettingModel, CheckoutSettingModel>()
                .ForMember(m => m.Id, k => k.MapFrom(a => a.Id));
            CreateMap<CheckoutSettingModel, FieldSettingModel>()
                .ForMember(m => m.Id, k => k.MapFrom(a => a.Id));

            CreateMap<OrderAdditionalContentModel, CheckoutSettingModel>()
                .ForMember(m => m.Id, k => k.MapFrom(a => a.Id));
            CreateMap<CheckoutSettingModel, OrderAdditionalContentModel>()
                .ForMember(m => m.Id, k => k.MapFrom(a => a.Id));



        }
    }

    public static class SettingsMapper
    {
        static SettingsMapper()
        {
            Mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<CommonMapperProfile>();
                cfg.AddProfile<SettingsMapperProfile>();
            }).CreateMapper();
        }

        internal static IMapper Mapper { get; }

        public static CheckoutSettingModel ToModelCheckoutSettingModelWeb(this CheckoutSettingModelWeb entity)
        {
            var model = Mapper.Map<CheckoutSettingModel>(entity);
            return model;
        }
        public static CheckoutSettingModel ToModelCustomerAccount(this CustomerAccountModel entity)
        {
            var model = Mapper.Map<CheckoutSettingModel>(entity);
            return model;
        }
        public static CheckoutSettingModel ToModelFieldSettingModel(this FieldSettingModel entity)
        {
            var model = Mapper.Map<CheckoutSettingModel>(entity);
            return model;
        }
        public static CheckoutSettingModel ToModelOrderAdditionalContentModel(this OrderAdditionalContentModel entity)
        {
            var model = Mapper.Map<CheckoutSettingModel>(entity);
            return model;
        }

        
    }
}