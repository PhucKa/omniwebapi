﻿using AutoMapper;
using BHN.SharedObject.APIDataModel;
using BHN.SharedObject.EBSMessage;
using BHN.SharedObject.ESBuyerModel;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.BusinessObjects.Models.Common;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System;
using System.Collections.Generic;

namespace Haravan.Web.Api.BusinessObjects.Mappers
{
    public class PageMapperProfile : Profile
    {
        public PageMapperProfile()
        {
            CreateMap<MGPageModel, ESPageModel>()
                 .ForMember(m => m.Id, k => k.MapFrom(a => a._id))
                 .ForMember(m => m.CreatedDate, k => k.MapFrom(a => a.created_at))
                 .ForMember(m => m.UpdatedDate, k => k.MapFrom(a => a.updated_at))
                 .ForMember(m => m.AuthorName, k => k.MapFrom(a => a.authorname))
                 .ForMember(m => m.StoreId, k => k.MapFrom(a => a.storeid))
                 .ForMember(m => m.PublishedDate, k => k.MapFrom(a => a.published_at))
                 .ForMember(m => m.Content, k => k.MapFrom(a => a.content))
                 .ForMember(m => m.UrlHandle, k => k.MapFrom(a => a.handle))
                 .ReverseMap();

            CreateMap<ESPageModel, MGPageModel>()
                .ForMember(m => m._id, k => k.MapFrom(a => a.Id))
                .ForMember(m => m.created_at, k => k.MapFrom(a => a.CreatedDate))
                .ForMember(m => m.updated_at, k => k.MapFrom(a => a.UpdatedDate))
                .ForMember(m => m.authorname, k => k.MapFrom(a => a.AuthorName))
                .ForMember(m => m.storeid, k => k.MapFrom(a => a.StoreId))
                .ForMember(m => m.published_at, k => k.MapFrom(a => a.PublishedDate))
                .ForMember(m => m.content, k => k.MapFrom(a => a.Content))
                .ForMember(m => m.handle, k => k.MapFrom(a => a.UrlHandle))
                .ReverseMap();

            CreateMap<PageDetailModel, MGPageModel>()
               .ForMember(m => m._id, k => k.MapFrom(a => a.Id))
               .ForMember(m => m.handle, k => k.MapFrom(a => a.UrlHandle))
               .ForMember(m => m.meta_description, k => k.MapFrom(a => a.MetaDescription))
               .ForMember(m => m.published_at, k => k.MapFrom(a => a.PublishedDate))
               .ForMember(m => m.shortcontent, k => k.MapFrom(a => a.ShortContent))
               .ForMember(m => m.meta_title, k => k.MapFrom(a => a.PageTitle))
               .ForMember(m => m.template_suffix, k => k.MapFrom(a => a.TemplateName))
               .ForMember(m => m.title, k => k.MapFrom(a => a.Title))
               .ForMember(m => m.content, k => k.MapFrom(a => a.Content))
               .ForMember(m => m.IsVisible, k => k.MapFrom(a => a.IsVisible))
               .ForMember(m => m.authorname, k => k.MapFrom(a => a.AuthorName))
               .ForMember(m => m.created_at, k => k.MapFrom(a => a.CreatedDate))
               .ForMember(m => m.created_user, k => k.MapFrom(a => a.Created_user))
               .ForMember(m => m.updated_at, k => k.MapFrom(a => a.UpdatedDate))
               .ForMember(m => m.shop_id, k => k.MapFrom(a => a.StoreId));
            // .ReverseMap();

            CreateMap<MGPageModel, PageListModel>()
                .ForMember(m => m.id, k => k.MapFrom(a => a._id))
                .ForMember(m => m.IsVisible, k => k.MapFrom(a => a.IsVisible))
                .ForMember(m => m.storeid, k => k.MapFrom(a => a.storeid))
                .ForMember(m => m.authorname, k => k.MapFrom(a => a.authorname))
                .ForMember(m => m.content, k => k.MapFrom(a => a.content))
                .ForMember(m => m.created_at, k => k.MapFrom(a => a.created_at))
                .ForMember(m => m.IsVisible, k => k.MapFrom(a => a.IsVisible))
                .ForMember(m => m.template_suffix, k => k.MapFrom(a => a.template_suffix))
                .ReverseMap();

            // getdetail
            CreateMap<PageDetailModel, MGPageModel>()
               .ForMember(m => m._id, k => k.MapFrom(a => a.Id))
               .ForMember(m => m.handle, k => k.MapFrom(a => a.UrlHandle))
               .ForMember(m => m.meta_description, k => k.MapFrom(a => a.MetaDescription))
               .ForMember(m => m.published_at, k => k.MapFrom(a => a.PublishedDate))
               .ForMember(m => m.shortcontent, k => k.MapFrom(a => a.ShortContent))
               .ForMember(m => m.meta_title, k => k.MapFrom(a => a.PageTitle))
               .ForMember(m => m.template_suffix, k => k.MapFrom(p => p.TemplateName))
               .ForMember(m => m.created_at, k => k.MapFrom(a => a.CreatedDate))
               .ForMember(m => m.updated_at, k => k.MapFrom(a => a.UpdatedDate))
               .ForMember(m => m.title, k => k.MapFrom(a => a.Title))
               .ForMember(m => m.IsVisible, k => k.MapFrom(a => a.IsVisible))
               .ReverseMap();

            CreateMap<MGPageModel, PageDetailShortModel>()
               .ForMember(m => m.Id, k => k.MapFrom(a => a._id))
                  .ForMember(m => m.Title, k => k.MapFrom(a => a.title))
                  .ForMember(m => m.UpdatedDate, k => k.MapFrom(a => a.updated_at))
                  .ForMember(m => m.Content, k => k.MapFrom(a => a.content))
                  .ForMember(m => m.ShortContent, k => k.MapFrom(a => a.shortcontent))
                  .ForMember(m => m.IsVisible, k => k.MapFrom(a => a.IsVisible))
                  .ForMember(m => m.AuthorName, k => k.MapFrom(a => a.authorname))
                  .ForMember(m => m.TemplateName, k => k.MapFrom(a => a.authorname))
                  .ForMember(m => m.AuthorName, k => k.MapFrom(a => a.template_suffix))
                  .ForMember(m => m.UrlHandle, k => k.MapFrom(a => a.handle))
                  .ForMember(m => m.StoreId, k => k.MapFrom(a => a.storeid));

            CreateMap<PageDetail, PageDetailModel>()
               .ForMember(m => m.Id, k => k.MapFrom(a => a.Id))
               .ForMember(m => m.TemplateName, k => k.MapFrom(a => a.TemplateName))
               .ReverseMap();

            CreateMap<PageDetailModel, PageAPIModel>()
              .ForMember(m => m.id, k => k.MapFrom(a => a.Id))
              .ForMember(m => m.author, k => k.MapFrom(a => a.AuthorName))
              .ForMember(m => m.body_html, k => k.MapFrom(a => a.Content))
              .ForMember(m => m.created_at, k => k.MapFrom(a => a.CreatedDate))
              .ForMember(m => m.handle, k => k.MapFrom(a => a.UrlHandle))
              .ForMember(m => m.published_at, k => k.MapFrom(a => a.PublishedDate))
              .ForMember(m => m.shop_id, k => k.MapFrom(a => a.StoreId))
              .ForMember(m => m.template_suffix, k => k.MapFrom(a => a.TemplateName))
              .ForMember(m => m.title, k => k.MapFrom(a => a.Title))
              .ForMember(m => m.updated_at, k => k.MapFrom(a => a.UpdatedDate))
              .ForMember(m => m.published, k => k.MapFrom(a => a.IsSetPublishDate))
              .ReverseMap();

            CreateMap<PageListModel, PageAPIModel>()
                  .ForMember(dest => dest.author, src => src.MapFrom(p => p.author))
                  .ForMember(dest => dest.body_html, src => src.MapFrom(p => p.content))
                  .ForMember(dest => dest.handle, src => src.MapFrom(p => p.handle))
                  .ForMember(dest => dest.id, src => src.ResolveUsing(p => p.id))
                  .ForMember(dest => dest.published_at, src => src.MapFrom(p => p.published_at))
                  .ForMember(dest => dest.shop_id, src => src.MapFrom(p => p.storeid))
                  .ForMember(dest => dest.template_suffix, k => k.MapFrom(p => p.template_suffix))
                  .ForMember(dest => dest.title, src => src.MapFrom(p => p.title))
                  .ForMember(dest => dest.updated_at, src => src.MapFrom(p => p.updated_at))
                  .ForMember(dest => dest.created_at, src => src.MapFrom(p => p.created_at))
                  .ReverseMap();

            CreateMap<MGPageModel, PageAPIModel>()
                  .ForMember(dest => dest.author, src => src.MapFrom(p => p.authorname))
                  .ForMember(dest => dest.body_html, src => src.MapFrom(p => p.content))
                  .ForMember(dest => dest.handle, src => src.MapFrom(p => p.handle))
                  .ForMember(dest => dest.id, src => src.ResolveUsing(p => p._id))
                  .ForMember(dest => dest.published_at, src => src.MapFrom(p => p.published_at))
                  .ForMember(dest => dest.shop_id, src => src.MapFrom(p => p.storeid))
                  .ForMember(dest => dest.template_suffix, k => k.MapFrom(p => p.template_suffix))
                  .ForMember(dest => dest.title, src => src.MapFrom(p => p.title))
                  .ForMember(dest => dest.updated_at, src => src.MapFrom(p => p.updated_at))
                  .ForMember(dest => dest.created_at, src => src.MapFrom(p => p.created_at))
                  .ReverseMap();

            CreateMap<PageMsg, PageAPIModel>()
              .ForMember(dest => dest.author, src => src.MapFrom(p => p.AuthorName))
              .ForMember(dest => dest.body_html, src => src.MapFrom(p => p.Content))
              .ForMember(dest => dest.created_at, src => src.MapFrom(p => p.CreatedDate))
              .ForMember(dest => dest.handle, src => src.MapFrom(p => p.UrlHandle))
              .ForMember(dest => dest.id, src => src.MapFrom(p => p.Id))
              .ForMember(dest => dest.published_at, src => src.MapFrom(p => p.PublishedDate))
              .ForMember(dest => dest.shop_id, src => src.MapFrom(p => p.StoreId))
              .ForMember(dest => dest.template_suffix, k => k.MapFrom(p => p.TemplateName))
              .ForMember(dest => dest.title, src => src.MapFrom(p => p.Title))
              .ForMember(dest => dest.updated_at, src => src.MapFrom(p => p.UpdatedDate));
            CreateMap<MGPageModel, PageMsg>()
                .ForMember(dest => dest.Id, src => src.MapFrom(p => p._id))
                .ForMember(dest => dest.StoreId, src => src.MapFrom(p => p.storeid))
                .ForMember(dest => dest.AuthorName, src => src.MapFrom(p => p.authorname))
                .ForMember(dest => dest.Content, src => src.MapFrom(p => p.content))
                .ForMember(dest => dest.CreatedDate, src => src.MapFrom(p => p.created_at))
                .ForMember(dest => dest.CreatedUser, src => src.MapFrom(p => p.created_user))
                .ForMember(dest => dest.UrlHandle, src => src.MapFrom(p => p.handle))
                .ForMember(dest => dest.PublishedDate, src => src.MapFrom(p => p.published_at))
                .ForMember(dest => dest.Title, src => src.MapFrom(p => p.title))
                .ForMember(dest => dest.ShortContent, src => src.MapFrom(p => p.shortcontent))
                .ForMember(dest => dest.UpdatedDate, src => src.MapFrom(p => p.updated_at))
                .ForMember(dest => dest.UpdatedUser, src => src.MapFrom(p => p.updated_user))
                .ForMember(dest => dest.TemplateName, k => k.MapFrom(p => p.template_suffix));

            CreateMap<PageDetailShortModel, DropdownlistModel>()
                   .ForMember(dest => dest.Id, src => src.MapFrom(k => k.Id))
                   .ForMember(dest => dest.Name, src => src.MapFrom(k => k.Title));

            CreateMap<MGPageModel, PageDetailModel>()
                .ForMember(m => m.Id, k => k.MapFrom(a => a._id))
                .ForMember(m => m.PageTitle, k => k.MapFrom(a => a.meta_title))
                .ForMember(m => m.MetaDescription, k => k.MapFrom(a => a.meta_description))
                .ForMember(m => m.UrlHandle, k => k.ResolveUsing(p => p.handle))
                .ForMember(dest => dest.TemplateName, k => k.MapFrom(p => p.template_suffix))
                .ForMember(m => m.PublishedDate, k => k.MapFrom(a => a.published_at))
                .ForMember(m => m.AuthorName, k => k.MapFrom(a => a.authorname));

            CreateMap<MGPageModel, ThemeDropdownModel>()
                .ForMember(m => m.Id, k => k.MapFrom(a => a._id))
                .ForMember(m => m.Name, k => k.MapFrom(a => a.title));

            CreateMap<PageDetailShortModel, SimpleListModel>()
                    .ForMember(a => a.Key, m => m.MapFrom(b => b.Id))
                    .ForMember(a => a.Value, m => m.MapFrom(b => b.UrlHandle))
                    .ForMember(a => a.Name, m => m.MapFrom(b => b.Title));
        }
    }

    public static class PageMapper
    {
        static PageMapper()
        {
            Mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<CommonMapperProfile>();
                cfg.AddProfile<PageMapperProfile>();
            }).CreateMapper();
        }

        internal static IMapper Mapper { get; }

        public static ESPageModel ToModelES(this MGPageModel entity)
        {
            var model = Mapper.Map<ESPageModel>(entity);

            return model;
        }

        public static PageListModel ToModelList(this MGPageModel entity)
        {
            var model = Mapper.Map<PageListModel>(entity);

            return model;
        }

        public static List<PageDetailShortModel> MGPageToPageDetailShortList(this List<MGPageModel> entity)
        {
            var model = Mapper.Map<List<PageDetailShortModel>>(entity);

            return model;
        }

        public static PageDetailModel MGPageToPageDetailModel(this MGPageModel entity)
        {
            var model = Mapper.Map<PageDetailModel>(entity);
            model.IsVisible = entity.published_at != null && entity.published_at.Value.ToUniversalTime() <= DateTime.UtcNow ? true : false;
            return model;
        }

        public static List<PageDetailModel> MGPageListToPageDetailModelList(this List<MGPageModel> entity)
        {
            var ret = new List<PageDetailModel>();
            foreach (var item in entity)
            {
                var xxx = item.MGPageToPageDetailModel();
                ret.Add(item.MGPageToPageDetailModel());
            }
            return ret;
        }

        public static PageDetail ToModel(this PageDetailModel entity)
        {
            var model = Mapper.Map<PageDetail>(entity);

            return model;
        }

        public static List<ThemeDropdownModel> ToDropdownList(this List<MGPageModel> entities)
        {
            return Mapper.Map<List<ThemeDropdownModel>>(entities);
        }

        public static PageAPIModel ToAPIModel(this MGPageModel entity)
        {
            return Mapper.Map<PageAPIModel>(entity);
        }

        public static MGPageModel ToModelMG(this PageDetailModel entity)
        {
            var model = Mapper.Map<MGPageModel>(entity);

            return model;
        }

        public static List<PageListModel> ToModelList(this List<MGPageModel> entity)
        {
            var ret = new List<PageListModel>();
            foreach (var item in entity)
            {
                ret.Add(item.ToModelList());
            }

            return ret;
        }

        public static List<PageAPIModel> PageListToAPIList(this List<PageListModel> entity)
        {
            return Mapper.Map<List<PageAPIModel>>(entity);
        }

        public static PageAPIModel PageMsgToPageAPI(this PageMsg entity)
        {
            return Mapper.Map<PageAPIModel>(entity);
        }

        public static PageMsg MGPageToPageMSG(this MGPageModel entity)
        {
            return Mapper.Map<PageMsg>(entity);
        }

        public static List<PageAPIModel> PageDetailListToPageAPIList(this List<PageDetailModel> entity)
        {
            return Mapper.Map<List<PageAPIModel>>(entity);
        }

        public static List<PageAPIModel> MGPageListToPageAPIList(this List<MGPageModel> entity)
        {
            return Mapper.Map<List<PageAPIModel>>(entity);
        }

        public static List<DropdownlistModel> DropdownToListPage(this List<PageDetailShortModel> entity)
        {
            return Mapper.Map<List<DropdownlistModel>>(entity);
        }

        public static PageDetailModel PageDetailToPageModel(this MGPageModel entity)
        {
            var model = Mapper.Map<PageDetailModel>(entity);
            return model;
        }

        public static List<PageDetailModel> ListPageDetailToListPageModel(this List<MGPageModel> entity)
        {
            var model = Mapper.Map<List<PageDetailModel>>(entity);
            return model;
        }

        public static List<SimpleListModel> SimpleListModelToPageDetailShortModel(this List<PageDetailShortModel> entity)
        {
            var model = Mapper.Map<List<SimpleListModel>>(entity);
            return model;
        }
    }
}