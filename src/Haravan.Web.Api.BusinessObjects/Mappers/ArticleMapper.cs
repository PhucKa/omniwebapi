﻿using AutoMapper;
using BHN.SharedObject.APIDataModel;
using BHN.SharedObject.EBSMessage;
using BHN.SharedObject.ESBuyerModel;
using Haravan.Web.Api.BusinessObjects.Models.Article;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System;
using System.Collections.Generic;

namespace Haravan.Web.Api.BusinessObjects.Mappers
{
    public class ArticleMapperProfile : Profile
    {
        public ArticleMapperProfile()
        {
            CreateMap<MGArticleModel, ArticleSearchRowModel>()
                .ForMember(m => m.Id, k => k.MapFrom(a => a._id))
                 .ForMember(m => m.AuthorId, k => k.MapFrom(a => a.authorid))
                 .ForMember(m => m.AuthorName, k => k.MapFrom(a => a.author))
                 .ForMember(m => m.UpdatedDate, k => k.MapFrom(a => a.updated_at))
                 .ForMember(m => m.ImageUrl, k => k.ResolveUsing(p =>
                  {
                      if (p.image != null)
                      {
                          return p.image.src;
                      }
                      return null;
                  }))
                 .ReverseMap();

            CreateMap<ArticleDetail, ArticleDetailModel>()
                   .ForMember(m => m.Id, k => k.MapFrom(a => a.Id))
                   .ForMember(m => m.TemplateName, k => k.MapFrom(a => a.TemplateName))
                   .ReverseMap();

            CreateMap<ESArticleModel, MGArticleModel>()
                .ForMember(m => m._id, k => k.MapFrom(a => a.Id))
                .ForMember(m => m.published_at, k => k.MapFrom(a => a.PublishedDate))
                .ForMember(m => m.created_at, k => k.MapFrom(a => a.CreatedDate))
                .ForMember(m => m.created_user, k => k.MapFrom(a => a.CreatedUser))
                .ForMember(m => m.updated_user, k => k.MapFrom(a => a.UpdatedUser))
                .ForMember(m => m.updated_at, k => k.MapFrom(a => a.UpdatedDate))
                .ReverseMap();

            //insert update
            CreateMap<MGArticleModel, ArticleDetailModel>()
                 .ForMember(m => m.Id, k => k.MapFrom(a => a._id))
                 .ForMember(m => m.AuthorId, k => k.MapFrom(a => a.authorid))
                 .ForMember(m => m.AuthorName, k => k.MapFrom(a => a.author))
                 .ForMember(m => m.CreatedDate, k => k.MapFrom(a => a.created_at))
                 .ForMember(m => m.UpdatedDate, k => k.MapFrom(a => a.updated_at))
                 .ForMember(m => m.UrlHandle, k => k.MapFrom(a => a.url_handle))
                 .ForMember(m => m.TemplateName, k => k.MapFrom(a => a.template_suffix))
                 .ForMember(m => m.OldUrlHandle, k => k.MapFrom(a => a.url_handle))
                  .ForMember(m => m.MetaDescription, k => k.MapFrom(a => a.meta_description))
                  .ForMember(m => m.PageTitle, k => k.MapFrom(a => a.page_title))
                 .ForMember(m => m.PublishedDate, k => k.MapFrom(a => a.published_at))
                 .ForMember(m => m.ImageId, k => k.ResolveUsing(p => p.image != null ? p.image._id : 0))
                  .ForMember(m => m.Tags, k => k.ResolveUsing(p => p.tags != null ? string.Join(",", p.tags) : null))
                 .ForMember(m => m.ImageUrl, k => k.ResolveUsing(p => p.image != null ? p.image.src : null));
            //.ReverseMap();

            // getdetail
            CreateMap<ArticleDetailModel, MGArticleModel>()
                 .ForMember(m => m._id, k => k.MapFrom(a => a.Id))
                 .ForMember(m => m.authorid, k => k.MapFrom(a => a.AuthorId))
                 .ForMember(m => m.author, k => k.MapFrom(a => a.AuthorName))
                 .ForMember(m => m.created_at, k => k.MapFrom(a => a.CreatedDate))
                 .ForMember(m => m.updated_at, k => k.MapFrom(a => a.UpdatedDate))
                 .ForMember(m => m.url_handle, k => k.MapFrom(a => a.UrlHandle))
                 .ForMember(m => m.template_suffix, k => k.MapFrom(p => p.TemplateName))
                 .ForMember(m => m.published_at, k => k.MapFrom(a => a.PublishedDate))
                 .ForMember(m => m.meta_description, k => k.MapFrom(a => a.MetaDescription))
                 .ForMember(m => m.page_title, k => k.MapFrom(a => a.PageTitle))
                 .ForMember(m => m.image, k => k.ResolveUsing(a =>
                 {
                     if (!string.IsNullOrEmpty(a.ImageUrl))
                     {
                         var rs = new MGArticleImageModel
                         {
                             src = a.ImageUrl,
                             alt = string.Empty,
                             _id = a.ImageId.HasValue ? a.ImageId.Value : 0
                         };
                         return rs;
                     }
                     return null;
                 }))
                 .ForMember(m => m.tags, k => k.MapFrom(a => a.Tags));
            //.ReverseMap();

            #region InternalMap

            CreateMap<ArticleListModelInternal, MGArticleModel>()
                .ForMember(m => m._id, k => k.MapFrom(a => a.Id))
                 .ForMember(m => m.authorid, k => k.MapFrom(a => a.AuthorId))
                 .ForMember(m => m.author, k => k.MapFrom(a => a.AuthorName))
                 .ForMember(m => m.published_at, k => k.MapFrom(a => a.PublishedDate))
                 .ForMember(m => m.blogid, k => k.MapFrom(a => a.BlogId))
                 .ForMember(m => m.blogtitle, k => k.MapFrom(a => a.BlogTitle))
                 .ForMember(m => m.updated_at, k => k.MapFrom(a => a.UpdatedDate))

                 .ReverseMap();

            CreateMap<ArticleDetailModel, ArticleDetailModelInternal>();
            CreateMap<ArticleDetailModel, ArticleDetailModelInternal>()
               .ForMember(m => m.Id, k => k.MapFrom(a => a.Id))
               .ForMember(m => m.PublishedDate, k => k.MapFrom(a => a.PublishedDate))
               .ForMember(m => m.AuthorName, k => k.MapFrom(a => a.AuthorName))
               .ForMember(m => m.BlogTitle, k => k.MapFrom(a => a.BlogTitle))
               .ForMember(m => m.PageTitle, k => k.MapFrom(a => a.PageTitle))
               .ForMember(m => m.TemplateName, k => k.MapFrom(a => a.TemplateName))
               .ReverseMap();

            #endregion InternalMap

            #region API

            CreateMap<ArticleAPIModel, MGArticleModel>()
              .ForMember(m => m._id, k => k.MapFrom(a => a.id))
               .ForMember(m => m.author, k => k.MapFrom(a => a.author))
               .ForMember(m => m.url_handle, k => k.MapFrom(a => a.handle))
               .ForMember(m => m.authorid, k => k.MapFrom(a => a.user_id))
               .ForMember(m => m.blogid, k => k.MapFrom(a => a.blog_id))
               .ForMember(m => m.content, k => k.MapFrom(a => a.body_html))
               .ForMember(m => m.excerpt, k => k.MapFrom(a => a.summary_html))
                .ForMember(m => m.tags, k => k.MapFrom(a => a.tags))
                .ForMember(m => m.template_suffix, k => k.MapFrom(a => a.template_suffix))

               .ReverseMap();

            CreateMap<ArticleMsg, MGArticleModel>()
              .ForMember(m => m._id, k => k.MapFrom(a => a.Id))
              .ForMember(m => m.published_at, k => k.MapFrom(a => a.PublishedDate))
               .ForMember(m => m.author, k => k.MapFrom(a => a.AuthorName))
              .ForMember(m => m.authorid, k => k.MapFrom(a => a.AuthorId))
              .ForMember(m => m.template_suffix, k => k.MapFrom(a => a.TemplateName))
              .ForMember(m => m.updated_user, k => k.MapFrom(a => a.UpdatedUser))
              .ForMember(m => m.updated_at, k => k.MapFrom(a => a.UpdatedDate))
              .ForMember(m => m.created_at, k => k.MapFrom(a => a.CreatedDate))
              .ForMember(m => m.created_user, k => k.MapFrom(a => a.CreatedUser))
              .ForMember(m => m.url_handle, k => k.MapFrom(a => a.UrlHandle))
              .ForMember(m => m.meta_description, k => k.MapFrom(a => a.MetaDescription))
              .ForMember(m => m.page_title, k => k.MapFrom(a => a.PageTitle))
              .ForMember(m => m.url_handle, k => k.MapFrom(a => a.UrlHandle))
               .ForMember(m => m.template_suffix, k => k.MapFrom(a => a.TemplateName))

              .ReverseMap();

            CreateMap<ArticleAPIModel, ArticleMsg>()
              .ForMember(m => m.Id, k => k.MapFrom(a => a.id))
              .ForMember(m => m.PublishedDate, k => k.MapFrom(a => a.published_at))
              .ForMember(m => m.AuthorName, k => k.MapFrom(a => a.author))
              .ForMember(m => m.BlogId, k => k.MapFrom(a => a.blog_id))
              .ForMember(m => m.TemplateName, k => k.MapFrom(a => a.template_suffix))
              .ForMember(m => m.CreatedUser, k => k.MapFrom(a => a.user_id))
              .ForMember(m => m.Content, k => k.MapFrom(a => a.body_html))
              .ForMember(m => m.Excerpt, k => k.MapFrom(a => a.summary_html))
              .ForMember(m => m.UpdatedDate, k => k.MapFrom(a => a.updated_at))
              .ForMember(m => m.MetaDescription, k => k.MapFrom(a => a.meta_description))
              .ForMember(m => m.PageTitle, k => k.MapFrom(a => a.page_title))
              .ForMember(m => m.UrlHandle, k => k.MapFrom(a => a.handle))
              .ForMember(m => m.TemplateName, k => k.MapFrom(a => a.template_suffix))
              .ReverseMap();

            CreateMap<ArticleMsg, ArticleAPIModel>()
            .ForMember(dest => dest.author, src => src.MapFrom(p => p.AuthorName))
                 .ForMember(dest => dest.handle, src => src.MapFrom(p => p.UrlHandle))
                 .ForMember(dest => dest.blog_id, src => src.MapFrom(p => p.BlogId))
                 .ForMember(dest => dest.body_html, src => src.MapFrom(p => p.Content))
                 .ForMember(dest => dest.created_at, src => src.MapFrom(p => p.CreatedDate))
                 .ForMember(dest => dest.id, src => src.MapFrom(p => p.Id))
                 .ForMember(dest => dest.published, src => src.ResolveUsing(p => p.PublishedDate >= DateTime.UtcNow))
                 .ForMember(dest => dest.published_at, src => src.MapFrom(p => p.PublishedDate))
                 .ForMember(dest => dest.summary_html, src => src.MapFrom(p => p.Excerpt))
                 .ForMember(m => m.tags, o => o.ResolveUsing(p =>
                 {
                     string value = null;
                     if (p.Tags != null && p.Tags.Count > 0)
                         value = String.Join(", ", p.Tags.ToArray());
                     return value;
                 }))
                 .ForMember(dest => dest.template_suffix, src => src.ResolveUsing(p =>
                 {
                     if (string.IsNullOrWhiteSpace(p.TemplateName)) return null;
                     if (p.TemplateName.Contains(".")) return p.TemplateName;
                     return null;
                 }))
                 .ForMember(dest => dest.title, src => src.MapFrom(p => p.Title))
                 .ForMember(dest => dest.updated_at, src => src.MapFrom(p => p.UpdatedDate))
                 .ForMember(dest => dest.user_id, src => src.MapFrom(p => p.AuthorId))
                 .ForMember(m => m.image, o => o.ResolveUsing(p =>
                 {
                     if (!string.IsNullOrEmpty(p.ImageUrl))
                     {
                         var rs = new ArticleImageAPIModel();
                         rs.src = p.ImageUrl;
                         rs.attachment = p.ImageAttactment;
                         return rs;
                     }
                     return null;
                 }))
                  .ForMember(dest => dest.template_suffix, src => src.MapFrom(p => p.TemplateName))

             .ReverseMap();

            CreateMap<ArticleDetailModel, ArticleAPIModel>()
             .ForMember(m => m.id, k => k.MapFrom(a => a.Id))
             .ForMember(m => m.published_at, k => k.MapFrom(a => a.PublishedDate))
             .ForMember(m => m.author, k => k.MapFrom(a => a.AuthorName))
             .ForMember(m => m.blog_id, k => k.MapFrom(a => a.BlogId))
              .ForMember(dest => dest.template_suffix, src => src.ResolveUsing(p =>
              {
                  if (string.IsNullOrWhiteSpace(p.TemplateName)) return null;
                  if (p.TemplateName.Contains(".")) return p.TemplateName;
                  return null;
              }))
             .ForMember(m => m.user_id, k => k.MapFrom(a => a.AuthorId))
             .ForMember(m => m.body_html, k => k.MapFrom(a => a.Content))
             .ForMember(m => m.summary_html, k => k.MapFrom(a => a.Excerpt))
             .ForMember(m => m.updated_at, k => k.MapFrom(a => a.UpdatedDate))
             .ForMember(m => m.title, k => k.MapFrom(a => a.Title))
             .ForMember(m => m.page_title, k => k.MapFrom(a => a.PageTitle))
             .ForMember(m => m.created_at, k => k.MapFrom(a => a.CreatedDate))
             .ForMember(m => m.meta_description, k => k.MapFrom(a => a.MetaDescription))
             .ForMember(m => m.tags, k => k.MapFrom(a => a.Tags))
             .ForMember(m => m.published, k => k.MapFrom(a => a.IsVisible))
             .ForMember(m => m.published_at, k => k.MapFrom(a => a.PublishedDate))
             .ForMember(m => m.image, k => k.ResolveUsing(p =>
             {
                 if (!string.IsNullOrEmpty(p.ImageUrl) && p.ImageId != null)
                 {
                     var rs = new ArticleImageAPIModel();
                     rs.src = p.ImageUrl;
                     rs.attachment = p.ImageAttactment;
                     return rs;
                 }
                 return null;
             }))
             .ForMember(m => m.handle, k => k.MapFrom(a => a.UrlHandle));
            #endregion API
        }
    }

    public static class ArticleMapper
    {
        static ArticleMapper()
        {
            Mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<CommonMapperProfile>();
                cfg.AddProfile<ArticleMapperProfile>();
            }).CreateMapper();
        }

        internal static IMapper Mapper { get; }

        public static List<ArticleSearchRowModel> ArticleToModelList(this List<MGArticleModel> entity)
        {
            return Mapper.Map<List<ArticleSearchRowModel>>(entity);
        }

        public static MGArticleModel ArticleToToModelMG(this ArticleDetailModel entity)
        {
            var model = Mapper.Map<MGArticleModel>(entity);

            return model;
        }

        public static ArticleDetailModel ArticleToModelDetail(this MGArticleModel entity)
        {
            var model = Mapper.Map<ArticleDetailModel>(entity);
            return model;
        }

        public static ESArticleModel ArticleToModelES(this MGArticleModel entity)
        {
            var model = Mapper.Map<ESArticleModel>(entity);

            return model;
        }

        public static ArticleDetail ArticleTToModel(this ArticleDetailModel entity)
        {
            var model = Mapper.Map<ArticleDetail>(entity);

            return model;
        }

        #region Internal

        public static List<ArticleListModelInternal> MGArticleToArticleSearchList(this List<MGArticleModel> entity)
        {
            return Mapper.Map<List<ArticleListModelInternal>>(entity);
        }

        public static ArticleDetailModelInternal ArticleDetaiToModelInternal(this ArticleDetailModel entity)
        {
            var model = Mapper.Map<ArticleDetailModelInternal>(entity);
            return model;
        }

        public static ArticleDetailModel ModelInternalToArticleDetaiT(this ArticleDetailModelInternal entity)
        {
            var model = Mapper.Map<ArticleDetailModel>(entity);
            return model;
        }

        #endregion Internal

        #region Api

        public static List<ArticleAPIModel> ArticleAPIToModelList(this List<MGArticleModel> entity)
        {
            return Mapper.Map<List<ArticleAPIModel>>(entity);
        }

        public static ArticleMsg ArticleMsgToModelDetail(this MGArticleModel entity)
        {
            var model = Mapper.Map<ArticleMsg>(entity);
            return model;
        }

        public static ArticleAPIModel ArticleAPIToArticleMsg(this ArticleMsg entity)
        {
            var model = Mapper.Map<ArticleAPIModel>(entity);
            return model;
        }
        public static ArticleAPIModel DetailToApi(this ArticleDetailModel entity)
        {
            var model = Mapper.Map<ArticleAPIModel>(entity);
            return model;
        }
        public static List<ArticleAPIModel> ToApiList(this List<ArticleDetailModel> entities)
        {
            return Mapper.Map<List<ArticleAPIModel>>(entities);
        }
        #endregion Api
    }
}