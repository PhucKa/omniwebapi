﻿using AutoMapper;
using MongoDB.Bson;

namespace Haravan.Web.Api.BusinessObjects.Mappers
{
    public class CommonMapperProfile : Profile
    {
        public CommonMapperProfile()
        {
            CreateMap<ObjectId?, string>()
                .ConvertUsing(a => a.HasValue ? a.ToString() : null);
            CreateMap<string, ObjectId?>()
                .ConvertUsing(a => string.IsNullOrEmpty(a) ? null as ObjectId? : ObjectId.Parse(a));

            CreateMap<ObjectId, string>()
                .ConvertUsing(a => a != null ? a.ToString() : null);
            CreateMap<string, ObjectId>()
                .ConvertUsing(a => string.IsNullOrEmpty(a) ? ObjectId.Empty : ObjectId.Parse(a));
        }
    }
}
