﻿using AutoMapper;
using BHN.SharedObject.APIDataModel;
using BHN.SharedObject.EBSMessage;
using BHN.SharedObject.ESBuyerModel;
using Haravan.Web.Api.BusinessObjects.Models.Comment;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using Haravan.Web.Api.Utils.Extensions;
using System.Collections.Generic;
using System.Net;

namespace Haravan.Web.Api.BusinessObjects.Mappers
{
    public class CommentMapperProfile : Profile
    {
        public CommentMapperProfile()
        {
            CreateMap<CommentListModel, MGCommentModel>()
                 .ForMember(m => m._id, k => k.MapFrom(a => a.Id))
                 .ReverseMap();

            CreateMap<ESCommentModel, MGCommentModel>()
                .ForMember(m => m._id, k => k.MapFrom(a => a.Id))
                .ForMember(m => m.status, k => k.MapFrom(a => a.Status))
                .ForMember(m => m.isdeleted, k => k.MapFrom(a => a.IsDeleted))
                .ReverseMap();

            CreateMap<MGCommentModel, CommentListModel>()
                .ForMember(m => m.Id, k => k.MapFrom(a => a._id))
                .ForMember(m => m.Content, src => src.ResolveUsing(k => { return WebUtility.HtmlDecode(k.content); }))
                .ForMember(dest => dest.StatusName, src => src.ResolveUsing(p =>
                {
                    return ResourceHelper.DisplayForEnumValue((CommentStatus)p.status);
                }))
                .ReverseMap();
            CreateMap<CommentListModel, CommentAPIModel>()
               .ForMember(dest => dest.status, src => src.ResolveUsing(p =>
               {
                   if (p.Isdeleted)
                   {
                       return "removed";
                   }

                   switch (p.Status)
                   {
                       case (int)CommentStatus.Spam:
                           return "spam";

                       case (int)CommentStatus.Approved:
                           return "published";

                       default:
                           return "unapproved";
                   }
               }))
               .ForMember(dest => dest.article_id, src => src.MapFrom(p => p.Articleid))
               .ForMember(dest => dest.author, src => src.MapFrom(p => p.Author))
               .ForMember(dest => dest.blog_id, src => src.MapFrom(p => p.BlogId))
               .ForMember(dest => dest.body, src => src.ResolveUsing(p => StringHelper.StripTagsCharArray(p.Content)))
               .ForMember(dest => dest.body_html, src => src.MapFrom(p => p.Content))
               .ForMember(dest => dest.created_at, src => src.MapFrom(p => p.Created_at))
               .ForMember(dest => dest.email, src => src.MapFrom(p => p.Email))
               .ForMember(dest => dest.user_agent, src => src.MapFrom(p => p.User_agent))
               //.ForMember(dest => dest.ip, src => src.MapFrom(p => p))
               .ForMember(dest => dest.id, src => src.MapFrom(p => p.Id))
               .ForMember(dest => dest.published_at, src => src.MapFrom(p => p.Created_at))
               .ForMember(dest => dest.updated_at, src => src.MapFrom(p => p.Update_at))
               .ReverseMap();

          
            CreateMap<MGCommentModel, CommentAPIModel>()
                    .ForMember(dest => dest.status, src => src.ResolveUsing(p =>
                    {
                        if (p.isdeleted)
                        {
                            return "removed";
                        }

                        switch (p.status)
                        {
                            case (int)CommentStatus.Spam:
                                return "spam";

                            case (int)CommentStatus.Approved:
                                return "published";

                            default:
                                return "unapproved";
                        }
                    }))
                     .ForMember(dest => dest.article_id, src => src.MapFrom(p => p.articleid))
                     .ForMember(dest => dest.author, src => src.MapFrom(p => p.author))
                     .ForMember(dest => dest.body, src => src.ResolveUsing(p => StringHelper.StripTagsCharArray(p.content)))
                     .ForMember(dest => dest.body_html, src => src.MapFrom(p => p.content))
                     .ForMember(dest => dest.created_at, src => src.MapFrom(p => p.created_at))
                     .ForMember(dest => dest.email, src => src.MapFrom(p => p.email))
                     .ForMember(dest => dest.user_agent, src => src.MapFrom(p => p.user_agent))
                     .ForMember(dest => dest.ip, src => src.MapFrom(p => p.browseriP))
                     .ForMember(dest => dest.id, src => src.MapFrom(p => p._id))
                     .ForMember(dest => dest.published_at, src => src.MapFrom(p => p.created_at))
                     .ForMember(dest => dest.updated_at, src => src.MapFrom(p => p.created_at))
                     .ReverseMap();

            CreateMap<CommentListModelSeller, CommentListModel>()
              .ForMember(m => m.Id, k => k.MapFrom(a => a.Id))
              .ForMember(m => m.Articleid, k => k.MapFrom(a => a.ArticleId))
              .ForMember(m => m.BlogId, k => k.MapFrom(a => a.BlogId))
              .ForMember(m => m.ArticleTitle, k => k.MapFrom(a => a.ArticleTitle))
              .ForMember(m => m.BlogTitle, k => k.MapFrom(a => a.BlogTitle))
              .ForMember(m => m.Content, k => k.MapFrom(a => a.Comment))
              .ForMember(m => m.Author, k => k.MapFrom(a => a.CreatorName))
              .ForMember(m => m.Email, k => k.MapFrom(a => a.CreatorEmail))
              .ForMember(m => m.Status, k => k.MapFrom(a => a.Status))
              .ForMember(m => m.StatusName, k => k.MapFrom(a => a.StatusName))
              .ForMember(m => m.Created_at, k => k.MapFrom(a => a.CreatedDate))
              .ForMember(m => m.Created_at, k => k.MapFrom(a => a.CreatedAt))
              .ForMember(m => m.User_agent, k => k.MapFrom(a => a.UserAgent))
              .ForMember(m => m.browseriP, k => k.MapFrom(a => a.BrowserIP))
              .ForMember(m => m.Update_at, k => k.MapFrom(a => a.UpdatedDate))

              .ReverseMap();

            CreateMap<MGCommentModel, CommentMsg>()
                .ForMember(x => x.Id, y => y.MapFrom(a => a._id))
                .ForMember(x => x.Comment, y => y.MapFrom(a => a.content))
                .ForMember(x => x.CreatorEmail, y => y.MapFrom(a => a.email))
                .ForMember(x => x.CreatorName, y => y.MapFrom(a => a.author))
                .ForMember(x => x.CreatedAt, y => y.MapFrom(a => a.created_at))
                .ReverseMap();
        }
    }

    public static class CommentMapper
    {
        static CommentMapper()
        {
            Mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<CommonMapperProfile>();
                cfg.AddProfile<CommentMapperProfile>();
            }).CreateMapper();
        }

        internal static IMapper Mapper { get; }

        public static List<CommentListModel> CommentToModelList(this List<MGCommentModel> entity)
        {
            return Mapper.Map<List<CommentListModel>>(entity);
        }

        public static ESCommentModel CommentToModelES(this MGCommentModel entity)
        {
            var model = Mapper.Map<ESCommentModel>(entity);
            return model;
        }
        public static CommentMsg CommentToModelMsg(this MGCommentModel entity)
        {
            var model = Mapper.Map<CommentMsg>(entity);
            return model;
        }
        #region Api

        public static List<CommentAPIModel> CommentAPIToModelMGComment(this List<CommentListModel> entity)
        {
            return Mapper.Map<List<CommentAPIModel>>(entity);
        }

        public static CommentAPIModel CommentAPIToModelMGCommentDetail(this MGCommentModel entity)
        {
            var model = Mapper.Map<CommentAPIModel>(entity);
            return model;
        }
        public static List<CommentListModelSeller> SellerToListModel(this List<CommentListModel> entity)
        {
            return Mapper.Map<List<CommentListModelSeller>>(entity);
        }
        #endregion Api
    }
}