﻿using AutoMapper;
using BHN.SharedObject.APIDataModel;
using BHN.SharedObject.EBSMessage;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System.Collections.Generic;

namespace Haravan.Web.Api.BusinessObjects.Mappers
{
    public class ScriptTagProfile : Profile
    {
        public ScriptTagProfile()
        {
            CreateMap<MGScriptTagsModel, ScriptTagAPIModel>()
                .ForMember(m => m.id, k => k.MapFrom(a => a._id))
                .ForMember(dest => dest.@event, src => src.ResolveUsing(p =>
                {
                    if (!string.IsNullOrWhiteSpace(p.evenname))
                    {
                        return p.evenname;
                    }
                    return "onload";
                }))
                .ForMember(dest => dest.display_scope, src => src.ResolveUsing(p =>
                {
                    return ((ScriptTagDisplayScope)p.display_scope).ToString();
                }));
        }
    }

    public static class ScriptTagsMapper
    {
        static ScriptTagsMapper()
        {
            Mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<CommonMapperProfile>();
                cfg.AddProfile<ScriptTagProfile>();
            }).CreateMapper();
        }

        internal static IMapper Mapper { get; }

        public static List<ScriptTagAPIModel> ToListApiModel(this List<MGScriptTagsModel> entity)
        {
            return Mapper.Map<List<ScriptTagAPIModel>>(entity);
        }
        public static ScriptTagAPIModel ToApiModel(this MGScriptTagsModel entity)
        {
            return Mapper.Map<ScriptTagAPIModel>(entity);
        }
    }
}
