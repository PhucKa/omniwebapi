﻿using AutoMapper;
using BHN.SharedObject.APIDataModel;
using BHN.SharedObject.ESBuyerModel;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.BusinessObjects.Models.Common;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Haravan.Web.Api.BusinessObjects.Mappers
{
    public class BlogMapperProfile : Profile
    {
        public BlogMapperProfile()
        {
            CreateMap<BlogSearchRowModel, MGBlogModel>()
             .ForMember(m => m._id, k => k.MapFrom(a => a.Id))
             .ForMember(m => m.updated_at, k => k.MapFrom(a => a.UpdatedDate))
             .ForMember(m => m.handle, k => k.MapFrom(a => a.UrlHandle))
             .ReverseMap();

            CreateMap<MGBlogModel, BlogDetailModel>()
             .ForMember(m => m.Id, k => k.MapFrom(a => a._id))
             .ForMember(m => m.Title, k => k.MapFrom(a => a.title))
             .ForMember(m => m.PageTitle, k => k.MapFrom(a => a.meta_title))
             .ForMember(m => m.Descriptions, k => k.MapFrom(a => a.description))
             .ForMember(m => m.UrlHandle, k => k.MapFrom(a => a.handle))
             .ReverseMap();

            CreateMap<BlogDetailModel, MGBlogModel>()
            .ForMember(m => m._id, k => k.MapFrom(a => a.Id))
             .ForMember(m => m.title, k => k.MapFrom(a => a.Title))
             .ForMember(m => m.meta_title, k => k.MapFrom(a => a.PageTitle))
             .ForMember(m => m.description, k => k.MapFrom(a => a.Descriptions))
             .ForMember(m => m.handle, k => k.MapFrom(a => a.UrlHandle))
             .ForMember(m => m.meta_description, k => k.MapFrom(a => a.MetaDescription))
             .ForMember(m => m.template_suffix, k => k.MapFrom(p => p.TemplateName))
            .ReverseMap();

            CreateMap<MGBlogModel, BlogDetailModel>()
            .ForMember(m => m.Id, k => k.MapFrom(a => a._id))
             .ForMember(m => m.Title, k => k.MapFrom(a => a.title))
             .ForMember(m => m.PageTitle, k => k.MapFrom(a => a.meta_title))
             .ForMember(m => m.Descriptions, k => k.MapFrom(a => a.description))
             .ForMember(m => m.UrlHandle, k => k.MapFrom(a => a.handle))
             .ForMember(m => m.MetaDescription, k => k.MapFrom(a => a.meta_description))
             .ForMember(m => m.TemplateName, k => k.MapFrom(p => p.template_suffix))
            .ReverseMap();

            CreateMap<DropdownlistModel, MGBlogModel>()
              .ForMember(m => m._id, k => k.MapFrom(a => a.Id))
              .ForMember(m => m.title, k => k.MapFrom(a => a.Name))
              .ForMember(m => m.handle, k => k.MapFrom(a => a.UrlHandle))

              .ReverseMap();

            CreateMap<BlogDetail, BlogDetailModel>()
              .ForMember(m => m.Id, k => k.MapFrom(a => a.Id))
              .ForMember(m => m.TemplateName, k => k.MapFrom(a => a.Template_Suffix))
              .ReverseMap();

            CreateMap<ESBlogModel, MGBlogModel>()
              .ForMember(m => m._id, k => k.MapFrom(a => a.Id))
              .ReverseMap();

            CreateMap<DropdownSimpleModel, MGFeedBurnerProviderModel>()
             .ForMember(m => m._id, k => k.MapFrom(a => a.Id))
             .ForMember(m => m.url, k => k.MapFrom(a => a.Name))
             .ReverseMap();

            CreateMap<MGBlogModel, BlogAPIModel>()
                .ForMember(m => m.id, k => k.MapFrom(a => a._id))
                .ForMember(m => m.commentable, k => k.ResolveUsing(p => p.comments_enabled == true ? "yes" : "no"))
                .ForMember(m => m.feedburner, k => k.MapFrom(a => a.feed_burner_url))
                .ForMember(m => m.feedburner_location, k => k.MapFrom(a => a.feedburnervalue))
                
                .ForMember(m => m.tags, o => o.ResolveUsing(p =>
                {
                    string value = null;
                    if (p.tags != null && p.tags.Length > 0)
                        value = string.Join(", ", p.tags.ToArray());
                    return value;
                }))
                .ForMember(m => m.feedburner_location, k => k.MapFrom(a => a.feedburnervalue))
                .ForMember(m => m.feedburner, k => k.MapFrom(a => a.feed_burner_url))
                .ForMember(m => m.template_suffix, k => k.MapFrom(a => a.template_suffix))
                .ReverseMap();

            CreateMap<BlogSearchRowModel, DropdownlistModel>()
            .ForMember(dest => dest.Id, src => src.MapFrom(k => k.Id))
            .ForMember(dest => dest.Name, src => src.MapFrom(k => k.Title));

            CreateMap<BlogDetailModel, BlogDetailViewModelInternal>();

            CreateMap<MGBlogModel, BlogDetailViewModelInternal>()
                .ForMember(m => m.Id, k => k.MapFrom(a => a._id))
                .ForMember(m => m.PageTitle, k => k.MapFrom(a => a.meta_title))
                .ForMember(m => m.MetaDescription, k => k.MapFrom(a => a.meta_description))
                .ForMember(m => m.UrlHandle, k => k.ResolveUsing(p => p.handle))
                .ForMember(m => m.TemplateName, k => k.MapFrom(a => a.template_suffix))
                .ForMember(m => m.Descriptions, k => k.MapFrom(a => a.description));

            CreateMap<BlogSearchRowModel, SimpleListModel>()
                    .ForMember(a => a.Key, m => m.MapFrom(b => b.Id))
                    .ForMember(a => a.Value, m => m.MapFrom(b => b.UrlHandle))
                    .ForMember(a => a.Name, m => m.MapFrom(b => b.Title));
        }
    }

    public static class BlogMapper
    {
        static BlogMapper()
        {
            Mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<CommonMapperProfile>();
                cfg.AddProfile<BlogMapperProfile>();
            }).CreateMapper();
        }

        internal static IMapper Mapper { get; }

        public static List<DropdownlistModel> BlogDropdownlistToModelList(this List<MGBlogModel> entity)
        {
            return Mapper.Map<List<DropdownlistModel>>(entity);
        }

        public static List<BlogSearchRowModel> BlogToModelList(this List<MGBlogModel> entity)
        {
            return Mapper.Map<List<BlogSearchRowModel>>(entity);
        }

        public static MGBlogModel BlogToModelMG(this BlogDetailModel entity)
        {
            var model = Mapper.Map<MGBlogModel>(entity);
            return model;
        }

        public static BlogDetailModel BlogDetailToModelMG(this MGBlogModel entity)
        {
            var model = Mapper.Map<BlogDetailModel>(entity);
            return model;
        }

        public static ESBlogModel BlogToModelES(this MGBlogModel entity)
        {
            var model = Mapper.Map<ESBlogModel>(entity);

            return model;
        }

        public static BlogDetailModel BlogToToModelDetail(this MGBlogModel entity)
        {
            var model = Mapper.Map<BlogDetailModel>(entity);
            return model;
        }

        public static List<BlogDetailModel> MGBlogsToToDetails(this List<MGBlogModel> entity)
        {
            var model = Mapper.Map<List<BlogDetailModel>>(entity);

            return model;
        }

        public static BlogDetail BlogTToModel(this BlogDetailModel entity)
        {
            var model = Mapper.Map<BlogDetail>(entity);

            return model;
        }

        public static BlogAPIModel MGBlogToApiBlog(this MGBlogModel entity)
        {
            var model = Mapper.Map<BlogAPIModel>(entity);

            return model;
        }

        public static List<BlogAPIModel> MGBlogListToApiBlogList(this List<MGBlogModel> entity)
        {
            var model = Mapper.Map<List<BlogAPIModel>>(entity);
            return model;
        }

        public static List<DropdownSimpleModel> BurnerProviderList(this List<MGFeedBurnerProviderModel> entity)
        {
            return Mapper.Map<List<DropdownSimpleModel>>(entity);
        }

        public static BlogDetailModel ModelInternalToblogDetai(this BlogDetailViewModelInternal entity)
        {
            var model = Mapper.Map<BlogDetailModel>(entity);
            return model;
        }

        public static BlogDetailViewModelInternal BlogDetaiToModelInternal(this BlogDetailModel entity)
        {
            var model = Mapper.Map<BlogDetailViewModelInternal>(entity);
            return model;
        }

        public static List<DropdownlistModel> DropdownToListBlog(this List<BlogSearchRowModel> entity)
        {
            return Mapper.Map<List<DropdownlistModel>>(entity);
        }

        public static BlogDetailViewModelInternal BlogDetailToBlogModel(this MGBlogModel entity)
        {
            var model = Mapper.Map<BlogDetailViewModelInternal>(entity);
            return model;
        }

        public static List<BlogDetailViewModelInternal> ListBlogDetailToListBlogModel(this List<MGBlogModel> entity)
        {
            var model = Mapper.Map<List<BlogDetailViewModelInternal>>(entity);
            return model;
        }

        public static List<SimpleListModel> SimpleListModelToBlogSearchRowModel(this List<BlogSearchRowModel> entity)
        {
            var model = Mapper.Map<List<SimpleListModel>>(entity);
            return model;
        }
    }
}