﻿using AutoMapper;
using BHN.SharedObject.EBSMessage;
using Haravan.Web.Api.BusinessObjects.Models;
using Haravan.Web.Api.BusinessObjects.Models.Common;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System.Collections.Generic;

namespace Haravan.Web.Api.BusinessObjects.Mappers
{
    public class LinkListMapperProfile : Profile
    {
        public LinkListMapperProfile()
        {
            CreateMap<MGRawLinkListFields, MGLinkModel>()
                .ForMember(m => m.title, a => a.MapFrom(b => b.LinkListFieldName))
                .ForMember(m => m.handle, a => a.MapFrom(b => b.LinkListFieldHandle))
                .ForMember(m => m.url, a => a.MapFrom(b => b.TextValue))
                .ForMember(m => m.sort, a => a.MapFrom(b => b.LinkListFieldOrder));
            CreateMap<LinkListMsg, MGLinkListModel>()
                .ForMember(m => m.title, a => a.MapFrom(b => b.LinkListName))
                .ForMember(m => m.handle, a => a.MapFrom(b => b.LinkListHandle))
                .ForMember(m => m.links, a => a.MapFrom(b => b.LinkListField));

            CreateMap<MGRawLinkList, MGLinkListModel>()
                .ForMember(m => m.title, a => a.MapFrom(b => b.LinkListName))
                .ForMember(m => m.handle, a => a.MapFrom(b => b.LinkListHandle));

           
            CreateMap<MGRawLinkList, LinkList>()
              .ForMember(m => m.Id, k => k.MapFrom(a => a._id))
              .ReverseMap();
            CreateMap<LinkList, MGRawLinkList>()
             .ForMember(m => m._id, k => k.MapFrom(a => a.Id))
             .ReverseMap();

            CreateMap<MGRawLinkList, NavigationDetailModel>()
                .ForMember(m => m.Id, k => k.MapFrom(a => a._id))
                .ReverseMap();
            CreateMap<NavigationDetailModel, MGRawLinkList>()
                .ForMember(m => m._id, k => k.MapFrom(a => a.Id))
                .ReverseMap();
            //detail new 
            CreateMap<MGRawLinkList, NavigationDetailModelPath>()
               .ForMember(m => m.Id, k => k.MapFrom(a => a._id))
               .ReverseMap();
            CreateMap<NavigationDetailModelPath, MGRawLinkList>()
                .ForMember(m => m._id, k => k.MapFrom(a => a.Id))
                .ReverseMap();

            CreateMap<LinkList, NavigationDetailModel>()
                .ForMember(m => m.Id, k => k.MapFrom(a => a.Id))
                .ForMember(m => m.LinkListFields, k => k.MapFrom(a => a.LinkListFields))
                .ReverseMap();
            CreateMap<NavigationDetailModel, LinkList>()
                .ForMember(m => m.Id, k => k.MapFrom(a => a.Id))
                .ForMember(m => m.LinkListFields, k => k.MapFrom(a => a.LinkListFields))
                .ReverseMap();
            CreateMap<LinkListFieldsDetailModel, LinkListFields>()
                 .ForMember(m => m.Id, k => k.MapFrom(a => a.Id))
                .ReverseMap();
            CreateMap<LinkListFields, LinkListFieldsDetailModel>()
                 .ForMember(m => m.Id, k => k.MapFrom(a => a.Id))
                .ReverseMap();

           
            CreateMap<MGRawLinkListFields, LinkListFieldsDetailModel>()
                 .ForMember(m => m.Id, k => k.MapFrom(a => a._id))
                .ReverseMap();

            CreateMap<MGRawLinkFields, LinkFieldsListModel>()
               .ForMember(m => m.Id, k => k.MapFrom(a => a._id))
               .ForMember(m => m.Name, k => k.MapFrom(a => a.FieldDisplay))
               .ForMember(dest => dest.HasOptions, src => src.ResolveUsing(k => k.HasOptions.HasValue && k.HasOptions.Value))
               .ForMember(dest => dest.HasTags, src => src.ResolveUsing(k => k.HasTags.HasValue && k.HasTags.Value))
               .ForMember(dest => dest.HasText, src => src.ResolveUsing(k => k.HasText.HasValue && k.HasText.Value))

               .ReverseMap();
            
            CreateMap<MGRawLinkListFields, LinkListFields>()
                .ForMember(m => m.Id, k => k.MapFrom(a => a._id))
                .ReverseMap();
            CreateMap<MGRawLinkListFields, LinkListFieldsModel>()
                .ForMember(m => m.Id, k => k.MapFrom(a => a._id))
                .ReverseMap();
            CreateMap<MGRawLinkList, LinkList>()
                .ForMember(m => m.Id, k => k.MapFrom(a => a._id))
                .ReverseMap();
            CreateMap<MGRawLinkList, LinkListModel>()
                .ForMember(m => m.Id, k => k.MapFrom(a => a._id))
                .ReverseMap();
            CreateMap<LinkListModel, MGRawLinkList>()
                .ForMember(m => m._id, k => k.MapFrom(a => a.Id))
                .ReverseMap();
            CreateMap<LinkListFields, LinkListFieldsModel>()
                .ReverseMap();
            CreateMap<LinkList, LinkListModel>()
               .ReverseMap();

            CreateMap<MGRawLinkList, LinkListMsg>()
                 .ForMember(m => m.Id, k => k.MapFrom(a => a._id));
            CreateMap<LinkList, MGRawLinkList>()
                .ForMember(m => m._id, k => k.MapFrom(a => a.Id));

            CreateMap<LinkListFields, LinkListFieldMsg>();
            CreateMap<MGRawLinkListFields, LinkListFieldMsg>()
                .ForMember(m => m.Id, k => k.MapFrom(a => a._id));
            CreateMap<MGRawLinkFields, LinkFieldMsg>();

            CreateMap<LinkList, SimpleListModel>()
                  .ForMember(dest => dest.Name, src => src.MapFrom(k => k.LinkListName))
                  .ForMember(dest => dest.Value, src => src.MapFrom(k => k.LinkListHandle));

            CreateMap<SimpleListModel, LinkList>()
                 .ForMember(dest => dest.LinkListName, src => src.MapFrom(k => k.Name))
                 .ForMember(dest => dest.LinkListHandle, src => src.MapFrom(k => k.Value));

           

            CreateMap<LinkListFieldsDetailModel, LinkListFieldsDetailModelPath>();
            CreateMap<LinkListFieldsDetailModel, MGRawLinkListFields>()
                .ForMember(m => m._id, k => k.MapFrom(a => a.Id));

            CreateMap<NavigationDetailModelPath, NavigationDetailModel>();
            CreateMap<LinkListFieldsDetailModelPath, LinkListFieldsDetailModel>();


            #region API

            CreateMap<MGUrlRedirectModel, RedirectAPIModel>()
                      .ForMember(dest => dest.id, src => src.MapFrom(k => k._id))
                      .ForMember(dest => dest.path, src => src.MapFrom(k => k.oldpath))
                      .ForMember(dest => dest.target, src => src.MapFrom(k => k.redirectto));

            CreateMap<RedirectAPIModel, UrlRedirectModel>()
                   .ForMember(dest => dest.Id, src => src.MapFrom(p => p.id))
                   .ForMember(dest => dest.OldPath, src => src.MapFrom(p => p.path))
                   .ForMember(dest => dest.RedirectTo, src => src.MapFrom(p => p.target));

            CreateMap<UrlRedirectModel, RedirectAPIModel>()
                .ForMember(dest => dest.id, src => src.MapFrom(k => k.Id))
                .ForMember(dest => dest.path, src => src.MapFrom(k => k.OldPath))
                .ForMember(dest => dest.target, src => src.MapFrom(k => k.RedirectTo));

            #endregion API
        }
    }

    public static class LinkListMapper
    {
        static LinkListMapper()
        {
            Mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<CommonMapperProfile>();
                cfg.AddProfile<LinkListMapperProfile>();
            }).CreateMapper();
        }

        internal static IMapper Mapper { get; }

        #region API

        public static RedirectAPIModel RedirectAPIToMGUrlRedirectmodel(this MGUrlRedirectModel entity)
        {
            var model = Mapper.Map<RedirectAPIModel>(entity);
            return model;
        }

        public static List<RedirectAPIModel> RedirectAPIToUrlRedirectModel(this List<UrlRedirectModel> entity)
        {
            var model = Mapper.Map<List<RedirectAPIModel>>(entity);
            return model;
        }

        #endregion API

       

        public static List<SimpleListModel> SimpleToLinkList(this List<LinkList> entity)
        {
            var model = Mapper.Map<List<SimpleListModel>>(entity);
            return model;
        }
        public static MGLinkModel ToMGLinkModel(this LinkListFieldMsg entity)
        {
            var model = Mapper.Map<MGLinkModel>(entity);
            return model;
        }
        public static NavigationDetailModel ToNavigationDetailModel(this NavigationDetailModelPath entity)
        {
            var model = Mapper.Map<NavigationDetailModel>(entity);
            return model;
        }
        public static LinkListFieldsDetailModel ToLinkListFieldsDetailModel(this LinkListFieldsDetailModelPath entity)
        {
            var model = Mapper.Map<LinkListFieldsDetailModel>(entity);
            return model;
        }
        public static List<LinkListFieldsDetailModel> ToListLinkListFieldsDetailModel(this List<LinkListFieldsDetailModelPath> entity)
        {
            var model = Mapper.Map<List<LinkListFieldsDetailModel>>(entity);
            return model;
        }
        public static LinkListFieldsDetailModelPath ToLinkListFieldsDetailModelPath(this LinkListFieldsDetailModel entity)
        {
            var model = Mapper.Map<LinkListFieldsDetailModelPath>(entity);
            return model;
        }

        public static LinkListFieldMsg ToLinkListFieldMsg(this MGRawLinkListFields entity)
        {
            var model = Mapper.Map<LinkListFieldMsg>(entity);
            return model;
        }

        public static List<MGLinkModel> MGLinkModelToListLinkListFieldMsg(this List<MGRawLinkListFields> entity)
        {
            var model = Mapper.Map<List<MGLinkModel>>(entity);
            return model;
        }

        public static MGLinkListModel ToMGLinkListModel(this LinkListMsg entity)
        {
            var model = Mapper.Map<MGLinkListModel>(entity);
            return model;
        }

        public static LinkListMsg ToLinkListMsg(this LinkList entity)
        {
            var model = Mapper.Map<LinkListMsg>(entity);
            return model;
        }

        

        public static LinkFieldsListModel ToLinkFieldsListModel(this MGRawLinkFields entity)
        {
            var model = Mapper.Map<LinkFieldsListModel>(entity);
            return model;
        }

        public static List<LinkFieldsListModel> ToListLinkFieldsListModel(this List<MGRawLinkFields> entity)
        {
            var model = Mapper.Map<List<LinkFieldsListModel>>(entity);
            return model;
        }

        public static LinkListFields ToLinkListFields(this MGRawLinkListFields entity)
        {
            var model = Mapper.Map<LinkListFields>(entity);
            return model;
        }

        public static LinkListFields ToLinkListFields(this LinkListFieldsModel entity)
        {
            var model = Mapper.Map<LinkListFields>(entity);
            return model;
        }

        public static List<LinkListFields> ToLinkListFields(this List<LinkListFieldsModel> entity)
        {
            var model = Mapper.Map<List<LinkListFields>>(entity);
            return model;
        }

        public static List<LinkListFields> ToListLinkListFields(this List<MGRawLinkListFields> entity)
        {
            var model = Mapper.Map<List<LinkListFields>>(entity);
            return model;
        }

        public static LinkListFieldsModel ToLinkListFieldsModel(this MGRawLinkListFields entity)
        {
            var model = Mapper.Map<LinkListFieldsModel>(entity);
            return model;
        }

        public static List<LinkListFieldsModel> ToListLinkListFieldsModel(this List<MGRawLinkListFields> entity)
        {
            var model = Mapper.Map<List<LinkListFieldsModel>>(entity);
            return model;
        }

        public static LinkList ToLinkList(this MGRawLinkList entity)
        {
            var model = Mapper.Map<LinkList>(entity);
            return model;
        }

        public static MGRawLinkList ToMGRawLinkList(this LinkList entity)
        {
            var model = Mapper.Map<MGRawLinkList>(entity);
            return model;
        }

        public static NavigationDetailModel ToNavigationDetailModel(this MGRawLinkList entity)
        {
            var model = Mapper.Map<NavigationDetailModel>(entity);
            return model;
        }
        public static NavigationDetailModelPath DetailNewToNavigationDetailModel(this MGRawLinkList entity)
        {
            var model = Mapper.Map<NavigationDetailModelPath>(entity);
            return model;
        }

        public static MGRawLinkList MGRawLinkListToNavigationDetailModel(this NavigationDetailModel entity)
        {
            var model = Mapper.Map<MGRawLinkList>(entity);
            return model;
        }

        public static LinkList NavigationDetailModelToLinkList(this NavigationDetailModel entity)
        {
            var model = Mapper.Map<LinkList>(entity);
            return model;
        }

        public static List<LinkList> ToListLinkList(this List<MGRawLinkList> entity)
        {
            var model = Mapper.Map<List<LinkList>>(entity);
            return model;
        }

        public static LinkListModel ToLinkListModel(this LinkList entity)
        {
            var model = Mapper.Map<LinkListModel>(entity);
            return model;
        }

        public static List<LinkListModel> ToListLinkListModel(this List<LinkList> entity)
        {
            var model = Mapper.Map<List<LinkListModel>>(entity);
            return model;
        }

       
        public static MGRawLinkListFields MGRawLinkListToLinkListFields(this LinkListFields entity)
        {
            var model = Mapper.Map<MGRawLinkListFields>(entity);
            return model;
        }

        public static List<MGRawLinkListFields> ToListMGRawLinkListFields(this List<LinkListFieldsDetailModel> entity)
        {
            var model = Mapper.Map<List<MGRawLinkListFields>>(entity);
            return model;
        }
        public static MGRawLinkListFields ToMGRawLinkListFields(this LinkListFieldsDetailModel entity)
        {
            var model = Mapper.Map<MGRawLinkListFields>(entity);
            return model;
        }
        public static LinkListFieldsDetailModel ToLinkListFieldsDetailModel(this MGRawLinkListFields entity)
        {
            var model = Mapper.Map<LinkListFieldsDetailModel>(entity);
            return model;
        }

        public static LinkListModel ToLinkListModel(this MGRawLinkList entity)
        {
            var model = Mapper.Map<LinkListModel>(entity);
            return model;
        }

        public static MGLinkListModel LinkListMsgToMGRawLinkList(this MGRawLinkList entity)
        {
            var model = Mapper.Map<MGLinkListModel>(entity);
            return model;
        }
    }
}