namespace Haravan.Web.Api.BusinessObjects
{
    public class ThemeEditorDropdownModel
    {
        public string title { get; set; }
        public string code { get; set; }
        public string url { get; set; }
    }
}