﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;

namespace Haravan.Web.Api.BusinessObjects.DnsDig
{
    public class Resolver
    {
        public const int DefaultPort = 53;

        public static readonly IPEndPoint[] DefaultDnsServers =
            {
                new IPEndPoint(IPAddress.Parse("8.8.8.8"), DefaultPort),
                new IPEndPoint(IPAddress.Parse("8.8.4.4"), DefaultPort),
                new IPEndPoint(IPAddress.Parse("208.67.222.222"), DefaultPort),
                new IPEndPoint(IPAddress.Parse("208.67.220.220"), DefaultPort)
            };

        private ushort m_Unique;
        private bool m_Recursion;
        private int m_Retries;
        private int m_Timeout;

        private List<IPEndPoint> m_DnsServers;

        public Resolver()
            : this(GetDnsServers())
        {
        }
        public Resolver(IPEndPoint[] DnsServers)
        {
            m_DnsServers = new List<IPEndPoint>();
            m_DnsServers.AddRange(DnsServers);

            m_Unique = (ushort)(new Random()).Next();
            m_Retries = 3;
            m_Timeout = 1;
            m_Recursion = true;
        }

        public Response Query(string name, QType qtype, QClass qclass)
        {
            Request request = new Request();
            request.AddQuestion(new Question(name, qtype, qclass));
            request.header.ID = m_Unique;
            request.header.RD = m_Recursion;
            return Lookup(request);
        }
        private Response Lookup(Request request)
        {
            byte[] responseMessage = new byte[512];

            for (int intAttempts = 0; intAttempts < m_Retries; intAttempts++)
            {
                for (int intDnsServer = 0; intDnsServer < m_DnsServers.Count; intDnsServer++)
                {
                    Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                    socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, m_Timeout * 1000);

                    try
                    {
                        socket.SendTo(request.Data, m_DnsServers[intDnsServer]);
                        int intReceived = socket.Receive(responseMessage);
                        byte[] data = new byte[intReceived];
                        Array.Copy(responseMessage, data, intReceived);
                        Response response = new Response(m_DnsServers[intDnsServer], data);
                        return response;
                    }
                    catch (SocketException)
                    {
                        continue;
                    }
                    finally
                    {
                        m_Unique++;
                        socket.Close();
                    }
                }
            }
            Response responseTimeout = new Response();
            responseTimeout.Error = "Timeout Error";
            return responseTimeout;
        }
        private static IPEndPoint[] GetDnsServers()
        {
            List<IPEndPoint> list = new List<IPEndPoint>();

            NetworkInterface[] adapters = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface n in adapters)
            {
                if (n.OperationalStatus == OperationalStatus.Up)
                {
                    IPInterfaceProperties ipProps = n.GetIPProperties();
                    foreach (IPAddress ipAddr in ipProps.DnsAddresses)
                    {
                        IPEndPoint entry = new IPEndPoint(ipAddr, DefaultPort);
                        if (!list.Contains(entry))
                            list.Add(entry);
                    }
                }
            }
            return list.ToArray();
        }
    }
}
