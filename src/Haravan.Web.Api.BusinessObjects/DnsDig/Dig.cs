﻿namespace Haravan.Web.Api.BusinessObjects.DnsDig
{
    public class Dig
    {
        public Resolver resolver = new Resolver();

        public Dig()
        {
        }

        public Response DigIt(string name)
        {
            return resolver.Query(name, QType.TXT, QClass.IN);
        }
    }
}