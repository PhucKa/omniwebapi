﻿using Haravan.Web.Api.BusinessObjects.MongoModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    public static class LinkListHandle
    {
        public const string Facebook = "fb-haravan";
        public const string FiveVN = "5s-haravan-";
        public const string WTT = "wtt-haravan-";
    }
    
    public class LinkList 
    {
        public long Id { get; set; }
        public long StoreId { get; set; }
        public string LinkListName { get; set; }
        public string LinkListHandle { get; set; }
        public string TreeHandle { get; set; }
        public bool IsSystemLink { get; set; } = false;
        public bool IsDeleted { get; set; } = false;
        public long? CreatedUser { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? UpdatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public List<LinkListFields> LinkListFields { get; set; }

    }
    public class LinkListFields 
    {
        public long Id { get; set; }
        public long LinkListId { get; set; }
        public int LinkFieldsId { get; set; }
        public long StoreId { get; set; }
        public string LinkListFieldName { get; set; }
        public string LinkListFieldHandle { get; set; }
        public int LinkListFieldOrder { get; set; }
        public string TreeHandle { get; set; }
        public Nullable<long> RefId { get; set; }
        public string TextValue { get; set; }
        public string Tags { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<long> CreatedUser { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<long> UpdatedUser { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        
    }
    
    public class LinkFieldsListModel
    {
        
        public long Id { get; set; }
        
        public string Name { get; set; }
        
        public string OptionsUrl { get; set; }
        
        public bool HasOptions { get; set; }
        
        public bool HasText { get; set; }
        
        public bool HasTags { get; set; }
    }
    public class ListLevelLinkListTrackProcess
    {
        public List<LinkListTrackProcess> lstTrackProcessLinkList { get; set; } = new List<LinkListTrackProcess>();
        public List<MGRawLinkListFields> lstTrackProcessLinkListField { get; set; } = new List<MGRawLinkListFields>();
        public int level { get; set; } = 0;
    }
    public class LinkListTrackProcess
    {
        public long idlinklistfield { get; set; }
        public MGRawLinkListFields objlinklistfield { get; set; }
        public long idlinklist { get; set; }
        public MGRawLinkList objlinklist { get; set; }
    }
}
