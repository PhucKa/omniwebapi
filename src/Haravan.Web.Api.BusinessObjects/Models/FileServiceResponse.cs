﻿namespace Haravan.Web.Api.BusinessObjects
{
    public class FileServiceResponse
    {
        public string FileName { get; set; }
        public string Ext { get; set; }
        public string FileUrl { get; set; }
        public long Size { get; set; }
    }
}
