﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    public class OnlineStoreSetting
    {
        public long Id { get; set; }
        public string StoreName { get; set; }
        public string HomeTitle { get; set; }
        public string Description { get; set; }
        public string GoogleAnalyticsAccount { get; set; }
        public string GoogleAnalyticsScript { get; set; }
        public bool? PasswordProtect { get; set; }
        public string Password { get; set; }
        public string MessageForVisitors { get; set; }
        public string FacebookPixelId { get; set; }
        public bool IsShowFbPixelChange { get; set; }
        public long? WebLocationId { get; set; }
        public bool IsUseEnhancedEcommerce { get; set; }
        public bool IsUseHaravanTracking { get; set; }
        public long StoreId { get; set; }
    }
}
