﻿using System;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    public class CheckoutSettingModel
    {
        public long Id { get; set; }

        public int CheckoutAccountType { get; set; }

        public bool IsUseBillingAddress { get; set; }

        public int EmailPromotionalMode { get; set; }

        public int PlacedOrderBehavior { get; set; }

        public int PaidOrderBehavior { get; set; }

        public bool IsNotifyCustomerViaEmail { get; set; }

        public bool IsAutoFulfillAllOrder { get; set; }

        public bool FulfilledOrderBehavior { get; set; }

        public string AdditionalContent { get; set; }

        public string RefundPolicy { get; set; }

        public string PrivacyPolicy { get; set; }

        public string TermsOfService { get; set; }

        public long CreatedUser { get; set; }

        public DateTime CreatedDate { get; set; }

        public long UpdatedUser { get; set; }

        public DateTime UpdatedDate { get; set; }

        public string VersionNo { get; set; }

        public bool IsDeleted { get; set; }
        public int EmailCheckoutBehavior { get; set; }
        public int PhoneCheckoutBehavior { get; set; }
        public int AddressCheckoutBehavior { get; set; }
        public int DistrictCheckoutBehavior { get; set; }
        public int WardCheckoutBehavior { get; set; }
        public int FullnameCheckoutBehavior { get; set; }
        public int AbandonedCheckoutBehavior { get; set; }
        public bool IsAllowCustomerCanPickLocation { get; set; }
        public string SecretMultipass { get; set; }
        public string AccountKitAppId { get; set; }
        public bool IsActiveAccountKit { get; set; }
        public bool AccountKitAllowCreateAccount { get; set; }
        public bool AccountKitVerifyAccountAfterLogin { get; set; }
    }
    public class EnableAccountKitRequest
    {
        public long id { get; set; }
        public int checkoutAccountType { get; set; }
        public string appId { get; set; }
        public string appSecret { get; set; }
        public string secret { get; set; }
        public bool isAccountKitAllowCreateAccount { get; set; }
        public bool isVerifyPasswordAfterLogin { get; set; }
    }
    public class EnableMultipassRequest
    {
        public long id { get; set; }
        public int checkoutAccountType { get; set; }
    }
    public class DisableByIdRequest
    {
        public long id { get; set; }       
    }
    public class CheckoutSettingModelWeb
    {
        public long Id { get; set; }
        public int CheckoutAccountType { get; set; }
        public string AdditionalContent { get; set; }
        public long CreatedUser { get; set; }
        public DateTime CreatedDate { get; set; }
        public long UpdatedUser { get; set; }
        public DateTime UpdatedDate { get; set; }        
        public bool IsDeleted { get; set; }
        public int EmailCheckoutBehavior { get; set; }
        public int PhoneCheckoutBehavior { get; set; }
        public int AddressCheckoutBehavior { get; set; }
        public int DistrictCheckoutBehavior { get; set; }
        public int WardCheckoutBehavior { get; set; }
        public int FullnameCheckoutBehavior { get; set; } 
      
    }
    
    public class CustomerAccountModel
    {
        public long Id { get; set; }
        public int CheckoutAccountType { get; set; }
    }
    public class FieldSettingModel
    {
        public long Id { get; set; }
        public int EmailCheckoutBehavior { get; set; }
        public int PhoneCheckoutBehavior { get; set; }
        public int AddressCheckoutBehavior { get; set; }
        public int DistrictCheckoutBehavior { get; set; }
        public int WardCheckoutBehavior { get; set; }
        public int FullnameCheckoutBehavior { get; set; }
    }
    public class OrderAdditionalContentModel
    {
        public long Id { get; set; }
        public string AdditionalContent { get; set; }
    }
    

}