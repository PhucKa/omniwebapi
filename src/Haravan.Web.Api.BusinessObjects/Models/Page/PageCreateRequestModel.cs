﻿using System.Collections.Generic;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    public class PageRequestModel
    {
        public PageDetailModel pages { get; set; }
        public List<NavigationDetailModel> linkList { get; set; }
    }

    public class OpenApiPageRequestModel
    {
        public Dictionary<string, object> dicData { get; set; }
    }

    public class APIRequestModel
    {
        public long Id { get; set; }
        public Dictionary<string, object> updatedData { get; set; }
    }

    public class APIRequestUpdateModel
    {
        public long Id { get; set; }
        public Dictionary<string, object> Data { get; set; }
    }
    public class OpenApiArticleRequestModel
    {
        public Dictionary<string, object> dicData { get; set; }
    }
}