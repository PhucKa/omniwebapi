﻿using System;
using System.Collections.Generic;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    public class PageDetailViewModel
    {
        public long Id { get; set; }

        public string Title { get; set; }
        public string Content { get; set; }

        public string ShortContent { get; set; }

        public int StoreId { get; set; }

        public DateTime? PublishedDate { get; set; }
        public string PageTitle { get; set; }
        public string MetaDescription { get; set; }

        public string UrlHandle { get; set; }

        public string BuyerDomain { get; set; }

        public string TemplateName { get; set; }



        public List<NavigationDetailModel> LinkList { get; set; }
        public bool IsSetPublishDate { get; set; }
        public bool IsVisible { get; set; }
    }
}