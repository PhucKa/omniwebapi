﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    [DataContract]
    public class PageListModel
    {
        [DataMember]
        public long id { get; set; }

        [DataMember]
        public string handle { get; set; }

        [DataMember]
        public string title { get; set; }

        [DataMember]
        public string shortcontent { get; set; }

        [DataMember]
        public string authorname { get; set; }

        [DataMember]
        public DateTime updated_at { get; set; }

        [DataMember]
        public string author { get; set; }

        [DataMember]
        public DateTime? created_at { get; set; }

        [DataMember]
        public DateTime? published_at { get; set; }

        [DataMember]
        public string content { get; set; }
        [DataMember]
        public long storeid { get; set; }
        [DataMember]
        public string template_suffix { get; set; }
        [DataMember]
        public bool IsVisible { get; set; }
    }
    public class PageList
    {
        public long pages_count { get; set; }
        public List<PageListModel> data { get; set; }
    }

}