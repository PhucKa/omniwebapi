﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    public class ArticleLogModel
    {
        public string Title { get; set; }

        public DateTime LogDate { get; set; }

        public string LogUser { get; set; }

        public string LinkDetail { get; set; }
        public long BlogId { get; set; }
    }

    public class PageLogModel
    {
        public string Title { get; set; }

        public DateTime LogDate { get; set; }

        public string LogUser { get; set; }

        public string LinkDetail { get; set; }
    }

    [DataContract]
    public class PageDetailModel
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Content { get; set; }

        [DataMember]
        public string ShortContent { get; set; }

        [DataMember]
        public bool IsDeleted { get; set; }

        [DataMember]
        public DateTime? PublishedDate { get; set; }

        [DataMember]
        public string PageTitle { get; set; }

        [DataMember]
        public string MetaDescription { get; set; }

        [DataMember]
        public string UrlHandle { get; set; }

        [DataMember]
        public string TemplateName { get; set; }

        [DataMember]
        public string AuthorName { get; set; }


        [DataMember]
        public List<NavigationDetailModel> LinkList { get; set; }

        [DataMember]
        public bool IsSetPublishDate { get; set; }

        [DataMember]
        public bool IsVisible { get; set; }

        [DataMember]
        public DateTime UpdatedDate { get; set; }

        [DataMember]
        public DateTime? CreatedDate { get; set; }

        public long? Created_user { get; set; }

        [DataMember]
        public long StoreId { get; set; }
    }

    public class PageDetailEcomViewModel
    {
        public PageDetailModel pages { get; set; }
        public List<NavigationDetailModel> linkList { get; set; }
    }

    public class PageDetail
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Content { get; set; }

        [DataMember]
        public string ShortContent { get; set; }

        [DataMember]
        public bool IsDeleted { get; set; }

        [DataMember]
        public DateTime? PublishedDate { get; set; }

        [DataMember]
        public string PageTitle { get; set; }

        [DataMember]
        public string MetaDescription { get; set; }

        [DataMember]
        public string UrlHandle { get; set; }

        [DataMember]
        public string TemplateName { get; set; }

        [DataMember]
        public string AuthorName { get; set; }

  

        [DataMember]
        public List<NavigationDetailModel> LinkList { get; set; }

        [DataMember]
        public bool IsSetPublishDate { get; set; }

        [DataMember]
        public bool IsVisible { get; set; }
    }

    [DataContract]
    public class PageDetailShortModel
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string UrlHandle { get; set; }

        [DataMember]
        public string AuthorName { get; set; }

        [DataMember]
        public string TemplateName { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Content { get; set; }

        [DataMember]
        public bool IsVisible { get; set; }

        [DataMember]
        public long StoreId { get; set; }

        [DataMember]
        public string ShortContent { get; set; }

        [DataMember]
        public DateTime UpdatedDate { get; set; }
    }
}