﻿using System.Collections.Generic;

namespace Haravan.Web.Api.BusinessObjects.Models
{
   
    public enum TemplateType
    {
        Article = 1,
        Blog = 2,
        Cart = 3,
        Collection = 4,
        Page = 5,
        Product = 6
    }

    public static class TemplateName
    {
        public const string Article = "article";
        public const string Blog = "blog";
        public const string Cart = "cart";
        public const string Collection = "collection";
        public const string Page = "page";
        public const string Product = "product";
        public const string Collection5s = "collection.5s";
        public const string Cart5s = "cart.5s";
        public const string Product5s = "product.5s";
        public const string CollectionWTT = "collection.wtt";
        public const string CartWTT = "cart.wtt";
        public const string ProductWTT = "product.wtt";
    }
    public class PagePublishRequest
    {
        public List<long> pagesIds { get; set; }
        public bool isPublish { get; set; }
    }
}
