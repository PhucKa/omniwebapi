﻿using System;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    public class ReportScreenRowModel
    {
        public long Id { get; set; }
        public int ViewId { get; set; }
        public string TypeName { get; set; }
        public string ReportNameSystem { get; set; }
        public string ReportName { get; set; }
        public string Description { get; set; }
        public bool IsSystemReport { get; set; }
        public long? CreatedUser { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? UpdatedUser { get; set; }
        public Nullable<int> OrderNumber { get; set; }
    }
}