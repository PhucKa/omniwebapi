﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    public class DataTempmodel
    {
        public string Url { get; set; }
        public string _id { get; set; }
        public string View { get; set; }
        public string TotalVisitors { get; set; }
        public string NewVisitors { get; set; }
        public string TotalVisits { get; set; }
        public string Avgtime { get; set; }
        public string Landings { get; set; }
        public string Exits { get; set; }
        public string Bounces { get; set; }
        public string Avgscroll { get; set; }
        public string AvgsTimecroll { get; set; }

        public string Day { get; set; }
        public string TotalSessions { get; set; }
        public string NewSessions { get; set; }
        public string UniqueSessions { get; set; }

        public string TimeDurations { get; set; }
        public string UsersDurations { get; set; }
        public string PercentageDurations { get; set; }

        public string TimeFrequency { get; set; }
        public string UsersFrequency { get; set; }
        public string PercentageFrequency { get; set; }

        public string NamePlatforms { get; set; }
        public string ValuePlatforms { get; set; }
        public string PercentPlatforms { get; set; }

        public string ProvincialCities { get; set; }
        public string TotalCities { get; set; }
        public string NewCities { get; set; }
        public string UniqueCities { get; set; }
    }

    public class ViewPageTableData
    {
        public long iTotalDisplayRecords { get; set; }
        public long iTotalRecords { get; set; }
        public long sEcho { get; set; }
        public List<Rootobject> aaData { get; set; }
    }

    public class ViewPageTableTotalVisitors : Rootobject
    {
    }

    public class ViewPageTableNewVisitors : Rootobject
    {
    }

    public class ViewPageTableTotalVisits : Rootobject
    {
    }

    public class Rootobject
    {
        public string _id { get; set; }
        public string uvalue { get; set; }
        public string u { get; set; }
        public string t { get; set; }
        public string s { get; set; }
        public string b { get; set; }
        public string e { get; set; }

        [JsonProperty(PropertyName = "d-calc")]
        public string dcalc { get; set; }

        public string d { get; set; }
        public string n { get; set; }

        [JsonProperty(PropertyName = "scr-calc")]
        public string scrcalc { get; set; }

        public string scr { get; set; }
        public View_Meta[] view_meta { get; set; }
        public string view { get; set; }
        public string url { get; set; }
    }

    public class View_Meta
    {
        public string _id { get; set; }
        public double org { get; set; }
        public string view { get; set; }
        public Domain domain { get; set; }
        public string url { get; set; }
    }

    public class Domain
    {
        public bool namedomain { get; set; }
    }
}