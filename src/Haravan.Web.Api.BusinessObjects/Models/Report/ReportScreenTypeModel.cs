﻿using System;
using System.Collections.Generic;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    public class ReportScreenTypeModel
    {
        public string Typename { get; set; }
        public Nullable<int> OrderNumber { get; set; }
        public List<ReportScreenRowModel> LstReportScreen { get; set; }
        public bool IsSystemReport { get; set; }
    }
    public class VariantWithLinkInReport
    {
        public string Link { get; set; }
        public string Sku { get; set; }
        public string Barcode { get; set; }
    }
}