﻿using System;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    public class ReportScreenFilterData
    {
        public long Id { get; set; }
        public long StoreId { get; set; }
        public long FilterId { get; set; }
        public string FilterData { get; set; }
        public DateTime? FilterDataDateTime { get; set; }
        public long? CreatedUser { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? UpdatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public object dependdata { get; set; }
        public string OperatorSymbol { get; set; }
        public string DisplayValue { get; set; }
    }
}