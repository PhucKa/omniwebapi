﻿using System;
using System.Collections.Generic;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    public class ReportScreenFilter
    {
        public long Id { get; set; }
        public long ReportScreenId { get; set; }
        public long StoreId { get; set; }
        public string FilterArea { get; set; }
        public bool IsDisplay { get; set; }
        public string FilterName { get; set; }
        public string FieldName { get; set; }
        public string FilterDataType { get; set; }
        public string OperatorSymbol { get; set; }
        public long? CreatedUser { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? UpdatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public List<ReportScreenFilterData> LstReportScreenFilterData { get; set; }
        public FilterOperator CurrentOperator { get; set; }
        public List<FilterOperator> AcceptOperators { get; set; } = new List<FilterOperator>();
        public FilterOperator CurrentCondition { get; set; }
        public string databind { get; set; }
        public string AcceptSymbols { get; set; }
    }

    public class FilterOperator
    {
        public string name { get; set; }
        public string symbol { get; set; }
        public string displaysymbol { get; set; }
    }
}