﻿using System;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    public class ReportScreenGroupProperty
    {
        public long Id { get; set; }
        public long? StoreId { get; set; }
        public long ReportScreenId { get; set; }
        public string GroupAreaName { get; set; }
        public string GroupPropertyField { get; set; }
        public string GroupPropertyName { get; set; }
        public Nullable<bool> IsSelected { get; set; }
        public long? CreatedUser { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? UpdatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public Nullable<int> OrderNumber { get; set; }
        public Nullable<int> OrderNumberGroup { get; set; }
        public Nullable<int> SelectOrderNumber { get; set; }
        public string DataFormat { get; set; }
        public Nullable<bool> IsDisplay { get; set; }
        public Nullable<bool> IsFix { get; set; }
    }
}