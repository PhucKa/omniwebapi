﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    public class ReportScreen
    {
        public long Id { get; set; }
        public int ViewId { get; set; }
        public string TypeName { get; set; }
        public string ReportName { get; set; }
        public string ReportNameSystem { get; set; }
        public string Description { get; set; }
        public bool IsSystemReport { get; set; }
        public bool IsAddNewReport { get; set; }
        public long? CreatedUser { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? UpdatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public List<ReportScreenFilter> LstReportScreenFilter { get; set; }
        public List<ReportScreenGroupProperty> LstReportScreenGroupProperty { get; set; }
        public List<ReportScreenMeasure> LstReportScreenMeasure { get; set; }
        public string Fieldsort { get; set; }
        public string Sortype { get; set; }
        public long FieldCurrentFilterId { get; set; }
        public int offset { get; set; }
        public int limit { get; set; }
        public Nullable<int> OrderNumber { get; set; }
        public string DisplayChartMode { get; set; }
        public bool IsApiReport { get; set; }
        public string StockType { get; set; }
    }

    public class EnumModel
    {
        public int Value { get; set; }

        public string DisplayName { get; set; }

        public string DisplayShortName { get; set; }

        public string Name { get; set; }
    }

    public class ReportDataModel
    {
        public List<BsonDocument> reportdata { get; set; }
        public Dictionary<string, List<object>> dependdata { get; set; }
    }

    public class reportdata
    {
        public DateTime date { get; set; }
        public double total { get; set; }
    }

    public class reportcapital
    {
        public DateTime date { get; set; }
        public double revenue { get; set; }
        public double capital { get; set; }
    }

    public class ReportDataViewModel
    {
        public List<string> data { get; set; }
        public Dictionary<string, List<object>> dependdata { get; set; }
        public List<ReportDataColumn> summarymeasuredata { get; set; } = new List<ReportDataColumn>();
        public List<ReportDataColumn> headerreport { get; set; } = new List<ReportDataColumn>();
        public List<List<List<object>>> datanew { get; set; } = new List<List<List<object>>>();
    }

    public class ReportDataViewWebModel
    {
        public Dictionary<string, List<object>> dependdata { get; set; }
        public List<ReportDataColumn> summarymeasuredata { get; set; } = new List<ReportDataColumn>();
        public List<ReportDataColumn> headerreport { get; set; } = new List<ReportDataColumn>();
        public List<List<object>> data { get; set; } = new List<List<object>>();
        public List<List<object>> datalink { get; set; } = new List<List<object>>();
        public long TotalRecords { get; set; }
    }

    public class ReportDataViewOmniModelV3
    {
        public Dictionary<string, List<object>> dependdata { get; set; }
        public List<ReportDataColumn> summarymeasuredata { get; set; } = new List<ReportDataColumn>();
        public List<ReportDataColumn> headerreport { get; set; } = new List<ReportDataColumn>();
        public List<List<List<object>>> data { get; set; } = new List<List<List<object>>>();
    }

    public class ReportDataColumn
    {
        public object Name { get; set; }
        public object Value { get; set; }
        public object NameValue { get; set; }
        public object Type { get; set; }
        public object ValueDisplay { get; set; }
        public object DataFormat { get; set; }
        public object SelectOrderNumber { get; set; }
        public object IsSelected { get; set; }
        public object IsFix { get; set; }
    }

    public class ApiDashboard
    {
        public List<ListModelDashboard> ListDashboard { get; set; }
        public List<TopSellerProductMobile> ListTopSellerProduct { get; set; }
        public List<TopSellerChannelMobile> ListTopChannelSale { get; set; }
        public long TotalFulfill { get; set; }
        public long TotalPayment { get; set; }
    }

    public class TopSellerProductMobile
    {
        //today,yesterday,thisweek,thismonth
        public string type { get; set; }

        public List<ProductItem> items { get; set; }
    }

    public class TopSellerChannelMobile
    {
        public string type { get; set; }
        public List<ChannelItem> items { get; set; }
    }

    public class ListModelDashboard
    {
        // today:hour, thisweek:day,thismonth:day,yesterday:hour
        public string type { get; set; }

        //type:today
        // total amount
        //type:thisweek
        public double total_amount { get; set; }

        // total orders
        public int total_order { get; set; }

        // list items
        public List<Items> items { get; set; }
    }

    public class Items
    {
        // hour or day
        public string hour_or_day { get; set; }  //hour  //day

        public string labeldisplay { get; set; }

        // total order item of hour or day
        public int total_order_item { get; set; }

        // total price item of hour or day
        public double total_price_item { get; set; }
    }

    public class ProductItem
    {
        // total item sales
        public long productid { get; set; }

        public string productname { get; set; }
        public int quantiy { get; set; }
        public double revenue { get; set; }
        public string linkimage { get; set; }
        public string linkbuyer { get; set; }
        public bool isdelete { get; set; }
    }

    public class ChannelItem
    {
        // channel sales
        public string channelname { get; set; }

        public int quantiy { get; set; }
    }

    #region Class API V2

    public class ApiDashboardChart
    {
        public List<ListModelDashboard> ListDashboard { get; set; }
    }

    public class TotalSummaryOrder
    {
        public long TotalFulfill { get; set; }
        public long TotalPayment { get; set; }
    }

    public class TopProductChannel
    {
        public List<TopSellerProductMobile> ListTopSellerProduct { get; set; }
        public List<TopSellerChannelMobile> ListTopChannelSale { get; set; }
    }

    #endregion Class API V2

    #region omni

    public class FilterDataModel
    {
        public string id { get; set; }
        public string name { get; set; }
        public string displayname_vi { get; set; }
        public string value { get; set; }
    }

    public class query
    {
        public string time_group { get; set; }
        public pagination pagination { get; set; } = new pagination();
        public period daterange { get; set; }
        public orderby order_by { get; set; }
        public List<filter> filters { get; set; }
        public List<metric> metrics { get; set; }
        public List<string> groupby { get; set; }
        public string ReportName { get; set; }
    }

    public class pagination
    {
        public int offset { get; set; }
        public int limit { get; set; }
    }

    public class period
    {
        public DateTime? startdate { get; set; }
        public DateTime? enddate { get; set; }
    }

    public class metric
    {
        public string measureName { get; set; }
        public string measureField { get; set; }
    }

    public class orderby
    {
        public string dimension { get; set; }
        public string direction { get; set; } = "DESC";
    }

    public class filter
    {
        public List<filteritem> query { get; set; }
        public string conjunction { get; set; } // and , or
        public string dimension { get; set; }
        public string datatype { get; set; }
    }

    public class filteritem
    {
        public string symbol { get; set; }  //=, !=,>,>=,<,<=
        public string value { get; set; }
        public string query { get; set; }
        public string conjunction { get; set; } // and , or
    }

    #endregion omni

    public class SYS_ReportScreen
    {
        public long Id { get; set; }
        public long StoreId { get; set; }
        public int ViewId { get; set; }
        public string TypeName { get; set; }
        public string ReportName { get; set; }
        public string ReportNameSystem { get; set; }
        public string Description { get; set; }
        public bool IsSystemReport { get; set; } = false;
        public bool? IsAddNewReport { get; set; }
        public long? CreatedUser { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? UpdatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string Fieldsort { get; set; }
        public string Sortype { get; set; }
        public int? OrderNumber { get; set; }
        public string DisplayChartMode { get; set; }
        public bool? IsDisplay { get; set; }
    }

    public class SYS_ReportScreenFilter
    {
        public long Id { get; set; }
        public long? ReportScreenId { get; set; }
        public long? StoreId { get; set; }
        public string FilterArea { get; set; }
        public bool IsDisplay { get; set; } = true;
        public string FilterName { get; set; }
        public string FieldName { get; set; }
        public string FilterDataType { get; set; }
        public string OperatorSymbol { get; set; }
        public long? CreatedUser { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? UpdatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string AcceptSymbols { get; set; }
        public string Conjunction { get; set; }
    }

    public class SYS_ReportScreenGroupProperty
    {
        public long Id { get; set; }
        public long? StoreId { get; set; }
        public long ReportScreenId { get; set; }
        public string GroupAreaName { get; set; }
        public string GroupPropertyField { get; set; }
        public string GroupPropertyName { get; set; }
        public bool? IsSelected { get; set; }
        public long? CreatedUser { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? UpdatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? SelectOrderNumber { get; set; }
        public int? OrderNumber { get; set; }
        public int? OrderNumberGroup { get; set; }
        public string DataFormat { get; set; }
        public bool? IsDisplay { get; set; }
        public bool? IsFix { get; set; }
    }

    public class SYS_ReportScreenMeasure
    {
        public long Id { get; set; }
        public long? StoreId { get; set; }
        public long ReportScreenId { get; set; }
        public string MeasureName { get; set; }
        public string MeasureField { get; set; }
        public bool? IsDefault { get; set; }
        public bool? IsMoney { get; set; }
        public bool? IsSelected { get; set; }
        public int? SelectOrderNumber { get; set; }
        public long? CreatedUser { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? UpdatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? DataFormat { get; set; }
        public string ChartType { get; set; }
        public bool? IsDraw { get; set; }
        public bool? IsFix { get; set; }
    }

    public class SYS_ReportScreenFilterData
    {
        public long Id { get; set; }
        public long? StoreId { get; set; }
        public long? FilterId { get; set; }
        public string FilterData { get; set; }
        public DateTime? FilterDataDateTime { get; set; }
        public long? CreatedUser { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? UpdatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string OperatorSymbol { get; set; }
    }
    public class WebReportDataModellRequest
    {
        public string reportId { get; set; }
        public query query { get; set; }
    }

}