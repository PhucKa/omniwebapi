﻿using System;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    public class ReportScreenMeasure
    {
        public long Id { get; set; }
        public long? StoreId { get; set; }
        public long ReportScreenId { get; set; }
        public string MeasureName { get; set; }
        public string MeasureField { get; set; }
        public Nullable<bool> IsDefault { get; set; }
        public Nullable<bool> IsMoney { get; set; }
        public Nullable<bool> IsSelected { get; set; }
        public long? CreatedUser { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? UpdatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public Nullable<int> SelectOrderNumber { get; set; }
        public Nullable<int> DataFormat { get; set; }
        public string ChartType { get; set; }
        public Nullable<bool> IsDraw { get; set; }
        public Nullable<bool> IsFix { get; set; }
    }
}