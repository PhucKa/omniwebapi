﻿using System.Collections.Generic;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    public static class ReportDatadefault
    {
        public static List<SYS_ReportScreen> SYS_ReportScreen = new List<SYS_ReportScreen>
        {
            new SYS_ReportScreen
                {
                     Id = 100735,
                     StoreId= 0,
                     ViewId= 8,
                     TypeName= "sales",
                     ReportName= "Số lượt truy cập trang ",
                     ReportNameSystem= "page_view",
                     Description= "Bảng báo cáo số lượt truy cập trang",
                     IsSystemReport= false,
                     IsAddNewReport= false,
                     CreatedUser= 1000108974,
                     CreatedDate= null,
                     UpdatedUser= 1000108974,
                     UpdatedDate= null,
                     Fieldsort= "View",
                     Sortype= "desc",
                     OrderNumber= 28,
                     DisplayChartMode= "ComboChart",
                     IsDisplay= true
                },
            new SYS_ReportScreen
                {
                     Id = 100736,
                     StoreId= 0,
                     ViewId= 8,
                     TypeName= "sales",
                     ReportName= "Số lượng traffic ",
                     ReportNameSystem= "analytics_session",
                     Description= "Bảng báo cáo số lượng traffic của trang",
                     IsSystemReport= false,
                     IsAddNewReport= false,
                     CreatedUser= 1000108974,
                     CreatedDate= null,
                     UpdatedUser= 1000108974,
                     UpdatedDate= null,
                     Fieldsort= "Day",
                     Sortype= "desc",
                     OrderNumber= 29,
                     DisplayChartMode= "ComboChart",
                     IsDisplay= true
                },
            new SYS_ReportScreen
                {
                     Id = 100737,
                     StoreId= 0,
                     ViewId= 8,
                     TypeName= "sales",
                     ReportName= "Thời gian truy cập trang",
                     ReportNameSystem= "analytics_durations",
                     Description= "Bảng báo cáo thời gian truy cập trang",
                     IsSystemReport= false,
                     IsAddNewReport= false,
                     CreatedUser= 1000108974,
                     CreatedDate= null,
                     UpdatedUser= 1000108974,
                     UpdatedDate= null,
                     Fieldsort= "TimeDurations",
                     Sortype= "desc",
                     OrderNumber= 29,
                     DisplayChartMode= "ComboChart",
                     IsDisplay= true
                },
             new SYS_ReportScreen
                {
                     Id = 100738,
                     StoreId= 0,
                     ViewId= 8,
                     TypeName= "sales",
                     ReportName= "Tần xuất truy cập trang",
                     ReportNameSystem= "analytics_frequency",
                     Description= "Bảng báo cáo tần xuất truy cập trang",
                     IsSystemReport= false,
                     IsAddNewReport= false,
                     CreatedUser= 1000108974,
                     CreatedDate= null,
                     UpdatedUser= 1000108974,
                     UpdatedDate= null,
                     Fieldsort= "TimeFrequency",
                     Sortype= "desc",
                     OrderNumber= 29,
                     DisplayChartMode= "ComboChart",
                     IsDisplay= true
                }

            
        };

        public static List<SYS_ReportScreenFilter> SYS_ReportScreenFilter = new List<SYS_ReportScreenFilter>
        {
            new SYS_ReportScreenFilter
                {
                        Id = 400,
                        ReportScreenId= 100735,
                        StoreId = 0,
                        FilterArea = "ViewPage",
                        IsDisplay = true,
                        FilterName = "StartDate",
                        FieldName = "OrderDate",
                        FilterDataType = "DateTime",
                        OperatorSymbol = "is",
                        CreatedUser = 1000108974,
                        CreatedDate= null,
                        UpdatedUser = 1000108974,
                        UpdatedDate= null,
                        AcceptSymbols = "is,isnot",
                        Conjunction = null
                },
            new SYS_ReportScreenFilter
                {
                        Id = 401,
                        ReportScreenId= 100735,
                        StoreId = 0,
                        FilterArea = "ViewPage",
                        IsDisplay = true,
                        FilterName = "EndDate",
                        FieldName = "OrderDate",
                        FilterDataType = "DateTime",
                        OperatorSymbol = "is",
                        CreatedUser = 1000108974,
                        CreatedDate= null,
                        UpdatedUser = 1000108974,
                        UpdatedDate= null,
                        AcceptSymbols = "is,isnot",
                        Conjunction = null
                },
            new SYS_ReportScreenFilter
                {
                        Id = 402,
                        ReportScreenId= 100736,
                        StoreId = 0,
                        FilterArea = "AnalyticsSession",
                        IsDisplay = true,
                        FilterName = "StartDate",
                        FieldName = "OrderDate",
                        FilterDataType = "DateTime",
                        OperatorSymbol = "is",
                        CreatedUser = 1000108974,
                        CreatedDate= null,
                        UpdatedUser = 1000108974,
                        UpdatedDate= null,
                        AcceptSymbols = "is,isnot",
                        Conjunction = null
                },
            new SYS_ReportScreenFilter
                {
                        Id = 403,
                        ReportScreenId= 100736,
                        StoreId = 0,
                        FilterArea = "AnalyticsSession",
                        IsDisplay = true,
                        FilterName = "EndDate",
                        FieldName = "OrderDate",
                        FilterDataType = "DateTime",
                        OperatorSymbol = "is",
                        CreatedUser = 1000108974,
                        CreatedDate= null,
                        UpdatedUser = 1000108974,
                        UpdatedDate= null,
                        AcceptSymbols = "is,isnot",
                        Conjunction = null
                },
             new SYS_ReportScreenFilter
                {
                        Id = 404,
                        ReportScreenId= 100737,
                        StoreId = 0,
                        FilterArea = "AnalyticsDurations",
                        IsDisplay = true,
                        FilterName = "StartDate",
                        FieldName = "OrderDate",
                        FilterDataType = "DateTime",
                        OperatorSymbol = "is",
                        CreatedUser = 1000108974,
                        CreatedDate= null,
                        UpdatedUser = 1000108974,
                        UpdatedDate= null,
                        AcceptSymbols = "is,isnot",
                        Conjunction = null
                },
            new SYS_ReportScreenFilter
                {
                        Id = 405,
                        ReportScreenId= 100737,
                        StoreId = 0,
                        FilterArea = "AnalyticsDurations",
                        IsDisplay = true,
                        FilterName = "EndDate",
                        FieldName = "OrderDate",
                        FilterDataType = "DateTime",
                        OperatorSymbol = "is",
                        CreatedUser = 1000108974,
                        CreatedDate= null,
                        UpdatedUser = 1000108974,
                        UpdatedDate= null,
                        AcceptSymbols = "is,isnot",
                        Conjunction = null
                },
            new SYS_ReportScreenFilter
                {
                        Id = 406,
                        ReportScreenId= 100738,
                        StoreId = 0,
                        FilterArea = "AnalyticsFrequency",
                        IsDisplay = true,
                        FilterName = "StartDate",
                        FieldName = "OrderDate",
                        FilterDataType = "DateTime",
                        OperatorSymbol = "is",
                        CreatedUser = 1000108974,
                        CreatedDate= null,
                        UpdatedUser = 1000108974,
                        UpdatedDate= null,
                        AcceptSymbols = "is,isnot",
                        Conjunction = null
                },
            new SYS_ReportScreenFilter
                {
                        Id = 407,
                        ReportScreenId= 100738,
                        StoreId = 0,
                        FilterArea = "AnalyticsFrequency",
                        IsDisplay = true,
                        FilterName = "EndDate",
                        FieldName = "OrderDate",
                        FilterDataType = "DateTime",
                        OperatorSymbol = "is",
                        CreatedUser = 1000108974,
                        CreatedDate= null,
                        UpdatedUser = 1000108974,
                        UpdatedDate= null,
                        AcceptSymbols = "is,isnot",
                        Conjunction = null
                },
        };

        public static List<SYS_ReportScreenGroupProperty> SYS_ReportScreenGroupProperty = new List<SYS_ReportScreenGroupProperty>
        {
            new SYS_ReportScreenGroupProperty
                {
                          Id = 40,
                          StoreId = 0,
                          ReportScreenId = 100735,
                          GroupAreaName = "ViewPage",
                          GroupPropertyField = "View",
                          GroupPropertyName = "View",
                          IsSelected = true,
                          CreatedUser = 1000108974,
                          CreatedDate = null,
                          UpdatedUser = 1000108974,
                          UpdatedDate = null,
                          SelectOrderNumber = 0,
                          OrderNumberGroup = 0,
                          DataFormat = "text",
                          IsDisplay = null,
                          IsFix = null
                },
            new SYS_ReportScreenGroupProperty
                {
                          Id = 41,
                          StoreId = 0,
                          ReportScreenId = 100735,
                          GroupAreaName = "ViewPage",
                          GroupPropertyField = "TotalVisitors",
                          GroupPropertyName = "TotalVisitors",
                          IsSelected = false,
                          CreatedUser = 1000108974,
                          CreatedDate = null,
                          UpdatedUser = 1000108974,
                          UpdatedDate = null,
                          SelectOrderNumber = 1,
                          OrderNumberGroup = 1,
                          DataFormat = "text",
                          IsDisplay = null,
                          IsFix = null
                },
            new SYS_ReportScreenGroupProperty
                {
                          Id = 42,
                          StoreId = 0,
                          ReportScreenId = 100735,
                          GroupAreaName = "ViewPage",
                          GroupPropertyField = "NewVisitors",
                          GroupPropertyName = "NewVisitors",
                          IsSelected = false,
                          CreatedUser = 1000108974,
                          CreatedDate = null,
                          UpdatedUser = 1000108974,
                          UpdatedDate = null,
                          SelectOrderNumber = 2,
                          OrderNumberGroup = 2,
                          DataFormat = "text",
                          IsDisplay = null,
                          IsFix = null
                },
            new SYS_ReportScreenGroupProperty
                {
                          Id = 43,
                          StoreId = 0,
                          ReportScreenId = 100735,
                          GroupAreaName = "ViewPage",
                          GroupPropertyField = "TotalVisits",
                          GroupPropertyName = "TotalVisits",
                          IsSelected = false,
                          CreatedUser = 1000108974,
                          CreatedDate = null,
                          UpdatedUser = 1000108974,
                          UpdatedDate = null,
                          SelectOrderNumber = 3,
                          OrderNumberGroup = 3,
                          DataFormat = "text",
                          IsDisplay = null,
                          IsFix = null
                },
            new SYS_ReportScreenGroupProperty
                {
                          Id = 44,
                          StoreId = 0,
                          ReportScreenId = 100735,
                          GroupAreaName = "ViewPage",
                          GroupPropertyField = "Avgtime",
                          GroupPropertyName = "Avgtime",
                          IsSelected = false,
                          CreatedUser = 1000108974,
                          CreatedDate = null,
                          UpdatedUser = 1000108974,
                          UpdatedDate = null,
                          SelectOrderNumber = 4,
                          OrderNumberGroup = 4,
                          DataFormat = "text",
                          IsDisplay = null,
                          IsFix = null
                },
            new SYS_ReportScreenGroupProperty
                {
                          Id = 45,
                          StoreId = 0,
                          ReportScreenId = 100735,
                          GroupAreaName = "ViewPage",
                          GroupPropertyField = "Landings",
                          GroupPropertyName = "Landings",
                          IsSelected = false,
                          CreatedUser = 1000108974,
                          CreatedDate = null,
                          UpdatedUser = 1000108974,
                          UpdatedDate = null,
                          SelectOrderNumber = 5,
                          OrderNumberGroup = 5,
                          DataFormat = "text",
                          IsDisplay = null,
                          IsFix = null
                },
            new SYS_ReportScreenGroupProperty
                {
                          Id = 46,
                          StoreId = 0,
                          ReportScreenId = 100735,
                          GroupAreaName = "ViewPage",
                          GroupPropertyField = "Exits",
                          GroupPropertyName = "Exits",
                          IsSelected = false,
                          CreatedUser = 1000108974,
                          CreatedDate = null,
                          UpdatedUser = 1000108974,
                          UpdatedDate = null,
                          SelectOrderNumber = 6,
                          OrderNumberGroup = 6,
                          DataFormat = "text",
                          IsDisplay = null,
                          IsFix = null
                },
            new SYS_ReportScreenGroupProperty
                {
                          Id = 46,
                          StoreId = 0,
                          ReportScreenId = 100735,
                          GroupAreaName = "ViewPage",
                          GroupPropertyField = "Bounces",
                          GroupPropertyName = "Bounces",
                          IsSelected = false,
                          CreatedUser = 1000108974,
                          CreatedDate = null,
                          UpdatedUser = 1000108974,
                          UpdatedDate = null,
                          SelectOrderNumber = 7,
                          OrderNumberGroup = 7,
                          DataFormat = "text",
                          IsDisplay = null,
                          IsFix = null
                },
            new SYS_ReportScreenGroupProperty
                {
                          Id = 48,
                          StoreId = 0,
                          ReportScreenId = 100735,
                          GroupAreaName = "ViewPage",
                          GroupPropertyField = "Avgscroll",
                          GroupPropertyName = "Avgscroll",
                          IsSelected = false,
                          CreatedUser = 1000108974,
                          CreatedDate = null,
                          UpdatedUser = 1000108974,
                          UpdatedDate = null,
                          SelectOrderNumber = 8,
                          OrderNumberGroup = 8,
                          DataFormat = "text",
                          IsDisplay = null,
                          IsFix = null
                },

             new SYS_ReportScreenGroupProperty
                {
                          Id = 49,
                          StoreId = 0,
                          ReportScreenId = 100736,
                          GroupAreaName = "AnalyticsSession",
                          GroupPropertyField = "Day",
                          GroupPropertyName = "Day",
                          IsSelected = true,
                          CreatedUser = 1000108974,
                          CreatedDate = null,
                          UpdatedUser = 1000108974,
                          UpdatedDate = null,
                          SelectOrderNumber = 0,
                          OrderNumberGroup = 0,
                          DataFormat = "text",
                          IsDisplay = null,
                          IsFix = null
                },
             new SYS_ReportScreenGroupProperty
                {
                          Id = 50,
                          StoreId = 0,
                          ReportScreenId = 100736,
                          GroupAreaName = "AnalyticsSession",
                          GroupPropertyField = "TotalSessions",
                          GroupPropertyName = "TotalSessions",
                          IsSelected = false,
                          CreatedUser = 1000108974,
                          CreatedDate = null,
                          UpdatedUser = 1000108974,
                          UpdatedDate = null,
                          SelectOrderNumber = 1,
                          OrderNumberGroup = 1,
                          DataFormat = "text",
                          IsDisplay = null,
                          IsFix = null
                },
             new SYS_ReportScreenGroupProperty
                {
                          Id = 51,
                          StoreId = 0,
                          ReportScreenId = 100736,
                          GroupAreaName = "AnalyticsSession",
                          GroupPropertyField = "NewSessions",
                          GroupPropertyName = "NewSessions",
                          IsSelected = false,
                          CreatedUser = 1000108974,
                          CreatedDate = null,
                          UpdatedUser = 1000108974,
                          UpdatedDate = null,
                          SelectOrderNumber = 2,
                          OrderNumberGroup = 2,
                          DataFormat = "text",
                          IsDisplay = null,
                          IsFix = null
                },
             new SYS_ReportScreenGroupProperty
                {
                          Id = 52,
                          StoreId = 0,
                          ReportScreenId = 100736,
                          GroupAreaName = "AnalyticsSession",
                          GroupPropertyField = "UniqueSessions",
                          GroupPropertyName = "UniqueSessions",
                          IsSelected = false,
                          CreatedUser = 1000108974,
                          CreatedDate = null,
                          UpdatedUser = 1000108974,
                          UpdatedDate = null,
                          SelectOrderNumber = 3,
                          OrderNumberGroup = 3,
                          DataFormat = "text",
                          IsDisplay = null,
                          IsFix = null
                },

             new SYS_ReportScreenGroupProperty
                {
                          Id = 53,
                          StoreId = 0,
                          ReportScreenId = 100737,
                          GroupAreaName = "AnalyticsDurations",
                          GroupPropertyField = "TimeDurations",
                          GroupPropertyName = "TimeDurations",
                          IsSelected = true,
                          CreatedUser = 1000108974,
                          CreatedDate = null,
                          UpdatedUser = 1000108974,
                          UpdatedDate = null,
                          SelectOrderNumber = 0,
                          OrderNumberGroup = 0,
                          DataFormat = "text",
                          IsDisplay = null,
                          IsFix = null
                },
             new SYS_ReportScreenGroupProperty
                {
                          Id = 54,
                          StoreId = 0,
                          ReportScreenId = 100737,
                          GroupAreaName = "AnalyticsDurations",
                          GroupPropertyField = "UsersDurations",
                          GroupPropertyName = "UsersDurations",
                          IsSelected = false,
                          CreatedUser = 1000108974,
                          CreatedDate = null,
                          UpdatedUser = 1000108974,
                          UpdatedDate = null,
                          SelectOrderNumber = 1,
                          OrderNumberGroup = 1,
                          DataFormat = "text",
                          IsDisplay = null,
                          IsFix = null
                },
             new SYS_ReportScreenGroupProperty
                {
                          Id = 55,
                          StoreId = 0,
                          ReportScreenId = 100737,
                          GroupAreaName = "AnalyticsDurations",
                          GroupPropertyField = "PercentageDurations",
                          GroupPropertyName = "PercentageDurations",
                          IsSelected = false,
                          CreatedUser = 1000108974,
                          CreatedDate = null,
                          UpdatedUser = 1000108974,
                          UpdatedDate = null,
                          SelectOrderNumber = 2,
                          OrderNumberGroup = 2,
                          DataFormat = "text",
                          IsDisplay = null,
                          IsFix = null
                },

             new SYS_ReportScreenGroupProperty
                {
                          Id = 56,
                          StoreId = 0,
                          ReportScreenId = 100738,
                          GroupAreaName = "AnalyticsFrequency",
                          GroupPropertyField = "TimeFrequency",
                          GroupPropertyName = "TimeFrequency",
                          IsSelected = true,
                          CreatedUser = 1000108974,
                          CreatedDate = null,
                          UpdatedUser = 1000108974,
                          UpdatedDate = null,
                          SelectOrderNumber = 0,
                          OrderNumberGroup = 0,
                          DataFormat = "text",
                          IsDisplay = null,
                          IsFix = null
                },
             new SYS_ReportScreenGroupProperty
                {
                          Id = 57,
                          StoreId = 0,
                          ReportScreenId = 100738,
                          GroupAreaName = "AnalyticsFrequency",
                          GroupPropertyField = "UsersFrequency",
                          GroupPropertyName = "UsersFrequency",
                          IsSelected = false,
                          CreatedUser = 1000108974,
                          CreatedDate = null,
                          UpdatedUser = 1000108974,
                          UpdatedDate = null,
                          SelectOrderNumber = 1,
                          OrderNumberGroup = 1,
                          DataFormat = "text",
                          IsDisplay = null,
                          IsFix = null
                },
             new SYS_ReportScreenGroupProperty
                {
                          Id = 58,
                          StoreId = 0,
                          ReportScreenId = 100738,
                          GroupAreaName = "AnalyticsFrequency",
                          GroupPropertyField = "PercentageFrequency",
                          GroupPropertyName = "PercentageFrequency",
                          IsSelected = false,
                          CreatedUser = 1000108974,
                          CreatedDate = null,
                          UpdatedUser = 1000108974,
                          UpdatedDate = null,
                          SelectOrderNumber = 2,
                          OrderNumberGroup = 2,
                          DataFormat = "text",
                          IsDisplay = null,
                          IsFix = null
                },
        };

        // Danh sách thống kê
        public static List<SYS_ReportScreenMeasure> SYS_ReportScreenMeasure = new List<SYS_ReportScreenMeasure>
        {
            new SYS_ReportScreenMeasure
                {
                         Id =  400,
                         StoreId = 0,
                         ReportScreenId = 100735,
                         MeasureName = "ViewPage",
                         MeasureField = "ViewPage",
                         IsDefault = true,
                         IsMoney = false,
                         IsSelected = true,
                         SelectOrderNumber = 1,
                         CreatedUser = 200000013004,
                         CreatedDate = null,
                         UpdatedUser = 1000108974,
                         UpdatedDate = null,
                         DataFormat = 0,
                         ChartType = "line",
                         IsDraw = true,
                         IsFix = null
                },
            new SYS_ReportScreenMeasure
                {
                         Id =  401,
                         StoreId = 0,
                         ReportScreenId = 100736,
                         MeasureName = "AnalyticsSession",
                         MeasureField = "AnalyticsSession",
                         IsDefault = true,
                         IsMoney = false,
                         IsSelected = true,
                         SelectOrderNumber = 2,
                         CreatedUser = 200000013004,
                         CreatedDate = null,
                         UpdatedUser = 1000108974,
                         UpdatedDate = null,
                         DataFormat = 0,
                         ChartType = "line",
                         IsDraw = true,
                         IsFix = null
                },
            new SYS_ReportScreenMeasure
                {
                         Id =  402,
                         StoreId = 0,
                         ReportScreenId = 100737,
                         MeasureName = "AnalyticsDurations",
                         MeasureField = "AnalyticsDurations",
                         IsDefault = true,
                         IsMoney = false,
                         IsSelected = true,
                         SelectOrderNumber = 2,
                         CreatedUser = 200000013004,
                         CreatedDate = null,
                         UpdatedUser = 1000108974,
                         UpdatedDate = null,
                         DataFormat = 0,
                         ChartType = "line",
                         IsDraw = true,
                         IsFix = null
                },
            new SYS_ReportScreenMeasure
                {
                         Id =  403,
                         StoreId = 0,
                         ReportScreenId = 100738,
                         MeasureName = "AnalyticsFrequency",
                         MeasureField = "AnalyticsFrequency",
                         IsDefault = true,
                         IsMoney = false,
                         IsSelected = true,
                         SelectOrderNumber = 2,
                         CreatedUser = 200000013004,
                         CreatedDate = null,
                         UpdatedUser = 1000108974,
                         UpdatedDate = null,
                         DataFormat = 0,
                         ChartType = "line",
                         IsDraw = true,
                         IsFix = null
                },
        };

        public static List<SYS_ReportScreenFilterData> SYS_ReportScreenFilterData = new List<SYS_ReportScreenFilterData>
        {
            new SYS_ReportScreenFilterData
                {
                      Id = 400,
                      StoreId = 0,
                      FilterId = 1000007133,
                      FilterData = "2",
                      FilterDataDateTime = null,

                      CreatedUser = 200000013004,
                      CreatedDate = null,
                      UpdatedUser = 1000108974,
                      UpdatedDate = null,
                      OperatorSymbol = "eq"
                }
        };
    }
}