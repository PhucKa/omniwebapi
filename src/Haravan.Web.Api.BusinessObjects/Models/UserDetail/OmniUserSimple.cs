﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    public class OmniUserSimple
    {
        public long Id { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
    public partial class UserDetail
    {
        public long Id { get; set; }
        public long StoreId { get; set; }
        public string Email { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Phone { get; set; }
        public string Homepage { get; set; }
        public string Bio { get; set; }
        public bool IsReceiveImportantEmail { get; set; } = false;
        public bool IsOwner { get; set; } = false;
        public int AccountType { get; set; } = 1;
        public long? CreatedUser { get; set; }
        public string LastLoginIp { get; set; }
        public DateTime CreatedDate { get; set; }
        public long? UpdatedUser { get; set; }
        public DateTime UpdatedDate { get; set; }
        public bool IsDeleted { get; set; } = false;
        public int OmniAccountType { get; set; } = 1;
    }
}
