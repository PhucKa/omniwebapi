﻿using System;
using System.Collections.Generic;

namespace Haravan.Web.Api.BusinessObjects.Models.user
{
    public class OmniUserDetail
    {
        public long Id { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public bool IsOwner { get; set; }
        public List<GroupPermissionSimpleModel> GroupPermissions { get; set; }
    }

    public class GroupPermissionSimpleModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
    public class WebUserDetailModel
    {
        public long Id { get; set; }
        public string Bio { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Homepage { get; set; }
        public long User_Account_Owner { get; set; }
    }
    public class UserUpdateModel
    {
        public long Id { get; set; }
        public bool IsOwner { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Homepage { get; set; }
        public string Bio { get; set; }
        public bool IsReceiveImportantEmail { get; set; }
        public bool CurrentIsOwner { get; set; }
        public SimpleListModel IsLimitAdminAccess { get; set; }
        public string Password { get; set; }
        public string CurrentPassword { get; set; }
        public string VersionNo { get; set; }
        public List<LoginHistoryModel> RecentLoginHistory { get; set; }
        public List<SimpleListModel> AdminAccess { get; set; }
        public bool CanUpdateRole { get; set; }
        public bool CanChangePassword { get; set; }
        public object DevicePermission { get; set; }
    }
    public class SimpleListModel
    {
        public long Key { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public int Count { get; set; }
        public string Type { get; set; }
        public bool IsSelected { get; set; }
        public string ImageUrl { get; set; }
        public string VersionNo { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string SupplierName { get; set; }

        public List<SimpleListModel> Model { get; set; }
    }
    public class LoginHistoryModel
    {
        public DateTime Date { get; set; }
        public string Ip { get; set; }
        public string Isp { get; set; }
        public string Location { get; set; }
        public string App { get; set; }
        public string Platform { get; set; }
    }
}