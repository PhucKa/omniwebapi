﻿using BHN.SharedObject.EBSMessage;
using System;
using System.Runtime.Serialization;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    [DataContract]
    public class FileModel
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long RootFileId { get; set; }

        [DataMember]
        public string FileName { get; set; }

        [DataMember]
        public string FileContent { get; set; }

        [DataMember]
        public string Url { get; set; }

        [DataMember]
        public int Size { get; set; }

        [DataMember]
        public string Extension { get; set; }

        [DataMember]
        public FileType Type { get; set; }

        [DataMember]
        public bool IsDeleted { get; set; }

        [DataMember]
        public long ProductId { get; set; }

        [DataMember]
        public long ImageOrder { get; set; }

        [DataMember]
        public string Alt { get; set; }

        [DataMember]
        public bool IsFeatured { get; set; }

        [DataMember]
        public DateTime UpdatedDate { get; set; }

        [DataMember]
        public string NewFileName { get; set; }

        [DataMember]
        public long ThemeFileId { get; set; }

        [DataMember]
        public string ThemeFileVersionNo { get; set; }

        [DataMember]
        public long ThemeId { get; set; }

        [DataMember]
        public string ThemeName { get; set; }

        [DataMember]
        public string ThemeVersionNo { get; set; }

        [DataMember]
        public long ThemeChangeSet { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }

        [DataMember]
        public bool IsAttachToVariant { get; set; }

        [DataMember]
        public string ProductName { get; set; }

        [DataMember]
        public string Attactment { get; set; }
    }
}