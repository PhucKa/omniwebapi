﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    public class ListLong
    {
        public List<long> lstlong { get; set; } = new List<long>();
    }
}
