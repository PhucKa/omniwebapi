﻿using System.Collections.Generic;

namespace Haravan.Web.Api.BusinessObjects.Models.Filter
{
    public class FilterTab
    {
        public long Id { get; set; }
        public int ViewId { get; set; }
        public string TabName { get; set; }
        public string FreeText { get; set; }
        public bool IsSystemTab { get; set; }
        public int SysType { get; set; }
        public List<FilterTabField> Fields { get; set; }
    }
}