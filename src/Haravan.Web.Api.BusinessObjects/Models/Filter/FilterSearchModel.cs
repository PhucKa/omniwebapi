﻿using Haravan.Web.Api.BusinessObjects.Enums;
using System.Collections.Generic;

namespace Haravan.Web.Api.BusinessObjects.Models.Filter
{
    public class FilterSearchModel : FilterSearchModelBase
    {
        public SysView ViewId { get; set; }
        public List<string> SelectField { get; set; }
        public string SortFilterNestedPath { get; set; }
        public FilterSearchField SortFilterFields { get; set; }
    }

    public class FilterSearchModelBase
    {
        public string FreeText { get; set; }
        public string FilterExpression { get; set; }
        public string SortExpression { get; set; }
        public List<FilterSearchField> Fields { get; set; }
        public string SortFieldName { get; set; }
        public string SortType { get; set; }
        public FilterSearchPage Page { get; set; }
    }

    public class FilterSearchPage
    {
        public int currentPage { get; set; }
        public int pageSize { get; set; }
    }

    public class FilterSearchField
    {
        public string FieldName { get; set; }
        public bool HasOptions { get; set; }
        public bool IsNummeric { get; set; }
        public string NummericOperator { get; set; }
        public string NummericValue { get; set; }
        public string OptionValue { get; set; }
        public string OptionValueValue { get; set; }
    }
}