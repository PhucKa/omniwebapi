﻿namespace Haravan.Web.Api.BusinessObjects.Models.Filter
{
    public class FilterTabField
    {
        public long Id { get; set; }
        public long TabId { get; set; }
        public int FieldId { get; set; }
        public string NummericOperator { get; set; }
        public string NummericValue { get; set; }
        public int? OptionId { get; set; }
        public string OptionValue { get; set; }
        public string OptionValueValue { get; set; }
        public string DisplayData { get; set; }
    }
}