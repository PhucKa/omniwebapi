﻿namespace Haravan.Web.Api.BusinessObjects.Models.Notification
{
    public class NotificationModel
    {
        public int    View_type { get; set; }
        public string Title     { get; set; }
        public string Content   { get; set; }
        public string Button    { get; set; }
        public string Action    { get; set; }
        public string Alert     { get; set; }
    }
}