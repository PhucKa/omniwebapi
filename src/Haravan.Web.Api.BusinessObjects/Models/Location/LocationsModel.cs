﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    public class LocationsModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string LocationName { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string AddressCont { get; set; }
        public string Phone { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public int? CountryId { get; set; }
        public string CountryName { get; set; }
        public int? ProvinceId { get; set; }
        public string ProvinceName { get; set; }
        public string PostalZipCode { get; set; }
        public bool IsPimaryLocation { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsShippingLocation { get; set; }
        public bool IsUnavailableQty { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public int? DistrictId { get; set; }
        public int? WardId { get; set; }
        public string WardName { get; set; }
        public string WardCode { get; set; }
        public string AddressDisplay { get; set; }
        public string DistrictName { get; set; }
        public bool HoldStock { get; set; }
        public string FlexShipHubCode { get; set; }
        public string ProShipHubCode { get; set; }
        public string GHTKHubCode { get; set; }
        public string GHNHubCode { get; set; }
        public string GHNv3HubCode { get; set; }
        public string ShipChungHubCode { get; set; }
        public string ViettelPostHubCode { get; set; }
        public string DHLHubCode { get; set; }
        public string ViettelPost2018HubCode { get; set; }
        public List<long> UserIds { get; set; }
        public List<long> GroupIds { get; set; }
        public int TypeId { get; set; }
        public long? AreaId { get; set; }
        public string AreaName { get; set; }
    }
}
