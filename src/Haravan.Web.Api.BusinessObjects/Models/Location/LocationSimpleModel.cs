﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    public class LocationSimpleModel
    {
        public long Id { get; set; }
        public string LocationName { get; set; }
        public string Address { get; set; }
        public bool IsPimaryLocation { get; set; }
    }
}
