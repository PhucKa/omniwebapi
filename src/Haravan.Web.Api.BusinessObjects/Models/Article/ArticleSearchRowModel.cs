﻿using Haravan.Web.Api.BusinessObjects.Models.Common;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Haravan.Web.Api.BusinessObjects.Models.Article
{
    public class ArticleSearchRowModel
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public DateTime? published_at { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [DataMember]
        public long BlogId { get; set; }

        [DataMember]
        public string BlogTitle { get; set; }

        [DataMember]
        public long AuthorId { get; set; }

        [DataMember]
        public string AuthorName { get; set; }

        [DataMember]
        public bool IsVisible { get; set; }

        

        [DataMember]
        public List<string> Tags { get; set; }

        [DataMember]
        public long? ImageId { get; set; }

        [DataMember]
        public string ImageUrl { get; set; }

    }

    public class ArticleListModel
    {
        public long article_count { get; set; }
        public List<ArticleSearchRowModel> data { get; set; }
    }

    public class ArticlePublisRequest
    {
        public List<long> articlesIds { get; set; }
        public bool isPublish { get; set; }
    }
    public class ArticleAddPublisRequest
    {
        public long articlesIds { get; set; }
        public long blogId { get; set; }
    }

    public class BulkArticleTagRequest
    {
        public List<long> articlesIds { get; set; }
        public List<string> Tags { get; set; }
    }

    [DataContract]
    public class ArticleDetailDropdownlistModel
    {
        [DataMember]
        public long CurrentUser { get; set; }

        [DataMember]
        public List<DropdownlistModel> AuthorList { get; set; }

        [DataMember]
        public List<DropdownlistModel> BlogList { get; set; }

        [DataMember]
        public List<string> ThemeList { get; set; }
    }
    public class ArticleListModelInternal
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public DateTime? PublishedDate { get; set; }
        [DataMember]
        public long BlogId { get; set; }
        [DataMember]
        public string BlogTitle { get; set; }
        [DataMember]
        public long AuthorId { get; set; }
        [DataMember]
        public string AuthorName { get; set; }
        [DataMember]
        public bool IsVisible { get; set; }
      
        public DateTime? UpdatedDate { get; set; }
        [DataMember]
        public List<string> Tags { get; set; }
    }
}