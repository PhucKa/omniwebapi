﻿using Haravan.Web.Api.BusinessObjects.Models.Comment;
using Haravan.Web.Api.BusinessObjects.MongoModels;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Haravan.Web.Api.BusinessObjects.Models.Article
{
    [DataContract]
    public class ArticleDetailModel
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Content { get; set; }

        [DataMember]
        public string Excerpt { get; set; }

        [DataMember]
        public long? AuthorId { get; set; }

        [DataMember]
        public string AuthorName { get; set; }

        [DataMember]
        public long BlogId { get; set; }

        [DataMember]
        public string BlogTitle { get; set; }

        [DataMember]
        public string BlogHandleUrl { get; set; }

        [DataMember]
        public int BlogCommentRule { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime CreatedDate { get; set; }
        public long? Created_user { get; set; }

        public DateTime UpdatedDate { get; set; }

        [DataMember]
        public DateTime? PublishedDate { get; set; }

        

        [DataMember]
        public string PageTitle { get; set; }

        [DataMember]
        public string MetaDescription { get; set; }

        [DataMember]
        public string TemplateName { get; set; }

        [DataMember]
        public string Tags { get; set; }

        [DataMember]
        public string NewBlog { get; set; }

        [DataMember]
        public List<CommentListModel> CommentList { get; set; }

        [DataMember]
        public long? ImageId { get; set; }

        [DataMember]
        public string ImageUrl { get; set; }

        [DataMember]
        public string UrlHandle { get; set; }

        [DataMember]
        public string OldUrlHandle { get; set; }

        [DataMember]
        public bool IsChangeUrlRedirect { get; set; }

        [DataMember]
        public string ViewUrl { get; set; }

        [DataMember]
        public bool IsAddNew { get; set; }

        [DataMember]
        public string ImageAttactment { get; set; }

        [DataMember]
        public string ImageAttactmentFilename { get; set; }

        [DataMember]
        public bool IsSetPublishDate { get; set; }

        [DataMember]
        public bool IsVisible { get; set; }

        public MGUserModel user { get; set; }
    }

    public class ArticleDetail
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Content { get; set; }

        [DataMember]
        public string Excerpt { get; set; }

        [DataMember]
        public long? AuthorId { get; set; }

        [DataMember]
        public string AuthorName { get; set; }

        [DataMember]
        public long BlogId { get; set; }

        [DataMember]
        public string BlogTitle { get; set; }

        [DataMember]
        public string BlogHandleUrl { get; set; }

        [DataMember]
        public int BlogCommentRule { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime CreatedDate { get; set; }

        [DataMember]
        public DateTime? PublishedDate { get; set; }

       

        [DataMember]
        public string PageTitle { get; set; }

        [DataMember]
        public string MetaDescription { get; set; }

        [DataMember]
        public string TemplateName { get; set; }

        [DataMember]
        public string Tags { get; set; }

        [DataMember]
        public string NewBlog { get; set; }

        [DataMember]
        public List<CommentListModel> CommentList { get; set; }

        [DataMember]
        public long? ImageId { get; set; }

        [DataMember]
        public string ImageUrl { get; set; }

        [DataMember]
        public string UrlHandle { get; set; }

        [DataMember]
        public string OldUrlHandle { get; set; }

        [DataMember]
        public bool IsChangeUrlRedirect { get; set; }

        [DataMember]
        public string ViewUrl { get; set; }

        [DataMember]
        public bool IsAddNew { get; set; }

        [DataMember]
        public string ImageAttactment { get; set; }

        [DataMember]
        public string ImageAttactmentFilename { get; set; }

        [DataMember]
        public bool IsSetPublishDate { get; set; }

        [DataMember]
        public bool IsVisible { get; set; }
    }

    public class ArticleDetailModelInternal
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Content { get; set; }

        [DataMember]
        public string Excerpt { get; set; }

        [DataMember]
        public long? AuthorId { get; set; }

        [DataMember]
        public string AuthorName { get; set; }

        [DataMember]
        public long BlogId { get; set; }

        [DataMember]
        public string BlogTitle { get; set; }

        [DataMember]
        public string BlogHandleUrl { get; set; }

        [DataMember]
        public int BlogCommentRule { get; set; }

        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }

        [DataMember]
        public DateTime? PublishedDate { get; set; }

        [DataMember]
        public string PageTitle { get; set; }

        [DataMember]
        public string MetaDescription { get; set; }

        [DataMember]
        public string TemplateName { get; set; }

        [DataMember]
        public string Tags { get; set; }

        [DataMember]
        public string NewBlog { get; set; }

        [DataMember]
        public List<CommentListModel> CommentList { get; set; }

        [DataMember]
        public Nullable<long> ImageId { get; set; }

        [DataMember]
        public string ImageUrl { get; set; }

        [DataMember]
        public string UrlHandle { get; set; }

        [DataMember]
        public string OldUrlHandle { get; set; }

        [DataMember]
        public bool IsChangeUrlRedirect { get; set; }

        [DataMember]
        public string ViewUrl { get; set; }

        [DataMember]
        public bool IsAddNew { get; set; }

        [DataMember]
        public string ImageAttactment { get; set; }

        [DataMember]
        public string ImageAttactmentFilename { get; set; }

        [DataMember]
        public bool IsSetPublishDate { get; set; }

        [DataMember]
        public bool IsVisible { get; set; }
    }
}