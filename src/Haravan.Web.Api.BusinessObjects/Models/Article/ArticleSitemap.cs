﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Haravan.Web.Api.BusinessObjects.Models.Article
{
    public class ArticleSitemap
    {
        public long _id { get; set; }
        public DateTime created_at { get; set; }
        public string url { get; set; }
        public string title { get; set; }
        public string url_handle { get; set; }
        public DateTime? updated_at { get; set; }
        public DateTime? published_at { get; set; }
    }
}
