﻿using Haravan.Web.Api.BusinessObjects.ESModels;
using System.Collections.Generic;

namespace Haravan.Web.Api.BusinessObjects.Models.Article
{
    public class ArticleDetailApiModel
    {
        public long articleId { get; set; }
        public Dictionary<string, object> updatedData { get; set; }
    }

    public class ArticleTagsApiModel
    {
        public FilterSearchModel filter { get; set; }
        public bool popular { get; set; }
    }
    public class ArticleGetCommentsApiModel
    {
        public long articleId { get; set; }
        public bool isSpam { get; set; }
    }
}