﻿using Haravan.Web.Api.BusinessObjects.Enums;
using Haravan.Web.Api.BusinessObjects.ESModels;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    [DataContract]
    public class NavigationListModel
    {
        [DataMember]
        public List<LinkListModel> LinkLists { get; set; }

        [DataMember]
        public List<LinkListTempMolde> LinkListsTeam { get; set; }

        [DataMember]
        public List<UrlRedirectModel> UrlRedirectList { get; set; }
    }

    public class LinkListTempMolde
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string LinkListName { get; set; }

        [DataMember]
        public string LinkListHandle { get; set; }

        [DataMember]
        public string ParentLinkListName { get; set; }
    }

    public class NavigationDetailApiViewModel
    {
        public long id { get; set; }
        public Dictionary<string, object> dicUpdated { get; set; }
    }

    public class LoadDropdownlistViewModel
    {
        public FilterSearchModel FilterSearchModel { get; set; }
        public LinkFieldType linkFieldType { get; set; }
    }

    public class LoadDropdownlistViewModelNavigation
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string query { get; set; }
        public LinkFieldType linkFieldType { get; set; }
    }
}