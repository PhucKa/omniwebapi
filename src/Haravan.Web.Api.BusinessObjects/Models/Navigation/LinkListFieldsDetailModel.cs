﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    public class LinkListFieldsDetailModel
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long LinkListId { get; set; }

        [DataMember]
        public string LinkListFieldName { get; set; }

        [DataMember]
        public string LinkListFieldHandle { get; set; }

        [DataMember]
        public int LinkListFieldOrder { get; set; }

        [DataMember]
        public long RefId { get; set; }

        [DataMember]
        public long StoreId { get; set; }

        [DataMember]
        public string SelectedOption { get; set; }

        [DataMember]
        public string TextValue { get; set; }

        [DataMember]
        public string Tags { get; set; }

        [DataMember]
        public int LinkFieldsId { get; set; }

        [DataMember]
        public string LinkFieldsName { get; set; }

        [DataMember]
        public string LinkFieldsDisplay { get; set; }

        [DataMember]
        public bool LinkFieldsHasText { get; set; }

        [DataMember]
        public bool LinkFieldsHasOptions { get; set; }

        [DataMember]
        public bool LinkFieldsHasTags { get; set; }

        [DataMember]
        public string LinkFieldsOptionUrl { get; set; }

        [DataMember]
        public bool IsDeleted { get; set; }
        [DataMember]
        public string TreeHandle { get; set; }

    }
    public class LinkListFieldsDetailModelPath
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long LinkListId { get; set; }

        [DataMember]
        public string LinkListFieldName { get; set; }

        [DataMember]
        public string LinkListFieldHandle { get; set; }

        [DataMember]
        public int LinkListFieldOrder { get; set; }

        [DataMember]
        public long? RefId { get; set; }

        [DataMember]
        public long StoreId { get; set; }

        [DataMember]
        public string SelectedOption { get; set; }

        [DataMember]
        public string TextValue { get; set; }

        [DataMember]
        public string Tags { get; set; }

        [DataMember]
        public int LinkFieldsId { get; set; }

        [DataMember]
        public string LinkFieldsName { get; set; }

        [DataMember]
        public string LinkFieldsDisplay { get; set; }

        [DataMember]
        public bool LinkFieldsHasText { get; set; }

        [DataMember]
        public bool LinkFieldsHasOptions { get; set; }

        [DataMember]
        public bool LinkFieldsHasTags { get; set; }

        [DataMember]
        public string LinkFieldsOptionUrl { get; set; }

        [DataMember]
        public bool IsDeleted { get; set; }

        [DataMember]
        public List<long> Path { get; set; }
        [DataMember]
        public string TreeHandle { get; set; }
    }
}