﻿using Haravan.Web.Api.BusinessObjects.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    [DataContract]
    public class UrlRedirectModel
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string OldPath { get; set; }

        [DataMember]
        public string RedirectTo { get; set; }

        [DataMember]
        public string Url { get; set; }

        [DataMember]
        public string VersionNo { get; set; }
    }

    [DataContract]
    public class RedirectAPIModel
    {
        [DataMember]
        public long id { get; set; }

        [DataMember]
        [Editable(true)]
        public string path { get; set; }

        [DataMember]
        [Editable(true)]
        public string target { get; set; }
    }

    public class RedirectAPIModelRequest
    {
        public long id { get; set; }

        public Dictionary<string, object> dicUpdated { get; set; }
    }

    public class GetLinkListByRefId_ExceptHandle
    {
        public long refId { get; set; }
        public int type { get; set; }
    }

    public class Collection_UpdateForLinkUrlApiAsync
    {
        public long collectionId { get; set; }
        public bool isDeleteAction { get; set; }
    }
    public class Prouct_UpdateForLinkUrlApiAsync
    {
        public long productId { get; set; }
        public bool isDeleteAction { get; set; }
    }
    public class UpdateProductUrlRedirect
    {
        public string oldPath { get; set; }
        public string newPath { get; set; }
    }
    public class SetLinkByRefId
    {
        public long refId { get; set; }
        public string refTitle { get; set; }
        public List<NavigationDetailModel> linkLists { get; set; }
        public LinkFieldType type { get; set; }
    }

    public class OpenRedirectRequestModel
    {
        public Dictionary<string, object> dicData { get; set; }
    }
    public class SetLinkListBycollectionId
    {
        public long collectionId { get; set; }
        public string collectionTitle { get; set; }
        public List<NavigationDetailModel> linkLists { get; set; }
    }
}