﻿using System.Runtime.Serialization;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    [DataContract]
    public class LinkListFieldsModel
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public int LinkFieldsId { get; set; }

        [DataMember]
        public string LinkListFieldName { get; set; }

        [DataMember]
        public string LinkListFieldHandle { get; set; }

        [DataMember]
        public int LinkListFieldOrder { get; set; }

        [DataMember]
        public string TextValue { get; set; }

        [DataMember]
        public string VersionNo { get; set; }

        [DataMember]
        public bool HasDropdown { get; set; }

        [DataMember]
        public long? RefId { get; set; }
    }
}