﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    public class NavigationLogModel
    {
        public string Title { get; set; }

        public DateTime LogDate { get; set; }

        public string LogUser { get; set; }

        public string LinkDetail { get; set; }
    }

    [DataContract]
    public class NavigationDetailModel
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string LinkListName { get; set; }

        [DataMember]
        public string LinkListHandle { get; set; }

        [DataMember]
        public long ParentLinkId { get; set; }

        [DataMember]
        public bool IsSystemLink { get; set; }

        [DataMember]
        public bool IsDeleted { get; set; }


        [DataMember]
        public List<LinkListFieldsDetailModel> LinkListFields { get; set; }        
    }
    [DataContract]
    public class NavigationDetailModelPath
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string LinkListName { get; set; }

        [DataMember]
        public string LinkListHandle { get; set; }

        [DataMember]
        public long ParentLinkId { get; set; }

        [DataMember]
        public bool IsSystemLink { get; set; }

        [DataMember]
        public bool IsDeleted { get; set; }

        [DataMember]
        public List<LinkListFieldsDetailModelPath> LinkListFields { get; set; }
    }

    
}