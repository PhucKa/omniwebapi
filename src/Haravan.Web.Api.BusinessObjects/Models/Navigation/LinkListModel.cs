﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    [DataContract]
    public class LinkListModel
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string LinkListName { get; set; }

        [DataMember]
        public string LinkListHandle { get; set; }

        [DataMember]
        public string ParentLinkListName { get; set; }

        [DataMember]
        public bool IsSystemLink { get; set; }

        [DataMember]
        public string VersionNo { get; set; }

        [DataMember]
        public List<LinkListFieldsModel> LinkListFields { get; set; }
    }
}