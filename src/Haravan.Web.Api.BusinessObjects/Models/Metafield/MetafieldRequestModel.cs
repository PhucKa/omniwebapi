﻿using BHN.SharedObject.APIDataModel;
using BHN.SharedObject.EBSMessage;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    [DataContract]
    public class OmniUpdateMetafieldRequestModel
    {
        [DataMember]
        public Dictionary<string, object> dicUpdated { get; set; }
    }

    [DataContract]
    public class OmniCreateMetafieldRequestModel
    {
        [DataMember]
        public MetafieldAPIModel model { get; set; }

        [DataMember]
        public List<int> owner_resources { get; set; }

        [DataMember]
        public MetafieldValueType value_type { get; set; }
    }
}