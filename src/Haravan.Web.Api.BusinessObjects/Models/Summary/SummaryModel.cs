﻿namespace Haravan.Web.Api.BusinessObjects.Models.Summary
{
    public class Get_Summaries
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}