﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    public class CreateShopModel
    {
        public string email { get; set; }
        public string user_name { get; set; }
        public string shop_name { get; set; }
        public string salechannel { get; set; }
    }
}
