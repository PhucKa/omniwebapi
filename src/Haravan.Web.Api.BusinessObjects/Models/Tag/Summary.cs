﻿using System;

namespace Haravan.Web.Api.BusinessObjects.Models.Tag
{
    public class ArticleTags
    {
        public long Id { get; set; }
        public long StoreId { get; set; }
        public string Name { get; set; }
        public long RefId { get; set; }
        public int Type { get; set; }
        public long? CreatedUser { get; set; }
        public DateTime CreatedDate { get; set; }
        public long? UpdatedUser { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}