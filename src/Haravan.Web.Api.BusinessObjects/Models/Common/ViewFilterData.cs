﻿using System.Collections.Generic;

namespace Haravan.Web.Api.BusinessObjects.Models.Common
{
    public class ListLongModel
    {
        public List<long> values { get; set; }
    }

    public class ViewFilterData
    {
        public List<FilterTab> Tabs { get; set; }

        public List<FilterField> Fields { get; set; }
    }

    public class ViewFilterDataAll
    {
        public List<ViewFilterData> data { get; set; }
    }

    public class FilterTab
    {
        public long Id { get; set; }
        public int ViewId { get; set; }
        public string TabName { get; set; }
        public string FreeText { get; set; }
        public bool IsSystemTab { get; set; }
        public int SysType { get; set; }
        public List<FilterTabField> Fields { get; set; }
    }

    public class FilterTabField
    {
        public long Id { get; set; }
        public long TabId { get; set; }
        public int FieldId { get; set; }
        public string NummericOperator { get; set; }
        public string NummericValue { get; set; }
        public int? OptionId { get; set; }
        public string OptionValue { get; set; }
        public string OptionValueValue { get; set; }
        public string DisplayData { get; set; }
    }

    public class FilterField
    {
        public int Id { get; set; }
        public string FieldName { get; set; }
        public string FieldNameDisplay { get; set; }
        public string OperatorText { get; set; }
        public bool IsNummeric { get; set; }
        public bool HasOptions { get; set; }
        public string OptionsUrl { get; set; }

        public List<FilterFieldOption> Options { get; set; }
    }

    public class FilterFieldOption
    {
        public int Id { get; set; }

        public int FieldId { get; set; }

        public string Value { get; set; }

        public string DisplayValue { get; set; }

        public string Type { get; set; }

        public string DisplayText { get; set; }
    }
}