﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Haravan.Web.Api.BusinessObjects.Models.Common
{
    [DataContract]
    public class DropdownlistModel
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string UrlHandle { get; set; }

        [DataMember]
        public bool IsDisable { get; set; }
    }
    public class DropdownlistUserModel
    {
        public List<DropdownlistModel> data { get; set; }
        public long CurrentUser { get; set; }
    }
    public class OmniSimpleModel
    {
        [DataMember]
        public long id { get; set; }
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string address { get; set; }
        [DataMember]
        public bool? isPrimaryLocation { get; set; }
    }
    [DataContract]
    public class OmniBuildHandleModel
    {
        [DataMember]
        public long id { get; set; }
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string handle { get; set; }

    }
    public class CollectionsListModel
    {
        public long totalrecord { get; set; }
        public List<DropdownSimpleModel> data { get; set; }
    }
    public class CollectionListModelRequest
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string UrlHandle { get; set; }
        public bool IsManualSelect { get; set; }
        public bool Visibility { get; set; }
        public string ConditionDisplay { get; set; }
        public List<string> ListConditionDisplay { get; set; }
        public long ImageId { get; set; }
        public string ImageUrl { get; set; }
        public bool IsDeleted { get; set; }
    
    }
    public class ProductSearchRowModel
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string ProductName { get; set; }
        [DataMember]
        public string ProductTypeName { get; set; }
        [DataMember]
        public string VendorName { get; set; }
        [DataMember]
        public int TotalVariants { get; set; }
        [DataMember]
        public int? IsTracking { get; set; }
        [DataMember]
        public int TotalInventory { get; set; }
        [DataMember]
        public int IsVisible { get; set; }
        [DataMember]
        public bool IsDeleted { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public int TotalOnHand { get; set; }
        [DataMember]
        public int TotalCommited { get; set; }
        [DataMember]
        public int TotalIncoming { get; set; }
        [DataMember]
        public string ProductURL { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime UpdatedDate { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
        [DataMember]
        public bool Available { get; set; }
        [DataMember]
        public bool NotAllowPromotion { get; set; }
    }
    public class DropdownSimpleModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
    public class ThemeDropdownModel:DropdownSimpleModel
    {
        public string Handle { get; set; }
        public string ImageUrl { get; set; }
    }
    public class SimpleListModel
    {
        public long Key { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public int Count { get; set; }
        public string Type { get; set; }
        public bool IsSelected { get; set; }
        public string ImageUrl { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SupplierName { get; set; }

        public List<SimpleListModel> Model { get; set; }
    }
    
    public class LinkListRowModel
    {
        
        public long Id { get; set; }

        
        public string LinkListFieldName { get; set; }

        
        public int LinkListFieldOrder { get; set; }
    }
    public class LoadUpdateLinkOrder
    {
        public List<LinkListRowModel> listRow { get; set; }
        public long linkListId { get; set; }
    }
    public class LinkType
    {
        public const string Collection = "collection_link";
        public const string Http = "http_link";
        public const string Product = "product_link";
        public const string Relative = "relative_link";
        public const string Page = "page_link";
        public const string Blog = "blog_link";
    }
}