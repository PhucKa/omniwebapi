﻿using Haravan.Web.Api.BusinessObjects.Models.Common;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    [DataContract]
    public class BlogSearchRowModel
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string UrlHandle { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public List<string> Tags { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }

    public class BlogListModel
    {
        public long blog_count { get; set; }
        public List<BlogSearchRowModel> data { get; set; }
    }

    public class BlogDropdownlistModel
    {
        public List<DropdownlistModel> data { get; set; }
    }

    public class AuthorListDropdownlistModel
    {
        public List<DropdownlistModel> data { get; set; }
    }
}