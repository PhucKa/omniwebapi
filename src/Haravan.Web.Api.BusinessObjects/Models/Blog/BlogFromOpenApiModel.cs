﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    public class OpenApiBlogRequestModel
    {
        public Dictionary<string, object> dicData { get; set; }
    }
}
