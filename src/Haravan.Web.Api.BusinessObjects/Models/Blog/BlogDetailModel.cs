﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    public class BlogDetailEcomViewModel
    {
        public BlogDetailModel model { get; set; }
        public List<NavigationDetailModel> linkList { get; set; }
    }

    [DataContract]
    public class BlogDetailModel
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public int FeedBurnerId { get; set; }

        [DataMember]
        public string FeedBurnerValue { get; set; }

        [DataMember]
        public int CommentRule { get; set; }

        [DataMember]
        public string PageTitle { get; set; }

        [DataMember]
        public string MetaDescription { get; set; }

        [DataMember]
        public string UrlHandle { get; set; }

        [DataMember]
        public string TemplateName { get; set; }

        public bool IsDeleted { get; set; }

        [DataMember]
        public List<NavigationDetailModel> LinkList { get; set; }

        [DataMember]
        public List<MetafieldModel> metafields { get; set; }

        [DataMember]
        public string Descriptions { get; set; }

        public long? created_user { get; set; }
        public DateTime? created_at { get; set; }
    }

    [DataContract]
    public class MetafieldModel
    {
        [DataMember]
        public string key { get; set; }

        [DataMember]
        public string @namespace { get; set; }

        [DataMember]
        public string value { get; set; }

        [DataMember]
        public string value_type { get; set; }

        [DataMember]
        public string description { get; set; }
    }

    public class BlogDetail
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public int FeedBurnerId { get; set; }

        [DataMember]
        public string FeedBurnerValue { get; set; }

        [DataMember]
        public int CommentRule { get; set; }

        [DataMember]
        public string PageTitle { get; set; }

        [DataMember]
        public string MetaDescription { get; set; }

        [DataMember]
        public string UrlHandle { get; set; }

        [DataMember]
        public string Template_Suffix { get; set; }

        [DataMember]
        public List<NavigationDetailModel> LinkList { get; set; }

        [DataMember]
        public List<MetafieldModel> metafields { get; set; }

        [DataMember]
        public string Descriptions { get; set; }
    }

    public class BlogDetailViewModelInternal
    {
        public long Id { get; set; }

        public string Title { get; set; }

        public int FeedBurnerId { get; set; }

        public string FeedBurnerValue { get; set; }

        public int CommentRule { get; set; }

        public string PageTitle { get; set; }

        public string MetaDescription { get; set; }

        public string UrlHandle { get; set; }

        public string TemplateName { get; set; }

     

        public bool IsDeleted { get; set; }
        public List<NavigationDetailModel> LinkList { get; set; }
        public List<MetafieldModel> metafields { get; set; }

        public string Descriptions { get; set; }
    }

    public class BlogDetailModelToEcom
    {
        public List<string> listUrlHandle { get; set; }
    }

    public class BlogDetailInternalViewModel
    {
        public BlogDetailModel model { get; set; }
        public List<NavigationDetailModel> linkList { get; set; }
    }
}