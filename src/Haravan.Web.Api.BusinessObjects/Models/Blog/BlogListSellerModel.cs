﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    [DataContract]
    public class BlogListSellerModel
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string UrlHandle { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public List<string> Tags { get; set; }
 
    }
}
