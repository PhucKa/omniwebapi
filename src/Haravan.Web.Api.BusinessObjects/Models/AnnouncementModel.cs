﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    public class AnnouncementModel
    {
        public string announcement_type { get; set; }  // information (yellow), warning (orange), error(red)
        public string title { get; set; }
        public string content { get; set; }
        public string action { get; set; }  //button,link,noaction
        public string action_content { get; set; }
        public string action_link { get; set; }
        public string code { get; set; }
    }
    
}
