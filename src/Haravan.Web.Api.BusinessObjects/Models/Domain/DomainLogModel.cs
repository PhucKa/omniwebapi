﻿using System;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    public class DomainLogModel
    {
        public string Title { get; set; }

        public DateTime LogDate { get; set; }

        public string LogUser { get; set; }

        public string Ip { get; set; }
        public string App { get; set; }
        public string Platform { get; set; }
        public string Isp { get; set; }
        public string Location { get; set; }
    }
}