﻿using System;
using System.Runtime.Serialization;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    [DataContract]
    public class DomainRegisterModel
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string DomainName { get; set; }

        [DataMember]
        public string Extension { get; set; }

        [DataMember]
        public int ServicePackYear { get; set; }

        [DataMember]
        public decimal ServicePackPrice { get; set; }

        [DataMember]
        public decimal ServicePackPriceDiscount { get; set; }

        [DataMember]
        public int ServicePackDiscountPercent { get; set; }

        [DataMember]
        public string ContactName { get; set; }

        [DataMember]
        public string ContactIdentityNumber { get; set; }

        [DataMember]
        public DateTime? ContactBirthday { get; set; }

        [DataMember]
        public string ContactEmail { get; set; }

        [DataMember]
        public string ContactPhone { get; set; }

        [DataMember]
        public string ContactAddress { get; set; }

        [DataMember]
        public bool HideRegisterInfo { get; set; }

        [DataMember]
        public decimal? HideRegisterFee { get; set; }

        [DataMember]
        public decimal TotalMoneyDomain { get; set; }
    }
}