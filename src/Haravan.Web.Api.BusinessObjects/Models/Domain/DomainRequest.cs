﻿namespace Haravan.Web.Api.BusinessObjects.Models
{
    public class PrimaryDomainRequest
    {
        public long domainId { get; set; }
        public bool isRedirect { get; set; }
    }

    public class RemoveDomainRequest
    {
        public long domainId { get; set; }
        
    }

    public class AddDomainRequest
    {
        public string dmName { get; set; }
        public bool isHttps { get; set; }
    }

    public class VerifyToReclaimDomainRequest
    {
        public string domainName { get; set; }
        public bool isHttps { get; set; }
    }

    public class UpdateIsHttpsRequest
    {
        public long domainId { get; set; }
        public string name { get; set; }
        public bool isUseHttps { get; set; }
        public int sslType { get; set; }
    }

    public class RemoveHttps
    {
        public long domainId { get; set; }
        public string name { get; set; }
    }

    public class AddHttps
    {
        public long domainId { get; set; }
        public string name { get; set; }       
    }

    public class CheckValidDomainRequest
    {
        public string hostname { get; set; }
    }
}