﻿using BHN.SharedObject.EBSMessage;
using System;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    public class ShopPaymentTransaction
    {
        public long Id { get; set; }
        public string Transaction { get; set; }
        public decimal Cost { get; set; }
        public int Months { get; set; }
        public int Days { get; set; }
        public decimal Total { get; set; }
        public decimal TotalAmount { get; set; }
        public bool IsDisplayAmount { get; set; }
        public int ShopPaymentType { get; set; }
        public string ShopPaymentTypeCode { get; set; }
        public string ShopPaymentTypeName { get; set; }
        public int ShopPaymentMethodId { get; set; }
        public string ShopPaymentMethodName { get; set; }
        public string AdditionData { get; set; }
        public int AdditionType { get; set; }
        public int Status { get; set; }
        public string StatusName { get; set; }
        public PaymentExternalGateway? PaymentExternalGateway { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string Notes { get; set; }
        
        public PaymentBank? SelectPaymentBank { get; set; }
        public ShopPlanPaymentMethod? ShopPlanPaymentMethod { get; set; }

        public int ShopPlanId { get; set; }
        public string ShopPlanName { get; set; }

        public long AppId { get; set; }
        public string AppName { get; set; }
        public string AppApiKey { get; set; }

        public long? RefId { get; set; }
        public string DomainName { get; set; }

        public bool IsBonus { get; set; }
        public bool IsTrial { get; set; }
    }
}