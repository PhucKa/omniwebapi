﻿using System.Collections.Generic;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    public class DataDomainModel
    {
        public string IpAssigned { get; set; }
        public List<SYSDomainModel> ListDomain { get; set; }
    }

    public class CheckValidDomainModel
    {
        public int Status { get; set; }
        public bool IsError { get; set; }
    }
}