﻿using System;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    public class SYSDomainModel
    {
        public long Id { get; set; }
        public long StoreId { get; set; }
        public string Name { get; set; }
        public bool IsSubDomain { get; set; }
        public bool IsPrimary { get; set; }
        public bool IsHttps { get; set; }
        public bool IsRedirect { get; set; }
        public bool CanChangeDomain { get; set; }

        public bool IsChild { get; set; }
        public bool ParentHttps { get; set; }
        public int SslType { get; set; }
        public int TotalDays { get; set; }
        public DateTime? LastRefreshAt { get; set; }
        public long? CertInfoId { get; set; }
        public bool isPointed { get; set; }
    }
}