﻿using System.Collections.Generic;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    public class DomainPriceModel
    {
        public long Id { get; set; }
        public string Extension { get; set; }
        public decimal Price { get; set; }
        public List<DomainPriceDiscountModel> Discount { get; set; }
    }

    public class DomainPriceDiscountModel
    {
        public int Year { get; set; }
        public int DiscountPercent { get; set; }
    }
}