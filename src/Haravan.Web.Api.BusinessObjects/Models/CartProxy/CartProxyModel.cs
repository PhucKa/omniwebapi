using System.Runtime.Serialization;

namespace Haravan.Web.Api.BusinessObjects
{
    [DataContract]
    public class CartProxyModel
    {
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string AppApiKey { get; set; }
        [DataMember]
        public string AppSecretKey { get; set; }
        [DataMember]
        public long AppId { get; set; }
    }
}