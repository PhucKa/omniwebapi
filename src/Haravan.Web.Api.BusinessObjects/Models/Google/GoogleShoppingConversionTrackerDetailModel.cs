﻿using System;

namespace Haravan.Web.Api.BusinessObjects.Models.Google
{
    public class GoogleShoppingConversionTrackerDetailApiModel
    {
        public long id { get; set; }
        public string google_shopping_site_tag { get; set; }
        public string google_event_snippet { get; set; }
        public string merchant_id { get; set; }
        public string adwords_id { get; set; }
        public int conversion_type { get; set; } 
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
    }
}