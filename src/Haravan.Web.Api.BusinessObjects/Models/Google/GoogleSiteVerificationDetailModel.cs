﻿using System;

namespace Haravan.Web.Api.BusinessObjects.Models.Google
{
    public class GoogleSiteVerificationDetailApiModel
    {
        public long id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public string verification_code { get; set; }
        public string merchant_id { get; set; }
        public bool is_verify { get; set; }
    }
}