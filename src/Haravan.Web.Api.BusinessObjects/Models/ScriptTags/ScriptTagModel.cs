﻿using BHN.SharedObject.APIDataModel;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Haravan.Web.Api.BusinessObjects
{
    [DataContract]
    public class ScriptTagUpdateRequest
    {
        [DataMember]
        public ScriptTagAPIModel updated_model { get; set; }
        [DataMember]
        public List<string> updated_fields { get; set; }
    }
    [DataContract]
    public class ScriptTagModel
    {
        [DataMember]
        public string EventName { get; set; }
        [DataMember]
        public string Src { get; set; }
        [DataMember]
        public string ScriptName { get; set; }
        [DataMember]
        public string FileContent { get; set; }
        [DataMember]
        public bool IsSrc { get; set; }
        [DataMember]
        public int DisplayScope { get; set; }
    }
}
