﻿using BHN.SharedObject.EBSMessage;
using System;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    public class ThemeFileModel
    {
        public long Id { get; set; }
        public long ThemeId { get; set; }
        public long RootFileId { get; set; }
        public string FileName { get; set; }
        public bool CanBeDeleted { get; set; }
        public string VersionNo { get; set; }
        public string Type { get; set; }
        public FileModel File { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long ThemeChangeSet { get; set; }
    }

    public class RenameThemeFileRequest
    {
        public long Id { get; set; }
        public string NewFileName { get; set; }
        public string FileName { get; set; }
        public long ThemeId { get; set; }
        public FileType Type { get; set; }
        public long RootFileId { get; set; }
    }

    public class FileModelRequest
    {
        public long Id { get; set; }
        public long RootFileId { get; set; }
        public string FileName { get; set; }
        public string NewFileName { get; set; }
        public string VersionNo { get; set; }
        public FileType Type { get; set; }
    }

    public class DuplicateThemeModel
    {
        public long themeId { get; set; }
        public string prefixText { get; set; }
    }
}