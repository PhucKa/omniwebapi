﻿using System;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    public class Themes
    {
        public long Id { get; set; }
        public long StoreId { get; set; }
        public string Name { get; set; }
        public string Descriptions { get; set; }
        public int Type { get; set; } = 0;
        public bool IsPublished { get; set; } = false;
        public bool IsImporting { get; set; } = false;
        public bool IsDeleted { get; set; } = false;
        public long? DefaultLocaleRootFileId { get; set; }
        public string DefaultImageIds { get; set; }
        public long? DefaultThemeId { get; set; }
        public string DefaultCategories { get; set; }
        public string SiteDemoUrl { get; set; }
        public string DefaultImageUrls { get; set; }
        public long ChangeSet { get; set; } = 0;
        public long? CreatedUser { get; set; }
        public DateTime CreatedDate { get; set; }
        public long? UpdatedUser { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string InstallHash { get; set; }
    }

    public partial class ThemeFiles
    {
        public long Id { get; set; }
        public long StoreId { get; set; }
        public long ThemeId { get; set; }
        public long RootFileId { get; set; }
        public string FileName { get; set; }
        public bool CanBeDeleted { get; set; } = true;
        public bool IsDeleted { get; set; } = false;
        public long? CreatedUser { get; set; }
        public DateTime CreatedDate { get; set; }
        public long? UpdatedUser { get; set; }
        public DateTime UpdatedDate { get; set; }

        public Files Files { get; set; }
        public Themes Themes { get; set; }
    }

    public partial class Files
    {
        public long Id { get; set; }
        public long StoreId { get; set; }
        public long RootFileId { get; set; }
        public string FileName { get; set; }
        public string Url { get; set; }
        public int Size { get; set; }
        public int Type { get; set; }
        public string Extension { get; set; }
        public bool IsDeleted { get; set; } = false;
        public long? CreatedUser { get; set; }
        public DateTime CreatedDate { get; set; }
        public long? UpdatedUser { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string Alt { get; set; }
    }
}