﻿using System.Collections.Generic;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    public class ThemeListModel
    {
        public bool CanAddNew { get; set; }
        public List<ThemeModel> ThemeList { get; set; }
        public bool HasImportError { get; set; }
    }
}