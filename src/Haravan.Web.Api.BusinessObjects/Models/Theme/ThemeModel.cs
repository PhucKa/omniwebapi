﻿using BHN.SharedObject.EBSMessage;
using System.Collections.Generic;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    public class ThemeModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Descriptions { get; set; }
        public string Type { get; set; }
        public bool IsPublished { get; set; }
        public bool IsImporting { get; set; }
        public string VersionNo { get; set; }
        public List<ThemeFileModel> ThemeFileLayout { get; set; }
        public List<ThemeFileModel> ThemeFileTemplate { get; set; }
        public List<ThemeFileModel> ThemeFileSnippet { get; set; }
        public List<ThemeFileModel> ThemeFileAsset { get; set; }
        public List<ThemeFileModel> ThemeFileConfig { get; set; }
        public List<ThemeFileModel> ThemeFileLocale { get; set; }
        public List<string> NewFileTemplate { get; set; }
        public ThemeFileModel ThemeFileSetting { get; set; }
        public ThemeFileModel ThemeFileSettingData { get; set; }
        public string DefaultImageIds { get; set; }
        public List<string> DefaultImageUrls { get; set; }
        public List<string> DefaultCategories { get; set; }
        public string SiteDemoUrl { get; set; }
        public long ChangeSet { get; set; }
        public long? DefaultLocaleRootFileId { get; set; }
        public bool IsDeleted { get; set; }
        public bool HasAnyLocale { get; set; }
        public bool IsSettingsSchema { get; set; }
    }

    public class ThemeModelWebApi
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Descriptions { get; set; }
        public string Type { get; set; }
        public bool IsPublished { get; set; }
        public bool IsImporting { get; set; }
        public string VersionNo { get; set; }
        public List<ThemeFileModel> ThemeFile { get; set; }
        public List<string> NewFileTemplate { get; set; }
        public ThemeFileModel ThemeFileSetting { get; set; }
        public ThemeFileModel ThemeFileSettingData { get; set; }
        public string DefaultImageIds { get; set; }
        public List<string> DefaultImageUrls { get; set; }
        public List<string> DefaultCategories { get; set; }
        public string SiteDemoUrl { get; set; }
        public long ChangeSet { get; set; }
        public long? DefaultLocaleRootFileId { get; set; }
        public bool IsDeleted { get; set; }
        public bool HasAnyLocale { get; set; }
    }

    public class ThemeNameModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }

    public class ThemeFileModelRequest
    {
        public long Id { get; set; }
        public long ThemeId { get; set; }
        public long RootFileId { get; set; }
        public string NewContent { get; set; }
        public string FileName { get; set; }
        public string VersionNo { get; set; }
        public FileType Type { get; set; }
        public string Extension { get; set; }
    }

    public class ThemePublishModelList
    {
        public long Id { get; set; }
    }

    public class ThemePublishModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public bool IsPublished { get; set; }
    }

    public class ThemeListeFileForEdit
    {
        public long id { get; set; }
        public ThemeFileType themeFileType { get; set; }
    }

    public class ThemeListeFileForSave
    {
        public ThemeFileModel themeFile { get; set; }
        public string newConten { get; set; }
    }

    public class ThemeFileModelAdd
    {
        public ThemeFileModel oldThemeFile { get; set; }
        public ThemeFileModel themeFile { get; set; }
    }

    public class ThemeFileModelAddRequest
    {
        public long Id { get; set; }
        public long ThemeId { get; set; }
        public string FileName { get; set; }
        public string Type { get; set; }
    }

    public class GetThemeFileContentModel
    {
        public long themeId { get; set; }
        public string url { get; set; }
    }

    public class GetDataFromFileId
    {
        public long themeId { get; set; }
        public long fileId { get; set; }
    }

    public class UploadThemeFileModel
    {
        public long themeId { get; set; }
        public string versionNo { get; set; }
        public string name { get; set; }
    }

    public class ValidateUploadThemeAssetModel
    {
        public long themeId { get; set; }
        public string name { get; set; }
    }

    public class ThemesFree
    {
        public string name { get; set; }
        public string code { get; set; }
        public long id { get; set; }
        public string image { get; set; }
        public string link_install { get; set; }
        public string link_demo { get; set; }
    }

    public class FreeThemesModel
    {
        public long Id { get; set; }
        public long StoreId { get; set; }
        public string Name { get; set; }

        public int Type { get; set; } = 0;
        public bool IsPublished { get; set; } = false;

        public bool IsDeleted { get; set; } = false;
        public long? DefaultLocaleRootFileId { get; set; }
        public string DefaultImageIds { get; set; }
        public long? DefaultThemeId { get; set; }
        public string DefaultCategories { get; set; }
        public bool IsImporting { get; set; } = false;
        public string InstallHash { get; set; }
    }

    public class FileVersionThemeFileModel
    {
        public long Id { get; set; }
        public long RootFileId { get; set; }
    }

    public class GetThemeFileUploadModel
    {
        public long themeId { get; set; }
        public FileModel[] fileList { get; set; }
    }

    public class ListUrlHandleModel
    {
        public List<string> listUrlHandle { get; set; }
    }

    public class GetDataFromUrlModel
    {
        public string url { get; set; }
        public bool? is_setting_html { get; set; }
    }

    public class LoadLocaleContentModel
    {
        public string url { get; set; }
        public bool? is_setting_html { get; set; }
        public string Nametheme { get; set; }
    }

    public class SettingDataContentModel
    {
        public string content { get; set; }
    }

    public class OpenThemeRequestModel
    {
        public Dictionary<string, object> dicData { get; set; }
    }

    public class ModelDeleteAssetApi
    {
        public int fileType { get; set; }
        public string fileName { get; set; }
    }

    public class SaveLocaleThemeFileModelRequest
    {
        public ThemeFileModel themeFile { get; set; }
        public string newContent { get; set; }
    }

    public class LoadCurrentLocaleModel
    {
        public long id { get; set; }
        public string locale { get; set; }
    }
}