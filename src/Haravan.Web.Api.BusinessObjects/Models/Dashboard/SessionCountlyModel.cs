﻿using System.Collections.Generic;

namespace Haravan.Web.Api.BusinessObjects.Models
{
    public class RetrieveSessionCountlyModel
    {
        public string Id { get; set; }
        public long RequestsToSever { get; set; }
        public long NewSessions { get; set; }
        public long TotalSessions { get; set; }
        public long UniqueLocations { get; set; }
        public long TotalSessionDuration { get; set; }
        public long Durations { get; set; }
        public long Loyalty { get; set; }
        public long Frequency { get; set; }
    }

    public class DataAnalyticsSessionModel
    {
        public List<RetrieveSessionCountlyModel> data { get; set; }
        public long CountTotalSessions { get; set; }
        public List<CharDataModel> DataChar { get; set; }
    }

    public class DashboardAnalyticsViewVisitPage
    {
        public List<CharDataModel> TotalVisitors { get; set; }
        public List<CharDataModel> NewVisitors { get; set; }
        public List<CharDataModel> TotalVisits { get; set; }
    }

    public class DashboardAnalyticsSession
    {
        public List<CharDataModel> TotalUsers { get; set; }
        public List<CharDataModel> TotalSessions { get; set; }
        public List<CharDataModel> Newusers { get; set; }
        public long CountTotalUsers { get; set; }
        public long CountTotalSessions { get; set; }
        public long CountNewusers { get; set; }
        public long CountTotal { get; set; }
    }

    public class CharDataModel
    {
        public string Id { get; set; }
        public long Data { get; set; }
    }

    public class DashboardAnalyticsDurationsModel
    {
        public List<AnalyticsDurationsModel> Data { get; set; }
        public List<CharDataModel> DataChar { get; set; }
        public long TotalUsers { get; set; }
    }

    public class DashboardAnalyticsFrequencyModel
    {
        public List<AnalyticsFrequencyModel> Data { get; set; }
        public List<CharDataModel> DataChar { get; set; }
        public long TotalUsers { get; set; }
    }

    public class DashboardAnalyticsLocationModel
    {
        public CitiesModelCountLy Data { get; set; }
        public List<CharDataModel> DataChar { get; set; }
        public long TotalUsers { get; set; }
    }

    public class DashboardAnalyticsPlatformModel
    {
        public List<AnalyticsPlatformCountlyAPIModel> Data { get; set; }
        public List<CharDataModel> DataChar { get; set; }
        public long TotalUsers { get; set; }
    }

    public class AnalyticsSessionCountlyModel
    {
        public string Id { get; set; }
        public long TotalUser { get; set; }
        public long TotalSessions { get; set; }
        public long NewUsers { get; set; }
        public long TotalSessionDuration { get; set; }
        public long TotalAPIRequests { get; set; }
    }

    public class AnalyticsSessionNewUsers : RetrieveSessionCountlyAPIModel
    {
    }

    public class AnalyticsSessionTotalSessions : RetrieveSessionCountlyAPIModel
    {
    }

    public class AnalyticsSessionTotalUsers : RetrieveSessionCountlyAPIModel
    {
    }

    public class RetrieveSessionCountlyAPIModel
    {
        public string _id { get; set; }
        public int t { get; set; }
        public int n { get; set; }
        public int u { get; set; }
        public int d { get; set; }
        public int ds { get; set; }
        public int f { get; set; }
        public int l { get; set; }
        public int e { get; set; }
    }

    public class ViewPageTableRequest
    {
        public long org { get; set; }
        public int sEcho { get; set; }
        public int iColumns { get; set; }
        public string sColumns { get; set; }
        public int iDisplayStart { get; set; }
        public int iDisplayLength { get; set; }
        public string mDataProp_0 { get; set; }
        public string mDataProp_1 { get; set; }
        public string mDataProp_2 { get; set; }
        public string mDataProp_3 { get; set; }
        public string mDataProp_4 { get; set; }
        public string mDataProp_5 { get; set; }
        public string mDataProp_6 { get; set; }
        public string mDataProp_7 { get; set; }
        public string mDataProp_8 { get; set; }
        public string mDataProp_9 { get; set; }
        public string sSearch { get; set; }
        public bool bRegex { get; set; }
        public string sSearch_0 { get; set; }
        public bool bRegex_0 { get; set; }
        public bool bSearchable_0 { get; set; }
        public string sSearch_1 { get; set; }
        public bool bRegex_1 { get; set; }
        public bool bSearchable_1 { get; set; }
        public string sSearch_2 { get; set; }
        public bool bRegex_2 { get; set; }
        public bool bSearchable_2 { get; set; }
        public string sSearch_3 { get; set; }
        public bool bRegex_3 { get; set; }
        public bool bSearchable_3 { get; set; }
        public string sSearch_4 { get; set; }
        public bool bRegex_4 { get; set; }
        public bool bSearchable_4 { get; set; }
        public string sSearch_5 { get; set; }
        public bool bRegex_5 { get; set; }
        public bool bSearchable_5 { get; set; }
        public string sSearch_6 { get; set; }
        public bool bRegex_6 { get; set; }
        public bool bSearchable_6 { get; set; }
        public string sSearch_7 { get; set; }
        public bool bRegex_7 { get; set; }
        public bool bSearchable_7 { get; set; }
        public string sSearch_8 { get; set; }
        public bool bRegex_8 { get; set; }
        public bool bSearchable_8 { get; set; }
        public string sSearch_9 { get; set; }
        public bool bRegex_9 { get; set; }
        public bool bSearchable_9 { get; set; }
        public int iSortCol_0 { get; set; }
        public string sSortDir_0 { get; set; }
        public int iSortingCols { get; set; }
        public bool bSortable_0 { get; set; }
        public bool bSortable_1 { get; set; }
        public bool bSortable_2 { get; set; }
        public bool bSortable_3 { get; set; }
        public bool bSortable_4 { get; set; }
        public bool bSortable_5 { get; set; }
        public bool bSortable_6 { get; set; }
        public bool bSortable_7 { get; set; }
        public bool bSortable_8 { get; set; }
        public bool bSortable_9 { get; set; }
        public string period { get; set; }
    }

    public class AnalyticsSessionCountlyAPIModel
    {
        public string _id { get; set; }
        public int t { get; set; }
        public int n { get; set; }
        public int u { get; set; }
        public int d { get; set; }
        public int e { get; set; }
    }

    public class AnalyticsDurationsCountlyAPIModel
    {
        public string ds { get; set; }
        public string t { get; set; }
        public string percent { get; set; }
    }

    public class AnalyticsDurationsModel
    {
        public string TimeDurations { get; set; }
        public int UsersDurations { get; set; }
        public string PercentageDurations { get; set; }
    }

    public class AnalyticsFrequencyModel
    {
        public string TimeFrequency { get; set; }
        public int UsersFrequency { get; set; }
        public string PercentageFrequency { get; set; }
    }

    public class AnalyticsFrequencyCountlyAPIModel
    {
        public string f { get; set; }
        public int t { get; set; }
        public string percent { get; set; }
    }

    public class AnalyticsPlatformCountlyAPIModel
    {
        public string name { get; set; }
        public int value { get; set; }
        public int percent { get; set; }
    }

    public class CitiesModelCountLy
    {
        public List<string> Cities { get; set; }
        public List<Provincial> Provincial { get; set; }
    }

    public class Provincial
    {
        public string Name { get; set; }
        public int n { get; set; }
        public int t { get; set; }
        public int u { get; set; }
    }
}