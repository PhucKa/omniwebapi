﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Haravan.Web.Api.BusinessObjects.Models.Comment
{
    public class CommentDetailModel
    {
        public long ArticleId { get; set; }
        public string ArticleTitle { get; set; }
        public long BlogId { get; set; }
        public string BlogTitle { get; set; }
        public string Comment { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatorEmail { get; set; }
        public string CreatorName { get; set; }
        public long Id { get; set; }
        public int Status { get; set; }
        public string StatusName { get; set; }
        public bool IsDeleted { get; set; }

        public string BrowserIP { get; set; }
        public string UserAgent { get; set; }
    }

    public class CreateCommentModel
    {
        public Dictionary<string, object> dicInserted { get; set; }
        public CommentDetailModel model { get; set; }
    }

    public class UpdateCommentModel
    {
        public long commentId { get; set; }
        public Dictionary<string, object> dicUpdated { get; set; }
    }

    public class UpdateCommentStatus
    {
        public long commentId { get; set; }
        public int status { get; set; }
    }

    public class UpdateCommentDeleted
    {
        public long commentId { get; set; }
        public bool isDeleted { get; set; }
    }

    [DataContract]
    public class CommentListModel
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long Articleid { get; set; }

        [DataMember]
        public long BlogId { get; set; }

        [DataMember]
        public string ArticleTitle { get; set; }

        [DataMember]
        public string BlogTitle { get; set; }

        [DataMember]
        public string Author { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Body { get; set; }

        [DataMember]
        public string Rrl { get; set; }

        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public string StatusName { get; set; }

        [DataMember]
        public string Content { get; set; }

        [DataMember]
        public DateTime Created_at { get; set; }

        [DataMember]
        public DateTime Update_at { get; set; }

        [DataMember]
        public DateTime Publich_at { get; set; }

        [DataMember]
        public bool Isdeleted { get; set; }

        [DataMember]
        public string User_agent { get; set; }

        [DataMember]
        public bool From_api { get; set; }

        public string browseriP { get; set; }
    }

    public class CommentListModelSeller
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long ArticleId { get; set; }

        [DataMember]
        public long BlogId { get; set; }

        [DataMember]
        public string ArticleTitle { get; set; }

        [DataMember]
        public string BlogTitle { get; set; }

        [DataMember]
        public string Comment { get; set; }

        [DataMember]
        public string CreatorName { get; set; }

        [DataMember]
        public string CreatorEmail { get; set; }

        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public string StatusName { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }

        [DataMember]
        public DateTime CreatedAt { get; set; }

        [DataMember]
        public bool IsDeleted { get; set; }

        [DataMember]
        public string UserAgent { get; set; }

        [DataMember]
        public string BrowserIP { get; set; }

        [DataMember]
        public DateTime UpdatedDate { get; set; }
    }

    public class CommentUpdateModel
    {
        public long[] ids { get; set; }
        public int status { get; set; }
    }

    public class CommentDeleteModel
    {
        public long[] ids { get; set; }
    }

    public class OpenApiCommentRequestModel
    {
        public Dictionary<string, object> dicData { get; set; }
    }
}