﻿using Haravan.Libs.Abstractions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;

namespace Haravan.Web.Api.BusinessObjects
{
    public interface IInternalApiContext
    {
        #region request information

        HttpContext Context { get; set; }

        bool IsError { get; }
        IReadOnlyDictionary<string, string> Errors { get; }
        IReadOnlyDictionary<string, string> Warnings { get; }
        IReadOnlyDictionary<string, string> Success { get; }
        void AddSuccess(string msg);
        void AddSuccess(string key, string msg);
        void AddError(string msg);
        void AddError(string key, string msg);
        void AddInfo(string msg);
        void AddInfo(string key, string msg);
        void AddWarning(string msg);
        void AddWarning(string key, string msg);
        List<object> GetInfos();
        List<object> GetErrors();
        void ClearError();

        #endregion

        long? OrgId { get; set; }
        long? UserId { get; set; }
        string UserName { get; set; }
        string UserEmail { get; set; }

    }

    public class InternalApiContext : IInternalApiContext
    {


        public InternalApiContext()
        {
            this.Context = Context;
            errors = new Dictionary<string, string>();
            warnings = new Dictionary<string, string>();
            infos = new Dictionary<string, string>();
            success = new Dictionary<string, string>();
        }

        public HttpContext Context { get; set; }

        #region request information
        public Dictionary<string, string> errors { get; }
        public Dictionary<string, string> warnings { get; }
        public Dictionary<string, string> infos { get; }
        public Dictionary<string, string> success { get; }

        public IReadOnlyDictionary<string, string> Errors => errors;
        public IReadOnlyDictionary<string, string> Warnings => warnings;
        public IReadOnlyDictionary<string, string> Success => success;

        public bool IsError { get { return errors.Any(); } }
        public bool ISWarning { get { return warnings.Any(); } }
        public bool IsInfo { get { return infos.Any(); } }

        public void AddError(string msg)
        {
            errors.Add(Guid.NewGuid().ToString("n"), msg);
        }
        public void AddError(string key, string msg)
        {
            errors.Add(key, msg);
        }
        public void AddSuccess(string msg)
        {
            success.Add(Guid.NewGuid().ToString("n"), msg);
        }
        public void AddSuccess(string key, string msg)
        {
            success.Add(key, msg);
        }
        public void AddInfo(string msg)
        {
            infos.Add(Guid.NewGuid().ToString("n"), msg);
        }
        public void AddInfo(string key, string msg)
        {
            infos.Add(key, msg);
        }
        public void AddWarning(string msg)
        {
            warnings.Add(Guid.NewGuid().ToString("n"), msg);
        }
        public void AddWarning(string key, string msg)
        {
            warnings.Add(key, msg);
        }
        public List<object> GetInfos()
        {
            if (this.IsInfo)
                return infos.Select(m => m.Value).ToList<object>();
            return null;
        }
        public List<object> GetErrors()
        {
            if (this.IsError)
                return errors.Select(m => m.Value).ToList<object>();
            return null;
        }
        public void ClearError()
        {
            if (this.errors != null && this.errors.Any())
            {
                this.errors.Clear();
            }
        }
        #endregion

        private const string HeaderStoreId = "X-ECOM-STORE";
        private const string HeaderUserId = "X-ECOM-USER";
        private const string HeaderUserName = "X-ECOM-USERNAME";
        private const string HeaderUserEmail = "X-ECOM-USEREMAIL";



        public long? OrgId
        {
            get
            {
                var data = GetHeaderValueAs<string>(HeaderStoreId);

                if (!long.TryParse(data, out long storeId))
                    return null;

                return storeId;
            }
            set
            {
                this.OrgId = value;
            }
        }




        public long? UserId
        {
            get
            {
                var data = GetHeaderValueAs<string>(HeaderUserId);


                if (!long.TryParse(data, out long userId))
                    return 0;

                return userId;
            }
            set
            {
                this.UserId = value;
            }
        }
        public string UserName
        {
            get
            {
                var userName = GetHeaderValueAs<string>(HeaderUserName);
                if (!string.IsNullOrEmpty(userName))
                    userName = WebUtility.UrlDecode(userName);
                return userName;
            }
            set
            {
                this.UserName = value;
            }
        }
        public string UserEmail
        {
            get
            {
                var userEmail = GetHeaderValueAs<string>(HeaderUserEmail);
                if (!string.IsNullOrEmpty(userEmail))
                    userEmail = WebUtility.UrlDecode(userEmail);
                return userEmail;
            }
            set
            {
                this.UserEmail = value;
            }
        }

        public string OrgName => throw new NotImplementedException();

        public string ClientIp => throw new NotImplementedException();

        public string ClientAgent => throw new NotImplementedException();

        public string Access_Token => throw new NotImplementedException();

        public IServiceProvider Provider => throw new NotImplementedException();

        public ClaimsPrincipal User => throw new NotImplementedException();

        public string Domain => throw new NotImplementedException();

        public string Path => throw new NotImplementedException();

        private T GetHeaderValueAs<T>(string headerName)
        {

            if (Context?.Request?.Headers?.TryGetValue(headerName, out StringValues values) ?? false)
            {
                var rawValues = values.ToString();

                if (!string.IsNullOrEmpty(rawValues))
                    return (T)Convert.ChangeType(values.ToString(), typeof(T));
            }

            return default(T);
        }

    }
}
