﻿using Haravan.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace Haravan.Web.Api.Utils.Extensions
{
    public static class StringHelper
    {
        public static bool IsEmpty(this string str)
        {
            return string.IsNullOrEmpty(str);
        }

        public static bool IsWhiteSpace(this string str)
        {
            return string.IsNullOrWhiteSpace(str);
        }

        public static bool CheckStringIsNullOrEmptyOrWhitespace(this string str)
        {
            return string.IsNullOrEmpty(str) || string.IsNullOrWhiteSpace(str);
        }

        public static bool IsSameAs(this string str1, string str2)
        {
            return string.Compare(str1, str2, true) == 0;
        }
        public static string StripTagsCharArray(string input)
        {
            return string.IsNullOrWhiteSpace(input)
                ? input
                : Regex.Replace(input, @"<[^<>]*?>", string.Empty);
        }

        public static string StripTagsCharArrayByWhitespace(string input)
        {
            return string.IsNullOrWhiteSpace(input)
                ? input
                : Regex.Replace(input, @"<[^<>]*?>", " ");
        }

        public static byte[] ToByte(this string str)
        {
            if (str == null) return null;
            return Convert.FromBase64String(str);
        }

        static char HexDigit(int num)
        {
            return (char)((num < 10) ? (num + '0') : (num + ('A' - 10)));
        }

        public static string ToHex(this byte[] sArray)
        {
            string result = null;

            if (sArray != null)
            {
                char[] hexOrder = new char[sArray.Length * 2];

                int digit;
                for (int i = 0, j = 0; i < sArray.Length; i++)
                {
                    digit = (sArray[i] & 0xf0) >> 4;
                    hexOrder[j++] = HexDigit(digit);
                    digit = sArray[i] & 0x0f;
                    hexOrder[j++] = HexDigit(digit);
                }
                result = new string(hexOrder);
            }
            return result.ToLower();
        }

        public static byte[] FromHex(this string hexString)
        {
            if (hexString == null)
                throw new ArgumentNullException("hexString");

            hexString = hexString.ToUpper();

            bool spaceSkippingMode = false;

            int i = 0;
            int length = hexString.Length;

            if ((length >= 2) &&
                (hexString[0] == '0') &&
                ((hexString[1] == 'x') || (hexString[1] == 'X')))
            {
                length = hexString.Length - 2;
                i = 2;
            }

            // Hex strings must always have 2N or (3N - 1) entries.

            if (length % 2 != 0 && length % 3 != 2)
            {
                throw new ArgumentException("Argument_InvalidHexFormat");
            }

            byte[] sArray;

            if (length >= 3 && hexString[i + 2] == ' ')
            {
                spaceSkippingMode = true;

                // Each hex digit will take three spaces, except the first (hence the plus 1).
                sArray = new byte[length / 3 + 1];
            }
            else
            {
                // Each hex digit will take two spaces
                sArray = new byte[length / 2];
            }

            int digit;
            int rawdigit;
            for (int j = 0; i < hexString.Length; i += 2, j++)
            {
                rawdigit = ConvertHexDigit(hexString[i]);
                digit = ConvertHexDigit(hexString[i + 1]);
                sArray[j] = (byte)(digit | (rawdigit << 4));
                if (spaceSkippingMode)
                    i++;
            }
            return (sArray);
        }

        static int ConvertHexDigit(char val)
        {
            if (val <= '9' && val >= '0')
                return (val - '0');
            else if (val >= 'a' && val <= 'f')
                return ((val - 'a') + 10);
            else if (val >= 'A' && val <= 'F')
                return ((val - 'A') + 10);
            else
                throw new ArgumentException("ArgumentOutOfRange_Index");
        }

        public static string UppercaseFirstLetter(string s)
        {
            if (string.IsNullOrEmpty(s))
                return s;
            if (s.Length == 1)
                return s.ToUpper();
            return s.Remove(1).ToUpper() + s.Substring(1);
        }
        public static string LowercaseFirstLetter(string s)
        {
            if (string.IsNullOrEmpty(s))
                return s;
            if (s.Length == 1)
                return s.ToLower();
            return s.Remove(1).ToLower() + s.Substring(1);
        }
        public static string ToAscii(this string str)
        {
            if (string.IsNullOrWhiteSpace(str)) return null;
            str = UnSignString(str);

            //Remove Special Char
            var regex = new Regex(@"[^a-zA-Z0-9\-_\ \.\[\]]");
            str = regex.Replace(str, "_");

            return str;
        }

        public static string UnSignString(this string str)
        {
            string[] signs = new string[] {
                "aAeEoOuUiIdDyY",
                "áàạảãâấầậẩẫăắằặẳẵ",
                "ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ",
                "éèẹẻẽêếềệểễ",
                "ÉÈẸẺẼÊẾỀỆỂỄ",
                "óòọỏõôốồộổỗơớờợởỡ",
                "ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ",
                "úùụủũưứừựửữ",
                "ÚÙỤỦŨƯỨỪỰỬỮ",
                "íìịỉĩ",
                "ÍÌỊỈĨ",
                "đ",
                "Đ",
                "ýỳỵỷỹ",
                "ÝỲỴỶỸ"
            };
            for (int i = 1; i < signs.Length; i++)
            {
                for (int j = 0; j < signs[i].Length; j++)
                {
                    str = str.Replace(signs[i][j], signs[0][i - 1]);
                }
            }
            return str;
        }

        public static string GenJs(int number)
        {
            var one = "+!![]";
            var onefirst = "!+[]+0";
            var str = number.ToString();
            var sb = new StringBuilder("0");
            foreach (var c in str)
            {
                var num = int.Parse(c.ToString());
                sb.Append("+(");
                bool isHas = false;
                for (int n = 0; n <= num; n++)
                {
                    if (n == 0) { }
                    else if (n == 1)
                    {
                        sb.Append(onefirst);
                        isHas = true;
                    }
                    else
                    {
                        sb.Append(one);
                        isHas = true;
                    }
                }
                if (!isHas)
                {
                    sb.Append("0)+[]");
                }
                else
                    sb.Append(")+[]");
            }

            return sb.ToString();
        }


        public static string GetFriendlyImageUrl(this string imageUrl)
        {
            if (CheckStringIsNullOrEmptyOrWhitespace(imageUrl))
            {
                return string.Empty;
            }
            var slug = imageUrl;
            if (slug.Length > 218)
                slug = slug.Substring(0, 218);

            slug = UnSignString(slug);

            slug = Regex.Replace(slug, @"[^a-zA-Z0-9]+", "_");
            slug = Regex.Replace(slug, @"[^a-zA-Z0-9\-_/\\]+", "");
            slug = Regex.Replace(slug, "(-)+", "_").Trim('_');
            slug = slug.ToLower();
            if (string.IsNullOrEmpty(slug))
            {
                slug = "default_image_url";
            }
            else if (slug[slug.Length - 1] == '_')
                slug = slug.Remove(slug.Length - 1, 1);
            return slug;
        }

        public static string CreateFriendlyURL(string title)
        {
            if (string.IsNullOrEmpty(title))
                return string.Empty;

            string slug = title;
            if (slug.Length > 218)
                slug = slug.Substring(0, 218);

            slug = UnSignString(slug);

            slug = Regex.Replace(slug, @"[^a-zA-Z0-9]+", "-");
            slug = Regex.Replace(slug, @"[^a-zA-Z0-9\-_/\\]+", "");
            slug = Regex.Replace(slug, "(-)+", "-").Trim('-');
            slug = slug.ToLower();
            if (string.IsNullOrEmpty(slug))
            {
                slug = "default-handle";
            }
            else if (slug[slug.Length - 1] == '-')
                slug = slug.Remove(slug.Length - 1, 1);
            return slug;
        }



        public static string JoinVariantTitle(string value1, string value2, string value3)
        {
            string result = string.Empty;
            if (!CheckStringIsNullOrEmptyOrWhitespace(value1)) result = $"{value1}";
            if (!CheckStringIsNullOrEmptyOrWhitespace(value2)) result += $" / {value2}";
            if (!CheckStringIsNullOrEmptyOrWhitespace(value3)) result += $" / {value3}";
            return result;
        }

        public static (string firstname, string lastname) SplitFullName(string fullName)
        {
            if (string.IsNullOrWhiteSpace(fullName))
                return (null, null);

            int intPosEscape = fullName.LastIndexOf(' ');
            if (intPosEscape >= 0)
                return (fullName.Substring(0, intPosEscape).Trim(), fullName.Substring(intPosEscape + 1).Trim());
            else
                return (fullName.Trim(), null);
        }
        public static string JoinFullName(string lastName, string firstName)
        {
            var result = string.Empty;
            if (!CheckStringIsNullOrEmptyOrWhitespace(lastName)) result = $"{lastName}";
            if (!CheckStringIsNullOrEmptyOrWhitespace(firstName)) result += $" {firstName}";
            if (!CheckStringIsNullOrEmptyOrWhitespace(result)) return result.Trim();
            return result;
        }
        public static string IsValidSummaryData(string obj)
        {
            if (CheckStringIsNullOrEmptyOrWhitespace(obj))
            {
                return null;
            }
            var result = string.Empty;

            var arrStr = obj.Trim().Split(',').ToList();
            var resultList = new List<string>();

            if (arrStr != null && arrStr.Any())
            {
                var resultExist = new List<string>();
                foreach (var str in arrStr)
                {
                    var summary = str.StripHtml();
                    var unsign = UnSignString(summary).ToLower();
                    if (!resultExist.Contains(unsign))
                    {
                        resultExist.Add(unsign);
                        resultList.Add(summary);
                    }
                }
            }
            if (resultList != null && resultList.Any())
            {
                result = string.Join(",", resultList);
            }
            return result;
        }

        public static bool IsValidUrl(this string url)
        {
            if (url == null) return false;
            return Uri.TryCreate(url, UriKind.Absolute, out Uri uriResult)
               && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
        }

        public static bool IsPasswordStrong(string password)
        {
            if (string.IsNullOrEmpty(password)
                || password.Length < 6
                || password.Length > 50)
                return false;

            int score = 0;

            if (Regex.IsMatch(password, @"[0-9]+(\.[0-9][0-9]?)?", RegexOptions.ECMAScript))
                score++;
            if (Regex.IsMatch(password, @"[a-z]", RegexOptions.ECMAScript))
                score++;
            if (Regex.IsMatch(password, @"[A-Z]", RegexOptions.ECMAScript))
                score++;

            if ((PasswordScore)score == PasswordScore.Strong)
                return true;

            return false;
        }

        public enum PasswordScore
        {
            Weak = 1,
            Medium = 2,
            Strong = 3,
        }

        public static (string option1, string value1, string option2, string value2, string option3, string value3) GetVariantOptions(string optionValue)
        {
            var option1 = string.Empty;
            var value1 = string.Empty;
            var option2 = string.Empty;
            var value2 = string.Empty;
            var option3 = string.Empty;
            var value3 = string.Empty;
            if (!CheckStringIsNullOrEmptyOrWhitespace(optionValue))
            {
                var optionvalues = optionValue.Split(new string[] { "|||" }, StringSplitOptions.None);
                if (optionvalues != null && optionvalues.Length >= 1)
                {
                    string[] values = optionvalues[0].Split(new string[] { "|::|" }, StringSplitOptions.None);
                    option1 = values[0];
                    value1 = values[1];
                }
                if (optionvalues != null && optionvalues.Length >= 2)
                {
                    string[] values = optionvalues[1].Split(new string[] { "|::|" }, StringSplitOptions.None);
                    option2 = values[0];
                    value2 = values[1];
                }
                if (optionvalues != null && optionvalues.Length >= 3)
                {
                    string[] values = optionvalues[2].Split(new string[] { "|::|" }, StringSplitOptions.None);
                    option3 = values[0];
                    value3 = values[1];
                }
            }
            return (option1, value1, option2, value2, option3, value3);

        }


        public static bool IsValidEmail(this string email, int maxLength)
        {
            var patternEmail =
            "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$";

            if (!Regex.IsMatch(email, patternEmail))
            {
                return false;
            }

            if (email.Length > maxLength)
            {
                return false;
            }

            return true;
        }

        public static bool IsValidPhone(this string phone)
        {
            if (CheckStringIsNullOrEmptyOrWhitespace(phone))
            {
                return true;
            }

            var patternPhone = @"^\d{8,15}$";
            return Regex.IsMatch(phone, patternPhone);
        }



        public static string Hash(this string text, string salt = null)
        {
            text = (text ?? "");
            salt = (salt ?? "");

            return CryptoHelpers.Sha512Hash(salt, text);
        }

        public static (string value1, string value2, string value3) SplitVariantTitle(this string variantTitle)
        {
            if (CheckStringIsNullOrEmptyOrWhitespace(variantTitle))
            {
                return (null, null, null);
            }
            var splitOptions = variantTitle.Trim().Split('/');
            string value1 = string.Empty, value2 = string.Empty, value3 = string.Empty;
            if (splitOptions != null && splitOptions.Length > 0)
            {
                if (splitOptions.Length >= 1)
                {
                    value1 = splitOptions[0];
                }
                if (splitOptions.Length >= 2)
                {
                    value2 = splitOptions[1];
                }
                if (splitOptions.Length >= 3)
                {
                    value3 = splitOptions[2];
                }
                return (value1, value2, value3);
            }
            return (null, null, null);
        }
        public static string GetDisplayName(this Enum value)
        {
            Type enumType = value.GetType();
            var enumValue = Enum.GetName(enumType, value);
            MemberInfo member = enumType.GetMember(enumValue)[0];

            var attrs = member.GetCustomAttributes(typeof(DisplayAttribute), false);
            var outString = ((DisplayAttribute)attrs[0]).Name;

            if (((DisplayAttribute)attrs[0]).ResourceType != null)
            {
                outString = ((DisplayAttribute)attrs[0]).GetName();
            }

            return outString;
        }


    }
}
