﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Haravan.Web.Api.Utils.Extensions
{
    public static class ResourceHelper
    {
        public static string DisplayForEnumValue(Object value)
        {
            Type enumType = value.GetType();
            var fieldInfo = enumType.GetField(value.ToString());
            if (fieldInfo != null)
            {
                DisplayAttribute displayAtt = fieldInfo.GetCustomAttributes(typeof(DisplayAttribute), false).FirstOrDefault() as DisplayAttribute;
                return displayAtt.GetName();
            }
            else
            {
                return null;
            }
        }
    }
}