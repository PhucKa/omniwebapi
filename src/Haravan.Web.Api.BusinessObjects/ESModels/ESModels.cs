﻿using BHN.Core;
using BHN.Entity;
using Haravan.Web.Api.BusinessObjects.Enums;
using System;
using System.Collections.Generic;

namespace Haravan.Web.Api.BusinessObjects.ESModels
{
    public class FilterSearchModel : FilterSearchModelBase
    {
        public SysView ViewId { get; set; }
        public List<string> SelectField { get; set; }
        public string SortFilterNestedPath { get; set; }
        public FilterSearchField SortFilterFields { get; set; }
    }

    public class ESUrlRedirectModel : ESEntity
    {
        public string OldPath { get; set; }
        public string RedirectTo { get; set; }
        public DateTime CreatedDate { get; set; }
        public string OldPath_ori { get; set; }
        public string RedirectTo_ori { get; set; }
    }

    public class ESGlobalSearchResult
    {
        public string title { get; set; }
        public string jsonArrayResult { get; set; }
        public Dictionary<int, long> Ids { get; set; }
        public long totalrecord { get; set; } = 0;
    }

    public class ESArticleModelv2 : ESEntity
    {
        public long BlogId { get; set; }
        public long AuthorId { get; set; }
        public string BlogTitle { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string AuthorName { get; set; }
        public List<string> Tags { get; set; }
        public DateTime? PublishedDate { get; set; }
        public long? CreatedUser { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? UpdatedUser { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long sysprioritytype { get; set; }
    }
    public class ESBlogModelv2 : ESEntity
    {
        public string Title { get; set; }
        public string UrlHandle { get; set; }
        public DateTime PublishedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public List<string> Tags { get; set; }
    }
    public class ESCommentModelv2 : ESEntity
    {

        public long ArticleId { get; set; }
        public string Comment { get; set; }
        public int Status { get; set; }
        public string CreatorName { get; set; }
        public string CreatorEmail { get; set; }
        public string ArticleTitle { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime CreatedAt { get; set; }
        public bool IsDeleted { get; set; }
        public long BlogId { get; set; }
    }
    public class ESPageModelv2 : ESEntity
    {
        public string Title { get; set; }
        public string AuthorName { get; set; }
        public string ShortContent { get; set; }
        public string UrlHandle { get; set; }
        public DateTime? PublishedDate { get; set; }
        public string Content { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public long sysprioritytype { get; set; }
    }
    public class ESUrlRedirectModelv2 : ESEntity
    {
        public string OldPath { get; set; }
        public string RedirectTo { get; set; }
        public DateTime CreatedDate { get; set; }
        public string OldPath_ori { get; set; }
        public string RedirectTo_ori { get; set; }
    }
}